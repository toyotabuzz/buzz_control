<?
ob_start("ob_gzhandler"); 
//header("Cache-Control: no-cache");
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter("must-revalidate");
session_save_path ("");
session_start();
// -- Database ------------------------------------------------------------
    define("DB_HOST", "10.21.193.246");			    // Database Host
    define("DB_USER", "root");			        // Database User
    define("DB_PASS", "password");				        // password for user of database
    define("DB_NAME", "buzz1");               // Database Name

	
/** Directory *************/
	Define ("gDIR_IMG","../images/misc/");
	Define ("gDIR_UPLOAD","../upload/");
	
// -- Paths ---------------------------------------------------------------
	define("PATH_BASE","");
    define("PATH_CLASS",PATH_BASE."class/");
	define("PATH_INCLUDE",PATH_BASE."include/");
	define("PATH_IMG",PATH_BASE."images/misc/");
	define("PATH_UPLOAD",PATH_BASE."upload/");
	
// -- Users and Misc -------------------------------------------------

	define("USERNAME_MIN", 3);
    define("USERNAME_MAX", 15);
	define("PASSWORD_MIN", 3);
    define("PASSWORD_MAX", 15);

    define("TITLE", "Buzz Web Service");               // Title of pages
	

// -- Class include -------------------------------------------------------
    include(PATH_CLASS."clDb.php");
	include(PATH_CLASS."clDataList.php");

	include (PATH_CLASS."english.php");
	include (PATH_CLASS."functions.php");

// -- Sort and Paging -------------------------------------------------
	define ("DEF_PAGING",15);
	define ("DEF_SORT_ASC","images/arrowDown.gif");
	define ("DEF_SORT_DESC","images/arrowUp.gif");
	define ("DEF_SORT_ASC_ACTIVE","images/arrowDownRed.gif");
	define ("DEF_SORT_DESC_ACTIVE","images/arrowUpRed.gif");
?>

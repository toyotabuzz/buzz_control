<?
include("common.php");

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objPaymentRemarkList = new PaymentRemarkList();
$objPaymentRemarkList->setFilter(" del != 'yes' ");
$objPaymentRemarkList->setPageSize(0);
$objPaymentRemarkList->setSort(" rank ASC ");
$objPaymentRemarkList->load();

$objPaymentDiscountList = new PaymentDiscountList();
$objPaymentDiscountList->setFilter(" del != 'yes' ");
$objPaymentDiscountList->setPageSize(0);
$objPaymentDiscountList->setSort(" rank ASC ");
$objPaymentDiscountList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objOrder = new Order();
$objOrderExtra = new OrderExtra();

$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$objInsureItem = new InsureItem();
$objInsureItem->set_insure_item_id($hInsureItemId);
$objInsureItem->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();
$objInsureCarCom  = $objInsureCar->get_company_id();

$objInsureForm = new InsureForm();
$objInsureForm->set_insure_id($objInsure->get_insure_id());
$objInsureForm->loadByCondition('insure_id ='.$objInsure->get_insure_id().' And frm_type = 0901');
$objInsureFormCom = $objInsureForm->get_company_id();

$objMM = new Member();
$objMM->setMemberId($sMemberId);
$objMM->load();

$objCompanyMM = new Company();
$objCompanyMM->setCompanyId($objMM->getCompanyId());
$objCompanyMM->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($hCustomerId);
$objCustomer->load();

$objMember = new member();
$objMember->setMemberId($objInsure->get_sale_id());
$objMember->load();
$hSaleName=$objMember->getFirstname()." ".$objMember->getLastname();

$objQIList = new InsureItemList();
$objQIList->setFilter(" P.insure_id = $hInsureId ");
$objQIList->setPageSize(0);
$objQIList->setSort(" insure_item_id ASC ");
$objQIList->load();


//get quotation add no
$objQuoAddOn = new QuotationItemAddonList();
$objQuoAddOn->setFilter(" quotation_item_id = '".$objInsure->get_quotation_item_id()."'");
$objQuoAddOn->setPageSize(0);
$objQuoAddOn->setSortDefault(" created_at ASC ");
$objQuoAddOn->load();

//get quotation add no
$objInsureReceiptList = new InsureReceiptList();
$objInsureReceiptList->setFilter(" insure_item_id = '".$hInsureItemId."' AND order_price_id IS NULL AND isactive = 'Y' ");
$objInsureReceiptList->setPageSize(0);
$objInsureReceiptList->setSortDefault(" created_at ASC ");
$objInsureReceiptList->load();

//get quotation add no
$objInsureReceipPaytList = new InsureReceiptList();
$objInsureReceipPaytList->setFilter(" insure_item_id = '".$hInsureItemId."' AND order_price_id IS NOT NULL AND isactive = 'Y' ");
$objInsureReceipPaytList->setPageSize(0);
$objInsureReceipPaytList->setSortDefault(" created_at ASC ");
$objInsureReceipPaytList->load();

// echo '<pre> CarCom = '; print_r($objInsureReceiptList);
//get quotation no
$objQuoItem = new InsureQuotationItem();
$objQuoItem->loadByCondition(" quotation_item_id = ".$objInsure->get_quotation_item_id());
// echo '<pre>'; print_r($objQuoItem);

// echo '<pre> CarCom = '; print_r($objInsureCarCom);
// echo '<pre> FormCom = '; print_r($objInsureFormCom);
// echo '<pre> MCom = '; print_r($objCompanyMM); exit;

if (empty($hSubmit)) {
	if ($objInsureItem->get_payment_status() =="��������") {
		$strMode="Update";

		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" status = 4 AND order_extra_id = $hInsureItemId  AND order_id = $hInsureId ");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();		
		
		$arrDate = explode(" ",$objInsureItem->get_insure_date());
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Hours = $arrTime[0];
		$Minute = $arrTime[1];
		
		
	} else {
		$strMode="Add";
		$arrDate = explode(" ",date("Y-m-d H:i:s"));
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Hours = $arrTime[0];
		$Minute = $arrTime[1];
	}

} else {
	if (!empty($hSubmit)) {
		$objOrderPaymentListCheck = new OrderPaymentList();
		$objOrderPaymentListCheck->setFilter(" status = 4 AND order_extra_id = $hInsureItemId  AND order_id = $hInsureId ");
		$objOrderPaymentListCheck->setPageSize(0);
		$objOrderPaymentListCheck->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentListCheck->load();	
		
		
			//update to insure_Item
			//get quotation no
			$objQ = new InsureQuotation();
			$objQ->loadByCondition(" car_id= ".$objInsure->get_car_id()." AND year_extend = '".$objInsure->get_year_extend()."' AND selected= 'yes'  ");
			
			$hOrderNo = $objQ->get_quotation_number()."-".$objInsureItem->get_payment_order();

			//update to insure_item_detail
			
			$hHaveHead = "No";//�� vat
			$hHaveNoHead = "No";//����� vat
			
			$hPnum = $_POST["hPnum"];
			for($i=1;$i<=$hPnum;$i++){
			
			
				if($_POST["hPaymentSubjectId_".$i] > 0){
					$objPaymentSubject = new PaymentSubject();
					$objPaymentSubject->setPaymentSubjectId($_POST["hPaymentSubjectId_".$i]);
					$objPaymentSubject->load();
					
					if($objPaymentSubject->getNohead() == 1){
						$hHaveHead = "Yes";
					}
					
					if($objPaymentSubject->getNohead() == 0){
						$hHaveNoHead = "Yes";
					}				
				}
			
				if($_POST["hOrderPriceId_".$i] > 0){
					//update ��¡��
					$objInsureItemDetail = new InsureItemDetail();
					$objInsureItemDetail->setOrderPriceId($_POST["hOrderPriceId_".$i] );
					$objInsureItemDetail->setPaymentSubjectId($_POST["hPaymentSubjectId_".$i]);
					$objInsureItemDetail->setPriceAcc(str_replace(",", "", $_POST["hPriceAcc_".$i]));
					$objInsureItemDetail->setQty($_POST["hQty_".$i]);
					$objInsureItemDetail->setRemark($_POST["hRemark_".$i]);
					$objInsureItemDetail->setPayin($_POST["hPayin_".$i]);
					$objInsureItemDetail->setPrice(str_replace(",", "",$_POST["hPrice_".$i]));
					$objInsureItemDetail->updateAcc();
					
				}else{
					

					if(str_replace(",", "", $_POST["hPrice_".$i]) > 0){
						$objInsureItemDetail = new InsureItemDetail();		
						$objInsureItemDetail->setOrderId($hInsureItemId);
						$objInsureItemDetail->setOrderExtraId($hInsureId);
						$objInsureItemDetail->setPaymentSubjectId($_POST["hPaymentSubjectId_".$i]);
						$objInsureItemDetail->setPriceAcc(str_replace(",", "", $_POST["hPriceAcc_".$i]));
						$objInsureItemDetail->setQty($_POST["hQty_".$i]);
						$objInsureItemDetail->setRemark($_POST["hRemark_".$i]);
						$objInsureItemDetail->setPayin($_POST["hPayin_".$i]);
						$objInsureItemDetail->setPrice(str_replace(",", "",$_POST["hPrice_".$i]));
						$objInsureItemDetail->add();
					}
					
				}//end check hOrderPriceId
			}//end for
			
			//update order payment
			for($i=0;$i<=3;$i++){
					
					$objOrderPayment = new OrderPayment();
					$objOrderPayment->setOrderExtraId($hInsureItemId);
					$objOrderPayment->setOrderId($hInsureId);
					$objOrderPayment->setOrderPaymentId($hOrderPaymentId[$i]);
					$objOrderPayment->setPaymentTypeId($_POST["hPaymentTypeId_".$i]);
					$objOrderPayment->setPaymentRemark($_POST["hPaymentRemark_".$i]);
					$objOrderPayment->setPaymentRemarkId($_POST["hPaymentRemarkId_".$i]);
					$objOrderPayment->setPaymentDiscountId($_POST["hPaymentDiscountId_".$i]);
					$objOrderPayment->setPrice(str_replace(",","",$_POST["hPaymentPrice_".$i]));
					$objOrderPayment->setBankId($_POST["hBankId_".$i]);
					$objOrderPayment->setBranch($_POST["hBankBranch_".$i]);
					$objOrderPayment->setCheckNo($_POST["hCheckNo_".$i]);
					$objOrderPayment->setNoHead($_POST["hNoHead_".$i]);
					$strCheckDate =  $_POST["Year01_".$i]."-".$_POST["Month01_".$i]."-".$_POST["Day01_".$i];
					$objOrderPayment->setCheckDate($strCheckDate);
					
					$objOrderPayment->setStatus(4);
					if($hOrderPaymentId[$i] != ""){
						if($_POST["hPaymentPrice_".$i] > 0){
							//echo $i." ".$_POST["hPaymentTypeId_".$i]."<br>";
							$objOrderPayment->update();
						}else{
							$objOrderPayment->delete();
						}
					}else{
						if($_POST["hPaymentPrice_".$i] > 0){
							$objOrderPayment->add();
						}
					}

			}

			$sumpay = 0;
			foreach ($_POST["hPaymentOther"] as $key => $value) {
				$sumpay += (float) str_replace(",","",$_POST["hPaymentOtherAmt"][$value]);
			}
			if($sumpay > 0){

				$objInsureItemDetail = new InsureItemDetail();		
				$objInsureItemDetail->setOrderId($hInsureItemId);
				$objInsureItemDetail->setOrderExtraId($hInsureId);
				$objInsureItemDetail->setPaymentSubjectId(95); // SET DEFAULT
				$objInsureItemDetail->setPriceAcc(str_replace(",","",$sumpay));
				$objInsureItemDetail->setQty(1);
				$objInsureItemDetail->setRemark('���кҧ��ǹ');
				$objInsureItemDetail->setPayin(1);
				$objInsureItemDetail->setPrice(str_replace(",","",$sumpay));
				$objInsureItemDetail->add();

				foreach ($_POST["hPaymentOther"] as $key => $value) {
					$objInsureReceipt = new InsureReceipt();
					$objInsureReceipt->set_id($value);
					$objInsureReceipt->set_order_price_id($objInsureItemDetail->getOrderPriceId());
					$objInsureReceipt->updateOrderPayment();
				}

			}
			
			//exit;
			
			$objInsureItem = new InsureItem();	
			$objInsureItem->set_insure_item_id($hInsureItemId);
			$objInsureItem->load();
			
			if($objInsureItem->get_payment_number_head()  == ""){
		
				if($hHaveHead == "Yes"){
					$objMMCompany = $objMM->getCompanyId();
					$objInsureCarCom  = $objInsureCar->get_company_id();

					//������ҡ sale ��ҹ�� ��� �ҡ insureform�����
					if (!empty($objInsureFormCom)) {
						$sCompanyId = $objInsureFormCom;
					} else {
						$sCompanyId = $objInsureCarCom;
					}
					
					$objCompany = new Company();
					$objCompany->setCompanyId($sCompanyId);
					$objCompany->load();
					
					$objInsureItem = New InsureItem();
					$objInsureItem->loadMaxHead(" payment_number_head LIKE '".$objCompany->getShortTitleEn().date("y")."%'");
					if($objInsureItem->get_payment_number_head() != ""){
						$orderNum = substr($objInsureItem->get_payment_number_head(),-5)+1;
						if($orderNum <10) $strBooking = "0000".$orderNum;
						if($orderNum <100 AND $orderNum >=10) $strBooking = "000".$orderNum;
						if($orderNum <1000  AND $orderNum >=100) $strBooking = "00".$orderNum;
						if($orderNum <10000  AND $orderNum >=1000) $strBooking = "0".$orderNum;
						if($orderNum >=10000  ) $strBooking = $orderNum;
						$hPaymentNumberHead = $objCompany->getShortTitleEn().date("ymd")."/A".$strBooking;
					}else{
						$hPaymentNumberHead = $objCompany->getShortTitleEn().date("ymd")."/A00001";
					}
					
					$objCompany = new Company();
					$objCompany->setCompanyId($sCompanyId);
					$objCompany->load();
			
					$objInsureItem = New InsureItem();
					$objInsureItem->loadMaxCashierNumber(" cashier_number LIKE '".$objCompany->getShortTitleEn().date("y")."%'");
					if($objInsureItem->get_cashier_number() != ""){
						$orderNum = substr($objInsureItem->get_cashier_number(),-5)+1;
						if($orderNum <10) $strBooking = "0000".$orderNum;
						if($orderNum <100 AND $orderNum >=10) $strBooking = "000".$orderNum;
						if($orderNum <1000  AND $orderNum >=100) $strBooking = "00".$orderNum;
						if($orderNum <10000  AND $orderNum >=1000) $strBooking = "0".$orderNum;
						if($orderNum >=10000  ) $strBooking = $orderNum;
						$hCashierNumber = $objCompany->getShortTitleEn().date("y")."/".$strBooking;
					}else{
						$hCashierNumber = $objCompany->getShortTitleEn().date("y")."/00001";
					}
					
					$hOldNumberHead = $hCashierNumber;
					
				}
				

			
			}else{
				$hCashierNumber =$objInsureItem->get_cashier_number();
				$hOldNumberHead =$objInsureItem->get_old_number_head();
				$hPaymentNumberHead =$objInsureItem->get_payment_number_head();
			}
			
			
			if($objInsureItem->get_payment_number_nohead()  == ""){
		
				if($hHaveNoHead == "Yes"){
					$objMMCompany = $objMM->getCompanyId();
					$objInsureCarCom  = $objInsureCar->get_company_id();

					//������ҡ sale ��ҹ�� ��� �ҡ insureform�����
					if (!empty($objInsureFormCom)) {
						$sCompanyId = $objInsureFormCom;
					} else {
						$sCompanyId = $objInsureCarCom;
					}
					
					$objCompany = new Company();
					$objCompany->setCompanyId($sCompanyId);
					$objCompany->load();
					
					$objInsureItem = New InsureItem();
					$objInsureItem->loadMaxNoHead(" payment_number_nohead LIKE '".$objCompany->getShortTitleEn().date("y")."%'");
					//echo $objInsureItem->get_payment_number_nohead(); 
					if($objInsureItem->get_payment_number_nohead() != ""){
						$orderNum = substr($objInsureItem->get_payment_number_nohead(),-5)+1;
						if($orderNum <10) $strBooking = "0000".$orderNum;
						if($orderNum <100 AND $orderNum >=10) $strBooking = "000".$orderNum;
						if($orderNum <1000  AND $orderNum >=100) $strBooking = "00".$orderNum;
						if($orderNum <10000  AND $orderNum >=1000) $strBooking = "0".$orderNum;
						if($orderNum >=10000  ) $strBooking = $orderNum;
						$hPaymentNumberNoHead = $objCompany->getShortTitleEn().date("ymd")."/B".$strBooking;
					}else{
						$hPaymentNumberNoHead = $objCompany->getShortTitleEn().date("ymd")."/B00001";
					}
					
				}
			
			}else{
				$hPaymentNumberNoHead = $objInsureItem->get_payment_number_nohead();
			}
			
			//echo " hPaymentNumberNoHead =".$hPaymentNumberNoHead."<br>";
			//echo " hPaymentNumberHead =".$hPaymentNumberHead."<br>";
			//exit;

			$objInsureItem = new InsureItem();
			$objInsureItem->set_insure_item_id($hInsureItemId);
			$objInsureItem->load();
			
			if($objInsureItem->get_payment_status() == ""){
			
				$objInsureItem = new InsureItem();
				$objInsureItem->set_insure_item_id($hInsureItemId);
				$objInsureItem->set_payment_status("��������");
				$objInsureItem->set_payment_no($hOrderNo);
				$objInsureItem->set_insure_date($Year."-".$Month."-".$Day." ".$Hours.":".$Minute.":00");
				$objInsureItem->updateStatus();

			
				
				$objInsureItem = new InsureItem();	
				$objInsureItem->set_insure_item_id($hInsureItemId);
				$objInsureItem->set_acc_approve("yes");
				$objInsureItem->set_acc_date(date("Y-m-d H:i:s"));

				$objInsureItem->set_acc_by($sMemberId);
				$objInsureItem->set_acc_company_id($sCompanyId);
				$objInsureItem->set_acc_remark($hAccRemark);
				$objInsureItem->set_payment_number_head($hPaymentNumberHead);
				$objInsureItem->set_payment_number_nohead($hPaymentNumberNoHead);
				$objInsureItem->set_cashier_number($hCashierNumber);			
				$objInsureItem->updateAcc();

				$objMMCompany = $objMM->getCompanyId();
				$objInsureCarCom  = $objInsureCar->get_company_id();
				$objInsureFormCom = $objInsureForm->get_company_id();

				if ((int)$objMMCompany != (int)$objInsureFormCom) {
					if(!empty($objInsureFormCom)) {
						$Company = new Company();
						$Company->setCompanyId($objInsureForm->get_company_id());
						$Company->load();
						$objInsureItem->set_old_number_nohead($objCompanyMM->getShortTitleEn()." �Ѻ᷹ ".$Company->getShortTitleEn());
						$objInsureItem->updateOldNumberNohead();
					} else {
						if ((int)$objMMCompany != (int)$objInsureCarCom) {
						    $Company = new Company();
						    $Company->setCompanyId($objInsureCar->get_company_id());
						    $Company->load();
						    $objInsureItem->set_old_number_nohead($objCompanyMM->getShortTitleEn()." �Ѻ᷹ ".$Company->getShortTitleEn());
						    $objInsureItem->updateOldNumberNohead();
						}
					}
				}

				if($hOldNumberHead != ""){
					$objInsureItem->set_old_number_head($hOldNumberHead);
					$objInsureItem->updateOldNumberHead();
				}
			}
			
			$objInsure = new Insure();
			$objInsure->set_insure_id($hInsureId);
			$objInsure->load();
			
			$objInsureCar = new InsureCar();
			$objInsureCar->set_car_id($objInsure->get_car_id());
			$objInsureCar->load();

			
			if( ($objInsureCar->get_operate() != "ccard") or   ($objInsure->get_status() != "ccard")   ){
			
			//check update ccard ��� ���Фú�ء�Ǵ ��������ʹ����ҧ��к�
			$objInsureItemList = new InsureItemList();
			$objInsureItemList->setFilter(" P.insure_id = $hInsureId and payment_status=''  ");
			$objInsureItemList->setPageSize(0);
			$objInsureItemList->load();
			//echo $objInsureItemList->mCount;
			//echo  "dd";
			//exit;

			if($objInsureItemList->mCount == 0 ){
			
			
				//���ʹ����ҧ
				$objInsureItemDetailList = new InsureItemDetailList();
				$objInsureItemDetailList->setFilter(" order_extra_id = $hInsureId  ");
				$objInsureItemDetailList->setPageSize(0);
				$objInsureItemDetailList->load();
				forEach($objInsureItemDetailList->getItemList() as $objItem) {
					
					$remain = $remain + $objItem->getPriceAcc() - $objItem->getPrice();

				}
				
				if($remain >= 0){
					
					$objInsure = new Insure();
					$objInsure->set_insure_id($hInsureId);
					$objInsure->set_status("ccard");
					$objInsure->updateStatus();

					$objInsure = new Insure();
					$objInsure->set_insure_id($hInsureId);
					$objInsure->load();
					
					$objInsureCar = new InsureCar();
					$objInsureCar->set_car_id($objInsure->get_car_id());
					$objInsureCar->set_date_verify($objInsure->get_k_start_date());
					$objInsureCar->set_operate("ccard");
					$objInsureCar->set_insure_company_id($objInsure->get_k_prb_id());
					$objInsureCar->set_lock_sale(1);
					$objInsureCar->set_fix_sale($objInsure->get_sale_id());
					$objInsureCar->updateCCard();

				}
				
				
			}
			
			
			}
			
			// �� sms �Ţ�������� �Ѻ�����Թ 
			if ( $objOrderPaymentListCheck->getCount() == 0) //�礢����š�ê����Թ
			{
				if ($objInsureItem->get_payment_order() == 1 || $objInsureItem->get_payment_order() == $objInsure->get_pay_number()) {
					if ($objCustomer->getMobile() != '') { //���������Ѿ��
						$k_number = '';
						if ($objInsure->get_k_number() != 0 || $objInsure->get_k_number() != '') { //���Ţ��������
							$linkPaymentReceive = getReceivePaymentLink($hInsureId,$hInsureItemId,$objCustomer->getMobile(),$sMemberId);
						}
					}
				} 
			}

			header("location:icardPrintPreview.php?hInsureId=$hInsureId&hInsureItemId=$hInsureItemId&hRemain=$remain&hPaymentNumberHead=$hPaymentNumberHead&hCashierNumber=$hCashierNumber");
			exit;

	}
}

$pageTitle = "1. ����������ѹ";
$pageContent = "1.9  �ѹ�֡�����Ū����Թ��Сѹ���";
$strHead03 = "�ѹ�֡�����š�èͧ";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}	
		var sumhPaymentOther = 0;
		var hPaymentOther = document.getElementsByClassName("hPaymentOther");
		hPaymentOther.each(function(index, value){
			if(index.checked){
				otherVal = 'hPaymentOther_'+index.value;
				sumhPaymentOther += parseFloat(rip_comma(document['forms']['frm01'][otherVal]['value']).toFixed(2)); 
			}
		});
		
		var hPaymentPrice_0  = rip_comma(rip_comma(document.forms.frm01.hPaymentPrice_0.value).toFixed(2));
		var hPaymentPrice_1  = rip_comma(rip_comma(document.forms.frm01.hPaymentPrice_1.value).toFixed(2));
		var hPaymentPrice_2  = rip_comma(rip_comma(document.forms.frm01.hPaymentPrice_2.value).toFixed(2));
		var hPaymentPrice_3  = rip_comma(rip_comma(document.forms.frm01.hPaymentPrice_3.value).toFixed(2));
		var sumhPaymentPrice = hPaymentPrice_0+hPaymentPrice_1+hPaymentPrice_2+hPaymentPrice_3+sumhPaymentOther;
		    sumhPaymentPrice = parseFloat(sumhPaymentPrice.toFixed(2));
		var hTotalPrice      = rip_comma(document.forms.frm01.hTotalPrice.value);
		var checkSum         = hTotalPrice-sumhPaymentPrice;
		    checkSum         = parseFloat(checkSum.toFixed(2));
		//console.log('checkSum | '+checkSum);
		if (checkSum > 5){ // �ʹ���¹��¡����ʹ����
			//console.log('hTotalPrice | '+hTotalPrice);
			//console.log('sumhPaymentPrice2 | '+sumhPaymentPrice);
			alert("�ӹǹ�Թ����������ͧ�����š�ê����Թ ���ú����ʹ�������ͧ����");
			return false;
		}

		// if (checkSum < -5){ �ʹ�����ҡ�����ʹ����
		// 	//console.log('hTotalPrice | '+hTotalPrice);
		// 	//console.log('sumhPaymentPrice2 | '+sumhPaymentPrice);
		// 	alert("�ӹǹ�Թ����������ͧ�����š�ê����Թ ���ú����ʹ�������ͧ����");
		// 	return false;
		// }
	

		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if(confirm("�س��ͧ��÷��кѹ�֡��¡���������?")){
			document.getElementById("hSubmit1").style.display = "none";
			document.frm01.submit();
			return true;
		}else{
			return false;
		}
	

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="icardUpdate.php" method="POST" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hOrderId?>">
	   <input type="hidden" name="hBookingNumber" value="<?=$objOrderExtra->getBookingNumber();?>">
	   <input type="hidden" name="hSendingNumber" value="<?=$objOrderExtra->getSendingNumber();?>">
	   <input type="hidden" name="hSendingNumberNohead" value="<?=$objOrderExtra->getSendingNumberNohead();?>">
	  <input type="hidden" name="hTrick" value="true">
	   <input type="hidden" name="hInsureId" value="<?=$hInsureId;?>">
	   <input type="hidden" name="hInsureItemId" value="<?=$hInsureItemId;?>">
	   <input type="hidden" name="hCustomerId" value="<?=$hCustomerId;?>">

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�â��</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"></td>
			<td  valign="top">
				
			</td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hours value="<?=$Hours?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute value="<?=$Minute?>"></td>		
						</tr>
						</table>
			
			</td>
		</tr>
		
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >				
				<?=$hSaleName?>
			</td>
			<td class="i_background03" valign="top"><strong>�ѹ��������ͧ</strong>   </td>
			<td valign="top" >			
				<?=formatShortDate($objInsure->get_date_protect())?>
			</td>	
		</tr>
		<?
			$objPrb = new InsureCompany();
			$objPrb->setInsureCompanyId($objInsure->get_p_prb_id());
			$objPrb->load();
			
			$objPrb01 = new InsureBroker();
			$objPrb01->setInsureBrokerId($objInsure->get_p_broker_id());
			$objPrb01->load();
		?>
		<tr>
			<td class="i_background03" ><strong>����ѷ �ú.</strong></td>			
			<td ><?if($objPrb01->getTitle() != ""){?><?=$objPrb01->getTitle()?>,&nbsp;<?}?><?=$objPrb->getTitle()?></td>
			<td class="i_background03" ><strong>�����Ţ �ú.</strong></td>			
			<td ><?=$objInsure->get_p_number();?></td>
		</tr>
		<?
			$objKom = new InsureCompany();
			$objKom->setInsureCompanyId($objInsure->get_k_prb_id());
			$objKom->load();
			
			$objPrb02 = new InsureBroker();
			$objPrb02->setInsureBrokerId($objInsure->get_k_broker_id());
			$objPrb02->load();			
		?>
		<tr>
			<td class="i_background03" ><strong>����ѷ��������</strong><br></td>			
			<td ><?if($objPrb02->getTitle() != ""){?><?=$objPrb02->getTitle()?>,&nbsp;<?}?><?=$objKom->getTitle()?></td>
			<td class="i_background03" ><strong>�����Ţ��������</strong></td>			
			<td ><?=$objInsure->get_k_number();?></td>
		</tr>
		</table>
	</td>
</tr>
</table>
	
<br>

<?if($hInsureId != ""){

		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsureId);
		$objInsure->load();

		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objOrder = new Order();
				
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objInsureCar->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
		$objCarSeries->load();

		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objInsureCar->get_color());
		$objCarColor->load();
		
?>

		<table width="100%" class="i_background02">
		<tr>
			<td>
				<table width="100%" cellpadding="2" cellspacing="0">
				<tr>
					<td><strong>������</strong></td>
					<td align="center">:</td>
					<td  class="i_background04" >
					<?=$objCarType->getTitle()?>					
					</td>
					<td><strong>���</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objCarModel->getTitle()?></td>					
					<td><strong>Ẻ</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objCarSeries->getTitle()?></td>
				</tr>
				<tr>
					<td><strong>��</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" >
				<?
					$objCarColor = new CarColor();
					$objCarColor->setCarColorId($objInsureCar->get_color());
					$objCarColor->load();
					echo $objCarColor->getTitle();
				?>
					</td>
					<td><strong>����¹</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objInsureCar->get_code()?></td>	
					<td><strong>��</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objInsureCar->get_register_year()?></td>
				</tr>
				<tr>
					<td><strong>�Ţ����ͧ</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objInsureCar->get_engine_number()?></td>
					<td><strong>�Ţ�ѧ</strong></td>
					<td align="center">:</td>			
					<td  class="i_background04" ><?=$objInsureCar->get_car_number()?></td>	
					<td><strong>�Ҥ�ö</strong></td>
					<td align="center">:</td>
					<td  class="i_background04" ><?=number_format($objInsureCar->get_price(),2)?></td>	
				</tr>		
	
				</table>
			</td>
		</tr>
		</table>		
		
		<br>
<?}?>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��һ�Сѹ���</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td >
				<table>
				<tr>
					<td valign="top">
						<?=$objCustomer->getTitle();?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<?=$name?>
					</td>

				</tr>
				</table>
			</td>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td  ><?=$objCustomer->getAddressLabel();?></td>
		</tr>

		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><?=$objCustomer->getTumbon();?></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><?=$objCustomer->getAmphur();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objCustomer->getProvince();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objCustomer->getZip();?></td>
		</tr>	
		</table>
	</td>
</tr>
</table>

<?if($objInsureItem->get_transfer() == 1){?>

		<table class="search" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td colspan="3" bgcolor="#E9D1D1"><input type="checkbox" name="hTransfer_<?=$i?>" value=1 <?if($objInsureItem->get_transfer() == 1) echo "checked"?>>&nbsp;�ó��١����͹�Թ��ҹ��Ҥ�� ���� ATM ��س��к���¡�ê��дѧ���</td>
			<td colspan="3" bgcolor="#E9D1D1" align="center"><h2><?if($objInsureItem->get_transfer_acc_approve() == 1){?><font color=red>��¡�ù���ҹ���͹��ѵԪ����Թ�ҡ�ҧ�ѭ������</font><?}else{?><font color=red>��¡�ù������ҹ���͹��ѵԪ����Թ�ҡ�ҧ�ѭ��</font><?}?></h2></td>
		</tr>	
		<tr>
			<td class="listTitle" align="center">��Ҥ��</td>
			<td class="listTitle" align="center">�Ң�</td>
			<td class="listTitle" align="center">�����Ţ��/��Ի ATM</td>
			<td class="listTitle" align="center">�ӹǹ�Թ</td>
			<td class="listTitle" align="center">�ѹ���</td>
			<td class="listTitle" align="center">�����˵�</td>
		</tr>
		<tr>
			<td align="center"><?$objBankList->printSelect("hTransferBank_$i",$objInsureItem->get_transfer_bank(),"��س����͡��¡��");?></td>
			<td align="center"><input type="text" name="hTransferBranch_<?=$i?>" size="20"  value="<?=$objInsureItem->get_transfer_branch()?>"></td>
			<td align="center"><input type="text" name="hTransferNumber_<?=$i?>" size="15"  value="<?=$objInsureItem->get_transfer_number()?>"></td>
			<td><input type="text" style="text-align=right;"  onblur="tryNumberFormat(this);"   name="hTransferPrice_<?=$i?>" size="15"  value="<?=number_format($objInsureItem->get_transfer_price(),2)?>"></td>
			<td>
			<?$arrTDate = explode("-",$objInsureItem->get_transfer_date())?>
		  	<table cellspacing="0" cellpadding="0">
			<tr>
				<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value="<?=$arrTDate[2]?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value="<?=$arrTDate[1]?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value="<?=$arrTDate[0]?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>			
			</tr>
			</table>			
			</td>
			<td><input type="text" name="hTransferRemark_<?=$i?>" size="30"  value="<?=$objInsureItem->get_transfer_remark()?>"></td>
		</tr>
		</table>

<?}?>

<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š����¡��</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">�ӹǹ�Թ����ͧ����</td>
	<td align="center" class="listTitle">�ӹǹ�Թ������</td>
	<td align="center" class="listTitle">����Թ</td>
	<td align="center" class="listTitle">��ҧ����</td>
</tr>
<?
$objITList = new InsureItemDetailList();
$objITList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= $hInsureId ");
$objITList->setPageSize(0);
$objITList->setSort(" order_price_id ASC ");
$objITList->load();
$i=1;
forEach($objITList->getItemList() as $objItem) {
?>

<tr id="bar_<?=$i?>" >
	<td align="center" class="ListDetail"><input type="hidden" size="2" name="hOrderPriceId_<?=$i?>" value="<?=$objItem->getOrderPriceId()?>"><?=$i?></td>
	<td align="center" class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$i,$objItem->getPaymentSubjectId(),"��س����͡");?><input type="text" size="20" name="hRemark_<?=$i?>" value="<?=$objItem->getRemark()?>"></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$i?>)"  size="5" id="hQty_<?=$i?>" name="hQty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$i?>)"  id="hPayin_<?=$i?>" name="hPayin_<?=$i?>" value="1"  <?if($objItem->getPayin() == 1) echo "checked";?> ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" readonly size="10" onblur="sum_var(<?=$i?>);" id="hPrice_<?=$i?>"  name="hPrice_<?=$i?>" value="<?=number_format($objItem->getPrice(),2)?>"></td>	
	<?
	if($strMode =="Add"){
		$objItem->setPriceAcc($objItem->getPrice());
	}
	
	?>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  size="10"  id="hPriceAcc_<?=$i?>"  name="hPriceAcc_<?=$i?>" value="<?=number_format($objItem->getPriceAcc(),2)?>"></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$i?>" name="hPriceSum_<?=$i?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceRemain_<?=$i?>" name="hPriceRemain_<?=$i?>" value=""></td>
</tr>	
<?$sum_acc = $sum_acc+$objItem->getPriceAcc()?>
<?$i++;}//end for each?>
<?
$m=$i;
for($j=$m;$j<=5;$j++){?>
<tr id="bar_<?=$j?>" >
	<td align="center" class="ListDetail"><input type="hidden" size="2" name="hOrderPriceId_<?=$j?>" value=""><?=$j?></td>
	<td align="center" class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$j,"","��س����͡");?><input type="text" size="20" name="hRemark_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$j?>)"  size="5" id="hQty_<?=$j?>" name="hQty_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$j?>)"  id="hPayin_<?=$j?>" name="hPayin_<?=$j?>" value="1"  ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  size="10" onblur="sum_var(<?=$i?>);" id="hPrice_<?=$j?>"  name="hPrice_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$j?>);tryNumberFormat(this);"  size="10"  id="hPriceAcc_<?=$j?>"  name="hPriceAcc_<?=$j?>" value=""></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$j?>" name="hPriceSum_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceRemain_<?=$j?>" name="hPriceRemain_<?=$j?>" value=""></td>
</tr>	
<?}//end for 5?>		
<?php 
	if($objInsureItem->get_payment_order() == 1){
		if($objITList->getCount() == 0 && $objQuoAddOn->getCount() > 0){
			forEach($objQuoAddOn->getItemList() as $objAddOn) {
				//get Quotation Add No Match
				$objQuoAddOnMatch = new QuotationAddonMatchList();
				$objQuoAddOnMatch->setFilter(" quotation_addon_id = '".$objAddOn->get_tmquotation_addon_id()."'");
				$objQuoAddOnMatch->setPageSize(0);
				$objQuoAddOnMatch->setSortDefault(" payment_subject_id DESC ");
				$objQuoAddOnMatch->load();
				forEach($objQuoAddOnMatch->getItemList() as $objAddOnItem) {
					$checked = '';
					$get_value = $objAddOn->get_value();
					if($objAddOnItem->get_quotation_addon_id() == 4){
						if($objAddOnItem->get_payment_subject_id() == 88){
							$checked = 'checked';
						}
						$get_value = 1000;
					}
?>
	<tr id="bar_<?=$j?>" >
		<td align="center" class="ListDetail"><input type="hidden" size="2" name="hOrderPriceId_<?=$j?>" value=""><?=$j?></td>
		<td align="center" class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$j,$objAddOnItem->get_payment_subject_id(),"��س����͡");?><input type="text" size="20" name="hRemark_<?=$j?>" value=""></td>
		<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$j?>)"  size="5" id="hQty_<?=$j?>" name="hQty_<?=$j?>" value="1"></td>
		<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$j?>)"  id="hPayin_<?=$j?>" name="hPayin_<?=$j?>" value="1"  <?=$checked?>></td>
		<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  size="10"  id="hPrice_<?=$j?>" onblur="sum_var(<?=$i?>);" name="hPrice_<?=$j?>" value="<?=$get_value?>"></td>
		<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$j?>);tryNumberFormat(this);"  size="10"  id="hPriceAcc_<?=$j?>"  name="hPriceAcc_<?=$j?>" value="<?=$get_value?>"></td>	
		<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$j?>" name="hPriceSum_<?=$j?>" value=""></td>
		<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceRemain_<?=$j?>" name="hPriceRemain_<?=$j?>" value=""></td>
	</tr>	
<?php
					$j++;
				}
			}
		} 
	}
	$pnum = $j-1;
?>
<input type="hidden" size="20" name="hPnum" value="<?=$pnum?>">

<tr>
	<td align="center" class="listTitle" colspan="4"><strong>�����˵�</strong></td>		
	<td align="center" class="listTitle" ><strong>�ʹ�������ͧ����</strong></td>	
	<td align="center" class="listTitle" ></td>	
	<td align="center" class="listTitle" ><strong>����ط��</strong></td>	
	<td align="center" class="listTitle" ><strong>�����ҧ����</strong></td>	
</tr>
<tr>
	<td class="listDetail" colspan="4"><input type="text" id="hInsureRemark" name="hInsureRemark"  size="50" value=""></td>
	<td align="center" class="listDetail" ><input readonly type="text" style="text-align=right;"  id="hTotalPrice" name="hTotalPrice"  size="10" value="<?=number_format($objInsureItem->get_payment_price(),2)?>"></td>	
	<td align="center" class="listDetail" ></td>	
	<td align="center" class="listDetail" ><input readonly  type="text" style="text-align=right;"  id="hTotalPriceAcc" name="hTotalPriceAcc"  size="10" value=""></td>	
	<td align="center" class="listDetail" ><input readonly  type="text" style="text-align=right;"  id="hTotalRemain" name="hTotalRemain"  size="10" value=""></td>	
</tr>
</table>


	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>

	<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail02" width="5%" align="center">�ӴѺ</td>
	<!-- <td class="listDetail02" width="5%" align="center">������</td> -->
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>	
	<td align="center" width="10%" class="listDetail02">�Ըա�ê���</td>
	<td align="center" width="10%" class="listDetail02">�����˵�</td>
	<td align="center" width="15%" class="listDetail02">��ǹŴ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>	
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>	
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
</tr>
<?
if($strMode=="Update"){
	$objOrderPaymentList = new OrderPaymentList();
	$objOrderPaymentList->setFilter(" status = 4 AND order_extra_id = $hInsureItemId  AND order_id = $hInsureId ");
	$objOrderPaymentList->load();
	$i=0;
	forEach($objOrderPaymentList->getItemList() as $objItem) {
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<!-- <td align="center">
		<select name="hNoHead_<?php //echo $i; ?>">
			<option value="0" <?php //if($objItem->getNoHead() == 0) echo "selected"?>>��س����͡
			<option value="1" <?php //if($objItem->getNoHead() == 1) echo "selected"?>>������»�Сѹ���
			<option value="2" <?php //if($objItem->getNoHead() == 2) echo "selected"?>>�������+��Һ�ԡ��
			<option value="5" <?php //if($objItem->getNoHead() == 5) echo "selected"?>>������»�Сѹ����������			
			
		</select>
	</td> -->
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_".$i,$objItem->getPaymentTypeId(),"��س����͡��¡��");?></td>
	<td align="center"><?$objPaymentRemarkList->printSelect("hPaymentRemarkId_".$i,$objItem->getPaymentRemarkId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentRemark_<?=$i?>" size="45"  value="<?=$objItem->getPaymentRemark();?>"></td>
	<td align="center"><?$objPaymentDiscountList->printSelect("hPaymentDiscountId_".$i,$objItem->getPaymentDiscountId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" onblur="tryNumberFormat(this);" id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" size="15"  value="<?=$objItem->getPrice();?>"></td>	
	<td align="center"><?$objBankList->printSelect("hBankId_".$i,$objItem->getBankId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value="<?=$objItem->getBranch();?>"></td>	
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value="<?=$objItem->getCheckNo();?>"></td>	
	<td align="center">
		<?
		$arrDate = $objItem->getCheckDate();
		$Day01="";
		$Month01="";
		$Year01="";
		if($arrDate != "0000-00-00" or $arrDate != ""){
			$arrDate = explode("-",$arrDate);
			$Day01=$arrDate[2];
			$Month01=$arrDate[1];
			$Year01=$arrDate[0];
		}
		?>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?$i++;}?>
<?$j =  4 - $objOrderPaymentList->mCount;?>
<?$k=$i;
for($i=$k;$i<4;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<!-- <td align="center">
		<select name="hNoHead_<?php //echo $i; ?>">
			<option value="0" >��س����͡
			<option value="1" >������»�Сѹ���
			<option value="2" >�������+��Һ�ԡ��
			<option value="5" >������»�Сѹ����������
		</select>
	</td>	 -->
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><?$objPaymentRemarkList->printSelect("hPaymentRemarkId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentRemark_<?=$i?>" size="45"  value=""></td>
	<td align="center"><?$objPaymentDiscountList->printSelect("hPaymentDiscountId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" onblur="tryNumberFormat(this);" id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>	
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}else{?>

<?$i=0;
for($i=0;$i<4;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<!-- <td align="center">
		<select name="hNoHead_<?php //echo $i; ?>">
			<option value="0" >��س����͡
			<option value="1" >������»�Сѹ���
			<option value="2" >�������+��Һ�ԡ��
			<option value="5" >������»�Сѹ����������
		</select>
	</td>	 -->
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><?$objPaymentRemarkList->printSelect("hPaymentRemarkId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentRemark_<?=$i?>" size="45"  value=""></td>
	<td align="center"><?$objPaymentDiscountList->printSelect("hPaymentDiscountId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_".$i,"","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>	
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>	
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}?>
<br>
	<?php 
		if($objInsureReceiptList->getCount()){
	?>
	<table width="100%" cellpadding="3" cellspacing="0">
		<tr>
			<td class="i_background_pink">�������Թʴ�Ѻ᷹</td>
		</tr>
	</table>
	<table class="search" cellpadding="1" cellspacing="1" width="100%" border="0">
		
		<tr>
			<td  align="center" class="listTitle"></td>
			<td  align="center" class="listTitle"><strong>�Ţ��������</strong></td>
			<td  align="center" class="listTitle"><strong>�ѹ������</strong></td>
			<td  align="center" class="listTitle"><strong>����Ѻ����</strong></td>
			<td  align="center" class="listTitle"><strong>�ʹ�Թ</strong></td>
		</tr>
		
		<?php 
			forEach($objInsureReceiptList->getItemList() as $objRecItem) {
				$objCreatedBy = new member();
				$objCreatedBy->setMemberId($objRecItem->get_created_by());
				$objCreatedBy->load();
				$CreatedByName=$objCreatedBy->getFirstname()." ".$objCreatedBy->getLastname();
		?>
		<tr>
			<td  align="center" class="ListDetail"><input class="hPaymentOther" type="checkbox" name="hPaymentOther[]" value="<?=$objRecItem->get_id();?>"></td>
			<td  align="center" class="ListDetail"><strong><?=$objRecItem->get_receipt_no();?></strong></td>
			<td  align="center" class="ListDetail"><strong><?=date("d-m-Y H:i", strtotime($objRecItem->get_receipt_date()));?></strong></td>
			<td  align="center" class="ListDetail"><strong><?=$CreatedByName;?></strong></td>
			<td  align="right" class="ListDetail"><input type="hidden" id="hPaymentOther_<?=$objRecItem->get_id();?>" name="hPaymentOtherAmt[<?=$objRecItem->get_id();?>]" value="<?=$objRecItem->get_receipt_amt();?>"><strong><?= number_format($objRecItem->get_receipt_amt(),2);?></strong></td>
		</tr>

		<?php 
			}
		?>
		
	</table>
	<br>
	<?php 
		}
		if($objInsureReceipPaytList->getCount()){
	?>
	<table width="100%" cellpadding="3" cellspacing="0">
		<tr>
			<td class="i_background_pink">�ʹ�����Թʴ�Ѻ᷹</td>
		</tr>
	</table>
	<table class="search" cellpadding="1" cellspacing="1" width="100%" border="0">
		
		<tr>
			<td  align="center" class="listTitle"><strong>�Ţ��������</strong></td>
			<td  align="center" class="listTitle"><strong>�ѹ������</strong></td>
			<td  align="center" class="listTitle"><strong>����Ѻ����</strong></td>
			<td  align="center" class="listTitle"><strong>�ʹ�Թ</strong></td>
		</tr>
		
		<?php 
			forEach($objInsureReceipPaytList->getItemList() as $objRecPayItem) {
				$objPayCreatedBy = new member();
				$objPayCreatedBy->setMemberId($objRecPayItem->get_created_by());
				$objPayCreatedBy->load();
				$CreatedByName=$objPayCreatedBy->getFirstname()." ".$objPayCreatedBy->getLastname();
		?>

		<tr>
			<td  align="center" class="ListDetail"><strong><?=$objRecPayItem->get_receipt_no();?></strong></td>
			<td  align="center" class="ListDetail"><strong><?=date("d-m-Y H:i", strtotime($objRecPayItem->get_receipt_date()));?></strong></td>
			<td  align="center" class="ListDetail"><strong><?=$CreatedByName;?></strong></td>
			<td  align="right" class="ListDetail"><strong><?= number_format($objRecPayItem->get_receipt_amt(),2);?></strong></td>
		</tr>

		<?php 
			}
		?>
		
	</table>
	<br>
	<?php 
		}
	?>
<form>
<?
$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objBookBankList = new BookBankList();
$objBookBankList->setPageSize(0);
$objBookBankList->setSortDefault(" title ASC ");
$objBookBankList->load();
?>
		<table class="search" cellpadding="1" cellspacing="1" width="100%" border=0>
		<tr>
			<td colspan="8" align="center" class="listTitle"><strong>��¡�úѹ�֡��ê��л�Сѹ���</strong></td>
			<td colspan="4" align="center" class="listDetail02"><strong>��¡�õ�Ǩ�ͺ����͹�ҡ�ѭ��</strong></td>
		</tr>
		<tr>
			<td class="listTitle" align="center">��������ê���</td>
			<td class="listTitle" align="center">�ѭ�� Buzz</td>
			<td class="listTitle" align="center">�͹�ҡ��Ҥ��</td>
			<td class="listTitle" align="center">�͹�ҡ�Ң�</td>
			<td class="listTitle" align="center">�����Ţ��<br>/��Ի ATM</td>
			<td class="listTitle" align="center">�ӹǹ�Թ</td>
			<td class="listTitle" align="center">�ѹ���</td>
			<td class="listTitle" align="center">�����˵�</td>
			<td class="listDetail02" align="center">͹��ѵԨҡ�ѭ��</td>
			<td class="listDetail02" align="center">�ѹ���͹��ѵ�</td>
			<td class="listDetail02" align="center">�����˵�</td>
			<td class="listDetail02" align="center">���ѹ�֡</td>
			
			
		</tr>
		<?for($x=1;$x<=5;$x++){
			$objInsurePayment =  new InsurePayment();
			$objInsurePayment->loadByCondition("  order_id= ".$hInsureItemId."  and order_extra_id = $hInsureId and payment_no= $x   ");
			if($objInsurePayment->getPrice() > 0){
		?>
		<input type="hidden" name="hCheckPaymentTypeId_<?=$x?>" value="<?=$objInsurePayment->getPaymentTypeId()?>">
		<input type="hidden" name="hCheckPaymentBookBankId_<?=$x?>" value="<?=$objInsurePayment->getBookBankId()?>">
		<input type="hidden" name="hCheckPaymentBankId_<?=$x?>" value="<?=$objInsurePayment->getBankId()?>">
		<input type="hidden" name="hCheckBranch_<?=$x?>" value="<?=$objInsurePayment->getBranch()?>">
		<input type="hidden" name="hCheckCheckNo_<?=$x?>" value="<?=$objInsurePayment->getCheckNo()?>">
		<input type="hidden" name="hCheckPrice_<?=$x?>" value="<?=$objInsurePayment->getPrice()?>">
		<input type="hidden" name="hCheckCheckDate_<?=$x?>" value="<?=$objInsurePayment->getCheckDate()?>">
		<input type="hidden" name="hCheckInsurePaymentId_<?=$x?>" value="<?=$objInsurePayment->getInsurePaymentId()?>">
		<tr>
			<td ><?$objPaymentTypeList->printSelect("hCheckPaymentType_".$x,$objInsurePayment->getPaymentTypeId(),"���͡��������ê���");?></td>
			<td ><?$objBookBankList->printSelect("hCheckPaymentBookBank_".$x,$objInsurePayment->getBookBankId(),"���͡�ѭ�� Buzz");?></td>
			<td ><?$objBankList->printSelect("hCheckPaymentBank_".$x,$objInsurePayment->getBankId(),"���͡��¡�ø�Ҥ��");?></td>
			<td align="center"><?=$objInsurePayment->getBranch()?></td>
			<td align="center"><?=$objInsurePayment->getCheckNo()?></td>
			<td><?=number_format($objInsurePayment->getPrice(),2)?></td>
			<td>
			<?=formatShortDate($objInsurePayment->getCheckDate())?>

			</td>
			<td><?=$objInsurePayment->getRemark()?></td>
			<td><?=$objInsurePayment->getAccCheck()?></td>
			<td><?=formatShortDate($objInsurePayment->getAccCheckDate())?></td>
			<td><?=$objInsurePayment->getAccCheckRemark()?></td>
			<td>
			<?$objS = new Member();
				$objS->setMemberId($objInsurePayment->getAccCheckBy());
				$objS->load();
				echo $objS->getFirstname()?></td>
			<td><input type="button" value="check" onclick="check(<?=$x?>)"></td>	
		</tr>
		<?}?>
		<?}?>
		</table>





	</td>
</tr>
</table>
<br>



<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button"  id="hSubmit1" name="hSubmit1" value="�ѹ�֡��¡��" class="button"  onclick="return check_submit();" >
<?}else{?>
	<input type="button"  id="hSubmit1" name="hSubmit1" value="�ѹ�֡��¡��" class="button"  onclick="return check_submit();" >
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='icardNewList.php'">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	window.addEventListener( "pageshow", function ( event ) {
		console.log(event);
	var historyTraversal = event.persisted || ( typeof window.performance != "undefined" && window.performance.navigation.type === 2 );

	console.log(historyTraversal);
	if ( historyTraversal ) {
		// Handle page restore.
		window.location.reload();
	}
	});

	function check(val){
		
		
		
		
		
		
	}



	function sum_var(val){
		var pnum = '<?php echo $pnum; ?>';
		console.log('pnum | '+pnum);
		num01         = 0;
		num02         = 0;
		num03         = 0;
		num04         = 0;
		num05         = false;
		num06         = 0;
		remain_total  = 0;
		sumTotalPrice = 0;
		
		for(i=1;i<=pnum;i++){ //pnum = 5 old code
 			
			
			num01 =	document.getElementById("hQty_"+i).value;
			num02 =	rip_comma(document.getElementById("hPriceAcc_"+i).value);
			num06 =	rip_comma(document.getElementById("hPrice_"+i).value);
			num05 = document.getElementById("hPayin_"+i).checked;

			if(num05 == true){
				num03=(num01*num02)*-1;
				num07=(num01*num06)*-1;
				if(num06 > 0){
					num06=num06*-1;
				}
			}else{
				num03=(num01*num02);	
				num07=(num01*num06);
				if(num06 < 0){
					num06=num06*-1;
				}
			}
			remain = 0;
			remain = num07-num03;
			document.getElementById("hPriceSum_"+i).value= formatCurrency(num03);
			document.getElementById("hPriceRemain_"+i).value= formatCurrency(remain);
			//document.getElementById("hPrice_"+i).value= formatCurrency(num06);
			
			
			num04 = num04+num03;
			remain_total = remain_total+remain;
			sumTotalPrice += num06;
			 			
		}
		//console.log('sumTotalPrice | '+sumTotalPrice);
		document.getElementById("hTotalPrice").value= formatCurrency(sumTotalPrice); // ��� �ӹǹ�Թ����ͧ����
		document.getElementById("hTotalPriceAcc").value= formatCurrency(num04);	
		document.getElementById("hTotalRemain").value= formatCurrency(remain_total);	
		
	}
	
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
	sum_var(1);
	</script>
	
	

<?
	include("h_footer.php")
?>
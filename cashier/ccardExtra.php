<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 0 AND nohead=0  ");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" rank, title ASC ");
$objPaymentSubjectList->load();

$objPaymentSubjectList01 = new PaymentSubjectList();
$objPaymentSubjectList01->setFilter(" type_id = 0 AND nohead=1 ");
$objPaymentSubjectList01->setPageSize(0);
$objPaymentSubjectList01->setSortDefault(" rank, title ASC ");
$objPaymentSubjectList01->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault(" title ASC");
$objFundCompanyList->load();

$objOrder = new Order();
$objOrderExtra = new OrderExtra();

if($hOrderId != ""){

		$objOrder->setOrderId($hOrderId);
		$objOrder->load();
		
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();
		
		$arrDate = explode(" ",$objOrder->getRecieveDate());
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Hour2 = $arrTime[0];
		$Minute2 = $arrTime[1];
		
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();
		
		$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
		$objCustomer->load();
		
		//load sale name
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrderBooking->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		

		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();

}



if (empty($hSubmit)) {
	if ($hId!="") {

		$strMode="Update";
		$objOrderExtra->setOrderExtraId($hId);
		$objOrderExtra->load();

		
		$arrDate = explode(" ",$objOrderExtra->getExtraDate());
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day1 = $arrDay[2];
		$Month1 = $arrDay[1];
		$Year1 = $arrDay[0];
		
		$Hours1 = $arrTime[0];
		$Minute1 = $arrTime[1];
		

		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" status = 1 AND order_extra_id = $hId");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_extra_id = $hId  AND status = 1");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" order_price_id ASC ");
		$objOrderPriceList->load();
		
		
	} else {
		$strMode="Add";
		
		$arrDate = explode(" ",date("Y-m-d H:i:s"));
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day1 = $arrDay[2];
		$Month1 = $arrDay[1];
		$Year1 = $arrDay[0];
		
		$Hours1 = $arrTime[0];
		$Minute1 = $arrTime[1];
		

		
	}

} else {
	if (!empty($hSubmit)) {

            $objOrderExtra->setOrderExtraId($hId);
			$objOrderExtra->setOrderId($hOrderId);
			$hExtraDate = $Year1."-".$Month1."-".$Day1." ".$Hours1.":".$Minute1.":00";
			$objOrderExtra->setExtraDate($hExtraDate);
			$objOrderExtra->setExtraType(2);
			$objOrderExtra->setBookingNumber($hBookingNumber);
			$objOrderExtra->setSendingNumber($hSendingNumber);
			$objOrderExtra->setSendingNumberNohead($hSendingNumberNohead);
			$objOrderExtra->setAddBy($sMemberId);
			$objOrderExtra->setEditBy($sMemberId);
			
			$pasrErrOrder = $objOrderExtra->check($hSubmit);
			
			If (  Count($pasrErrOrder) == 0){

				//check value is zero
				for($i=0;$i<$hOrderPriceCountHead;$i++){
					$value1= $_POST["hPriceHead_".$i]*1;
					if($value1 != 0){
						if($value1 < 0) $value1= $value1*-1;
						$total_value1= $value1+$total_value1;
					}
				}
				
				for($i=0;$i<$hOrderPriceCount;$i++){
					$value= $_POST["hPrice_".$i]*1;
					if($value != 0){
						if($value < 0) $value= $value*-1;
						$total_value= $value+$total_value;
					}
				}
			
				if ($strMode=="Update") {
						//create booking number
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();
					
					if($hSendingNumber == "" ){
						if( $total_value1 != 0 ){
							$strSendingNumber = $objInfo->getOrderNumber();
							$objOrderExtra->setSendingNumber($strSendingNumber);
							$objInfo->updateOrderNumberRecent();
						}
						
					}
					
					if($hSendingNumberNohead == "" ){
						if( $total_value  !=0 ){
							$strSendingNumberNohead = $objInfo->getOrderNumberNohead();
							$objOrderExtra->setSendingNumberNohead($strSendingNumberNohead);
							$objInfo->updateOrderNumberNoheadRecent();

						}
					}
					$objOrderExtra->update();
				}else{
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();
					if( $total_value1 !=0 ){
						$strSendingNumber = $objInfo->getOrderNumber();
						$objOrderExtra->setSendingNumber($strSendingNumber);
						$objInfo->updateOrderNumberRecent();
					}
					if( $total_value != 0 ){
						$strSendingNumberNohead = $objInfo->getOrderNumberNohead();
						$objOrderExtra->setSendingNumberNohead($strSendingNumberNohead);
						$objInfo->updateOrderNumberNoheadRecent();
					}
					$hId= $objOrderExtra->add();
				
				}//end check strMode
				
				//update order price
				for($i=0;$i<$hOrderPriceCountHead;$i++){
					$objOrderPrice = new OrderPrice();
					$objOrderPrice->setOrderId($hOrderId);
					$objOrderPrice->setOrderExtraId($hId);
					$objOrderPrice->setOrderPriceId($hOrderPriceIdHead[$i]);
					$objOrderPrice->setPaymentSubjectId($hPaymentSubjectIdHead[$i]);
					$objOrderPrice->setRemark($hRemarkHead[$i]);
					$objOrderPrice->setQty($_POST["hQtyHead_".$i]);
					$objOrderPrice->setPrice($_POST["hPriceHead_".$i]);
					$objOrderPrice->setVat(1);
					$objOrderPrice->setPayin($_POST["hPayinHead_".$i]);
					$objOrderPrice->setOrderPaymentId($hOrderPaymentIdHead[$i]);
					$objOrderPrice->setStatus(1);
					if($hOrderPriceIdHead[$i] != ""){
							if($_POST["hPriceHead_".$i] > 0){
								$objOrderPrice->update();
							}else{
								if($hOrderPriceIdHead[$i] > 0){
									$objOrderPrice->delete();
								}
							}
						}else{
							if($_POST["hPriceHead_".$i] > 0){
								$objOrderPrice->add();
							}
					}
				}
				
				
				//update order payment
				for($i=0;$i<3;$i++){
					$objOrderPayment = new OrderPayment();

						$objOrderPayment->setOrderId($hOrderId);
						$objOrderPayment->setOrderExtraId($hId);
						$objOrderPayment->setOrderPaymentId($hOrderPaymentIdHead[$i]);
						$objOrderPayment->setPaymentTypeId($_POST["hPaymentTypeIdHead_".$i]);
						$objOrderPayment->setPrice($_POST["hPaymentPriceHead_".$i]);
						$objOrderPayment->setBankId($_POST["hBankIdHead_".$i]);
						$objOrderPayment->setBranch($_POST["hBankBranchHead_".$i]);
						$objOrderPayment->setCheckNo($_POST["hCheckNoHead_".$i]);
						$strCheckDate =  $_POST["Year01Head_".$i]."-".$_POST["Month01Head_".$i]."-".$_POST["Day01Head_".$i];
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(1);
						if($hOrderPaymentIdHead[$i] != ""){
							if($_POST["hPaymentPriceHead_".$i] > 0){
								$objOrderPayment->update();
							}else{
								if($hOrderPaymentIdHead[$i] > 0){
									$objOrderPayment->delete();
								}
							}
						}else{
							if($_POST["hPaymentPriceHead_".$i] > 0){
								$objOrderPayment->add();
							}
						}

				}
				
				
				
				//update order price
				for($i=0;$i<$hOrderPriceCount;$i++){
					$objOrderPrice = new OrderPrice();
					$objOrderPrice->setOrderId($hOrderId);
					$objOrderPrice->setOrderExtraId($hId);
					$objOrderPrice->setOrderPriceId($hOrderPriceId[$i]);
					$objOrderPrice->setPaymentSubjectId($hPaymentSubjectId[$i]);
					$objOrderPrice->setRemark($hRemark[$i]);
					$objOrderPrice->setQty($_POST["hQty_".$i]);
					$objOrderPrice->setPrice($_POST["hPrice_".$i]);
					$objOrderPrice->setVat($_POST["hVat_".$i]);
					$objOrderPrice->setPayin($_POST["hPayin_".$i]);
					$objOrderPrice->setOrderPaymentId($hOrderPaymentId[$i]);
					$objOrderPrice->setStatus(2);
					if($hOrderPriceId[$i] != ""){
							if($_POST["hPrice_".$i] > 0){
								$objOrderPrice->update();
							}else{
								if($hOrderPriceId[$i] > 0){
									$objOrderPrice->delete();
								}
							}
						}else{
							if($_POST["hPrice_".$i] > 0){
								$objOrderPrice->add();
							}
					}
				}
				
				
				//update order payment
				for($i=0;$i<3;$i++){
					$objOrderPayment = new OrderPayment();

						$objOrderPayment->setOrderId($hOrderId);
						$objOrderPayment->setOrderExtraId($hId);
						$objOrderPayment->setOrderPaymentId($hOrderPaymentId[$i]);
						$objOrderPayment->setPaymentTypeId($_POST["hPaymentTypeId_".$i]);
						$objOrderPayment->setPrice($_POST["hPaymentPrice_".$i]);
						$objOrderPayment->setBankId($_POST["hBankId_".$i]);
						$objOrderPayment->setBranch($_POST["hBankBranch_".$i]);
						$objOrderPayment->setCheckNo($_POST["hCheckNo_".$i]);
						$strCheckDate =  $_POST["Year01_".$i]."-".$_POST["Month01_".$i]."-".$_POST["Day01_".$i];
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(2);
						if($hOrderPaymentId[$i] != ""){
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->update();
							}else{
								if($hOrderPaymentId[$i] > 0){
									$objOrderPayment->delete();
								}
							}
						}else{
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->add();
							}
						}

					 
				}

				unset ($objOrder);
				header("location:ccardExtraPrintPreview.php?hId=$pId$hId");
				exit;

			
			}else{
				$arrDate = explode("-",$objOrder->getRecieveDate());
				$Day = $arrDate[2];
				$Month = $arrDate[1];
				$Year = $arrDate[0];
				
				$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
				$objBookingCustomer->load();
				
				$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objCustomer->load();
				
				$objOrderPaymentList = new OrderPaymentList();
				$objOrderPaymentList->setFilter(" order_id = $hId  AND status = 1 ");
				$objOrderPaymentList->setPageSize(0);
				$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
				$objOrderPaymentList->load();
		
				$objOrderPriceList = new OrderPriceList();
				$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 1 ");
				$objOrderPriceList->setPageSize(0);
				$objOrderPriceList->setSortDefault(" OP.order_price_id ASC ");
				$objOrderPriceList->load();
				
				//load sale name
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getSendingSaleId());
				$objSale->load();
				
				$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
				
				$objSale = new Member();
				$objSale->setMemberId($objOrderBooking->getBookingSaleId());
				$objSale->load();
				
				$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
				// load stock car and red code
				
				$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
				$objBookingCarColor->load();
				
				$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
				$objBookingCarSeries->load();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
		}//end count error
	}//end 	if (!empty($hSubmit)) {
}//if (empty($hSubmit)) {
$pageTitle = "1. ����������ѹ";
$strHead03="�ѹ�֡��¡�����ͺ";
if($strMode == "Add"){ $pageContent = "1.3 �ѹ�֡��¡�����ͺ ";}else{$pageContent = "1.3 �ѹ�֡��¡�����ͺ";}
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
		
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}				
		
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hSendingSaleId.value=="")
		{
			alert("��س����͡��ѡ�ҹ��¨ҡ�к�");
			document.forms.frm01.hSendingSaleId.focus();
			return false;
		} 			

		if( document.frm01.hNoRedCode.checked == false ){
		
			if (document.forms.frm01.hStockRedId.value=="" || document.forms.frm01.hStockRedId.value== "0")
			{
				alert("��س����͡����ᴧ�ҡ�к�");
				document.forms.frm01.hStockRedName.focus();
				return false;
			} 					
		}
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س����͡�ӺŨҡ�Ӻ�");
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س����͡����ͨҡ�к�");
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س����͡�ѧ��Ѵ�ҡ�к�");
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س����͡������ɳ���ҡ�к�");
			document.forms.frm01.hZip.focus();
			return false;
		} 						

		if (document.forms.frm01.hStockCarId.value=="" || document.forms.frm01.hStockCarId.value== 0)
		{
			alert("��س����͡�����Ţ�ѧ�ҡ�к�");
			document.forms.frm01.hCarNumber.focus();
			return false;
		} 			
		
		document.getElementById("hSubmit1").style.display = "none";
		document.frm01.submit();
		return true;

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				

<form name="frm01" action="ccardExtra.php" method="POST" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hOrderId?>">
	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">	  
	  <input type="hidden" name="hSendingNumber" value="<?=$objOrderExtra->getSendingNumber()?>">
	  <input type="hidden" name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>">	  
	  <input type="hidden" name="hSendingNumberNohead" value="<?=$objOrderExtra->getSendingNumberNohead()?>">	  
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingSaleId" value="<?=$objOrder->getBookingSaleId()?>">
	  <input type="hidden" name="hBookingCarColor" value="<?=$objOrder->getBookingCarColor()?>">
	  <input type="hidden" name="hBookingCarSeries" value="<?=$objOrder->getBookingCarSeries()?>">
	  <input type="hidden" name="hTrick" value="true">
	  

<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="10%" valign="top"></td>
			<td width="40%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">


					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day1 value="<?=$Day1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month1 value="<?=$Month1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year1 value="<?=$Year1?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year1,Day1, Month1, Year1,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hours1 value="<?=$Hours1?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute1 value="<?=$Minute1?>"></td>		
						</tr>
						</table>
	
				
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>�ѹ����Ѻ�ͺö</strong></td>
			<td width="40%" valign="top" >
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hour2 value="<?=$Hour2?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute2 value="<?=$Minute2?>"></td>			
						</tr>
						</table>

				
			</td>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong></td>
			<td width="40%" valign="top">
						<input type="hidden" readonly size="2" name="hSendingSaleId"  value="<?=$objOrder->getSendingSaleId();?>">
						<input type="text" name="hSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm01.hSendingSaleId.value != '' ) frm01.hBuyType[0].focus();if(event.keyCode !=13 ) frm01.hSendingSaleId.value='';"   value="<?=$hSaleName?>">
						&nbsp;&nbsp;<input type="checkbox" onclick="changeSale()"  value="1" name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>>����¹��ѡ�ҹ���
			</td>			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>��������ë���</strong></td>
			<td valign="top" ><input type="radio" onclick="showFn(1);" name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 OR $objOrder->getBuyType() == 0) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" name="hBuyType" onclick="showFn(2);" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��</td>
			<td class="i_background03" valign="top"><label id="fn" style="<?if($objOrder->getBuyType()  !=2) echo "display:none";?>"><strong>����ѷ�ṹ��</strong></label></td>
			<td valign="top">
				<label id="fnValue" style="<?if($objOrder->getBuyType() !=2) echo "display:none";?>"><?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?></label>
			</td>			
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>����¹����ᴧ</strong></td>
			<td valign="top">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
					        <input type="hidden"  readonly name="hStockRedIdTemp" size="2"  value="<?=$objOrder->getStockRedId();?>">
							<input type="hidden"  readonly name="hStockRedId" size="2"  value="<?=$objOrder->getStockRedId();?>">	
							<input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>
				
			</td>	
			<td class="i_background03" valign="top"><strong>�ҤҢ��</strong></td>
			<td valign="top">
				<input type="text"  name="hOrderPrice" size="20"  value="<?=$objOrder->getOrderPrice()?>"> �ҷ
			</td>		
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=$objBookingCustomer->getTitle();?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<?=$name?>
					</td>
					<td valign="top">					
					<?$objBookingCustomer->getCustomerTitleId();?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><?=$objBookingCustomer->getAddress();?></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			
			<td  width="40%" valign="top"><?=$objBookingCustomer->getTumbon();?></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><?=$objBookingCustomer->getAmphur();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCustomer" onclick="setCustomerValue()" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹����Ѻ�ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<INPUT  name="hCustomerName" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerId.value != '' ) frm01.hCustomerTitleId.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerId.value='';"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><input type="text" name="hAddress" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><input type="hidden" readonly size="6" name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" name="hTumbon" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';"  size="30"  value="<?=$objCustomer->getTumbon();?>"></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><input type="hidden" readonly size="4" name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text" name="hAmphur" onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';"  size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="2" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text" name="hProvince" onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';"  size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="5" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hCarNumber.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';"  size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="10%"><strong>���ö</strong> </td>
			<td valign="top" width="40%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="10%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="40%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>


<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="10%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>
			
			<td width="40%" ><input type="hidden" size="3" readonly name="hStockCarId" value="<?=$objOrder->getStockCarId()?>"><input type="text" name="hCarNumber" size="30" onKeyDown="if(event.keyCode==13 && frm01.hStockCarId.value != '' ) frm01.hEngineNumber.focus();if(event.keyCode !=13 ) frm01.hStockCarId.value='';"  value="<?=$objStockCar->getCarNumber()?>"></td>
			<td width="10%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><input type="text" name="hEngineNumber" size="30"  value="<?=$objStockCar->getEngineNumber()?>"></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<?$objCarRoleList->printSelect("hCarModelId",$objStockCar->getCarModelId(),"��س����͡");?>
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<?$objCarSeriesList->printSelect("hCarSeriesId",$objStockCar->getCarSeriesId(),"��س����͡");?>
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
<br>


<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">��������¡��</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
</tr>
<?
$i=0;
forEach($objPaymentSubjectList->getItemList() as $objItem) {
	$objOrderPrice = new OrderPrice();
	if($strMode=="Update"){
		$objOrderPrice->loadByCondition(" order_extra_id=$hId AND payment_subject_id = ".$objItem->getPaymentSubjectId());
	}
?>
<input type="hidden"  name="hPaymentSubjectIdHead[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
<input type="hidden"  name="hOrderPriceIdHead[<?=$i?>]" value="<?=$objOrderPrice->getOrderPriceId();?>">

<tr>
	<td align="center" class="ListDetail"><?=$i+1?></td>
	<td align="left" class="ListDetail"><?=$objItem->getTitle();?> <?if($objItem->getTitle() == "����"){?><input type="text"  size="30" name="hRemarkHead[<?=$i?>]" value="<?=$objOrderPrice->getRemark();?>"><?}?></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_all_head();" onfocus="sum_all_head();"  size="5" name="hQtyHead_<?=$i?>" value="<?if( $objOrderPrice->getQty() > 0 ){echo $objOrderPrice->getQty();}else{ echo "1";}?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onclick="sum_all_head()"  onblur="sum_all_head();" onfocus="sum_all_head();" name="hPayinHead_<?=$i?>" value="1" <?if($objOrderPrice->getPayin() == 1) echo "checked";?> ></td>
	<?$sumPrice = ($objOrderPrice->getPrice()*$objOrderPrice->getQty())?>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_all_head();" onfocus="sum_all_head();"  size="20" name="hPriceHead_<?=$i?>" value="<?=$objOrderPrice->getPrice();?>"></td>	
	<td align="center" class="ListDetail"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceSumHead_<?=$i?>" value="<?=number_format($sumPrice,2);?>"></td>
</tr>	
<?$i++;}?>
<input type="hidden"  name="hOrderPriceCountHead" value="<?=$i?>">				
<tr>
	<td align="center" class="listDetail" colspan="4" rowspan="2" >
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hSendingRemarkHead" rows="3" cols="70"><?=$objOrder->getSendingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right"  class="listDetail" colspan="1"><strong>����Թ</strong></td>		
	<td align="center"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceTotalHead" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>				
<tr>	
	<td align="right"  class="listDetail" colspan="1"><strong>������Ť������</strong></td>	
	<td align="center"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceVat" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>
<tr>
	<td align="center" class="listDetail" colspan="4">
	</td>
	<td align="right" class="listDetail" colspan="1"><strong>�ʹ�ط��</strong></td>	
	<td align="center"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceTotalHeadAll" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>
</table>


	</td>
</tr>
</table>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail02" width="5%" align="center">�ӴѺ</td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
</tr>
<?
$objOrderPaymentList = new OrderPaymentList();
if($strMode =="Update"){
	$objOrderPaymentList->setFilter(" order_extra_id=$hId AND status=1 ");
	$objOrderPaymentList->load();
}
if($objOrderPaymentList->mCount >0 ){

$i=0;
forEach($objOrderPaymentList->getItemList() as $objItem) {
?>
<input type="hidden"  name="hOrderPaymentIdHead[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeIdHead_$i",$objItem->getPaymentTypeId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPriceHead_<?=$i?>" size="15"  value="<?=$objItem->getPrice();?>"></td>
	<td align="center"><?$objBankList->printSelect("hBankIdHead_$i",$objItem->getBankId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranchHead_<?=$i?>" size="20"  value="<?=$objItem->getBranch();?>"></td>
	<td align="center"><input type="text" name="hCheckNoHead_<?=$i?>" size="10"  value="<?=$objItem->getCheckNo();?>"></td>
	<td align="center">
		<?
		$arrDate = $objItem->getCheckDate();
		$Day01="";
		$Month01="";
		$Year01="";
		if($arrDate != "0000-00-00" or $arrDate != ""){
			$arrDate = explode("-",$arrDate);
			$Day01=$arrDate[2];
			$Month01=$arrDate[1];
			$Year01=$arrDate[0];
		}
		?>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Head_<?=$i?>  value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Head_<?=$i?> value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Head_<?=$i?> value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Head_<?=$i?>,Day01Head_<?=$i?>, Month01Head_<?=$i?>, Year01Head_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?$i++;}?>
<?$k=$i;
for($i=$k;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentIdHead[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeIdHead_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPriceHead_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankIdHead_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranchHead_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNoHead_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Head_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Head_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Head_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Head_<?=$i?>,Day01Head_<?=$i?>, Month01Head_<?=$i?>, Year01Head_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}else{?>

<?$i=0;
for($i=0;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentIdHead[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeIdHead_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPriceHead_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankIdHead_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranchHead_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNoHead_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Head_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Head_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Head_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Head_<?=$i?>,Day01Head_<?=$i?>, Month01Head_<?=$i?>, Year01Head_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}?>




	</td>
</tr>
</table>
<br>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">��������¡�úѭ�չ͡</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
</tr>
<?
$i=0;
forEach($objPaymentSubjectList01->getItemList() as $objItem) {
	$objOrderPrice = new OrderPrice();
	if($strMode=="Update"){
		$objOrderPrice->loadByCondition(" order_extra_id='$hId' AND payment_subject_id = ".$objItem->getPaymentSubjectId());
	}
?>
<input type="hidden"  name="hPaymentSubjectId[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
<input type="hidden"  name="hOrderPriceId[<?=$i?>]" value="<?=$objOrderPrice->getOrderPriceId();?>">

<tr>
	<td align="center" class="ListDetail"><?=$i+1?></td>
	<td align="left" class="ListDetail"><?=$objItem->getTitle();?> <?if($objItem->getTitle() == "����"){?><input type="text"  size="30" name="hRemark[<?=$i?>]" value="<?=$objOrderPrice->getRemark();?>"><?}?></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_all();" onfocus="sum_all();"  size="5" name="hQty_<?=$i?>" value="<?if( $objOrderPrice->getQty() > 0 ){echo $objOrderPrice->getQty();}else{ echo "1";}?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onclick="sum_all()"  onblur="sum_all();" onfocus="sum_all();" name="hPayin_<?=$i?>" value="1" <?if($objOrderPrice->getPayin() == 1) echo "checked";?> ></td>
	<?$sumPrice = ($objOrderPrice->getPrice()*$objOrderPrice->getQty())?>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_all();" onfocus="sum_all();"  size="20" name="hPrice_<?=$i?>" value="<?=$objOrderPrice->getPrice();?>"></td>	
	<td align="right" class="ListDetail"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceSum_<?=$i?>" value="<?=number_format($sumPrice,2);?>"></td>
</tr>	
<?$i++;}?>
<input type="hidden"  name="hOrderPriceCount" value="<?=$i?>">				
<tr>
	<td align="center" class="listDetail" colspan="4">
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hBookingRemark" rows="3" cols="70"><?=$objOrder->getBookingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" ><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceTotal" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>
</table>


	</td>
</tr>
</table>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ��¡�úѭ�չ͡</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail02" width="5%" align="center">�ӴѺ</td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
</tr>
<?	
$objOrderPaymentList = new OrderPaymentList();
if($strMode =="Update"){
	$objOrderPaymentList->setFilter(" order_extra_id='$hId' AND status=2 ");
	$objOrderPaymentList->load();
}
if($objOrderPaymentList->mCount > 0 ){

	$i=0;
	forEach($objOrderPaymentList->getItemList() as $objItem) {
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i",$objItem->getPaymentTypeId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value="<?=$objItem->getPrice();?>"></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i",$objItem->getBankId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value="<?=$objItem->getBranch();?>"></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value="<?=$objItem->getCheckNo();?>"></td>
	<td align="center">
		<?
		$arrDate = $objItem->getCheckDate();
		$Day01="";
		$Month01="";
		$Year01="";
		if($arrDate != "0000-00-00" or $arrDate != ""){
			$arrDate = explode("-",$arrDate);
			$Day01=$arrDate[2];
			$Month01=$arrDate[1];
			$Year01=$arrDate[0];
		}
		?>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?$i++;}?>
<?$k=$i;
for($i=$k;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}else{?>

<?$i=0;
for($i=0;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}?>




	</td>
</tr>
</table>
<br>
<br>
<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button" id="hSubmit1"  name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();" >
<?}else{?>
	<input type="button" id="hSubmit1" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();" >
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='ccardList.php'" class="button">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {return "ccardAutoCustomer.php?q=" + this.text.value;});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {return "ccardAutoOrderNumber.php?q=" + this.text.value;});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {var str = this.text.value;	if( str.length > 2){return "acardAutoTumbon.php?q=" + this.text.value;}});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {var str = this.text.value;if( str.length > 2){return "acardAutoAmphur.php?q=" + this.text.value;}});		
	
	new CAPXOUS.AutoComplete("hZip", function() {var str = this.text.value;	if( str.length > 2){return "acardAutoTumbon.php?q=" + this.text.value;}});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "ccardAutoSale.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hCarNumber", function() {
		return "ccardAutoStockCar.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hStockRedName", function() {
		return "ccardAutoRedCode.php?q=" + this.text.value;
	});	
	
</script>
<script>
	function setCustomerValue(){
		if(document.frm01.hSwitchCustomer.checked == true){
		document.frm01.hCustomerName.value= "";
		document.frm01.hSendingCustomerId.value= "";
		document.frm01.hTitle.value= "";
		document.frm01.hCustomerTitleId.value= "";
		document.frm01.hAddress.value= "";
		document.frm01.hTumbon.value= "";
		document.frm01.hAmphur.value= "";
		document.frm01.hProvince.value= "";
		document.frm01.hTumbonCode.value= "";
		document.frm01.hAmphurCode.value= "";
		document.frm01.hProvinceCode.value= "";		
		document.frm01.hZip.value= "";
		document.frm01.hZipCode.value= "";
		document.frm01.hTitle.focus();
		}else{
			//retrive old value
		document.frm01.hCustomerName.value= "<?=$objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();?>";
		document.frm01.hSendingCustomerId.value= "<?=$objBookingCustomer->getCustomerId()?>";
		document.frm01.hTitle.value= "<?=$objBookingCustomer->getTitle()?>";
		document.frm01.hCustomerTitleId.value= "<?=$objBookingCustomer->getCustomerTitleId()?>";
		document.frm01.hAddress.value= "<?=$objBookingCustomer->getAddress()?>";
		document.frm01.hTumbon.value= "<?=$objBookingCustomer->getTumbon()?>";
		document.frm01.hAmphur.value= "<?=$objBookingCustomer->getAmphur()?>";
		document.frm01.hProvince.value= "<?=$objBookingCustomer->getProvince()?>";
		document.frm01.hTumbonCode.value= "<?=$objBookingCustomer->getTumbonCode()?>";
		document.frm01.hAmphurCode.value= "<?=$objBookingCustomer->getAmphurCode()?>";
		document.frm01.hProvinceCode.value= "<?=$objBookingCustomer->getProvinceCode()?>";		
		document.frm01.hZip.value= "<?=$objBookingCustomer->getZip()?>";
		document.frm01.hZipCode.value= "<?=$objBookingCustomer->getZip()?>";

		}

	}

	function checkAddPrice(){
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡��");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentSubjectId.focus();
			return false;
		} 

		if (document.forms.frm01.hQty.value=="")
		{
			alert("��س��кبӹǹ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hQty.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hPrice.value=="")
		{
			alert("��س��к��Ҥ�");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPrice.focus();
			return false;
		} 			
		
		return true;	
	
	}
	
	function showBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "";	
		document.getElementById("showBookingCustomer").style.display = "none";		
		document.getElementById("hideBookingCustomer").style.display = "";		
	}

	function hideBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "none";	
		document.getElementById("showBookingCustomer").style.display = "";			
		document.getElementById("hideBookingCustomer").style.display = "none";
	}

	function showBookingOrder(){
		document.getElementById("bookingOrder").style.display = "";	
		document.getElementById("showBookingOrder").style.display = "none";		
		document.getElementById("hideBookingOrder").style.display = "";		
	}

	function hideBookingOrder(){
		document.getElementById("bookingOrder").style.display = "none";	
		document.getElementById("showBookingOrder").style.display = "";			
		document.getElementById("hideBookingOrder").style.display = "none";
	}
	
	function showBookingCar(){
		document.getElementById("bookingCar").style.display = "";	
		document.getElementById("showBookingCar").style.display = "none";		
		document.getElementById("hideBookingCar").style.display = "";		
	}

	function hideBookingCar(){
		document.getElementById("bookingCar").style.display = "none";	
		document.getElementById("showBookingCar").style.display = "";			
		document.getElementById("hideBookingCar").style.display = "none";
	}
	
	function showFn(val){
		if(val ==1){
			document.getElementById("fn").style.display = "none";	
			document.getElementById("fnValue").style.display = "none";	
		}else{
			document.getElementById("fn").style.display = "";	
			document.getElementById("fnValue").style.display = "";	
		}
	}
	
</script>
<script>

	function checkOther(){
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="15")
		{
			document.getElementById("spanRemark").style.display = "";	
			document.frm01.hRemark.focus();
		}else{
			document.getElementById("spanRemark").style.display = "none";	
			document.frm01.hQty.focus();
		}
	}
	
	function changeSale(){
		if(document.forms.frm01.hSwitchSale.checked == true){
			document.frm01.hSendingSaleId.value= "";
			document.frm01.hSaleName.value= "";		
			document.frm01.hSaleName.focus();		
		}else{
			document.frm01.hSendingSaleId.value= "<?=$objOrder->getBookingSaleId()?>";
			document.frm01.hSaleName.value= "<?=$hBookingSaleName?>";
		}	
	}
	
	function checkPrice(){		
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡�ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentSubjectId.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hQty.value =="" || document.forms.frm01.hQty.value < 0 )
		{
			alert("��س��кبӹǹ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hQty.focus();
			return false;
		} 				
		
		if (document.forms.frm01.hPrice.value =="" || document.forms.frm01.hPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPrice.focus();
			return false;
		} 
		return true;
		
	}
	
	function checkPayment(){
	
		if (document.forms.frm01.hPaymentTypeId.options[frm01.hPaymentTypeId.selectedIndex].value=="0")
		{
			alert("��س����͡�ٻẺ��ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentTypeId.focus();
			return false;
		} 					

		if (document.forms.frm01.hPaymentPrice.value =="" || document.forms.frm01.hPaymentPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentPrice.focus();
			return false;
		} 
		return true;
	}
	
	function checkRedCode(){
		if(document.frm01.hNoRedCode.checked == true){
			document.frm01.hStockRedId.value = "";
			document.frm01.hStockRedName.value="";
		}
	}
	
	
	function sum_all_head(){
	sum = 0;
	<?
	$i=0;
	forEach($objPaymentSubjectList->getItemList() as $objItem) {?>
		hPriceHead_<?=$i?> = 0;
		hQtyHead_<?=$i?> = 0;
		
		if(document.frm01.hPriceHead_<?=$i?>.value != "" && document.frm01.hPriceHead_<?=$i?>.value != "0"){ hPriceHead_<?=$i?>= (document.frm01.hPriceHead_<?=$i?>.value*1);}		
		if(document.frm01.hQtyHead_<?=$i?>.value != "" && document.frm01.hQtyHead_<?=$i?>.value != "0"){ hQtyHead_<?=$i?>= (document.frm01.hQtyHead_<?=$i?>.value*1);}		
		
		if(document.frm01.hPayinHead_<?=$i?>.checked == true){
			document.frm01.hPriceSumHead_<?=$i?>.value = hPriceHead_<?=$i?>*hQtyHead_<?=$i?>*-1;
			sum= sum-(hPriceHead_<?=$i?>*hQtyHead_<?=$i?>);			
		}else{
			document.frm01.hPriceSumHead_<?=$i?>.value = hPriceHead_<?=$i?>*hQtyHead_<?=$i?>;
			sum= sum+(hPriceHead_<?=$i?>*hQtyHead_<?=$i?>);			
		}
	<?$i++;}?>
	
		
		vat=sum*0.07;
		document.frm01.hPriceTotalHead.value = Math.round(sum-vat);
		document.frm01.hPriceVat.value = Math.round(vat);
		document.frm01.hPriceTotalHeadAll.value = Math.round(sum);
	
	}
	
	sum_all_head();
	
	
	
	function sum_all(){
	sum = 0;
	<?
	$i=0;
	forEach($objPaymentSubjectList01->getItemList() as $objItem) {?>
		hPrice_<?=$i?> = 0;
		hQty_<?=$i?> = 0;
		
		if(document.frm01.hPrice_<?=$i?>.value != "" && document.frm01.hPrice_<?=$i?>.value != "0"){ hPrice_<?=$i?>= (document.frm01.hPrice_<?=$i?>.value*1);}		
		if(document.frm01.hQty_<?=$i?>.value != "" && document.frm01.hQty_<?=$i?>.value != "0"){ hQty_<?=$i?>= (document.frm01.hQty_<?=$i?>.value*1);}		
		
		if(document.frm01.hPayin_<?=$i?>.checked == true){
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>*-1;
			sum= sum-(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}else{
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>;
			sum= sum+(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}
	<?$i++;}?>
	
		document.frm01.hPriceTotal.value = sum;
	
	}
	
	sum_all();
	
	
</script>


<?
	include("h_footer.php")
?>
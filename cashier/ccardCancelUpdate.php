<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();
$objCarSeries = new CarSeries();

$objCustomerBList = new CustomerList();
$objCustomerBList->setFilter(" type_id = 'B' ");
$objCustomerBList->setPageSize(0);
$objCustomerBList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 1 or type_id=2");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();

		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getSendingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();		
		
		$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
		$objCustomer->load();
		
		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" OP.order_id = $hId  AND status = 1 ");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 1 ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" order_price_id ASC ");
		$objOrderPriceList->load();
		
		//load sale name
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrderBooking->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
		
		
		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objStockCar->getCarColorId());
		$objCarColor->load();		
		
	} else {
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {
            $objOrder->setOrderId($hOrderId); 
			$objOrder->setOrderStatus(9);
			$objOrder->setSendingCancelApprove($hSendingCancelApprove);
			$objOrder->setSendingCancelNumber($hSendingCancelNumber);
            $objOrder->setSendingCancelReason($hSendingCancelReason);
            $objOrder->setSendingCancelPrice($hSendingCancelPrice);
			$objOrder->setEditBy($sMemberId);
			
			$pasrErrOrder = $objOrder->checkCashierSendingCancel($hSubmit);

			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					$objOrder->updateCashierSendingCancel();
				}

				unset ($objOrder);
				header("location:ccardCancelPrintPreview.php?hId=$hId");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)

		}
}

$pageTitle = "1. ����������ѹ";
$pageContent = "1.6  �ѹ�֡�����š�ä׹ö";
$strHead03 = "�ѹ�֡�����š�ä׹ö";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hSendingCancelNumber.value=="")
		{
			alert("��س��к��Ţ���㺤׹ö");
			document.forms.frm01.hSendingCancelNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.hSendingCancelReason.value=="")
		{
			alert("��س��к��˵ؼš�ä׹ö");
			document.forms.frm01.hSendingCancelReason.focus();
			return false;
		} 			
	
		if (document.forms.frm01.hSendingCancelApprove.value=="")
		{
			alert("��س��кؼ��͹��ѵԤ׹ö");
			document.forms.frm01.hSendingCancelApprove.focus();
			return false;
		} 		
		
		return true;		

	}
	
	function check_submit_first()
	{

		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���ö");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		return true;		

	}	
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				

<?if($hId != ""){?>
<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="10%" valign="top"></td>
			<td width="40%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><?=$objOrder->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ������ͺ</strong></td>
			<td  valign="top"><?=$objOrder->getOrderDate()?></td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong></td>
			<td width="40%" valign="top" >
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top"><input type="checkbox" value="1" name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>>����¹��ѡ�ҹ���</td>
					<td>
					</td>					
				</tr>
				</table>				
			</td>
			<td class="i_background03" width="10%" valign="top"></td>
			<td width="40%" valign="top">						
						<?=$hSaleName?>
			</td>			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>��������ë���</strong></td>
			<td valign="top" ><input type="radio" onclick="showFn(1);" name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 OR $objOrder->getBuyType() == 0) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" name="hBuyType" onclick="showFn(2);" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��</td>
			<td class="i_background03" valign="top"><label id="fn" style="<?if($objOrder->getBuyType()  !=2) echo "display:none";?>"><strong>����ѷ�ṹ��</strong></label></td>
			<td valign="top">
				<?
				$objFundCompany = new FundCompany();
				$objFundCompany->setFundCompanyId($objOrder->getBuyCompany());
				$objFundCompany->load();
				?>
				<label id="fnValue" style="<?if($objOrder->getBuyType() !=2) echo "display:none";?>"><?=$objFundCompany->getTitle()?></label>
			</td>			
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>����¹����ᴧ</strong></td>
			<td valign="top">				
				<?=$objStockRed->getStockName()?>
			</td>	
			<td class="i_background03" valign="top"><strong>�ҤҢ��</strong></td>
			<td valign="top">
				<?=number_format($objOrder->getOrderPrice())?> �ҷ
			</td>		
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=$objBookingCustomer->getTitle();?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<?=$name?>
					</td>
					<td valign="top">					
					<?$objBookingCustomer->getCustomerTitleId();?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><?=$objBookingCustomer->getAddress();?></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			
			<td  width="40%" valign="top"><?=$objBookingCustomer->getTumbon();?></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><?=$objBookingCustomer->getAmphur();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">�������١������ͺ</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCustomer" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹������ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=$objCustomer->getTitle();?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<?=$name?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><?=$objCustomer->getAddress();?></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><?=$objCustomer->getTumbonCode();?></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><?=$objCustomer->getAmphurCode();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objCustomer->getProvinceCode();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objCustomer->getZip();?></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="10%"><strong>���ö</strong> </td>
			<td valign="top" width="40%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="10%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="40%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>


<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="10%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>
			
			<td width="40%" ><?=$objStockCar->getCarNumber()?></td>
			<td width="10%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><?=$objStockCar->getEngineNumber()?></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<?=$objCarSeries->getCarModelTitle()?>
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<?=$objCarSeries->getTitle()?>
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<?if($strMode == "Update"){		
		if ($objOrderPriceList->getCount() > 0 ){
?>	
<tr>
	<td align="center"  class="listTitle" width="5%">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">��������´</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">vat</td>
	<td align="center"  class="listTitle">�ӹǹ�Թ</td>

</tr>
				<?
				$i=0;
				forEach($objOrderPriceList->getItemList() as $objItem) {?>
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td align="left" class="ListDetail">
						<?=$objItem->getPaymentSubjectTitle()?>
					</td>
					<td align="center" class="ListDetail"><?=$objItem->getRemark();?></td>
					<td align="center" class="ListDetail"><?=$objItem->getQty();?></td>
					<td align="right" class="ListDetail"><?=$objItem->getPrice();?></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hVatTemp[<?=$i?>]" value="1" <?if($objItem->getVat() == 1) echo "checked";?> ></td>
					<?$sumPrice = ($objItem->getPrice()*$objItem->getQty())?>
					<td align="right" class="ListDetail"><?=number_format($sumPrice,2);?></td>
					
				</tr>
					<?
					if($objItem->getVat() == 1){
						$hPriceIncludeVat = $hPriceIncludeVat+$sumPrice;
					}else{
						$hPriceExcludeVat = $hPriceExcludeVat+$sumPrice;
					}
				?>				
				<?
					++$i;
				}
				$grandTotal = $hPriceIncludeVat+$hPriceExcludeVat;
				if($hPriceIncludeVat > 0){
					$totalVat = $hPriceIncludeVat/1.07;
					$vat = $hPriceIncludeVat-$totalVat;
				}
				$totalVat = $totalVat+$hPriceExcludeVat;
				unset($objOrderPriceList);
				?>				
<tr>
	<td align="center" class="listDetail" colspan="4" rowspan="2" >
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><?=nl2br($objOrder->getSendingRemark())?></td>
		</tr>
		</table>	
	</td>
	<td align="right"  class="listDetail" colspan="2"><strong>����Թ</strong></td>		
	<td align="right"  class="error"><?=number_format($totalVat,2);?></td>	
</tr>				
<tr>	
	<td align="right"  class="listDetail" colspan="2"><strong>������Ť������</strong></td>	
	<td align="right"  class="error"><?=number_format($vat,2);?></td>	

</tr>
<tr>
	<td align="center" class="listDetail" colspan="4">
		<table width="100%">
		<tr>
			<td valign="top" width="50" >�ӹǹ�Թ</td>
			<td  class="error"><?currencyThai($grandTotal);?>&nbsp;�ҷ��ǹ</td>
			<td></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" colspan="2"><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><?=number_format($grandTotal,2);?></td>	
</tr>
	
<?}?>
	
<?}?>

</table>

	</td>
</tr>
</table>
<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�������ٻẺ��ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">

<?if($strMode == "Update"){	
		if ($objOrderPaymentList->getCount() > 0 ){?>	
<tr>
	<td align="center" class="listTitle" width="5%">�ӴѺ</td>
	<td align="center" class="listTitle">�ٻẺ��ê����Թ</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
	<td align="center" class="listTitle">��Ҥ��</td>
	<td align="center" class="listTitle">�Ң�</td>
	<td align="center" class="listTitle">�Ţ�����</td>
	<td align="center" class="listTitle">ŧ�ѹ���</td>
</tr>
				<?
				$i=0;
				forEach($objOrderPaymentList->getItemList() as $objItem) {
					$arrDate = explode("-",$objItem->getCheckDate());
				?>
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td align="left" class="ListDetail"><?=$objItem->getPaymentTypeTitle()?></td>
					<td align="right" class="ListDetail"><?=number_format($objItem->getPrice(),2);?></td>
					<td align="left" class="ListDetail"><?=$objItem->getBankTitle()?></td>					
					<td class="ListDetail"><?=$objItem->getBranch();?></td>
					<td class="ListDetail"><?=$objItem->getCheckNo();?></td>
					<td align="center" class="ListDetail"><?if($objItem->getCheckDate() != "0000-00-00") echo $objItem->getCheckDate();?></td>
				</tr>
				<?
					++$i;
				}
				unset($objOrderPaymentList);
				?>
				<input type="hidden" name="hCountTotalPayment" value="<?=$i?>">
<tr>
	<td align="center" class="listDetail" colspan="7" ></td>
</tr>				



		<?}?>
		


<?}?>


		</table>
	</td>
</tr>
</table>
<br>


<form name="frm01" action="ccardCancelUpdate.php" method="POST"  onsubmit="return check_submit();confirmCancel();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">

<table align="center" width="100%">
		<tr>
			<td  width="20%" class="i_background03" valign="top"><strong>�Ţ�����Ѻö�׹</strong></td>						
			<td  valign="top"><input type="text" name="hSendingCancelNumber" size="15"  value="<?=$objOrder->getSendingCancelNumber();?>"></td>
		</tr>
		<tr>
			<td  width="20%" class="i_background03" valign="top"><strong>�˵ؼ�㹡�ä׹ö</strong></td>						
			<td  valign="top"><textarea name="hSendingCancelReason" rows="3" cols="80"><?=$objOrder->getSendingCancelReason();?></textarea></td>
		</tr>
		<tr>
			<td  width="20%" valign="top" class="i_background03"><strong>�ӹǹ�Թ�׹</strong></td>
			<td  valign="top"><input type="text" name="hSendingCancelPrice" size="10"  value="<?=$objOrder->getSendingCancelPrice();?>"> �ҷ</td>
		</tr>
		<tr>
			<td  width="20%" valign="top" class="i_background03"><strong>���͹��ѵԤ׹ö</strong></td>
			<td  valign="top"><input type="text" name="hSendingCancelApprove" size="30"  value="<?=$objOrder->getSendingCancelApprove();?>"></td>
		</tr>				
</table>

<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
      <input type="submit" name="hSubmit1" value="����¡�ä׹ö"  class="button" >
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit"  class="button"  value="¡��ԡ��¡��" onclick="window.location='ccardCancelUpdate.php'">			
	<br><br>
	</td>
</tr>
</table>
</form>

<?}else{?>
<table width="100%" class="i_background05">
<form name="frm01" action="ccardCancelUpdate.php" method="POST" onsubmit="return check_submit_first();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">


<tr>
	<td align="center"><font color=red>��س��к��Ţ���㺨ͧ����ͧ��ä׹ö</font></td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td align="center">
		<table cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>
			<td><input type="submit" name="hSubmit1" value="������¡��"  class="button" ></td>		
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

<table width="100%" >
<tr>
	<td align="center" height="200">

	</td>
</tr>
</table>
<?}?>


<script src="smartforms.js"></script>
<script>
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {
		return "ccardCancelAutoOrderNumber.php?q=" + this.text.value;
	});		
</script>
<script>
	function confirmCancel(){
		if(confirm('�س��ͧ��ä׹�ͧ������� ?')){
			return true;
		}else{
			return false;
		}
	}
</script>
<script>
	function setCustomerValue(){

		document.frm01.hCustomerName.value= "";
		document.frm01.hSendingCustomerId.value= "";
		document.frm01.hTitle.value= "";
		document.frm01.hCustomerTitleId.value= "";
		document.frm01.hAddress.value= "";
		document.frm01.hTumbon.value= "";
		document.frm01.hAmphur.value= "";
		document.frm01.hProvince.value= "";
		document.frm01.hTumbonCode.value= "";
		document.frm01.hAmphurCode.value= "";
		document.frm01.hProvinceCode.value= "";		
		document.frm01.hZip.value= "";

	}

	function checkAddPrice(){
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡��");
			document.forms.frm01.hPaymentSubjectId.focus();
			return false;
		} 

		if (document.forms.frm01.hQty.value=="")
		{
			alert("��س��кبӹǹ");
			document.forms.frm01.hQty.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hPrice.value=="")
		{
			alert("��س��к��Ҥ�");
			document.forms.frm01.hPrice.focus();
			return false;
		} 			
		
		return true;	
	
	}
	
	function showBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "";	
		document.getElementById("showBookingCustomer").style.display = "none";		
		document.getElementById("hideBookingCustomer").style.display = "";		
	}

	function hideBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "none";	
		document.getElementById("showBookingCustomer").style.display = "";			
		document.getElementById("hideBookingCustomer").style.display = "none";
	}

	function showBookingOrder(){
		document.getElementById("bookingOrder").style.display = "";	
		document.getElementById("showBookingOrder").style.display = "none";		
		document.getElementById("hideBookingOrder").style.display = "";		
	}

	function hideBookingOrder(){
		document.getElementById("bookingOrder").style.display = "none";	
		document.getElementById("showBookingOrder").style.display = "";			
		document.getElementById("hideBookingOrder").style.display = "none";
	}
	
	function showBookingCar(){
		document.getElementById("bookingCar").style.display = "";	
		document.getElementById("showBookingCar").style.display = "none";		
		document.getElementById("hideBookingCar").style.display = "";		
	}

	function hideBookingCar(){
		document.getElementById("bookingCar").style.display = "none";	
		document.getElementById("showBookingCar").style.display = "";			
		document.getElementById("hideBookingCar").style.display = "none";
	}
	
</script>
<?
	include("h_footer.php")
?>
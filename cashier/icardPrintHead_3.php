<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');
require_once('../include/numtoword.php');
define("img_check1","check1.jpg");
define("img_check2","check2.jpg");

$resultinwords=new numtoword;
$objCustomer = new Customer();
$objOrder = new Order();
$objOrderExtra = new OrderExtra();
$objOrderPayment = new OrderPayment();
$objOrderPrice = new OrderPrice();
$objCarSeries = new CarSeries();
$objInsureBroker = new InsureBroker();

$objCompany =  new Company();
$objCompany->setCompanyId($sCompanyId);
$objCompany->load();

if ($hInsureItemId!="") {

		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsureId);
		$objInsure->load();
		
		$objInsureItem = new InsureItem();
		$objInsureItem->set_insure_item_id($hInsureItemId);
		$objInsureItem->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objInsureBroker = new InsureBroker();
		$objInsureBroker->setInsureBrokerId($objInsure->get_k_broker_id());
		$objInsureBroker->load();
		
		$objInsureCompany = new InsureCompany();
		$objInsureCompany->setInsureCompanyId($objInsure->get_k_prb_id());
		$objInsureCompany->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objSale = new Member();
		$objSale->setMemberId($objInsureCar->get_sale_id());
		$objSale->load();

		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objPaymentList = new OrderPaymentList();
		$objPaymentList->setFilter(" order_extra_id = $hInsureItemId  AND order_id= $hInsureId AND status = 4");
		$objPaymentList->setPageSize(0);
		$objPaymentList->setSortDefault(" order_payment_id ASC ");
		$objPaymentList->load();

		$objPriceList = new InsureItemDetailList();
		$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= $hInsureId and PS.nohead = 0  ");
		$objPriceList->setPageSize(0);
		$objPriceList->setSort(" PS.rank ASC ");
		$objPriceList->load();
		

}


if(isset($hNumber01)){
	
	$objInsureItem =new InsureItem();
	$objInsureItem->set_insure_item_id($hInsureItemId);
	$objInsureItem->set_old_number_nohead($hNumber01);
	$objInsureItem->updateOldNumberNohead();
	
}

if(isset($hNumber02)){
	
	$objInsureItem =new InsureItem();
	$objInsureItem->set_insure_item_id($hInsureItemId);
	$objInsureItem->set_old_number_head($hNumber02);
	$objInsureItem->updateOldNumberHead();
	
}


class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",0,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(0,0,0);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

$pdf->AddPage();
$pdf->SetFont('angsa','B',14);



//***********************************************************  Load Data ***********************************************
				$v=0;
				$objPriceList = new InsureItemDetailList();
				$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= ".$hInsureId." and PS.nohead = 1 ");
				$objPriceList->setPageSize(0);
				$objPriceList->setSort(" PS.rank ASC ");
				$objPriceList->load();
				
				forEach($objPriceList->getItemList() as $objItem) {
				
				$objPaymentSubject = new PaymentSubject();
				$objPaymentSubject->setPaymentSubjectId($objItem->getPaymentSubjectId());
				$objPaymentSubject->load();
				$arrT1[$v] = $objPaymentSubject->getTitle()." ".$objItem->getRemark() ;
				$arrT2[$v] = 1;
				$arrT3[$v] = number_format($objItem->getPrice(),2);
				$arrT4[$v] = number_format($objItem->getPriceAcc(),2);
					$v++;
				}
				
				$num = 7-$objPriceList->mCount;

				$objPriceList = new InsureItemDetailList();
				$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= ".$hInsureId." and PS.nohead = 1  ");
				$objPriceList->setPageSize(0);
				$objPriceList->setSort(" PS.rank ASC ");
				$objPriceList->load();
				forEach($objPriceList->getItemList() as $objItem) {
				if($objItem->getPrice() > 0){
					$sumPrice = ($objItem->getPrice()*$objItem->getQty());
					$sumPriceAcc = ($objItem->getPriceAcc()*$objItem->getQty());
					$sumPriceTotal = $sumPriceTotal+($objItem->getPrice()*$objItem->getQty());
					$sumPriceAccTotal = $sumPriceAccTotal+($objItem->getPriceAcc()*$objItem->getQty());
					if($objItem->getVat() == 1){
						if($objItem->getPayin() == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPriceAcc;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPriceAcc;
						}
					}else{
						if($objItem->getPayin() == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPriceAcc;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPriceAcc;
						}
						
					}
				}
				}
				
				
				
				$totalVat = $hPriceIncludeVat+$hPriceExcludeVat;
				$before_vat = $totalVat/1.07;
				$vat = $before_vat*(7/100);
				$total_all = $before_vat+$vat;
				
				if($sumPriceTotal != $sumPriceAccTotal){
				$k--;

				if($sumPriceTotal > $sumPriceAccTotal){
					$text = "*** �����˵� : �Ҵ���� ".number_format( ($sumPriceTotal-$sumPriceAccTotal),2)." �ҷ";
				}else{
					$text = "*** �����˵� : �����Թ ".number_format( ($sumPriceAccTotal-$sumPriceTotal),2)." �ҷ";
				}
				
				$arrT1[$v] = $text;
				$arrT2[$v] = "";
				$arrT3[$v] = "";
				$arrT4[$v] = "";
				$v++;
				
				}
				
				
				
				for($n=1;$n<=$num;$n++){
					if($n == 1){
						if($objInsure->get_k_number() != "") $nText = "�����Ţ�������� ".$objInsure->get_k_number();
						if($objInsure->get_p_number() != "") $nText.= "  �����Ţ�ú. ".$objInsure->get_p_number();
						$arrT1[$v] = $nText;
						$arrT2[$v] = "";
						$arrT3[$v] = "";
						$arrT4[$v] = "";
						$v++;
					}
				}				

				
				$objMember = new Member();
				$objMember->setMemberId($objInsureItem->get_acc_by());
				$objMember->load();
				$hOfficer = $objMember->getFirstname()." ".$objMember->getLastname();
				
				$objOrderPaymentList = new OrderPaymentList();
				$objOrderPaymentList->setFilter(" status = 4 AND order_extra_id = $hInsureItemId  AND order_id = $hInsureId  ");
				$objOrderPaymentList->load();
				$i=0;
				forEach($objOrderPaymentList->getItemList() as $objItem) {
					if($objItem->getNoHead() == 1){
						$hPaymentType =$objItem->getPaymentTypeId();
						//�Թʴ 1
						//�Թ�͹ 4
						//�� 3
						//�ôԵ 2
						$objBank = new Bank();
						$objBank->setBankId($objItem->getBankId());
						$objBank->load();
						$hBank = $objBank->getTitle();
						$hBranch = $objItem->getBranch();
						$hCheckNo =$objItem->getCheckNo();
					
					}
				}
				
				
//***********************************************************  Load Data ***********************************************


$objCL = new CashierBillItemList();
$objCL->setFilter(" cashier_bill_id = ".$hCashierBillId);
$objCL->setPageSize(100);
$objCL->setSort(" rank asc ");
$objCL->load();
forEach($objCL->getItemList() as $objItem) {
	$pdf->SetFont('angsa','B',14);
	if($objItem->get_title() != ""){
		if($objItem->get_rank() == 5 or $objItem->get_rank() ==6){
			$pdf->SetFont('angsa','B',16);
		}
	
		$pdf->Text($objItem->get_x1(),$objItem->get_y1(),trim($objItem->get_title()));
	}else{
		if($objItem->get_rank() == 10){
			//����١���
			if($objCustomer->getTitle() != "˨�" AND $objCustomer->getTitle() != "���" ){
				$title = $objCustomer->getCustomerTitleIdDetail01();
			}else{
				$title = $objCustomer->getTitle();
			}
			
			$strText =$title." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname();
			
		}
		if($objItem->get_rank() == 12){
			//�Ţ��Шӵ�Ǽ����������
			$strText = $objCustomer->getIDCard();
		}
		if($objItem->get_rank() == 14){
			//����¹
			$strText=$objInsureCar->get_code();
		}
		if($objItem->get_rank() == 16){
			//�ѹ���
			$strText=formatShortDateTimeCashier($objInsureItem->get_acc_date());
		}
		if($objItem->get_rank() == 18){
			//�������1
			$strText=$objCustomer->getAddressLabel();
		}
		if($objItem->get_rank() == 20){
			//�Ţ���
			//$strText=$objInsureItem->get_old_number_head();
			$strText =$objInsureItem->get_cashier_number();
		}
		if($objItem->get_rank() == 21){
			//�������2
				if($objCustomer->getProvince() == "��ا෾��ҹ��"){
				
				if($objCustomer->getTumbon() != "") $text=  "�ǧ ".$objCustomer->getTumbon();
				if(substr($objCustomer->getAmphur(),0,3) == "ࢵ"){
					if($objCustomer->getAmphur() != "") $text.=  "  ".$objCustomer->getAmphur();
				}else{
					if($objCustomer->getAmphur() != "") $text.=  " ࢵ ".$objCustomer->getAmphur();
				}
				if($objCustomer->getProvince() != "") $text.=  " �ѧ��Ѵ ".$objCustomer->getProvince();
				if($objCustomer->getZip() != "") $text.=  " ������ɳ��� ".$objCustomer->getZip();	
				
				}else{
				
				if($objCustomer->getTumbon() != "") $text=  "�Ӻ� ".$objCustomer->getTumbon();
				if($objCustomer->getAmphur() != "") $text.=  " ����� ".$objCustomer->getAmphur();
				if($objCustomer->getProvince() != "") $text.=  " �ѧ��Ѵ ".$objCustomer->getProvince();
				if($objCustomer->getZip() != "") $text.=  " ������ɳ��� ".$objCustomer->getZip();	
				
				}			
				
				$strText=$text;
				
		}
		if($objItem->get_rank() == 23){
			//��ѡ�ҹ���
			$strText= $objSale->getFirstname()." ".$objSale->getLastname();
		}
		if($objItem->get_rank() == 25){		
			//����
			$strText =$objInsureItem->get_payment_number_head();
			//$strText ="";
		}
		
		//��¡��1
		if($objItem->get_rank() == 30){
			$strText = $arrT1[0];
		}		

		if($objItem->get_rank() == 31){
			//�ӹǹ1
			$strText = $arrT2[0];
		}
		if($objItem->get_rank() == 32){
			//˹�����1
			$strText = $arrT3[0];
		}
		if($objItem->get_rank() == 33){
			//�ӹǹ�Թ1
			$strText = $arrT4[0];
		}			

		if($objItem->get_rank() == 34){
			$strText = $arrT1[1];
		}		

		if($objItem->get_rank() == 35){
			//�ӹǹ1
			$strText = $arrT2[1];
		}
		if($objItem->get_rank() == 36){
			//˹�����1
			$strText = $arrT3[1];
		}
		if($objItem->get_rank() == 37){
			//�ӹǹ�Թ1
			$strText = $arrT4[1];
		}			
				
		if($objItem->get_rank() == 38){
			$strText = $arrT1[2];
		}		

		if($objItem->get_rank() == 39){
			//�ӹǹ1
			$strText = $arrT2[2];
		}
		if($objItem->get_rank() == 40){
			//˹�����1
			$strText = $arrT3[2];
		}
		if($objItem->get_rank() == 41){
			//�ӹǹ�Թ1
			$strText = $arrT4[2];
		}			

		if($objItem->get_rank() == 42){
			$strText = $arrT1[3];
		}		

		if($objItem->get_rank() == 43){
			//�ӹǹ1
			$strText = $arrT2[3];
		}
		if($objItem->get_rank() == 44){
			//˹�����1
			$strText = $arrT3[3];
		}
		if($objItem->get_rank() == 45){
			//�ӹǹ�Թ1
			$strText = $arrT4[3];
		}			

		if($objItem->get_rank() == 46){
			//��¡��5
			$strText = $arrT1[4];
		}
		if($objItem->get_rank() == 47){
			//�ӹǹ5
			$strText = $arrT2[4];
		}
		if($objItem->get_rank() == 48){
			//˹�����5
			$strText = $arrT3[4];
		}
		if($objItem->get_rank() == 49){
			//�ӹǹ�Թ5
			$strText = $arrT4[4];
		}						
		
		if($objItem->get_rank() == 50){
			//��¡��6
			$strText = $arrT1[5];			
		}
		if($objItem->get_rank() == 51){
			//�ӹǹ6
			$strText = $arrT2[5];			
		}
		if($objItem->get_rank() == 52){
			//˹�����6
			$strText = $arrT3[5];			
		}
		if($objItem->get_rank() == 53){
			//�ӹǹ�Թ6
			$strText = $arrT4[5];			
		}
		
		if($objItem->get_rank() == 54){
			//��¡��7
			$strText = $arrT1[6];			
		}
		if($objItem->get_rank() == 55){
			//�ӹǹ7
			$strText = $arrT2[6];			
		}
		if($objItem->get_rank() == 56){
			//˹�����7
			$strText = $arrT3[6];			
		}
		if($objItem->get_rank() == 57){
			//�ӹǹ�Թ7
			$strText = $arrT4[6];			
		}		
		
		
		
		
		
		if($objItem->get_rank() == 58){
			//�����˵�1
		}		
		
		if($objItem->get_rank() == 59){
			//�����˵�2
		}
		
		if($objItem->get_rank() == 61){
			//�ӹǹ�Թ���
			$strText =number_format($totalVat,2);
		}		
		
		if($objItem->get_rank() == 63){
			//��Ť�ҡ�͹����
			$strText =number_format($before_vat,2);
		}		
		
		if($objItem->get_rank() == 65){
			//������Ť������
			$strText =number_format($vat,2);
		}		
		
		if($objItem->get_rank() == 66){
			//�ӹǹ�Թ�������ѡ��
			$strText = currencyThai01($total_all);
		}										
		
		if($objItem->get_rank() == 68){
			//�ӹǹ�Թ���������
			$strText = number_format($total_all,2);
		}								
		
		if($objItem->get_rank() == 70){
			//�Թʴ
			$strText="";
			if($hPaymentType ==1){
				$pdf->Image(img_check2,$objItem->get_x1(),$objItem->get_y1(),10);
			}else{
				$pdf->Image(img_check1,$objItem->get_x1(),$objItem->get_y1(),10);
			}
		}								
		
		if($objItem->get_rank() == 78){
			//�Թ�͹
			$strText="";			
			if($hPaymentType ==4){
				$pdf->Image(img_check2,$objItem->get_x1(),$objItem->get_y1(),10);
			}else{
				$pdf->Image(img_check1,$objItem->get_x1(),$objItem->get_y1(),10);
			}
		}								
		
		if($objItem->get_rank() == 81){
			//��
			$strText="";
			if($hPaymentType ==3){
				$pdf->Image(img_check2,$objItem->get_x1(),$objItem->get_y1(),10);
			}else{
				$pdf->Image(img_check1,$objItem->get_x1(),$objItem->get_y1(),10);
			}
		}								
		
		if($objItem->get_rank() == 72){
			//�ѵ��ôԵ
			$strText="";			
			if($hPaymentType ==2){
				$pdf->Image(img_check2,$objItem->get_x1(),$objItem->get_y1(),10);
			}else{
				$pdf->Image(img_check1,$objItem->get_x1(),$objItem->get_y1(),10);
			}		}								
		
		if($objItem->get_rank() == 74){
			//�ѵ��ôԵ�����Ţ
			$strText="";			
		}								
		
		if($objItem->get_rank() == 76){
			//�Ǵ
			$strText="";			
		}								
		
		if($objItem->get_rank() == 85){
			//�Ǵ
			$strText=$objCarModel->getTitle();
		}										
		
		
		
		if($objItem->get_rank() == 80){
			//�Թ�͹��������´
			$strText= $hBank." �Ң� ".$hBranch;
		}
		$pdf->Text($objItem->get_x1(),$objItem->get_y1(),trim($strText));		

		}
		

}

				
	//Unset($objCustomerList);
	$pdf->Output();	
	
	unset($objCustomer);
	unset($objOrder);
	unset($objOrderExtra);
	unset($objOrderPayment);
	unset($objOrderPrice);
	unset($objCarSeries);
	unset($objInsureBroker);
	unset($objCompany);
	unset($objInsure);
	unset($objInsureItem);
	unset($objInsureCar);
	unset($objInsureBroker);
	unset($objInsureCompany);
	unset($objCarModel);
	unset($objSale);
	unset($objCustomer);
	unset($objPaymentList);
	unset($objPriceList);
	unset($pdf);
	unset($objPaymentSubject);
	
	?>
	<script>
	//window.close();
	</script>	
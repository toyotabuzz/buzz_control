<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if (isset($hSearch) )
{  
	$hStartDate = $Year."-".$Month."-".$Day;
	$hEndDate   = $YearEnd."-".$MonthEnd."-".$DayEnd;
	
	if($hStartDate != "--" && $hEndDate != "--"){
		$pstrCondition  = " ( P.payment_date BETWEEN '".$hStartDate."' AND '".$hEndDate."' ) ";
	}else{
		if($hStartDate != "--"){	
			$pstrCondition  = " ( P.payment_date = '".$hStartDate."'  ) ";	
		}
	}

	$hBeginDate = $YearBegin."-".$MonthBegin."-".$DayBegin;
	$hFinishDate   = $YearFinish."-".$MonthFinish."-".$DayFinish;

	if($hBeginDate != "--" && $hFinishDate != "--"){
		$pstrCondition  = " ( OP.check_date BETWEEN '".$hBeginDate."' AND '".$hFinishDate."' ) ";
	}else{
		if($hBeginDate != "--"){	
			$pstrCondition  = " ( OP.check_date = '".$hBeginDate."'  ) ";
		}
	}
	
	// if($hPrb != ""){
	// 	if($pstrCondition == ""){
	// 		$pstrCondition.= "  I.p_number LIKE '%$hPrb%'  ";
	// 	}else{
	// 		$pstrCondition.= " AND  I.p_number LIKE '%$hPrb%'   ";
	// 	}	
	// }
	
	if($hCompanyId  > 0){
		if($pstrCondition == ""){
			$pstrCondition.= "  F.company_id = '$hCompanyId' ";
		}else{
			$pstrCondition.= " AND   F.company_id = '$hCompanyId'  ";
		}	
	}
	
	// if($hKom != ""){
	// 	if($pstrCondition == ""){
	// 		$pstrCondition.= "  I.k_number LIKE '%$hKom%'  ";
	// 	}else{
	// 		$pstrCondition.= " AND  I.k_number LIKE '%$hKom%'   ";
	// 	}	
	// }
	
	if($hRedNumber != ""){
		if($pstrCondition == ""){
			$pstrCondition.= "  IC.code LIKE '%$hRedNumber%'  ";
		}else{
			$pstrCondition.= " AND IC.code LIKE '%$hRedNumber%'    ";
		}	
	}

	if($hCarNumber != ""){
		if($pstrCondition == ""){
			$pstrCondition.= "  IC.car_number LIKE '%$hCarNumber%'  ";
		}else{
			$pstrCondition.= " AND IC.car_number LIKE '%$hCarNumber%'    ";
		}	
	}

	if($hWaitTransfer != 2){
		if($hPaymentStyleId != 0){
			if($pstrCondition == ""){
				$pstrCondition.= "  P.payment_style_id = '$hPaymentStyleId'  ";
			}else{
				$pstrCondition.= " AND P.payment_style_id = '$hPaymentStyleId'    ";
			}	
		}
	}
	
	if( $hCustomerName !=""){
			if($pstrCondition != ""){
				$pstrCondition.=" AND (  ( C.firstname LIKE '%".trim($hCustomerName)."%')  OR  ( C.lastname LIKE '%".trim($hCustomerName)."%')   )  ";
			}else{
				$pstrCondition.=" ( ( C.firstname LIKE '%".trim($hCustomerName)."%')  OR  ( C.lastname LIKE '%".trim($hCustomerName)."%')   )   ";
			}
	}
	
	if($hWaitTransfer != '2'){

		if($pstrCondition == ""){
			$pstrCondition.= "  payment_status= ''  AND  (  ( I.k_check =1 and  I.k_status <> 'cancel' ) OR  ( I.p_check=1 AND I.p_status <> 'cancel' ) )  ";
		}else{
			$pstrCondition.= " AND payment_status='' AND  ( ( I.k_check =1 and  I.k_status <> 'cancel' ) OR  ( I.p_check=1 AND I.p_status <> 'cancel' )  )  ";
		}
	}
	
	
	$sConditionCashier01 = $pstrCondition;	
	session_register("sConditionCashier01");
	
	
}else{

	if($hWaitTransfer != '2'){

		if($sConditionCashier01 == ""){
			$pstrCondition ="  payment_status= ''  AND  (  ( I.k_check =1 and  I.k_status <> 'cancel' ) OR  ( I.p_check=1 AND I.p_status <> 'cancel' ) ) ";
			$sConditionCashier01 = $pstrCondition;	
			session_register("sConditionCashier01");
		}else{
			$pstrCondition = $sConditionCashier01;
		}

	}


}

/*
if($pstrCondition == ""){
	$pstrCondition.= "  payment_status= ''  AND  (  ( k_check =1 and  k_status <> 'cancel' ) OR  ( p_check=1 AND p_status <> 'cancel' ) )  ";
}else{
	$pstrCondition.= " AND payment_status='' AND  ( ( k_check =1 and  k_status <> 'cancel' ) OR  ( p_check=1 AND p_status <> 'cancel' )  )  ";
}
*/
if($hPage == "") $hPage =1;

if($hWaitTransfer == '2'){
	if($pstrCondition != ""){
		$pstrCondition.=" AND OP.payment_type_id = 4 AND acc_check='' AND P.payment_status =''  ";
	}else{
		$pstrCondition.=" OP.payment_type_id = 4 AND acc_check='' AND P.payment_status =''  ";
	}
}

// echo $pstrCondition;

$objInsureList = new InsureItemList();
$objInsureList->setFilter($pstrCondition);
$objInsureList->setPageSize(PAGE_SIZE);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" payment_date, payment_order ASC ");
if($hWaitTransfer == '2'){
	$objInsureList->loadOrderPayment();
}else{
	$objInsureList->load(); 
}

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentStyleList = new PaymentStyleList();
$objPaymentStyleList->setFilter(" ref_type='PAYMENT' AND ref_code='STYLE' ");
$objPaymentStyleList->setPageSize(0);
$objPaymentStyleList->setSortDefault(" ref_seq ASC ");
$objPaymentStyleList->load();

$pCurrentUrl = "icardNewList.php?hKeyword=$hKeyword&hType=$hType";
	// for paging only (sort resets page viewing)


$pageTitle = "1. �к��������١���";
$strHead03 = "���Ң������١���";
$pageContent = "1.8 �ѹ�֡�Ѻ���л�Сѹ���";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
				<table align="center" class="search">
				
				
				<tr>
					<td >�Ң� :</td>
					<td >�ѹ���Ѵ����  :</td>
					<td >�֧�ѹ���  :</td>
					<td >�ѹ�������Թ  :</td>
					<td >�֧�ѹ���  :</td>
					<td >�Ţ����¹ :</td>	
					<td >�Ţ�ѧ : </td>						
					<td >����-���ʡ�� : </td>	
					<td >�ٻẺ��ê����Թ </td>	
					<td >��͹��ѵ��Թ�͹ </td>	
					<td></td>
					<td></td>
				</tr>		
				<tr>
					<form  name="frm01" action="<?=$PHP_SELF?>" method="get" onKeyDown="if(event.keyCode==13) event.keyCode=9;" onsubmit="return check_submit();" >
					<td>
						<?
						$objCList = new CompanyList();
						$objCList->setPageSize(0);
						$objCList->load();
						$objCList->printSelect("hCompanyId",$hCompanyId,"��س����͡�Ң�");
						?>
					</td>
					<td align="center">	
					
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"  id="hDay" name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2" id="hMonth" name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" id="hYear" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
						</tr>
						</table>
						
					</td>

					<td align="center">	
					
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2" id="DayEnd"  name=DayEnd value="<?=$DayEnd?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2" id="MonthEnd" name=MonthEnd value="<?=$MonthEnd?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" id="YearEnd" onblur="checkYear(this,this.value);"  name=YearEnd value="<?=$YearEnd?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearEnd,DayEnd, MonthEnd, YearEnd,popCal);return false"></td>		
						</tr>
						</table>

					</td>
					<td align="center">
						<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"  id="hDayBegin" name=DayBegin value="<?=$DayBegin?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2" id="hMonthBegin" name=MonthBegin value="<?=$MonthBegin?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" id="hYearBegin" onblur="checkYear(this,this.value);"  name=YearBegin value="<?=$YearBegin?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearBegin,DayBegin, MonthBegin, YearBegin,popCal);return false"></td>		
						</tr>
						</table>

					</td>

					<td align="center">	
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2" id="DayFinish"  name=DayFinish value="<?=$DayFinish?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2" id="MonthFinish" name=MonthFinish value="<?=$MonthFinish?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" id="YearFinish" onblur="checkYear(this,this.value);"  name=YearFinish value="<?=$YearFinish?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearFinish,DayFinish, MonthFinish, YearFinish,popCal);return false"></td>		
						</tr>
						</table>
						
					</td>	
					<td >
						<INPUT align="middle" size="15" id="hRedNumber" name=hRedNumber value="<?=$hRedNumber?>">
					</td>	
					<td >
						<INPUT align="middle" size="15" id="hCarNumber" name=hCarNumber value="<?=$hCarNumber?>">
					</td>						
					<td >
						<INPUT align="middle" size="15" id="hCustomerName" name=hCustomerName value="<?=$hCustomerName?>">
					</td>
					<td >
						<?php echo $objPaymentStyleList->printSelectScript("hPaymentStyleId",$hPaymentStyleId,"��س����͡"); ?>
					</td>	
					<td><input type="checkbox" onblur="sum_var(1)" name="hWaitTransfer" value="2" <?php if($hWaitTransfer == '2'){ echo 'checked'; } ?> onclick="return addDate();"> </td>
					<td><input type="submit" name="hSearch" value="Search"></td>
					</form>	

					<form target="_blank"  name="frm02" action="payment-insurance-excel.php?hSearch=hSearch" method="post" >
						<td><input type="submit" name="hExport" value="Export" onclick="return checkExport();"></td>
					</form>	

				</tr>		
				</table>
				
				
<?if($objInsureList->getCount() > 0){?>
	
<div  align="center">�� <?=$objInsureList->getCount();?> ��¡��</div>


<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="5%" align="center" class="ListTitle">�ӴѺ</td>
	<td width="7%" align="center" class="ListTitle">�ѹ���Ѵ����</td>
	<td width="7%" align="center" class="ListTitle">�ѹ�������Թ</td>
	<td width="5%" align="center" class="ListTitle">�Ǵ���</td>
	<td width="15%" align="center" class="ListTitle">����</td>
	<td width="15%" align="center" class="ListTitle">
	<font color="#006600"><strong>�����Ţ �ú.</strong></font><br>
	<font color="#cc3366"><strong>�����Ţ��������� (�ѹ�����觡�������)</strong></font>	
	</td>
	<td width="10%" align="center" class="ListTitle">����¹ö</td>
	<td width="10%" align="center" class="ListTitle">�ӹǹ�Թ<br>����ͧ����</td>
	<td width="10%" align="center" class="ListTitle">�ٻẺ��ê����Թ</td>
	<td width="10%" align="center" class="ListTitle">Payment</td>
	<td width="10%" align="center" class="ListTitle" valign="top">�ʴ�������<br>��Сѹ���</td>
	<td width="10%" align="center" class="ListTitle">�����Թ</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objInsureCar->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
		$objCarSeries->load();
		
		$objInsurePayment = new InsurePayment();
		$objInsurePayment->loadByCondition('  order_Id  = '.$objItem->get_insure_item_id());
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td align="center" valign="top"><?=(($hPage-1)*PAGE_SIZE)+$i+1?></td>
	<td valign="top" align="center" >
		<?php echo formatShortDate($objItem->get_payment_date()); ?> 
		<?php 
			if($objItem->get_payment_time() != ''){ 
				if($objItem->get_payment_time() != '00:00:00'){ 
					$arrTime = explode(":",$objItem->get_payment_time());
					echo '<br>'.$arrTime[0].':'.$arrTime[1];
				}
			}
		?>		
	</td>
	<td valign="top" align="center" ><?php echo formatShortDate($objInsurePayment->getCheckDate()); ?> </td>	
	<td valign="top" align="center" ><?=$objItem->get_payment_order()?>/<?=$objInsure->get_pay_number()?></td>	
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  >
	<font color="#006600"><strong><?=$objInsure->get_p_number()?></strong></font>
	<?if($objInsure->get_p_number() != "" and $objInsure->get_k_number() != ""){?><br><?}?>
	<font color="#cc3366"><strong><?=$objInsure->get_k_number()?></strong>
	<?if($objInsure->get_k_send() ){
		$objIML1 = new InsureMailItemDetail();
		$objIM1 = new InsureMailItem();

			$objIML1->set_insure_mail_item_detail_id($objInsure->get_k_send());
			$objIML1->load();
			$objIM1->set_insure_mail_item_id($objIML1->get_insure_mail_item_id());
			$objIM1->load();
	?> &nbsp;&nbsp;(<?=formatLongDateTime($objIM1->get_from_date());?>)
	
	<?
		unset($objIML1);
		unset($objIM1);
		}?>
	</font>
	</td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?></td>
	<td valign="top"  align="right"  ><?=number_format($objItem->get_payment_price(),2)?></td>
	<td width="10%" align="center"><?=$objItem->get_payment_style_title()?></td>
	<td width="10%" align="center">
		<?php 
			if (($objItem->get_payment_style_id()!='')&&($objItem->get_payment_style_id()==3)) { // �ʴ�����੾�Ъ��д��� Payment Gateway ��ҹ��
				$allowPaymentLink = true;
				$objITOld = new InsureItem();
				if($objItem->get_payment_order() != 1){
					$objITOld->loadByCondition(" insure_id = ".$objItem->get_insure_id()." and payment_order = ".($objItem->get_payment_order()-1)." ");
					if ($objITOld->get_payment_status()=='') {
						$allowPaymentLink = false;
					}
				}else{
					if($objItem->get_payment_status()!='') {
						$allowPaymentLink = false;
					}
				}
				if ($allowPaymentLink) {
		?>

		
				<a style="font: bold 11px Arial; text-decoration: none; background-color: #EEEEEE; color: #333333; padding: 2px 6px 2px 6px; border-top: 1px solid #CCCCCC; border-right: 1px solid #333333; border-bottom: 1px solid #333333; border-left: 1px solid #CCCCCC;" href="/insurance/sendPaymentToCustomer.php?hInsureId=<?=$objItem->get_insure_id()?>&hInsureItemId=<?=$objItem->get_insure_item_id()?>">Payment</a>
		<?php
				}
			}
		?>
	</td>
	<td valign="top"  align="center"  ><a href="insure_detail.php?hInsureItemId=<?=$objItem->get_insure_item_id()?>&hInsureId=<?=$objItem->get_insure_id()?>&hCustomerId=<?=$objCustomer->getInsureCustomerId()?>">�ʴ�������</a></td>
	<td  valign="top" align="center">
	<?if($hInsureCheck != $objItem->get_insure_id()){?>
	<a href="icardUpdate.php?hInsureItemId=<?=$objItem->get_insure_item_id()?>&hInsureId=<?=$objItem->get_insure_id()?>&hCustomerId=<?=$objCustomer->getInsureCustomerId()?>">�ѹ�֡�Ѻ����</a>
	<?}?>
	</td>	
</tr>

<tr><td colspan="2"></td><td colspan="5" background="../images/line_double.gif" height="3"></td></tr>
		
		
	<?
	$hInsureCheck = $objItem->get_insure_id();
	++$i;
	}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?}//end check hSearch?>

<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
	function check_submit(){
		document.frm01.action = "icardNewList.php";
		document.frm01.method = "GET";
		document.frm01.submit();
	}
	function checkExport(){
		document.frm01.action = "payment-insurance-excel.php?hSearch=hSearch";
		document.frm01.method = "POST";
		document.frm01.submit();
		return false;
	}
	function addDate(){
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;  if(month.toString().length==1){var month = '0'+month;}
		var day = now.getDate(); if(now.toString().length==1){var day = '0'+day;}
		var hour = now.getHours(); if(hour.toString().length==1){var hour = '0'+hour;}
		var minu = now.getMinutes(); if(minu.toString().length==1){var minu = '0'+minu;}
		var seco = now.getSeconds(); if(seco.toString().length==1){var seco = '0'+seco;}
		// var date = year+'-'+month+'-'+day+'-'+hour+'-'+minu+'-'+seco;
		document.getElementById("hDayBegin").value= day;
		document.getElementById("hMonthBegin").value= month;
		document.getElementById("hYearBegin").value= year;

		document.getElementById("DayFinish").value= day;
		document.getElementById("MonthFinish").value= month;
		document.getElementById("YearFinish").value= year;
		// console.log(date);
	}
</script>
<?
	include("h_footer.php")
?>

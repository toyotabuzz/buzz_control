<?
include("common.php");
require_once('../include/numtoword.php');

$resultinwords=new numtoword;
$objCustomer = new Customer();
$objOrder = new Order();
$objOrderPayment = new OrderPayment();
$objOrderPrice = new OrderPrice();
$objCarSeries = new CarSeries();

if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getOrderDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$objPaymentList = new OrderPaymentList();
		$objPaymentList->setFilter(" order_id = $hId  AND status = 0");
		$objPaymentList->setPageSize(0);
		$objPaymentList->setSortDefault(" order_payment_id ASC ");
		$objPaymentList->load();

		$objPriceList = new OrderPriceList();
		$objPriceList->setFilter(" OP.order_id = $hId  AND status = 0");
		$objPriceList->setPageSize(0);
		$objPriceList->setSortDefault(" order_price_id ASC ");
		$objPriceList->load();
		
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();		
		
} else {
	header("location:error.php?hErrMsg='��辺����㺨ͧ'");
	exit;
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title></title>
	<title>Toyota Buzz  : Cashier System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_print.css">
	
</head>

<body>
<table width=770  cellpadding="0" cellspacing="0" border="0">
<tr>
	<td valign="top">
<table width="100%" cellpadding="0" cellpadding="0" border=0>
<tr>
	<td width="70%" height=20 valign="top">
		<table align="right">
		<tr>
			<td></td>
			<td align="right" valign="top">
				<span class="Head">��������´��ê����Թ</span>&nbsp;
			
			</td>
		</tr>
		</table>	
	</td>
</tr>
<tr>
	<td width="70%" height=15 valign="top">
		&nbsp;
	</td>
	<td align="right">
		
	</td>
</tr>
</table>
<table width="100%" border=0>
<tr  >
	<td rowspan="2" width=80% valign=top >
	<?
		if($objCustomer->getTitle() == "���" or $objCustomer->getTitle() == "˨�"){
			 $title=$objCustomer->getTitle().".&nbsp;";
		}else{
			if($objCustomer->getTitle() == "��ҹ��������õ�"){
				 $title="";
			}else{
			 	$title=$objCustomer->getTitle();
			}
		}
		?>
		<table width="100%" border="0">
		<tr>
			<td width="10%"  valign="top"></td>
			<td width="60%" valign="top" height=20><?=$title.$objCustomer->getFirstname()."  ".$objCustomer->getLastname()?></td>
			<td width="15%"  valign="top"></td>
			<td width="15%" valign="top"></td>
		</tr>
		<tr>
			<td  valign="top"></td>
			<td  valign="top" colspan=3 height=25>
				<?=$objCustomer->getAddressLabel()?>		
				<?if($objCustomer->getTumbon() != "") echo  "�Ӻ�/�ǧ ".$objCustomer->getTumbon();?>
				<?if($objCustomer->getAmphur() != "") echo  "�����/ࢵ ".$objCustomer->getAmphur();?>
				<?if($objCustomer->getProvince() != "") echo  "�ѧ��Ѵ ".$objCustomer->getProvince();?>
				<?if($objCustomer->getZip() != "") echo  "������ɳ��� ".$objCustomer->getZip();?>												
			</td>
		</tr>
		<tr>
			<td  valign="top"></td>
			<td colspan="3" valign="top">
				<table width="100%">
				<tr>
					<td width="15%" valign="top" ></td>
					<td  valign="top" ><?=$objCarSeries->getCarModelTitle()?></td>
					<td  valign="top" >Ẻö</td>
					<td  valign="top" ><?=$objCarSeries->getTitle()?></td>
					<td  valign="top" >��</td>
					<td  valign="top" ><?=$objCarColor->getTitle()?></td>
				</tr>
				</table>
	
			
			</td>
		</tr>
		</table>	
	</td>
	<td align="center"   valign="top" width="12%">
		<?$arrDate = explode("-",$objOrder->getBookingCancelDate());?>
		<table width="100%" border=0>
		<tr>
			<td align="center" ><br></td>
		</tr>
		<tr>
			<td align="center" class=small><?=$arrDate[2]."-".$arrDate[1]."-".$arrDate[0];?></td>
		</tr>
		</table>	
	</td>
	<td  align="center"   valign="top" width="8%">
		<table width="100%">
		<tr>
			<td align="center" ><br></td>
		</tr>
		<tr>
			<td align="center" ><span class="small"><?=$objOrder->getBookingCancelNumber()?></span></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td  valign="top">
		<table width="100%">
		<tr>
			<td align="center"  height=25><br></td>
		</tr>
		<?
		$objMember = new Member();
		$objMember->setMemberId($objOrder->getBookingSaleId());
		$objMember->load();
		
		?>
		<tr>
			<td align="center" class=small><?=$objMember->getFirstname()."  ".$objMember->getLastname();?></td>
		</tr>
		</table>		
	
	</td>
	<td valign="top">
		<table width="100%">
		<tr>
			<td align="center"  height=25><br></td>
		</tr>
		<tr>
			<td align="center"><?=$objOrder->getOrderNumber();?></td>
		</tr>
		</table>		
	
	</td>
</tr>
</table>
<table>
<tr>
	<td height=25>&nbsp;</td>
</tr>
</table>

<table width="100%" cellpadding="1" cellspacing="0" align=center border=0>
<tr>
	<td class="i_blueSmallHeader1" align="center" width="10%"></td>
	<td class="i_blueSmallHeader1" align="center" width="50%"></td>
	<td class="i_blueSmallHeader1" align="center" width="10%"></td>
	<td class="i_blueSmallHeader1" align="center" width="15%"></td>
	<td class="i_blueSmallHeader1" align="center" width="15%"></td>
</tr>
<tr>
	<td colspan="5" height="230" valign="top">
		<table width="100%" cellpadding="0" cellspacing="3">
		<tr >
			<td width="10%" align="center" valign="top" >1.</td>
			<td width="50%"  valign="top">�Թ�׹�ͧ</td>
			<td width="10%"  align="right"  valign="top">1</td>
			<td width="15%"  align="right"  valign="top"><?=number_format($objOrder->getBookingCancelPrice())?></td>
			<td width="15%"  align="right"  valign="top"><?=number_format($objOrder->getBookingCancelPrice(),2)?></td>
		</tr>		
		</table>
	</td>
</tr>
<tr>
	<td colspan="3" >
		<table width="100%">
		<tr>
			<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td ><?currencyThai($objOrder->getBookingCancelPrice());?>&nbsp;�ҷ��ǹ</td>
		</tr>
		</table>

	</td>
	<td ></td>
	<td align="right" height=25 ><?=number_format($objOrder->getBookingCancelPrice(),2);?>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td rowspan="3" colspan="3" valign="top"><br>
		�����˵� : <?=$objOrder->getBookingCancelReason()?>
	
	
	
	</td>
	<td ></td>
	<td  align="right" height=20 >&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td ></td>
	<td  align="right" height=20 ><?=number_format($objOrder->getBookingCancelPrice(),2);?>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td  align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center"></td>
		</tr>
		<tr>
			<td    align="center"></td>
		</tr>
		</table>

	</td>
	<td  align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center"></td>
		</tr>
		<tr>
			<td   align="center"></td>
		</tr>
		</table>	
	</td>
</tr>
</table>

</td>
</tr>
</table>


</body>
</html>




<?
include("common.php");

$objCustomer = new Customer();
$objCarSeries = new CarSeries();

$objCarType = new CarType();
$objCarType->loadByCondition(" title LIKE '%TOYOTA%'");

$objCarType01 = new CarModelList();
$objCarType01->setFilter(" car_type_id = ".$objCarType->getCarTypeId());
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault("title ASC");
$objCarSeriesList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setFilter(" car_type_id = ".$objCarType->getCarTypeId());
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 1 or type_id=2");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate1 = explode("-",$objOrder->getBookingCashierDate());
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
		
		$arrTime = explode(":",$objOrder->getBookingCashierTime());
		$Hours1 = $arrTime[0];
		$Minute1 = $arrTime[1];
	
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$arrMobile = explode(",",$objCustomer->getMobile());
		$hMobile_1 = $arrMobile[0];
		$hMobile_2 = $arrMobile[1];
		$hMobile_3 = $arrMobile[2];
		
		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" order_id = $hId  AND status = 0");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 0");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" order_price_id ASC ");
		$objOrderPriceList->load();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$hBookingCarModel = $objCarSeries->getCarModelId();
		$hBookingCarSeries = $objOrder->getBookingCarSeries();
		
	} else {
		$strMode="Add";
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate1 = explode("-",date("Y-m-d"));
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
		
		$Hours1 = date("H");
		$Minute1 = date("i");
		
		if($hCustomerId != ""){
			$objCustomer->setCustomerId($hCustomerId);
			$objCustomer->load();		
		}
		
		
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hCustomerId);
            $objCustomer->setTypeId("B");
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard(1);
			$objCustomer->setCCard($hCCard);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}
				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setAddress($hAddress);
			
			$objCustomer->setHomeNo($hHomeNo);
			$objCustomer->setCompanyName($hCompanyName);
			$objCustomer->setBuilding($hBuilding);
			$objCustomer->setMooNo($hMooNo);
			$objCustomer->setMooban($hMooban);
			$objCustomer->setSoi($hSoi);
			$objCustomer->setRoad($hRoad);
			
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setIncomplete($hIncomplete);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setHomeTel($hHomeTel);
			$objCustomer->setCompanyId($sCompanyId);
			
			$i=0;
			if($hMobile_1 != ""){ $arrMobile[$i] = $hMobile_1;$i++;}
			if($hMobile_2 != ""){ $arrMobile[$i] = $hMobile_2;$i++;}
			if($hMobile_3 != ""){ $arrMobile[$i] = $hMobile_3;$i++;}
			
			$hMobile = implode(",",$arrMobile);
			
			$objCustomer->setMobile($hMobile);
			$objCustomer->setOfficeTel($hOfficeTel);
			$objCustomer->setFax($hFax);
		
			
            $objOrder->setOrderId($hOrderId);
			if($hOrderStatus <1 ){
				$objOrder->setOrderStatus(0);
				
			}else{
				$objOrder->setOrderStatus($hOrderStatus);

			}
			
			if($strMode == "Add"){
			    $objOrder->setOrderNumber($sCompanyId."/".date("y")."/".$hOrderNumber);
            }else{
				$objOrder->setOrderNumber($hOrderNumber);
			}
			
			$objOrder->setOrderReservePrice($hOrderReservePrice);
            $objOrder->setOrderLocation($hOrderLocation);
			
			$objOrder->setBookingCustomerId($hBookingCustomerId);
			$objOrder->setBookingNumber($hBookingNumber);
			$objOrder->setBookingSaleId($hBookingSaleId);
			$objOrder->setBookingNumberNohead($hBookingNumberNohead);
			$hBookingDate = $Year."-".$Month."-".$Day;
			$objOrder->setBookingDate($hBookingDate);
			$hBookingDate1 = $Year1."-".$Month1."-".$Day1;
			$objOrder->setBookingCashierDate($hBookingDate1);
			$hBookingCashierTime = $Hours1.":".$Minute1.":00";
			$objOrder->setBookingCashierTime($hBookingCashierTime);
			$objOrder->setBookingCashierId($sMemberId);
			$objOrder->setBookingRemark($hBookingRemark);
			$objOrder->setBookingCarType($hBookingCarModel);
			$objOrder->setBookingCarSeries($hBookingCarSeries);
			
			$objCarSeries = new CarSeries();
			$objCarSeries->setCarSeriesId($hBookingCarSeries);
			$objCarSeries->load();
			
			$objOrder->setBookingCarPrice($objCarSeries->getPrice());
			$objOrder->setOrderPrice($objCarSeries->getPrice());
			$objOrder->setBookingCarColor($hBookingCarColor);
			$objOrder->setAddBy($sMemberId);
			$objOrder->setEditBy($sMemberId);
			$objOrder->setCompanyIds($sCompanyId);
			
    		$pasrErrCustomer = $objCustomer->checkCashierBooking($hSubmit);
			$pasrErrOrder = $objOrder->checkCashierBooking($hSubmit);

			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				/*
				if($hBookingSaleId == "" OR $hBookingSaleId == 0){
					$arrName = explode(" ",trim($hSaleName));
					//check �����ҧ '  '
					if($arrName[2] != "") $arrName[1] = $arrName[2];
					
					//check �����ҧ '   '
					if($arrName[3] != "") $arrName[1] = $arrName[3];
					
					//check �����ҧ '    '
					if($arrName[4] != "") $arrName[1] = $arrName[4];	
					
					$objSale = new Member();
					if($objSale->loadByCondition(" firstname ='".$arrName[0]."'  AND lastname = '".$arrName[1]."'  ")){
						$hBookingSaleId = $objSale->getMemberId();
					}else{
						$objMember = new Member();
						$objMember->setFirstname($arrName[0]);
						$objMember->setLastname($arrName[1]);
						$objMember->setUsername(date("YmdHis"));
						$objMember->setStatus(1);
						$hBookingSaleId = $objMember->add();
					}
				}				
				*/
	            $objOrder->setBookingSaleId($hBookingSaleId);

				if ($strMode=="Update") {
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();
					
					$objCustomer->updateCashierBooking();
					
					for($i=0;$i<$hOrderPriceCount;$i++){
						$value= $_POST["hPrice_".$i]*1;
						if($value != 0){
							if($value < 0) $value= $value*-1;
							$total_value= $value+$total_value;
						}
					}
					
					if($objOrder->getBookingNumberNohead() =="" AND $total_value > 0 ){
						$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
						$objOrder->setBookingNumberNohead($strBookingNumberNohead);
						$objInfo->updateOrderNumberNoheadRecent();
					}
					$objOrder->updateCashierBooking();
				} else {
					if($hCustomerId != ""){
						$objCustomer->updateCashierBooking();
					}else{
						$hCustomerId = $objCustomer->addCashierBooking();
					}					
					$objOrder->setBookingCustomerId($hCustomerId);
					//create booking number
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();
					/*
					$strBookingNumber = $objInfo->getOrderNumber();
					$objOrder->setBookingNumber($strBookingNumber);
					$objInfo->updateOrderNumberRecent();
					*/
					
					for($i=0;$i<$hOrderPriceCount;$i++){
						$value= $_POST["hPrice_".$i]*1;
						if($value != 0){
							if($value < 0) $value= $value*-1;
							$total_value= $value+$total_value;
						}
					}
					
					if($total_value > 0 ){
						$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
						$objOrder->setBookingNumberNohead($strBookingNumberNohead);
						$objInfo->updateOrderNumberNoheadRecent();
					}
					
					$objOrder->setSendingCustomerId($hCustomerId);
					$objOrder->setSendingSaleId($hBookingSaleId);
					
					$pId = $objOrder->add();
					$hId=$pId;

				}
				
				//update order price
				for($i=0;$i<$hOrderPriceCount;$i++){
					$objOrderPrice = new OrderPrice();
					$objOrderPrice->setOrderId($hId);
					$objOrderPrice->setOrderPriceId($hOrderPriceId[$i]);
					$objOrderPrice->setPaymentSubjectId($hPaymentSubjectId[$i]);
					$objOrderPrice->setRemark($hRemark[$i]);
					$objOrderPrice->setQty($_POST["hQty_".$i]);
					$objOrderPrice->setPrice($_POST["hPrice_".$i]);
					$objOrderPrice->setVat($_POST["hVat_".$i]);
					$objOrderPrice->setPayin($_POST["hPayin_".$i]);
					$objOrderPrice->setOrderPaymentId($hOrderPaymentId[$i]);
					$objOrderPrice->setStatus(0);
					if($hOrderPriceId[$i] != ""){
						$objOrderPrice->update();
					}else{
						$objOrderPrice->add();
					}
				}
				
				
				//update order payment
				for($i=0;$i<5;$i++){
						$objOrderPayment = new OrderPayment();
						
						$objOrderPayment->setOrderId($hId);
						$objOrderPayment->setOrderPaymentId($hOrderPaymentId[$i]);
						$objOrderPayment->setPaymentTypeId($_POST["hPaymentTypeId_".$i]);
						$objOrderPayment->setPrice($_POST["hPaymentPrice_".$i]);
						$objOrderPayment->setBankId($_POST["hBankId_".$i]);
						$objOrderPayment->setBranch($_POST["hBankBranch_".$i]);
						$objOrderPayment->setCheckNo($_POST["hCheckNo_".$i]);
						$strCheckDate =  $_POST["Year01_".$i]."-".$_POST["Month01_".$i]."-".$_POST["Day01_".$i];
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(0);
						if($hOrderPaymentId[$i] != ""){
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->update();
							}else{
								$objOrderPayment->delete();
							}
						}else{
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->add();
							}
						}
					
						
					 	
				}

				unset ($objCustomer);
				unset ($objOrder);


				if($hCompanyName != "" and $hCompanyNameCode == ""){
					$objAO  = new AreaOther();
					if(!$objAO->loadByCondition(" area_type = 'company_name' and title = '".$hCompanyName."'  ")){
						$objAO->setAreaType("company_name");
						$objAO->setTitle($hCompanyName);
						$objAO->add();
					}
				}

				if($hBuilding != "" and $hBuildingCode == ""){
					$objAO  = new AreaOther();
					if(!$objAO->loadByCondition(" area_type = 'building' and title = '".$hBuilding."'  ")){
						$objAO->setAreaType("building");
						$objAO->setTitle($hBuilding);
						$objAO->add();
					}
				}
				
				if($hMooban != "" and $hMoobanCode == ""){
					$objAO  = new AreaOther();
					if(!$objAO->loadByCondition(" area_type = 'mooban' and title = '".$hMooban."'  ")){
						$objAO->setAreaType("mooban");
						$objAO->setTitle($hMooban);
						$objAO->add();
					}
				}
				
				if($hSoi != "" and $hSoiCode == ""){
					$objAO  = new AreaOther();
					if(!$objAO->loadByCondition(" area_type = 'soi' and title = '".$hSoi."'  ")){
						$objAO->setAreaType("soi");
						$objAO->setTitle($hSoi);
						$objAO->add();
					}
				}
				
				if($hRoad != "" and $hRoadCode == ""){
					$objAO  = new AreaOther();
					if(!$objAO->loadByCondition(" area_type = 'road' and title = '".$hRoad."'  ")){
						$objAO->setAreaType("road");
						$objAO->setTitle($hRoad);
						$objAO->add();
					}
				}

				header("location:bcardPrintPreview.php?hId=$hId");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)

		}
}

$pageTitle = "1. ����������ѹ";
$pageContent = "1.1  �ѹ�֡�����š�èͧö";
$strHead03 = "�ѹ�֡�����š�èͧ";
include("h_header.php");
?>
<script type="text/javascript" src="../function/check_mobile.js"></script>
<script language="JavaScript">

	function check_submit()
	{
	
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}		
	
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hBookingSaleId.value=="")
		{
			alert("��س����͡ Sale �ҡ�к�");
			document.forms.frm01.hSaleName.focus();
			return false;
		} 		
	
	
		if (document.forms.frm01.hOrderReservePrice.value=="")
		{
			alert("��س��к�ǧ�Թ�ͧ");
			document.forms.frm01.hOrderReservePrice.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if(document.forms.frm01.hIncomplete.checked == false){
			
			/*
			if (document.forms.frm01.hAddress.value=="")
			{
				alert("��س��кط������");
				document.forms.frm01.hAddress.focus();
				return false;
			} 	
		*/
			
			
			if (document.forms.frm01.hTumbonCode.value=="")
			{
				alert("��س��кصӺ�");
				document.forms.frm01.hTumbon.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hAmphurCode.value=="")
			{
				alert("��س��к������");
				document.forms.frm01.hAmphur.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hProvinceCode.value=="")
			{
				alert("��س��кبѧ��Ѵ");
				document.forms.frm01.hProvince.focus();
				return false;
			} 
			
			if (document.forms.frm01.hZipCode.value=="")
			{
				alert("��س��к�������ɳ���");
				document.forms.frm01.hZip.focus();
				return false;
			} 							

		}
		
		if ( (document.forms.frm01.hMobile_1.value=="" && document.forms.frm01.hMobile_2.value=="" && document.forms.frm01.hMobile_3.value=="" )    && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			Set_Display_Type('1');
			document.forms.frm01.hHomeTel.focus();
			return false;
		} 	

	
		if ( document.forms.frm01.hBookingCarModel.options[frm01.hBookingCarModel.selectedIndex].value==""  || document.forms.frm01.hBookingCarModel.options[frm01.hBookingCarModel.selectedIndex].value=="0" )
		{
			alert("��س����͡���ö");
			document.forms.frm01.hBookingCarModel.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hBookingCarSeries.value=="" || document.forms.frm01.hBookingCarSeries.value=="0")
		{
			alert("��س����͡Ẻö");
			document.forms.frm01.hBookingCarSeries.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hBookingCarColor.options[frm01.hBookingCarColor.selectedIndex].value=="0")
		{
			alert("��س����͡��ö");
			document.forms.frm01.hBookingCarColor.focus();
			return false;
		}	
		
		if(confirm("�س��ͧ��÷��кѹ�֡��¡���������?")){
			document.getElementById("hSubmit1").style.display = "none";
			document.frm01.submit();
			return true;
		}
		
		return false;
		
	}


	function populate01(frm01, selected, objSelect2) {
	<?
			
			$i=1;
			forEach($objCarType01->getItemList() as $objItemCarType) {
				if($i==1) $hCarType = $objItemCarType->getCarModelId();
			?>
				var individualCategoryArray<?=$objItemCarType->getCarModelId();?> = new Array(
			<?
				$objCarModelList = new CarSeriesList();
				$objCarModelList->setFilter(" car_model_id = ".$objItemCarType->getCarModelId()." AND ( expire='0000-00-00' OR expire > '".date("Y-m-d")."'  )  ");
				$objCarModelList->setPageSize(0);
				$objCarModelList->setSortDefault(" title ASC");
				$objCarModelList->load();
				$text = "";
					$text.= " \"('- ��س����͡ -')\",";
				forEach($objCarModelList->getItemList() as $objItemCarModel) {
					$text.= " \"('".$objItemCarModel->getTitle()."  / [ �Ҥ� ".number_format($objItemCarModel->getPrice())."] ')\",";
				}
				$text = substr($text,0,(strlen($text)-1));
				echo $text;
				echo ");\n";
				$i++;
			}
		?>
	
	<?
			forEach($objCarType01->getItemList() as $objItemCarType) {?>
				var individualCategoryArrayValue<?=$objItemCarType->getCarModelId();?> = new Array(
			<?
				$objCarModelList = new CarSeriesList();
				$objCarModelList->setFilter(" car_model_id = ".$objItemCarType->getCarModelId()." AND ( expire='0000-00-00' OR expire > '".date("Y-m-d")."'  )  ");
				$objCarModelList->setPageSize(0);
				$objCarModelList->setSortDefault(" title ASC");
				$objCarModelList->load();
				$text = "";
				$text.= " \"('0')\",";
				forEach($objCarModelList->getItemList() as $objItemCarModel) {
					$text.= " \"('".$objItemCarModel->getCarSeriesId()."')\",";
				}
				$text = substr($text,0,(strlen($text)-1));
				echo $text;
				echo ");\n";
			}
		?>	
		
		
	<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
		if (selected == '<?=$objItemCarType->getCarModelId()?>') 	{
	
			while (individualCategoryArray<?=$objItemCarType->getCarModelId()?>.length < objSelect2.options.length) {
				objSelect2.options[(objSelect2.options.length - 1)] = null;
			}
			for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarModelId()?>.length; i++) 	{
				eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarModelId()?>[i]);
				eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarModelId()?>[i]);			
			}
		}			
	<?}?>
	
	}

</script>

<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="bcardUpdate.php" method="POST"  onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hLock" value="<?=$hLock?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">
	  <input type="hidden" name="hCustomerId"  value="<?=$objCustomer->getCustomerId();?>">
	  <input type="hidden" name="hBookingNumberNohead" value="<?=$objOrder->getBookingNumberNohead()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">	  
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>"> 
	  <input type="hidden" name="hTrick" value="true">
	  <input type="hidden"  name="hOrderStatus" size="20"  value="<?=$objOrder->getOrderStatus()?>">

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"></td>
			<td  valign="top"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">

					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day1 value="<?=$Day1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month1 value="<?=$Month1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year1 value="<?=$Year1?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year1,Day1, Month1, Year1,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hours1 value="<?=$Hours1?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute1 value="<?=$Minute1?>"></td>									
						</tr>
						</table>
			</td>
		</tr>
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><?if($strMode == "Add"){?><?=$sCompanyId?>/<?=date("y")?>/<?}?><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">

					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
						</tr>
						</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >
				<input type="hidden" readonly name="hBookingSaleId" size="2"  value="<?=$objOrder->getBookingSaleId();?>">
				<input type="text" name="hSaleName" onKeyDown="if(event.keyCode==13 && frm01.hBookingSaleId.value != '' ) frm01.hOrderReservePrice.focus();if(event.keyCode !=13 ) frm01.hBookingSaleId.value='';" size="30"  value="<?=$hSaleName?>">
			</td>
			<td class="i_background03" valign="top"><strong>�Թ�ͧ</strong>   </td>
			<td valign="top" >			
				<input type="text" name="hOrderReservePrice" size="15"  value="<?=$objOrder->getOrderReservePrice();?>"> �ҷ
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<?if($hLock == 1){?>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="2">
		<tr>
			<td valign="top"  class="titleHeader">����-���ʡ�� :</td>
			<td valign="top"  class="titleHeader"> </td>
			<td valign="top"  class="titleHeader">�������������ó� :</td>
			<td valign="top"  class="titleHeader"></td>
		</tr>		
		<tr>
			<td valign="top" colspan="2">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>										
					<INPUT type="text" disabled size=50 value="<?=$name?>"><INPUT type="hidden" name="hCustomerName"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>		
			</td>					
			<td valign="top" colspan="2">
				<table>
				<tr>
					
					<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
					<td class="i_background03">���꡷���� ��Ң������١����������ó�</strong></td>
					<td class="i_background03"></td>
					<td></td>
				</tr>
				</table>	
			</td>
		</tr>			
		<tr>
			<td valign="top"  class="titleHeader">�Ţ��� :</td>
			<td valign="top"  class="titleHeader">����ѷ : </td>
			<td valign="top"  class="titleHeader">�Ҥ�� :</td>
			<td valign="top"  class="titleHeader">������ :</td>
		</tr>
		<tr>
			<td valign="top"><INPUT  name="hHomeNo"  size=10 value="<?=$objCustomer->getHomeNo();?>"></td>					
			<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode"  size=3 value=""><INPUT  name="hCompanyName"  size=30 value="<?=$objCustomer->getCompanyName();?>"></td>
			<td valign="top"><INPUT type="hidden"  name="hBuildingCode"  size=3 value=""><INPUT  name="hBuilding"  size=30 value="<?=$objCustomer->getBuilding();?>"></td>
			<td valign="top"><INPUT  name="hMooNo"  size=10 value="<?=$objCustomer->getMooNo();?>"></td>					
		</tr>		
		<tr>
			<td valign="top"  class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
			<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
			<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
			<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
		</tr>
		<tr>
			<td valign="top"><INPUT type="hidden"   name="hMoobanCode"  size=3 value=""><INPUT  name="hMooban"  size=30 value="<?=$objCustomer->getMooban();?>"></td>					
			<td valign="top"><INPUT type="hidden"   name="hSoiCode"  size=3 value=""><INPUT  name="hSoi"  size=30 value="<?=$objCustomer->getSoi();?>"></td>
			<td valign="top"><INPUT type="hidden"   name="hRoadCode"   size=3 value=""><INPUT  name="hRoad"   size=30 value="<?=$objCustomer->getRoad();?>"></td>
			<td>
			<input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"><br>
			<input type="text"  maxlength="50"  name="hAddress1" maxlength="50" size="50"  value="<?=$objCustomer->getAddress1();?>">
			</td>
		</tr>
		<tr>
			<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
			<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
			<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
			<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
		</tr>
		<tr>							
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="30"  value="<?=$objCustomer->getTumbon();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"></td><td><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td></tr></table></td>
		</tr>				
		<tr>
			<td class="titleHeader" valign="top">�������Ѿ���ҹ :</td>
			<td class="titleHeader" valign="top">�����÷��Դ����� 1:</td>					
			<td class="titleHeader" valign="top">�����÷��Դ����� 2:</td>
			<td class="titleHeader" valign="top">�����÷��Դ����� 3:</td>
		</tr>				
		<tr>					
			<td valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"></td>
			<td valign="top"><input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"></td>
			<td valign="top"><input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"></td>
			<td valign="top"><input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"></td>
		</tr>		
		<tr>
			<td class="titleHeader" valign="top">���Ѿ����ӧҹ :</td>
			<td class="titleHeader" valign="top">ῡ��:</td>					
			<td class="titleHeader" valign="top">������:</td>
			<td class="titleHeader" valign="top"></td>
		</tr>				
		<tr>					
			<td valign="top"><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"></td>
			<td valign="top"><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"></td>
			<td valign="top"><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td valign="top"></td>
		</tr>					
		</table>
	</td>
</tr>
</table>
<?}else{?>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td valign="top"  class="titleHeader">����-���ʡ�� :</td>
			<td valign="top"  class="titleHeader"> </td>
			<td valign="top"  class="titleHeader">�������������ó� :</td>
			<td valign="top"  class="titleHeader"></td>
		</tr>		
		<tr>
			<td valign="top" colspan="2">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>										
					<INPUT  name="hCustomerName"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>			
			</td>					
			<td valign="top" colspan="2">
				<table>
				<tr>
					
					<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
					<td class="i_background03">���꡷���� ��Ң������١����������ó�</strong></td>
					<td class="i_background03"></td>
					<td></td>
				</tr>
				</table>			
			</td>
		</tr>	
		<tr>
			<td valign="top"  class="titleHeader">�Ţ��� :</td>
			<td valign="top"  class="titleHeader">����ѷ : </td>
			<td valign="top"  class="titleHeader">�Ҥ�� :</td>
			<td valign="top"  class="titleHeader">������ :</td>
		</tr>
		<tr>
			<td valign="top"><INPUT  name="hHomeNo"  size=10 value="<?=$objCustomer->getHomeNo();?>"></td>					
			<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode"  size=3 value=""><INPUT  name="hCompanyName"  size=30 value="<?=$objCustomer->getCompanyName();?>"></td>
			<td valign="top"><INPUT type="hidden"  name="hBuildingCode"  size=3 value=""><INPUT  name="hBuilding"  size=30 value="<?=$objCustomer->getBuilding();?>"></td>
			<td valign="top"><INPUT  name="hMooNo"  size=10 value="<?=$objCustomer->getMooNo();?>"></td>					
		</tr>		
		<tr>
			<td valign="top"  class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
			<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
			<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
			<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
		</tr>
		<tr>
			<td valign="top"><INPUT type="hidden"   name="hMoobanCode"  size=3 value=""><INPUT  name="hMooban"  size=30 value="<?=$objCustomer->getMooban();?>"></td>					
			<td valign="top"><INPUT type="hidden"   name="hSoiCode"  size=3 value=""><INPUT  name="hSoi"  size=30 value="<?=$objCustomer->getSoi();?>"></td>
			<td valign="top"><INPUT type="hidden"   name="hRoadCode"   size=3 value=""><INPUT  name="hRoad"   size=30 value="<?=$objCustomer->getRoad();?>"></td>
			<td>
			<input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"><br>
			<input type="text"  maxlength="50"  name="hAddress1" maxlength="50" size="50"  value="<?=$objCustomer->getAddress1();?>">
			</td>
		</tr>
		<tr>
			<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
			<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
			<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
			<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
		</tr>
		<tr>							
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="30"  value="<?=$objCustomer->getTumbon();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td></tr></table></td>
			<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"></td><td><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td></tr></table></td>
		</tr>				
		<tr>
			<td class="titleHeader" valign="top">�������Ѿ���ҹ :</td>
			<td class="titleHeader" valign="top">�����÷��Դ����� 1:</td>					
			<td class="titleHeader" valign="top">�����÷��Դ����� 2:</td>
			<td class="titleHeader" valign="top">�����÷��Դ����� 3:</td>
		</tr>				
		<tr>					
			<td valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"></td>
			<td valign="top"><input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"></td>
			<td valign="top"><input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"></td>
			<td valign="top"><input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"></td>
		</tr>		
		<tr>
			<td class="titleHeader" valign="top">���Ѿ����ӧҹ :</td>
			<td class="titleHeader" valign="top">ῡ��:</td>					
			<td class="titleHeader" valign="top">������:</td>
			<td class="titleHeader" valign="top"></td>
		</tr>				
		<tr>					
			<td valign="top"><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"></td>
			<td valign="top"><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"></td>
			<td valign="top"><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td valign="top"></td>
		</tr>					
		</table>
	</td>
</tr>
</table>

<?}?>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö���ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="10%"><strong>���ö</strong> </td>
			<td valign="top" width="40%">
				<?=$objCarType01->printSelectScript("hBookingCarModel",$hBookingCarModel,"��س����͡","populate01(document.frm01,document.frm01.hBookingCarModel.options[document.frm01.hBookingCarModel.selectedIndex].value,document.frm01.hBookingCarSeries)");?>

			</td>		
			<td width="10%" class="i_background03"><strong>Ẻö</strong></td>			
			<td width="40%">
				<?$objCarSeriesList->printSelect("hBookingCarSeries",$hBookingCarSeries,"��س����͡");?>
			</td>		
				<?
				if($strMode == "Update"){?>
				<script>
				populate01(document.frm01,document.frm01.hBookingCarModel.options[document.frm01.hBookingCarModel.selectedIndex].value,document.frm01.hBookingCarSeries);
				document.frm01.hBookingCarSeries.value=<?=$hBookingCarSeries;?>;
				</script>				
				<?}?>
		</tr>			
		<tr>
			<td  class="i_background03"><strong>������</strong> </td>
			<td >
				<input  type="hidden" size=3  name="hBookingCarType" value="<?=$objOrder->getBookingCarType()?>">
				<select  name="hCarType">
					<option value="1" <?if($hBookingCarType == "1") echo "selected"?>>ö��
					<option value="2" <?if($hBookingCarType == "2") echo "selected"?>>ö�к�
					<option value="5" <?if($hBookingCarType == "3") echo "selected"?>>ö 6 �����
					<option value="3" <?if($hBookingCarType == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($hBookingCarType == "4") echo "selected"?>>ö���
				</select>	
			</td>		
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hBookingCarColor",$objOrder->getBookingCarColor(),"��س����͡");?>
			</td>

		</tr>			
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hBookingCarGear">
					<option value="Auto" <?if($objOrder->getBookingCarGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objOrder->getBookingCarGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03" ></td>
			<td >
				
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>

<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š����¡��</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
</tr>
<?
$i=0;
forEach($objPaymentSubjectList->getItemList() as $objItem) {
	$objOrderPrice = new OrderPrice();
	if($strMode=="Update"){
		$objOrderPrice->loadByCondition(" order_id=$hId  AND order_extra_id = 0 AND payment_subject_id = ".$objItem->getPaymentSubjectId());
	}
?>
<input type="hidden"  name="hPaymentSubjectId[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
<input type="hidden"  name="hOrderPriceId[<?=$i?>]" value="<?=$objOrderPrice->getOrderPriceId();?>">

<tr>
	<td align="center" class="ListDetail"><?=$i+1?></td>
	<td align="left" class="ListDetail"><?=$objItem->getTitle();?> <?if($objItem->getTitle() == "����"){?><input type="text"  size="30" name="hRemark[<?=$i?>]" value="<?=$objOrderPrice->getRemark();?>"><?}?></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_all();" onfocus="sum_all();"  size="5" name="hQty_<?=$i?>" value="<?if( $objOrderPrice->getQty() > 0 ){echo $objOrderPrice->getQty();}else{ echo "1";}?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onclick="sum_all()"  onblur="sum_all();" onfocus="sum_all();" name="hPayin_<?=$i?>" value="1" <?if($objOrderPrice->getPayin() == 1) echo "checked";?> ></td>
	<?$sumPrice = ($objOrderPrice->getPrice()*$objOrderPrice->getQty())?>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_all();" onfocus="sum_all();"  size="20" name="hPrice_<?=$i?>" value="<?=$objOrderPrice->getPrice();?>"></td>	
	<td align="right" class="ListDetail"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceSum_<?=$i?>" value="<?=number_format($sumPrice,2);?>"></td>
</tr>	
<?$i++;}?>
<input type="hidden"  name="hOrderPriceCount" value="<?=$i?>">				
<tr>
	<td align="center" class="listDetail" colspan="4">
		<table width="100%">
		<tr>
			<td valign="top" >�����˵�</td>
			<td><textarea name="hBookingRemark" rows="3" cols="70"><?=$objOrder->getBookingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" ><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceTotal" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>
</table>


	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail02" width="5%" align="center">�ӴѺ</td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
</tr>
<?
if($strMode=="Update"){
	$objOrderPaymentList = new OrderPaymentList();
	$objOrderPaymentList->setFilter(" order_id=$hId AND status=0 AND order_extra_id = 0 ");
	$objOrderPaymentList->load();
	$i=0;
	forEach($objOrderPaymentList->getItemList() as $objItem) {
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i",$objItem->getPaymentTypeId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value="<?=$objItem->getPrice();?>"></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i",$objItem->getBankId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value="<?=$objItem->getBranch();?>"></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value="<?=$objItem->getCheckNo();?>"></td>
	<td align="center">
		<?
		$arrDate = $objItem->getCheckDate();
		$Day01="";
		$Month01="";
		$Year01="";
		if($arrDate != "0000-00-00" or $arrDate != ""){
			$arrDate = explode("-",$arrDate);
			$Day01=$arrDate[2];
			$Month01=$arrDate[1];
			$Year01=$arrDate[0];
		}
		?>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?$i++;}?>
<?$j =  5 - $objOrderPaymentList->mCount;?>
<?$k=$i;
for($i=$k;$i<5;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}else{?>

<?$i=0;
for($i=0;$i<5;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}?>




	</td>
</tr>
</table>
<br>

<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button" name="hSubmit1" id="hSubmit1" value="�ѹ�֡��¡��" class="button"  onclick="return check_submit();" >
<?}else{?>
	<input type="button" name="hSubmit1" id="hSubmit1"  value="�ѹ�֡��¡��" class="button"  onclick="return check_submit();" >
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='bcardList.php'">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {
		return "bcardAutoCustomer.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hCompanyName", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm01&hField=hCompanyNameCode&hField01=hBuilding&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm01&hField=hBuildingCode&hField01=hMooNo&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm01&hField=hMoobanCode&hField01=hSoi&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm01&hField=hSoiCode&hField01=hRoad&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm01&hField=hRoadCode&hField01=hTumbon&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {
		return "bcardAutoOrderNumber.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	new CAPXOUS.AutoComplete("hCarSeries", function() {
		return "bcardAutoCarSeries.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "bcardAutoSale.php?q=" + this.text.value;
	});	
	
	
	function sum_all(){
	sum = 0;
	<?
	$i=0;
	forEach($objPaymentSubjectList->getItemList() as $objItem) {?>
		hPrice_<?=$i?> = 0;
		hQty_<?=$i?> = 0;
		
		if(document.frm01.hPrice_<?=$i?>.value != "" && document.frm01.hPrice_<?=$i?>.value != "0"){ hPrice_<?=$i?>= (document.frm01.hPrice_<?=$i?>.value*1);}		
		if(document.frm01.hQty_<?=$i?>.value != "" && document.frm01.hQty_<?=$i?>.value != "0"){ hQty_<?=$i?>= (document.frm01.hQty_<?=$i?>.value*1);}		
		
		if(document.frm01.hPayin_<?=$i?>.checked == true){
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>*-1;
			sum= sum-(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}else{
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>;
			sum= sum+(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}
	<?$i++;}?>
	
		document.frm01.hPriceTotal.value = sum;
	
	}
	
	sum_all();
</script>
<?
	include("h_footer.php")
?>
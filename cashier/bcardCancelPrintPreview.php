<?
include("common.php");
require_once('../include/numtoword.php');

$resultinwords=new numtoword;
$objCustomer = new Customer();
$objOrder = new Order();
$objOrderPayment = new OrderPayment();
$objOrderPrice = new OrderPrice();
$objCarSeries = new CarSeries();

if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getOrderDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$objPaymentList = new OrderPaymentList();
		$objPaymentList->setFilter(" order_id = $hId  AND status = 0");
		$objPaymentList->setPageSize(0);
		$objPaymentList->setSortDefault(" order_payment_id ASC ");
		$objPaymentList->load();

		$objPriceList = new OrderPriceList();
		$objPriceList->setFilter(" OP.order_id = $hId  AND status = 0");
		$objPriceList->setPageSize(0);
		$objPriceList->setSortDefault(" order_price_id ASC ");
		$objPriceList->load();
		
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();		
		
} else {
	header("location:error.php?hErrMsg='��辺����㺨ͧ'");
	exit;
}



$pageTitle = "1. ����������ѹ";
$pageContent = "1.5 �ѹ�֡�����š�ä׹�ͧ > ����������";
$strHead03 = "���������稤׹�ͧ";
include("h_header.php");
?>
<br>
<table width="700" cellpadding="0" cellpadding="0" class="search" align="center">
<tr>
	<td>

<table width="100%" cellpadding="0" cellpadding="0">
<tr>
	<td width="70%" height="50">
	</td>
	<td align="center">
			<span class="Head">��������´��ê����Թ</span>&nbsp;
	</td>
</tr>
</table>
<table width="100%">
<tr  class="i_Excel">
	<td rowspan="2" class="i_Excel">
		<table width="100%">
		<tr>
			<td width="17%" class="small" valign="top">����١���<br>Customer Name</td>
			<td width="54%" valign="top"><?=$objCustomer->getCustomerTitleIdDetail().$objCustomer->getFirstname()."  ".$objCustomer->getLastname()?></td>
			<td width="15%" class="small" valign="top">�����١���</td>
			<td width="15%" valign="top"></td>
		</tr>
		<tr>
			<td class="small" valign="top">�������<br>Address</td>
			<td class="small" valign="top">
				<?=$objCustomer->getAddressLabel()?>
				<?if($objCustomer->getTumbon() != "") echo  "�Ӻ�/ࢵ ".$objCustomer->getTumbon();?>
				<?if($objCustomer->getAmphur() != "") echo  "�����/�ǧ ".$objCustomer->getAmphur();?>
				<?if($objCustomer->getProvince() != "") echo  "�ѧ��Ѵ ".$objCustomer->getProvince();?>
				<?if($objCustomer->getZip() != "") echo  "������ɳ��� ".$objCustomer->getZip();?>												
			</td>
			<td class="small" valign="top">����¹����ᴧ</td>
			<td class="small" valign="top"></td>
		</tr>
		<tr>
			<td class="small" valign="top">��������´�Թ���</td>
			<td colspan="3">
				<table width="100%">
				<tr>
					<td width="15%" class="small">Model</td>
					<td width="20%"><?=$objCarSeries->getCarModelTitle()?></td>
					<td width="15%"class="small">Ẻö</td>
					<td width="20%"><?=$objCarSeries->getTitle()?></td>
					<td width="10%"class="small">��</td>
					<td width="20%"><?=$objCarColor->getTitle()?></td>
				</tr>
				<tr>
					<td class="small">�Ţ����ͧ</td>
					<td></td>
					<td class="small">�Ţ��Ƕѧ</td>
					<td colspan="3"></td>
				</tr>
				</table>
	
			
			</td>
		</tr>
		</table>	
	</td>
	<td align="center"  class="i_Excel" valign="top">
		<table width="100%">
		<tr>
			<td align="center" class="small">�ѹ���</td>
		</tr>
		<tr>
			<td align="center"><?=$objOrder->getBookingDate();?></td>
		</tr>
		</table>	
	</td>
	<td  align="center"  class="i_Excel" valign="top">
		<table width="100%">
		<tr>
			<td align="center" class="small">�Ţ���</td>
		</tr>
		<tr>
			<td align="center"><?=$objOrder->getBookingCancelNumber();?></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="center" width="15%"  class="i_Excel" valign="top">
		<table width="100%">
		<tr>
			<td align="center" class="small">���;�ѡ�ҹ���</td>
		</tr>
		<?
		$objMember = new Member();
		$objMember->setMemberId($objOrder->getBookingSaleId());
		$objMember->load();
		
		?>
		<tr>
			<td align="center"><?=$objMember->getFirstname()."  ".$objMember->getLastname();?></td>
		</tr>
		</table>		
	
	</td>
	<td align="center" width="15%"  class="i_Excel" valign="top">
		<table width="100%">
		<tr>
			<td align="center" class="small">�Ţ������觨ͧ</td>
		</tr>
		<tr>
			<td align="center"><?=$objOrder->getOrderNumber();?></td>
		</tr>
		</table>		
	
	</td>
</tr>
</table>

<table width="100%" cellpadding="1" cellspacing="0">
<tr>
	<td class="i_blueSmallHeader" align="center" width="5%">�ӴѺ<br>Item</td>
	<td class="i_blueSmallHeader" align="center" width="45%">��������´<br>Description</td>
	<td class="i_blueSmallHeader" align="center" width="10%">�ӹǹ<br>Quantity</td>
	<td class="i_blueSmallHeader" align="center" width="20%">˹�����<br>Unit Price</td>
	<td class="i_blueSmallHeader" align="center" width="20%">�ӹǹ�Թ<br>Amount</td>
</tr>
<tr>
	<td colspan="5" background="../images/bgBill.gif" height="150" valign="top">
		<table width="100%" cellpadding="0" cellspacing="3">
		<tr >
			<td width="5%" align="center" valign="top" >1.</td>
			<td width="45%"  valign="top">�Թ�׹�ͧ</td>
			<td width="10%"  align="right"  valign="top">1</td>
			<td width="20%"  align="right"  valign="top"><?=number_format($objOrder->getBookingCancelPrice())?></td>
			<td width="20%"  align="right"  valign="top"><?=number_format($objOrder->getBookingCancelPrice(),2)?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="3" bgcolor="#BDDCF4" >
		<table width="100%">
		<tr>
			<td class="small">�ӹǹ�Թ<br>Amount</td>
			<td ><?currencyThai($objOrder->getBookingCancelPrice());?>&nbsp;�ҷ��ǹ</td>
		</tr>
		</table>
	</td>
	<td class="i_Excel"><span class="small">����Թ<br>Sub Total</span></td>
	<td align="right" class="i_Excel"><?=number_format($objOrder->getBookingCancelPrice(),2);?></td>
</tr>
<tr>
	<td rowspan="2" colspan="3" class="error" valign="top">
		�����˵� : <?=$objOrder->getBookingCancelReason()?>
	</td>
	<td class="i_Excel"><span class="small">�ʹ�ط��<br>Net Total</span></td>
	<td class="i_Excel" align="right"><?=number_format($objOrder->getBookingCancelPrice(),2);?></td>
</tr>
<tr>
	<td class="i_Excel" align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center">..................................</td>
		</tr>
		<tr>
			<td class="small"   align="center">����Ѻ�Թ/Collector</td>
		</tr>
		</table>

	</td>
	<td class="i_Excel" align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center">..................................</td>
		</tr>
		<tr>
			<td class="small"  align="center">���ºѭ��/Account Dept.</td>
		</tr>
		</table>	
	</td>
</tr>
</table>



</td>
</tr>
</table>
<table width="100%" cellpadding="10">
<tr>
	<td align="center"><input type="button" name="hCheckNo" value="���١��ͧ��Ѻ����" class="button" onclick="window.location='bcardCancelUpdate.php?hId=<?=$hId?>'">&nbsp;&nbsp;<input type="button" name="hCheckYes"  class="button"  value="�١��ͧ����¡�ä׹�ͧ����" onclick="window.location='bcardCancelUpdate.php'">&nbsp;&nbsp;<input type="button" name="hCheckYes"  class="button"  value="�١��ͧ�����������Ѻ�Թ"  onclick="window.location='bcardCancelPrint.php?hId=<?=$hId?>'"></td>
</tr>
</table>


<?
	include("h_footer.php")
?>
<?
include("common.php");
require_once('../include/numtoword.php');

$resultinwords=new numtoword;
$objCustomer = new Customer();
$objOrder = new Order();
$objOrderPayment = new OrderPayment();
$objOrderPrice = new OrderPrice();
$objCarSeries = new CarSeries();

if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getOrderDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$objPaymentList = new OrderPaymentList();
		$objPaymentList->setFilter(" order_id = $hId  AND order_extra_id = 0 AND status = 0");
		$objPaymentList->setPageSize(0);
		$objPaymentList->setSortDefault(" order_payment_id ASC ");
		$objPaymentList->load();

		$objPriceList = new OrderPriceList();
		$objPriceList->setFilter(" OP.order_id = $hId AND order_extra_id = 0  AND status = 0");
		$objPriceList->setPageSize(0);
		$objPriceList->setSortDefault(" order_price_id ASC ");
		$objPriceList->load();
		
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();		
		
} else {
	header("location:error.php?hErrMsg='��辺����㺨ͧ'");
	exit;
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title></title>
	<title>Toyota Buzz  : Cashier System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_print.css">
	<SCRIPT language=javascript src="../function/functions.js" type=text/javascript></SCRIPT>
	<link rel="stylesheet" type="text/css" href="autocomplete.css"/>
	<script type="text/javascript" src="prototype.js"></script>
	<script type="text/javascript" src="autocomplete.js"></script>	
	
</head>

<body>
<table width=770  cellpadding="0" cellspacing="0" border="0">
<tr>
	<td valign="top">
<table width="100%" cellpadding="0" cellpadding="0" border=0>
<tr>
	<td width="70%" height=20 valign="top">
		<table align="right">
		<tr>
			<td></td>
			<td align="right" valign="top">
				<span class="Head">��������´��ê����Թ</span>&nbsp;
			
			</td>
		</tr>
		</table>	
	</td>
</tr>
<tr>
	<td width="70%" height=20 valign="top">
		&nbsp;
	</td>
	<td align="right">
		
	</td>
</tr>
</table>
<table width="100%" border=0>
<tr  >
	<td rowspan="2" width=80% valign=top >
	<?
		if($objCustomer->getTitle() != "˨�" AND $objCustomer->getTitle() != "���" ){
			$title = $objCustomer->getCustomerTitleIdDetail01();
		}else{
			$title = $objCustomer->getTitle();
		}
		?>
		<table width="100%" border="0">
		<tr>
			<td width="10%"  valign="top"></td>
			<td width="60%" valign="top" height=20><?=$title." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname()?></td>
			<td width="15%"  valign="top"></td>
			<td width="15%" valign="top"></td>
		</tr>
		<tr>
			<td  valign="top"></td>
			<td  valign="top" colspan=3 height=25>
				<?=$objCustomer->getAddressLabel()?>		
				<?if($objCustomer->getTumbon() != "") echo  "�Ӻ�/�ǧ ".$objCustomer->getTumbon();?>
				<?if($objCustomer->getAmphur() != "") echo  "�����/ࢵ ".$objCustomer->getAmphur();?>
				<?if($objCustomer->getProvince() != "") echo  "�ѧ��Ѵ ".$objCustomer->getProvince();?>
				<?if($objCustomer->getZip() != "") echo  "������ɳ��� ".$objCustomer->getZip();?>												
			</td>
		</tr>
		<tr>
			<td  valign="top"></td>
			<td colspan="3" valign="top">
				<table width="100%">
				<tr>
					<td width="15%" valign="top" ></td>
					<td  valign="top" ><?=$objCarSeries->getCarModelTitle()?></td>
					<td  valign="top" >Ẻö</td>
					<td  valign="top" ><?=$objCarSeries->getTitle()?></td>
					<td  valign="top" >��</td>
					<td  valign="top" ><?=$objCarColor->getTitle()?></td>
				</tr>
				</table>
	
			
			</td>
		</tr>
		</table>	
	</td>
	<td align="center"   valign="top" width="12%">
		<?$arrDate = explode("-",$objOrder->getBookingCashierDate());?>
		<table width="100%" border=0>
		<tr>
			<td align="center" ><br></td>
		</tr>
		<tr>
			<td align="center" class=small><?=$arrDate[2]."-".$arrDate[1]."-".$arrDate[0];?></td>
		</tr>
		</table>	
	</td>
	<td  align="center"   valign="top" width="8%">
		<table width="100%">
		<tr>
			<td align="center" ><br></td>
		</tr>
		<?
		$code = $objOrder->getBookingNumberNohead();
		$code1= substr($code,0,2);
		$code2 = substr($code,2,strlen($code));
		
		?>
		<tr>
			<td align="center" ><span class="tah_15"><?=$code1?></span><span class="small"><?=$code2?></span></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td  valign="top">
		<table width="100%">
		<tr>
			<td align="center"  height=25><br></td>
		</tr>
		<?
		$objMember = new Member();
		$objMember->setMemberId($objOrder->getBookingSaleId());
		$objMember->load();
		
		?>
		<tr>
			<td align="center" class=small><?=$objMember->getFirstname()."  ".$objMember->getLastname();?></td>
		</tr>
		</table>		
	
	</td>
	<td valign="top">
		<table width="100%">
		<tr>
			<td align="center"  height=25><br></td>
		</tr>
		<tr>
			<td align="center"><?=$objOrder->getOrderNumber();?></td>
		</tr>
		</table>		
	
	</td>
</tr>
</table>
<table>
<tr>
	<td height=25>&nbsp;</td>
</tr>
</table>

<table width="100%" cellpadding="1" cellspacing="0" align=center border=0>
<tr>
	<td class="i_blueSmallHeader" align="center" width="10%"></td>
	<td class="i_blueSmallHeader" align="center" width="50%"></td>
	<td class="i_blueSmallHeader" align="center" width="10%"></td>
	<td class="i_blueSmallHeader" align="center" width="15%"></td>
	<td class="i_blueSmallHeader" align="center" width="15%"></td>
</tr>
<tr>
	<td colspan="5" height="230" valign="top">
		<table width="100%" cellpadding="0" cellspacing="3">
		<?
		$i=0;
		forEach($objPriceList->getItemList() as $objItem) {
		if($objItem->getPrice() > 0){?>
		<tr >
			<td width="10%" align="center" valign="top" ><?=$i+1?></td>
			<td width="50%"  valign="top"><?=$objItem->getPaymentSubjectTitle()?> <?=$objItem->getRemark()?>&nbsp;</td>
			<td width="10%"  align="right"  valign="top"><?=$objItem->getQty()?>&nbsp;&nbsp;</td>
			<td width="15%"  align="right"  valign="top"><?if($objItem->getPayin() == 1){echo "-";}?><?=number_format($objItem->getPrice(),2)?>&nbsp;&nbsp;&nbsp;</td>
			<?$sumPrice = ($objItem->getPrice()*$objItem->getQty())?>
			<td width="15%"  align="right"  valign="top"><?if($objItem->getPayin() == 1){echo "-";}?><?=number_format( $sumPrice,2)?>&nbsp;&nbsp;&nbsp;</td>
		</tr>
				<?
					if($objItem->getVat() == 1){
						if($objItem->getPayin() == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPrice;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPrice;
						}
					}else{
						if($objItem->getPayin() == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPrice;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPrice;
						}
						
					}

				$i++;
				
				}
				?>
		
		<?}
				$grandTotal = $hPriceIncludeVat+$hPriceExcludeVat;
				if($hPriceIncludeVat > 0){
					$totalVat = $hPriceIncludeVat/1.07;
					$vat = $hPriceIncludeVat-$totalVat;
				}
				$totalVat = $totalVat+$hPriceExcludeVat;		
		?>
		</table>
	</td>
</tr>
<tr>
	<td colspan="3" >
		<table width="100%">
		<tr>
			<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td ><?currencyThai($grandTotal);?>&nbsp;�ҷ��ǹ</td>
		</tr>
		</table>

	</td>
	<td ></td>
	<td align="right" height=25 ><?=number_format($totalVat,2);?>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td rowspan="3" colspan="3" valign="top"><br>
		<table width="95%" align="center">
		<tr>
			<td   >�ٻẺ��ê����Թ</td>
			<td    >��Ҥ��</td>
			<td    >�Ң�</td>
			<td   >�Ţ�����</td>
			<td   >�ѹ���</td>
			<td  align="right"   >�ӹǹ�Թ</td>
		</tr>
		<?
		$i=0;
		forEach($objPaymentList->getItemList() as $objItem) {
		if($objItem->getPrice() > 0){
		?>
		<tr >
			<td ><?=$objItem->getPaymentTypeTitle()?>&nbsp;</td>
			<td  align="left"><?=$objItem->getBankTitle()?></td>
			<td  align="left"><?=$objItem->getBranch()?></td>
			<td  align="center"><?=$objItem->getCheckNo()?></td>
			<td  align="center"><?if($objItem->getCheckDate() != "0000-00-00") echo $objItem->getCheckDate();?></td>
			<td  align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>
		<?}
		}?>
		</table>
	
	
	
	</td>
	<td ></td>
	<td  align="right" height=20 ><?=number_format($vat,2);?>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td ></td>
	<td  align="right" height=20 ><?=number_format($grandTotal,2);?>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
	<td  align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center"></td>
		</tr>
		<tr>
			<td    align="center"></td>
		</tr>
		</table>

	</td>
	<td  align="center">
		<table>
		<tr>
			<td height="40" valign="bottom"  align="center"></td>
		</tr>
		<tr>
			<td   align="center"></td>
		</tr>
		</table>	
	</td>
</tr>
</table>

</td>
</tr>
</table>
</body>
</html>
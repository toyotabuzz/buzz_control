<?
include("conf.php");

include(PATH_CLASS."clUser.php");
include(PATH_CLASS."clMember.php");
include(PATH_CLASS."clModule.php");
include(PATH_CLASS."clMemberModule.php");
include(PATH_CLASS."utilityFunction.php");

session_save_path ("");
session_start();

$objUser = new Member();

track_member_log($sMemberId,URL_BASE,"logout",$REMOTE_ADDR, $SCRIPT_NAME );
$objUser->logout();
header("Location: index.php");
exit;

?>
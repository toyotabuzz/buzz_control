<?
include("common.php");

$objCustomer = new Customer();

$objCustomerBList = new CustomerList();
$objCustomerBList->setFilter(" type_id = 'B' ");
$objCustomerBList->setPageSize(0);
$objCustomerBList->load();

$yearStart = date("Y")."-01-01";
$yearEnd = date("Y")."-12-31";

$objCustomerBYearList = new CustomerList();
$objCustomerBYearList->setFilter(" type_id = 'B'  AND ( add_date >= '$yearStart' AND add_date <= '$yearEnd' ) ");
$objCustomerBYearList->setPageSize(0);
$objCustomerBYearList->load();

$monthStart  = date("Y-m")."-01";
$monthEnd  = date("Y-m")."-31";

$objCustomerBMonthList = new CustomerList();
$objCustomerBMonthList->setFilter(" type_id = 'B' AND ( add_date >= '$monthStart' AND add_date <= '$monthEnd' ) ");
$objCustomerBMonthList->setPageSize(0);
$objCustomerBMonthList->load();

$objCustomerBDayList = new CustomerList();
$objCustomerBDayList->setFilter(" type_id = 'B' AND add_date >= '".date("Y-m-d")." 00:00:00' ");
$objCustomerBDayList->setPageSize(0);
$objCustomerBDayList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 1 or type_id=2");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate1 = explode("-",$objOrder->getBookingCashierDate());
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
	
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" order_id = $hId  AND status = 0");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 0");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" order_price_id ASC ");
		$objOrderPriceList->load();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		
	} else {
		$strMode="Add";
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate1 = explode("-",date("Y-m-d"));
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
		
		
		
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hCustomerId);
            $objCustomer->setTypeId("B");
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard(1);
			$objCustomer->setCCard($hCCard);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			$arrName = explode(" ",$hCustomerName);
			$objCustomer->setFirstname(trim($arrName[0]));
			if($arrName[2] != "") $arrName[1] = trim($arrName[1]." ".$arrName[2]);
			$objCustomer->setlastname($arrName[1]);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setIncomplete($hIncomplete);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			
            $objOrder->setOrderId($hOrderId);       
			$objOrder->setOrderStatus($hOrderStatus);
            $objOrder->setOrderNumber($hOrderNumber);
            $objOrder->setOrderPrice($hOrderPrice);
			$objOrder->setOrderReservePrice($hOrderReservePrice);
            $objOrder->setOrderLocation($hOrderLocation);
			
			$objOrder->setBookingCustomerId($hBookingCustomerId);
			$objOrder->setBookingNumber($hBookingNumber);
			$objOrder->setBookingNumberNohead($hBookingNumberNohead);
			$hBookingDate = $Year."-".$Month."-".$Day;
			$objOrder->setBookingDate($hBookingDate);
			$hBookingDate1 = $Year1."-".$Month1."-".$Day1;
			$objOrder->setBookingCashierDate($hBookingDate1);
			$objOrder->setBookingRemark($hBookingRemark);
			$objOrder->setBookingCarType($hBookingCarType);
			$objOrder->setBookingCarSeries($hBookingCarSeries);
			$objOrder->setBookingCarColor($hBookingCarColor);
			$objOrder->setAddBy($sMemberId);
			$objOrder->setEditBy($sMemberId);
			
			//Update Price
			if($hUpdatePrice){
					if ($strMode == "Update"){
						$objOrderPrice = new OrderPrice();
					    for ($i = 0; $i < $hCountTotal; $i++) {
					        if ( $hDelete[$i] )  //Delete is checked.
					        {
								$objOrderPrice->setOrderPriceId($hDelete[$i]);
								$objOrderPrice->delete();
							}else{
								$objOrderPrice->setOrderPriceId($hOrderPriceIdTemp[$i]);
								$objOrderPrice->setPaymentSubjectId($hPaymentSubjectIdTemp[$i]);
								$objOrderPrice->setOrderId($hId);
								$objOrderPrice->setRemark($hRemarkTemp[$i]);
								$objOrderPrice->setQty($hQtyTemp[$i]);
								$objOrderPrice->setPrice($hPriceTemp[$i]);
								$objOrderPrice->setVat($hVatTemp[$i]);
								$objOrderPrice->setPayin($hPayinTemp[$i]);
								$objOrderPrice->setOrderPaymentId($hOrderPaymentIdTemp[$i]);
								$objOrderPrice->setStatus(0);
								$objOrderPrice->update();
							}// end if
							
					    }// end for
						unset($objOrderPrice);
						header("location:bcardUpdate.php?hId=$hId");
						exit;
					}else{
						$arrPaymentSubjectId  = explode(",::",$sPaymentSubjectId);
						$arrRemark = explode(",::",$sRemark);
						$arrQty = explode(",::",$sQty);
						$arrPrice = explode(",::",$sPrice);
						$arrVat = explode(",::",$sVat);
						$arrPayin = explode(",::",$sPayin);
						$arrOrderPaymentId = explode(",::",$sOrderPaymentId);

					    for ($i = 0; $i < sizeof($arrPaymentSubjectId);$i++) {
				        	if ( !$hDelete[$i] ){  //Delete is checked.
								$tPaymentSubjectId = $tPaymentSubjectId.",::".$hPaymentSubjectIdTemp[$i];
								$tRemark = $tRemark.",::".$hRemarkTemp[$i];
								$tQty = $tQty.",::".$hQtyTemp[$i];
								$tPrice= $tPrice.",::".$hPriceTemp[$i];
								$tVat= $tVat.",::".$hVatTemp[$i];
								$tPayin= $tPayin.",::".$hPayinTemp[$i];
								$tOrderPaymentId= $tOrderPaymentId.",::".$hOrderPaymentIdTemp[$i];
							}// end if 
				    	}// end for
					}//end if 74
					
					if ($tPaymentSubjectId != ""){
						$tPaymentSubjectId = substr($tPaymentSubjectId,3,strlen($tPaymentSubjectId));
						$tRemark = substr($tRemark,3,strlen($tRemark));
						$tQty = substr($tQty,3,strlen($tQty));
						$tPrice = substr($tPrice,3,strlen($tPrice));
						$tVat = substr($tVat,3,strlen($tVat));
						$tPayin = substr($tPayin,3,strlen($tPayin));
						$tOrderPaymentId = substr($tOrderPaymentId,3,strlen($tOrderPaymentId));
						$sPaymentSubjectId = $tPaymentSubjectId;
						$sRemark = $tRemark;
						$sQty = $tQty;
						$sPrice = $tPrice;
						$sVat = $tVat;
						$sPayin = $tPayin;
						$sOrderPaymentId = $tOrderPaymentId;
					}else{
						$sPaymentSubjectId="";
						$sRemark="";
						$sQty="";
						$sPrice="";
						$sVat="";
						$sPayin="";
						$sOrderPaymentId="";
					}

			}
			
			if($hAddPrice){
				//Check error
				
				if ($hPaymentSubjectId == "")$asrErr["PaymentSubjectId"]="��س����͡��¡��";
				if ($hQty == ""  )$asrErr["Qty"]="��س��кبӹǹ";
				if ($hPrice == "")$asrErr["Price"]="��س��к��Ҥ�";
				if (sizeof($asrErr) == 0 ){
						$arrPaymentSubjectId = explode("::",$hPaymentSubjectId);
						$hPaymentSubjectId = $arrPaymentSubjectId[0];
				// Add account Id with out session
					if ($strMode == "Update"){
						$objOrderPrice = new OrderPrice();
						$objOrderPrice->setOrderId($hId);
						$objOrderPrice->setPaymentSubjectId($arrPaymentSubjectId[0]);
						$objOrderPrice->setRemark($hRemark);
						$objOrderPrice->setQty($hQty);
						$objOrderPrice->setPrice($hPrice);
						$objOrderPrice->setVat($hVat);
						$objOrderPrice->setPayin($hPayin);
						$objOrderPrice->setPayin($hOrderPaymentId);
						$objOrderPrice->setStatus(0);
						$objOrderPrice->add();
						unset($objOrderPrice);
						header("location:bcardUpdate.php?hId=$hId");
						exit;
					}else{
						// Add account Id
						if ($sPaymentSubjectId == ""){
							$sPaymentSubjectId = $hPaymentSubjectId;
							$sRemark = $hRemark;
							$sQty = $hQty;
							$sPrice = $hPrice;
							$sVat = $hVat;
							$sPayin = $hPayin;
							$sOrderPaymentId = $hOrderPaymentId;
						}else{
							$sPaymentSubjectId = $sPaymentSubjectId.",::".$hPaymentSubjectId;
							$sRemark = $sRemark.",::".$hRemark;
							$sQty = $sQty.",::".$hQty;
							$sPrice = $sPrice.",::".$hPrice;
							$sVat = $sVat.",::".$hVat;
							$sPayin = $sPayin.",::".$hPayin;
							$sOrderPaymentId = $sOrderPaymentId.",::".$hOrderPaymentId;
							$hPaymentSubjectId = "";
						}// end if
					}// end check strmode
					$hPaymentSubjectId = "";
				}// end check length
			
			}

			//Update Payment
			if($hUpdatePayment){
					if ($strMode == "Update"){
						$objOrderPayment = new OrderPayment();
					    for ($i = 0; $i < $hCountTotalPayment; $i++) {
					        if ( $hDelete[$i] )  //Delete is checked.
					        {
								$objOrderPayment->setOrderPaymentId($hDelete[$i]);
								$objOrderPayment->delete();
							}else{
								$objOrderPayment->setOrderPaymentId($hOrderPaymentIdTemp[$i]);
								$objOrderPayment->setPaymentTypeId($hPaymentTypeIdTemp[$i]);
								$objOrderPayment->setPrice($hPaymentPriceTemp[$i]);
								$objOrderPayment->setOrderId($hId);
								$objOrderPayment->setBankId($hBankIdTemp[$i]);
								$objOrderPayment->setBranch($hBankBranchTemp[$i]);
								$objOrderPayment->setCheckNo($hCheckNoTemp[$i]);
								$strCheckDate =  $_POST["Year01Temp$i"]."-".$_POST["Month01Temp$i"]."-".$_POST["Day01Temp$i"];
								$objOrderPayment->setCheckDate($strCheckDate);
								$objOrderPayment->setStatus(0);
								$objOrderPayment->update();
								
								for ($j = 0; $j < $hCountTotalPricePayment; $j++) {
									if($hPricePayment[$i][$j]){
										if($hOrderPricePaymentId[$i][$j] == 0 OR $hOrderPricePaymentId[$i][$j] == "" ){
											$objOrderPricePayment = new OrderPricePayment();
											$objOrderPricePayment->setOrderId($hId);
											$objOrderPricePayment->setOrderPriceId($hOrderPriceIdTemp01[$i][$j]);
											$objOrderPricePayment->setOrderPaymentId($hOrderPaymentIdTemp[$i]);
											$objOrderPricePayment->add();
										}
									}else{
										if($hOrderPricePaymentId[$i][$j] > 0){
											$objOrderPricePayment = new OrderPricePayment();
											$objOrderPricePayment->setOrderPricePaymentId($hOrderPricePaymentId[$i][$j]);
											$objOrderPricePayment->delete();
										}
									}
								}
								
								
							}// end if
							
					    }// end for
						unset($objOrderPayment);
						header("location:bcardUpdate.php?hId=$hId");
						exit;
					}else{
						$arrPaymentTypeId  = explode(",::",$sPaymentTypeId);
						$arrPaymentPrice  = explode(",::",$sPaymentPrice);
						$arrBankId  = explode(",::",$sBankId);
						$arrBankBranch  = explode(",::",$sBankBranch);
						$arrCheckNo  = explode(",::",$sCheckNo);
						$arrDay01  = explode(",::",$sDay01);
						$arrMonth01  = explode(",::",$sMonth01);
						$arrYear01  = explode(",::",$sYear01);

					    for ($i = 0; $i < sizeof($arrPaymentTypeId);$i++) {
				        	if ( !$hDelete[$i] ){  //Delete is checked.
								$tPaymentTypeId = $tPaymentTypeId.",::".$hPaymentTypeIdTemp[$i];
								$tPaymentPrice = $tPaymentPrice.",::".$hPaymentPriceTemp[$i];
								$tBankId = $tBankId.",::".$hBankIdTemp[$i];
								$tBankBranch = $tBankBranch.",::".$hBankBranchTemp[$i];
								$tCheckNo = $tCheckNo.",::".$hCheckNoTemp[$i];
								$tDay01 = $tDay01.",::".$hDay01Temp[$i];
								$tMonth01 = $tMonth01.",::".$hMonth01Temp[$i];
								$tYear01 = $tYear01.",::".$hYear01Temp[$i];
							}// end if 
				    	}// end for
					}//end if 74
					
					if ($tPaymentTypeId != ""){
						$tPaymentTypeId = substr($tPaymentTypeId,3,strlen($tPaymentTypeId));
						$tPaymentPrice = substr($tPaymentPrice,3,strlen($tPaymentPrice));
						$tBankId = substr($tBankId,3,strlen($tBankId));
						$tBankBranch = substr($tBankBranch,3,strlen($tBankBranch));
						$tCheckNo = substr($tCheckNo,3,strlen($tCheckNo));
						$tDay01 = substr($tDay01,3,strlen($tDay01));
						$tMonth01 = substr($tMonth01,3,strlen($tMonth01));
						$tYear01 = substr($tYear01,3,strlen($tYear01));
						$sPaymentTypeId = $tPaymentTypeId;
						$sPaymentPrice = $tPaymentPrice;
						$sBankId = $tBankId;
						$sBankBranch = $tBankBranch;
						$sCheckNo = $tCheckNo;
						$sDay01 = $tDay01;
						$sMonth01 = $tMonth01;
						$sYear01 = $tYear01;
					}else{
						$sPaymentTypeId="";
						$sPaymentPrice="";
						$sBankId="";
						$sBankBranch="";
						$sCheckNo="";
						$sDay01="";
						$sMonth01="";
						$sYear01="";
					}

			}
			
			if($hAddPayment){
				//Check error
				
				if ($hPaymentTypeId == "")$asrErr["PaymentTypeId"]="��س����͡��¡��";
				if ($hPaymentPrice == ""  )$asrErr["PaymentPrice"]="��س��кبӹǹ�Թ";
				
				if (sizeof($asrErr) == 0 ){
						$arrPaymentTypeId = explode("::",$hPaymentTypeId);
						$hPaymentTypeId = $arrPaymentTypeId[0];
				// Add account Id with out session
					if ($strMode == "Update"){
						$objOrderPayment = new OrderPayment();
						$objOrderPayment->setOrderId($hId);
						$objOrderPayment->setPaymentTypeId($arrPaymentTypeId[0]);
						$objOrderPayment->setPrice($hPaymentPrice);
						$objOrderPayment->setBankId($hBankId);
						$objOrderPayment->setBranch($hBankBranch);
						$objOrderPayment->setCheckNo($hCheckNo);
						$strCheckDate =  $Year01."-".$Month01."-".$Day01;
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(0);
						$objOrderPayment->add();
						unset($objOrderPayment);
						header("location:bcardUpdate.php?hId=$hId");
						exit;
					}else{
						// Add account Id
						if ($sPaymentTypeId == ""){
							$sPaymentTypeId = $hPaymentTypeId;
							$sPaymentPrice = $hPaymentPrice;
							$sBankId = $hBankId;
							$sBankBranch = $hBankBranch;
							$sCheckNo = $hCheckNo;
							$sDay01 = $Day01;
							$sMonth01 = $Month01;
							$sYear01 = $Year01;

						}else{
							$sPaymentTypeId = $sPaymentTypeId.",::".$hPaymentTypeId;
							$sPaymentPrice = $sPaymentPrice.",::".$hPaymentPrice;
							$sBankId = $sBankId.",::".$hBankId;
							$sBankBranch = $sBankBranch.",::".$hBankBranch;
							$sCheckNo = $sCheckNo.",::".$hCheckNo;
							$sDay01 = $sDay01.",::".$Day01;
							$sMonth01 = $sMonth01.",::".$Month01;
							$sYear01 = $sYear01.",::".$Year01;
							$hPaymentTypeId = "";
						}// end if
					}// end check strmode
					$hPaymentTypeId = "";
				}// end check length
			
			}			
			
			
    		$pasrErrCustomer = $objCustomer->checkCashierBooking($hSubmit);
			$pasrErrOrder = $objOrder->checkCashierBooking($hSubmit);

			if (!$hAddPrice and !$hUpdatePrice and !$hAddPayment and !$hUpdatePayment){
			
			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

			
				if($hBookingSaleId == "" OR $hBookingSaleId == 0){
					$arrName = explode(" ",trim($hSaleName));
					//check �����ҧ '  '
					if($arrName[2] != "") $arrName[1] = $arrName[2];
					
					//check �����ҧ '   '
					if($arrName[3] != "") $arrName[1] = $arrName[3];
					
					//check �����ҧ '    '
					if($arrName[4] != "") $arrName[1] = $arrName[4];	
					
					$objSale = new Member();
					if($objSale->loadByCondition(" firstname ='".$arrName[0]."'  AND lastname = '".$arrName[1]."'  ")){
						$hBookingSaleId = $objSale->getMemberId();
					}else{
						$objMember = new Member();
						$objMember->setFirstname($arrName[0]);
						$objMember->setLastname($arrName[1]);
						$objMember->setUsername(date("YmdHis"));
						$objMember->setStatus(1);
						$hBookingSaleId = $objMember->add();
					}
				}				
	            $objOrder->setBookingSaleId($hBookingSaleId);
			
			
			
				if ($strMode=="Update") {
					$objInfo = new Info();
					$objInfo->setInfoId(1);
					$objInfo->load();
					
					$objCustomer->updateCashierBooking();
					
					if($objOrder->getBookingNumberNohead() ==""){
						$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
						$objOrder->setBookingNumberNohead($strBookingNumberNohead);
						$objInfo->updateOrderNumberNoheadRecent();
					}
					
					
					$objOrder->updateCashierBooking();
				} else {
					if($hCustomerId != ""){
						$objCustomer->updateCashierBooking();
					}else{
						$hCustomerId = $objCustomer->addCashierBooking();
					}					
					$objOrder->setBookingCustomerId($hCustomerId);
					//create booking number
					$objInfo = new Info();
					$objInfo->setInfoId(1);
					$objInfo->load();
					/*
					$strBookingNumber = $objInfo->getOrderNumber();
					$objOrder->setBookingNumber($strBookingNumber);
					$objInfo->updateOrderNumberRecent();
					*/
					$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
					$objOrder->setBookingNumberNohead($strBookingNumberNohead);
					$objInfo->updateOrderNumberNoheadRecent();
					
					$objOrder->setSendingCustomerId($hCustomerId);
					$objOrder->setSendingSaleId($hBookingSaleId);
					
					$pId = $objOrder->add();
					$hId=$pId;
					
					//add order_stock_other
					$objOrderStockOther = new OrderStockOther();
					$objOrderStockOther->setOrderId($pId);
					$objOrderStockOther->setType(1);
					for($i=1;$i<6;$i++){
						$objOrderStockOther->add();
					}
					
					$objOrderStockOther = new OrderStockOther();
					$objOrderStockOther->setOrderId($pId);
					$objOrderStockOther->setType(2);
					for($i=1;$i<6;$i++){
						$objOrderStockOther->add();
					}
					
					
				}
				
				if($strMode=="Add" AND $sPaymentSubjectId != ""){
					$arrPaymentSubjectId  = explode(",::",$sPaymentSubjectId);
					$arrRemark  = explode(",::",$sRemark);
					$arrQty  = explode(",::",$sQty);
					$arrPrice = explode(",::",$sPrice);
					$arrVat = explode(",::",$sVat);
					$arrPayin = explode(",::",$sPayin);
					$arrOrderPaymentId = explode(",::",$sOrderPaymentId);
					
					$objOrderPrice = new OrderPrice();
					for ($i = 0; $i < sizeof($arrPaymentSubjectId);$i++) {
						$objOrderPrice->setOrderId($pId);
						$objOrderPrice->setPaymentSubjectId($arrPaymentSubjectId[$i]);
						$objOrderPrice->setRemark($arrRemark[$i]);
						$objOrderPrice->setQty($arrQty[$i]);
						$objOrderPrice->setPrice($arrPrice[$i]);
						$objOrderPrice->setVat($arrVat[$i]);
						$objOrderPrice->setPayin($arrPayin[$i]);
						$objOrderPrice->setOrderPaymentId($arrOrderPaymentId[$i]);
						$objOrderPrice->setStatus(0);
						$hOrderPriceId[$i] = $objOrderPrice->add();
					}
					unset($objOrderPayment);
				}				
				
				
				if($strMode=="Add" AND $sPaymentTypeId != ""){
					$arrPaymentTypeId  = explode(",::",$sPaymentTypeId);
					$arrPaymentPrice  = explode(",::",$sPaymentPrice);
					$arrBankId  = explode(",::",$sBankId);
					$arrBankBranch  = explode(",::",$sBankBranch);
					$arrCheckNo  = explode(",::",$sCheckNo);
					$arrDay01  = explode(",::",$sDay01);
					$arrMonth01  = explode(",::",$sMonth01);
					$arrYear01  = explode(",::",$sYear01);
					
					$objOrderPayment = new OrderPayment();
					for ($i = 0; $i < sizeof($arrPaymentTypeId);$i++) {
						$objOrderPayment->setOrderId($pId);
						$objOrderPayment->setPaymentTypeId($arrPaymentTypeId[$i]);
						$objOrderPayment->setPrice($arrPaymentPrice[$i]);
						$objOrderPayment->setBankId($arrBankId[$i]);
						$objOrderPayment->setBranch($arrBankBranch[$i]);
						$objOrderPayment->setCheckNo($arrCheckNo[$i]);
						$strCheckDate =  $arrYear01[$i]."-".$arrMonth01[$i]."-".$arrDay01[$i];
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(0);
						$hOrderPaymentId= $objOrderPayment->add();
						
						for ($k = 0; $k < sizeof($arrPaymentSubjectId);$k++) {
							$objOrderPricePayment = new OrderPricePayment();
							$objOrderPricePayment->setOrderId($hId);
							$objOrderPricePayment->setOrderPriceId($hOrderPriceId[$i]);
							$objOrderPricePayment->setOrderPaymentId($hOrderPaymentId);
							$objOrderPricePayment->add();
						}
						
						
					}
					unset($objOrderPayment);
				}								
				
				
				unset ($objCustomer);
				unset ($objOrder);
				header("location:bcardPrintPreview.php?hId=$hId");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)
			
			
			}//ecnd check addprice
		}
}

$pageTitle = "1. ����������ѹ";
$pageContent = "1.1  �ѹ�֡�����š�èͧö";
$strHead03 = "�ѹ�֡�����š�èͧ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}		
	
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hOrderReservePrice.value=="")
		{
			alert("��س��к�ǧ�Թ�ͧ");
			document.forms.frm01.hOrderReservePrice.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if(document.forms.frm01.hIncomplete.checked == false){
			
	
			if (document.forms.frm01.hAddress.value=="")
			{
				alert("��س��кط������");
				document.forms.frm01.hAddress.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hTumbonCode.value=="")
			{
				alert("��س��кصӺ�");
				document.forms.frm01.hTumbon.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hAmphurCode.value=="")
			{
				alert("��س��к������");
				document.forms.frm01.hAmphur.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hProvinceCode.value=="")
			{
				alert("��س��кبѧ��Ѵ");
				document.forms.frm01.hProvince.focus();
				return false;
			} 
			
			if (document.forms.frm01.hZipCode.value=="")
			{
				alert("��س��к�������ɳ���");
				document.forms.frm01.hZip.focus();
				return false;
			} 							

		}

	
		if (document.forms.frm01.hBookingCarType.options[frm01.hBookingCarType.selectedIndex].value=="0")
		{
			alert("��س����͡���ö");
			document.forms.frm01.hBookingCarType.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hCarSeries.value=="")
		{
			alert("��س����͡Ẻö");
			document.forms.frm01.hCarSeries.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hBookingCarColor.options[frm01.hBookingCarColor.selectedIndex].value=="0")
		{
			alert("��س����͡��ö");
			document.forms.frm01.hBookingCarColor.focus();
			return false;
		}	

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<table width="100%" class="i_background05">
<tr>
	<td>&nbsp;</td>
	<td align="right">�ӹǹ Booking ������ : <?=$objCustomerBList->mCount;?> ��ª��� ��:<?=$objCustomerBYearList->mCount;?>   ��͹:<?=$objCustomerBMonthList->mCount;?>  �ѹ :<?=$objCustomerBDayList->mCount;?>  </td>
</tr>
</table>



<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="bcardUpdate.php" method="POST" onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">
	  <input type="hidden" name="hCustomerId"  value="<?=$objCustomer->getCustomerId();?>">
	  <input type="hidden" name="hBookingNumberNohead" value="<?=$objOrder->getBookingNumberNohead()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">	  
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>"> 
	  <input type="hidden" name="hTrick" value="true">

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<table>
				<tr>
					<td>
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
						</tr>
						</table>
					</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>�ѹ��������</strong></td>
					<td>
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day1 value="<?=$Day1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month1 value="<?=$Month1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year1 value="<?=$Year1?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year1,Day1, Month1, Year1,popCal);return false"></td>		
						</tr>
						</table>
					
					</td>
				</tr>
				</table>			
				
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >
				<input type="hidden" readonly name="hBookingSaleId" size="2"  value="<?=$objOrder->getBookingSaleId();?>">
				<input type="text" name="hSaleName" onKeyDown="if(event.keyCode==13 && frm01.hBookingSaleId.value != '' ) frm01.hOrderReservePrice.focus();if(event.keyCode !=13 ) frm01.hBookingSaleId.value='';" size="30"  value="<?=$hSaleName?>">
			</td>
			<td class="i_background03" valign="top"><strong>�Թ�ͧ</strong>   </td>
			<td valign="top" >			
				<input type="text" name="hOrderReservePrice" size="15"  value="<?=$objOrder->getOrderReservePrice();?>"> �ҷ
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="2">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<INPUT  name="hCustomerName"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
			</td>
			<td>
				<table>
				<tr>
					<td class="i_background03"><strong>�������������ó�</strong></td>
					<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
					<td class="i_background03"></td>
					<td></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><input type="text" maxlength="50" name="hAddress" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><input type="hidden" readonly size="6" name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" name="hTumbon" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';"  size="30"  value="<?=$objCustomer->getTumbon();?>"></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><input type="hidden" readonly size="4" name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text" name="hAmphur" onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';"  size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="2" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text" name="hProvince" onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';"  size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="5" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip" maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hBookingCarType.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';"  size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>	
		</table>
	</td>
</tr>
</table>

<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö���ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="10%" class="i_background03"><strong>Ẻö</strong></td>			
			<?
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				$hCarSeries = $objCarSeries->getTitle();
			?>
			<td width="40%">
				<input type="hidden" readonly size="3" name="hBookingCarSeries"  value="<?=$objOrder->getBookingCarSeries();?>">
				<input type="text" name="hCarSeries" size="30"  value="<?=$hCarSeries;?>">
			</td>		
			<td class="i_background03" valign="top" width="10%"><strong>���ö</strong> </td>
			<td valign="top" width="40%">
				<input type="hidden" size=3 name="hBookingCarModel" <?=$objOrder->getBookingCarType()?>>
				<label id="car_model"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>				
		</tr>			
		<tr>
			<td  class="i_background03"><strong>������</strong> </td>
			<td >
				<input  type="hidden" size=3  name="hBookingCarType" value="<?=$objOrder->getBookingCarType()?>">
				<select  name="hCarType">
					<option value="1" <?if($objCarSeries->getCarType() == "1") echo "selected"?>>ö��
					<option value="2" <?if($objCarSeries->getCarType() == "2") echo "selected"?>>ö�к�
					<option value="3" <?if($objCarSeries->getCarType() == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($objCarSeries->getCarType() == "4") echo "selected"?>>ö���
				</select>	
			</td>		
			<td class="i_background03" ><strong>Model</strong> </td>
			<td >
				<label id="car_code"><?=$objCarSeries->getModel()?></label>				
			</td>
		</tr>			
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hBookingCarGear">
					<option value="Auto" <?if($objOrder->getBookingCarGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objOrder->getBookingCarGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hBookingCarColor",$objOrder->getBookingCarColor(),"��س����͡");?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>

<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š����¡��</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center" width="5%" class="listDetail"></td>
	<td align="center" width="20%" class="listDetail02">��¡��</td>
	<td align="center" width="20%" class="listDetail02">��������´</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ</td>
	<td align="center" width="10%" class="listDetail02">˹�����</td>
	<td align="center" width="5%" class="listDetail02">vat</td>
	<td align="center" width="5%" class="listDetail02">�Դź</td>
	<td align="center" width="15%" class="listDetail02"></td>
	<td align="center" width="10%" class="listDetail02"></td>	
</tr>
<tr>
	<td class="listDetail"></td>
	<td align="left" class="listDetail"><?$objPaymentSubjectList->printSelectScript("hPaymentSubjectId",$_POST["hPaymentSubjectId"],"��س����͡��¡��","checkOther();");?></td>
	<td align="center" class="listDetail"><span id="spanRemark" style="display:none"><input type="text" name="hRemark" size="30" value=""></span></td>
	<td align="center" class="listDetail"><input type="text" style="text-align=right;" name="hQty" size="5"  value="1"></td>
	<td align="center" class="listDetail"><input type="text" style="text-align=right;" name="hPrice" size="20" value=""></td>
	<td align="center" class="listDetail"><input type="checkbox" name="hVat" value="1"></td>
	<td align="center" class="listDetail"><input type="checkbox" name="hPayin" value="1"></td>
	<td align="center" class="listDetail"></td>
	<td align="center" class="listDetail"><input type="Submit" name="hAddPrice" class="button" onclick="return checkPrice()" value="&nbsp;&nbsp;������¡��&nbsp;&nbsp;"></td>

</tr>
		<input type="hidden" name="sPaymentSubjectId" value="<?=$sPaymentSubjectId?>">
		<input type="hidden" name="sRemark" value="<?=$sRemark?>">
		<input type="hidden" name="sQty" value="<?=$sQty?>">
		<input type="hidden" name="sPrice" value="<?=$sPrice?>">
		<input type="hidden" name="sVat" value="<?=$sVat?>">
		<input type="hidden" name="sPayin" value="<?=$sPayin?>">
		<input type="hidden" name="sOrderPaymentId" value="<?=$sOrderPaymentId?>">
<?if($strMode == "Update"){		
		if ($objOrderPriceList->getCount() > 0 ){
?>	
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">��������´</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">vat</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center"  class="listTitle">�ӹǹ�Թ</td>
	<td align="center"  class="listTitle"></td>	

</tr>
				<?
				$i=0;
				forEach($objOrderPriceList->getItemList() as $objItem) {?>
				<input type="hidden"  name="hPaymentSubjectIdTemp[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
				<input type="hidden"  name="hOrderPriceIdTemp[<?=$i?>]" value="<?=$objItem->getOrderPriceId();?>">
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td align="left" class="ListDetail">						
						<?$objPaymentSubjectList->printSelect("hPaymentSubjectIdTemp[$i]",$objItem->getPaymentSubjectId(), "��س����͡��¡��");?>
					</td>
					<td align="center" class="ListDetail"><input type="text"  size="30" name="hRemarkTemp[<?=$i?>]" value="<?=$objItem->getRemark();?>"></td>
					<td align="center" class="ListDetail"><input type="text" style="text-align=right;"   size="5" name="hQtyTemp[<?=$i?>]" value="<?=$objItem->getQty();?>"></td>
					<td align="right" class="ListDetail"><input type="text" style="text-align=right;"   size="20" name="hPriceTemp[<?=$i?>]" value="<?=$objItem->getPrice();?>"></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hVatTemp[<?=$i?>]" value="1" <?if($objItem->getVat() == 1) echo "checked";?> ></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hPayinTemp[<?=$i?>]" value="1" <?if($objItem->getPayin() == 1) echo "checked";?> ></td>
					<?$sumPrice = ($objItem->getPrice()*$objItem->getQty())?>
					<td align="right" class="ListDetail"><?=number_format($sumPrice,2);?></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$objItem->getOrderPriceId()?>"></td>
				</tr>
					<?
					if($objItem->getVat() == 1){
						if($objItem->getPayin() == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPrice;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPrice;
						}
					}else{
						if($objItem->getPayin() == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPrice;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPrice;
						}
						
					}
				?>				
				<?
					++$i;
				}
				$grandTotal = $hPriceIncludeVat+$hPriceExcludeVat;
				if($hPriceIncludeVat > 0){
					$totalVat = $hPriceIncludeVat/1.07;
					$vat = $hPriceIncludeVat-$totalVat;
				}
				$totalVat = $totalVat+$hPriceExcludeVat;
				
				?>
				<input type="hidden" name="hCountTotal" value="<?=$i?>">
<tr>
	<td align="center" class="listDetail" colspan="5" rowspan="2" >
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hBookingRemark" rows="3" cols="70"><?=$objOrder->getBookingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right"  class="listDetail" colspan="2"><strong>����Թ</strong></td>		
	<td align="right"  class="error"><?=number_format($totalVat,2);?></td>	
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePrice" class="button" value="��Ѻ��ا��¡��"></td>	
</tr>				
<tr>	
	<td align="right"  class="listDetail" colspan="2"><strong>������Ť������</strong></td>	
	<td align="right"  class="error"><?=number_format($vat,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>
<tr>
	<td align="center" class="listDetail" colspan="5">
		<table width="100%">
		<tr>
			<td valign="top" width="50" >�ӹǹ�Թ</td>
			<td  class="error"><?currencyThai($grandTotal);?>&nbsp;�ҷ��ǹ</td>
			<td></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" colspan="2"><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><?=number_format($grandTotal,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>


		<?}?>


		
<?}else{?>
		<?if ($sPaymentSubjectId != ""){
				$arrPaymentSubjectId  = explode(",::",$sPaymentSubjectId);
				$arrRemark = explode(",::",$sRemark);
				$arrQty = explode(",::",$sQty);
				$arrPrice = explode(",::",$sPrice);
				$arrVat = explode(",::",$sVat);
				$arrPayin = explode(",::",$sPayin);
			?>

<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">��������´</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">vat</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center"  class="listTitle">�ӹǹ�Թ</td>
	<td align="center"  class="listTitle"></td>	
</tr>
				<?
				for($i=0;$i<sizeof($arrPaymentSubjectId);$i++){
					$objPaymentSubject = new PaymentSubject();
					$objPaymentSubject->setPaymentSubjectId($arrPaymentSubjectId[$i]);
					$objPaymentSubject->load();
				?>
<tr>
	<td align="right" class="ListDetail"><?=$i+1?></td>
	<td align="left" class="ListDetail"><?$objPaymentSubjectList->printSelect("hPaymentSubjectIdTemp[$i]",$arrPaymentSubjectId[$i], "��س����͡��¡��");?></td>
	<td align="center" class="ListDetail"><input type="text"  size="30" name="hRemarkTemp[<?=$i?>]" value="<?=$arrRemark[$i];?>"></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" size="5" name="hQtyTemp[<?=$i?>]" value="<?=$arrQty[$i];?>"></td>
	<td align="right" class="ListDetail"><input type="text" style="text-align=right;"  size="20" name="hPriceTemp[<?=$i?>]" value="<?=$arrPrice[$i];?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox" name="hVatTemp[<?=$i?>]" value="1" <?if($arrVat[$i] == 1) echo "checked";?>></td>
	<td align="center" class="ListDetail"><input type="checkbox" name="hPayinTemp[<?=$i?>]" value="1" <?if($arrPayin[$i] == 1) echo "checked";?>></td>
	<?$sumPrice = ($arrPrice[$i]*$arrQty[$i]);?>
	<td align="right" class="ListDetail"><?=number_format($sumPrice,2);?></td>
	<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$i+1?>"></td>
</tr>				
					<?
					if($arrVat[$i] == 1){
						if($arrPayin[$i] == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPrice;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPrice;
						}
					}else{
						if($arrPayin[$i] == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPrice;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPrice;
						}
					}
					?>
				<?}
				$grandTotal = $hPriceIncludeVat+$hPriceExcludeVat;
				if($hPriceIncludeVat > 0){
					$totalVat = $hPriceIncludeVat/1.07;
					$vat = $hPriceIncludeVat-$totalVat;
				}
				$totalVat = $totalVat+$hPriceExcludeVat;
				
				?>
<tr>
	<td align="center" class="listDetail" colspan="5" rowspan="2" >
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hBookingRemark" rows="3" cols="70"><?=$objOrder->getBookingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right"  class="listDetail" colspan="2"><strong>����Թ</strong></td>		
	<td align="right"  class="error"><?=number_format($totalVat,2);?></td>	
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePrice" class="button" value="��Ѻ��ا��¡��"></td>	
</tr>				
<tr>	
	<td align="right"  class="listDetail" colspan="2"><strong>������Ť������</strong></td>	
	<td align="right"  class="error"><?=number_format($vat,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>
<tr>
	<td align="center" class="listDetail" colspan="5">
		<table width="100%">
		<tr>
			<td valign="top" width="50" >�ӹǹ�Թ</td>
			<td  class="error"><?currencyThai($grandTotal);?>&nbsp;�ҷ��ǹ</td>
			<td>�ҷ��ǹ</td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" colspan="2"><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><?=number_format($grandTotal,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>



			<?}?>
	
<?}?>

</table>

	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail" width="5%"></td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
	<td class="listDetail" width="10%"></td>
</tr>
<tr>
	<td class="listDetail"></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
	<td align="center" class="listDetail"><input type="Submit" name="hAddPayment" class="button" value="&nbsp;&nbsp;������¡��&nbsp;&nbsp;" onclick="return checkPayment();"></td>
</tr>
		<input type="hidden" name="sPaymentTypeId" value="<?=$sPaymentTypeId?>">
		<input type="hidden" name="sPaymentPrice" value="<?=$sPaymentPrice?>">
		<input type="hidden" name="sBankId" value="<?=$sBankId?>">
		<input type="hidden" name="sBankBranch" value="<?=$sBankBranch?>">
		<input type="hidden" name="sCheckNo" value="<?=$sCheckNo?>">
		<input type="hidden" name="sDay01" value="<?=$sDay01?>">
		<input type="hidden" name="sMonth01" value="<?=$sMonth01?>">
		<input type="hidden" name="sYear01" value="<?=$sYear01?>">
<?if($strMode == "Update"){	
		if ($objOrderPaymentList->getCount() > 0 ){?>	
<tr>
	<td align="center" class="listTitle">�ӴѺ</td>
	<td align="center" class="listTitle">�ٻẺ��ê����Թ</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
	<td align="center" class="listTitle">��Ҥ��</td>
	<td align="center" class="listTitle">�Ң�</td>
	<td align="center" class="listTitle">�Ţ�����</td>
	<td align="center" class="listTitle">ŧ�ѹ���</td>
	<td class="listDetail"></td>
</tr>
				<?
				$i=0;
				forEach($objOrderPaymentList->getItemList() as $objItem) {
					$arrDate = explode("-",$objItem->getCheckDate());
				?>
				<input type="hidden"  name="hPaymentTypeIdTemp[<?=$i?>]" value="<?=$objItem->getPaymentTypeId();?>">
				<input type="hidden"  name="hOrderPaymentIdTemp[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td  align="center" class="ListDetail">						
						<?$objPaymentTypeList->printSelect("hPaymentTypeIdTemp[$i]",$objItem->getPaymentTypeId(), "��س����͡��¡��");?>
					</td>
					<td align="center" class="ListDetail"><input type="text"  size="15" name="hPaymentPriceTemp[<?=$i?>]" value="<?=$objItem->getPrice();?>"></td>
					<td  align="center" class="ListDetail">						
						<?$objBankList->printSelect("hBankIdTemp[$i]",$objItem->getBankId(), "��س����͡��¡��");?>
					</td>					
					<td  align="center" class="ListDetail"><input type="text"   size="20" name="hBankBranchTemp[<?=$i?>]" value="<?=$objItem->getBranch();?>"></td>
					<td  align="center" class="ListDetail"><input type="text"   size="10" name="hCheckNoTemp[<?=$i?>]" value="<?=$objItem->getCheckNo();?>"></td>
					<td align="center" class="ListDetail">
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Temp<?=$i?>  value="<?=$arrDate[2];?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Temp<?=$i?> value="<?=$arrDate[1];?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Temp<?=$i?> value="<?=$arrDate[0];?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Temp<?=$i?>,Day01Temp<?=$i?>, Month01Temp<?=$i?>, Year01Temp<?=$i?>,popCal);return false"></td>
						</tr>
						</table>		
					
					</td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$objItem->getOrderPaymentId()?>"></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="6">
						<table>
						<tr>
						<?$k=1;
							$r=0;
						forEach($objOrderPriceList->getItemList() as $objItem01) {
						$objOrderPricePayment = new OrderPricePayment();
						$objOrderPricePayment->loadByCondition(" order_id=$hId AND order_price_id = ".$objItem01->getOrderPriceId()." AND order_payment_id= ".$objItem->getOrderPaymentId()."  ");
						?>
						<td width="20%">
							<input type="hidden" name="hOrderPricePaymentId[<?=$i?>][<?=$r?>]" value="<?=$objOrderPricePayment->getOrderPricePaymentId();?>">
							<input type="hidden" name="hOrderPriceIdTemp01[<?=$i?>][<?=$r?>]" value="<?=$objItem01->getOrderPriceId();?>">
							<input value="1" type="checkbox" <?if($objOrderPricePayment->getOrderPricePaymentId() != "") echo "checked"?> name="hPricePayment[<?=$i?>][<?=$r?>]"><?=$objItem01->getPaymentSubjectTitle()?>,<?=$objItem01->getPrice()?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>	
						<?
						if(fmod($k,5) ==0){
							echo "</tr><tr>";
						}else{
						
						}
						$k++;
						$r++;
						}?>
						</table>
					</td>
					<td></td>
				</tr>
				<?
					++$i;
				}
				
				?>
				<input type="hidden" name="hCountTotalPayment" value="<?=$i?>">
				<input type="hidden" name="hCountTotalPricePayment" value="<?=$r?>">
<tr>
	<td align="center" class="listDetail" colspan="7" ></td>
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePayment" class="button" value="��Ѻ��ا��¡��"></td>	
</tr>				



		<?}?>
		


<?}else{?>
		<?if ($sPaymentTypeId != ""){
				$arrPaymentTypeId  = explode(",::",$sPaymentTypeId);
				$arrPaymentPrice = explode(",::",$sPaymentPrice);
				$arrBankId = explode(",::",$sBankId);
				$arrBankBranch = explode(",::",$sBankBranch);
				$arrCheckNo = explode(",::",$sCheckNo);
				$arrDay01 = explode(",::",$sDay01);
				$arrMonth01 = explode(",::",$sMonth01);
				$arrYear01 = explode(",::",$sYear01);
			?>
<tr>
	<td align="center" class="listTitle">�ӴѺ</td>
	<td align="center" class="listTitle">�ٻẺ��ê����Թ</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
	<td align="center" class="listTitle">��Ҥ��</td>
	<td align="center" class="listTitle">�Ң�</td>
	<td align="center" class="listTitle">�Ţ�����</td>
	<td align="center" class="listTitle">ŧ�ѹ���</td>
	<td class="listDetail"></td>
</tr>
				<?
				for($i=0;$i<sizeof($arrPaymentTypeId);$i++){
					$objPaymentType = new PaymentType();
					$objPaymentType->setPaymentTypeId($arrPaymentTypeId[$i]);
					$objPaymentType->load();
				?>
<tr>
	<td align="right" class="ListDetail"><?=$i+1?></td>
	<td  align="center" class="ListDetail"><?$objPaymentTypeList->printSelect("hPaymentTypeIdTemp[$i]",$arrPaymentTypeId[$i], "��س����͡��¡��");?></td>
	<td align="center" class="ListDetail"><input type="text"  style="text-align=right;" size="15" name="hPaymentPriceTemp[<?=$i?>]" value="<?=$arrPaymentPrice[$i];?>"></td>
	<td  align="center" class="ListDetail"><?$objBankList->printSelect("hBankIdTemp[$i]",$arrBankId[$i], "��س����͡��¡��");?></td>
	<td align="center" class="ListDetail"><input type="text" size="20" name="hBankBranchTemp[<?=$i?>]" value="<?=$arrBankBranch[$i];?>"></td>
	<td align="center" class="ListDetail"><input type="text"size="10" name="hCheckNoTemp[<?=$i?>]" value="<?=$arrCheckNo[$i];?>"></td>
	<td align="center" class="ListDetail">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Temp<?=$i?>  value="<?=$arrDay01[$i];?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Temp<?=$i?> value="<?=$arrMonth01[$i];?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Temp<?=$i?> value="<?=$arrYear01[$i];?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Temp<?=$i?>,Day01Temp<?=$i?>, Month01Temp<?=$i?>, Year01Temp<?=$i?>,popCal);return false"></td>
		</tr>
		</table>		
	
	</td>
	<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$i+1?>"></td>
</tr>
<tr>
	<td></td>
	<td colspan="6">
		<table>
		<tr>
		<?
		for($k=0;$k<sizeof($arrPaymentSubjectId);$k++){
			$objPaymentSubject = new PaymentSubject();
			$objPaymentSubject->setPaymentSubjectId($arrPaymentSubjectId[$k]);
			$objPaymentSubject->load();
		?>
		<td width="20%">
			<input value="1" type="checkbox" <?if($hPricePayment[$i][$k] != "") echo "checked"?> name="hPricePayment[<?=$i?>][<?=$k?>]"><?=$objPaymentSubject->getTitle()?>,<?=$arrPrice[$k];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>	
		<?
		if(fmod(($k+1),5) ==0){echo "</tr><tr>";}
		}?>
		</tr>
		</table>
	</td>
	<td></td>
</tr>
	<?}?>			
<tr>
	<td align="center" class="listDetail" colspan="7" ></td>
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePayment" class="button" value="��Ѻ��ا��¡��"></td>	
</tr>
<?}?>


<?}?>


		</table>
	</td>
</tr>
</table>
<br>



<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}else{?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='bcardList.php'">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {
		return "bcardAutoCustomer.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {
		return "bcardAutoOrderNumber.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	new CAPXOUS.AutoComplete("hCarSeries", function() {
		return "bcardAutoCarSeries.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "bcardAutoSale.php?q=" + this.text.value;
	});	
	
</script>
<script>

	function checkOther(){
		<?
		$objPaymentSubject = new PaymentSubject();
		$objPaymentSubject->loadByCondition("title = '����' AND type_id= 1 ");
		?>
	
	
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="<?=$objPaymentSubject->getPaymentSubjectId();?>")
		{
			document.getElementById("spanRemark").style.display = "";	
			document.frm01.hRemark.focus();
		}else{
			document.getElementById("spanRemark").style.display = "none";	
			document.frm01.hQty.focus();
		}
	}
	
	function checkPrice(){		
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡�ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentSubjectId.focus();			
			return false;
		} 			
		
		if (document.forms.frm01.hQty.value =="" || document.forms.frm01.hQty.value < 0 )
		{
			alert("��س��кبӹǹ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hQty.focus();
			return false;
		} 				
		
		if (document.forms.frm01.hPrice.value =="" || document.forms.frm01.hPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPrice.focus();
			return false;
		} 
		return true;
		
	}		
	
	function checkPayment(){		
		if (document.forms.frm01.hPaymentTypeId.options[frm01.hPaymentTypeId.selectedIndex].value=="0")
		{
			alert("��س����͡�ٻẺ��ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentTypeId.focus();			
			return false;
		} 			
		
		if (document.forms.frm01.hPaymentPrice.value =="" || document.forms.frm01.hPaymentPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentPrice.focus();
			return false;
		} 
		return true;
		
	}		
	
</script>
<?
	include("h_footer.php")
?>
<?
include("common.php");

$objCustomer = new Customer();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 3");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objOrder = new Order();
$objOrderExtra = new OrderExtra();

if($hOrderId != ""){

		$objOrder->setOrderId($hOrderId);
		$objOrder->load();
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();

}




if (empty($hSubmit)) {
	if ($hId!="") {
		$strMode="Update";
		$objOrderExtra->setOrderExtraId($hId);
		$objOrderExtra->load();
		
		$arrDate = explode("-",$objOrderExtra->getExtraDate());
		$Day1 = $arrDate[2];
		$Month1 = $arrDate[1];
		$Year1 = $arrDate[0];

		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" status = 0 AND order_extra_id = $hId");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_extra_id = $hId  AND status = 0");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" order_price_id ASC ");
		$objOrderPriceList->load();
		
		
	} else {
		$strMode="Add";
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate1 = explode("-",date("Y-m-d"));
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];

	}

} else {
	if (!empty($hSubmit)) {
			
            $objOrderExtra->setOrderExtraId($hId);
			$objOrderExtra->setOrderId($hOrderId);
			$hExtraDate = $Year1."-".$Month1."-".$Day1;
			$objOrderExtra->setExtraDate($hExtraDate);
			$objOrderExtra->setExtraType(3);
			$objOrderExtra->setPrId($hPrId);
			$objOrderExtra->setName($hName);
			$objOrderExtra->setAddress($hAddress);
			$objOrderExtra->setBookingNumber($hBookingNumber);
			$objOrderExtra->setSendingNumber($hSendingNumber);
			$objOrderExtra->setSendingNumberNohead($hSendingNumberNohead);
			$objOrderExtra->setAddBy($sMemberId);
			$objOrderExtra->setEditBy($sMemberId);
			$objOrderExtra->setCompanyId($sCompanyId);

			$pasrErrOrder = $objOrderExtra->check($hSubmit);

			If (  Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();
					
					if($objOrderExtra->getBookingNumber() ==""){
						$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
						$objOrderExtra->setBookingNumber($strBookingNumberNohead);
						$objInfo->updateOrderNumberNoheadRecent();
					}
					$objOrderExtra->update();
					
				} else {
				
					//create booking number
					$objInfo = new Company();
					$objInfo->setCompanyId($sCompanyId);
					$objInfo->load();

					$strBookingNumberNohead = $objInfo->getOrderNumberNohead();
					$objOrderExtra->setBookingNumber($strBookingNumberNohead);
					$objInfo->updateOrderNumberNoheadRecent();
					
					$hId=$objOrderExtra->add();

				}
				
				//update order price
				for($i=0;$i<$hOrderPriceCount;$i++){
					$objOrderPrice = new OrderPrice();
					$objOrderPrice->setOrderId($hOrderId);
					$objOrderPrice->setOrderExtraId($hId);
					$objOrderPrice->setOrderPriceId($hOrderPriceId[$i]);
					$objOrderPrice->setPaymentSubjectId($hPaymentSubjectId[$i]);
					$objOrderPrice->setRemark($hRemark[$i]);
					$objOrderPrice->setQty($_POST["hQty_".$i]);
					$objOrderPrice->setPrice($_POST["hPrice_".$i]);
					$objOrderPrice->setVat($_POST["hVat_".$i]);
					$objOrderPrice->setPayin($_POST["hPayin_".$i]);
					$objOrderPrice->setOrderPaymentId($hOrderPaymentId[$i]);
					$objOrderPrice->setStatus(3);
					if($hOrderPriceId[$i] != ""){
						$objOrderPrice->update();
					}else{
						$objOrderPrice->add();
					}
				}
				
				
				//update order payment
				for($i=0;$i<3;$i++){
					$objOrderPayment = new OrderPayment();
						
						$objOrderPayment->setOrderExtraId($hId);
						$objOrderPayment->setOrderId($hOrderId);
						$objOrderPayment->setOrderPaymentId($hOrderPaymentId[$i]);
						$objOrderPayment->setPaymentTypeId($_POST["hPaymentTypeId_".$i]);
						$objOrderPayment->setPrice($_POST["hPaymentPrice_".$i]);
						$objOrderPayment->setBankId($_POST["hBankId_".$i]);
						$objOrderPayment->setBranch($_POST["hBankBranch_".$i]);
						$objOrderPayment->setCheckNo($_POST["hCheckNo_".$i]);
						$strCheckDate =  $_POST["Year01_".$i]."-".$_POST["Month01_".$i]."-".$_POST["Day01_".$i];
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setStatus(3);
						if($hOrderPaymentId[$i] != ""){
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->update();
							}else{
								$objOrderPayment->delete();
							}
						}else{
							if($_POST["hPaymentPrice_".$i] > 0){
								$objOrderPayment->add();
							}
						}
						

					 
				}

				unset ($objOrderExtra);
				header("location:extraPrintPreview.php?hId=$hId");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)

		}
}

$pageTitle = "1. ����������ѹ";
$pageContent = "1.7  �ѹ�֡�����ū����ػ�ó����� ";
$strHead03 = "�ѹ�֡�����ū����ػ�ó����� ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}		
	
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="extraUpdate.php" method="POST" onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hOrderId?>">
	   <input type="hidden" name="hBookingNumber" value="<?=$objOrderExtra->getBookingNumber();?>">
	  <input type="hidden" name="hTrick" value="true">

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ������觫���</strong></td>
			<td  valign="top"><input type="text" name="hPrId" size="20"  value="<?=$objOrderExtra->getPrId()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">

					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day1 value="<?=$Day1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month1 value="<?=$Month1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year1 value="<?=$Year1?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year1,Day1, Month1, Year1,popCal);return false"></td>		
						</tr>
						</table>
	
				
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>����-���ʡ��</strong>   </td>
			<td width="40%" valign="top" >
				<input type="text" name="hName" size="35"  value="<?=$objOrderExtra->getName();?>">
			</td>
			<td class="i_background03" valign="top"><strong>�������</strong>   </td>
			<td valign="top" >			
				<textarea name="hAddress" rows="3" cols="50"><?=$objOrderExtra->getAddress();?></textarea>
				
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š����¡��</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
</tr>
<?
$i=0;
forEach($objPaymentSubjectList->getItemList() as $objItem) {
	$objOrderPrice = new OrderPrice();
	if($strMode=="Update"){
		$objOrderPrice->loadByCondition(" order_extra_id=$hId AND payment_subject_id = ".$objItem->getPaymentSubjectId());
	}
?>
<input type="hidden"  name="hPaymentSubjectId[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
<input type="hidden"  name="hOrderPriceId[<?=$i?>]" value="<?=$objOrderPrice->getOrderPriceId();?>">

<tr>
	<td align="center" class="ListDetail"><?=$i+1?></td>
	<td align="left" class="ListDetail">
		<table>
		<tr>
			<td width="100"><?=$objItem->getTitle();?></td>
			<td>&nbsp;<input type="text"  size="40" name="hRemark[<?=$i?>]" value="<?=$objOrderPrice->getRemark();?>"></td>
		</tr>
		</table>

	
	</td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_all();" onfocus="sum_all();"  size="5" name="hQty_<?=$i?>" value="<?if( $objOrderPrice->getQty() > 0 ){echo $objOrderPrice->getQty();}else{ echo "1";}?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onclick="sum_all()"  onblur="sum_all();" onfocus="sum_all();" name="hPayin_<?=$i?>" value="1" <?if($objOrderPrice->getPayin() == 1) echo "checked";?> ></td>
	<?$sumPrice = ($objOrderPrice->getPrice()*$objOrderPrice->getQty())?>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_all();" onfocus="sum_all();"  size="20" name="hPrice_<?=$i?>" value="<?=$objOrderPrice->getPrice();?>"></td>	
	<td align="right" class="ListDetail"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceSum_<?=$i?>" value="<?=number_format($sumPrice,2);?>"></td>
</tr>	
<?$i++;}?>
<input type="hidden"  name="hOrderPriceCount" value="<?=$i?>">				
<tr>
	<td align="center" class="listDetail" colspan="4">
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hBookingRemark" rows="3" cols="70"><?=$objOrder->getBookingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" ><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><input type="text" readonly="" style="text-align=right;"   size="20" name="hPriceTotal" value="<?=number_format($grandTotal,2);?>"></td>	

</tr>
</table>


	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail02" width="5%" align="center">�ӴѺ</td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="20%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="20%" class="listDetail02">ŧ�ѹ���</td>
</tr>
<?
if($strMode=="Update"){
	$objOrderPaymentList = new OrderPaymentList();
	$objOrderPaymentList->setFilter(" order_extra_id=$hId AND status=3 ");
	$objOrderPaymentList->load();
	$i=0;
	forEach($objOrderPaymentList->getItemList() as $objItem) {
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i",$objItem->getPaymentTypeId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value="<?=$objItem->getPrice();?>"></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i",$objItem->getBankId(),"��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value="<?=$objItem->getBranch();?>"></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value="<?=$objItem->getCheckNo();?>"></td>
	<td align="center">
		<?
		$arrDate = $objItem->getCheckDate();
		$Day01="";
		$Month01="";
		$Year01="";
		if($arrDate != "0000-00-00" or $arrDate != ""){
			$arrDate = explode("-",$arrDate);
			$Day01=$arrDate[2];
			$Month01=$arrDate[1];
			$Year01=$arrDate[0];
		}
		?>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?$i++;}?>
<?$j =  3 - $objOrderPaymentList->mCount;?>
<?$k=$i;
for($i=$k;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}else{?>

<?$i=0;
for($i=0;$i<3;$i++){
?>
<input type="hidden"  name="hOrderPaymentId[<?=$i?>]" value="">
<tr>
	<td class="listDetail" align="center"><?=$i+1?></td>
	<td align="center"><?$objPaymentTypeList->printSelect("hPaymentTypeId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hPaymentPrice_<?=$i?>" size="15"  value=""></td>
	<td align="center"><?$objBankList->printSelect("hBankId_$i","","��س����͡��¡��");?></td>
	<td align="center"><input type="text" name="hBankBranch_<?=$i?>" size="20"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo_<?=$i?>" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?> value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?> value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>,Day01_<?=$i?>, Month01_<?=$i?>, Year01_<?=$i?>, popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
</tr>
<?}?>
</table>

<?}?>






	</td>
</tr>
</table>
<br>



<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}else{?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='extraList.php'">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>

	function sum_all(){
	sum = 0;
	<?
	$i=0;
	forEach($objPaymentSubjectList->getItemList() as $objItem) {?>
		hPrice_<?=$i?> = 0;
		hQty_<?=$i?> = 0;
		
		if(document.frm01.hPrice_<?=$i?>.value != "" && document.frm01.hPrice_<?=$i?>.value != "0"){ hPrice_<?=$i?>= (document.frm01.hPrice_<?=$i?>.value*1);}		
		if(document.frm01.hQty_<?=$i?>.value != "" && document.frm01.hQty_<?=$i?>.value != "0"){ hQty_<?=$i?>= (document.frm01.hQty_<?=$i?>.value*1);}		
		
		if(document.frm01.hPayin_<?=$i?>.checked == true){
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>*-1;
			sum= sum-(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}else{
			document.frm01.hPriceSum_<?=$i?>.value = hPrice_<?=$i?>*hQty_<?=$i?>;
			sum= sum+(hPrice_<?=$i?>*hQty_<?=$i?>);			
		}
	<?$i++;}?>
	
		document.frm01.hPriceTotal.value = sum;
	
	}
	
	sum_all();
</script>
<?
	include("h_footer.php")
?>
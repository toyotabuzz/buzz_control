<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');
require_once('../include/numtoword.php');

$resultinwords=new numtoword;
$objCustomer = new Customer();
$objOrder = new Order();
$objOrderExtra = new OrderExtra();
$objOrderPayment = new OrderPayment();
$objOrderPrice = new OrderPrice();
$objCarSeries = new CarSeries();

$objCompany =  new Company();
$objCompany->setCompanyId($sCompanyId);
$objCompany->load();

if ($hInsureItemId!="") {

		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsureId);
		$objInsure->load();
		
		$objInsureItem = new InsureItem();
		$objInsureItem->set_insure_item_id($hInsureItemId);
		$objInsureItem->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objInsureCompany = new InsureCompany();
		$objInsureCompany->setInsureCompanyId($objInsure->get_k_prb_id());
		$objInsureCompany->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsureCar->get_sale_id());
		$objMember->load();

		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objPaymentList = new OrderPaymentList();
		$objPaymentList->setFilter(" order_extra_id = $hInsureItemId  AND order_id= $hInsureId AND status = 4");
		$objPaymentList->setPageSize(0);
		$objPaymentList->setSortDefault(" order_payment_id ASC ");
		$objPaymentList->load();

		$objPriceList = new InsureItemDetailList();
		$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= $hInsureId and PS.nohead = 0  ");
		$objPriceList->setPageSize(0);
		$objPriceList->setSort(" PS.rank ASC ");
		$objPriceList->load();
		

}


if(isset($hNumber01)){
	
	$objInsureItem =new InsureItem();
	$objInsureItem->set_insure_item_id($hInsureItemId);
	$objInsureItem->set_old_number_nohead($hNumber01);
	$objInsureItem->updateOldNumberNohead();
	
}

if(isset($hNumber02)){
	
	$objInsureItem =new InsureItem();
	$objInsureItem->set_insure_item_id($hInsureItemId);
	$objInsureItem->set_old_number_head($hNumber02);
	$objInsureItem->updateOldNumberHead();
	
}


class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",0,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,50,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(0,0,0);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


			
				$pdf->AddPage();



				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				
				
				$objLabel01 = new Label();
				$objLabel01->loadByCondition(" company_id=$sCompanyId and mtype = '������Ѻ�Թ��Сѹ���' and label_id = $hPrinterId ");
				
				
				$pdf->SetFont('angsa','B',14);	
				$strtext = $objLabel->getText01();
				
				if($objInsureItem->get_acc_company_id() == 12){ $text= "�ӹѡ�ҹ�˭�";}
				if($objInsureItem->get_acc_company_id() == 13){ $text= "�Ңҷ�� 1";}
				if($objInsureItem->get_acc_company_id() == 14){ $text= "�Ңҷ�� 1";}
				if($objInsureItem->get_acc_company_id() == 17){ $text= "�Ңҷ�� 2";}
				if($objInsureItem->get_acc_company_id() == 18){ $text= "�Ңҷ�� 3";}
				$pdf->SetXY(0,19);
				$pdf->Cell(200,20,"",0,0,"R");
				$pdf->Cell(300,20," �Ңҷ���͡㺡ӡѺ���� : ".$text,0,0,"L");
				$pdf->Ln();

				
				if($objLabel01->getText02() == ""){
					$pdf->SetXY(0,43);
				}else{
					$pdf->SetXY(0,$objLabel01->getText02());
				}
				
				
				$pdf->Cell(70,20,"",0,0,"C");
				if($objCustomer->getTitle() != "˨�" AND $objCustomer->getTitle() != "���" ){
					$title = $objCustomer->getCustomerTitleIdDetail01();
				}else{
					$title = $objCustomer->getTitle();
				}
				$pdf->Cell(280,20,$title." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname(),0,0,"L");
				$pdf->Cell(90,20,$objInsureCar->get_code(),0,0,"C");
				
				
				$pdf->Cell(150,20,formatShortDateTimeCashier($objInsureItem->get_acc_date()),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(280,20,$objCustomer->getAddressLabel(),0,0,"L");
				$pdf->Cell(90,20,$objCarModel->getTitle(),0,0,"C");
				$pdf->Cell(130,20,"",0,0,"C");
				$pdf->Ln();
				
				if($objCustomer->getProvince() == "��ا෾��ҹ��"){
				
				if($objCustomer->getTumbon() != "") $text=  "�ǧ ".$objCustomer->getTumbon();
				if(substr($objCustomer->getAmphur(),0,3) == "ࢵ"){
					if($objCustomer->getAmphur() != "") $text.=  "  ".$objCustomer->getAmphur();
				}else{
					if($objCustomer->getAmphur() != "") $text.=  " ࢵ ".$objCustomer->getAmphur();
				}
				if($objCustomer->getProvince() != "") $text.=  " �ѧ��Ѵ ".$objCustomer->getProvince();
				if($objCustomer->getZip() != "") $text.=  " ������ɳ��� ".$objCustomer->getZip();	
				
				}else{
				
				if($objCustomer->getTumbon() != "") $text=  "�Ӻ� ".$objCustomer->getTumbon();
				if($objCustomer->getAmphur() != "") $text.=  " ����� ".$objCustomer->getAmphur();
				if($objCustomer->getProvince() != "") $text.=  " �ѧ��Ѵ ".$objCustomer->getProvince();
				if($objCustomer->getZip() != "") $text.=  " ������ɳ��� ".$objCustomer->getZip();	
				
				}
				
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(410,20,$text,0,0,"L");
				$pdf->Cell(100,20,$objMember->getFirstname()." ".$objMember->getLastname(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(280,20,"",0,0,"L");
				$pdf->Cell(115,20,"",0,0,"C");
				$pdf->Cell(80,20,$objInsureItem->get_payment_number_head(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Ln();
				

				if($objLabel01->getText03() != ""){
					$pdf->SetXY(0,$objLabel01->getText03());
				}	
				
				
				$objPriceList = new InsureItemDetailList();
				$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= ".$hInsureId." and PS.nohead = 1 ");
				$objPriceList->setPageSize(0);
				$objPriceList->setSort(" PS.rank ASC ");
				$objPriceList->load();
				
				forEach($objPriceList->getItemList() as $objItem) {
				
				$objPaymentSubject = new PaymentSubject();
				$objPaymentSubject->setPaymentSubjectId($objItem->getPaymentSubjectId());
				$objPaymentSubject->load();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(360,20,$objPaymentSubject->getTitle(),0,0,"L");
				$pdf->Cell(30,20,"1",0,0,"C");
				$pdf->Cell(70,20,number_format($objItem->getPrice(),2),0,0,"R");
				$pdf->Cell(60,20,number_format($objItem->getPriceAcc(),2),0,0,"R");
				$pdf->Ln();
				}
				
				$num = 7-$objPriceList->mCount;

				$objPriceList = new InsureItemDetailList();
				$objPriceList->setFilter(" OP.order_id =".$hInsureItemId." and OP.order_extra_id= ".$hInsureId." and PS.nohead = 1  ");
				$objPriceList->setPageSize(0);
				$objPriceList->setSort(" PS.rank ASC ");
				$objPriceList->load();
				forEach($objPriceList->getItemList() as $objItem) {
				if($objItem->getPrice() > 0){
					$sumPrice = ($objItem->getPrice()*$objItem->getQty());
					$sumPriceAcc = ($objItem->getPriceAcc()*$objItem->getQty());
					$sumPriceTotal = $sumPriceTotal+($objItem->getPrice()*$objItem->getQty());
					$sumPriceAccTotal = $sumPriceAccTotal+($objItem->getPriceAcc()*$objItem->getQty());
					if($objItem->getVat() == 1){
						if($objItem->getPayin() == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPriceAcc;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPriceAcc;
						}
					}else{
						if($objItem->getPayin() == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPriceAcc;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPriceAcc;
						}
						
					}
				}
				}
				
				
				
				$totalVat = $hPriceIncludeVat+$hPriceExcludeVat;
				$before_vat = $totalVat/1.07;
				$vat = $before_vat*(7/100);
				$total_all = $before_vat+$vat;
				
				if($sumPriceTotal != $sumPriceAccTotal){
				$k--;

				if($sumPriceTotal > $sumPriceAccTotal){
					$pdf->Cell(50,20,"",0,0,"C");
					$pdf->Cell(340,20,"*** �����˵� : �Ҵ���� ".number_format( ($sumPriceTotal-$sumPriceAccTotal),2)." �ҷ",0,0,"C");
					$pdf->Cell(50,20,"",0,0,"C");
					$pdf->Cell(60,20,"",0,0,"R");
					$pdf->Cell(70,20,"",0,0,"R");
					$pdf->Ln();
				}else{
					$pdf->Cell(50,20,"",0,0,"C");
					$pdf->Cell(340,20,"*** �����˵� : �����Թ ".number_format( ($sumPriceAccTotal-$sumPriceTotal),2)." �ҷ",0,0,"C");
					$pdf->Cell(50,20,"",0,0,"C");
					$pdf->Cell(60,20,"",0,0,"R");
					$pdf->Cell(70,20,"",0,0,"R");
					$pdf->Ln();
				}
				
				}
				
				
				
				for($n=1;$n<=$num;$n++){
					if($n == 1){
						if($objInsure->get_k_number() != "") $nText = "�����Ţ�������� ".$objInsure->get_k_number();
						if($objInsure->get_p_number() != "") $nText.= "  �����Ţ�ú. ".$objInsure->get_p_number();
						$pdf->Cell(50,20,"",0,0,"C");
						$pdf->Cell(340,20,$nText,0,0,"L");
						$pdf->Cell(50,20,"",0,0,"C");
						$pdf->Cell(60,20,"",0,0,"R");
						$pdf->Cell(70,20,"",0,0,"R");
						$pdf->Ln();
					}else{
						$pdf->Cell(50,20,"",0,0,"C");
						$pdf->Cell(340,20,"",0,0,"C");
						$pdf->Cell(50,20,"",0,0,"C");
						$pdf->Cell(60,20,"",0,0,"R");
						$pdf->Cell(70,20,"",0,0,"R");
						$pdf->Ln();
					}
				}				
				
				
						

				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(390,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"R");
				$pdf->Cell(60,20,number_format($totalVat,2),0,0,"R");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(390,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"R");
				$pdf->Cell(60,20,number_format($before_vat,2),0,0,"R");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(390,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"R");
				$pdf->Cell(60,20,number_format($vat,2),0,0,"R");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(390,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"R");
				$pdf->Cell(60,20,number_format($total_all,2),0,0,"R");
				$pdf->Ln();
				
				if($objLabel01->getText04() != ""){
					$pdf->SetXY(0,$objLabel01->getText04());
				}

				$objMember = new Member();
				$objMember->setMemberId($objInsureItem->get_acc_by());
				$objMember->load();
				
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(400,20,"",0,0,"C");
				$pdf->Cell(130,20, "( ".$objMember->getFirstname()." ".$objMember->getLastname().")",0,0,"C");
				$pdf->Ln();
				
				

	//Unset($objCustomerList);
	$pdf->Output();	
	?>
	<script>
	//window.close();
	</script>	
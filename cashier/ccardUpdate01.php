<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 0 or type_id=2");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" rank, title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault(" title ASC");
$objFundCompanyList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();

		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();		
		
		$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
		$objCustomer->load();
		
		$objOrderPaymentList = new OrderPaymentList();
		$objOrderPaymentList->setFilter(" order_id = $hId  AND status = 1 ");
		$objOrderPaymentList->setPageSize(0);
		$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
		$objOrderPaymentList->load();

		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 1 ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->setSortDefault(" OP.order_price_id ASC ");
		$objOrderPriceList->load();
		
		//load sale name
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$objSale = new Member();
		$objSale->setMemberId($objOrderBooking->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
		
		
		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
	} else {
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hSendingCustomerId);
            $objCustomer->setTypeId("C");
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard($hBCard);
			$objCustomer->setCCard(1);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			$arrName = explode(" ",$hCustomerName);
			$objCustomer->setFirstname(trim($arrName[0]));
			if($arrName[2] != "") $arrName[1] = trim($arrName[1]." ".$arrName[2]);
			$objCustomer->setlastname($arrName[1]);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			
			$objOrderBooking->setOrderId($hOrderId);
			$objOrderBooking->load();
			
            $objOrder->setOrderId($hOrderId);
			$objOrder->setSendingCustomerId($hSendingCustomerId);
			$objOrder->setSendingNumber($hSendingNumber);
			$objOrder->setSendingNumberNohead($hSendingNumberNohead);
			$hSendingDate = $Year."-".$Month."-".$Day;
            $objOrder->setRecieveDate($hSendingDate);
			$objOrder->setSendingSaleId($hSendingSaleId);
			$objOrder->setCarNumber($hCarNumber);
			$objOrder->setSendingRemark($hSendingRemark);
			$objOrder->setOrderStatus(6);
            $objOrder->setOrderNumber($hOrderNumber); 
			$objOrder->setOrderPrice($hOrderPrice); 
			$objOrder->setStockCarId($hStockCarId);
			$objOrder->setNoRedCode($hNoRedCode);
			$objOrder->setStockRedId($hStockRedId);
			$objOrder->setBuyCompany($hBuyCompany);
			$objOrder->setBuyType($hBuyType);
			
			if($hBookingCarSeries != $hCarSeriesId){
				$objOrder->setSwitchCar(1);
			}else{
				$objOrder->setSwitchCar(0);
			}
			
			if($hSendingCustomerId == "" OR ($hBookingCustomerId != $hSendingCustomerId)){
				$objOrder->setSwitchCustomer(1);
			}else{
				$objOrder->setSwitchCustomer(0);
			}
			
			if($hBookingSaleId != $hSendingSaleId){
				$objOrder->setSwitchSale(1);
			}else{
				$objOrder->setSwitchSale(0);
			}
			
			
    		$pasrErrCustomer = $objCustomer->checkCashierSending($hSubmit);
			$pasrErrOrder = $objOrder->checkCashierSending($hSubmit);
			//Update Price
			if($hUpdatePrice){
					if ($strMode == "Update"){
						$objOrderPrice = new OrderPrice();
					    for ($i = 0; $i < $hCountTotal; $i++) {
					        if ( $hDelete[$i] )  //Delete is checked.
					        {
								$objOrderPrice->setOrderPriceId($hDelete[$i]);
								$objOrderPrice->delete();
							}else{
								$objOrderPrice->setOrderPriceId($hOrderPriceIdTemp[$i]);
								$objOrderPrice->setPaymentSubjectId($hPaymentSubjectIdTemp[$i]);
								$objOrderPrice->setOrderId($hId);
								$objOrderPrice->setRemark($hRemarkTemp[$i]);
								$objOrderPrice->setQty($hQtyTemp[$i]);
								$objOrderPrice->setPrice($hPriceTemp[$i]);
								$objOrderPrice->setVat($hVatTemp[$i]);
								$objOrderPrice->setPayin($hPayinTemp[$i]);
								$objOrderPrice->setStatus(1);
								$objOrderPrice->update();
							}// end if
							
					    }// end for
						unset($objOrderPrice);
						//header("location:ccardUpdate.php?hId=$hId");
						//exit;
						
					}else{
						$arrPaymentSubjectId  = explode(",::",$sPaymentSubjectId);
						$arrRemark = explode(",::",$sRemark);
						$arrQty = explode(",::",$sQty);
						$arrPrice = explode(",::",$sPrice);
						$arrVat = explode(",::",$sVat);
						$arrPayin = explode(",::",$sPayin);

					    for ($i = 0; $i < sizeof($arrPaymentSubjectId);$i++) {
				        	if ( !$hDelete[$i] ){  //Delete is checked.
								$tPaymentSubjectId = $tPaymentSubjectId.",::".$hPaymentSubjectIdTemp[$i];
								$tRemark = $tRemark.",::".$hRemarkTemp[$i];
								$tQty = $tQty.",::".$hQtyTemp[$i];
								$tPrice= $tPrice.",::".$hPriceTemp[$i];
								$tVat= $tVat.",::".$hVatTemp[$i];
								$tPayin= $tPayin.",::".$hPayinTemp[$i];
							}// end if 
				    	}// end for
					}//end if 74
					
					if ($tPaymentSubjectId != ""){
						$tPaymentSubjectId = substr($tPaymentSubjectId,3,strlen($tPaymentSubjectId));
						$tRemark = substr($tRemark,3,strlen($tRemark));
						$tQty = substr($tQty,3,strlen($tQty));
						$tPrice = substr($tPrice,3,strlen($tPrice));
						$tVat = substr($tVat,3,strlen($tVat));
						$tPayin = substr($tPayin,3,strlen($tPayin));
						$sPaymentSubjectId = $tPaymentSubjectId;
						$sRemark = $tRemark;
						$sQty = $tQty;
						$sPrice = $tPrice;
						$sVat = $tVat;
						$sPayin = $tPayin;
					}else{
						$sPaymentSubjectId="";
						$sRemark="";
						$sQty="";
						$sPrice="";
						$sVat="";
						$sPayin="";
					}

			}
			
			if($hAddPrice){
				//Check error
				
				if ($hPaymentSubjectId == "")$asrErr["PaymentSubjectId"]="��س����͡��¡��";
				if ($hQty == ""  )$asrErr["Qty"]="��س��кبӹǹ";
				if ($hPrice == "")$asrErr["Price"]="��س��к��Ҥ�";
				if (sizeof($asrErr) == 0 ){
						$arrPaymentSubjectId = explode("::",$hPaymentSubjectId);
						$hPaymentSubjectId = $arrPaymentSubjectId[0];
				// Add account Id with out session
					if ($strMode == "Update"){
						$objOrderPrice = new OrderPrice();
						$objOrderPrice->setOrderId($hId);
						$objOrderPrice->setPaymentSubjectId($arrPaymentSubjectId[0]);
						$objOrderPrice->setRemark($hRemark);
						$objOrderPrice->setQty($hQty);
						$objOrderPrice->setPrice($hPrice);
						$objOrderPrice->setVat($hVat);
						$objOrderPrice->setPayin($hPayin);
						$objOrderPrice->setStatus(1);
						$objOrderPrice->add();
						unset($objOrderPrice);
						//header("location:ccardUpdate.php?hId=$hId");
						//exit;
						
					}else{
						// Add account Id
						if ($sPaymentSubjectId == ""){
							$sPaymentSubjectId = $hPaymentSubjectId;
							$sRemark = $hRemark;
							$sQty = $hQty;
							$sPrice = $hPrice;
							$sVat = $hVat;
							$sPayin = $hPayin;
						}else{
							$sPaymentSubjectId = $sPaymentSubjectId.",::".$hPaymentSubjectId;
							$sRemark = $sRemark.",::".$hRemark;
							$sQty = $sQty.",::".$hQty;
							$sPrice = $sPrice.",::".$hPrice;
							$sVat = $sVat.",::".$hVat;
							$sPayin = $sPayin.",::".$hPayin;
							$hPaymentSubjectId = "";
						}// end if
					}// end check strmode
					$hPaymentSubjectId = "";
				}// end check length
			
			}

			//Update Payment
			if($hUpdatePayment){
					if ($strMode == "Update"){
						$objOrderPayment = new OrderPayment();
					    for ($i = 0; $i < $hCountTotalPayment; $i++) {
					        if ( $hDelete[$i] )  //Delete is checked.
					        {
								$objOrderPayment->setOrderPaymentId($hDelete[$i]);
								$objOrderPayment->delete();
							}else{
								$objOrderPayment->setOrderPaymentId($hOrderPaymentIdTemp[$i]);
								$objOrderPayment->setPaymentTypeId($hPaymentTypeIdTemp[$i]);
								$objOrderPayment->setPrice($hPaymentPriceTemp[$i]);
								$objOrderPayment->setOrderId($hId);
								$objOrderPayment->setBankId($hBankIdTemp[$i]);
								$objOrderPayment->setBranch($hBankBranchTemp[$i]);
								$objOrderPayment->setCheckNo($hCheckNoTemp[$i]);
								$strCheckDate =  $_POST["Year01Temp$i"]."-".$_POST["Month01Temp$i"]."-".$_POST["Day01Temp$i"];
								$objOrderPayment->setCheckDate($strCheckDate);
								$objOrderPayment->setStatus(1);
								$objOrderPayment->setRemark($hRemarkTemp[$i]);
								$objOrderPayment->setNoHead($hNoHeadTemp[$i]);
								$objOrderPayment->update();
								
								for ($j = 0; $j < $hCountTotalPricePayment; $j++) {
									if($hPricePayment[$i][$j]){
										if($hOrderPricePaymentId[$i][$j] == 0 OR $hOrderPricePaymentId[$i][$j] == "" ){
											$objOrderPricePayment = new OrderPricePayment();
											$objOrderPricePayment->setOrderId($hId);
											$objOrderPricePayment->setOrderPriceId($hOrderPriceIdTemp01[$i][$j]);
											$objOrderPricePayment->setOrderPaymentId($hOrderPaymentIdTemp[$i]);
											$objOrderPricePayment->add();
										}
									}else{
										if($hOrderPricePaymentId[$i][$j] > 0){
											$objOrderPricePayment = new OrderPricePayment();
											$objOrderPricePayment->setOrderPricePaymentId($hOrderPricePaymentId[$i][$j]);
											$objOrderPricePayment->delete();
										}
									}
								}														
								
								
								
							}// end if
							
					    }// end for
						unset($objOrderPayment);
						//header("location:ccardUpdate.php?hId=$hId");
						//exit;
						
					}else{
						$arrPaymentTypeId  = explode(",::",$sPaymentTypeId);
						$arrPaymentPrice  = explode(",::",$sPaymentPrice);
						$arrBankId  = explode(",::",$sBankId);
						$arrBankBranch  = explode(",::",$sBankBranch);
						$arrCheckNo  = explode(",::",$sCheckNo);
						$arrDay01  = explode(",::",$sDay01);
						$arrMonth01  = explode(",::",$sMonth01);
						$arrYear01  = explode(",::",$sYear01);
						$arrRemark  = explode(",::",$sRemark);
						$arrNoHead  = explode(",::",$sNoHead);
						
					    for ($i = 0; $i < sizeof($arrPaymentTypeId);$i++) {
				        	if ( !$hDelete[$i] ){  //Delete is checked.
								$tPaymentTypeId = $tPaymentTypeId.",::".$hPaymentTypeIdTemp[$i];
								$tPaymentPrice = $tPaymentPrice.",::".$hPaymentPriceTemp[$i];
								$tBankId = $tBankId.",::".$hBankIdTemp[$i];
								$tBankBranch = $tBankBranch.",::".$hBankBranchTemp[$i];
								$tCheckNo = $tCheckNo.",::".$hCheckNoTemp[$i];
								$tDay01 = $tDay01.",::".$hDay01Temp[$i];
								$tMonth01 = $tMonth01.",::".$hMonth01Temp[$i];
								$tYear01 = $tYear01.",::".$hYear01Temp[$i];
								$tRemarkId = $tRemarkId.",::".$hRemarkIdTemp[$i];
								$tNoHead = $tNoHead.",::".$hNoHeadTemp[$i];
							}// end if 
				    	}// end for
					}//end if 74
					
					if ($tPaymentTypeId != ""){
						$tPaymentTypeId = substr($tPaymentTypeId,3,strlen($tPaymentTypeId));
						$tPaymentPrice = substr($tPaymentPrice,3,strlen($tPaymentPrice));
						$tBankId = substr($tBankId,3,strlen($tBankId));
						$tBankBranch = substr($tBankBranch,3,strlen($tBankBranch));
						$tCheckNo = substr($tCheckNo,3,strlen($tCheckNo));
						$tDay01 = substr($tDay01,3,strlen($tDay01));
						$tMonth01 = substr($tMonth01,3,strlen($tMonth01));
						$tYear01 = substr($tYear01,3,strlen($tYear01));
						$tRemarkId = substr($tRemarkId,3,strlen($tRemarkId));
						$tNoHead = substr($tNoHead,3,strlen($tNoHead));
						
						$sPaymentTypeId = $tPaymentTypeId;
						$sPaymentPrice = $tPaymentPrice;
						$sBankId = $tBankId;
						$sBankBranch = $tBankBranch;
						$sCheckNo = $tCheckNo;
						$sDay01 = $tDay01;
						$sMonth01 = $tMonth01;
						$sYear01 = $tYear01;
						$sRemarkId = $tRemarkId;
						$sNoHead = $tNoHead;
					}else{
						$sPaymentTypeId="";
						$sPaymentPrice="";
						$sBankId="";
						$sBankBranch="";
						$sCheckNo="";
						$sDay01="";
						$sMonth01="";
						$sYear01="";
						$sRemarkId="";
						$sNoHead="";
					}

			}
			
			if($hAddPayment){
				//Check error
				
				if ($hPaymentTypeId == "")$asrErr["PaymentTypeId"]="��س����͡��¡��";
				if ($hPaymentPrice == ""  )$asrErr["PaymentPrice"]="��س��кبӹǹ�Թ";
				
				if (sizeof($asrErr) == 0 ){
						$arrPaymentTypeId = explode("::",$hPaymentTypeId);
						$hPaymentTypeId = $arrPaymentTypeId[0];
				// Add account Id with out session
					if ($strMode == "Update"){
						$objOrderPayment = new OrderPayment();
						$objOrderPayment->setOrderId($hId);
						$objOrderPayment->setPaymentTypeId($arrPaymentTypeId[0]);
						$objOrderPayment->setPrice($hPaymentPrice);
						$objOrderPayment->setBankId($hBankId);
						$objOrderPayment->setBranch($hBankBranch);
						$objOrderPayment->setCheckNo($hCheckNo);
						$strCheckDate =  $Year01."-".$Month01."-".$Day01;
						$objOrderPayment->setCheckDate($strCheckDate);
						$objOrderPayment->setRemark($hRemark);
						$objOrderPayment->setNoHead($hNoHead);
						$objOrderPayment->setStatus(1);
						$objOrderPayment->add();
						unset($objOrderPayment);
						//header("location:ccardUpdate.php?hId=$hId");
						//exit;
						
					}else{
						// Add account Id
						if ($sPaymentTypeId == ""){
							$sPaymentTypeId = $hPaymentTypeId;
							$sPaymentPrice = $hPaymentPrice;
							$sBankId = $hBankId;
							$sBankBranch = $hBankBranch;
							$sCheckNo = $hCheckNo;
							$sDay01 = $Day01;
							$sMonth01 = $Month01;
							$sYear01 = $Year01;
							$sRemark = $hRemark;
							$sNoHead = $hNoHead;
						}else{
							$sPaymentTypeId = $sPaymentTypeId.",::".$hPaymentTypeId;
							$sPaymentPrice = $sPaymentPrice.",::".$hPaymentPrice;
							$sBankId = $sBankId.",::".$hBankId;
							$sBankBranch = $sBankBranch.",::".$hBankBranch;
							$sCheckNo = $sCheckNo.",::".$hCheckNo;
							$sDay01 = $sDay01.",::".$Day01;
							$sMonth01 = $sMonth01.",::".$Month01;
							$sYear01 = $sYear01.",::".$Year01;
							$sRemark = $sRemark.",::".$hRemark;
							$sNoHead = $sNoHead.",::".$hNoHead;
							$hPaymentTypeId = "";
						}// end if
					}// end check strmode
					$hPaymentTypeId = "";
				}// end check length
			
			}			
			
			
			if (!$hAddPrice and !$hUpdatePrice and !$hAddPayment and !$hUpdatePayment){
			
			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					if($hSendingCustomerId != ""){
						$objCustomer->updateCashierBooking();
					}else{
						$hSendingCustomerId = $objCustomer->addCashierBooking();
					}
					$objOrder->setSendingCustomerId($hSendingCustomerId);
					
					if($hSendingNumber == ""){
					//create booking number
					$objInfo = new Info();
					$objInfo->setInfoId(1);
					$objInfo->load();
					$strSendingNumber = $objInfo->getOrderNumber();
					$objOrder->setSendingNumber($strSendingNumber);
					$objInfo->updateOrderNumberRecent();

					$strSendingNumberNohead = $objInfo->getOrderNumberNohead();
					$objOrder->setSendingNumberNohead($strSendingNumberNohead);
					$objInfo->updateOrderNumberNoheadRecent();
					
					
					}
					$objOrder->updateCashierSending();
					
					//update stock car
					$objStockCar = new StockCar();
					$objStockCar->setStockCarId($hStockCarId);
					$objStockCar->setStatus(2);
					$objStockCar->setCustomerId($hSendingCustomerId);
					$objStockCar->updateStatus();
					
					//update stock red id
					if(isset($hNoRedCode)){
						//remove redcode
						if($hStockRedIdTemp > 0){
							$objStockRed = new StockRed();
							$objStockRed->setStockRedId($hStockRedIdTemp);
							$objStockRed->setStockCarId($hStockCarId);
							$objStockRed->updateStockCarId();
						}
					}else{
						$objStockRed = new StockRed();
						$objStockRed->setStockRedId($hStockRedId);
						$objStockRed->setStockCarId($hStockCarId);
						$objStockRed->updateStockCarId();
					}
				}
				
				unset ($objCustomer);
				unset ($objOrder);
				header("location:ccardPrintPreview.php?hId=$pId$hId");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)
			
			}else{
				$arrDate = explode("-",$objOrder->getRecieveDate());
				$Day = $arrDate[2];
				$Month = $arrDate[1];
				$Year = $arrDate[0];
				
				$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
				$objBookingCustomer->load();
				
				$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objCustomer->load();
				
				$objOrderPaymentList = new OrderPaymentList();
				$objOrderPaymentList->setFilter(" order_id = $hId  AND status = 1 ");
				$objOrderPaymentList->setPageSize(0);
				$objOrderPaymentList->setSortDefault(" order_payment_id ASC ");
				$objOrderPaymentList->load();
		
				$objOrderPriceList = new OrderPriceList();
				$objOrderPriceList->setFilter(" OP.order_id = $hId  AND status = 1 ");
				$objOrderPriceList->setPageSize(0);
				$objOrderPriceList->setSortDefault(" OP.order_price_id ASC ");
				$objOrderPriceList->load();
				
				//load sale name
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getSendingSaleId());
				$objSale->load();
				
				$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
				
				$objSale = new Member();
				$objSale->setMemberId($objOrderBooking->getBookingSaleId());
				$objSale->load();
				
				$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
				
				
				// load stock car and red code
				
				$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
				$objBookingCarColor->load();
				
				$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
				$objBookingCarSeries->load();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
				
			}//ecnd check addprice
		}
}

$pageTitle = "1. ����������ѹ";
$strHead03="�ѹ�֡��¡�����ͺ";
if($strMode == "Add"){ $pageContent = "1.3 �ѹ�֡��¡�����ͺ ";}else{$pageContent = "1.3 �ѹ�֡��¡�����ͺ";}
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
		
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}				
		
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hSendingSaleId.value=="")
		{
			alert("��س����͡��ѡ�ҹ��¨ҡ�к�");
			document.forms.frm01.hSendingSaleId.focus();
			return false;
		} 			

		if( document.frm01.hNoRedCode.checked == false ){
		
			if (document.forms.frm01.hStockRedId.value=="" || document.forms.frm01.hStockRedId.value== "0")
			{
				alert("��س����͡����ᴧ�ҡ�к�");
				document.forms.frm01.hStockRedName.focus();
				return false;
			} 					
		}
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س����͡�ӺŨҡ�Ӻ�");
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س����͡����ͨҡ�к�");
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س����͡�ѧ��Ѵ�ҡ�к�");
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س����͡������ɳ���ҡ�к�");
			document.forms.frm01.hZip.focus();
			return false;
		} 						

		if (document.forms.frm01.hStockCarId.value=="" || document.forms.frm01.hStockCarId.value== 0)
		{
			alert("��س����͡�����Ţ�ѧ�ҡ�к�");
			document.forms.frm01.hCarNumber.focus();
			return false;
		} 			
		
		return true;

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				

<form name="frm01" action="ccardUpdate.php" method="POST" onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">	  
	  <input type="hidden" name="hSendingNumber" value="<?=$objOrder->getSendingNumber()?>">
	  <input type="hidden" name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>">	  
	  <input type="hidden" name="hSendingNumberNohead" value="<?=$objOrder->getSendingNumberNohead()?>">	  
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingSaleId" value="<?=$objOrder->getBookingSaleId()?>">
	  <input type="hidden" name="hBookingCarColor" value="<?=$objOrder->getBookingCarColor()?>">
	  <input type="hidden" name="hBookingCarSeries" value="<?=$objOrder->getBookingCarSeries()?>">
	  <input type="hidden" name="hTrick" value="true">
	  

<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="40%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="10%" valign="top"></td>
			<td width="40%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ������ͺ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="10%" valign="top"><strong>��ѡ�ҹ���</strong></td>
			<td width="40%" valign="top" >
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top"><input type="checkbox" onclick="changeSale()"  value="1" name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>>����¹��ѡ�ҹ���</td>
					<td>
					</td>					
				</tr>
				</table>

				
			</td>
			<td class="i_background03" width="10%" valign="top"></td>
			<td width="40%" valign="top">
						<input type="hidden" readonly size="2" name="hSendingSaleId"  value="<?=$objOrder->getSendingSaleId();?>">
						<input type="text" name="hSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm01.hSendingSaleId.value != '' ) frm01.hBuyType[0].focus();if(event.keyCode !=13 ) frm01.hSendingSaleId.value='';"   value="<?=$hSaleName?>">

			</td>			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>��������ë���</strong></td>
			<td valign="top" ><input type="radio" onclick="showFn(1);" name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 OR $objOrder->getBuyType() == 0) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" name="hBuyType" onclick="showFn(2);" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��</td>
			<td class="i_background03" valign="top"><label id="fn" style="<?if($objOrder->getBuyType()  !=2) echo "display:none";?>"><strong>����ѷ�ṹ��</strong></label></td>
			<td valign="top">
				<label id="fnValue" style="<?if($objOrder->getBuyType() !=2) echo "display:none";?>"><?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?></label>
			</td>			
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>����¹����ᴧ</strong></td>
			<td valign="top">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
					        <input type="hidden"  readonly name="hStockRedIdTemp" size="2"  value="<?=$objOrder->getStockRedId();?>">
							<input type="hidden"  readonly name="hStockRedId" size="2"  value="<?=$objOrder->getStockRedId();?>">	
							<input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>
				
			</td>	
			<td class="i_background03" valign="top"><strong>�ҤҢ��</strong></td>
			<td valign="top">
				<input type="text"  name="hOrderPrice" size="20"  value="<?=$objOrder->getOrderPrice()?>"> �ҷ
			</td>		
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=$objBookingCustomer->getTitle();?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<?=$name?>
					</td>
					<td valign="top">					
					<?$objBookingCustomer->getCustomerTitleId();?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><?=$objBookingCustomer->getAddress();?></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			
			<td  width="40%" valign="top"><?=$objBookingCustomer->getTumbon();?></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><?=$objBookingCustomer->getAmphur();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCustomer" onclick="setCustomerValue()" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹����Ѻ�ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<INPUT  name="hCustomerName" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerId.value != '' ) frm01.hCustomerTitleId.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerId.value='';"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><input type="text" name="hAddress" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><input type="hidden" readonly size="6" name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" name="hTumbon" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';"  size="30"  value="<?=$objCustomer->getTumbon();?>"></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><input type="hidden" readonly size="4" name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text" name="hAmphur" onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';"  size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="2" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text" name="hProvince" onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';"  size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="5" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hCarNumber.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';"  size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#FFCEE7">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="10%"><strong>���ö</strong> </td>
			<td valign="top" width="40%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="10%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="40%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>


<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="10%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>
			
			<td width="40%" ><input type="hidden" size="3" readonly name="hStockCarId" value="<?=$objOrder->getStockCarId()?>"><input type="text" name="hCarNumber" size="30" onKeyDown="if(event.keyCode==13 && frm01.hStockCarId.value != '' ) frm01.hEngineNumber.focus();if(event.keyCode !=13 ) frm01.hStockCarId.value='';"  value="<?=$objStockCar->getCarNumber()?>"></td>
			<td width="10%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><input type="text" name="hEngineNumber" size="30"  value="<?=$objStockCar->getEngineNumber()?>"></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<?$objCarRoleList->printSelect("hCarModelId",$objStockCar->getCarModelId(),"��س����͡");?>
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<?$objCarSeriesList->printSelect("hCarSeriesId",$objStockCar->getCarSeriesId(),"��س����͡");?>
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�����š�ê����Թ</td>
</tr>
</table>

<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td align="center" width="5%" class="listDetail"></td>
	<td align="center" width="20%" class="listDetail02">��¡��</td>
	<td align="center" width="20%" class="listDetail02">��������´</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ</td>
	<td align="center" width="10%" class="listDetail02">˹�����</td>
	<td align="center" width="7%" class="listDetail02">vat</td>
	<td align="center" width="8%" class="listDetail02">�Դź</td>
	<td align="center" width="10%" class="listDetail02"></td>
	<td align="center" width="10%" class="listDetail02"></td>	
</tr>
<tr>
	<td class="listDetail"></td>
	<td align="left" class="listDetail"><?$objPaymentSubjectList->printSelectScript("hPaymentSubjectId",$_POST["hPaymentSubjectId"],"��س����͡��¡��","checkOther();");?></td>
	<td align="center" class="listDetail"><span id="spanRemark" style="display:none"><input type="text" name="hRemark" size="30" value=""></span></td>
	<td align="center" class="listDetail"><input type="text" style="text-align=right;" name="hQty" size="5"  value="1"></td>
	<td align="center" class="listDetail"><input type="text" style="text-align=right;" name="hPrice" size="15" value=""></td>
	<td align="center" class="listDetail"><input type="checkbox" name="hVat" value="1"></td>
	<td align="center" class="listDetail"><input type="checkbox" name="hPayin" value="1"></td>
	<td align="center" class="listDetail"></td>
	<td align="center" class="listDetail"><input type="Submit" name="hAddPrice"  class="button" onclick="return checkPrice()" value="&nbsp;&nbsp;������¡��&nbsp;&nbsp;"></td>
</tr>
		<input type="hidden" name="sPaymentSubjectId" value="<?=$sPaymentSubjectId?>">
		<input type="hidden" name="sRemark" value="<?=$sRemark?>">
		<input type="hidden" name="sQty" value="<?=$sQty?>">
		<input type="hidden" name="sPrice" value="<?=$sPrice?>">
		<input type="hidden" name="sVat" value="<?=$sVat?>">
		<input type="hidden" name="sPayin" value="<?=$sPayin?>">
<?if($strMode == "Update"){		
		if ($objOrderPriceList->getCount() > 0 ){
?>	
<tr>
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle">��������´</td>
	<td align="center"  class="listTitle">�ӹǹ</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle">vat</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center"  class="listTitle">�ӹǹ�Թ</td>
	<td align="center"  class="listTitle">ź��¡��</td>	
</tr>
				<?
				$i=0;
				forEach($objOrderPriceList->getItemList() as $objItem) {?>
				<input type="hidden"  name="hPaymentSubjectIdTemp[<?=$i?>]" value="<?=$objItem->getPaymentSubjectId();?>">
				<input type="hidden"  name="hOrderPriceIdTemp[<?=$i?>]" value="<?=$objItem->getOrderPriceId();?>">
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td align="left" class="ListDetail">						
						<?$objPaymentSubjectList->printSelect("hPaymentSubjectIdTemp[$i]",$objItem->getPaymentSubjectId(), "��س����͡��¡��");?>
					</td>
					<td align="center" class="ListDetail"><input type="text"  size="30" name="hRemarkTemp[<?=$i?>]" value="<?=$objItem->getRemark();?>"></td>
					<td align="center" class="ListDetail"><input type="text" style="text-align=right;"   size="5" name="hQtyTemp[<?=$i?>]" value="<?=$objItem->getQty();?>"></td>
					<td align="right" class="ListDetail"><input type="text" style="text-align=right;"   size="15" name="hPriceTemp[<?=$i?>]" value="<?=$objItem->getPrice();?>"></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hVatTemp[<?=$i?>]" value="1" <?if($objItem->getVat() == 1) echo "checked";?> ></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hPayinTemp[<?=$i?>]" value="1" <?if($objItem->getPayin() == 1) echo "checked";?> ></td>
					<?$sumPrice = ($objItem->getPrice()*$objItem->getQty())?>
					<td align="right" class="ListDetail"><?=number_format($sumPrice,2);?></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$objItem->getOrderPriceId()?>"></td>
				</tr>
					<?
					if($objItem->getVat() == 1){
						if($objItem->getPayin() == 1){
							$hPriceIncludeVat = $hPriceIncludeVat-$sumPrice;
						}else{
							$hPriceIncludeVat = $hPriceIncludeVat+$sumPrice;
						}
					}else{
						if($objItem->getPayin() == 1){
							$hPriceExcludeVat = $hPriceExcludeVat-$sumPrice;
						}else{
							$hPriceExcludeVat = $hPriceExcludeVat+$sumPrice;
						}
						
					}
				?>				
				<?
					++$i;
				}
				$grandTotal = $hPriceIncludeVat+$hPriceExcludeVat;
				if($hPriceIncludeVat > 0){
					$totalVat = $hPriceIncludeVat/1.07;
					$vat = $hPriceIncludeVat-$totalVat;
				}
				$totalVat = $totalVat+$hPriceExcludeVat;
				
				?>
				<input type="hidden" name="hCountTotal" value="<?=$i?>">
<tr>
	<td align="center" class="listDetail" colspan="5" rowspan="2" >
		<table width="100%">
		<tr>
			<td valign="top" width="50">�����˵�</td>
			<td><textarea name="hSendingRemark" rows="3" cols="70"><?=$objOrder->getSendingRemark()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td align="right"  class="listDetail" colspan="2"><strong>����Թ</strong></td>		
	<td align="right"  class="error"><?=number_format($totalVat,2);?></td>	
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePrice" value="��Ѻ��ا��¡��" class="button"></td>	
</tr>				
<tr>	
	<td align="right"  class="listDetail" colspan="2"><strong>������Ť������</strong></td>	
	<td align="right"  class="error"><?=number_format($vat,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>
<tr>
	<td align="center" class="listDetail" colspan="5">
		<table width="100%">
		<tr>
			<td valign="top" width="50" >�ӹǹ�Թ</td>
			<td  class="error"><?currencyThai($grandTotal);?>&nbsp;�ҷ��ǹ</td>
			<td></td>
		</tr>
		</table>	
	</td>
	<td align="right" class="listDetail" colspan="2"><strong>�ʹ�ط��</strong></td>	
	<td align="right"  class="error"><?=number_format($grandTotal,2);?></td>	
	<td align="" class="listDetail"></td>	
</tr>


		<?}?>
		
	<?}?>

</table>

	</td>
</tr>
</table>
<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">�������ٻẺ��ê����Թ</td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" align="center" cellpadding="3" cellspacing="1">
<tr>
	<td class="listDetail" width="5%"></td>
	<td align="center" width="15%" class="listDetail02">�ٻẺ��ê����Թ</td>
	<td align="center" width="10%" class="listDetail02">�ӹǹ�Թ</td>
	<td align="center" width="15%" class="listDetail02">��Ҥ��</td>
	<td align="center" width="10%" class="listDetail02">�Ң�</td>
	<td align="center" width="10%" class="listDetail02">�Ţ�����</td>
	<td align="center" width="15%" class="listDetail02">ŧ�ѹ���</td>
	<td align="center" width="15%" class="listDetail02">�����˵�</td>
	<td class="listDetail" width="10%"></td>
</tr>
<tr>
	<td class="listDetail"></td>	
	<td><?$objPaymentTypeList->printSelect("hPaymentTypeId","","��س����͡��¡��");?></td>
	<td align="center"><input type="text"  style="text-align=right;"  name="hPaymentPrice" size="10"  value=""></td>
	<td><?$objBankList->printSelect("hBankId","","��س����͡��¡��");?></td>
	<td><input type="text" name="hBankBranch" size="15"  value=""></td>
	<td align="center"><input type="text" name="hCheckNo" size="10"  value=""></td>
	<td align="center">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01  value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>
		</tr>
		</table>	
	
	</td>
	<td align="center"><input type="text" name="hRemark" size="10"  value=""></td>
	<td align="center" class="listDetail"><input type="Submit" name="hAddPayment"  class="button" onclick="return checkPayment()" value="&nbsp;&nbsp;������¡��&nbsp;&nbsp;"></td>
</tr>
		<input type="hidden" name="sPaymentTypeId" value="<?=$sPaymentTypeId?>">
		<input type="hidden" name="sNoHead" value="<?=$sNoHead?>">
		<input type="hidden" name="sPaymentPrice" value="<?=$sPaymentPrice?>">
		<input type="hidden" name="sBankId" value="<?=$sBankId?>">
		<input type="hidden" name="sBankBranch" value="<?=$sBankBranch?>">
		<input type="hidden" name="sCheckNo" value="<?=$sCheckNo?>">
		<input type="hidden" name="sDay01" value="<?=$sDay01?>">
		<input type="hidden" name="sMonth01" value="<?=$sMonth01?>">
		<input type="hidden" name="sYear01" value="<?=$sYear01?>">
		<input type="hidden" name="sRemark" value="<?=$sRemark?>">
<?if($strMode == "Update"){	
		if ($objOrderPaymentList->getCount() > 0 ){?>	
<tr>
	<td align="center" class="listTitle">�ӴѺ</td>
	<td align="center" class="listTitle">�ٻẺ��ê����Թ</td>
	<td align="center" class="listTitle">�ӹǹ�Թ</td>
	<td align="center" class="listTitle">��Ҥ��</td>
	<td align="center" class="listTitle">�Ң�</td>
	<td align="center" class="listTitle">�Ţ�����</td>
	<td align="center" class="listTitle">ŧ�ѹ���</td>
	<td align="center" class="listTitle">�����˵�</td>
	<td align="center" class="listTitle">ź��¡��</td>
</tr>
				<?
				$i=0;
				forEach($objOrderPaymentList->getItemList() as $objItem) {
					$arrDate = explode("-",$objItem->getCheckDate());
				?>
				<input type="hidden"  name="hPaymentTypeIdTemp[<?=$i?>]" value="<?=$objItem->getPaymentTypeId();?>">
				<input type="hidden"  name="hOrderPaymentIdTemp[<?=$i?>]" value="<?=$objItem->getOrderPaymentId();?>">
				<tr>
					<td align="right" class="ListDetail"><?=$i+1?></td>
					<td align="left" class="ListDetail">						
						<?$objPaymentTypeList->printSelect("hPaymentTypeIdTemp[$i]",$objItem->getPaymentTypeId(), "��س����͡��¡��");?>
					</td>
					<td align="center" class="ListDetail"><input type="text"  style="text-align=right;"   size="10" name="hPaymentPriceTemp[<?=$i?>]" value="<?=$objItem->getPrice();?>"></td>
					<td align="left" class="ListDetail">						
						<?$objBankList->printSelect("hBankIdTemp[$i]",$objItem->getBankId(), "��س����͡��¡��");?>
					</td>					
					<td align="right" class="ListDetail"><input type="text"   size="15" name="hBankBranchTemp[<?=$i?>]" value="<?=$objItem->getBranch();?>"></td>
					<td align="center" class="ListDetail"><input type="text"   size="10" name="hCheckNoTemp[<?=$i?>]" value="<?=$objItem->getCheckNo();?>"></td>
					<td align="center" class="ListDetail">
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Temp<?=$i?>  value="<?=$arrDate[2];?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Temp<?=$i?> value="<?=$arrDate[1];?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Temp<?=$i?> value="<?=$arrDate[0];?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Temp<?=$i?>,Day01Temp<?=$i?>, Month01Temp<?=$i?>, Year01Temp<?=$i?>,popCal);return false"></td>
						</tr>
						</table>		
					
					</td>
					<td align="center" class="ListDetail"><input type="text"   size="10" name="hRemarkTemp[<?=$i?>]" value="<?=$objItem->getRemark()?>"></td>
					<td align="center" class="ListDetail"><input type="checkbox" name="hDelete[<?=$i?>]" value="<?=$objItem->getOrderPaymentId()?>"></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="7">
						<table>
						<tr>
						<?$k=1;
							$r=0;
						forEach($objOrderPriceList->getItemList() as $objItem01) {
						$objOrderPricePayment = new OrderPricePayment();
						$objOrderPricePayment->loadByCondition(" order_id=$hId AND order_price_id = ".$objItem01->getOrderPriceId()." AND order_payment_id= ".$objItem->getOrderPaymentId()."  ");
						?>
						<td width="20%">
							<input type="hidden" name="hOrderPricePaymentId[<?=$i?>][<?=$r?>]" value="<?=$objOrderPricePayment->getOrderPricePaymentId();?>">
							<input type="hidden" name="hOrderPriceIdTemp01[<?=$i?>][<?=$r?>]" value="<?=$objItem01->getOrderPriceId();?>">
							<input value="1" type="checkbox" <?if($objOrderPricePayment->getOrderPricePaymentId() != "") echo "checked"?> name="hPricePayment[<?=$i?>][<?=$r?>]"><?=$objItem01->getPaymentSubjectTitle()?>,<?=$objItem01->getPrice()?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>	
						<?
						if(fmod($k,5) ==0){
							echo "</tr><tr>";
						}else{
						
						}
						$k++;
						$r++;
						}?>
						</table>
					</td>
					<td></td>
				</tr>				
				<?
					++$i;
				}
				unset($objOrderPaymentList);
				?>
				<input type="hidden" name="hCountTotalPayment" value="<?=$i?>">
				<input type="hidden" name="hCountTotalPricePayment" value="<?=$r?>">
<tr>
	<td align="left" class="listDetail" colspan="8" ></td>
	<td align="center" class="listDetail"><input type="Submit" name="hUpdatePayment" value="��Ѻ��ا��¡��" class="button"></td>	
</tr>				



		<?}?>		

<?}?>


		</table>
	</td>
</tr>
</table>
<br>
<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}else{?>
	<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='ccardUpdate.php'" class="button">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {return "ccardAutoCustomer.php?q=" + this.text.value;});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {return "ccardAutoOrderNumber.php?q=" + this.text.value;});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {var str = this.text.value;	if( str.length > 2){return "acardAutoTumbon.php?q=" + this.text.value;}});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {var str = this.text.value;if( str.length > 2){return "acardAutoAmphur.php?q=" + this.text.value;}});		
	
	new CAPXOUS.AutoComplete("hZip", function() {var str = this.text.value;	if( str.length > 2){return "acardAutoTumbon.php?q=" + this.text.value;}});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "ccardAutoSale.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hCarNumber", function() {
		return "ccardAutoStockCar.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hStockRedName", function() {
		return "ccardAutoRedCode.php?q=" + this.text.value;
	});	
	
</script>
<script>
	function setCustomerValue(){
		if(document.frm01.hSwitchCustomer.checked == true){
		document.frm01.hCustomerName.value= "";
		document.frm01.hSendingCustomerId.value= "";
		document.frm01.hTitle.value= "";
		document.frm01.hCustomerTitleId.value= "";
		document.frm01.hAddress.value= "";
		document.frm01.hTumbon.value= "";
		document.frm01.hAmphur.value= "";
		document.frm01.hProvince.value= "";
		document.frm01.hTumbonCode.value= "";
		document.frm01.hAmphurCode.value= "";
		document.frm01.hProvinceCode.value= "";		
		document.frm01.hZip.value= "";
		document.frm01.hZipCode.value= "";
		document.frm01.hTitle.focus();
		}else{
			//retrive old value
		document.frm01.hCustomerName.value= "<?=$objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();?>";
		document.frm01.hSendingCustomerId.value= "<?=$objBookingCustomer->getCustomerId()?>";
		document.frm01.hTitle.value= "<?=$objBookingCustomer->getTitle()?>";
		document.frm01.hCustomerTitleId.value= "<?=$objBookingCustomer->getCustomerTitleId()?>";
		document.frm01.hAddress.value= "<?=$objBookingCustomer->getAddress()?>";
		document.frm01.hTumbon.value= "<?=$objBookingCustomer->getTumbon()?>";
		document.frm01.hAmphur.value= "<?=$objBookingCustomer->getAmphur()?>";
		document.frm01.hProvince.value= "<?=$objBookingCustomer->getProvince()?>";
		document.frm01.hTumbonCode.value= "<?=$objBookingCustomer->getTumbonCode()?>";
		document.frm01.hAmphurCode.value= "<?=$objBookingCustomer->getAmphurCode()?>";
		document.frm01.hProvinceCode.value= "<?=$objBookingCustomer->getProvinceCode()?>";		
		document.frm01.hZip.value= "<?=$objBookingCustomer->getZip()?>";
		document.frm01.hZipCode.value= "<?=$objBookingCustomer->getZip()?>";

		}

	}

	function checkAddPrice(){
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡��");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentSubjectId.focus();
			return false;
		} 

		if (document.forms.frm01.hQty.value=="")
		{
			alert("��س��кبӹǹ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hQty.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hPrice.value=="")
		{
			alert("��س��к��Ҥ�");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPrice.focus();
			return false;
		} 			
		
		return true;	
	
	}
	
	function showBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "";	
		document.getElementById("showBookingCustomer").style.display = "none";		
		document.getElementById("hideBookingCustomer").style.display = "";		
	}

	function hideBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "none";	
		document.getElementById("showBookingCustomer").style.display = "";			
		document.getElementById("hideBookingCustomer").style.display = "none";
	}

	function showBookingOrder(){
		document.getElementById("bookingOrder").style.display = "";	
		document.getElementById("showBookingOrder").style.display = "none";		
		document.getElementById("hideBookingOrder").style.display = "";		
	}

	function hideBookingOrder(){
		document.getElementById("bookingOrder").style.display = "none";	
		document.getElementById("showBookingOrder").style.display = "";			
		document.getElementById("hideBookingOrder").style.display = "none";
	}
	
	function showBookingCar(){
		document.getElementById("bookingCar").style.display = "";	
		document.getElementById("showBookingCar").style.display = "none";		
		document.getElementById("hideBookingCar").style.display = "";		
	}

	function hideBookingCar(){
		document.getElementById("bookingCar").style.display = "none";	
		document.getElementById("showBookingCar").style.display = "";			
		document.getElementById("hideBookingCar").style.display = "none";
	}
	
	function showFn(val){
		if(val ==1){
			document.getElementById("fn").style.display = "none";	
			document.getElementById("fnValue").style.display = "none";	
		}else{
			document.getElementById("fn").style.display = "";	
			document.getElementById("fnValue").style.display = "";	
		}
	}
	
</script>
<script>

	function checkOther(){
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="15")
		{
			document.getElementById("spanRemark").style.display = "";	
			document.frm01.hRemark.focus();
		}else{
			document.getElementById("spanRemark").style.display = "none";	
			document.frm01.hQty.focus();
		}
	}
	
	function changeSale(){
		if(document.forms.frm01.hSwitchSale.checked == true){
			document.frm01.hSendingSaleId.value= "";
			document.frm01.hSaleName.value= "";		
			document.frm01.hSaleName.focus();		
		}else{
			document.frm01.hSendingSaleId.value= "<?=$objOrder->getBookingSaleId()?>";
			document.frm01.hSaleName.value= "<?=$hBookingSaleName?>";
		}	
	}
	
	function checkPrice(){		
		if (document.forms.frm01.hPaymentSubjectId.options[frm01.hPaymentSubjectId.selectedIndex].value=="0")
		{
			alert("��س����͡��¡�ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentSubjectId.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hQty.value =="" || document.forms.frm01.hQty.value < 0 )
		{
			alert("��س��кبӹǹ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hQty.focus();
			return false;
		} 				
		
		if (document.forms.frm01.hPrice.value =="" || document.forms.frm01.hPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPrice.focus();
			return false;
		} 
		return true;
		
	}
	
	function checkPayment(){
	
		if (document.forms.frm01.hPaymentTypeId.options[frm01.hPaymentTypeId.selectedIndex].value=="0")
		{
			alert("��س����͡�ٻẺ��ê����Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentTypeId.focus();
			return false;
		} 					

		if (document.forms.frm01.hPaymentPrice.value =="" || document.forms.frm01.hPaymentPrice.value < 0 )
		{
			alert("��س��кبӹǹ�Թ");
			document.frm01.hTrick.value="false";		
			document.forms.frm01.hPaymentPrice.focus();
			return false;
		} 
		return true;
	}
	
	function checkRedCode(){
		if(document.frm01.hNoRedCode.checked == true){
			document.frm01.hStockRedId.value = "";
			document.frm01.hStockRedName.value="";
		}
	}
	
	
</script>


<?
	include("h_footer.php")
?>
<?
ini_set('memory_limit', '-1');
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,20,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

				$objISIDL = new InsureMailItemDetailList();
				$objISIDL->setFilter(" O.insure_mail_item_id = $hId  and O.cancel=0 ");
				$objISIDL->setPageSize(0);
				$objISIDL->setSort(" code ASC  ");
				$objISIDL->load();
				
				$hSize = $objISIDL->mCount;
				unset($objISIDL);
				$objISIDL = null;
				
				$objInsureMailItem = new InsureMailItem();
				$objInsureMailItem->set_insure_mail_item_id($hId);
				$objInsureMailItem->load();

				$objInsureStockItem = new InsureMailItem();
				$objInsureStockItem->set_insure_mail_item_id($hId);
				$objInsureStockItem->load();

				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				


				
				if($hSize > 25){
				
				$hXId = 1;
				$i=0;
				$hNum = ceil($hSize/25);
				$hNum1=1;
				while($i < $hSize){
				
				$pdf->AddPage();
				$pdf->SetLeftMargin(20);
				
				$strtext = $objLabel->getText01();
				$pdf->Ln();
				$pdf->SetFont('angsa','B',14);
				$image1 = "images/post.jpg";
				$pdf->Image($image1,20,10,150,40);
				
				$pdf->Cell(550,20,"��Ѻ�ҡ���",0,0,"C");
				$pdf->Cell(50,20,$hNum1."/".$hNum,0,0,"L");
				$pdf->Ln();

				$pdf->Cell(550,20,"RECEIPT FOR BULK POSTING",0,0,"C");
				$pdf->Ln();				
				if($hMailType ==1){
				$pdf->Cell(550,20,"��ɳ����ǹ ����� EMS",0,0,"R");
				}
				if($hMailType ==2){
				$pdf->Cell(550,20,"��ɳ��� ŧ����¹",0,0,"R");
				}
				$pdf->Ln();
				
				$pdf->Cell(100,20,"�ҡ / From",0,0,"L");
				$pdf->Cell(500,20,$objLabel->getText01(),0,0,"L");
				$pdf->Ln();				
				
				$pdf->Cell(100,20,"������� / Address",0,0,"L");
				$pdf->Cell(500,20,$objLabel->getText06(),0,0,"L");
				$pdf->Ln();			
				
				$pdf->Cell(100,20,"�ѹ/��͹/�� �ҡ��",0,0,"L");
				$pdf->Cell(500,20,formatShortThaiDate($objInsureStockItem->get_from_date()),0,0,"L");
				$pdf->Ln();			
				
				$pdf->SetFont('angsa','',14);	
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(150,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(60,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(85,20,"���ѧ���",0,0,"C");
				$pdf->Cell(70,20,"��һ�Ш��ѹ",0,0,"C");
				$pdf->Ln();			
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(150,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(60,20,"",0,0,"C");
				$pdf->Cell(50,20,$objInsureMailItem->get_code(),0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(85,20,"As Follows",0,0,"C");
				$pdf->Cell(70,20,"Date Stamp",0,0,"C");
				$pdf->Ln();							
				$pdf->SetFont('angsa','B',14);	
				$pdf->Cell(25,20,"�ӴѺ",1,0,"C");
				$pdf->Cell(150,20,"�������Ѻ",1,0,"C");
				$pdf->Cell(130,20,"���·ҧ",1,0,"C");
				$pdf->Cell(50,20,"�Ţ���",1,0,"C");
				$pdf->Cell(40,20,"���˹ѡ",1,0,"C");
				$pdf->Cell(85,20,"��Һ�ԡ��",1,0,"C");
				$pdf->Cell(70,20,"�����˵�",1,0,"C");
				$pdf->Ln();

				$pdf->Cell(25,20,"No.",1,0,"C");
				$pdf->Cell(150,20,"Name of Address",1,0,"C");
				$pdf->Cell(70,20,"(�ѧ��Ѵ)",1,0,"C");
				$pdf->Cell(60,20,"(������ɳ���)",1,0,"C");
				$pdf->Cell(50,20,"Number",1,0,"C");
				$pdf->Cell(40,20,"(����)",1,0,"C");
				$pdf->Cell(45,20,"�ҷ(Baht)",1,0,"C");
				$pdf->Cell(40,20,"ʵ.(Stg.)",1,0,"C");
				$pdf->Cell(70,20,"Remarks",1,0,"C");
				$pdf->Ln();				
				$pdf->SetFont('angsa','',14);

				$objISIDL = new InsureMailItemDetailList();
				$objISIDL->setFilter(" O.insure_mail_item_id = $hId and  O.code > '$hXId'   and O.cancel=0  ");
				//echo " O.insure_mail_item_id = $hId and  O.code > '$hXId'   and O.cancel=0  ";
				$objISIDL->setPageSize(25);
				$objISIDL->setSort(" O.code ASC  ");
				$objISIDL->load();
				
				//$pdf->Cell(70,20," O.insure_mail_item_id = $hId and O.code > $hXId ",1,0,"C");
				//$pdf->Ln();				
				forEach($objISIDL->getItemList() as $objItem01) {
							
					$objItem = new Insure();
					$objItem->set_insure_id($objItem01->get_insure_id());
					$objItem->load();
					
					$objInsureCar = new InsureCar();
					$objInsureCar->set_car_id($objItem->get_car_id());
					$objInsureCar->load();		
			
					$objIC = new InsureCustomer();
					$objIC->setInsureCustomerId($objInsureCar->get_insure_customer_id());
					$objIC->load();
					
					$code = substr($objItem01->get_code(),-7);
					$code = substr($code,0,5);
							
							$pdf->Cell(25,20,$i+1,1,0,"C");
							$pdf->Cell(150,20,$objIC->getTitleLabel()." ".$objIC->getFirstname()." ".$objIC->getLastname(),1,0,"L");
							$pdf->Cell(70,20,$objIC->getProvince(),1,0,"C");
							$pdf->Cell(60,20,$objIC->getZip(),1,0,"C");
							$pdf->Cell(50,20,$code,1,0,"C");
							$pdf->Cell(40,20,"",1,0,"C");
							$pdf->Cell(45,20,"",1,0,"C");
							$pdf->Cell(40,20,"",1,0,"C");
							$pdf->Cell(70,20,"",1,0,"C");
							$pdf->Ln();
							$hXId = $objItem01->get_code();
							$i++;
							
				unset($objInsureCar);
				unset($objIC);
				unset($code);
				unset($objItem);

				}
				
				

				
				
				if(($i+1)>$hSize){
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"���������",0,0,"C");
				$pdf->Cell(200,20,"..........................................................................................",0,0,"C");
				$pdf->Cell(50,20,"��Ѻ/���",0,0,"C");
				$pdf->Cell(40,20,"���Թ",0,0,"C");
				$pdf->Cell(45,20,"",1,0,"C");
				$pdf->Cell(40,20,"",1,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Ln();		
				
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"Total",0,0,"C");
				$pdf->Cell(200,20,"",0,0,"C");
				$pdf->Cell(50,20,"Pieces",0,0,"C");
				$pdf->Cell(40,20,"Amount",0,0,"C");
				$pdf->Cell(45,20,"",0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Ln();		


				$pdf->Cell(550,20,"��Ѻ�ҡ���������ѡ�ҹ��ýҡ�� �ô���ѡ����騴������ء���ͺ�ǹ ��� �������� 6 ��͹ (੾�� EMS 4 ��͹) �Ѻ�Ѵ�ҡ�ѹ���ҡ��",0,0,"L");
				$pdf->Ln();		

				$pdf->Cell(550,20,"��õԴ��������ͧ�����ǡѺ��ýҡ�� ��ͧ����Ѻ�ҡ��Ѻ������ʴ��ء���� �ԩй�� ���. �Ҩ���ӡ�õ�Ǩ�ͺ�����ͺ�ǹ���",0,0,"L");
				$pdf->Ln();						

				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"",0,0,"C");
				$pdf->Cell(200,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(60,20,"��ѡ�ҹ�Ѻ�ҡ",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Ln();		

				
				}
				
				$hNum1++;

				
				}//end while
				
				
				
				}else{
				
				
				$pdf->AddPage();
				$pdf->SetLeftMargin(20);
				
				$strtext = $objLabel->getText01();
				$pdf->Ln();
				$pdf->SetFont('angsa','B',14);
				$image1 = "images/post.jpg";
				$pdf->Image($image1,20,10,150,40);
				
				$pdf->Cell(550,20,"��Ѻ�ҡ���",0,0,"C");
				$pdf->Ln();

				$pdf->Cell(550,20,"RECEIPT FOR BULK POSTING ".$hSize,0,0,"C");
				$pdf->Ln();				
				if($hMailType ==1){
				$pdf->Cell(550,20,"��ɳ����ǹ ����� EMS",0,0,"R");
				}
				if($hMailType ==2){
				$pdf->Cell(550,20,"��ɳ��� ŧ����¹",0,0,"R");
				}
				$pdf->Ln();
				
				$pdf->Cell(100,20,"�ҡ / From",0,0,"L");
				$pdf->Cell(500,20,$objLabel->getText01(),0,0,"L");
				$pdf->Ln();				
				
				$pdf->Cell(100,20,"������� / Address",0,0,"L");
				$pdf->Cell(500,20,$objLabel->getText06(),0,0,"L");
				$pdf->Ln();			
				
				$pdf->Cell(100,20,"�ѹ/��͹/�� �ҡ��",0,0,"L");
				$pdf->Cell(500,20,formatShortThaiDate($objInsureStockItem->get_from_date()),0,0,"L");
				$pdf->Ln();			
				
				$pdf->SetFont('angsa','',14);	
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(150,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(60,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(85,20,"���ѧ���",0,0,"C");
				$pdf->Cell(70,20,"��һ�Ш��ѹ",0,0,"C");
				$pdf->Ln();			
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(150,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(60,20,"",0,0,"C");
				$pdf->Cell(50,20,$objInsureMailItem->get_code(),0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(85,20,"As Follows",0,0,"C");
				$pdf->Cell(70,20,"Date Stamp",0,0,"C");
				$pdf->Ln();							
				$pdf->SetFont('angsa','B',14);	
				$pdf->Cell(25,20,"�ӴѺ",1,0,"C");
				$pdf->Cell(150,20,"�������Ѻ",1,0,"C");
				$pdf->Cell(130,20,"���·ҧ",1,0,"C");
				$pdf->Cell(50,20,"�Ţ���",1,0,"C");
				$pdf->Cell(40,20,"���˹ѡ",1,0,"C");
				$pdf->Cell(85,20,"��Һ�ԡ��",1,0,"C");
				$pdf->Cell(70,20,"�����˵�",1,0,"C");
				$pdf->Ln();

				$pdf->Cell(25,20,"No.",1,0,"C");
				$pdf->Cell(150,20,"Name of Address",1,0,"C");
				$pdf->Cell(70,20,"(�ѧ��Ѵ)",1,0,"C");
				$pdf->Cell(60,20,"(������ɳ���)",1,0,"C");
				$pdf->Cell(50,20,"Number",1,0,"C");
				$pdf->Cell(40,20,"(����)",1,0,"C");
				$pdf->Cell(45,20,"�ҷ(Baht)",1,0,"C");
				$pdf->Cell(40,20,"ʵ.(Stg.)",1,0,"C");
				$pdf->Cell(70,20,"Remarks",1,0,"C");
				$pdf->Ln();				
				$pdf->SetFont('angsa','',14);	
				
				
				
	$objISIDL = new InsureMailItemDetailList();
	$objISIDL->setFilter(" O.insure_mail_item_id = $hId   and O.cancel=0 ");
	$objISIDL->setPageSize(0);
	$objISIDL->setSort(" O.code  ASC  ");
	$objISIDL->load();
	$i=1;
	forEach($objISIDL->getItemList() as $objItem01) {
				
		$objItem = new Insure();
		$objItem->set_insure_id($objItem01->get_insure_id());
		$objItem->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();		

		$objIC = new InsureCustomer();
		$objIC->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objIC->load();
		
		$code = substr($objItem01->get_code(),-7);
		$code = substr($code,0,5);
				
				$pdf->Cell(25,20,$i,1,0,"C");
				$pdf->Cell(150,20,$objIC->getTitleLabel()." ".$objIC->getFirstname()." ".$objIC->getLastname(),1,0,"L");
				$pdf->Cell(70,20,$objIC->getProvince(),1,0,"C");
				$pdf->Cell(60,20,$objIC->getZip(),1,0,"C");
				$pdf->Cell(50,20,$code,1,0,"C");
				$pdf->Cell(40,20,"",1,0,"C");
				$pdf->Cell(45,20,"",1,0,"C");
				$pdf->Cell(40,20,"",1,0,"C");
				$pdf->Cell(70,20,"",1,0,"C");
				$pdf->Ln();
		$i++;
		
		unset($objInsureCar);
		unset($objIC);
		unset($code);
		unset($objItem);

	}
	


				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"���������",0,0,"C");
				$pdf->Cell(200,20,"..........................................................................................",0,0,"C");
				$pdf->Cell(50,20,"��Ѻ/���",0,0,"C");
				$pdf->Cell(40,20,"���Թ",0,0,"C");
				$pdf->Cell(45,20,"",1,0,"C");
				$pdf->Cell(40,20,"",1,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Ln();		
				
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"Total",0,0,"C");
				$pdf->Cell(200,20,"",0,0,"C");
				$pdf->Cell(50,20,"Pieces",0,0,"C");
				$pdf->Cell(40,20,"Amount",0,0,"C");
				$pdf->Cell(45,20,"",0,0,"C");
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Ln();		


				$pdf->Cell(550,20,"��Ѻ�ҡ���������ѡ�ҹ��ýҡ�� �ô���ѡ����騴������ء���ͺ�ǹ ��� �������� 6 ��͹ (੾�� EMS 4 ��͹) �Ѻ�Ѵ�ҡ�ѹ���ҡ��",0,0,"L");
				$pdf->Ln();		

				$pdf->Cell(550,20,"��õԴ��������ͧ�����ǡѺ��ýҡ�� ��ͧ����Ѻ�ҡ��Ѻ������ʴ��ء���� �ԩй�� ���. �Ҩ���ӡ�õ�Ǩ�ͺ�����ͺ�ǹ���",0,0,"L");
				$pdf->Ln();						

				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"",0,0,"C");
				$pdf->Cell(200,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(60,20,"��ѡ�ҹ�Ѻ�ҡ",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Ln();		
				
				
				
	
	
				}//end size > 30
				
	$pdf->Output();	
	?>
	<script>
	//window.close();
	</script>	
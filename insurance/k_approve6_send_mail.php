<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,20,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

				//pageing
				$objISIDL = new InsureStockItemDetailList();
				$objISIDL->setFilter(" O.insure_stock_item_id = $hId ");
				$objISIDL->setPageSize(0);
				$objISIDL->setSort(" SS.insure_company_id, k_number ASC  ");
				$objISIDL->load();
				
				$mAll = $objISIDL->mCount;
				$mPage = ceil($mAll/25);
				
				
				//end paging
				
				
				
				
				for($vv = 1;$vv <= $mPage;$vv++){
				$pdf->AddPage();
				$pdf->SetLeftMargin(20);

				$objInsureStockItem = new InsureStockItem();
				$objInsureStockItem->set_insure_stock_item_id($hId);
				$objInsureStockItem->load();

				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				

				$strtext = $objLabel->getText01();
				$pdf->Ln();
				$pdf->SetFont('angsa','B',14);

				$pdf->Cell(550,20,"��͹����ʵ�ͤ��������",0,0,"C");
				$pdf->Cell(20,20,$vv."/".$mPage,0,0,"C");
				$pdf->Ln();

				$pdf->Cell(550,20,"RECEIPT FOR TRANSFER STOCK INSURANCE",0,0,"C");
				$pdf->Ln();				
				$pdf->Cell(550,20,"",0,0,"R");
				$pdf->Ln();
				
				$objFromM = new Member();
				$objFromM->setMemberId($objInsureStockItem->get_from_officer());
				$objFromM->load();
				
				$objFromC = new Company();
				$objFromC->setCompanyId($objInsureStockItem->get_from_company());
				$objFromC->load();
				
				$objToM = new Member();
				$objToM->setMemberId($objInsureStockItem->get_to_officer());
				$objToM->load();
				
				$objToC = new Company();
				$objToC->setCompanyId($objInsureStockItem->get_to_company());
				$objToC->load();				
				
				$strText = $objFromC->getTitle();
				if($objInsureStockItem->get_from_department() == "ins"){
					$strText.=" Ἱ� ��Сѹ���";
				}else{
					$strText.=" Ἱ� �ѭ��";
				}
				
				$pdf->Cell(100,20,"����͹ / From",0,0,"L");
				$pdf->Cell(500,20,$strText,0,0,"L");
				$pdf->Ln();				
				
				$strText = $objToC->getTitle();
				if($objInsureStockItem->get_to_department() == "ins"){
					$strText.=" Ἱ� ��Сѹ���";
				}else{
					$strText.=" Ἱ� �ѭ��";
				}				
				
				$pdf->Cell(100,20,"����Ѻ / To",0,0,"L");
				$pdf->Cell(500,20,$strText,0,0,"L");
				$pdf->Ln();			
				
				$pdf->Cell(100,20,"�ѹ/��͹/��",0,0,"L");
				$pdf->Cell(500,20,formatShortThaiDate($objInsureStockItem->get_from_date()),0,0,"L");
				$pdf->Ln();			
				$pdf->Ln();			
					
				$pdf->SetFont('angsa','B',14);	
				$pdf->Cell(25,20,"�ӴѺ",1,0,"C");
				$pdf->Cell(150,20,"�����١���",1,0,"C");
				$pdf->Cell(130,20,"�����Ţ��������",1,0,"C");
				$pdf->Cell(50,20,"�Ţ����¹",1,0,"C");
				$pdf->Cell(40,20,"�Ң�",1,0,"C");
				$pdf->Cell(85,20,"��",1,0,"C");
				$pdf->Cell(70,20,"�����˵�",1,0,"C");
				$pdf->Ln();

				$pdf->SetFont('angsa','',14);	
				
	$objISIDL = new InsureStockItemDetailList();
	$objISIDL->setFilter(" O.insure_stock_item_id = $hId ");
	$objISIDL->setPageSize(25);
	$objISIDL->setPage($vv);
	$objISIDL->setSort(" SS.insure_company_id, k_number ASC  ");
	$objISIDL->load();
	$i=1;
	forEach($objISIDL->getItemList() as $objItem01) {
				
		$key = $objItem01->get_insure_stock_id();
				
		$objInsureStock = new InsureStock();
		$objInsureStock->set_insure_stock_id($key);
		$objInsureStock->load();
		
		$objItem = new Insure();
		$objItem->set_insure_id($objInsureStock->get_insure_id());
		$objItem->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();		

		$objIC = new InsureCustomer();
		$objIC->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objIC->load();
		
		$objSale = new Member();
		$objSale->setMemberId($objItem->get_sale_id());
		$objSale->load();
		
		$objCompany = new Company();
		$objCompany->setCompanyId($objItem->get_company_id());
		$objCompany->load();
		
				
				$pdf->Cell(25,20,(($vv-1)*25)+$i,1,0,"C");
				$pdf->Cell(150,20,$objIC->getTitleLabel()." ".$objIC->getFirstname()." ".$objIC->getLastname(),1,0,"L");
				$pdf->Cell(130,20,$objInsureStock->get_stock_number(),1,0,"C");
				$pdf->Cell(50,20,$objInsureCar->get_code(),1,0,"C");
				$pdf->Cell(40,20,$objCompany->getShortTitle(),1,0,"C");
				$pdf->Cell(85,20,$objSale->getFirstname(),1,0,"C");
				$pdf->Cell(70,20,"",1,0,"C");
				$pdf->Ln();
		$i++;
	}
	
	

	
	if($vv == $mPage){

				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(80,20,"���������",0,0,"C");
				$pdf->Cell(200,20,"..........................................................................................",0,0,"C");
				$pdf->Cell(50,20,"��Ѻ/���",0,0,"C");


				$pdf->Ln();		
				$pdf->Ln();		


				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(50,20,"��ѡ�ҹ����͹",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(60,20,"��ѡ�ҹ����Ѻ�͹",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Ln();		
				
				$pdf->Cell(25,20,"",0,0,"C");
				$pdf->Cell(50,20,"�ѹ����͹",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(60,20,"�ѹ����Ѻ�͹",0,0,"L");
				$pdf->Cell(150,20,".........................................................",0,0,"C");
				$pdf->Ln();		

	}
				
				
	}//end paging
				
	$pdf->Output();	
	?>
	<script>
	//window.close();
	</script>	
	<?include "unset_all.php";?>
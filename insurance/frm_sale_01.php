<?
include("common.php");
$objInsure = new Insure();
$objInsure->set_insure_id($hId);
$objInsure->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();

$objCustomer = new Customer();
$objCustomer->setCustomerId($objInsure->get_customer_id());
$objCustomer->load();

$objMember = new Member();
$objMember->setMemberId($objInsure->get_sale_id());
$objMember->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$arrDate = explode("-",$objInsure->get_date_protect());
$hNextYear = $arrDate[0]+1;
$hNewDate = $hNextYear."-".$arrDate[1]."-".$arrDate[2];

$hDiscount = $objInsure->get_k_num09()+$objInsure->get_k_num10()+$objInsure->get_k_num11();


?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Toyota Buzz  : Control Center System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_report_quotation.css">
</head>

<body>

<table width="700">
<tr>
	<td>
		<table width="98%">
		<tr>
			<td width="30%"></td>
			<td width="30%" align="center"></td>
			<td width="40%" align="center" >�ѹ��� <?=formatThaiDate(date("Y-m-d"))?></td>
		</tr>
		</table>
		<table width="98%">
		<tr>
			<td width="40">����ͧ</td>
			<td >�ͺ�س������ԡ�ô�ҹ��Сѹ��� (BUZZ CARE)</td>

		</tr>
		<tr>
			<td >���¹</td>
			<td >�س <?=$objCustomer->getFirstname()."  ".$objCustomer->getLastname();?></td>

		</tr>		
		<tr>
			<td></td>
			<td ><?=$objCustomer->getAddress03()."<br>".$objCustomer->getTumbon03()." ".$objCustomer->getAmphur03()."<br>".$objCustomer->getProvince03()." ".$objCustomer->getZip03();?><br><br><br></td>
		</tr>
		<tr>
			<td></td>
			<td >�������ҹ���ͺ��������ҧ� ���͡���ԡ�ô�ҹ��Сѹ��� (BUZZ CARE) �ҡ ����ѷ��µ�� �ӡѴ</td>
		</tr>				
		<tr>
			<td colspan="2" align="justify">����Ѻ���������Сѹ���ö¹�� <strong><?=$objCarType->getTitle()?></strong>	���  <strong><?=$objCarModel->getTitle()?></strong> ����¹ <strong><?=$objInsureCar->get_code()?></strong></td>
		</tr>
		<tr>
			<td colspan="2">������ͧ  <strong><?=formatThaiDate($objInsure->get_date_protect())?></strong> �֧   <strong><?=formatThaiDate($hNewDate)?></strong>   ��� ����ѷ� ��ͧ�͢ͺ��Фس�����ҧ�٧�� � �͡�ȹ�����<br><br></td>
		</tr>			
		<tr>
			<td colspan="2"><img src="../images/spacer.gif" alt="" width="40" height="1" border="0">��駹�������繡���׹�ѹ�ʹ��ê��� ���١��ͧ�֧����ػ��¡�ä������� ��������ҹ���Ǩ�ͺ�ա����˹�� ��͹�ӡ�ê��� �ѧ���</td>
		</tr>	
		<tr>
			<td></td>
			<td >
			<table width="100%">
			<tr>
				<td>
					������� <?=$objInsure->get_k_type()?> ( <?=$objInsure->get_k_fix()?> ) 
					<?if($objInsure->get_p_prb_id() > 0){?>
						��� �ú. 
					<?
					if($objInsure->get_p_prb_id() > 0){
						$objB = new InsureCompany();
						$objB->setInsureCompanyId($objInsure->get_p_prb_id());
						$objB->load();
						echo $objB->getTitle();
					}
					?>				
					<?}else{?>
						������ �ú.
					<?}?>				
				</td>
				<td align="right"><?=number_format($objInsure->get_k_num05(),2);?></td>
				<td>�ҷ</td>
			</tr>
			<?if($hDiscount > 0 ){?>
			<tr>
				<td>��ǹŴ�Թʴ / ���ú. ��Ť��</td>
				<td align="right"><?=number_format($hDiscount,2);?></td>
				<td>�ҷ</td>
			</tr>
			<?}?>
			<tr>
				<td>��������ط����ѧ�ѡ��ǹŴ / ��໭���� �����繨ӹǹ</td>
				<td align="right"><strong><?=number_format($objInsure->get_k_num13(),2);?></strong></td>
				<td>�ҷ</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</table>
			<br><br>
			<table width="98%">
			<tr>
				<td width="30%"></td>
				<td width="30%" align="center"></td>
				<td width="40%" align="center" >���ʴ������Ѻ���</td>
			</tr>
			</table>
			<table width="98%">
			<tr>
				<td width="30%"></td>
				<td width="30%" align="center"></td>
				<td width="40%" align="center" ><?=$objMember->getFirstname()." ".$objMember->getLastname()?></td>
			</tr>
			</table>
			<table width="98%">
			<tr>
				<td width="30%"></td>
				<td width="30%" align="center"></td>
				<td width="40%" align="center" >(<img src="../images/spacer.gif" alt="" width="150" height="1" border="0">)</td>
			</tr>
			</table>			
			<br><br>
			</td>
		</tr>		
		<tr>
			<td></td>
			<td >�ҡ��ҹ��Ǩ�ͺ�����Ŷ١��ͧ�ç�ѹ��سҪ��Ф�����������ѹ��� <strong><?=formatThaiDate($objInsure->get_date_protect())?></strong> �ѡ�繾�Фس���</td>
		</tr>												
		</table>
		<br><br><br>
		<table>
		<tr>
			<td colspan="2">�Ըա�ê����Թ</td>
		</tr>
		<tr>
			<td align="right">1.</td>
			<td>���� ���Թʴ/�ѵ��ôԵ/�� ���µ��ͧ������ѷ ( Zone �ٹ���ԡ�� )</td>
		</tr>
		<tr>
			<td align="right">2.</td>
			<td>�����¡���͹�Թ��ҹ�ѭ�ո�Ҥ�� �ѧ���</td>
		</tr>
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� ��ا�����ظ�� ( �آ��Ժ�� 2 ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 296-1-14599-5</td>
		</tr>	
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� ��ا෾ ( �Ң��ǹ���� ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 192-0-99548-5</td>
		</tr>	
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� �¾ҳԪ�� ( �Ң��ǹ���� ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 143-224515-9</td>
		</tr>						
		</table>
<br><br>
		�����˵� �ó��͹�Թ ��س�ῡ�� � Pay in �ҷ�� FAX: 02-375-4771 �֧ �ԡҹ�� ���ͷҧ����ѷ� �����͡ ������Ѻ�Թ����ҹ
</td>
</tr>
</table>
		
</body>
</html>
<?include "unset_all.php";?>
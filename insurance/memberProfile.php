<?
include "common.php";
$objMember = new Member();

$objModuleList = new ModuleList();
$objModuleList->setPageSize(0);
$objModuleList->setSortDefault(" title ASC");
$objModuleList->load();

$objMemberLogList = new MemberLogList();

$objDepartmentList = new DepartmentList();
$objDepartmentList->setPageSize(0);
$objDepartmentList->setSortDefault(" title ASC");
$objDepartmentList->load();

$objTeamList = new TeamList();
$objTeamList->setPageSize(0);
$objTeamList->setSortDefault(" title ASC");
$objTeamList->load();

if (empty($hSubmit)) {
	if ($sMemberId != "") {
		$objMember->setMemberId($sMemberId);
		$objMember->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objMember->getStartDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate01 = explode("-",$objMember->getEndDate());
		$Day01 = $arrDate01[2];
		$Month01 = $arrDate01[1];
		$Year01 = $arrDate01[0];
		
		
		$objMemberLogList->setFilter(" member_id = $sMemberId ");
		$objMemberLogList->setPageSize(10);
		$objMemberLogList->setSortDefault(" date_log DESC");
		$objMemberLogList->load();
		
	} else {
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {
	
			$objMember->setMemberId($sMemberId);
			$objMember->load();	
			$hOldPassword1 = $objMember->getPassword();

            $objMember->setMemberId($sMemberId);
			$objMember->setFirstname($hFirstname);
			$objMember->setLastname($hLastname);
			$objMember->setNickname($hNickname);
			$objMember->setEmail($hEmail);
			$objMember->setIDCard($hIDCard);
			$objMember->setInsureLicense($hInsureLicense);
			$objMember->setAddress($hAddress);
			$objMember->setTel($hTel);
			$objMember->setPhoneExt($hPhoneExt);

			$objMember->setUsername($hUsername);
			$objMember->setPassword($hNewPassword);
			$objMember->setPassword1($hConfirmPassword);

			if($hChangePassword){
				$pasrErr = $objMember->check($strMode);
				if($hOldPassword != $hOldPassword1){
					$pasrErr["oldpassword"] = "�׹�ѹ���ʼ�ҹ������١��ͧ";
				}
				
			}
			
			If ( Count($pasrErr) == 0 ){
			
				if ($strMode=="Update") {
					$objMember->updateInsureProfile();
					
					if($hChangePassword){
						$objMember->updatePassword();
					}
					
				}
				unset($objMember);
				header("location:main.php");
				exit;
			}else{
				$objMember->init();
			}//end check Count($pasrErr)
		}else{
			$objMember = new Member();
			$objMember->setMemberId($sMemberId);
			$objMember->load();	
			
		}
}

$pageTitle = "1. �к� Member";
$pageContent = "1.1 ��䢢�������ǹ���";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hFirstname.value=="")
		{
			alert("��س��кت���");
			document.forms.frm01.hFirstname.focus();
			return false;
		} 			
	
		if (document.forms.frm01.hLastname.value=="")
		{
			alert("��س��кع��ʡ��");
			document.forms.frm01.hLastname.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hNickname.value=="")
		{
			alert("��س��кت������");
			document.forms.frm01.hNickname.focus();
			return false;
		}
		
		if (document.forms.frm01.hIDCard.value=="")
		{
			alert("��س��кغѵû�ЪҪ�");
			document.forms.frm01.hIDCard.focus();
			return false;
		}
		
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		}
		
		if (document.forms.frm01.hTel.value=="")
		{
			alert("��س��к����Ѿ��");
			document.forms.frm01.hTel.focus();
			return false;
		}				
		
		if (document.forms.frm01.hChangePassword.checked == true )
		{
			if (document.forms.frm01.hUsername.value=="")
			{
				alert("��س��к� Username");
				document.forms.frm01.hUsername.focus();
				return false;
			}	
			
			if (document.forms.frm01.hOldPassword.value=="")
			{
				alert("��س��к����ʼ�ҹ���");
				document.forms.frm01.hOldPassword.focus();
				return false;
			}	
			
			if (document.forms.frm01.hNewPassword.value=="")
			{
				alert("��س��к����ʼ�ҹ����");
				document.forms.frm01.hNewPassword.focus();
				return false;
			}else{
				if (document.forms.frm01.hConfirmPassword.value=="")
				{
					alert("��س��׹�ѹ���ʼ�ҹ");
					document.forms.frm01.hConfirmPassword.focus();
					return false;
				}else{
					if (document.forms.frm01.hNewPassword.value != document.forms.frm01.hConfirmPassword.value)
					{
						alert("��س��׹�ѹ���ʼ�ҹ����");
						document.forms.frm01.hConfirmPassword.focus();
						return false;
					}	
				
				}	
			
			}	
			
		}						
		return true;		
	}
</script>

<br>
<form name="frm01" action="memberProfile.php" method="POST" onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$sMemberId?>">
	  <input type="hidden" name="hMemberId" value="<?=$sMemberId?>">

 
						  <table width="90%"  align="center">
						  	<tr>
							<td colspan="2" ><strong>Member  Information</strong><hr size="1" color="#F4EFDF"></td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right" width="20%">����:</td>
							<td >
						        <input type="text" name="hFirstname" size="30" value="<?=$objMember->getFirstname();?>">
						        <?printError($pasrErr["firstname"]);?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">���ʡ��:</td>
							<td >
						        <input type="text" name="hLastname" size="30" value="<?=$objMember->getLastname();?>">
						        <?printError($pasrErr["lastname"]);?>
						    </td>
							</tr>			
							<tr>
						    <td class="bodyhead" align="right">�������:</td>
							<td >
						        <input type="text" name="hNickname" size="30" value="<?=$objMember->getNickname();?>">						       
						    </td>
							</tr>					
							<tr>
						    <td class="bodyhead" align="right">Email:</td>
							<td >
						        <input type="text" name="hEmail" size="30" value="<?=$objMember->getEmail();?>">
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">�ѵû�ЪҪ�:</td>
							<td >
						        <input type="text" name="hIDCard" size="30" value="<?=$objMember->getIDCard();?>">
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">����Դ���:</td>
							<td >
						        <input type="text" name="hTel" size="30" value="<?=$objMember->getTel();?>">
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">����������:</td>
							<td >
						        <input type="text" name="hPhoneExt" size="30" value="<?=$objMember->getPhoneExt();?>">
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">Ἱ�:</td>
							<td >
								<?echo $objMember->getDepartmentDetail();?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">���˹�:</td>
							<td ><?=$objMember->getPositionDetail()?></td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">���:</td>
							<td >
						        <?=$objMember->getTeamDetail();?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">�͹حҵԻ�Сѹ�Թ�����:</td>
							<td class="error" >
						        <input type="text" name="hInsureLicense" size="30" value="<?=$objMember->getInsureLicense();?>"> �ó��繾�ѡ�ҹ��Сѹ����ô�к�
						    </td>
							</tr>							
							<tr>
						      <td colspan="2" align="center" > <br><br>
						        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
								<input type="submit" name="hSubmit1" onclick="return check_submit();" value="&nbsp;&nbsp;�ѹ�֡������&nbsp;&nbsp;">

						       &nbsp;&nbsp;&nbsp; <input type="Button" name="hSubmit" value="¡��ԡ������" onclick="window.location='main.php'">
						      </td>
						    </tr>							


					</table>	
</form>
<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
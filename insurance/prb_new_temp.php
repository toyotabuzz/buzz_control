<?
include("common.php");
define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);
$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();

$objInsureOld = new Insure();
$objInsureList = new InsureList();
$objOrder = new Order();
$objInsureItem = new InsureItem();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort("  use_type, code, title ASC");
$objInsureFeeList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

if(!isset($hSubmit)){

if($hInsureId != ""){
	$objInsureOld->set_insure_id($hInsureId);
	$objInsureOld->load();
	$arrDate = explode("-",$objInsureOld->get_date_protect());
	$DayProtect = $arrDate[2];
	$MonthProtect = $arrDate[1];
	$YearProtect = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_p_start_date());
	$hPStartDay = $arrDate[2];
	$hPStartMonth = $arrDate[1];
	$hPStartYear = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_p_end_date());
	$hPEndDay = $arrDate[2];
	$hPEndMonth = $arrDate[1];
	$hPEndYear = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_p_rnum_date());
	$hPRnumDay = $arrDate[2];
	$hPRnumMonth = $arrDate[1];
	$hPRnumYear = $arrDate[0];
	
	$strMode = "Update";
}else{
	$strMode = "Add";
}

}else{


if(isset($hSubmit)){
	$objInsureOld->set_insure_id($hInsureId);
	$objInsureOld->set_sale_id($sMemberId);
	$objInsureOld->set_car_id($hCarId);
	$objInsureOld->set_customer_id($objInsureCar->get_insure_customer_id());
	$objInsureOld->set_company_id($sCompanyId);
	$objInsureOld->set_year_extend($hYearExtend);
	$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;

	$objInsureOld->set_date_protect($hDateProtect);
	$objInsureOld->set_k_broker_id($hKBrokerId);
	$objInsureOld->set_k_prb_id($hKPrbId);
	$objInsureOld->set_k_stock_id($hKStockId);
	$objInsureOld->set_k_number($hKNumber);
	$hKStartDate = $hKStartYear."-".$hKStartMonth."-".$hKStartDay;
	$objInsureOld->set_k_start_date($hKStartDate);
	$hKEndDate = $hKEndYear."-".$hKEndMonth."-".$hKEndDay;
	$objInsureOld->set_k_end_date($hKEndDate);
	$objInsureOld->set_p_broker_id($hPBrokerId);
	$objInsureOld->set_p_prb_id($hPPrbId);
	$objInsureOld->set_p_stock_id($hPStockId);
	$objInsureOld->set_p_number($hPNumber);
	$hPStartDate = $hPStartYear."-".$hPStartMonth."-".$hPStartDay;
	$objInsureOld->set_p_start_date($hPStartDate);
	$hPEndDate = $hPEndYear."-".$hPEndMonth."-".$hPEndDay;
	$objInsureOld->set_p_end_date($hPEndDate);
	$objInsureOld->set_k_num01(str_replace(",", "",$hFund));
	$objInsureOld->set_k_num02(str_replace(",", "", $hBeasuti));
	$objInsureOld->set_k_num03(str_replace(",", "", $hArgon));
	$objInsureOld->set_k_num04(str_replace(",", "", $hPasi));
	$objInsureOld->set_k_num05(str_replace(",", "", $hTotal01));
	$objInsureOld->set_k_num06(str_replace(",", "", $hTotal05));
	$objInsureOld->set_k_num07(str_replace(",", "", $hTotal02));
	$objInsureOld->set_k_num08(str_replace(",", "", $hTotal03));
	$objInsureOld->set_k_num09(str_replace(",", "", $hDiscountPercentPrice));
	$objInsureOld->set_k_num10(str_replace(",", "", $hDiscountOtherPrice));
	$objInsureOld->set_k_num11(str_replace(",", "", $hPrbPrice01));
	$objInsureOld->set_k_num12(str_replace(",", "", $hStockProductTotal));
	$objInsureOld->set_k_num13(str_replace(",", "", $hTotal));
	$objInsureOld->set_k_niti($hNiti);
	$objInsureOld->set_k_dis_type_01($hDiscountType01);
	$objInsureOld->set_k_dis_type_02($hDiscountType02);
	$objInsureOld->set_k_dis_type_03($hDiscountType03);
	$objInsureOld->set_k_dis_con_01($hDiscountPercent);
	$objInsureOld->set_k_dis_con_02($hDiscountOtherRemark);
	$objInsureOld->set_k_free($hFreeRemark);
	$objInsureOld->set_k_type($hInsureType);
	$objInsureOld->set_k_type_remark($hInsureTypeRemark);
	$objInsureOld->set_k_fix($hRepair);
	$objInsureOld->set_p_car_type($hInsureFeeId);
	$objInsureOld->set_p_prb_price(str_replace(",", "", $hPrbPrice));
	$objInsureOld->set_p_year($hCarStatus);
	$hCallDate = $YearReportDate."-".$MonthReportDate."-".$DayReportDate;
	$hGetDate = $YearRecieveReport."-".$MonthRecieveReport."-".$DayRecieveReport;
	$objInsureOld->set_p_call_date($hCallDate);
	$objInsureOld->set_p_get_date($hGetDate);
	$objInsureOld->set_p_call_number($hAccNo);
	$objInsureOld->set_p_remark($hRemark);
	$objInsureOld->set_pay_number($hPaymentAmount);
	$objInsureOld->set_pay_type($hPaymentCondition);	
	$objInsureOld->set_status("ccard");	
	$objInsureOld->set_direct("yes");	
	$objInsureOld->set_p_rnum($hPRnum);
	$hPRnumDate = $hPRnumYear."-".$hPRnumMonth."-".$hPRnumDay;
	$objInsureOld->set_p_rnum_date($hPRnumDate);	
	
	
	if($strMode == "Add"){	
		$hInsureId = $objInsureOld->add();
		//echo " insure_id=   ".$hInsureId;
		if($hPStockId > 0){
			$objSI = new StockInsure();
			$objSI->set_stock_insure_id($hPStockId);
			$objSI->set_insure_id($hInsureId);
			$objSI->set_status_sale("���");
			$objSI->set_price(str_replace(",", "", $hPrbPrice));
			$objSI->updateStatusSale();
		}	
		
		$objInsure = new InsureCar();
		$objInsure->set_car_id($hCarId);
		$objInsure->set_date_verify($hDateProtect);
		$objInsure->set_operate("ccard");
		$objInsure->set_sale_id($hSaleId);
		$objInsure->updateOperate();
	}else{
	 	$objInsureOld->update();
	}
	
	
	
	header("location:prb_new.php?hId=$hInsureId&hCarId=$hCarId&hInsureId=$hInsureId");
	exit;

}

}


$pageTitle = "1. �к��������١���";
$pageContent = "1.8 �ѹ�֡�Ѻ�ú. ö�������ᴧ�����";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<table width="100%">
<tr>
	<td valign="top" >

	
	
	<br><br>
		<div class="error" >ʶҹоú : <?=$objInsureOld->get_status();?>
		<?if($objInsureOld->get_status() == "cancel"){
		$objSI = new StockInsure();
		$objSI->set_stock_insure_id($objInsureOld->get_p_stock_id());
		$objSI->load();
		echo "  ʵ�ͤ�Ѩ�غѹ : ".$objSI->get_status();
		echo "  ʶҹ� : ".$objSI->get_status_sale();
		echo "  ��һ�Ѻ : ".$objSI->get_cprice()." �ҷ ";
		echo "  �ѹ���¡��ԡ : ".formatShortDate($objSI->get_cancel_date());
		echo "  �����˵� : ".$objSI->get_stock_remark();
		?>
		
		<?}?>
		</div>
	<br><br>
	
	
<?if($hCarId != ""){?>				
<?include "insureDetail.php"?>
		
	
<form name="frm01" action="prb_new.php" method="post" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
<?if($objInsureCar->get_sale_id() == 0 ){?>
<input type="hidden" name="hSaleId" value="<?=$sMemberId;?>">
<?}else{?>
<input type="hidden" name="hSaleId" value="<?=$objInsureCar->get_sale_id();?>">
<?}?>
<input type="hidden" name="hSaveForm" value="Yes">		
<input type="hidden" name="hCarId" value="<?=$hCarId?>">

<input type="hidden" name="hYearExtend" value="<?=date("Y")?>">
<input type="hidden" name="hSubmit" value="Yes">
<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
<input type="hidden" name="hId" value="<?=$hId?>">
<input type="hidden" name="strMode" value="<?=$strMode?>">		

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background"><strong>�����Ż�Сѹ���</strong></td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>	
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td></td>			
			<td class="listTitle"><strong>������ö��Т�Ҵö¹��</strong></td>
			<td class="listTitle"><strong>�Ҥ� �ú.</strong></td>
			<td class="listTitle"><strong>�շ���ͻ�Сѹ</strong></td>
			<td class="listTitle"><strong>�ѹ��������ͧ</strong></td>
			<td class="listTitle"></td>
		</tr>		
		<tr>	
			<td></td>
			<td >
				<select name="hInsureFeeId" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsureOld->get_p_car_type() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_use_type();?> >><?=$objItem->get_code();?>, <?=$objItem->get_title();?>, <font color=red>[<?=$objItem->get_total();?>]</font>
				<?}?>
				</select>			

			
			</td>
			<td><input type="text" name="hPrbPrice" style="text-align=right;"  onblur="tryNumberFormat(this);"  size="10" value="<?=$objInsureOld->get_p_prb_price();?>"></td>
			<td>
				<select name="hCarStatus">
					<option value="0">- ��س����͡ -
					<option value="�շ�� 1" selected  <?if($objInsureOld->get_p_year() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsureOld->get_p_year() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsureOld->get_p_year() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsureOld->get_p_year() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsureOld->get_p_year() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsureOld->get_p_year() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsureOld->get_p_year() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsureOld->get_p_year() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsureOld->get_p_year() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsureOld->get_p_year() == "�շ�� 10") echo "selected"?>>�շ�� 10
				</select>
			</td>

			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayProtect value="<?=$DayProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthProtect value="<?=$MonthProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearProtect" value="<?=$YearProtect?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearProtect,DayProtect, MonthProtect, YearProtect,popCal);return false"></td>		
				</tr>
				</table>
			</td>
			<td></td>
		</tr>
		<tr>
			<td></td>			
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>

		<tr>
			<td class="listTitle">
			<strong>�Ҥ�ѧ�Ѻ</strong><br>			
			</td>
			<?if($hInsureId > 0){?>
			<td class="listDetail">
			<?
			if($objInsureOld->get_p_broker_id() > 0){
				$objB = new InsureBroker();
				$objB->setInsureBrokerId($objInsureOld->get_p_broker_id());
				$objB->load();
				echo $objB->getTitle();
			}else{
				echo "��µç";
			}
			?>
			
			</td>
			<td class="listDetail">
			<?
			if($objInsureOld->get_p_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsureOld->get_p_prb_id());
				$objB->load();
				echo $objB->getTitle();
			}
			?>
			</td>
			<td class="listDetail"><?=$objInsureOld->get_p_number();?></td>
			<input type="hidden" name="hPBrokerId" value="<?=$objInsureOld->get_p_broker_id();?>">
			<input type="hidden" name="hPPrbId" value="<?=$objInsureOld->get_p_prb_id();?>">
			<input type="hidden" name="hPStockId" value="<?=$objInsureOld->get_p_stock_id();?>">
			<input type="hidden" name="hPNumber" value="<?=$objInsureOld->get_p_number();?>">
			
			<?}else{?>
			<td class="listDetail"><?$objBrokerList->printSelectScript("hPBrokerId",$hPBrokerId,"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hPPrbId",$hPPrbId,"����к�");?>		</td>
			<td class="listDetail"><input type="hidden" name="hPStockId"  size="5" value="<?=$hPStockId;?>"><input type="text" name="hPNumber"  size="20" value="<?=$hPNumber;?>">	</td>
			<?}?>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPStartDay value="<?=$hPStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPStartMonth value="<?=$hPStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPStartYear" value="<?=$hPStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPStartYear,hPStartDay, hPStartMonth, hPStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPEndDay value="<?=$hPEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPEndMonth value="<?=$hPEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPEndYear" value="<?=$hPEndYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPEndYear,hPEndDay, hPEndMonth, hPEndYear,popCal);return false"></td>		
				</tr>
				</table>		
			</td>		
		</tr>
		<tr>
			<td></td>			
			<td class="listTitle"></td>
			<td class="listTitle"></td>
			<td class="listTitle"></td>
			<td class="listTitle">�ѹ����Ѻ�駧ҹ</td>
			<td class="listTitle">�Ţ����Ѻ�駧ҹ</td>
		</tr>		
		<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPRnumDay value="<?=$hPRnumDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPRnumMonth value="<?=$hPRnumMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPRnumYear" value="<?=$hPRnumYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPRnumYear,hPRnumDay, hPRnumMonth, hPRnumYear,popCal);return false"></td>		
				</tr>
				</table>			
		</td>
		<td><input type="text" name="hPRnum" maxlength="20"  size="20" value="<?=$objInsureOld->get_p_rnum();?>"></td>													
		</tr>
		
		</table>

	<br>

</table>		


<?
$objM01 = new Member();
$objM01->setMemberId($sMemberId);
$objM01->load();
if($objM01->getInsureEditor() == "1" and $hInsureId > 0 ){
?>



<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($hInsureId > 0 ){?>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_p_send() == 0){?><input type="button" value="���ش" onclick="window.open('insure_p_damage.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_p_send() == 0){?><input type="button" value="¡��ԡ" onclick="window.open('insure_p_cancel.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_status() == "ccard" ){?><input type="button" value="�׹ʶҹ���������" onclick="window.open('insure_c_cancel.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>


<?}?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<br><br>
	<?
			if($objInsureOld->get_p_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsureOld->get_p_prb_id());
				$objB->load();
				if($objB->getTitle() =="��ا෾��Сѹ��� ���."){?>
				<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_02.php?hId=<?=$hInsureId?>&hFormType=0501')" >
				<?}else{?>
					<?if($objB->getTitle() =="�Թ��蹤���Сѹ��� ���."){?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_03.php?hId=<?=$hInsureId?>&hFormType=0501')" >
					<?}else{?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_01.php?hId=<?=$hInsureId?>&hFormType=0501')" >					
					<?}?>
				<?
			}}
	?>
	���͡�������㹡���͡�ú 
	<input type="radio" name="hAddressType" value=1 checked> �������Ѩ�غѹ
	<input type="radio" name="hAddressType" value=2> �������������¹��ҹ
	<input type="radio" name="hAddressType" value=3> ���������ӧҹ
	<input type="radio" name="hAddressType" value=4> ������������͡���

<?}else{?>



	<?if( $hInsureId > 0 ){?>
<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?
			if($objInsureOld->get_p_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsureOld->get_p_prb_id());
				$objB->load();
				if($objB->getTitle() =="��ا෾��Сѹ��� ���."){?>
				<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_02.php?hId=<?=$hInsureId?>&hFormType=0501')" >
				<?}else{?>
					<?if($objB->getTitle() =="�Թ��蹤���Сѹ��� ���."){?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_03.php?hId=<?=$hInsureId?>&hFormType=0501')" >
					<?}else{?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_01.php?hId=<?=$hInsureId?>&hFormType=0501')" >					
					<?}?>
				<?}
				}?>
	���͡�������㹡���͡�ú 
	<input type="radio" name="hAddressType" value=1 checked> �������Ѩ�غѹ
	<input type="radio" name="hAddressType" value=2> �������������¹��ҹ
	<input type="radio" name="hAddressType" value=3> ���������ӧҹ
	<input type="radio" name="hAddressType" value=4> ������������͡���
	
	<?}else{?>
<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?}?>


<?}?>
<input type="button" value="��Ѻ�����¡��" name="hCheck" onclick="window.location='acard_prb_list.php';">
</div>
		
		
</form>




<?}else{?>	
		<table class="search" width="100%">
		<tr>
			<td height="300" align="center" valign="middle">
				<h5 class="error">��辺��¡�÷���ͧ���</h5>
				
			</td>
		</tr>
		</table>
<?}?>

	
	</td>
</tr>
</table>

<?
	include("h_footer.php")
?>
<?if($hInsureId <=0){?>
<script>
	new CAPXOUS.AutoComplete("hKNumber", function() {
		return "auto_kom.php?hValue=hKStockId&hBrokerId="+document.frm01.hKBrokerId.value+"&hInsureCompanyId="+document.frm01.hKPrbId.value+"&q="+this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hPNumber", function() {
		return "auto_prb.php?hValue=hPStockId&hBrokerId="+document.frm01.hPBrokerId.value+"&hInsureCompanyId="+document.frm01.hPPrbId.value+"&q="+this.text.value;
	});	
</script>
<?}?>
<script>
	function checkSubmit(){

		if(document.frm01.hInsureFeeId.value ==0){
			alert("��س����͡������ö��Т�Ҵö¹��");
			document.frm01.hInsureFeeId.focus();
			return false;
		}	
		
		if(document.frm01.hPrbPrice.value <=0){
			alert("��س��к��Ҥ� �ú.");
			document.frm01.hPrbPrice.focus();
			return false;
		}	
		
		if(document.frm01.hCarStatus.value ==0){
			alert("��س����͡�շ���ͻ�Сѹ");
			document.frm01.hCarStatus.focus();
			return false;
		}				
	
		if (document.forms.frm01.DayProtect.value=="" || document.forms.frm01.DayProtect.value=="00")
		{
			alert("��س��к��ѹ��������ͧ");
			document.forms.frm01.DayProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.DayProtect.value,1,31) == false) {
				document.forms.frm01.DayProtect.focus();
				return false;
			}
		} 		
	
		if (document.forms.frm01.MonthProtect.value==""  || document.forms.frm01.MonthProtect.value=="00")
		{
			alert("��س��к���͹��������ͧ");
			document.forms.frm01.MonthProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.MonthProtect.value,1,12) == false){
				document.forms.frm01.MonthProtect.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.YearProtect.value==""  || document.forms.frm01.YearProtect.value=="0000")
		{
			alert("��س��кػշ�������ͧ");
			document.forms.frm01.YearProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.YearProtect.value,<?=date("Y")-150?>,<?=date("Y")?>) == false) {
				document.forms.frm01.YearProtect.focus();
				return false;
			}
		} 			
	
		if( document.frm01.hPPrbId.value ==0   && document.frm01.hPBrokerId.value ==0 ){
			alert("��س����͡����ѷ��Сѹ���ҧ����ҧ˹��");
			document.frm01.hPBrokerId.focus();
			return false;
		}
		
		if(document.frm01.hPPrbId.value > 0 || document.frm01.hPBrokerId.value > 0){
			
			if(document.frm01.hPNumber.value == ""){
				alert("��س��к������Ţ��������");
				document.frm01.hPNumber.focus();
				return false;
			}
			
			if (document.forms.frm01.hPStartDay.value=="" || document.forms.frm01.hPStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hPStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartDay.value,1,31) == false) {
					document.forms.frm01.hPStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPStartMonth.value==""  || document.forms.frm01.hPStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hPStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartMonth.value,1,12) == false){
					document.forms.frm01.hPStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPStartYear.value==""  || document.forms.frm01.hPStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hPStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartYear.value,<?=date("Y")-150?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hPEndDay.value=="" || document.forms.frm01.hPEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hPEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndDay.value,1,31) == false) {
					document.forms.frm01.hPEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPEndMonth.value==""  || document.forms.frm01.hPEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hPEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndMonth.value,1,12) == false){
					document.forms.frm01.hPEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPEndYear.value==""  || document.forms.frm01.hPEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hPEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndYear.value,<?=date("Y")-150?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPEndYear.focus();
					return false;
				}
			} 						
			
		}
		

				
		document.frm01.submit();
		return true;
	}
	
		function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPrbPrice.value = <?=$objItem->get_total()?>;
		}
	<?}?>

	}
	
	
	function PrintPrb(frm_name){
		if(document.frm01.hAddressType[0].checked == true){	window.open(frm_name+'&hAddressType=1');}
		if(document.frm01.hAddressType[1].checked == true){	window.open(frm_name+'&hAddressType=2');}
		if(document.frm01.hAddressType[2].checked == true){	window.open(frm_name+'&hAddressType=3');}
		if(document.frm01.hAddressType[3].checked == true){	window.open(frm_name+'&hAddressType=4');}
		
	}
	
	</script>
<?include "unset_all.php";?>
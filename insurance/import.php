<?
include "common.php";
//error_reporting("ALL");


$objCustomerList = new InsureCarList();
$objCustomerList->setFilter(" import = 0 ");
$objCustomerList->setPageSize(1000);
$objCustomerList->setSortDefault(" date_verify ASC");
$objCustomerList->loadSearch();
forEach($objCustomerList->getItemList() as $o_customer) {

	$objC = new Customer();
	$objIC = new InsureCustomer();

	//echo $o_customer->get_customer_id();
	
	$objC->setCustomerId($o_customer->get_customer_id());
	$objC->load();	
	//echo $objC->getFirstname()."<br>";
	
	$objIC->setCustomerId($objC->getCustomerId());
    $objIC->setTypeId($objC->getTypeId());
	$objIC->setACard($objC->getACard());
	$objIC->setBCard($objC->getBCard());
	$objIC->setCCard($objC->getCCard());
	$objIC->setICard($objC->getICard());
    $objIC->setGradeId($objC->getGradeId());
    $objIC->setGroupId($objC->getGroupId());
	$objIC->setEventId($objC->getEventId());
	$objIC->setFollowUp($objC->getFollowUp());
    $objIC->setInformationDate($objC->getInformationDate());
    $objIC->setSaleId($objC->getSaleId());
    $objIC->setBirthday($objC->getBirthday());
    $objIC->setTitle($objC->getTitle());			
	$objIC->setFirstname($objC->getFirstname());
	$objIC->setlastname($objC->getLastname());
	$objIC->setCustomerTitleId($objC->getCustomerTitleId());
	$objIC->setCustomerTitleDetail($objC->getCustomerTitleDetail());
	$objIC->setEmail($objC->getEmail());
	$objIC->setIDCard($objC->getIDCard());
	$objIC->setAddress($objC->getAddress());
	$objIC->setAddress1($objC->getAddress1());
	$objIC->setTumbon($objC->getTumbon());
	$objIC->setAmphur($objC->getAmphur());
	$objIC->setProvince($objC->getProvince());
	$objIC->setTumbonCode($objC->getTumbonCode());
	$objIC->setAmphurCode($objC->getAmphurCode());
	$objIC->setProvinceCode($objC->getProvinceCode());
	$objIC->setZipCode($objC->getZipCode());
	$objIC->setZip($objC->getZip());
	$objIC->setHomeTel($objC->getHomeTel());
	$objIC->setMobile($objC->getMobile());
	$objIC->setOfficeTel($objC->getOfficeTel());
	$objIC->setFax($objC->getFax());
	$objIC->setContactName($objC->getContactName());
	$objIC->setAddress01($objC->getAddress01());
	$objIC->setAddress011($objC->getAddress011());
	$objIC->setTumbon01($objC->getTumbon01());
	$objIC->setAmphur01($objC->getAmphur01());
	$objIC->setProvince01($objC->getProvince01());
	$objIC->setTumbonCode01($objC->getTumbonCode01());
	$objIC->setAmphurCode01($objC->getAmphurCode01());
	$objIC->setProvinceCode01($objC->getProvinceCode01());	
	$objIC->setZipCode01($objC->getZipCode01());	
	$objIC->setZip01($objC->getZip01());
	$objIC->setTel01($objC->getTel01());
	$objIC->setFax01($objC->getFax01());
	$objIC->setCompany($objC->getCompany());
	$objIC->setAddress02($objC->getAddress02());
	$objIC->setAddress021($objC->getAddress021());
	$objIC->setTumbon02($objC->getTumbon02());
	$objIC->setAmphur02($objC->getAmphur02());
	$objIC->setProvince02($objC->getProvince02());
	$objIC->setTumbonCode02($objC->getTumbonCode02());
	$objIC->setAmphurCode02($objC->getAmphurCode02());
	$objIC->setProvinceCode02($objC->getProvinceCode02());	
	$objIC->setZipCode02($objC->getZipCode02());	
	$objIC->setZip02($objC->getZip02());
	$objIC->setTel02($objC->getTel02());
	$objIC->setFax02($objC->getFax02());
	
	$objIC->setName03($objC->getName03());
	$objIC->setAddress03($objC->getAddress03());
	$objIC->setAddress031($objC->getAddress031());
	$objIC->setTumbon03($objC->getTumbon03());
	$objIC->setAmphur03($objC->getAmphur03());
	$objIC->setProvince03($objC->getProvince03());
	$objIC->setTumbonCode03($objC->getTumbonCode03());
	$objIC->setAmphurCode03($objC->getAmphurCode03());
	$objIC->setProvinceCode03($objC->getProvinceCode03());	
	$objIC->setZipCode03($objC->getZipCode03());	
	$objIC->setZip03($objC->getZip03());
	$objIC->setTel03($objC->getTel03());
	$objIC->setFax03($objC->getFax03());

	$objIC->setName04($objC->getName04());
	$objIC->setAddress04($objC->getAddress04());
	$objIC->setAddress041($objC->getAddress041());
	$objIC->setTumbon04($objC->getTumbon04());
	$objIC->setAmphur04($objC->getAmphur04());
	$objIC->setProvince04($objC->getProvince04());
	$objIC->setTumbonCode04($objC->getTumbonCode04());
	$objIC->setAmphurCode04($objC->getAmphurCode04());
	$objIC->setProvinceCode04($objC->getProvinceCode04());	
	$objIC->setZipCode04($objC->getZipCode04());	
	$objIC->setZip04($objC->getZip04());
	$objIC->setTel04($objC->getTel04());
	$objIC->setFax04($objC->getFax04());
	
	$objIC->setIncomplete($objC->getIncomplete());
	$objIC->setMailback($objC->getMailback());
	$objIC->setVerifyAddress($objC->getVerifyAddress());
	$objIC->setVerifyPhone($objC->getVerifyPhone());
	$objIC->setFax03($objC->getFax03());			
	$objIC->setAddBy($objC->getAddBy());
	$objIC->setAddDate($objC->getAddDate());
	$objIC->setEditBy($objC->getEditBy());
	$objIC->setEditDate($objC->getEditDate());
	$objIC->setDeleteStatus($objC->getDeleteStatus());
	$objIC->setDeleteBy($objC->getDeleteBy());
	$objIC->setDeleteReason($objC->getDeleteReason());
	$objIC->setDeleteDate($objC->getDeleteDate());
	$objIC->setAcardOld($objC->getAcardOld());
	$objIC->setMailbackRemark($objC->getMailbackRemark());
	$objIC->setMailbackDate($objC->getMailbackDate());
	$objIC->setDuplicates($objC->getDuplicates());
	$objIC->setRemark($objC->getRemark());
	$objIC->setCompanyId($objC->getCompanyId());
	$objIC->setInsureMember($objC->getInsureMember());
	

	if($hId = $objIC->add()){
		$objIC = new InsureCar();
		$objIC->updateSql(" update t_insure_car set insure_customer_id = $hId ,  import=1 where  car_id= ".$o_customer->get_car_id()."  ");
		echo $objC->getFirstname()."<br>";
	}
}
?>
<?include "unset_all.php";?>
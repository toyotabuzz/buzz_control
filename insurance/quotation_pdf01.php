<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

$objIQ = new InsureQuotation();
$objQL01 = new InsureQuotationItem();
$objQL02 = new InsureQuotationItem();
$objQL03 = new InsureQuotationItem();

$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();
		
$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$objSale = new Member();
$objSale->setMemberId($objInsure->get_sale_id());
$objSale->load();

$objCompany = new Company();
$objCompany->setCompanyId($objInsure->get_company_id());
$objCompany->load();

$objIQ->set_quotation_id($hId);
$objIQ->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$arrDate = explode("-",$objIQ->get_date_add());
$Day02 = $arrDate[2];
$Month02 = $arrDate[1];
$Year02 = $arrDate[0];

$objQIList = new InsureQuotationItemList();
$objQIList->setFilter(" quotation_id = $hId ");
$objQIList->setPageSize(0);
$objQIList->setSort(" quotation_item_id ASC ");
$objQIList->load();
$i=0;
forEach($objQIList->getItemList() as $objItem) {
	if($i==0){
		$objQL01->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL01->load();
	}
	
	if($i==1){
		$objQL02->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL02->load();
	}

	if($i==2){
		$objQL03->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL03->load();
	}
	$i++;
}

$objInsureList = New InsureList;

$objLabel = new Label();
$strText01= "��˹������š�þ������ʹ��Ҥ�";	
$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
define("IMG_INSURE","../images/misc/".$objLabel->getText02());

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	$this->Image(IMG_INSURE,20,10,180);
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{

    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(10,10,10);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

$pdf->AddPage();
$pdf->SetLeftMargin(20);

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(230,30);
$pdf->Write(10,"��ʹ��Ҥ����»�Сѹ���ö¹��");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,70);
$pdf->Write(10,"�Ţ�����ʹ��Ҥ� : ".$objIQ->get_quotation_number());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,85);
$pdf->Write(10,$objLabel->getText01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,100);
$pdf->Write(10,"�Ţ����͹حҵ : ".$objLabel->getText03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,115);
$pdf->Write(10,"���Դ��� : ".$objSale->getFirstname()." ".$objSale->getLastname()." (".$objSale->getNickname().") ��� ".$objSale->getPhoneExt());

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(20,130+$extend1);
$pdf->Cell(380,10,"",0,0,"C");
$pdf->Cell(150,10,"�� : ".$objLabel->getText04()." ῡ�� : ".$objLabel->getText05(),0,0,"L");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(30,70);
$pdf->Write(10,"���¹  ".$objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname());


if($hAddressType==1 or $hAddressType=="" ){
	$hAddress1 = $objCustomer->getAddressLabel();
	if($objCustomer->mProvince == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon." ࢵ ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon." ����� ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
	}	
}
if($hAddressType==2){
	$hAddress1 = $objCustomer->getAddressLabel01();
	if($objCustomer->mProvince01 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon01." ࢵ ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon01." ����� ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
	}	
}
if($hAddressType==3){
	$hAddress1 = $objCustomer->getAddressLabel02();
	if($objCustomer->mProvince02 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon02." ࢵ ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon02." ����� ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
	}	
}
if($hAddressType==4){
	$hAddress1 = $objCustomer->getAddressLabel03();
	if($objCustomer->mProvince03 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon03." ࢵ ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon03." ����� ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
	}	
}

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(30,85);
$pdf->Write(10,"�������  ".$hAddress1);

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(45,100);
$pdf->Write(10,$hAddress2);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,145);
$pdf->Write(10,"�����һ�Сѹ��� : ".$objIQ->get_text01());

$image1 = "check.jpg";
$image2 = "un_check.jpg";
if($objIQ->get_text02()=="����кؼ��Ѻ���") { $pdf->Image($image1, 350,146,7); }else{$pdf->Image($image2, 350, 146,7); }
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(360,145);
$pdf->Write(10,"����кؼ��Ѻ��� ");

if($objIQ->get_text02()=="�кؼ��Ѻ���") { $pdf->Image($image1, 420,146,7); }else{$pdf->Image($image2, 420, 146,7); }
$pdf->SetXY(430,145);
$pdf->Write(10,"�кؼ��Ѻ��� ");

if($objIQ->get_text02()=="����") { $pdf->Image($image1, 470,146,7); }else{$pdf->Image($image2, 470, 146,7); }
$pdf->SetXY(480,145);
$pdf->Write(10,"����  ".$objIQ->get_text03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,165);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text04());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(170,165);
if($objIQ->get_text05() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatThaiDate1($objIQ->get_text05()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(290,165);
$pdf->Write(10,"�͹حҵ� : ".$objIQ->get_text06());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(364,165);
if($objIQ->get_text25() != "--"){
$pdf->Write(10,"�ѹ͹حҵ : ".formatThaiDate1($objIQ->get_text25()));
}else{
$pdf->Write(10,"�ѹ͹حҵ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(470,165);
if($objIQ->get_text07() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatThaiDate1($objIQ->get_text07()));
}else{
	if($objIQ->get_permit_all1() != ""){
		$pdf->Write(10,"�ѹ������� : ��ʹ�վ ");
	}else{
		$pdf->Write(10,"�ѹ������� : ");
	}
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,180);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text08());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(170,180);
if($objIQ->get_text09() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatThaiDate1($objIQ->get_text09()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(290,180);
$pdf->Write(10,"�͹حҵ� : ".$objIQ->get_text10());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(364,180);
if($objIQ->get_text26() != "--"){
$pdf->Write(10,"�ѹ͹حҵ : ".formatThaiDate1($objIQ->get_text26()));
}else{
$pdf->Write(10,"�ѹ͹حҵ : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(470,180);
if($objIQ->get_text11() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatThaiDate1($objIQ->get_text11()));
}else{
	if($objIQ->get_permit_all2() != ""){
		$pdf->Write(10,"�ѹ������� : ��ʹ�վ ");
	}else{
		$pdf->Write(10,"�ѹ������� : ");
	}
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,200);
if($objIQ->get_text12() != "--"){
$pdf->Write(10,"�������һ�Сѹ��� : ".formatThaiDate1($objIQ->get_text12()));
}else{
$pdf->Write(10,"�������һ�Сѹ��� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,200);
if($objIQ->get_text13() != "--"){
$pdf->Write(10,"����ش : ".formatThaiDate1($objIQ->get_text13()));
}else{
$pdf->Write(10,"����ش : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(330,200);
$pdf->Write(10,"���� : ".$objIQ->get_text14()." �.");



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(260,220);
$pdf->Write(10,"��¡��ö¹����һ�Сѹ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,240);
$pdf->Write(10,"�ӴѺ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(47,240);
$pdf->Write(10,"����");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,240);
$pdf->Write(10,"����ö¹��/���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,240);
$pdf->Write(10,"�Ţ����¹");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(310,240);
$pdf->Write(10,"�Ţ��Ƕѧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,240);
$pdf->Write(10,"�����");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(435,240);
$pdf->Write(10,"������ö");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(490,240);
$pdf->Write(10,"�ӹǹ/��Ҵ/���˹ѡ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,260);
$pdf->Write(10,$objIQ->get_text15());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(48,260);
$pdf->Write(10,$objIQ->get_text16());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(70,260);
$pdf->Write(10,$objIQ->get_text17());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(70,275);
$pdf->Write(10,$objIQ->get_text18());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(190,260);
$pdf->Write(10,$objIQ->get_text19());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(190,275);
$pdf->Write(10,$objIQ->get_text20());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(270,260);
$pdf->Write(10,$objIQ->get_text21());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,260);
$pdf->Write(10,$objIQ->get_text22());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(440,260);
$pdf->Write(10,$objIQ->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(490,260);
$pdf->Write(10,$objIQ->get_text24());


$pdf->line(20,141,580,141);
$pdf->line(20,161,580,161);
$pdf->line(20,196,580,196);
$pdf->line(20,216,580,216);
$pdf->line(20,236,580,236);
$pdf->line(20,256,580,256);
$pdf->line(20,290,580,290);

$pdf->line(20,141,20,290);
$pdf->line(580,141,580,290);

$pdf->line(45,236,45,290);
$pdf->line(70,236,70,290);
$pdf->line(190,236,190,290);
$pdf->line(260,236,260,290);
$pdf->line(390,236,390,290);
$pdf->line(430,236,430,290);
$pdf->line(480,236,480,290);

$pdf->SetFont('angsa','B',13);	
$pdf->SetXY(260,295);
$pdf->Write(10,"��������´����������ͧ");

if($objQL01->get_text21() >0 or $objQL02->get_text21() >0  or $objQL03->get_text21() >0  ){







	if($objQL01->get_text06() >0 or $objQL02->get_text06() >0  or $objQL03->get_text06() >0  ){
		$vvv = 27;
	}else{
		$vvv = 26;
	}








for($yy=0;$yy<$vvv;$yy++){
	$yyy = ($yy*15)+310;
	
	if($objQL01->get_text06() >0 or $objQL02->get_text06() >0  or $objQL03->get_text06() >0  ){
		if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16) and ($yy!=17) and ($yy!=18) and ($yy!=20) and ($yy!=21) and ($yy!=22) and ($yy!=23)  and ($yy!=25) ){
		$pdf->line(20,$yyy,580,$yyy);
		}else{
		$pdf->line(280,$yyy,580,$yyy);
		}
	}else{
		if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16) and ($yy!=17) and ($yy!=19) and ($yy!=20) and ($yy!=21) and ($yy!=22) and ($yy!=24) ){
		$pdf->line(20,$yyy,580,$yyy);
		}else{
		$pdf->line(280,$yyy,580,$yyy);
		}
	}	
	
	

}

$pdf->line(20,310,20,$yyy);

$pdf->line(280,310,280,$yyy);
$pdf->line(380,310,380,$yyy);
$pdf->line(480,310,480,$yyy);

$pdf->line(580,310,580,$yyy);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetXY(20,310+$extend);
$pdf->SetFont('angsa','B',12);
$pdf->Cell(260,15,"",0,0,"C");
$pdf->Cell(100,15,$objQL01->get_text23(),1,0,"C");
$pdf->Cell(100,15,$objQL02->get_text23(),1,0,"C");
$pdf->Cell(100,15,$objQL03->get_text23(),1,0,"C");
$pdf->Ln();

$pdf->Cell(260,15,"",0,0,"C");
$pdf->Cell(100,15,$objQL01->get_text01(),1,0,"C");
$pdf->Cell(100,15,$objQL02->get_text01(),1,0,"C");
$pdf->Cell(100,15,$objQL03->get_text01(),1,0,"C");
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�����Ѻ�Դ�ͺ��ͺؤ����¹͡",1,0,"C");
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����",1,0,"L");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�����ͤ���",1,0,"R");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ�ͷ�Ѿ���Թ���ó�",1,0,"L");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������ͧö¹������һ�Сѹ���",1,0,"C");
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ��ö¹������һ�Сѹ���",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"ö¹���٭���/�����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

if($objQL01->get_text06() > 0 or $objQL02->get_text06() > 0 or $objQL03->get_text06() > 0){
$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������������ǹ�á",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();
}


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������ͧ����͡���Ṻ���� �ѧ���",1,0,"C");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',11);	
$pdf->Cell(260,15,"�غѵ��˵���ǹ�ؤ��/�ؾ���Ҿ���� ���ª��Ե�٭���������м��Ѻ��� 1�������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�غѵ��˵���ǹ�ؤ��/�ؾ���Ҿ���Ǥ��� ���Ѻ��� 1 �������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����ѡ�Ҿ�Һ�ŵ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"��Сѹ��Ǽ��Ѻ���",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"���»�Сѹ����ط�� + �ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ѡ � ������ 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������������ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������ѧ�ѡ � ��� ���� 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"���� �ú. �ط�� + �ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ѡ � ������ 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������������ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������ѧ�ѡ � ��� ���� 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"��ǹŴ�����",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text21());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text21());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text21());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ʹ�����ط��",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();






	if($objQL01->get_text06() >0 or $objQL02->get_text06() >0  or $objQL03->get_text06() >0  ){
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,710);
$pdf->Write(15,$objIQ->get_remark());

	}else{
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,698);
$pdf->Write(15,$objIQ->get_remark());

	}	






}else{





	if($objQL01->get_text06() >0 or $objQL02->get_text06() >0  or $objQL03->get_text06() >0  ){
		$vvv = 26;
	}else{
		$vvv = 25;
	}








for($yy=0;$yy<$vvv;$yy++){
	$yyy = ($yy*15)+310;
	
	if($objQL01->get_text06() >0 or $objQL02->get_text06() >0  or $objQL03->get_text06() >0  ){
		if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16)  and ($yy!=17) and ($yy!=18)  and ($yy!=20) and ($yy!=21)  and ($yy!=22) and ($yy!=23)  ){
		$pdf->line(20,$yyy,580,$yyy);
		}else{
		$pdf->line(280,$yyy,580,$yyy);
		}
	}else{
		if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16) and ($yy!=17) and ($yy!=19) and ($yy!=20) and ($yy!=21) and ($yy!=22)  ){
		$pdf->line(20,$yyy,580,$yyy);
		}else{
		$pdf->line(280,$yyy,580,$yyy);
		}
	}	
	

}

$pdf->line(20,310,20,$yyy);

$pdf->line(280,310,280,$yyy);
$pdf->line(380,310,380,$yyy);
$pdf->line(480,310,480,$yyy);

$pdf->line(580,310,580,$yyy);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetXY(20,310+$extend);
$pdf->SetFont('angsa','B',12);
$pdf->Cell(260,15,"",0,0,"C");
$pdf->Cell(100,15,$objQL01->get_text23(),1,0,"C");
$pdf->Cell(100,15,$objQL02->get_text23(),1,0,"C");
$pdf->Cell(100,15,$objQL03->get_text23(),1,0,"C");
$pdf->Ln();

$pdf->Cell(260,15,"",0,0,"C");
$pdf->Cell(100,15,$objQL01->get_text01(),1,0,"C");
$pdf->Cell(100,15,$objQL02->get_text01(),1,0,"C");
$pdf->Cell(100,15,$objQL03->get_text01(),1,0,"C");
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�����Ѻ�Դ�ͺ��ͺؤ����¹͡",1,0,"C");
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����",1,0,"L");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�����ͤ���",1,0,"R");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ�ͷ�Ѿ���Թ���ó�",1,0,"L");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text05());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������ͧö¹������һ�Сѹ���",1,0,"C");
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������µ��ö¹������һ�Сѹ���",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text07());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"ö¹���٭���/�����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text08());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


if($objQL01->get_text06() > 0 or $objQL02->get_text06() > 0 or $objQL03->get_text06() > 0){
$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������������ǹ�á",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text06());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();
}

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����������ͧ����͡���Ṻ���� �ѧ���",1,0,"C");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text09());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',11);	
$pdf->Cell(260,15,"�غѵ��˵���ǹ�ؤ��/�ؾ���Ҿ���� ���ª��Ե�٭���������м��Ѻ��� 1�������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�غѵ��˵���ǹ�ؤ��/�ؾ���Ҿ���Ǥ��� ���Ѻ��� 1 �������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Cell(100,15,"��������ͧ",1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"����ѡ�Ҿ�Һ�ŵ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text11());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"��Сѹ��Ǽ��Ѻ���",1,0,"L");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode(".",$objQL01->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL02->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode(".",$objQL03->get_text12());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0]),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"���»�Сѹ����ط�� + �ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text13());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ѡ � ������ 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text24());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text14());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������������ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text15());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������ѧ�ѡ � ��� ���� 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text16());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"���� �ú. �ط�� + �ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();



$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ѡ � ������ 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text25());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�������",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text18());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������������ҡ�",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text19());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"������ѧ�ѡ � ��� ���� 1%",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text20());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();

$pdf->SetFont('angsa','B',12);	
$pdf->Cell(260,15,"�ʹ�����ط��",0,0,"R");

$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text22());if($arrNum0[0] >0){$pdf->Cell(100,15,number_format($arrNum0[0],2),1,0,"C");}else{$pdf->Cell(100,15,"",1,0,"C");}unset($arrNum0);
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,698);
$pdf->Write(15,$objIQ->get_remark());











}

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,750);
$pdf->SetTextColor(255,0,0);
$pdf->Write(15,"�����˵�  ����ѷ����վ�ѡ�ҹ��Ѻ�Թ");

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,750);
//$pdf->Write(15,"���ʴ������Ѻ���");
$pdf->Cell(150,20,"���ʴ������Ѻ���",0,0,"C");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,790);
$pdf->Cell(150,20,"(".$objSale->getFirstname()." ".$objSale->getLastname().")",0,0,"C");
//$pdf->Write(15,"(".$objSale->getFirstname()." ".$objSale->getLastname().")");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,810);
$pdf->Cell(150,20,"��ѡ�ҹ��Сѹ���ö¹��",0,0,"C");
//$pdf->Write(15,"��ѡ�ҹ��Сѹ���ö¹��");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,770);
$pdf->SetRightMargin(100);
//$pdf->Cell(80,20,$objIQ->get_xtra02(),0,0,"L");
$pdf->Write(15,$objIQ->get_xtra02());


$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,800);
$pdf->Write(15,$objLabel->getText01());

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,800);
$pdf->Write(15,$objLabel->getText01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,815);
$pdf->Write(15,$objLabel->getText06());

$pdf->Output();	
?>
<script>window.close();</script>	
<?include "unset_all.php";?>
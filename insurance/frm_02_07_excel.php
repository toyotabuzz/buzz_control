<?
header("Content-type: application/x-download");
if($Year01 >0){
header('Content-Disposition: attachment; filename="INS01-['.$Day.'-'.$Month.'-'.$Year.']-['.$Day01.'-'.$Month01.'-'.$Year01.'].xls"');
}else{
header('Content-Disposition: attachment; filename="INS01-['.$Day.'-'.$Month.'-'.$Year.'].xls"');
}

include("common.php");

$objInsure = new Insure();
$objInsure->set_insure_id($hId);
$objInsure->load();

$objInsureType = new InsureType();
$objInsureType->setInsureTypeId($objInsure->get_k_type());
$objInsureType->load();

$objCompany = new Company();
$objCompany->setCompanyId($sCompanyId);
$objCompany->load();

$objInsureCompany = new InsureCompany();
$objInsureCompany->setInsureCompanyId($objInsure->get_k_prb_id());
$objInsureCompany->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$objMember = new Member();
$objMember->setMemberId($objInsureCar->get_sale_id());
$objMember->load();

$objF = new InsureForm();
$objF->loadByCondition(" insure_id= $hId and frm_type= '$hFormType'  ");

if(!($hSubmit)){
	$objForm = new InsureForm();
	if($objForm->loadByCondition("  insure_id=$hId and frm_type='0207' ")){
		
		
		
		
		
	}else{
		$objF->set_t01(formatThaiDate(date("Y-m-d")));
		$objF->set_t02($objMember->getFirstname()." (".$objMember->getNickname().")");
		$objF->set_t04($objCompany->getTitle());
		$objF->set_t15($objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname());
		$objF->set_t16($objCustomer->getIDCard());
		$objF->set_t17($objCustomer->getAddress());
		$objF->set_t18($objCustomer->getTumbon());
		$objF->set_t19($objCustomer->getAmphur());
		$objF->set_t20($objCustomer->getProvince());
		$objF->set_t21($objCustomer->getZip());
		$objF->set_t22($objCustomer->getHomeTel());
		$objF->set_t23($objCustomer->getOfficeTel());
		$objF->set_t24($objCustomer->getMobile());
		
		$objF->set_t25($objCarType->getTitle());
		$objF->set_t26($objCarModel->getTitle());
		$objF->set_t27($objCarSeries->getTitle());
		$objF->set_t28($objInsureCar->get_register_year());
		$objF->set_t29($objInsureCar->get_car_number());
		$objF->set_t30($objInsureCar->get_price());
		$objF->set_t31($objInsureCar->get_engine_number());
		$objF->set_t32("");
		$objF->set_t33($objCarColor->getTitle());
		$objF->set_t34($objInsureCar->get_code());
		$objF->set_t35("");
		$objF->set_t36("");
		
		$objF->set_t37($objInsureCompany->getTitle());
		$objF->set_t38($objInsureType->getTitle());
		$objF->set_t40($objInsure->get_k_number());
		
		$objF->set_t52(number_format($objInsure->get_k_num01()));
		$objF->set_t53(number_format($objInsure->get_k_num05()));
		
		$objF->set_t54(formatThaiDate($objInsure->get_k_start_date()));
		$objF->set_t55(formatThaiDate($objInsure->get_p_start_date()));
		
	}
}



if(isset($hSubmit)){

	$objF->set_insure_id($hId);
	$objF->set_frm_type("0207");
	$objF->set_code($hCode);
	$objF->set_title($hTitle);
	$objF->set_remark($hRemark);
	$objF->set_t01($hT01);
	$objF->set_t02($hT02);
	$objF->set_t03($hT03);
	$objF->set_t04($hT04);
	$objF->set_t05($hT05);
	$objF->set_t06($hT06);
	$objF->set_t07($hT07);
	$objF->set_t08($hT08);
	$objF->set_t09($hT09);
	$objF->set_t10($hT10);
	$objF->set_t11($hT11);
	$objF->set_t12($hT12);
	$objF->set_t13($hT13);
	$objF->set_t14($hT14);
	$objF->set_t15($hT15);
	$objF->set_t16($hT16);
	$objF->set_t17($hT17);
	$objF->set_t18($hT18);
	$objF->set_t19($hT19);
	$objF->set_t20($hT20);
	$objF->set_t21($hT21);
	$objF->set_t22($hT22);
	$objF->set_t23($hT23);
	$objF->set_t24($hT24);
	$objF->set_t25($hT25);
	$objF->set_t26($hT26);
	$objF->set_t27($hT27);
	$objF->set_t28($hT28);
	$objF->set_t29($hT29);
	$objF->set_t30($hT30);
	$objF->set_t31($hT31);
	$objF->set_t32($hT32);
	$objF->set_t33($hT33);
	$objF->set_t34($hT34);
	$objF->set_t35($hT35);
	$objF->set_t36($hT36);
	$objF->set_t37($hT37);
	$objF->set_t38($hT38);
	$objF->set_t39($hT39);
	$objF->set_t40($hT40);
	$objF->set_t41($hT41);
	$objF->set_t42($hT42);
	$objF->set_t43($hT43);
	$objF->set_t44($hT44);
	$objF->set_t45($hT45);
	$objF->set_t46($hT46);
	$objF->set_t47($hT47);
	$objF->set_t48($hT48);
	$objF->set_t49($hT49);
	$objF->set_t50($hT50);
	$objF->set_t51($hT51);
	$objF->set_t52($hT52);
	$objF->set_t53($hT53);
	$objF->set_t54($hT54);
	$objF->set_t55($hT55);
	$objF->set_t56($hT56);
	$objF->set_t57($hT57);
	$objF->set_t58($hT58);
	$objF->set_t59($hT59);
	$objF->set_t60($hT60);
	$objF->set_t61($hT61);
	$objF->set_t62($hT62);
	$objF->set_t63($hT63);
	$objF->set_t64($hT64);
	$objF->set_t65($hT65);
	$objF->set_t66($hT66);
	$objF->set_t67($hT67);
	$objF->set_t68($hT68);
	$objF->set_t69($hT69);
	$objF->set_t70($hT70);
	$objF->set_t71($hT71);
	$objF->set_t72($hT72);
	$objF->set_t73($hT73);
	$objF->set_t74($hT74);
	$objF->set_t75($hT75);
	$objF->set_t76($hT76);
	$objF->set_t77($hT77);
	$objF->set_t78($hT78);
	$objF->set_t79($hT79);
	$objF->set_t80($hT80);
	$objF->set_t81($hT81);
	$objF->set_t82($hT82);
	$objF->set_t83($hT83);
	$objF->set_t84($hT84);
	$objF->set_t85($hT85);
	$objF->set_t86($hT86);
	$objF->set_t87($hT87);
	$objF->set_t88($hT88);
	$objF->set_t89($hT89);
	$objF->set_t90($hT90);
	$objF->set_t91($hT91);
	$objF->set_t92($hT92);
	$objF->set_t93($hT93);
	$objF->set_t94($hT94);
	$objF->set_t95($hT95);
	$objF->set_t96($hT96);
	$objF->set_t97($hT97);
	$objF->set_t98($hT98);
	$objF->set_t99($hT99);
	$objF->set_t00($hT00);	
	
	if($hInsureFormId != "" ){
		$objF->set_insure_form_id($hInsureFormId);
		$objF->update();
	}else{
		$hInsureFormId = $objF->add();
		$objF->set_insure_form_id($hInsureFormId);
	}
	
	
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Toyota Buzz  : Control Center System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element.css">
</head>

<body>
<form action="frm_02_07.php" method="post" name="frm">
<input type="hidden" name="hInsureFormId" value="<?=$objF->get_insure_form_id()?>">
<input type="hidden" name="hSubmit" value="submit">
<input type="hidden" name="hId" value="<?=$hId?>">
<input type="hidden" name="hFormType" value="0207">


<table width="850" align="center" class="search" bgcolor="#ffffff">
<tr>
	<td>
	<table width="100%">
	<tr>
		<td align="center" class="Head">����ѷ �Ե��� ������� �Թ����ѹ�� �ӡѴ �Ңһ������</td>
	</tr>
	<tr>
		<td align="center">175 �Ҥ���ҸëԵ�� �������� ��� 14  ��� �Ҹ���  �ǧ���������  ࢵ�ҷ�  ��ا෾�  10120</td>
	</tr>
	<tr>
		<td align="center">��. +66(0) 2679-6165-87  �����. +66 (0) 2679 6526-27</td>
	</tr>
	</table>
	<br><br>
	<table width="100%">
	<tr>
		<td width="50%" rowspan="2">
			<table width="100%">
			<tr>
				<td>�ѹ��� </td>
				<td><?=$objF->get_t01()?></td>
			</tr>
			<tr>
				<td><input type="checkbox" value="1" name="hT03" <?if($objF->get_t03() == 1 ) echo "checked"?>></td>
				<td>�ç����Թʴ</td>
			</tr>
			<tr>
				<td><input type="checkbox" value="1" name="hT05" <?if($objF->get_t05() == 1 ) echo "checked"?>></td>
				<td>�ç��� GOA (�����) �·�������</td>
			</tr>
			<tr>
				<td><input type="checkbox" value="1" name="hT07" <?if($objF->get_t07() == 1 ) echo "checked"?>></td>
				<td>�ç��� GOA (�١��Ҩ���/����������)</td>
			</tr>
			<tr>
				<td><input type="checkbox" value="1" name="hT09" <?if($objF->get_t09() == 1 ) echo "checked"?>></td>
				<td>�����㹹�� <input type="text" name="hT10" size="30" value="<?=$objF->get_t10()?>"></td>
			</tr>
			</table>		
		</td>
		<td width="50%">
		
			<table width="100%">
			<tr>
				<td>�����</td>
				<td><?=$objF->get_t02()?></td>
			</tr>
			<tr>
				<td>����ѷ</td>
				<td><?=$objF->get_t04()?></td>
			</tr>
			</table>
		
		</td>
	</tr>
	<tr>
		<td>
		
			<table class="i_Excel" align="center">
			<tr>
				<td>�����Ţ�Ѻ��</td>
				<td><?=$objF->get_t06()?></td>
			</tr>
			<tr>
				<td>����Ѻ��</td>
				<td><?=$objF->get_t08()?></td>
			</tr>
			</table>		
		
		</td>
	</tr>
</table>

<table width="850" cellpadding="3">
<tr>
	<td width="50%">
		<table align="center">
		<tr>
			<td class="tah_13"><strong>���駷ӻ�Сѹ �����������´�ѧ���</strong></td>
		</tr>
		<tr>
			<td><input type="checkbox" value="1" name="hT59" <?if($objF->get_t59() == 1 ) echo "checked"?>>ö����  <input type="checkbox" value="1" name="hT60" <?if($objF->get_t60() == 1 ) echo "checked"?>>ö������   <input type="checkbox" value="1" name="hT61" <?if($objF->get_t61() == 1 ) echo "checked"?>>ö�͹����</td>
		</tr>
		</table>

	
	</td>
	<td width="50%">
		<table>
		<tr>
			<td>�Ţ���ѵù��˹��</td>
			<td><?=$objF->get_t11()?></td>
		</tr>
		<tr>
			<td>��ѡ�ҹ����Ѻ��Сѹ���</td>
			<td><?=$objF->get_t12()?></td>
		</tr>
		<tr>
			<td>�������/�Ţ���</td>
			<td><?=$objF->get_t13()?> / <?=$objF->get_t14()?></td>
		</tr>
		</table>

	
	</td>
</tr>
</table>
	

<table>
<tr>
	<td>���ͼ����һ�Сѹ </td>
	<td><?=$objF->get_t15()?>&nbsp;&nbsp;&nbsp;�Ţ���ѵû�ЪҪ� / ����¹�ԵԺؤ���Ţ��� <?=$objF->get_t16()?></td>
</tr>
<tr>
	<td>�������</td>
	<td><?=$objF->get_t17()?></td>
</tr>
<tr>
	<td colspan="2">
<table width="100%">
<tr>
	<td>�ǧ/�Ӻ�</td>
	<td><?=$objF->get_t18()?></td>
	<td>ࢵ/�����</td>
	<td><?=$objF->get_t19()?></td>
	<td>�ѧ��Ѵ</td>
	<td><?=$objF->get_t20()?></td>
	<td>������ɳ���</td>
	<td><?=$objF->get_t21()?></td>
</tr>
<tr>
	<td>���Ѿ���ҹ</td>
	<td><?=$objF->get_t22()?></td>
	<td>���Ѿ����ӧҹ</td>
	<td><?=$objF->get_t23()?></td>
	<td>��Ͷ��</td>
	<td><?=$objF->get_t24()?></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td colspan="8"><strong>��������´ö</strong></td>
</tr>
<tr>
	<td>������</td>
	<td><input type="text" name="hT25" size="20" value="<?=$objF->get_t25()?>"></td>
	<td>���</td>
	<td><input type="text" name="hT26" size="20" value="<?=$objF->get_t26()?>"></td>
	<td>Ẻ</td>
	<td><input type="text" name="hT27" size="20" value="<?=$objF->get_t27()?>"></td>
	<td>��</td>
	<td><input type="text" name="hT28" size="10" value="<?=$objF->get_t28()?>"></td>
</tr>
<tr>
	<td>��Ƕѧ</td>
	<td><input type="text" name="hT29" size="20" value="<?=$objF->get_t29()?>"></td>
	<td>�Ҥ�ö¹��</td>
	<td><input type="text" name="hT30" size="20" value="<?=$objF->get_t30()?>"></td>
	<td>����ͧ¹��</td>
	<td><input type="text" name="hT31" size="20" value="<?=$objF->get_t31()?>"> </td>
	<td>�����ء�к͡�ٺ(�ի�)</td>
	<td><input type="text" name="hT32" size="10" value="<?=$objF->get_t32()?>"></td>
</tr>
<tr>
	<td>��</td>
	<td><input type="text" name="hT33" size="20" value="<?=$objF->get_t33()?>"></td>
	<td>����¹</td>
	<td><input type="text" name="hT34" size="20" value="<?=$objF->get_t34()?>"></td>
	<td>�ѧ��Ѵ</td>
	<td><input type="text" name="hT35" size="20" value="<?=$objF->get_t35()?>"></td>
	<td></td>
	<td></td>
</tr>
</table>
	
	
	
	
	
	
	
	
	
	</td>
</tr>
</table>
<table>
<tr>
	<td valign="top">�ػ�ó쵡��</td>
	<td colspan="3">
		<textarea name="hT36" rows="3" cols="100"><?=$objF->get_t36()?></textarea>
	</td>
</tr>

<tr>
	<td>����ѷ��Сѹ���</td>
	<td><input type="text" name="hT37" size="30" value="<?=$objF->get_t37()?>"></td>
	<td>������������ͧ</td>
	<td><input type="text" name="hT38" size="30" value="<?=$objF->get_t38()?>"></td>
</tr>
<tr>
	<td>�����Ţ������</td>
	<td><input type="text" name="hT39" size="30" value="<?=$objF->get_t39()?>"></td>
	<td>�������� �.�.�. �Ţ���</td>
	<td><input type="text" name="hT40" size="30" value="<?=$objF->get_t40()?>"></td>
</tr>
<tr>
	<td>����Ѻ�Ż���ª�� (�ṹ��)</td>
	<td colspan="3"><input type="text" name="hT41" size="95" value="<?=$objF->get_t41()?>"></td>
</tr>
<tr>
	<td>������</td>
	<td><input type="checkbox" value="1" name="hT42" <?if($objF->get_t42() == 1 ) echo "checked"?>>����кت��ͼ��Ѻ��� <input type="checkbox" value="1" name="hT62" <?if($objF->get_t62() == 1 ) echo "checked"?>> �кت��ͼ��Ѻ���</td>
	<td>ö��Шӵ��˹�</td>
	<td><input type="text" name="hT43" size="30" value="<?=$objF->get_t43()?>"></td>
</tr>
<tr>
	<td>���ͼ��Ѻ����� 1</td>
	<td><input type="text" name="hT44" size="30" value="<?=$objF->get_t44()?>"></td>
	<td>�ѹ/��͹/���Դ</td>
	<td><input type="text" name="hT45" size="30" value="<?=$objF->get_t45()?>"></td>
</tr>
<tr>
	<td>�Ţ���ѵû�ЪҪ�</td>
	<td><input type="text" name="hT46" size="30" value="<?=$objF->get_t46()?>"></td>
	<td>�Ţ���㺢Ѻ���</td>
	<td><input type="text" name="hT47" size="30" value="<?=$objF->get_t47()?>"></td>
</tr>
<tr>
	<td>���ͼ��Ѻ����� 2</td>
	<td><input type="text" name="hT48" size="30" value="<?=$objF->get_t48()?>"></td>
	<td>�ѹ/��͹/���Դ</td>
	<td><input type="text" name="hT49" size="30" value="<?=$objF->get_t49()?>"></td>
</tr>
<tr>
	<td>�Ţ���ѵû�ЪҪ�</td>
	<td><input type="text" name="hT50" size="30" value="<?=$objF->get_t50()?>"></td>
	<td>�Ţ���㺢Ѻ���</td>
	<td><input type="text" name="hT51" size="30" value="<?=$objF->get_t51()?>"></td>
</tr>

<tr>
	<td>�ع��Сѹ</td>
	<td><input type="text" name="hT52" size="30" value="<?=$objF->get_t52()?>"></td>
	<td>���»�Сѹ���</td>
	<td><input type="text" name="hT53" size="30" value="<?=$objF->get_t53()?>"></td>
</tr>
<tr>
	<td>�ѹ�����������ͧ�Ҥ��Ѥ��</td>
	<td><input type="text" name="hT54" size="30" value="<?=$objF->get_t54()?>"></td>
	<td>�ѹ�����������ͧ �.�.�.</td>
	<td><input type="text" name="hT55" size="30" value="<?=$objF->get_t55()?>"></td>
</tr>
<tr>
	<td>�Ţ�����������</td>
	<td><input type="text" name="hT56" size="30" value="<?=$objF->get_t56()?>"></td>
	<td>����ѷ��Сѹ���</td>
	<td><input type="text" name="hT57" size="30" value="<?=$objF->get_t57()?>"></td>
</tr>
<tr>
	<td>�����˵�</td>
	<td colspan="3"><input type="text" name="hT58" size="95" value="<?=$objF->get_t58()?>"></td>
</tr>
</table>



<br><br>
<div align="center"><input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?if($objF->get_insure_form_id() != ""){?><input type="button" value="�����Ẻ�����" name="hCheck" onclick="window.open('frm_02_07_print.php?');"><?}?></div>
</form>
<br><br>
	
	
	
	</td>
</tr>
</table>


		
</body>
</html>
<script>
	function checkSubmit(){
		if(confirm("�س��ͧ��÷��кѹ�֡�������������")){
			document.frm.submit();
			return true;
		}
		return false;
	}
</script>
<?include "unset_all.php";?>
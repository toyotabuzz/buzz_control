<?
include("common.php");

$objInsureBea = new InsureBea();
$objInsureBea->set_insure_bea_id($hId);
$objInsureBea->load();

$objInsureBeaItemDetail01 = new InsureBeaItemDetail();
$objInsureBeaItemDetail02 = new InsureBeaItemDetail();
$objInsureBeaItemDetail03 = new InsureBeaItemDetail();
$objInsureBeaItemDetail04 = new InsureBeaItemDetail();
$objInsureBeaItemDetail05 = new InsureBeaItemDetail();

$objInsureCompany = new InsureCompany();
$objInsureCompany->setInsureCompanyId($objInsureBea->get_insure_company_id());
$objInsureCompany->load();

$objInsureBeaItem = new InsureBeaItem();

if (empty($hSubmit)) {
		$strMode="Add";
} else {
	if (!empty($hSubmit)) {

			$objInsureBeaItem->set_insure_bea_item_id($hInsureBeaItemId);
			$objInsureBeaItem->set_insure_bea_id($hId);
			$hStartDate = $hOfficerYear."-".$hOfficerMonth."-".$hOfficerDay;
			$objInsureBeaItem->set_start_date($hStartDate);
			$hEndDate = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
			$objInsureBeaItem->set_end_date($hEndDate);

			$objInsureBeaItem->set_plan0101($hPlan0101);
			$objInsureBeaItem->set_plan0102($hPlan0102);
			$objInsureBeaItem->set_plan0103(str_replace(",", "",$hPlan0103));
			$objInsureBeaItem->set_plan0104(str_replace(",", "",$hPlan0104));
			$objInsureBeaItem->set_plan0105(str_replace(",", "",$hPlan0105));
			$objInsureBeaItem->set_plan0106(str_replace(",", "",$hPlan0106));
			$objInsureBeaItem->set_plan0107(str_replace(",", "",$hPlan0107));
			$objInsureBeaItem->set_plan0108($hPlan0108);
			$objInsureBeaItem->set_plan0109(str_replace(",", "",$hPlan0109));
			$objInsureBeaItem->set_plan0110(str_replace(",", "",$hPlan0110));
			$objInsureBeaItem->set_plan0111(str_replace(",", "",$hPlan0111));
			$objInsureBeaItem->set_plan0112(str_replace(",", "",$hPlan0112));
			

    		$pasrErr = $objInsureBeaItem->check($hSubmit);

			If ( Count($pasrErr) == 0 ){

				$pId=$objInsureBeaItem->add();
				
				//delete old file
				if (file_exists("csv/".$_FILES["hFile"]["name"])  )
			    {
				    unLink("csv/".$_FILES["hFile"]["name"]);
			    }
				
				move_uploaded_file($_FILES["hFile"]["tmp_name"],"csv/".$_FILES["hFile"]["name"]); // Copy/Upload CSV
				
				$objCSV = fopen("csv/".$_FILES["hFile"]["name"], "r");
				while (($objArr = fgetcsv($objCSV, 1000, ",")) !== FALSE) {
				
					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_id($pId);
					$objIBD->set_insure_bea_id($hId);
					$objIBD->set_total(str_replace(",", "",$objArr[0]));
					
					if($objArr[1] > 0){
					
						$objIB = new InsureBeaItemDetail();

						$objIB->set_insure_bea_item_id($pId);
						$objIB->set_insure_bea_id($hId);
						$objIB->set_plan(1);
						$objIB->set_total(str_replace(",", "",$objArr[0]));
						$objIB->set_price1(str_replace(",", "",$objArr[1]));
						$objIB->set_price2(str_replace(",", "",$objArr[2]));
						$objIB->set_price3(str_replace(",", "",$objArr[3]));
						$objIB->set_price4(str_replace(",", "",$objArr[4]));
						$objIB->set_driver_age($hDriverAge01);
						$objIB->add();

					}
					
					if($objArr[5] > 0){
						$objIB = new InsureBeaItemDetail();

						$objIB->set_insure_bea_item_id($pId);
						$objIB->set_insure_bea_id($hId);
						$objIB->set_plan(2);
						$objIB->set_total(str_replace(",", "",$objArr[0]));
						$objIB->set_price1(str_replace(",", "",$objArr[5]));
						$objIB->set_price2(str_replace(",", "",$objArr[6]));
						$objIB->set_price3(str_replace(",", "",$objArr[7]));
						$objIB->set_price4(str_replace(",", "",$objArr[8]));
						$objIB->set_driver_age($hDriverAge02);
						$objIB->add();					
					}

					if($objArr[9] > 0){
						$objIB = new InsureBeaItemDetail();

						$objIB->set_insure_bea_item_id($pId);
						$objIB->set_insure_bea_id($hId);
						$objIB->set_plan(3);
						$objIB->set_total(str_replace(",", "",$objArr[0]));
						$objIB->set_price1(str_replace(",", "",$objArr[9]));
						$objIB->set_price2(str_replace(",", "",$objArr[10]));
						$objIB->set_price3(str_replace(",", "",$objArr[11]));
						$objIB->set_price4(str_replace(",", "",$objArr[12]));
						$objIB->set_driver_age($hDriverAge03);
						$objIB->add();					
					}
					
					if($objArr[13] > 0){
						$objIB = new InsureBeaItemDetail();

						$objIB->set_insure_bea_item_id($pId);
						$objIB->set_insure_bea_id($hId);
						$objIB->set_plan(4);
						$objIB->set_total(str_replace(",", "",$objArr[0]));
						$objIB->set_price1(str_replace(",", "",$objArr[13]));
						$objIB->set_price2(str_replace(",", "",$objArr[14]));
						$objIB->set_price3(str_replace(",", "",$objArr[15]));
						$objIB->set_price4(str_replace(",", "",$objArr[16]));
						$objIB->set_driver_age($hDriverAge04);
						$objIB->add();					
					}
					
					if($objArr[17] > 0){
						$objIB = new InsureBeaItemDetail();

						$objIB->set_insure_bea_item_id($pId);
						$objIB->set_insure_bea_id($hId);
						$objIB->set_plan(5);
						$objIB->set_total(str_replace(",", "",$objArr[0]));
						$objIB->set_price1(str_replace(",", "",$objArr[17]));
						$objIB->set_price2(str_replace(",", "",$objArr[18]));
						$objIB->set_price3(str_replace(",", "",$objArr[19]));
						$objIB->set_price4(str_replace(",", "",$objArr[20]));
						$objIB->set_driver_age($hDriverAge05);
						$objIB->add();					
					}

				}
				fclose($objCSV);
				

				unset ($objInsureBeaItem);

				header("location:beaUpdateItem.php?hId=$hId");
				exit;
			}else{
				$objInsureBea->init();
			}//end check Count($pasrErr)
		}
}


$pageTitle = "3. �����������ѡ";
$strHead03="�ѹ�֡�����ź���ѷ��Сѹ���";
$pageContent =  " 3.14 �����Ť�����»��.�Ҥ��Ѥ�� > �Ҥ����»�Сѹ��� ";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<br>
<form name="hFrm03" method="post" action="beaUpdateItem.php">
<input type="hidden" name="hSearch1" value="yes">
<table align="center" cellspacing="2" cellpadding="2" border="0" class="search" width="800">
<tr>
	<td class="listTitle01"  align="center">Ẻ�ҹ�������Ǣ�ͧ�Ѻ��Сѹ��� <strong><?=$objInsureCompany->getTitle();?></strong></td>
</tr>
<tr>	
	<td >
		<?
		$objIBList = new InsureBeaList();
		$objIBList->setFilter(" insure_company_id = ".$objInsureBea->get_insure_company_id());
		$objIBList->setSort(" title ASC ");
		$objIBList->setPageSize(0);
		$objIBList->load();
		?>
		<select name="hId">		
		<?
		forEach($objIBList->getItemList() as $objItem) {
		?>
		<option value="<?=$objItem->get_insure_bea_id()?>" <?if($hId == $objItem->get_insure_bea_id()) echo "selected"?>><?=$objItem->get_title();?>
	<?}?>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="hSubmit" value="����">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="hSubmit" value="��Ѻ˹����¡�����" onclick="window.location='beaList.php';">
	</td>

</tr>
    <tr> 

      <td> 
	  		<strong>������ö</strong> : <?=$objInsureBea->get_personal_type()?> &nbsp;&nbsp;
			<strong>��������ë���</strong> : <?=$objInsureBea->get_fix_type()?> &nbsp;&nbsp;
			<strong>��ǹŴ����ѵԴ�</strong> : <?=$objInsureBea->get_percent()?> % &nbsp;&nbsp;
			<strong>��ö</strong> : <?=$objInsureBea->get_car_year()?> &nbsp;&nbsp;
			<?
		$objInsureType = new InsureType();
		$objInsureType->setInsureTypeId($objItem->get_insure_type_id());
		$objInsureType->load();
			?>
			<strong>������</strong> : <?=$objInsureType->getTitle()?> &nbsp;&nbsp;			
			<strong>���ʧҹ</strong> : 
				<?
				$objInsureFee = new InsureFee1();
				$objInsureFee->set_insure_fee_id($objInsureBea->get_fee_code());
				$objInsureFee->load();
				?>			
				<?=$objInsureFee->get_code();?>&nbsp;&nbsp;
			<strong>��������</strong>  : <?=$objInsureBea->get_group_type()?> 
			
      </td>
    </tr>		
</table>
</form>
<br>
<form name="frm01" enctype="multipart/form-data" action="beaUpdateItemAdd.php?hId=<?=$hId?>" method="POST"  >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hInsureBeaId" value="<?=$hId?>">
	<table align="center" cellspacing="2" cellpadding="2" border="0" class="search" >
	<tr>
		<td class="listTitle01"  colspan="1" align="center">�ѹ�֡�Ҥ�����</td>
	</tr>
	 <tr>
      <td colspan="1"> 
			<table>
			<tr>
			<td>�ѹ��� :</td>
			<td>
				<table>
				<td><INPUT align="middle" size="2" maxlength="2"   name=hOfficerDay value="<?=$hOfficerDay?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hOfficerMonth value="<?=$hOfficerMonth?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hOfficerYear" value="<?=$hOfficerYear?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hOfficerYear,hOfficerDay, hOfficerMonth, hOfficerYear,popCal);return false"></td>							
				</table>
			</td>
			<td>
				<table>
				<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay value="<?=$hInsureDay?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth value="<?=$hInsureMonth?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hInsureYear" value="<?=$hInsureYear?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear,hInsureDay, hInsureMonth, hInsureYear,popCal);return false"></td>							
				</table>
			</td>		
			</tr>	
			</table>
</td>
</tr>	

<tr>	
	<td  class="listTitle" align="center">
			�բ����Ū�ͧ��� 1 <input type="checkbox" name="hDriverAgeCheck01">
	  		<select name="hDriverAge01">
				<option value="" <?if($objInsureBeaItemDetail01->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail01->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail01->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail01->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail01->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail01->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>	
</tr>
<tr >
	<td  class="listTitle" align="center">
			�բ����Ū�ͧ��� 2 <input type="checkbox" name="hDriverAgeCheck02">
	  		<select name="hDriverAge02">
				<option value="" <?if($objInsureBeaItemDetail02->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail02->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail02->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail02->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail02->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail02->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>	
</tr>
<tr >
	<td  class="listTitle" align="center">
			�բ����Ū�ͧ��� 3 <input type="checkbox" name="hDriverAgeCheck03">
	  		<select name="hDriverAge03">
				<option value="" <?if($objInsureBeaItemDetail03->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail03->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail03->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail03->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail03->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail03->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>	
</tr>
<tr >
	<td  class="listTitle" align="center">
			�բ����Ū�ͧ��� 4 <input type="checkbox" name="hDriverAgeCheck04">
	  		<select name="hDriverAge04">
				<option value="" <?if($objInsureBeaItemDetail04->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail04->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail04->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail04->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail04->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail04->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>	
</tr>
<tr >
	<td  class="listTitle" align="center">
			�բ����Ū�ͧ��� 5 <input type="checkbox" name="hDriverAgeCheck05">
	  		<select name="hDriverAge05">
				<option value="" <?if($objInsureBeaItemDetail05->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail05->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail05->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail05->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail05->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail05->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>	

</tr>
<tr >
	<td ><strong>��������´����������ͧ</strong></td>	
</tr>
<tr >		
	<td  class="listDetail"   align="center"><input type="text" size="20"  id="hPlan0101" name="hPlan0101" value="<?=$objInsureBeaItem->get_plan0101()?>"></td>
</tr>
<tr >		
	<td  class="listDetail"   align="center"><input type="text" size="20"  id="hPlan0102" name="hPlan0102" value="<?=$objInsureBeaItem->get_plan0102()?>"></td>
</tr>
<tr >
	<td colspan="1"  class="listTitle" ><strong>�����Ѻ�Դ�ͺ��ͺؤ����¹͡</strong></td>
</tr>
<tr >
	<td colspan="1"   align="left">����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����</td>
</tr>
<tr>		
	<td   align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0103" name="hPlan0103" value="<?=$objInsureBeaItem->get_plan0103()?>"></td>

</tr>
<tr >
	<td colspan="1"    class="listDetail"  >�����ͤ���</td>
</tr>
<tr>		
	<td  class="listDetail"    align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0104" name="hPlan0104" value="<?=$objInsureBeaItem->get_plan0104()?>"></td>

</tr>
<tr >
	<td colspan="1"   align="left">����������µ�ͷ�Ѿ���Թ���ó�</td>
</tr>
<tr>		
	<td   align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0105" name="hPlan0105" value="<?=$objInsureBeaItem->get_plan0105()?>"></td>
</tr>
<tr >
	<td colspan="1"    class="listTitle" >����������ͧö¹������һ�Сѹ���</td>
</tr>
<tr >
	<td colspan="1"   align="left">����������µ��ö¹������һ�Сѹ���</td>
</tr>
<tr>		
	<td   align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0106" name="hPlan0106" value="<?=$objInsureBeaItem->get_plan0106()?>"></td>
</tr>
<tr >
	<td colspan="1"     class="listDetail" align="left">ö¹���٭���/�����</td>
</tr>
<tr>		
	<td  class="listDetail"    align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0107" name="hPlan0107" value="<?=$objInsureBeaItem->get_plan0107()?>"></td>
</tr>
<tr >
	<td colspan="1"     class="listDetail" align="left">�������������ǹ�á</td>
</tr>
<tr>		
	<td  class="listDetail"    align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0112" name="hPlan0112" value="<?=$objInsureBeaItem->get_plan0112()?>"></td>
</tr>
<tr >
	<td colspan="1"     class="listTitle" ><strong>����������ͧ����͡���Ṻ����</strong></td>
</tr>
<tr>		
	<td class="listTitle"   align="center"><input type="text" size="20"   id="hPlan0108"  name="hPlan0108" value="<?=$objInsureBeaItem->get_plan0108()?>"></td>
</tr>
<tr >
	<td colspan="1"    class="listDetail"  align="left">�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����</td>
</tr>
<tr>		
	<td  class="listDetail"    align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0109"  name="hPlan0109" value="<?=$objInsureBeaItem->get_plan0109()?>"></td>
</tr>
<tr >
	<td colspan="1"    align="left">����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����</td>
</tr>
<tr>		
	<td   align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0110" name="hPlan0110" value="<?=$objInsureBeaItem->get_plan0110()?>"></td>
</tr>
<tr >
	<td colspan="1"    class="listDetail"  align="left">��Сѹ��Ǽ��Ѻ���</td>
</tr>
<tr>		
	<td  class="listDetail"    align="center"><input type="text" size="20" onblur="tryNumberFormat(this)"  id="hPlan0111" name="hPlan0111" value="<?=$objInsureBeaItem->get_plan0111()?>"></td>
</tr>			
    <tr> 
      <td colspan="1" valign="top">���Ẻ�Ҥ� CSV:</td>
</tr>
<tr>		  
      <td colspan="1"> 
			<input type="file" name="hFile"> ������ҧẺ�����ʡ�� CSV <a target="_blank" href="Book1.csv">����</a>
      </td>
    </tr>		
    <tr> 
      <td align="center" colspan="1">
	  <br><br><br><br>
        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
		<?if ($strMode == "Update"){?>
			<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" onclick="return check_form();">
		<?}else{?>
			<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" onclick="return check_form();">
		<?}?>
        <input type="Button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='beaList.php'">
      </td>
    </tr>
  </table>
</form>

<br><br><br><br>
<?
	include("h_footer.php")
?>
<script>
	new CAPXOUS.AutoComplete("hInsureFeeCode", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_fee.php?hFrm=frm01&q=" + this.text.value;
		}
	});		


	function check_form(){

			if (document.forms.frm01.hOfficerDay.value=="" || document.forms.frm01.hOfficerDay.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerDay.value,1,31) == false) {
					document.forms.frm01.hOfficerDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hOfficerMonth.value==""  || document.forms.frm01.hOfficerMonth.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerMonth.value,1,12) == false){
					document.forms.frm01.hOfficerMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hOfficerYear.value==""  || document.forms.frm01.hOfficerYear.value=="0000")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hOfficerYear.focus();
					return false;
				}
			} 				
		
	
	
			if (document.forms.frm01.hInsureDay.value=="" || document.forms.frm01.hInsureDay.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureDay.value,1,31) == false) {
					document.forms.frm01.hInsureDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hInsureMonth.value==""  || document.forms.frm01.hInsureMonth.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureMonth.value,1,12) == false){
					document.forms.frm01.hInsureMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hInsureYear.value==""  || document.forms.frm01.hInsureYear.value=="0000")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hInsureYear.focus();
					return false;
				}
			} 				


		document.forms.frm01.submit();
	}

	
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
	function copy(vals){
	
		eval('val=document.getElementById("hCopy'+vals+'").value');	
		
		if(val == 1){
			val1 = document.getElementById("hPlan0101").value;
			val2 = document.getElementById("hPlan0102").value;
			val3 = document.getElementById("hPlan0103").value;
			val4 = document.getElementById("hPlan0104").value;
			val5 = document.getElementById("hPlan0105").value;
			val6 = document.getElementById("hPlan0106").value;
			val7 = document.getElementById("hPlan0107").value;
			val8 = document.getElementById("hPlan0108").value;
			val9 = document.getElementById("hPlan0109").value;
			val10 = document.getElementById("hPlan0110").value;
			val11 = document.getElementById("hPlan0111").value;
		}
	
		if(val == 2){
			val1 = document.getElementById("hPlan0201").value;
			val2 = document.getElementById("hPlan0202").value;
			val3 = document.getElementById("hPlan0203").value;
			val4 = document.getElementById("hPlan0204").value;
			val5 = document.getElementById("hPlan0205").value;
			val6 = document.getElementById("hPlan0206").value;
			val7 = document.getElementById("hPlan0207").value;
			val8 = document.getElementById("hPlan0208").value;
			val9 = document.getElementById("hPlan0209").value;
			val10 = document.getElementById("hPlan0210").value;
			val11 = document.getElementById("hPlan0211").value;
		}	
	
		if(val == 3){
			val1 = document.getElementById("hPlan0301").value;
			val2 = document.getElementById("hPlan0302").value;
			val3 = document.getElementById("hPlan0303").value;
			val4 = document.getElementById("hPlan0304").value;
			val5 = document.getElementById("hPlan0305").value;
			val6 = document.getElementById("hPlan0306").value;
			val7 = document.getElementById("hPlan0307").value;
			val8 = document.getElementById("hPlan0308").value;
			val9 = document.getElementById("hPlan0309").value;
			val10 = document.getElementById("hPlan0310").value;
			val11 = document.getElementById("hPlan0311").value;
		}
		
		if(val == 4){
			val1 = document.getElementById("hPlan0401").value;
			val2 = document.getElementById("hPlan0402").value;
			val3 = document.getElementById("hPlan0403").value;
			val4 = document.getElementById("hPlan0404").value;
			val5 = document.getElementById("hPlan0405").value;
			val6 = document.getElementById("hPlan0406").value;
			val7 = document.getElementById("hPlan0407").value;
			val8 = document.getElementById("hPlan0408").value;
			val9 = document.getElementById("hPlan0409").value;
			val10 = document.getElementById("hPlan0410").value;
			val11 = document.getElementById("hPlan0411").value;
		}
		
		if(val == 5){
			val1 = document.getElementById("hPlan0501").value;
			val2 = document.getElementById("hPlan0502").value;
			val3 = document.getElementById("hPlan0503").value;
			val4 = document.getElementById("hPlan0504").value;
			val5 = document.getElementById("hPlan0505").value;
			val6 = document.getElementById("hPlan0506").value;
			val7 = document.getElementById("hPlan0507").value;
			val8 = document.getElementById("hPlan0508").value;
			val9 = document.getElementById("hPlan0509").value;
			val10 = document.getElementById("hPlan0510").value;
			val11 = document.getElementById("hPlan0511").value;
		}
		
		if(val == 6){
			val1 = document.getElementById("hPlan0601").value;
			val2 = document.getElementById("hPlan0602").value;
			val3 = document.getElementById("hPlan0603").value;
			val4 = document.getElementById("hPlan0604").value;
			val5 = document.getElementById("hPlan0605").value;
			val6 = document.getElementById("hPlan0606").value;
			val7 = document.getElementById("hPlan0607").value;
			val8 = document.getElementById("hPlan0608").value;
			val9 = document.getElementById("hPlan0609").value;
			val10 = document.getElementById("hPlan0610").value;
			val11 = document.getElementById("hPlan0611").value;
		}
		
		if(val == 7){
			val1 = document.getElementById("hPlan0701").value;
			val2 = document.getElementById("hPlan0702").value;
			val3 = document.getElementById("hPlan0703").value;
			val4 = document.getElementById("hPlan0704").value;
			val5 = document.getElementById("hPlan0705").value;
			val6 = document.getElementById("hPlan0706").value;
			val7 = document.getElementById("hPlan0707").value;
			val8 = document.getElementById("hPlan0708").value;
			val9 = document.getElementById("hPlan0709").value;
			val10 = document.getElementById("hPlan0710").value;
			val11 = document.getElementById("hPlan0711").value;
		}
		
		if(val == 8){
			val1 = document.getElementById("hPlan0801").value;
			val2 = document.getElementById("hPlan0802").value;
			val3 = document.getElementById("hPlan0803").value;
			val4 = document.getElementById("hPlan0804").value;
			val5 = document.getElementById("hPlan0805").value;
			val6 = document.getElementById("hPlan0806").value;
			val7 = document.getElementById("hPlan0807").value;
			val8 = document.getElementById("hPlan0808").value;
			val9 = document.getElementById("hPlan0809").value;
			val10 = document.getElementById("hPlan0810").value;
			val11 = document.getElementById("hPlan0811").value;
		}
		
		if(val == 9){
			val1 = document.getElementById("hPlan0901").value;
			val2 = document.getElementById("hPlan0902").value;
			val3 = document.getElementById("hPlan0903").value;
			val4 = document.getElementById("hPlan0904").value;
			val5 = document.getElementById("hPlan0905").value;
			val6 = document.getElementById("hPlan0906").value;
			val7 = document.getElementById("hPlan0907").value;
			val8 = document.getElementById("hPlan0908").value;
			val9 = document.getElementById("hPlan0909").value;
			val10 = document.getElementById("hPlan0910").value;
			val11 = document.getElementById("hPlan0911").value;
		}
		
		if(val == 10){
			val1 = document.getElementById("hPlan1001").value;
			val2 = document.getElementById("hPlan1002").value;
			val3 = document.getElementById("hPlan1003").value;
			val4 = document.getElementById("hPlan1004").value;
			val5 = document.getElementById("hPlan1005").value;
			val6 = document.getElementById("hPlan1006").value;
			val7 = document.getElementById("hPlan1007").value;
			val8 = document.getElementById("hPlan1008").value;
			val9 = document.getElementById("hPlan1009").value;
			val10 = document.getElementById("hPlan1010").value;
			val11 = document.getElementById("hPlan1011").value;
		}															
	
			eval('document.getElementById("hPlan'+vals+'01").value=val1');
			eval('document.getElementById("hPlan'+vals+'02").value=val2');
			eval('document.getElementById("hPlan'+vals+'03").value=val3');
			eval('document.getElementById("hPlan'+vals+'04").value=val4');
			eval('document.getElementById("hPlan'+vals+'05").value=val5');
			eval('document.getElementById("hPlan'+vals+'06").value=val6');
			eval('document.getElementById("hPlan'+vals+'07").value=val7');
			eval('document.getElementById("hPlan'+vals+'08").value=val8');
			eval('document.getElementById("hPlan'+vals+'09").value=val9');
			eval('document.getElementById("hPlan'+vals+'10").value=val10');
			eval('document.getElementById("hPlan'+vals+'11").value=val11');
	
	
	}
	
</script>
<?include "unset_all.php";?>
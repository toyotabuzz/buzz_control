<?
header('Content-Type: text/html; charset=utf-8');
include "common.php";
$q = trim($q);
$pos = strpos($q, ' ');

if($pos == false){	
	$strCondition = " ( province_name LIKE '%".trim($q)."%' or pcode LIKE '%".trim($q)."%'  )";
}

$objProvinceList = new ProvinceList();
$objProvinceList->setFilter($strCondition);
$objProvinceList->setPageSize(0);
$objProvinceList->setSortDefault(" province_name ASC");
$objProvinceList->loadUTF8();

forEach($objProvinceList->getItemList() as $objItem) {
?>
<div onSelect="this.text.value='<?=$objItem->getPcode()?>';"><span class='informal'></span><span><?=$objItem->getProvinceName()?> (<?=$objItem->getPcode()?>)</span></div>
<?}

unset($objProvinceList);
?>
<?include "unset_all.php";?>
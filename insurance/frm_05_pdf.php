<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

function add_space($text,$val){
	$len = strlen($text);
	$new = $val-$len;
	if($new > 0 ){
		$new = $new/2;
	}
	
	for($x=0;$x<$new;$x++){
		$space = $space." ";
	}
	
	$new_text = $space.$text.$space;
	return $new_text;
}


class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,50,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


			
				$pdf->AddPage();
				$pdf->SetLeftMargin(50);

				$objInsure = new Insure();
				$objInsure->set_insure_id($hId);
				$objInsure->load();
				
				$objCompany = new Company();
				$objCompany->setCompanyId($sCompanyId);
				$objCompany->load();
				
				$objInsureCompany = new InsureCompany();
				$objInsureCompany->setInsureCompanyId($objInsure->get_k_prb_id());
				$objInsureCompany->load();
				
				$objMember = new Member();
				$objMember->setMemberId($objInsure->get_sale_id());
				$objMember->load();
				
				$objSale = new Member();
				$objSale->setMemberId($objInsure->get_sale_id());
				$objSale->load();				
				
				$objInsureCar = new InsureCar();
				$objInsureCar->set_car_id($objInsure->get_car_id());
				$objInsureCar->load();
				
				$objCustomer = new InsureCustomer();
				$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
				$objCustomer->load();
				
				$objCarType = new CarType();
				$objCarType->setCarTypeId($objInsureCar->get_car_type());
				$objCarType->load();
				
				$objCarModel = new CarModel();
				$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
				$objCarModel->load();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objInsureCar->get_color());
				$objCarColor->load();
				
				$hStartDate= formatThaiDate($objInsure->get_date_protect());
				$hStartYear = substr($hStartDate,-4);
				$hEndYear = $hStartYear+1;
				$hEndDate = substr($hStartDate,0,(strlen($hStartDate)-4)).$hEndYear;
				
				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				
				$objInsureItem = new InsureItem();
				$objInsureItem->loadByCondition(" insure_id = $hId and payment_no=1 ");
				
				$objF = new InsureForm();
				$objF->loadByCondition(" insure_id= $hId and frm_type= '0801'  ");
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = "Ẻ�Ӣ��觨��¤�����»�Сѹ���ö¹��";
				$pdf->SetXY(50,50+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetXY(390,45+$hHeight);
				$pdf->Cell(160,70,"",1,0,"C");
				$pdf->SetFont('angsa','B',12);	
				$strtext = formatThaiDate(date("Y-m-d"));
				$pdf->SetXY(400,50+$hHeight);
				$pdf->Write(10,"�ѹ��� ".$strtext);
				
				if($objCarModel->getInsureName() != ""){
					$arrCarModel[0] = $objCarModel->getInsureName();
				}else{
					$arrCarModel = explode(" ",$objCarModel->getTitle());
				}
				$strtext = $objCarType->getTitle()."/".$arrCarModel[0];
				$pdf->SetXY(400,65+$hHeight);
				$pdf->Write(10,"ö/��� ".$strtext);
				
				$strtext = $objInsureCar->get_code()." ".$objInsureCar->get_province_code();
				$pdf->SetXY(400,80+$hHeight);
				$pdf->Write(10,"����¹ ".$strtext);
				
				$strtext = $objSale->getFirstname()." (".$objSale->getNickname()." ) ". $objSale->getTel();
				$pdf->SetXY(400,95+$hHeight);
				$pdf->Write(10,"��ѡ�ҹ ".$strtext);


				$hCustomerName = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();

				
				if($hAddressType == 1){
					$hAddress1 = $objCustomer->getAddressLabel();
					if($objCustomer->mProvince == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon." ࢵ ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
					}else{
						$hAddress2 = "�Ӻ� ".$objCustomer->mTumbon." ����� ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
					}					
				}
				if($hAddressType == 2){
					$hAddress1 = $objCustomer->getAddressLabel01();
					if($objCustomer->mProvince01 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon01." ࢵ ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon01." ����� ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
					}					
				}
				if($hAddressType == 3){
					$hAddress1 = $objCustomer->getAddressLabel02();
					if($objCustomer->mProvince02 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon02." ࢵ ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon02." ����� ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
					}					
				}
				if($hAddressType == 4){
					$hAddress1 = $objCustomer->getAddressLabel03();
					if($objCustomer->mProvince03 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon03." ࢵ ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon03." ����� ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
					}					
				}
				
				$pdf->SetXY(50,120+$hHeight);
				$hYear=27;
				$pdf->Cell(20,20,"",0,0,"C");
				$pdf->Cell(50,20,"��Ҿ���",0,0,"C");
				$pdf->Cell(340,20,$hCustomerName,0,0,"C");
				$pdf->Cell(50,20,"����",0,0,"C");
				$pdf->Cell(20,20,"",0,0,"C");
				$pdf->Cell(20,20,"��",0,0,"C");
				$pdf->Ln();
				
				$pdf->line(120,135+$hHeight,470,135+$hHeight);
				
				$pdf->line(500,135+$hHeight,535,135+$hHeight);
				
				$pdf->Cell(500,20,"�������    ".$hAddress1."  ".$hAddress2,0,0,"L");
				$pdf->Ln();
				$pdf->line(70,155+$hHeight,540,155+$hHeight);

				if($objCustomer->getHomeTel() != ""){
				$hTel.= "          Tel. ".$objCustomer->getHomeTel(); 
				}
				if($objCustomer->getFax() != ""){
				$hTel.= "          Fax. ".$objCustomer->getFax(); 
				}
				if($objCustomer->getMobile() != ""){
				$hTel.= "          Mobile. ".$objCustomer->getMobile();
				}
				
				$pdf->Cell(50,20,"���ӧҹ",0,0,"L");
				$pdf->Cell(450,20,$hTel,0,0,"C");
				$pdf->Ln();
				$pdf->line(80,175+$hHeight,540,175+$hHeight);
				
				
				$pdf->Cell(180,20,"���駤���������ͧ������������Сѹ���ö¹��",0,0,"L");
				$pdf->Cell(160,20,$objInsureCompany->getTitle(),0,0,"C");
				$pdf->Cell(160,20,"��ҹ����ѷ ���������Թ����ѹ�� �á���� �ӡѴ ",0,0,"L");
				$pdf->Ln();
				$pdf->line(223,195+$hHeight,395,195+$hHeight);
				
				$pdf->Cell(60,20,"������ͧ�����",0,0,"L");
				$pdf->Cell(50,20,formatShortThaiDate($objInsure->get_k_start_date()),0,0,"C");
				$pdf->Cell(30,20,"�֧",0,0,"C");
				$pdf->Cell(50,20,formatShortThaiDate($objInsure->get_k_end_date()),0,0,"C");
				$pdf->Cell(50,20,"�������",0,0,"C");
				$pdf->Cell(100,20,number_format($objInsure->get_k_num08(),2),0,0,"C");
				$pdf->Cell(50,20,"  �ҷ  ��Ҿú. ",0,0,"C");
				$pdf->Cell(80,20,number_format($objInsure->get_k_num11(),2),0,0,"C");
				$pdf->Cell(30,20,"�ҷ",0,0,"R");
				$pdf->Ln();
				
				$pdf->line(100,215+$hHeight,165,215+$hHeight);
				$pdf->line(185,215+$hHeight,250,215+$hHeight);
				$pdf->line(290,215+$hHeight,390,215+$hHeight);
				$pdf->line(440,215+$hHeight,530,215+$hHeight);
				
				
				
				$pdf->Cell(5,20,"(   ",0,0,"L");
				$pdf->Cell(340,20,currencyThai01($objInsure->get_k_num13()),0,0,"C");
				$pdf->Cell(5,20,"   )",0,0,"R");
				$pdf->Cell(150,20,"�դ������ʧ����觪��Ф��������",0,0,"L");
				$pdf->Ln();
				$pdf->line(65,235+$hHeight,385,235+$hHeight);
				

				$pdf->Cell(180,20,"   ".$objInsure->get_pay_number()."    �Ǵ ��Т�Ҿ��Ңͪ����ҤҴѧ���     ",0,0,"L");
				if($objInsure->get_k_num10() > 0){
					$pdf->Cell(50,20,"��ǹŴ",0,0,"C");
					$pdf->Cell(30,20,number_format($objInsure->get_k_num10(),2),0,0,"C");
					$pdf->Cell(30,20,"�ҷ",0,0,"C");
				  }
				$pdf->Ln();
				$pdf->line(50,255+$hHeight,70,255+$hHeight);

				$pdf->SetXY(70,240+$hHeight);
				$pdf->WriteHTML($bodytag);
				$pdf->Ln();
				$pdf->Ln();
				
				for($i=1;$i<=$objInsure->get_pay_number();$i++){
					$objInsureItem = new InsureItem();
					$objInsureItem->loadByCondition(" insure_id = $hId AND payment_order=$i  ");
					$sum_bea=0;
					$sum_other=0;
					$objITList = new InsureItemDetailList();
					$objITList->setFilter(" OP.order_id ='".$objInsureItem->get_insure_item_id()."' and OP.order_extra_id= $hId ");
					$objITList->setPageSize(0);
					$objITList->setSort(" order_price_id ASC ");
					$objITList->load();
					forEach($objITList->getItemList() as $objItem) {
					
						$objPS = new PaymentSubject();
						$objPS->setPaymentSubjectId($objItem->getPaymentSubjectId());
						$objPS->load();
						
						if ( $objItem->getPaymentSubjectId() ==40 or $objItem->getPaymentSubjectId() ==39 or $objItem->getPaymentSubjectId() ==72or $objItem->getPaymentSubjectId() ==73  ){
							if($objItem->getPayin() == 0) {
								$sum_bea = $sum_bea+$objItem->getPrice();
								$sum_bea_all = $sum_bea_all+$objItem->getPrice();
							}else{
								$sum_bea = $sum_bea -$objItem->getPrice();
								$sum_bea_all = $sum_bea_all - $objItem->getPrice();
							}
						}else{
							if($objItem->getPayin() == 0) {
								$sum_other = $sum_other + $objItem->getPrice();
								$sum_other_all = $sum_other_all+$objItem->getPrice();
							}else{
								$sum_other = $sum_other -$objItem->getPrice();
								$sum_other_all = $sum_other_all - $objItem->getPrice();
							}
							
						}
					
					}
						
						$pdf->Cell(100,20,"�Ǵ��� ".$i." �����ѹ���",0,0,"C");
						$pdf->Cell(70,20,formatThaiDate($objInsureItem->get_payment_date()),0,0,"C");
						$pdf->Cell(70,20,"�ʹ����",0,0,"C");
						$pdf->Cell(90,20,number_format($sum_bea,2)." �ҷ",0,0,"R");
						$pdf->Cell(100,20,"�Ţ��������        ".$objInsureItem->get_payment_number(),0,0,"C");
						$pdf->Ln();
						$pdf->line(140,275+$hHeight+($i*20),240,275+$hHeight+($i*20));
						$pdf->line(300,275+$hHeight+($i*20),390,275+$hHeight+($i*20));
						$pdf->line(450,275+$hHeight+($i*20),550,275+$hHeight+($i*20));
				}
				
						$pdf->Cell(100,20,"",0,0,"C");
						$pdf->Cell(70,20,"",0,0,"C");
						$pdf->Cell(80,20,"���",0,0,"C");
						$pdf->Cell(80,20,number_format($objInsure->get_k_num13(),2)." �ҷ",0,0,"R");
						$pdf->Cell(80,20,"",0,0,"C");
						$pdf->Ln();
						$pdf->line(300,275+$hHeight+($i*20),390,275+$hHeight+($i*20));
						$pdf->line(300,277+$hHeight+($i*20),390,277+$hHeight+($i*20));
				
				$pdf->Ln();				
				$bodytag = "          ��Ҿ����Ѻ��Һ�������������Ң�Ҿ������Ѻ����������ͧ����������ҷ���˹��ҡ��Ҿ�������Ф�����¶١��ͧ���<br>��˹��Ǵ���������Դ�Ѵ   ��駹���ҡ��Ҿ��ҼԴ�Ѵ�����§�Ǵ˹�觧Ǵ�   ��Ҿ����Թ������  ����ѷ ���������Թ����ѹ�� �á���� �ӡѴ �ӡ��<br>";
				$bodytag.= "¡��ԡ��������  ���Ծѡ��ͧ�͡����ǡ�͹  ��Т�Ҿ����Թ������Է�����¡��ͧ㹡�âͤ׹�Թ����ҡ��Ҿ������������������";
				$pdf->WriteHTML($bodytag);
				
				$pdf->Ln();
				$pdf->Ln();
				$bodytag = "��Ҿ�������ҹ����������͹䢴����Ǩ֧ŧ�����ͪ��͵���Ӣ͹��";
				$pdf->WriteHTML($bodytag);
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Cell(500,20,"ŧ����.................................................................�����蹤Ӣ�",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,20,"(..............................................................)",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,20,"**�óռ���繵��������͵ç�Ѻ��Ңͧö���ǧ������᷹��Т����Һѵû�ЪҪ��繵����Ҷ١��ͧ",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(250,10,"����ѷ � ��Ԩ�óҤӢ͢�ҧ�������դ������",0,0,"C");
				$pdf->Cell(10,10,"     ",1,0,"L");
				$pdf->Cell(70,10,"͹��ѵ�",0,0,"L");
				$pdf->Cell(10,10,"     ",1,0,"L");
				$pdf->Cell(100,10,"���͹��ѵ� ����Ӣ͹��",0,0,"L");
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Cell(500,20,"ŧ����.................................................................���͹��ѵ�",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,20,"(..............................................................)",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,-64,"",1,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,67,"",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(100,15,"�Ըա�ê����Թ",0,0,"L");
				$pdf->Ln();
				$pdf->Cell(300,15,"        - �������Թʴ/�ѵ��ôԵ ���µ��ͧ������ѷ�",0,0,"L");
				$pdf->Ln();
				$pdf->Cell(300,15,"        - �����¡���͹�Թ��ҹ�ѭ�ո�Ҥ�� �ѧ���",0,0,"L");
				$pdf->Ln();
				
				$objBB = new BookBankList();
				$objBB->setFilter(" company_id = 14 ");
				$objBB->setPageSize(0);
				$objBB->setSort("title ASC");
				$objBB->load();
				$v=0;
				forEach($objBB->getItemList() as $objItem) {
					$arrBB[$v] = "�.".$objItem->get_bank()." ".$objItem->get_branch()." ".$objItem->get_type()." ".$objItem->get_code();
					$v++;
				}
				
				if($arrBB[0] != ""){
				$pdf->Cell(10,10,"",0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[0],0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[1],0,0,"L");
				$pdf->Ln();
				$pdf->Cell(5,5,"",0,0,"L");
				$pdf->Ln();
				}
				if($arrBB[2] != ""){
				$pdf->Cell(10,10,"",0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[2],0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[3],0,0,"L");
				$pdf->Ln();
				$pdf->Cell(5,5,"",0,0,"L");
				$pdf->Ln();
				}
				if($arrBB[4] != ""){
				$pdf->Cell(10,10,"",0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[4],0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[5],0,0,"L");
				$pdf->Ln();
				$pdf->Cell(5,5,"",0,0,"L");
				$pdf->Ln();
				}
				if($arrBB[6] != ""){
				$pdf->Cell(10,10,"",0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[6],0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[7],0,0,"L");
				$pdf->Ln();
				$pdf->Cell(5,5,"",0,0,"L");
				$pdf->Ln();
				}
				if($arrBB[8] != ""){
				$pdf->Cell(10,10,"",0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[8],0,0,"L");
				$pdf->Cell(10,10,"",1,0,"L");
				$pdf->Cell(250,10," ".$arrBB[9],0,0,"L");
				$pdf->Ln();
				$pdf->Cell(5,5,"",0,0,"L");
				$pdf->Ln();
				}
				

				$pdf->Ln();
				$pdf->Cell(500,20,"*** ���ͺѭ�� ����ѷ �������� �Թ����ѹ�� �á���� �ӡѴ ***",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(500,20,"*** �óժ����Թ���� ��س� FAX � PAY IN �Ҷ֧���˹�ҷ����Դ��ͷ�ҹ ��� FAX ".$objLabel->getText05()."  ���ͨ�����Թ����͡������Ѻ�Թ���� ***",0,0,"C");
				
				
				
	Unset($objCustomerList);
	$pdf->Output();	
	?>
	<?include "unset_all.php";?>
	<script>
	//window.close();
	</script>	
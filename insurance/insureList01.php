<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);


$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.verify_code LIKE '$Year02-".$Month02."' ";
		}else{
			$pstrCondition .=" P.verify_code LIKE '$Year02-".$Month02."'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND P.verify_code LIKE '%-".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.verify_code LIKE '%-".$Month02."-%'   ";	
		}
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_broker_id = '".$hInsureBrokerId."' ) ";
		}
}

if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_company_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_company_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( PRB.title LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  PRB.title LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_broker_kom_id  = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_broker_kom_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.stock_kom_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.stock_kom_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.kom_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.kom_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

if($hStatus != "0"){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}




}//end hSearch



if($pstrCondition == ""){
	$pstrCondition.= " P.insure_old=0 ";
}else{
	$pstrCondition.= " AND P.insure_old=0 ";
}

echo $pstrCondition;

$objInsureList = new InsureList();
$objInsureList->setFilter($pstrCondition);
$objInsureList->setPageSize(PAGE_SIZE);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" date_protect DESC ");
$objInsureList->setSort($hSort);
$objInsureList->loadList();

$pCurrentUrl = "insureList.php?hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType";
	// for paging only (sort resets page viewing)


$pageTitle = "1. �к��������١���";
$strHead03 = "���Ң������١���";
$pageContent = "1.5 ���Ң����� ICARD";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">	�ѹ��������ͧ�繪�ǧ : </td>
	<td>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
			<td class="error">&nbsp;*���Ҩҡ�ѹ��������ͧ������к�㹢�����</td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="right">	INSURE CODE : </td>
	<td>
			<table>
			<tr>
				<td>
					<select name="Month02">
					<option value="00" <?if($Month02 == "00") echo "selected"?>>- �ء��͹ -
					<option value="01" <?if($Month02 == "01") echo "selected"?>>���Ҥ�
					<option value="02" <?if($Month02 == "02") echo "selected"?>>����Ҿѹ��
					<option value="03" <?if($Month02 == "03") echo "selected"?>>�չҤ�
					<option value="04" <?if($Month02 == "04") echo "selected"?>>����¹
					<option value="05" <?if($Month02 == "05") echo "selected"?>>����Ҥ�
					<option value="06" <?if($Month02 == "06") echo "selected"?>>�Զع�¹		
					<option value="07" <?if($Month02 == "07") echo "selected"?>>�á�Ҥ�
					<option value="08" <?if($Month02 == "08") echo "selected"?>>�ԧ�Ҥ�
					<option value="09" <?if($Month02 == "09") echo "selected"?>>�ѹ¹¹
					<option value="10" <?if($Month02 == "10") echo "selected"?>>���Ҥ�
					<option value="11" <?if($Month02 == "11") echo "selected"?>>��Ȩԡ�¹
					<option value="12" <?if($Month02 == "12") echo "selected"?>>�ѹ�Ҥ�																					
				</select>				
				</td>
				<td>-</td>
				<td>
				<select name="Year02">
					<option value="0">- �ء�� -
					<?for($i=2007;$i<=date("Y")+3;$i++){?>
					<option value="<?=$i?>"><?=$i?>
					<?}?>				
				</select>				
				</td>
				<td class="error">&nbsp;* ���Ҩҡ�鴵͹���ҧ ICARD ��ҧ�ԧ�����͹�շ�����ҧ ICARD</td>
			</tr>
			</table>
	</td>
</tr>		
<tr>
	<td align="right">������ö :</td>
	<td>
			<table>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType","","����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td><strong>���ö</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel","","����к�")?>
			</td>
			</table>	
	</td>
</tr>
<tr>
	<td align="right"> ����ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �����١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	
<tr>
	<td align="right"> ���Ҿú :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId","","����к�");?></td>
			<td>�Ţ�ú</td>
			<td><input type="text" name="hKeywordPrb" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> ���ҡ�������� :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId01","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId01","","����к�");?></td>
			<td>�Ţ���������</td>
			<td><input type="text" name="hKeywordKom" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> ���Ҿ�ѡ�ҹ :</td>
	<td class="small">
     	<?=$objMemberList->printSelect("hSaleId",$hSaleId,"������");?>
	</td>
</tr>	
<tr>
	<td align="right"> ����ʶҹ� :</td>
	<td class="small">
		<select name="hStatus">
			<option value=0>- �ءʶҹ� -
			<option value="new">1. �Ѵ�������
			<option value="call 1">2. �ä��駷�� 1 �ʹͺ�ԡ��
			<option value="quotation">3. �͡�͡�����ʹ��Ҥ�
			<option value="call 2">4. �ä��駷�� 2 �Դ��â��
			<option value="set up">5. ���͡��úѹ�֡�â�¡�˹�����
			<option value="tracking">6. �ä��駷�� 3 �Ѵ���¡�ê���
			<option value="complete">7. ���ͺ���������
			<option value="cancel">8. ¡��ԡ���������
		</select>
	</td>
</tr>	
<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureList->getCount();?> ��¡��</div>


<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="5%" align="center" class="ListTitle">INSURE CODE</td>
	<td width="5%" align="center" class="ListTitle">�ѹ������ͧ</td>
	<td width="10%" align="center" class="ListTitle">�ú</td>
	<td width="10%" align="center" class="ListTitle">���������</td>
	<td width="10%" align="center" class="ListTitle">����</td>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="5%" align="center" class="ListTitle">�Ţ����¹</td>
	<td width="5%" align="center" class="ListTitle">SALE</td>	
	<td width="10%" align="center" class="ListTitle">ʶҹ�</td>	
	<td width="5%" align="center" class="ListTitle">��������´</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem) {
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objInsureCar->get_customer_id());
		$objCustomer->load();
		
		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objItem->get_insure_company_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objItem->get_stock_kom_id());
		$objKomCompany->load();
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" align="center" ><?=$objItem->get_verify_code()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_date_protect())?></td>	
	<td valign="top" ><?=$objPrbCompany->getTitle()?></td>		
	<td valign="top" ><?=$objKomCompany->getTitle()?></td>		
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  ><?=$objCustomer->getMobile()?></td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>
	<td valign="top"  align="center" ><?=$objItem->get_status_detail()?></td>
	<td  valign="top" align="center">
	<a target="_blank" href="insureView.php?hId=<?=$objItem->get_insure_id()?>">��������´</a>
	</td>	
</tr>
	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>
<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
</script>
<?
	include("h_footer.php")
?>

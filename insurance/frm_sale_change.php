<?php
header('Content-Type: text/html; charset=UTF-8');
include("../conf_inside.php");
$dataResult = '';
if (!empty($_FILES)) {
    $server_name = $_SERVER['REQUEST_URI'];
    $host       = DB_HOST;
    $user       = DB_USER;
    $password   = DB_PASS;
    $dbname     = DB_NAME;
    $conn = mysqli_connect($host, $user, $password, $dbname);
    mysqli_query("SET NAMES tis620");
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $file = $_FILES['file']['tmp_name'];
    if (!empty($file)) {
        try {

            $conn->autocommit(FALSE);
            $handle = fopen($file, "r");
            $row = 0;
            $arrReplace = array('/', '-', ',');
            while (($file_csv = fgetcsv($handle, 1000, ",")) !== false) {
                // ROW
                if ($row > 0) {
                    $vinNo          = empty($file_csv[0]) ? null : $file_csv[0];
                    $carFnCode      = empty($file_csv[1]) ? null : $file_csv[1];
                    $code           = empty($file_csv[2]) ? null : $file_csv[2];
                    // echo mb_detect_encoding($str);exit();
                     
                    $carID          = empty($file_csv[4]) ? null : $file_csv[4];
                    $car_number     = empty($file_csv[5]) ? null : $file_csv[5];
                    $oldSaleId      = empty($file_csv[6]) ? 0 : $file_csv[6];
                    $newSaleID      = empty($file_csv[7]) ? null : $file_csv[7];
                    $leadSpecial    = empty($file_csv[8]) ? null : $file_csv[8];

                    $dateVerify     = empty($file_csv[3]) ? 0 : str_replace($arrReplace, '', $file_csv[3]);
                    $dateVerifyImport = "'0000-00-00'";
                    if (!empty($file_csv[3])) {
                        if ($dateVerify != 0) {
                            $dateVerifyImport = "'".date('Y-m-d', strtotime($file_csv[3]))."'";
                        }
                    }

                    $sqlCheckCar = 'SELECT * FROM t_insure_car WHERE car_id = ' . $carID . ' AND sale_id = ' . $oldSaleId;
                    // echo '<br> -->'.$sqlCheckCar;
                    $resultCar = $conn->query($sqlCheckCar);
                    if ($resultCar->num_rows > 0) {
                        while ($dataCar = $resultCar->fetch_assoc()) {
                            
                            $oldDateVerify =  "'".$dataCar['date_verify']."'";
                            $NewDateVerify = "'0000-00-00'";
                            if (intval($dateVerify) != 0) {
                                $NewDateVerify = "'".date('Y-m-d', strtotime($file_csv[3]))."'";
                            } else {
                                if (!empty($dataCar['date_verify']) || $dataCar['date_verify'] != '0000-00-00') {
                                    $day            = date('d',strtotime($dataCar['date_verify']));
                                    $month          = date('m',strtotime($dataCar['date_verify']));
                                    $year           = date('Y');
                                    $NewDateVerify =  "'".date('Y-m-d', strtotime($year . '-' . $month . '-' . $day))."'";
                                }
                            }   

                            $sqlInsertLog = "INSERT INTO t_insure_car_sale_log (car_id, sale_id, date_verify, created_at)
                            VALUES (" . $dataCar['car_id'] . ", " . $dataCar['sale_id'] . ",".$oldDateVerify.", NOW())";
                            $resultLog = $conn->query($sqlInsertLog);
                            // echo '<br> -->'.$sqlInsertLog;
                            if (!$resultLog) {
                                $conn->rollback();
                                $dataResult = '<center><h2 style="font-size: 15px; color:red;"> Log เกิดข้อผิดพลาด ' . $resultLog->error . '</h2></center>';
                                echo '<pre>';echo $dataResult;echo '</pre>';
                                echo '<pre>';echo $sqlInsertLog;echo '</pre>';
                                echo '<pre>';var_dump($resultLog);echo '</pre>';
                                echo '<pre>';print_r(mysqli_error($conn));echo '</pre>';
                                $conn->rollback();
                                exit;
                            }

                             //CHECK TEAM ID
                             $sqlCheckTeam = 'SELECT team FROM t_member WHERE member_id ='.$newSaleID;
                             $resultTeam = $conn->query($sqlCheckTeam);
                             if ($resultTeam->num_rows > 0) {
                                 while ($dataTeam = $resultTeam->fetch_assoc()) {
                                     $teamID = $dataTeam['team'];
                                 }
                             } else {
                                 $teamID = $dataCar['team_id'];
                             }

                            $sqlUpdateSaleCar = "UPDATE t_insure_car SET sale_id= '" . $newSaleID . "', team_id = '" .$teamID. "', date_verify = ". $NewDateVerify .", operate='system' , lead_special = '" . $leadSpecial . "' WHERE car_id=" . $dataCar['car_id'];
                            // echo '<br> -->'.$sqlUpdateSaleCar;
                            $resultUpdateSale = $conn->query($sqlUpdateSaleCar);
                            
                            // $resultUpdateSale = true;
                            // if  (intval($dataCar['insure_id']) > 0) {
                            //     $sqlUpdateSaleInsure = "UPDATE t_insure SET sale_id= '" . $newSaleID . "', team_id = '" .$teamID. "' WHERE car_id=" . $dataCar['car_id'].' AND insure_id = '.$dataCar['insure_id'];
                            //     // echo '<br> -->'.$sqlUpdateSaleInsure;
                            //     $resultUpdateSaleInsure = $conn->query($sqlUpdateSaleInsure);
                            // }
                            
                            if (!$resultUpdateSale) {
                                $conn->rollback();
                                $dataResult = '<center><h2 style="font-size: 15px; color:red;"> UpdateSale เกิดข้อผิดพลาด ' . $resultUpdateSale->error . '</h2></center>';
                                echo '<pre>';echo $dataResult;echo '</pre>';
                                echo '<pre> sqlUpdateSaleCar ';echo $sqlUpdateSaleCar;echo '</pre>';
                                // echo '<pre> sqlUpdateSaleInsure ';echo $sqlUpdateSaleInsure;echo '</pre>';
                                echo '<pre>';var_dump($resultUpdateSale);echo '</pre>';
                                echo '<pre>';print_r(mysqli_error($conn));echo '</pre>';
                                $conn->rollback();
                                exit;
                            }
                        }
                        //SAVE FILE IMPORT
                        $sqlInsertFileImportY = 'INSERT INTO t_insure_car_file_import (vin_no,car_fn_code,code_number,date_verify,car_id,car_number,old_sale_id,new_sale_id,file_import_name, import_at,import_status,lead_special)
                        VALUES ("' . $vinNo . '","' . $carFnCode . '","' . $code . '",'.$dateVerifyImport.',"' . $carID . '","' . $car_number . '","' . $oldSaleId . '","' . $newSaleID . '","' . $_FILES['file']['name'] . '",NOW(),"Y","' . $leadSpecial . '");';
                        $resultImportY = $conn->query($sqlInsertFileImportY);
                        if (!$resultImportY) {
                            $dataResult = '<center><h2 style="font-size: 15px; color:red;"> Import เกิดข้อผิดพลาด ' . $resultImportY->error . '</h2></center>';
                            echo '<pre>';echo $dataResult;echo '</pre>';
                            echo '<pre>';echo $sqlInsertFileImportY;echo '</pre>';
                            echo '<pre>';var_dump($resultImportY);echo '</pre>';
                            echo '<pre>';print_r(mysqli_error($conn));echo '</pre>';
                            $conn->rollback();
                            exit;
                        }
                    } else {
                        //SAVE FILE IMPORT
                        $sqlInsertFileImportN = 'INSERT INTO t_insure_car_file_import (vin_no,car_fn_code,code_number,date_verify,car_id,car_number,old_sale_id,new_sale_id,file_import_name, import_at,import_status,lead_special)
                        VALUES ("' . $vinNo . '","' . $carFnCode . '","' . $code . '",'.$dateVerifyImport.',"' . $carID . '","' . $car_number . '","' . $oldSaleId . '","' . $newSaleID . '","' . $_FILES['file']['name'] . '",NOW(),"N","' . $leadSpecial . '");';
                        $resultImportN = $conn->query($sqlInsertFileImportN);
                        if (!$resultImportN) {
                            $dataResult = '<center><h2 style="font-size: 15px; color:red;"> Import เกิดข้อผิดพลาด ' . $resultImportN->error . '</h2></center>';
                            echo '<pre>';echo $dataResult;echo '</pre>';
                            echo '<pre>';echo $sqlInsertFileImportN;echo '</pre>';
                            echo '<pre>';var_dump($resultImportN);echo '</pre>';
                            echo '<pre>';print_r(mysqli_error($conn));echo '</pre>';
                            $conn->rollback();
                            exit;
                        }
                    }
                }
                $row = $row + 1;
            }
            // exit();
            $_POST = array();
            $conn->commit();
            $conn->autocommit(TRUE);
            $conn->close();
            $dataResult = '<center><h2 style="color:green;"> นำเข้าเรียบร้อย จำนวนทั้งหมด '.(intval($row)-1).' รายการ</h2></center>';
            session_start();
            $_SESSION["result"] = $dataResult;
            header("refresh: 2; url='".$server_name."'");
        } catch (Exception $ex) {
            $conn->rollback();
            $conn->autocommit(TRUE);
            $dataResult = '<center><h2 style="color:red;"> เกิดข้อผิดพลาด ' . $ex . '</h2></center>';
        }
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>เปลี่ยนแปลงเจ้าหน้าที่ขาย</title>
</head>

<body>
    <div class="container">

        <div class="panel-group" style="margin-top: 20px;">
            <div class="panel panel-primary">
                <div class="panel-heading">เปลี่ยนแปลงเจ้าหน้าที่ขาย</div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" method="post" role="form" id="formimport">
                        <div class="form-group">
                            <input type="file" name="file" id="file" size="150" class="form-control">
                            <p class="help-block">ทำการเลือกไฟล์ข้อมูลที่มีนามสกุล .csv เท่านั้น.</p>
                        </div>
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <a href="/insurance/main.php" class="btn btn-default">ย้อนกลับ</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-primary" onClick="getImport()" value="submit">นำเข้าข้อมูล</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/>
            <div class="panel panel-info">
                <div class="panel-heading"> ตัวอย่างไฟล์ Excel นำเข้าข้อมูลเปลี่ยนแปลงเจ้าหน้าที่ขาย</div>
                <div class="panel-body" style="padding: 0 !important;">
                    <div class="container-fluid" style="padding-right: 0px !important;  padding-left: 0px !important;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="info">
                                        <th>A</th>
                                        <th>B</th>
                                        <th>C</th>
                                        <th>D</th>
                                        <th>E</th>
                                        <th>F</th>
                                        <th>G</th>
                                        <th>H</th>
                                        <th>I</th>
                                    </tr>
                                    <tr class="info">
                                        <th>VIN NO</th>
                                        <th>CAR FN CODE</th>
                                        <th>CODE</th>
                                        <th>DDATE VERIFY </th>
                                        <th>CAR ID</th>
                                        <th>CAR NUMBER</th>
                                        <th>OLD SALE ID</th>
                                        <th>NEW SALE ID</th>
                                        <th>LEAD SPECIAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MR053HY9305340327</td>
                                        <td>TLT</td>
                                        <td>ฆฮ-4337</td>
                                        <td><?php echo date('Y-m-d'); ?></td>
                                        <td>284861</td>
                                        <td>MR053HY9305340327</td>
                                        <td>0</td>
                                        <td>2825</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>MR053HY9305367385</td>
                                        <td>SCB</td>
                                        <td>1กฐ-5092</td>
                                        <td><?php echo date('Y-m-d'); ?></td>
                                        <td>333452</td>
                                        <td>MR053HY9305367385</td>
                                        <td>0</td>
                                        <td>1256</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>MR053HY9305382123</td>
                                        <td>TNB</td>
                                        <td>1กฐ-6430</td>
                                        <td><?php echo date('Y-m-d'); ?></td>
                                        <td>350430</td>
                                        <td>MR053HY9305382123</td>
                                        <td>0</td>
                                        <td>831</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <div style="color:red; font-weight: bold;">
                                *หมายเหตุ กรุณาตรวจสอบไฟล์ Excel ให้ตรงกับรูปแบบที่กำหนดเท่านั้น<br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            if (!empty($_SESSION)) {
                echo $_SESSION["result"];
            }
            ?>
        </div>
        <?php if (!empty($_SESSION)) {
            unset($_SESSION["result"]);
        } ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            function getImport() {
                var fileCsv = $('#file').val();
                var validCsv = new Array(".csv");
                fileCsv = fileCsv.substring(fileCsv.lastIndexOf('.'));
                if (validCsv.indexOf(fileCsv) < 0) {
                    alert('ไฟล์ที่นำเข้าต้องเป็นไฟล์ CSV เท่านั้น');
                    return false;
                }
                if ($('#file').val() == '') {
                    alert(' กรุณาเลือกไฟล์ที่ต้องการนำเข้า');
                    return false;
                }

                if ($('#file').val() != '') {

                    $('#formimport').submit();
                }

            }
        </script>
</body>

</html>
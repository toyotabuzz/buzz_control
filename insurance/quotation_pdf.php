<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

$objIQ = new InsureQuotation();
$objQL01 = new InsureQuotationItem();
$objQL02 = new InsureQuotationItem();
$objQL03 = new InsureQuotationItem();


if ($hId !="") {
	$objIQ->set_quotation_id($hId);
	$objIQ->load();
	$strMode="Update";
	
	$hCarId = $objIQ->get_car_id();
	
	$arrDate = explode("-",$objIQ->get_date_add());
	$Day02 = $arrDate[2];
	$Month02 = $arrDate[1];
	$Year02 = $arrDate[0];
	
	$objQIList = new InsureQuotationItemList();
	$objQIList->setFilter(" quotation_id = $hId ");
	$objQIList->setPageSize(0);
	$objQIList->setSort(" quotation_item_id ASC ");
	$objQIList->load();

	$i=0;
	forEach($objQIList->getItemList() as $objItem) {

		if($i==0){
			$objQL01->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL01->load();
		}
		
		if($i==1){
			$objQL02->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL02->load();
		}

		if($i==2){
			$objQL03->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL03->load();
		}
		$i++;
	}
}

if($hCarId != ""){

		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($hCarId);
		$objInsureCar->load();
				
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objInsureCar->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
		$objCarSeries->load();

		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objInsureCar->get_color());
		$objCarColor->load();
		
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
		
}

$objInsureList = New InsureList;

$objLabel = new Label();
$strText01= "��˹������š�þ������ʹ��Ҥ�";	
$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
define("IMG_INSURE","../images/misc/".$objLabel->getText02());

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	$this->Image('../images/insure_quotation_pic01.gif',20,10,180);
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
	$this->Image(IMG_INSURE,20,780,400);
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(10,10,10);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

$pdf->AddPage();
$pdf->SetLeftMargin(20);

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(450,50);
$pdf->Write(10,"�Ţ��� : ".$objIQ->get_quotation_number());

$pdf->SetFont('angsa','B',20);	
$pdf->SetXY(280,60);
$pdf->Write(10,"��ʹ��Ҥ�");

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,90);
$pdf->Write(10,"ATTN :".$objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname());

$sText = "�� ";
if($objCustomer->getHomeTel() != ""){ $sText = $sText." ".$objCustomer->getHomeTel(); }
if($objCustomer->getMobile() != ""){if($objCustomer->getHomeTel() != ""){$sText = $sText.", ".$objCustomer->getMobile(); }else{$sText = $sText.$objCustomer->getMobile(); } }
if($objCustomer->getFax() != ""){ $sText = $sText." ῡ�� ".$objCustomer->getFax(); }

$pdf->SetXY(200,90);
$pdf->Write(10,$sText);

$pdf->SetXY(450,90);
$pdf->Write(10,"�ѹ��� ".formatThaiDate(date("Y-m-d")) );

$pdf->SetXY(20,110);
$pdf->Write(10,"FROM :".$objMember->getFirstname()." (".$objMember->getNickname().")");

$sText = "�� ".$objMember->getTel()." ῡ�� ".$objLabel->getText01();


$pdf->SetXY(200,110);
$pdf->Write(10,$sText);

$pdf->SetXY(20,130);
$pdf->Write(10,"�͹حҵ���˹�һ�Сѹ�Թ����� �Ţ��� : ".$objMember->getInsureLicense());
if($objMember->getEmail() != ""){ 

$sText = " ������� ".$objMember->getEmail(); 
$pdf->SetXY(300,130);
$pdf->Write(10,$sText);

}



$pdf->line(20,150,580,150);

$sText= '����¹ '.$objInsureCar->get_code();
$pdf->SetXY(20,160);
$pdf->Write(10,$sText);

$sText= '������ '.$objCarType->getTitle().' ��� '.$objCarModel->getTitle();
$pdf->SetXY(130,160);
$pdf->Write(10,$sText);

$sText= '��Ҵ '.$objCarSeries->getCC()." CC";
$pdf->SetXY(380,160);
$pdf->Write(10,$sText);

$pdf->SetXY(480,160);
$pdf->Write(10,'�� '.$objInsureCar->get_register_year());

$pdf->line(20,175,580,175);

$pdf->SetXY(380,180);
$pdf->Write(10,'��Сѹ����ѹ��� '.formatThaiDate($objInsureCar->get_date_verify()));


$pdf->SetXY(20,200);
$pdf->Cell(315,20,'��������´����������ͧ',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text01(),1,0,'C');
$pdf->Cell(80,20,$objQL02->get_text01(),1,0,'C');
$pdf->Cell(80,20,$objQL03->get_text01(),1,0,'C');
$pdf->Ln();
$pdf->Cell(315,20,'�����Ѻ�Դ�ͺ��ͺؤ����¹͡',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text02(),1,0,'C');
$pdf->Cell(80,20,$objQL02->get_text02(),1,0,'C');
$pdf->Cell(80,20,$objQL03->get_text02(),1,0,'C');
$pdf->Ln();
$pdf->Cell(315,20,'���ó� : ����������µ�ͪ��Ե��ҧ��� ���͹������ǹ�Թ �ú. / �� / ����',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text03(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text03(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text03(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'�����ͤ��� / ����Թ',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text04(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text04(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text04(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'����������µ�ͷ�Ѿ���Թ���ó�',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text05(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text05(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text05(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'����������ͧö¹������һ�Сѹ���',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text06(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text06(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text06(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'ö�����һ�Сѹ : ����������µ��ö�����һ�Сѹ',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text07(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text07(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text07(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'ö¹���٭��� / �����',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text08(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text08(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text08(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'����������ͧ����͡���Ṻ����',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text09(),1,0,'C');
$pdf->Cell(80,20,$objQL02->get_text09(),1,0,'C');
$pdf->Cell(80,20,$objQL03->get_text09(),1,0,'C');
$pdf->Ln();
$pdf->Cell(315,20,'�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text10(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text10(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text10(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text11(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text11(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text11(),1,0,'R');
$pdf->Ln();
$pdf->Cell(315,20,'��Сѹ��Ǽ��Ѻ���',1,0,'C');
$pdf->Cell(80,20,$objQL01->get_text12(),1,0,'R');
$pdf->Cell(80,20,$objQL02->get_text12(),1,0,'R');
$pdf->Cell(80,20,$objQL03->get_text12(),1,0,'R');
$pdf->Ln();

if($objIQ->get_text06() != "" ){
$shift="C";
if( ($objQL01->get_text19()*1) > 0) $shift="R";

$pdf->Cell(315,20,$objIQ->get_text06(),1,0,'C');

if($objQL01->get_text19() != "") { $num01=(($objQL01->get_text19())); }else{ $num01="";}
if($objQL02->get_text19() != "") { $num02=(($objQL02->get_text19())); }else{ $num02="";}
if($objQL03->get_text19() != "") { $num03=(($objQL03->get_text19())); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}

if($objIQ->get_text07() != "" ){
$shift="C";
if( ($objQL01->get_text20()*1) > 0) $shift="R";

$pdf->Cell(315,20,$objIQ->get_text07(),1,0,'C');

if($objQL01->get_text20() != "") { $num01=(($objQL01->get_text20())); }else{ $num01="";}
if($objQL02->get_text20() != "") { $num02=(($objQL02->get_text20())); }else{ $num02="";}
if($objQL03->get_text20() != "") { $num03=(($objQL03->get_text20())); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}

if($objIQ->get_text08() != "" ){
$shift="C";
if( ($objQL01->get_text21()*1) > 0) $shift="R";
$pdf->Cell(315,20,$objIQ->get_text08(),1,0,'C');

if($objQL01->get_text21() != "") { $num01=(($objQL01->get_text21())); }else{ $num01="";}
if($objQL02->get_text21() != "") { $num02=(($objQL02->get_text21())); }else{ $num02="";}
if($objQL03->get_text21() != "") { $num03=(($objQL03->get_text21())); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}

if($objIQ->get_text09() != "" ){
$shift="C";
if( ($objQL01->get_text22()*1) > 0) $shift="R";
$pdf->Cell(315,20,$objIQ->get_text09(),1,0,'C');

if($objQL01->get_text22() != "") { $num01=(($objQL01->get_text22())); }else{ $num01="";}
if($objQL02->get_text22() != "") { $num02=(($objQL02->get_text22())); }else{ $num02="";}
if($objQL03->get_text22() != "") { $num03=(($objQL03->get_text22())); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}


if($objIQ->get_text01() != ""  or $objQL01->get_text14() !="" or $objQL02->get_text14()   !=""or $objQL03->get_text14()  !=""){
$shift="C";
if( ($objQL01->get_text14()*1) > 0) $shift="R";
$pdf->setX(100);
$pdf->write(20,$objIQ->get_text01());
$pdf->SetLeftMargin(335);

if($objQL01->get_text14() != "") { $num01=(($objQL01->get_text14())); }else{ $num01="";}
if($objQL02->get_text14() != "") { $num02=(($objQL02->get_text14())); }else{ $num02="";}
if($objQL03->get_text14() != "") { $num03=(($objQL03->get_text14())); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}
if($objIQ->get_text02() != "" or $objQL01->get_text15()  !="" or $objQL02->get_text15()  !="" or $objQL03->get_text15()  !=""){
$shift="C";
if( ($objQL01->get_text15()*1) > 0) $shift="R";
$pdf->setX(100);
$pdf->write(20,$objIQ->get_text02());
$pdf->SetLeftMargin(335);

if($objQL01->get_text15() != "") { $num01=($objQL01->get_text15()); }else{ $num01="";}
if($objQL02->get_text15() != "") { $num02=($objQL02->get_text15()); }else{ $num02="";}
if($objQL03->get_text15() != "") { $num03=($objQL03->get_text15()); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);

$pdf->Ln();
$pdf->SetLeftMargin(20);
}
if($objIQ->get_text03() != "" or $objQL01->get_text16()   !="" or $objQL02->get_text16()  !=""  or $objQL03->get_text16()  !=""){
$shift="C";
if( ($objQL01->get_text16()*1) > 0) $shift="R";
$pdf->setX(100);
$pdf->write(20,$objIQ->get_text03());
$pdf->SetLeftMargin(335);

if($objQL01->get_text16() != "") { $num01=($objQL01->get_text16()); }else{ $num01="";}
if($objQL02->get_text16() != "") { $num02=($objQL02->get_text16()); }else{ $num02="";}
if($objQL03->get_text16() != "") { $num03=($objQL03->get_text16()); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}
if($objIQ->get_text04() != "" or $objQL01->get_text17()  !="" or $objQL02->get_text17()   !="" or $objQL03->get_text17()  !=""){
$shift="C";
if( ($objQL01->get_text17()*1) > 0) $shift="R";
$pdf->setX(100);
$pdf->write(20,$objIQ->get_text04());
$pdf->SetLeftMargin(335);

if($objQL01->get_text17() != "") { $num01=($objQL01->get_text17()); }else{ $num01="";}
if($objQL02->get_text17() != "") { $num02=($objQL02->get_text17()); }else{ $num02="";}
if($objQL03->get_text17() != "") { $num03=($objQL03->get_text17()); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}
if($objIQ->get_text05() != "" or $objQL01->get_text18()  !=""  or $objQL02->get_text18()   !="" or $objQL03->get_text18() !=""){
$shift="C";
if( ($objQL01->get_text18()*1) > 0) $shift="R";
$pdf->setX(100);
$pdf->write(20,$objIQ->get_text05());
$pdf->SetLeftMargin(335);

if($objQL01->get_text18() != "") { $num01=($objQL01->get_text18()); }else{ $num01="";}
if($objQL02->get_text18() != "") { $num02=($objQL02->get_text18()); }else{ $num02="";}
if($objQL03->get_text18() != "") { $num03=($objQL03->get_text18()); }else{ $num03="";}

$pdf->Cell(80,20,$num01,1,0,$shift);
$pdf->Cell(80,20,$num02,1,0,$shift);
$pdf->Cell(80,20,$num03,1,0,$shift);
$pdf->Ln();
$pdf->SetLeftMargin(20);
}
$pdf->Ln();
$pdf->setX(50);
$pdf->write(20,"�֧���¹�����;Ԩ�ó� �����ѧ��Ҥ����Ѻ���ҹ������ѹ���");
$pdf->setX(410);
$pdf->write(20,"���ʴ������Ѻ���");
$pdf->Ln();

$pdf->setX(70);
$pdf->write(20,"�͡��÷����㹡�÷ӻ�Сѹ���ö¹��");
$pdf->Ln();

$pdf->setX(70);
$pdf->write(20,"1) ���ҧ�����������  �������͹������� ");
$pdf->Ln();

$pdf->setX(70);
$pdf->write(20,"2) ������¡�è�����¹ö¹��");
$pdf->setX(410);
$pdf->write(20,"(".$objMember->getFirstname()." ".$objMember->getLastname().")");
$pdf->Ln();

$pdf->setX(70);
$pdf->write(20,"3) ����㺢Ѻ���  2 ��ҹ  (�ó� : �кؼ��Ѻ���)");
$pdf->setX(410);
$pdf->write(20,"��ѡ�ҹ��Сѹ���ö¹��");
$pdf->Ln();
$pdf->setX(50);
$pdf->write(20,"���������Сѹ��¹���ռ���ѧ�Ѻ�ѹ�� ����ͼ����һ�Сѹ��ª������»�Сѹ�������");
$pdf->Ln();
if($objIQ->get_remark() != ""){
$pdf->Ln();
$pdf->setX(50);
$pdf->write(20,"** �����˵� **");
$pdf->Ln();
$pdf->setX(50);
$pdf->write(20,$objIQ->get_remark());
$pdf->Ln();
}


$pdf->Output();	
?>
<script>window.close();</script>	
<?include "unset_all.php";?>
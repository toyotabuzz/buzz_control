<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;


if($hSubmitTrack){
	$objIT = new InsureItemTrack();
	$objIT->set_insure_id($hInsureId);
	$objIT->set_insure_item_id($hInsureItemId);
	$objIT->set_track_date(date("Y-m-d"));
	$objIT->set_track_new_date($hYear."-".$hMonth."-".$hDay);
	$objIT->set_track_status($hTrackStatus);
	$objIT->set_track_remark($hTrackRemark);
	$objIT->add();
}

if($hTrackDelete == "yes"){
	$objIT = new InsureItemTrack();
	$objIT->set_insure_item_track_id($hTrackId);
	$objIT->delete();
}


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND I.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" I.sale_id = '".$hSaleId."' ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND I.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" I.company_id = '".$hCompanyId."' ";	
	}
}

if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.verify_code LIKE '$Month02-$Year02' ";
		}else{
			$pstrCondition .=" P.verify_code LIKE '$Month02-$Year02'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND P.verify_code LIKE '".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.verify_code LIKE '".$Month02."-%'   ";	
		}
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.insure_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_broker_id = '".$hInsureBrokerId."' ) ";
		}
}

if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_prb_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_prb_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_prb_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_prb_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}



$sesCondition = $pstrCondition;
session_register("sesCondition",$sesCondition);

}//end hSearch


if($pstrCondition == ""){
	$pstrCondition= " acc_approve = 'No' and I.sale_id=$sMemberId ";
}else{
	$pstrCondition.=" AND acc_approve = 'No'  and I.sale_id=$sMemberId  ";
}

//echo $pstrCondition;

$objInsureItemList = new InsureItemList();
$objInsureItemList->setFilter($pstrCondition);
$objInsureItemList->setPageSize(PAGE_SIZE);
$objInsureItemList->setPage($hPage);
$objInsureItemList->setSortDefault(" payment_date ASC ");
$objInsureItemList->setSort($hSort);
$objInsureItemList->load();

$pCurrentUrl = "insure_sale_list.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "2. �к��ҹ��»�Сѹ���";
$strHead03 = "���Ң������١���";
$pageContent = " ��¡�õԴ�����ê����Թ";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>

<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	

<tr>
	<td align="right"> ��ѡ�ҹ��Ңͧ��¡�� :</td>
	<td class="small">
     	<?=$objMemberList->printSelect("hSaleId",$hSaleId,"������");?>
	</td>
</tr>	

<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureItemList->getCount();?> ��¡��</div>



<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureItemList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureItemList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureItemList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">

	<?
		$i=0;
		forEach($objInsureItemList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsure->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();

		if($hPage==0)$hPage=1;
	?>
<tr>
	<td width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td width="10%" align="center" class="ListTitle">�ѹ���Ѵ����</td>
	<td width="20%" align="center" class="ListTitle">����-���ʡ��</td>
	<td width=10%" align="center" class="ListTitle">�Ţ����¹</td>
	<td width="10%" align="center" class="ListTitle">�Ǵ���</td>
	<td width="10%" align="center" class="ListTitle">�ӹǹ�Թ</td>
	<td width="5%" align="center" class="ListTitle">��������´</td>
</tr>
<?
	$bgColor = "";
	$diff = diff_date($objItem->get_payment_date(),date("Y-m-d")) ;
	
	if( $diff>= - 15){	$bgColor = "yellow";}
	if( $diff>= -7){	$bgColor = "red";}
	
?>


<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" align="center" ><?=(($hPage-1)*50)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_payment_date())?></td>
	<td valign="top" ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>	
	<td valign="top" align="center" ><?=$objInsureCar->get_code()?></td>

	<td valign="top" align="center" ><?=$objItem->get_payment_order()?>/<?=$objInsure->get_pay_number()?></td>		
	<td valign="top" align="right" ><?=number_format($objItem->get_payment_price(),2)?></td>		
	<td  valign="top" align="center">
	<a target="_blank" href="step06List.php?hCarId=<?=$objInsureCar->get_car_id()?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$objItem->get_insure_id()?>#no<?=$objItem->get_payment_order()?>">��������´</a>
	</td>	
</tr>
<tr>
	<td bgcolor="<?=$bgColor?>">&nbsp;</td>
	<td class="listDetail01" valign="top" align="center"><strong>ʶҹС�õԴ���</strong></td>
	<td class="listDetail01"  valign="top" align="center"><strong>�ѹ����������</strong></td>
	<td class="listDetail01" valign="top" colspan="3" align="center"><strong>�����˵ء�õԴ���</strong></td>
	<td class="listDetail01" ></td>
</tr>
<form name="frmTrack_<?=$i?>" action="insure_pay_list.php" method="post" onsubmit="return check_submit(<?=$i?>);">
<input type="hidden" name="hInsureItemId" value="<?=$objItem->get_insure_item_id()?>">
<input type="hidden" name="hInsureId" value="<?=$objItem->get_insure_id()?>">
<tr>
	<td bgcolor="<?=$bgColor?>">&nbsp;</td>
	<td valign="top" class="listDetail02" align="center">
		<select name="hTrackStatus">
				<option value=""> - ��س����͡ -
				<option value="�����ѹ���">�����ѹ���
				<option value="����͹�ѹ����">����͹�ѹ����
				<option value="����">����
		<select>
	</td>
	<td valign="top" class="listDetail02" align="center">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>
		 </table>	
	</td>
	<td colspan="3" class="listDetail02" align="center">
		<input type="text" name="hTrackRemark" size="70">		
	</td>
	<td><input type="submit" name="hSubmitTrack" value="�ѹ�֡�Դ���" ></td>
</tr>
</form>


<?
$objIT = new InsureItemTrackList();
$objIT->setFilter(" insure_item_id = ".$objItem->get_insure_item_id()."   ");
$objIT->setPageSize(0);
$objIT->setSort(" track_date ASC  ");
$objIT->load();
if($objIT->mCount > 0){?>
<tr>
	<td bgcolor="<?=$bgColor?>">&nbsp;</td>
	<td  align="center" class="ListTitle">�ѹ���Դ���</td>
	<td  align="center" class="ListTitle">ʶҹС�õԴ���</td>
	<td  align="center" class="ListTitle">�ѹ�������͹�Ѵ</td>
	<td  colspan="2" align="center" class="ListTitle">�����˵�</td>
	<td  align="center" class="ListTitle">ź��¡��</td>
</tr>
<?
forEach($objIT->getItemList() as $objItemTrack) {
?>
<tr>
	<td bgcolor="<?=$bgColor?>">&nbsp;</td>
	<td valign="top" align="center"><?=formatShortDate($objItemTrack->get_track_date());?></td>
	<td valign="top" align="center"><?=$objItemTrack->get_track_status();?></td>
	<td valign="top" align="center"><?if($objItemTrack->get_track_status() == "����͹�ѹ����") echo formatShortDate($objItemTrack->get_track_new_date());?></td>
	<td valign="top" colspan="2"><?=$objItemTrack->get_track_remark();?></td>
	<td align="center" ><input type="button" value="ź��¡��" onclick="window.location='insure_pay_list.php?hTrackDelete=yes&hTrackId=<?=$objItemTrack->get_insure_item_track_id()?>'"></td>
</tr>
<?}?>
<?}?>
	<?
		++$i;
			}
	Unset($objInsureItemList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">

<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
	
	function check_submit(val){
		var hTrack;
		eval("hTrack=document.frmTrack_"+val+".hTrackStatus.value"); 
		if(hTrack==""){
			alert("��س����͡ʶҹС�õԴ���");
			eval("document.frmTrack_"+val+".hTrackStatus.focus()"); 
			return false;
		}
		
		eval("hDD=document.frmTrack_"+val+".Day.value"); 
		eval("hMM=document.frmTrack_"+val+".Month.value"); 
		eval("hYY=document.frmTrack_"+val+".Year.value"); 
		if(hTrack=="����͹�ѹ����" && (hDD.value=="" || hMM.value =="" || hYY== "")  ){
			alert("��س��к��ѹ����͹�Ѵ����");
			eval("document.frmTrack_"+val+".Day.focus()"); 
			return false;
		}
		
		
		return true;
	
	
	}
	
</script>
<?
	function diff_date($begin, $end){
    	$rbegin = is_string($begin) ? strtotime(strval($begin)) : $begin;
    	$rend = is_string($end) ? strtotime(strval($end)) : $end;
		
		$begin =  strtotime(strval($begin));
		$end = strtotime(strval($end));
		
    	$difftime = $end - $begin;
   		$diffdays = floor($difftime / (24 * 60 * 60)) + 1;
		return $diffdays;	
	}


	include("h_footer.php")
?>
<?include "unset_all.php";?>
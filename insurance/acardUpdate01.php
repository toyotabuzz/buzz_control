<?
include("common.php");

$objCustomer = new Customer();
$objEvent = new CustomerEvent();
$objOrderStockOtherList = new OrderStockOtherList();
$objOrderStockOtherPremiumList = new OrderStockOtherList();
$objStockProductList = new StockProductList();
$objCarSeries = new CarSeries();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objCustomerEventList = new CustomerEventList();
$objCustomerEventList->setPageSize(0);
$objCustomerEventList->setSortDefault("title ASC");
$objCustomerEventList->load();

$objSaleList = new MemberList();
$objSaleList->setPageSize(0);
$objSaleList->setSortDefault("department_id, firstname, lastname ASC");
$objSaleList->load();

$objCarTypeList = new CarTypeList();
$objCarTypeList->setPageSize(0);
$objCarTypeList->setSortDefault("title ASC");
$objCarTypeList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModel1List = new CarSeriesList();
$objCarModel1List->setPageSize(0);
$objCarModel1List->setSortDefault(" model ASC");
$objCarModel1List->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		

		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();

		if($hCustomerId != ""){	
			$objOrder->setBookingCustomerId($hCustomerId);			
		}
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		$strMode="Update";

		
		$objSale = new Member();
		$objSale->setMemberId($objCustomer->getSaleId());
		$objSale->load();
		
		$hCustomerSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		
		$arrDate = explode("-",$objOrder->getPrbExpire());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];		
		
		$objContactList = new ContactList();
		$objContactList->setPageSize(0);
		$objContactList->setSortDefault(" title ASC");
		$objContactList->load();				
		
		$objCustomerContactList = new CustomerContactList();
		$objCustomerContactList->setFilter("   customer_id = $hId ");
		$objCustomerContactList->setPageSize(0);
		$objCustomerContactList->setSortDefault(" contact_date DESC ");
		$objCustomerContactList->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();
		
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hCustomerId);
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard(1);
			$objCustomer->setCCard($hCCard);
            $objCustomer->setTypeId("B");
            $objCustomer->setGradeId($hGradeId);
            $objCustomer->setGroupId($hGroupId);
			$objCustomer->setEventId($hEventId);
			$hPostedDate = $Year."-".$Month."-".$Day;
            $objCustomer->setInformationDate($hPostedDate);
            $objCustomer->setSaleId($hCustomerSaleId);
			$hBirthday = $Year01."-".$Month01."-".$Day01;
            $objCustomer->setBirthday($hBirthday);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setEmail($hEmail);
			$objCustomer->setIDCard($hIDCard);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setHomeTel($hHomeTel);
			$objCustomer->setMobile($hMobile);
			$objCustomer->setOfficeTel($hOfficeTel);
			$objCustomer->setFax($hFax);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setAddress01($hAddress01);
			$objCustomer->setTumbon01($hTumbon01);
			$objCustomer->setAmphur01($hAmphur01);
			$objCustomer->setProvince01($hProvince01);
			$objCustomer->setTumbonCode01($hTumbonCode01);
			$objCustomer->setAmphurCode01($hAmphurCode01);
			$objCustomer->setProvinceCode01($hProvinceCode01);
			$objCustomer->setZipCode01($hZipCode01);
			$objCustomer->setZip01($hZip01);
			$objCustomer->setTel01($hTel01);
			$objCustomer->setFax01($hFax01);
			$objCustomer->setAddress02($hAddress02);
			$objCustomer->setTumbon02($hTumbon02);
			$objCustomer->setAmphur02($hAmphur02);
			$objCustomer->setProvince02($hProvince02);
			$objCustomer->setTumbonCode02($hTumbonCode02);
			$objCustomer->setAmphurCode02($hAmphurCode02);
			$objCustomer->setProvinceCode02($hProvinceCode02);
			$objCustomer->setZipCode02($hZipCode02);
			$objCustomer->setZip02($hZip02);
			$objCustomer->setTel02($hTel02);
			$objCustomer->setFax02($hFax02);
			$objCustomer->setAddress03($hAddress03);
			$objCustomer->setTumbon03($hTumbon03);
			$objCustomer->setAmphur03($hAmphur03);
			$objCustomer->setProvince03($hProvince03);
			$objCustomer->setTumbonCode03($hTumbonCode03);
			$objCustomer->setAmphurCode03($hAmphurCode03);
			$objCustomer->setProvinceCode03($hProvinceCode03);
			$objCustomer->setZipCode03($hZipCode03);
			$objCustomer->setZip03($hZip03);
			$objCustomer->setTel03($hTel03);
			$objCustomer->setFax03($hFax03);
			
			
            $objOrder->setOrderId($hOrderId);			
			
			$hBookingDate = $Year02."-".$Month02."-".$Day02;
			$objOrder->setBookingCustomerId($hBookingCustomerId);
			$objOrder->setBookingNumber($hBookingNumber);
			$objOrder->setBookingDate($hBookingDate);
            $objOrder->setBookingSaleId($hBookingSaleId);
			
			$objOrder->setOrderStatus(1);
            $objOrder->setOrderNumber($hOrderNumber);
            $objOrder->setOrderPrice($hOrderPrice);
			$objOrder->setOrderReservePrice($hOrderReservePrice);
            $objOrder->setOrderLocation($hOrderLocation);
			
			
            $objOrder->setBookingRemark($hBookingRemark);
			$objOrder->setBookingCarType($hBookingCarModel);
			$objOrder->setBookingCarSeries($hBookingCarSeries);
			$objOrder->setBookingCarGear($hBookingCarGear);
			$objOrder->setBookingCarColor($hBookingCarColor);
			$objOrder->setBookingCarRemark($hBookingCarRemark);
			
			
			$objOrder->setDiscountPrice($hDiscountPrice);
			$objOrder->setDiscountSubdown($hDiscountSubdown);
			$objOrder->setDiscountPremium($hOrderPremiumPriceTotal);
			$objOrder->setDiscountProduct($hOrderProductPriceTotal);
			$objOrder->setDiscountInsurance($hDiscountInsurance);
			$objOrder->setDiscountGoa($hDiscountGoa);
			$objOrder->setDiscount($hDiscount);
			$objOrder->setDiscountMargin($hDiscountMargin);
			$objOrder->setDiscountSubdownVat($hDiscountSubdownVat);
			$objOrder->setDiscountAll($hDiscountAll);
			$objOrder->setDiscountOver($hDiscountOver);

			$objOrder->setBuyType($hBuyType);
			$objOrder->setBuyCompany($hBuyCompany);
			$objOrder->setBuyFee($hBuyFee);
			$objOrder->setBuyTime($hBuyTime);
			$objOrder->setBuyPayment($hBuyPayment);
			$objOrder->setBuyPrice($hBuyPrice);
			$objOrder->setBuyDown($hBuyDown);
			$objOrder->setBuyTotal($hBuyTotal);
			$objOrder->setBuyRemark($hBuyRemark);
			$objOrder->setPrbCompany($hPrbCompany);
			$hPrbExpire = $Year05."-".$Month05."-".$Day05;
			
			$objOrder->setPrbExpire($hPrbExpire);
			$objOrder->setPrbPrice($hPrbPrice);
			$objOrder->setInsureCompany($hInsureCompany);
			$hInsureExpire = $Year06."-".$Month06."-".$Day06;
			$objOrder->setInsureExpire($hInsureExpire);
			$objOrder->setInsurePrice($hInsurePrice);
			$objOrder->setInsureType($hInsureType);
			$objOrder->setInsureYear($hInsureYear);
			$objOrder->setInsureBudget($hInsureBudget);
			$objOrder->setInsureFrom($hInsureFrom);
			$objOrder->setInsureFromDetail($hInsureFromDetail);
			$objOrder->setInsureRemark($hInsureRemark);			
			$objOrder->setInsureCustomerPay($hInsureCustomerPay);
			
			$objOrder->setTMBNumber($hTMBNumber);	
			$objOrder->setEditBy($sMemberId);
			
			$objCarSeries = new CarSeries();
			$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
			$objCarSeries->load();
			
			$objCarColor = new CarColor();
			$objCarColor->setCarColorId($objOrder->getBookingCarColor());
			$objCarColor->load();			
			
			
			if(isset($hOrderProductAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderProductId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderProductQty);
				$objOrderStock->setPrice($hOrderProductPrice);
				$objOrderStock->setTMT($hOrderProductTMT);
				if($hOrderProductOther){
					$objOrderStock->setRemark($hOrderProduct);
				}
				$objOrderStock->add();
				
			}
			
			if(isset($hOrderProductUpdate)){
				for($i=0;$i<$hCountOrderProduct;$i++){
					if($_POST["hOrderProductDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderProductId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(0);
						$objOrderStock->setQty($_POST["hOrderProductQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderProductPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderProductTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderProductRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}
			
			if(isset($hOrderPremiumAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderPremiumId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderPremiumQty);
				$objOrderStock->setPrice($hOrderPremiumPrice);
				$objOrderStock->setTMT($hOrderPremiumTMT);
				$objOrderStock->setStockType(1);
				if($hOrderPremiumOther){
					$objOrderStock->setRemark($hOrderPremium);
				}
				$objOrderStock->add();
			}
			
			if(isset($hOrderPremiumUpdate)){
				for($i=0;$i<$hCountOrderPremium;$i++){
					if($_POST["hOrderPremiumDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderPremiumId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(1);
						$objOrderStock->setQty($_POST["hOrderPremiumQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderPremiumPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderPremiumTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderPremiumRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}
			
	
			if(!$hOrderProductAdd AND !$hOrderPremiumAdd AND !$hOrderProductUpdate AND !$hOrderPremiumUpdate){
	
	
    		$pasrErrCustomer = $objCustomer->check($hSubmit);
			$pasrErrOrder = $objOrder->checkCrlBooking($hSubmit);

			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					$objCustomer->update();
					$objOrder->updateCrlBooking();
					
					header("location:bcardNewList.php");
					exit;
				}
				unset ($objCustomer);


			}else{
				$objCustomer->init();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objOrder->getBookingCarColor());
				$objCarColor->load();
				
			}//end check Count($pasrErr)
		}
		
	}//end check $hOrderProductAdd 
}


$pageTitle = "1. �к��������١���";
$strHead03 = "�ѹ�֡�����š�èͧ";
$pageContent = "1.3  �ѹ�֡�����š�èͧ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
	
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}			

		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day02.value=="" || document.forms.frm01.Day02.value=="00")
		{
			alert("��س��к��ѹ���ͧ");
			document.forms.frm01.Day02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day02.value,1,31) == false) {
				document.forms.frm01.Day02.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month02.value==""  || document.forms.frm01.Month02.value=="00")
		{
			alert("��س��к���͹���ͧ");
			document.forms.frm01.Month02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month02.value,1,12) == false){
				document.forms.frm01.Month02.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year02.value==""  || document.forms.frm01.Year02.value=="0000")
		{
			alert("��س��кػշ��ͧ");
			document.forms.frm01.hYear02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year02.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year02.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hBookingSaleId.value=="")
		{
			alert("��س��кؾ�ѡ�ҹ���");
			document.forms.frm01.hBookingSaleId.focus();
			return false;
		} 			
		
			
		if (document.forms.frm01.hOrderPrice.value=="")
		{
			alert("��س��к��ҤҢ��");
			document.forms.frm01.hOrderPrice.focus();
			return false;
		}				
	
		if (document.forms.frm01.hOrderReservePrice.value=="")
		{
			alert("��س��к�ǧ�Թ�ͧ");
			document.forms.frm01.hOrderReservePrice.focus();
			return false;
		} 		
	

		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			Set_Display_Type('1');
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س��кصӺ�");
			Set_Display_Type('1');
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س��к������");
			Set_Display_Type('1');
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س��кبѧ��Ѵ");
			Set_Display_Type('1');
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س��к�������ɳ���");
			Set_Display_Type('1');
			document.forms.frm01.hZip.focus();
			return false;
		} 						
		
		if (document.forms.frm01.hMobile.value=="" && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			document.forms.frm01.hHomeTel.focus();
			return false;
		} 	
		
		
		if (document.forms.frm01.hAddress03.value=="")
		{
			alert("��س��кط�����������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAddress03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode03.value=="")
		{
			alert("��س��кصӺŷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hTumbon03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode03.value=="")
		{
			alert("��س��к�����ͷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAmphur03.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode03.value=="")
		{
			alert("��س��кبѧ��Ѵ������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hProvince03.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode03.value=="")
		{
			alert("��س��к�������ɳ��������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hZip03.focus();
			return false;
		} 								
		
	
		if (document.forms.frm01.hBookingCarSeries.value=="")
		{
			alert("��س����͡Ẻö�ҡ�к�");
			document.forms.frm01.hBookingCarSeries.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hBookingCarColor.options[frm01.hBookingCarColor.selectedIndex].value=="0")
		{
			alert("��س����͡��ö");
			document.forms.frm01.hBookingCarColor.focus();
			return false;
		}	

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<script language=JavaScript >

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			if(document.frm01.hCustomerSamePlace01.checked == true){
				document.frm01.hAddress01.value = document.frm01.hAddress.value;
				document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
				document.frm01.hProvince01.value = document.frm01.hProvince.value;
				document.frm01.hZip01.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode01.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode01.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode01.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode01.value = document.frm01.hZipCode.value;		
				document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax01.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress01.value = "";
				document.frm01.hTumbon01.value = "";
				document.frm01.hAmphur01.value = "";
				document.frm01.hProvince01.value = "";
				document.frm01.hZip01.value = "";		
				document.frm01.hTumbonCode01.value = "";
				document.frm01.hAmphurCode01.value = "";
				document.frm01.hProvinceCode01.value = "";
				document.frm01.hZipCode01.value = "";		
				document.frm01.hTel01.value = "";		
				document.frm01.hFax01.value = "";		
			}
		}
   		if(val ==2){
			if(document.frm01.hCustomerSamePlace02.checked == true){
				document.frm01.hAddress02.value = document.frm01.hAddress.value;
				document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
				document.frm01.hProvince02.value = document.frm01.hProvince.value;
				document.frm01.hZip02.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode02.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode02.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode02.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode02.value = document.frm01.hZipCode.value;		
				document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax02.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress02.value = "";
				document.frm01.hTumbon02.value = "";
				document.frm01.hAmphur02.value = "";
				document.frm01.hProvince02.value = "";
				document.frm01.hZip02.value = "";		
				document.frm01.hTumbonCode02.value = "";
				document.frm01.hAmphurCode02.value = "";
				document.frm01.hProvinceCode02.value = "";
				document.frm01.hZipCode02.value = "";		
				document.frm01.hTel02.value = "";		
				document.frm01.hFax02.value = "";		
			}
			
		}
   		if(val ==3){
			if(document.frm01.hCustomerSamePlace03.checked == true){
				document.frm01.hAddress03.value = document.frm01.hAddress.value;
				document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
				document.frm01.hProvince03.value = document.frm01.hProvince.value;
				document.frm01.hZip03.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode03.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode03.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode03.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode03.value = document.frm01.hZipCode.value;		
				document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax03.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress03.value = "";
				document.frm01.hTumbon03.value = "";
				document.frm01.hAmphur03.value = "";
				document.frm01.hProvince03.value = "";
				document.frm01.hZip03.value = "";		
				document.frm01.hTumbonCode03.value = "";
				document.frm01.hAmphurCode03.value = "";
				document.frm01.hProvinceCode03.value = "";
				document.frm01.hZipCode03.value = "";		
				document.frm01.hTel03.value = "";		
				document.frm01.hFax03.value = "";		
			}
			
		}
   
   }

</script>
<br>




<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="acardUpdate.php?#free" method="POST"  onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	   <input type="hidden" name="hTrick" value="true">


<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١����������</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>������١���</strong> </td>
			<td width="30%">
				<?$objCustomerGroupList->printSelect("hGroupId",$objCustomer->getGroupId(),"��س����͡");?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong> </td>
			<td valign="top">
				<input type="hidden" name="hEventId"  value="<?=$objCustomer->getEventId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hEventId.value != '' ) frm01.hSale.focus();if(event.keyCode !=13 ) frm01.hEventId.value='';"   name="hEvent"  size=40 value="<?=$objEvent->getTitle()?>">

			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td >
				<input type="hidden" readonly size="3" name="hCustomerSaleId"  value="<?=$objCustomer->getSaleId();?>">
				<input type="text" name="hCustomerSaleName" onKeyDown="if(event.keyCode==13 && frm01.hCustomerSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hCustomerSaleId.value='';" size="30"  value="<?=$hCustomerSaleName?>">				
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><input type="text" name="hIDCard" size="30" maxlength="13"  value="<?=$objCustomer->getIDCard();?>"></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGradeList->printSelect("hGradeId",$objCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<INPUT  name="hCustomerName"  size="20"  size=30 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		</table>
		
		<table width="98%" cellpadding="0" cellspacing="0" align="center">
		<tr><td align="right">		
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>				
		</td></tr>
		<tr>
		<td  class="i_background">
		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="50"  value="<?=$objCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"></td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><input type="text" name="hMobile" size="30"  value="<?=$objCustomer->getMobile();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hMailback" value="1" <?if($objCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>	
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress01" maxlength="50" size="50"  value="<?=$objCustomer->getAddress01();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace01" onclick="sameplace(1)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode01"  value="<?=$objCustomer->getTumbonCode01();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode01.value != '' ) frm01.hAmphur01.focus();if(event.keyCode !=13 ) frm01.hTumbonCode01.value='';" name="hTumbon01" size="50"  value="<?=$objCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode01"  value="<?=$objCustomer->getAmphurCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode01.value != '' ) frm01.hProvince01.focus();if(event.keyCode !=13 ) frm01.hAmphurCode01.value='';" name="hAmphur01" size="30"  value="<?=$objCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode01"  value="<?=$objCustomer->getProvinceCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode01.value != '' ) frm01.hZip01.focus();if(event.keyCode !=13 ) frm01.hProvinceCode01.value='';" name="hProvince01" size="30"  value="<?=$objCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode01"  value="<?=$objCustomer->getZip01();?>"><input type="text" name="hZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode01.value != '' ) frm01.hTel01.focus();if(event.keyCode !=13 ) frm01.hZipCode01.value='';" size="30"  value="<?=$objCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel01" size="30"  value="<?=$objCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax01" size="30"  value="<?=$objCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress02" maxlength="50" size="50"  value="<?=$objCustomer->getAddress02();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace02" onclick="sameplace(2)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode02"  value="<?=$objCustomer->getTumbonCode02();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode02.value != '' ) frm01.hAmphur02.focus();if(event.keyCode !=13 ) frm01.hTumbonCode02.value='';" name="hTumbon02" size="50"  value="<?=$objCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode02"  value="<?=$objCustomer->getAmphurCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode02.value != '' ) frm01.hProvince02.focus();if(event.keyCode !=13 ) frm01.hAmphurCode02.value='';" name="hAmphur02" size="30"  value="<?=$objCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode02"  value="<?=$objCustomer->getProvinceCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode02.value != '' ) frm01.hZip02.focus();if(event.keyCode !=13 ) frm01.hProvinceCode02.value='';" name="hProvince02" size="30"  value="<?=$objCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode02"  value="<?=$objCustomer->getZip02();?>"><input type="text" name="hZip02"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode02.value != '' ) frm01.hTel02.focus();if(event.keyCode !=13 ) frm01.hZipCode02.value='';" size="30"  value="<?=$objCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel02" size="30"  value="<?=$objCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax02" size="30"  value="<?=$objCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress03" maxlength="50" size="50"  value="<?=$objCustomer->getAddress03();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace03" onclick="sameplace(3)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode03"  value="<?=$objCustomer->getTumbonCode03();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode03.value != '' ) frm01.hAmphur03.focus();if(event.keyCode !=13 ) frm01.hTumbonCode03.value='';" name="hTumbon03" size="50"  value="<?=$objCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode03"  value="<?=$objCustomer->getAmphurCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode03.value != '' ) frm01.hProvince03.focus();if(event.keyCode !=13 ) frm01.hAmphurCode03.value='';" name="hAmphur03" size="30"  value="<?=$objCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode03"  value="<?=$objCustomer->getProvinceCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode03.value != '' ) frm01.hZip03.focus();if(event.keyCode !=13 ) frm01.hProvinceCode03.value='';" name="hProvince03" size="30"  value="<?=$objCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode03"  value="<?=$objCustomer->getZip03();?>"><input type="text" name="hZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode03.value != '' ) frm01.hTel03.focus();if(event.keyCode !=13 ) frm01.hZipCode03.value='';" size="30"  value="<?=$objCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel03" size="30"  value="<?=$objCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax03" size="30"  value="<?=$objCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>
		
		</td></tr></table>
		<br>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="20%" class="i_background03"><strong>������ö</strong> </td>
			<td width="30%"><input type="text" name="hCarType"  value=""></td>		
			<td class="i_background03" width="20%"><strong>���ö</strong> </td>
			<td width="30%"><input type="text" name="hCarType"  value=""></td>
		</tr>			
		<tr>
			<td class="i_background03"><strong>Ẻö</strong> </td>
			<td >
				<input type="text" name="hCarType"  value="">
			</td>		
			<td class="i_background03" ><strong>��</strong> </td>
			<td >
				<input type="text" name="hCarType"  value="">
			</td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong> </td>
			<td >
				<?$objCarColorList->printSelect("hBookingCarColor",$objOrder->getBookingCarColor(),"��س����͡");?>
			</td>		
			<td class="i_background03" ><strong>������</strong> </td>
			<td >
				<input type="text" name="hCarType"  value="">
			</td>
		</tr>			
		<tr>
			<td class="i_background03" ><strong>�����</strong></td>
			<td>
				<select  name="hBookingCarGear">
					<option value="Auto" <?if($objOrder->getBookingCarGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objOrder->getBookingCarGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>	
			<td class="i_background03"><strong>����¹</strong> </td>
			<td >
				<input type="text" name="hCarType"  value="">
			</td>		

		</tr>				
		<tr>			
			<td  class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3"><input type="text" name="hBookingCarRemark" size="80"  value="<?=$objOrder->getBookingCarRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����Ż�Сѹ������</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="20%" class="i_background03"><strong>����ѷ��Сѹ���</strong> </td>
			<td width="30%"><input type="text" name="hCarType"  value=""></td>		
			<td class="i_background03" width="20%"><strong>�ѹ��������ͧ</strong> </td>
			<td width="30%"><input type="text" name="hCarType"  value=""></td>
		</tr>			
		<tr>
			<td class="i_background03"><strong>��������Сѹ���</strong> </td>
			<td colspan="3" >
				<table>
				<tr>
					<td width="100"><input type="radio" name="hInsureType"  value="GOA"> GOA</td>
					<td width="100"><input type="radio" name="hInsureType"  value="�.1/��ҧ"> �.1/��ҧ</td>
					<td width="100"><input type="radio" name="hInsureType"  value="�.1/���"> �.1/���</td>
					<td width="250"><input type="radio" name="hInsureType"  value="����"> ���� <input type="text" name="hInsureTypeRemark"  size="20" value=""></td>
					<td width="100"><input type="radio" name="hInsureType"  value="�ú."> �ú.</td>
				</tr>
				</table>
			</td>		
		</tr>		
		<tr>
			<td class="i_background03"><strong>���͹䢡�â��</strong> </td>
			<td colspan="3" >
			<table>
			<tr>
				<td width="100"><input type="radio" name="hPaymentCondition"  value="�������"> �������</td>
				<td width="100"><input type="radio" name="hPaymentCondition"  value="�觨���"> �觨���</td>
				<td width="250" ><input type="radio" name="hPaymentCondition"  value="��ǹŴ"> ��ǹŴ <input type="text" name="hInsureTypeRemark"  size="15" value=""></td>
				<td width="250" ><input type="radio" name="hPaymentCondition"  value="�ͧ��"> �ͧ�� <input type="text" name="hInsureTypeRemark"  size="15" value=""></td>
			</tr>
			</table>
			</td>	
		</tr>			
		<tr>
			<td class="i_background03" ><strong>�������</strong></td>
			<td colspan="3">
				<table>
				<tr>
					<td width="100"><input type="radio" name="hLoneType"  value="�Ҥ��Ѥ��"> �Ҥ��Ѥ��</td>
					<td width="200"><input type="text" name="hInsureTypeRemark"  size="20" value=""></td>
					<td width="100"><input type="radio" name="hLoneType"  value="�Ҥ�ѧ�Ѻ"> �Ҥ�ѧ�Ѻ</td>
					<td><input type="text" name="hInsureTypeRemark"  size="20" value=""></td>
					<td></td>
				</tr>
				</table>
			</td>	
		</tr>			
		<tr>
			<td class="i_background03"><strong>�Ըա�ê���</strong> </td>
			<td colspan="3" >
				<input type="radio" name="hPaymentType"  value="�������"> �Թʴ
				<input type="radio" name="hPaymentType"  value="�觨���"> �ѵ��ôԵ
				<input type="radio" name="hPaymentType"  value="��ǹŴ"> pay in
			</td>	
		</tr>				
		<tr>			
			<td  class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3"><input type="text" name="hBookingCarRemark" size="80"  value="<?=$objOrder->getBookingCarRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�ê��Ф�һ�Сѹ���</td>
</tr>
</table>
<table width="100%" cellpadding="3" cellspacing="1" class="i_background02">
<tr>
	<td class="listTitle" width="5%">�Ǵ���</td>
	<td class="listTitle" width="15%">�ѹ���</td>
	<td class="listTitle" width="15%">��������ê���</td>
	<td class="listTitle" width="10%">�ӹǹ</td>
	<td class="listTitle" width="15%">�Ţ��������</td>
	<td class="listTitle" width="20%">��������</td>
	<td class="listTitle" width="30%">�����˵�</td>
</tr>
<tr>
	<td class="listDetail" align="center">1</td>
	<td class="listDetail" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail" align="center">
		<select>
			<option value="">��س����͡
			<option value="�Թʴ">�Թʴ
			<option value="�ѵ��ôԵ">�ѵ��ôԵ
			<option value="pay in">pay in
		</select>
	</td>
	<td class="listDetail" align="center"><input type="text" name="hInsureTypeRemark"  size="10" value=""></td>
	<td class="listDetail" align="center"><input type="text" name="hInsureTypeRemark"  size="20" value=""></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus0">��������<input type="radio" name="hPaymentStatus0">��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hInsureTypeRemark"  size="20" value="">	</td>	
</tr>
<tr>
	<td class="listDetail" align="center">2</td>
	<td class="listDetail" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>	
</tr>
<tr>
	<td class="listDetail" align="center">3</td>
	<td class="listDetail" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>
	<td class="listDetail" align="center"></td>	
</tr>
</table>

<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();" >
<?}else{?>
	<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();" >
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button"  class="button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='bcardList.php'">			
	<br><br>
	</td>
</tr>
</table>
<?if ($strMode == "Update"){
$objMemberTemp = new Member();
$objMemberTemp->setMemberId($objOrder->getEditBy());
$objMemberTemp->load();
?>		
<table width="100%" >
<tr>
	<td align="right">��䢢���������ش�� :   <?=$objMemberTemp->getNickname()?>     �ѹ���    <?=$objOrder->getEditDate()?> </td>
</tr>
</table>
<?
unset($objMemberTemp);
}?>		
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {
		<?if($hId!= ""){?>
		return "bcardAutoCustomer.php?q=" + this.text.value+"&hId="+<?=$hId?>;
		<?}else{?>
		return "bcardAutoCustomer.php?q=" + this.text.value;
		<?}?>
	});
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoPostcode.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon01", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur01.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince01.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon02", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur02.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince02.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon03", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur03.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince03.php?q=" + this.text.value;
		}
	});		
	
	
	
	new CAPXOUS.AutoComplete("hEvent", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoEvent.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hCarNumber", function() {
		return "bcardAutoStockCar.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {
		return "bcardAutoOrderNumber.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "bcardAutoSale.php?q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hCustomerSaleName", function() {
		return "bcardAutoCustomerSale.php?q=" + this.text.value;
	});			
	
	new CAPXOUS.AutoComplete("hCarSeries", function() {
		return "bcardAutoCarSeries.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hOrderProduct", function() {
		return "bcardAutoStockProduct.php?hBookingCarSeries="+document.frm01.hBookingCarSeries.value+"&q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hOrderPremium", function() {
		return "bcardAutoStockProductPremium.php?q=" + this.text.value;
	});		
	
</script>

<script>
	function sumAll(){	
	
	}

	function check_product(){
	
	if(document.frm01.hOrderProductOther.checked == false){	
		if(document.frm01.hOrderProductId.value == "" || document.frm01.hOrderProductId.value == 0){
			alert("��س����͡��¡���Թ��Ҩҡ�к�");
			document.frm01.hTrick.value="false";	
			document.frm01.hOrderProduct.focus();	
			return false;
		}
	}else{
		if(document.frm01.hOrderProduct.value== ""){
			alert("��س��кت����Թ���");
			document.frm01.hTrick.value="false";		
			document.frm01.hOrderProduct.focus();
			return false;
		}
	
	}
	
	if(document.frm01.hOrderProductQty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		document.frm01.hTrick.value="false";		
		document.frm01.hOrderProductQty.focus();
		return false;
	}


	
	return true;		
	}

	function check_premium(){
	
	if(document.frm01.hOrderPremiumOther.checked == false){	
		if(document.frm01.hOrderPremiumId.value == "" || document.frm01.hOrderPremiumId.value == 0){
			alert("��س����͡��¡���Թ��Ҩҡ�к�");
			document.frm01.hTrick.value="false";	
			document.frm01.hOrderPremium.focus();	
			return false;
		}
	}else{
		if(document.frm01.hOrderPremium.value== ""){
			alert("��س��кت����Թ���");
			document.frm01.hTrick.value="false";		
			document.frm01.hOrderPremium.focus();
			return false;
		}
	
	}
	
	if(document.frm01.hOrderPremiumQty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		document.frm01.hTrick.value="false";		
		document.frm01.hOrderPremiumQty.focus();
		return false;
	}


	
	return true;		
	}	
	
	function checkInsure(){
		if(document.frm01.hInsureFrom[0].checked == true){
			document.getElementById("insure").style.display = "";	
		}else{
			document.getElementById("insure").style.display = "none";	
		}
	
	}	
	
</script>
<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
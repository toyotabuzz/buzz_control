<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

$objIQ = new InsureQuotation();
$objQL01 = new InsureQuotationItem();
$objQL02 = new InsureQuotationItem();
$objQL03 = new InsureQuotationItem();

$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();
		
$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$objSale = new Member();
$objSale->setMemberId($objInsure->get_sale_id());
$objSale->load();

$objCompany = new Company();
$objCompany->setCompanyId($objInsure->get_company_id());
$objCompany->load();

$objIQ->set_quotation_id($hId);
$objIQ->load();
$strMode="Update";

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objIQ->get_car_id());
$objInsureCar->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$arrDate = explode("-",$objIQ->get_date_add());
$Day02 = $arrDate[2];
$Month02 = $arrDate[1];
$Year02 = $arrDate[0];

$objQIList = new InsureQuotationItemList();
$objQIList->setFilter(" quotation_id = $hId ");
$objQIList->setPageSize(0);
$objQIList->setSort(" quotation_item_id ASC ");
$objQIList->load();
$i=0;
forEach($objQIList->getItemList() as $objItem) {
	if($i==0){
		$objQL01->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL01->load();
	}
	
	if($i==1){
		$objQL02->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL02->load();
	}

	if($i==2){
		$objQL03->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL03->load();
	}
	$i++;
}

$objInsureList = New InsureList;

$objLabel = new Label();
$strText01= "��˹������š�þ������ʹ��Ҥ�";	
$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
define("IMG_INSURE","../images/misc/".$objLabel->getText02());

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	$this->Image(IMG_INSURE,20,10,180);
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{

    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(10,10,10);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

$pdf->AddPage();
$pdf->SetLeftMargin(20);

$extend1= 35;

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(230,50+$extend1);
$pdf->Write(10,"��ʹ��Ҥ����»�Сѹ���ö¹��");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,70+$extend1);
$pdf->Write(10,"�Ţ�����ʹ��Ҥ� : ".$objIQ->get_quotation_number());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,85+$extend1);
$pdf->Write(10,$objLabel->getText01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,100+$extend1);
$pdf->Write(10,"�Ţ����͹حҵ : ".$objLabel->getText03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,115+$extend1);
$pdf->Write(10,"���Դ��� : ".$objSale->getFirstname()." ".$objSale->getLastname()." (".$objSale->getNickname().")");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,130+$extend1);
$pdf->Write(10,"�� : ".$objLabel->getText04()." ῡ�� : ".$objLabel->getText05());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,70+$extend1);
$pdf->Write(10,"���¹  ".$objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,85+$extend1);
$pdf->Write(10,"�������  ".$objCustomer->getAddressLabel());

if($objCustomer->mProvince == "��ا෾��ҹ��"){
	$sText = " �ǧ ".$objCustomer->mTumbon." ࢵ ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
}else{
	$sText =  " �Ӻ� ".$objCustomer->mTumbon." ����� ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(35,100+$extend1);
$pdf->Write(10,$sText);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,145+$extend1);
$pdf->Write(10,"�����һ�Сѹ��� : ".$objIQ->get_text01());

$image1 = "check.jpg";
$image2 = "un_check.jpg";
if($objIQ->get_text02()=="����кؼ��Ѻ���") { $pdf->Image($image1, 270,146+$extend1,7); }else{$pdf->Image($image2, 270, 146+$extend1,7); }
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(280,145+$extend1);
$pdf->Write(10,"����кؼ��Ѻ��� ");

if($objIQ->get_text02()=="�кؼ��Ѻ���") { $pdf->Image($image1, 370,146+$extend1,7); }else{$pdf->Image($image2, 370, 146+$extend1,7); }
$pdf->SetXY(380,145+$extend1);
$pdf->Write(10,"�кؼ��Ѻ��� ");

if($objIQ->get_text02()=="����") { $pdf->Image($image1, 450,146+$extend1,7); }else{$pdf->Image($image2, 450, 146+$extend1,7); }
$pdf->SetXY(460,145+$extend1);
$pdf->Write(10,"����  ".$objIQ->get_text03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,165+$extend1);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text04());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,165+$extend1);
if($objIQ->get_text05() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatShortDate($objIQ->get_text05()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(330,165+$extend1);
$pdf->Write(10,"�Ţ����͹حҵ� : ".$objIQ->get_text06());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(480,165+$extend1);
if($objIQ->get_text07() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatShortDate($objIQ->get_text07()));
}else{
$pdf->Write(10,"�ѹ������� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,180+$extend1);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text08());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,180+$extend1);
if($objIQ->get_text09() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatShortDate($objIQ->get_text09()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(330,180+$extend1);
$pdf->Write(10,"�Ţ����͹حҵ� : ".$objIQ->get_text10());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(480,180+$extend1);
if($objIQ->get_text11() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatShortDate($objIQ->get_text11()));
}else{
$pdf->Write(10,"�ѹ������� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,200+$extend1);
if($objIQ->get_text12() != "--"){
$pdf->Write(10,"�������һ�Сѹ��� : ".formatShortDate($objIQ->get_text12()));
}else{
$pdf->Write(10,"�������һ�Сѹ��� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,200+$extend1);
if($objIQ->get_text13() != "--"){
$pdf->Write(10,"����ش : ".formatShortDate($objIQ->get_text13()));
}else{
$pdf->Write(10,"����ش : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(330,200+$extend1);
$pdf->Write(10,"���� : ".$objIQ->get_text14()." �.");



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(260,220+$extend1);
$pdf->Write(10,"��¡��ö¹����һ�Сѹ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,240+$extend1);
$pdf->Write(10,"�ӴѺ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(47,240+$extend1);
$pdf->Write(10,"����");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,240+$extend1);
$pdf->Write(10,"����ö¹��/���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,240+$extend1);
$pdf->Write(10,"�Ţ����¹");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(310,240+$extend1);
$pdf->Write(10,"�Ţ��Ƕѧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,240+$extend1);
$pdf->Write(10,"�����");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(435,240+$extend1);
$pdf->Write(10,"������ö");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(490,240+$extend1);
$pdf->Write(10,"�ӹǹ/��Ҵ/���˹ѡ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,260+$extend1);
$pdf->Write(10,$objIQ->get_text15());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(48,260+$extend1);
$pdf->Write(10,$objIQ->get_text16());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(70,260+$extend1);
$pdf->Write(10,$objIQ->get_text17());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(70,275+$extend1);
$pdf->Write(10,$objIQ->get_text18());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(190,260+$extend1);
$pdf->Write(10,$objIQ->get_text19());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(190,275+$extend1);
$pdf->Write(10,$objIQ->get_text20());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(270,260+$extend1);
$pdf->Write(10,$objIQ->get_text21());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,260+$extend1);
$pdf->Write(10,$objIQ->get_text22());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(440,260+$extend1);
$pdf->Write(10,$objIQ->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(490,260+$extend1);
$pdf->Write(10,$objIQ->get_text24());


$pdf->line(20,141+$extend1,580,141+$extend1);
$pdf->line(20,161+$extend1,580,161+$extend1);
$pdf->line(20,196+$extend1,580,196+$extend1);
$pdf->line(20,216+$extend1,580,216+$extend1);
$pdf->line(20,236+$extend1,580,236+$extend1);
$pdf->line(20,256+$extend1,580,256+$extend1);
$pdf->line(20,290+$extend1,580,290+$extend1);

$pdf->line(20,141+$extend1,20,290+$extend1);
$pdf->line(580,141+$extend1,580,290+$extend1);

$pdf->line(45,236+$extend1,45,290+$extend1);
$pdf->line(70,236+$extend1,70,290+$extend1);
$pdf->line(190,236+$extend1,190,290+$extend1);
$pdf->line(260,236+$extend1,260,290+$extend1);
$pdf->line(390,236+$extend1,390,290+$extend1);
$pdf->line(430,236+$extend1,430,290+$extend1);
$pdf->line(480,236+$extend1,480,290+$extend1);

$extend = 60;

$pdf->SetFont('angsa','B',13);	
$pdf->SetXY(260,295+$extend);
$pdf->Write(10,"��������´����������ͧ");

if($objQL01->get_text16() >0 or $objQL02->get_text16() >0  or $objQL03->get_text16() >0  ){













for($yy=0;$yy<19;$yy++){
	$yyy = ($yy*15)+310+$extend;
	
	if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16) and ($yy!=17) and ($yy!=19) and ($yy!=20) and ($yy!=21)  ){
	$pdf->line(20,$yyy,580,$yyy);
	}else{
	$pdf->line(270,$yyy,580,$yyy);
	}
}

$pdf->line(20,310+$extend,20,$yyy);

$pdf->line(270,310+$extend,270,$yyy);
$pdf->line(380,310+$extend,380,$yyy);
$pdf->line(480,310+$extend,480,$yyy);

$pdf->line(580,310+$extend,580,$yyy);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(272,313+$extend);
$pdf->Write(10,$objQL01->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(382,313+$extend);
$pdf->Write(10,$objQL02->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(482,313+$extend);
$pdf->Write(10,$objQL03->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(272,328+$extend);
$pdf->Write(10,$objQL01->get_text01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(382,328+$extend);
$pdf->Write(10,$objQL02->get_text01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(482,328+$extend);
$pdf->Write(10,$objQL03->get_text01());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,343+$extend);
$pdf->Write(10,"�����Ѻ�Դ�ͺ��ͺؤ����¹͡");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,358+$extend);
$pdf->Write(10,"����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(270,355+$extend);$arrNum0 = explode("*",$objQL01->get_text03());if($arrNum0[0] >0){$pdf->Cell(110,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$pdf->SetXY(380,355+$extend);$arrNum0 = explode("*",$objQL02->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$pdf->SetXY(480,355+$extend);$arrNum0 = explode("*",$objQL03->get_text03());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$pdf->Ln();
/*
$pdf->SetXY(300,358+$extend);$arrNum0 = explode(".",$objQL01->get_text03());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(415,358+$extend);$arrNum1 = explode(".",$objQL02->get_text03());if($arrNum1[0] >0){$pdf->Write(10,$arrNum1[0]);}unset($arrNum0);
$pdf->SetXY(515,358+$extend);$arrNum2 = explode(".",$objQL03->get_text03());if($arrNum2[0] >0){$pdf->Write(10,$arrNum2[0]);}unset($arrNum0);
*/
$pdf->SetFont('angsa','B',12);	
//$pdf->SetXY(230,373+$extend);
$pdf->Cell(250,15,"�����ͤ���",1,0,"R");
$pdf->SetFont('angsa','B',14);	
$arrNum0 = explode("*",$objQL01->get_text04());if($arrNum0[0] >0){$pdf->Cell(110,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL02->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$arrNum0 = explode("*",$objQL03->get_text04());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);

$pdf->SetXY(300,373+$extend);$arrNum0 = explode(".",$objQL01->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,373+$extend);$arrNum0 = explode(".",$objQL02->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,373+$extend);$arrNum0 = explode(".",$objQL03->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,389+$extend);
$pdf->Write(10,"����������µ�ͷ�Ѿ���Թ���ó�");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,389+$extend);$arrNum0 = explode(".",$objQL01->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,389+$extend);$arrNum0 = explode(".",$objQL02->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,389+$extend);$arrNum0 = explode(".",$objQL03->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,403+$extend);
$pdf->Write(10,"����������ͧö¹������һ�Сѹ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,418+$extend);
$pdf->Write(10,"����������µ��ö¹������һ�Сѹ���");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,418+$extend);$arrNum0 = explode(".",$objQL01->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,418+$extend);$arrNum0 = explode(".",$objQL02->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,418+$extend);$arrNum0 = explode(".",$objQL03->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,433+$extend);
$pdf->Write(10,"ö¹���٭���/�����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,433+$extend);$arrNum0 = explode(".",$objQL01->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,433+$extend);$arrNum0 = explode(".",$objQL02->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,433+$extend);$arrNum0 = explode(".",$objQL03->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,448+$extend);
$pdf->Write(10,"����������ͧ����͡���Ṻ����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,448+$extend);$arrNum0 = explode(".",$objQL01->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,448+$extend);$arrNum0 = explode(".",$objQL02->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,448+$extend);$arrNum0 = explode(".",$objQL03->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,463+$extend);
$pdf->Write(10,"�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,463+$extend);$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,463+$extend);$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,463+$extend);$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,478+$extend);
$pdf->Write(10,"����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,478+$extend);$arrNum0 = explode(".",$objQL01->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,478+$extend);$arrNum0 = explode(".",$objQL02->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,478+$extend);$arrNum0 = explode(".",$objQL03->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,493+$extend);
$pdf->Write(10,"��Сѹ��Ǽ��Ѻ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,493+$extend);
$pdf->Write(10,"��Сѹ��Ǽ��Ѻ���");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,493+$extend);$arrNum0 = explode(".",$objQL01->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,493+$extend);$arrNum0 = explode(".",$objQL02->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,493+$extend);$arrNum0 = explode(".",$objQL03->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(180,508+$extend);
$pdf->Write(10,"���»�Сѹ��»����� 1");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,508+$extend);$arrNum0 = explode("*",$objQL01->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,508+$extend);$arrNum0 = explode("*",$objQL02->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,508+$extend);$arrNum0 = explode("*",$objQL03->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(214,523+$extend);
$pdf->Write(10,"�������  �ú.");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,523+$extend);$arrNum0 = explode("*",$objQL01->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,523+$extend);$arrNum0 = explode("*",$objQL02->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,523+$extend);$arrNum0 = explode("*",$objQL03->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(150,538+$extend);
$pdf->Write(10,"���»�Сѹ��»���� 1 ��� �ú.");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,538+$extend);$arrNum0 = explode("*",$objQL01->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,538+$extend);$arrNum0 = explode("*",$objQL02->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,538+$extend);$arrNum0 = explode("*",$objQL03->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(210,553+$extend);
$pdf->Write(10,"��ǹŴ �����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,553+$extend);$arrNum0 = explode("*",$objQL01->get_text16());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,553+$extend);$arrNum0 = explode("*",$objQL02->get_text16());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,553+$extend);$arrNum0 = explode("*",$objQL03->get_text16());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(210,568+$extend);
$pdf->Write(10,"�ʹ�����ط��");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(270,565+$extend);$arrNum0 = explode("*",$objQL01->get_text17());if($arrNum0[0] >0){$pdf->Cell(110,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$pdf->SetXY(380,565+$extend);$arrNum0 = explode("*",$objQL02->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);
$pdf->SetXY(480,565+$extend);$arrNum0 = explode("*",$objQL03->get_text17());if($arrNum0[0] >0){$pdf->Cell(100,15,$arrNum0[0],1,0,"C");}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,588+$extend);
$pdf->Write(15,$objIQ->get_remark());









}else{













for($yy=0;$yy<25;$yy++){
	$yyy = ($yy*15)+310+$extend;
	
	if( ($yy != 1) and ($yy!=14) and ($yy!=15) and ($yy!=16) and ($yy!=17) and ($yy!=19) and ($yy!=20) and ($yy!=21) and ($yy!=22)  ){
	$pdf->line(20,$yyy,580,$yyy);
	}else{
	$pdf->line(270,$yyy,580,$yyy);
	}
}

$pdf->line(20,310+$extend,20,$yyy);

$pdf->line(270,310+$extend,270,$yyy);
$pdf->line(380,310+$extend,380,$yyy);
$pdf->line(480,310+$extend,480,$yyy);

$pdf->line(580,310+$extend,580,$yyy);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(272,313+$extend);
$pdf->Write(10,$objQL01->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(382,313+$extend);
$pdf->Write(10,$objQL02->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(482,313+$extend);
$pdf->Write(10,$objQL03->get_text23());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(272,328+$extend);
$pdf->Write(10,$objQL01->get_text01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(382,328+$extend);
$pdf->Write(10,$objQL02->get_text01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(482,328+$extend);
$pdf->Write(10,$objQL03->get_text01());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(100,320+$extend);
$pdf->Write(10,"��������´����������ͧ");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,343+$extend);
$pdf->Write(10,"�����Ѻ�Դ�ͺ��ͺؤ����¹͡");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,358+$extend);
$pdf->Write(10,"����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,358+$extend);$arrNum0 = explode(".",$objQL01->get_text03());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(415,358+$extend);$arrNum1 = explode(".",$objQL02->get_text03());if($arrNum1[0] >0){$pdf->Write(10,$arrNum1[0]);}unset($arrNum0);
$pdf->SetXY(515,358+$extend);$arrNum2 = explode(".",$objQL03->get_text03());if($arrNum2[0] >0){$pdf->Write(10,$arrNum2[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(230,373+$extend);
$pdf->Write(10,"�����ͤ���");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,373+$extend);$arrNum0 = explode(".",$objQL01->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,373+$extend);$arrNum0 = explode(".",$objQL02->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,373+$extend);$arrNum0 = explode(".",$objQL03->get_text04());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,389+$extend);
$pdf->Write(10,"����������µ�ͷ�Ѿ���Թ���ó�");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,389+$extend);$arrNum0 = explode(".",$objQL01->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,389+$extend);$arrNum0 = explode(".",$objQL02->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,389+$extend);$arrNum0 = explode(".",$objQL03->get_text05());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,403+$extend);
$pdf->Write(10,"����������ͧö¹������һ�Сѹ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,418+$extend);
$pdf->Write(10,"����������µ��ö¹������һ�Сѹ���");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,418+$extend);$arrNum0 = explode(".",$objQL01->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,418+$extend);$arrNum0 = explode(".",$objQL02->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,418+$extend);$arrNum0 = explode(".",$objQL03->get_text07());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,433+$extend);
$pdf->Write(10,"ö¹���٭���/�����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,433+$extend);$arrNum0 = explode(".",$objQL01->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,433+$extend);$arrNum0 = explode(".",$objQL02->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,433+$extend);$arrNum0 = explode(".",$objQL03->get_text08());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(80,448+$extend);
$pdf->Write(10,"����������ͧ����͡���Ṻ����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,448+$extend);$arrNum0 = explode(".",$objQL01->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,448+$extend);$arrNum0 = explode(".",$objQL02->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,448+$extend);$arrNum0 = explode(".",$objQL03->get_text09());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,463+$extend);
$pdf->Write(10,"�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,463+$extend);$arrNum0 = explode(".",$objQL01->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,463+$extend);$arrNum0 = explode(".",$objQL02->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,463+$extend);$arrNum0 = explode(".",$objQL03->get_text10());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,478+$extend);
$pdf->Write(10,"����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,478+$extend);$arrNum0 = explode(".",$objQL01->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,478+$extend);$arrNum0 = explode(".",$objQL02->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,478+$extend);$arrNum0 = explode(".",$objQL03->get_text11());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,493+$extend);
$pdf->Write(10,"��Сѹ��Ǽ��Ѻ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,493+$extend);
$pdf->Write(10,"��Сѹ��Ǽ��Ѻ���");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,493+$extend);$arrNum0 = explode(".",$objQL01->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,493+$extend);$arrNum0 = explode(".",$objQL02->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,493+$extend);$arrNum0 = explode(".",$objQL03->get_text12());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(180,508+$extend);
$pdf->Write(10,"���»�Сѹ��»����� 1");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,508+$extend);$arrNum0 = explode("*",$objQL01->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,508+$extend);$arrNum0 = explode("*",$objQL02->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,508+$extend);$arrNum0 = explode("*",$objQL03->get_text13());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(214,523+$extend);
$pdf->Write(10,"������� �ú.");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,523+$extend);$arrNum0 = explode("*",$objQL01->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,523+$extend);$arrNum0 = explode("*",$objQL02->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,523+$extend);$arrNum0 = explode("*",$objQL03->get_text14());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(243,538+$extend);
$pdf->Write(10,"���»�Сѹ��»����� 1 ��� �ú.");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(300,538+$extend);$arrNum0 = explode("*",$objQL01->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(410,538+$extend);$arrNum0 = explode("*",$objQL02->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);
$pdf->SetXY(510,538+$extend);$arrNum0 = explode("*",$objQL03->get_text15());if($arrNum0[0] >0){$pdf->Write(10,$arrNum0[0]);}unset($arrNum0);




$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,550+$extend);
$pdf->Write(15,$objIQ->get_remark());

}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,700);
//$pdf->Write(15,"���ʴ������Ѻ���");
$pdf->Cell(150,20,"���ʴ������Ѻ���",0,0,"C");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,740);
$pdf->Cell(150,20,"(".$objSale->getFirstname()." ".$objSale->getLastname().")",0,0,"C");
//$pdf->Write(15,"(".$objSale->getFirstname()." ".$objSale->getLastname().")");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,760);
$pdf->Cell(150,20,"��ѡ�ҹ��Сѹ���ö¹��",0,0,"C");
//$pdf->Write(15,"��ѡ�ҹ��Сѹ���ö¹��");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,700);
$pdf->SetRightMargin(100);
//$pdf->Cell(80,20,$objIQ->get_xtra02(),0,0,"L");
$pdf->Write(15,$objIQ->get_xtra02());


$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,800);
$pdf->Write(15,$objLabel->getText01());

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(20,800);
$pdf->Write(15,$objLabel->getText01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,815);
$pdf->Write(15,$objLabel->getText06());

$pdf->Output();	
?>
<script>window.close();</script>	
<?include "unset_all.php";?>
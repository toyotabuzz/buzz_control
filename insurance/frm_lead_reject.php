<?php
header('Content-Type: text/html; charset=UTF-8');
include("../conf_inside.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Reject Lead</title>
</head>

<body>
<?php
if (!empty($_POST)) {
    $server_name = $_SERVER['REQUEST_URI'];
    $host       = DB_HOST;
    $user       = DB_USER;
    $password   = DB_PASS;
    $dbname     = DB_NAME;
    $conn = mysqli_connect($host, $user, $password, $dbname);
    mysqli_query("SET NAMES tis620");
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $sqlCheckCar = 'SELECT car_id FROM t_insure_car WHERE car_id = ' . $_GET['hId'] ;
    $resultCar = $conn->query($sqlCheckCar);
    if ($resultCar->num_rows > 0) {
        $sqlUpdateInsureCar = "UPDATE t_insure_car SET team = 0, team_id = 0, sale_id = 0 WHERE car_id = ". $_GET['hId'];
        $resultUpdateInsureCar = $conn->query($sqlUpdateInsureCar);

        $sqlInsertInsureReject = "INSERT INTO t_insure_lead_reject (car_id, reject_type, reject_remark, created_at, created_by)
                                    VALUES (".$_POST['car_id'].", ". $_POST['reject_type'].", '".$_POST['reject_remark']."', NOW(), ".$_SESSION['sMemberId'].");";
        $resultInsertInsureReject = $conn->query($sqlInsertInsureReject);
    }
}
?>
<form method="post" role="form" id="frm_reject" action="">
    <div class="container">
        <div class="panel-group" style="margin-top: 20px;">
            <div class="panel panel-primary">
                <div class="panel-heading">Reject Lead</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>หมายเหตุการ Reject</label>
                                <select name="reject_type" id="reject_type"  class="form-control">
                                    <option value="1">ขายรถ</option>
                                    <option value="2">คืนซาก</option>
                                    <option value="3">ลูกค้าคืนรถไฟแนนซ์</option>
                                    <option value="4">อื่นๆ</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>รายละเอียดการ Reject</label>
                                <textarea name="reject_remark" id="reject_remark"  rows="4" cols="10"  class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <a href="/insurance/main.php" class="btn btn-default">ย้อนกลับ</a>
                        </div>
                        <div class="col-md-6 text-right">
                        <?php if (empty($_POST)) { ?>
                            <button type="button" class="btn btn-success" onClick="saveReject()" id="btnsave" value="save">บันทึก Reject</button>
                        <?php } else {
                          echo  "<h3>ทำการบันทึกข้อมูลแล้ว</h3>";
                        } ?>
                        </div>
                    </div>
                </div>
            </div>
            <br/>

        </div>
    </div>
    <input type="hidden" name="car_id" value="<?php echo $_GET["hId"];?>">
</form>

<script>
    function saveReject() {
        if (confirm("ยืนยันการบันทึก Reject หรือไม่?")) {
            $('#frm_reject').submit();
        }
    }
</script>
</body>

</html>
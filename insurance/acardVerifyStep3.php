<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",1000);


$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objCustomerList = New CustomerList;


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hKeywordCar !="" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";
	}else{
		$pstrCondition .="  ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";	
	}
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hBuyCompany > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.buy_company = '".$hBuyCompany."' ";
	}else{
		$pstrCondition .=" IC.buy_company = '".$hBuyCompany."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" IC.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$Month02."-%' ";
	}else{
		$pstrCondition .=" IC.date_verify LIKE '%-".$Month02."-%'   ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" IC.company_id = '".$hCompanyId."' ";	
	}
}

if($hSetMonth != 0 and $hSetMonth != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$hSetMonth."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '%-".$hSetMonth."-%' ";	
	}
}

if($hSetYear != 0 and $hSetYear != ""){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '".$hSetYear."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '".$hSetYear."-%' ";	
	}
}

if($hRegisterYear > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.register_year = '".$hRegisterYear."' ";
	}else{
		$pstrCondition .=" IC.register_year = '".$hRegisterYear."' ";	
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  = '".$hStartDate."' ) ";
		}
	}
}


}//end hSearch


if(isset($hUpdate)){
	for($i=0;$i<$hCountTotal;$i++){
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($hInsureCarId[$i]);
		
		if($hSale[$i] == 9999 ){
			$objInsureCar->set_sale_id(0);
			$objInsureCar->updateSale();
			$objInsureCar->set_junk(1);
			$objInsureCar->updateJunk();
		}else{
			$objInsureCar->set_sale_id($hSale[$i]);
			$objInsureCar->updateSale();
		}
		
	}
}


if (!empty($hDelete) )   {  //Request to delete some CashierReserve release
    $objInsureCar = New InsureCar();
	$objInsureCar->set_car_id($hDelete);
	
	$objInsureList = new InsureList();
	$objInsureList->setFilter(" car_id= ".$hDelete);
	$objInsureList->setPageSize(0);
	$objInsureList->load();
	forEach($objInsureList->getItemList() as $objItem) {
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
	
		$objInsureQuotationList = new InsureQuotationList();
		$objInsureQuotationList->setFilter(" insure_id= ".$objItem->get_insure_id());
		$objInsureQuotationList->setPageSize(0);
		$objInsureQuotationList->load();
		forEach($objInsureQuotationList->getItemList() as $objItemQuotation) {
			$objInsureQuotation = new InsureQuotation();
			$objInsureQuotation->set_quotation_id($objItemQuotation->get_quotation_id());
			$objInsureQuotation->delete();
		}
		$objInsure->delete();

	}
	
	$objInsureCar->delete();
	++$intCountDelete;
    if ($pstrMsg == "" ) $pstrMsg .= " ź������ " .$intCountDelete . " ��¡�� ";
    unset($objCustomer);
} else {
    $pstrMsg = $hMsg;
}

//echo $pstrCondition;

$objCustomerList = new InsureCarList();
$objCustomerList->setFilter($pstrCondition);
$objCustomerList->setPageSize(PAGE_SIZE);
$objCustomerList->setPage($hPage);
$objCustomerList->setSortDefault(" niti, C.firstname, C.lastname, date_verify ASC");
$objCustomerList->setSort($hSort);
$objCustomerList->loadSearch();

$pCurrentUrl = "acardVerifyStep3.php?hBuyCompany=$hBuyCompany&hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType&Month02=$Month02&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hCarType=$hCarType&hCarModel=$hCarModel&hKeyword=$hKeyword&hSaleId=$hSaleId&hSetMonth=$hSetMonth&hSetYear=$hSetYear";
	// for paging only (sort resets page viewing)
$pCurrentUrlLink = "acardVerifyStep3.php?hBuyCompany=".$hBuyCompany."YYYhSearch=".$hSearch."YYYhKeyword=".$hKeyword."YYYhType=".$hType."YYYMonth02=".$Month02."YYYDay=".$Day."YYYMonth=".$Month."YYYYear=".$Year."YYYDay01=".$Day01."YYYMonth01=".$Month01."YYYYear01=".$Year01."YYYhCarType=".$hCarType."YYYhCarModel=".$hCarModel."YYYhKeyword=".$hKeyword."YYYhSaleId=".$hSaleId."YYYhPage=".$hPage;

	

$pageTitle = "1. �����Ż�Сѹ���";
$strHead03 = "�Ѵ����١��� ACARD ";
$pageContent = "1.2 �Ѵ����١��� ACARD ";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="acardVerifyStep3-1.php" method="get">
<table  class="search" cellpadding="3" width="95%" align="center">
<tr>
	<td width="100" align="right"></td>
	<td>1. ���͡��͹�շ���ͧ��èѴ���</td>
</tr>
<tr>
	<td width="100" align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>
<tr>
	<td align="right">��͹������� : </td>
	<td>
		<select name="hSetMonth">
			<option value="0"> - �ء��͹ -
			<option value="01" <?if($hSetMonth == "01") echo "selected"?>>���Ҥ�
			<option value="02" <?if($hSetMonth == "02") echo "selected"?>>����Ҿѹ��
			<option value="03" <?if($hSetMonth == "03") echo "selected"?>>�չҤ�
			<option value="04" <?if($hSetMonth == "04") echo "selected"?>>����¹
			<option value="05" <?if($hSetMonth == "05") echo "selected"?>>����Ҥ�
			<option value="06" <?if($hSetMonth == "06") echo "selected"?>>�Զع�¹		
			<option value="07" <?if($hSetMonth == "07") echo "selected"?>>�á�Ҥ�
			<option value="08" <?if($hSetMonth == "08") echo "selected"?>>�ԧ�Ҥ�
			<option value="09" <?if($hSetMonth == "09") echo "selected"?>>�ѹ¹¹
			<option value="10" <?if($hSetMonth == "10") echo "selected"?>>���Ҥ�
			<option value="11" <?if($hSetMonth == "11") echo "selected"?>>��Ȩԡ�¹
			<option value="12" <?if($hSetMonth == "12") echo "selected"?>>�ѹ�Ҥ�																					
		</select>
		&nbsp;&nbsp;�յ������&nbsp;&nbsp;
		<?$dateNow = date("Y");?>
		<select name="hSetYear">
			<option value="0"> - �ء�� -
			<?for($i=($dateNow-10);$i<=($dateNow+1);$i++){?>
			<option value="<?=$i?>" <?if($hSetYear==$i) echo "selected"?>><?=$i?>
			<?}?>				
		</select>		
		
	</td>
</tr>
<tr>
	<td></td>
	<td ><input type="submit" value="1. ���͡��͹����ͧ��èѴ���" name="hSearch"></td>
</tr>	
</table>
</form>	


<?if(isset($hSearch) and $objCustomerList->mCount > 0 ){?>

<form name="frm" action="<?=$pCurrentUrl?>" method="POST">
<table border="0" width="95%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	�鹾� 120 ��¡�÷���ѧ����ա�èѴ���
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="95%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="5%" align="center" class="ListTitle">��ö</td>
	<td width="7%" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td width="15%" align="center" class="ListTitle">����</td>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="5%" align="center" class="ListTitle">�Ţ����¹</td>
	<td width="5%" align="center" class="ListTitle">��Сѹ���</td>
	<td width="5%" align="center" class="ListTitle">�ṹ��</td>
	<td width="10%" align="center" class="ListTitle">�ʴ�������</td>

</tr>
	<?
		$i=0;
		forEach($objCustomerList->getItemList() as $objItem) {
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objItem->get_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objItem->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objItem->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objItem->get_car_series_id());
		$objCarSeries->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objInsureCompany = new InsureCompany();
		$objInsureCompany->setInsureCompanyId($objItem->get_insure_company_id());
		$objInsureCompany->load();
		
		$objFundCompany = new FundCompany();
		$objFundCompany->setFundCompanyId($objItem->get_buy_company());
		$objFundCompany->load();
		
		
	?>


<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" ><?=$objItem->get_niti()?></td>	
	<td valign="top" align="center" ><?=$objItem->get_register_year()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_date_verify())?></td>	
	<td valign="top"  ><?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  ><?=$objCustomer->getMobile()?></td>
	<td valign="top"  align="center" ><?=$objItem->get_code()?></td>
	<td valign="top"  ><?=$objInsureCompany->getShortTitle()?></td>
	<td valign="top"  ><?=$objFundCompany->getCode()?></td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=<?=$objItem->get_car_id()?>&hLink=<?=$pCurrentUrlLink?>');"></td>
</tr>

	<?
		$hNitiOld = $objItem->get_niti();
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?}//end check search?>
<br><br>

<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			//window.location = "acardVerifyStep3.php?hDelete="+val+"&hCompanyId=<?=$hCompanyId?>&Day=<?=$Day?>&Month=<?=$Month?>&Year=<?=$Year?>&Day01=<?=$Day01?>&Month01=<?=$Month01?>&Year01=&hSetMonth=10&hSetYear=0&hCarType=0&hCarModel=0&hRegisterYear=0000&hKeywordCar=&hKeyword=&hSaleId=0&hSearch=%A4%E9%B9%CB%D2%A2%E9%CD%C1%D9%C5";
			
			window.location= "<?=$pCurrentUrl?>&hDelete="+val; 
			return false;
		}else{
			return true;
		}	
	}
</script>
<?
	include("h_footer.php")
?>

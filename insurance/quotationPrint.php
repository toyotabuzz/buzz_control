<?
include("common.php");
$objIQ = new InsureQuotation();
$objQL01 = new InsureQuotationItem();
$objQL02 = new InsureQuotationItem();
$objQL03 = new InsureQuotationItem();


if ($hId !="") {
	$objIQ->set_quotation_id($hId);
	$objIQ->load();
	$strMode="Update";
	
	$hCarId = $objIQ->get_car_id();
	
	$arrDate = explode("-",$objIQ->get_date_add());
	$Day02 = $arrDate[2];
	$Month02 = $arrDate[1];
	$Year02 = $arrDate[0];
	
	$objQIList = new InsureQuotationItemList();
	$objQIList->setFilter(" quotation_id = $hId ");
	$objQIList->setPageSize(0);
	$objQIList->setSort(" quotation_item_id ASC ");
	$objQIList->load();

	$i=0;
	forEach($objQIList->getItemList() as $objItem) {

		if($i==0){
			$objQL01->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL01->load();
		}
		
		if($i==1){
			$objQL02->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL02->load();
		}

		if($i==2){
			$objQL03->set_quotation_item_id($objItem->get_quotation_item_id());
			$objQL03->load();
		}
		$i++;
	}
}

if($hCarId != ""){

		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($hCarId);
		$objInsureCar->load();
				
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objInsureCar->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
		$objCarSeries->load();

		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objInsureCar->get_color());
		$objCarColor->load();
		
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
		
}





?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Toyota Buzz  : Control Center System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_report_quotation.css">
</head>

<body>

<table width="700" align="center">
<tr>
	<td>

	
		<table width="98%" cellpadding="0" cellspacing="0">
		<tr>
			<td width="66%" colspan="2">
				<img src="../images/insure_quotation_pic03.gif" alt="" width="400" height="96" border="0">

			
			</td>			
			<td width="33%" align="right"><input type="button" value="�������ʹ��Ҥҹ��" onclick="window.location='quotation_pdf.php?hCarId=<?=$hCarId?>&hId=<?=$hId?>'"><br>�Ţ��� <?=$objIQ->get_quotation_number();?></td>
		</tr>
		</table>

		
		<table width="98%" align="center">
		<tr>
			<td>
				<table width="100%" cellpadding="0"  cellspacing="0">
				<tr>
					<td>ATTN :</td>
					<td><?=$objCustomer->getFirstname()."  ".$objCustomer->getLastname();?></td>
					<td>��</td>
					<td><?=$objCustomer->getHomeTel();?><?if($objCustomer->getHomeTel() != "" and $objCustomer->getMobile() != "") echo ",";?> <?=$objCustomer->getMobile();?></td>
					<td>ῡ��</td>
					<td><?=$objCustomer->getFax();?></td>
					<td align="right">�ѹ��� <?=formatThaiDate(date("Y-m-d"));?></td>
				</tr>
				<tr>
					<td>FROM :</td>
					<td><?=$objMember->getFirstname()." (".$objMember->getNickname();?>)</td>
					<td>��</td>
					<td><?=$objMember->getTel();?></td>
					<td>ῡ��</td>
					<td>0-2375-4771</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="6">�͹حҵ���˹�һ�Сѹ�Թ����� �Ţ��� <?=$objMember->getInsureLicense();?></td>

				</tr>				
				</table>

			
			</td>
		</tr>
		</table>
		<hr size="1">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" >
		<tr>
			<td>
				<table width="100%">
				<tr>
					<td>����¹</td>
					<td><?=$objInsureCar->get_code()?></td>				
					<td>������</td>
					<td><?=$objCarType->getTitle()?></td>
					<td>���</td>
					<td><?=$objCarModel->getTitle()?></td>
					<td>Ẻ</td>
					<td><?=$objCarSeries->getTitle()?></td>
					<td></td>
					<td align="right">�� <?=$objInsureCar->get_register_year()?></td>
				</tr>
				</table>

			</td>
		</tr>
		</table>
		<hr size="1">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" >
		<tr>
			<td align="right">
				��Сѹ����ѹ��� <?=formatThaiDate($objInsureCar->get_date_verify())?>

			</td>
		</tr>
		</table>
		<table width="98%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			
				<table width="100%"  cellpadding="2" cellspacing="0" class="search">
				<tr>
					<td width="61%" class="listDetail" align="center" ><strong>��������´����������ͧ</strong></td>
					<td width="13%" class="listDetail"  align="center"><?=$objQL01->get_text01();?>&nbsp;</td>
					<td width="13%" class="listDetail"  align="center"><?=$objQL02->get_text01();?>&nbsp;</td>
					<td width="13%" class="listDetail"  align="center"><?=$objQL03->get_text01();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  ><strong>�����Ѻ�Դ�ͺ��ͺؤ����¹͡</strong></td>
					<td class="listDetail" align="center" ><strong><?=$objQL01->get_text02();?>&nbsp;</td>
					<td class="listDetail" align="center" ><strong><?=$objQL02->get_text02();?>&nbsp;</td>
					<td class="listDetail"  ><strong><?=$objQL03->get_text02();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  >���ó� : ����������µ�ͪ��Ե��ҧ��� ���͹������ǹ�Թ �ú. / �� / ����</td>
					<td class="listDetail"   align="right"><?=$objQL01->get_text03();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL02->get_text03();?>&nbsp;</td>
					<td class="listDetail"  align="right"><?=$objQL03->get_text03();?>&nbsp;</td>
				</tr>
				<tr>
					<td   class="listDetail"  >�����ͤ��� / ����Թ</td>
					<td class="listDetail"   align="right"><?=$objQL01->get_text04();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL02->get_text04();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL03->get_text04();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  >����������µ�ͷ�Ѿ���Թ���ó�</td>
					<td class="listDetail"   align="right"><?=$objQL01->get_text05();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL02->get_text05();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL03->get_text05();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  ><strong>����������ͧö¹������һ�Сѹ���</strong></td>
					<td class="listDetail" align="center" ><?=$objQL01->get_text06();?>&nbsp;</td>
					<td class="listDetail" align="center" ><?=$objQL02->get_text06();?>&nbsp;</td>
					<td class="listDetail"align="center"  ><?=$objQL03->get_text06();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  >ö�����һ�Сѹ : ����������µ��ö�����һ�Сѹ</td>
					<td class="listDetail"  align="right"><strong><?=$objQL01->get_text07();?>&nbsp;</td>
					<td class="listDetail"  align="right"><strong><?=$objQL02->get_text07();?>&nbsp;</td>
					<td class="listDetail"  align="right" ><strong><?=$objQL03->get_text07();?>&nbsp;</td>
				</tr>
				<tr>
					<td   class="listDetail"  >ö¹���٭��� / �����</td>
					<td class="listDetail"  align="right"><strong><?=$objQL01->get_text08();?>&nbsp;</td>
					<td class="listDetail"  align="right" ><strong><?=$objQL02->get_text08();?>&nbsp;</td>
					<td class="listDetail"  align="right"  ><strong><?=$objQL03->get_text08();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  ><strong>����������ͧ����͡���Ṻ����</strong></td>
					<td class="listDetail" align="center" ><?=$objQL01->get_text09();?>&nbsp;</td>
					<td class="listDetail" align="center" ><?=$objQL02->get_text09();?>&nbsp;</td>
					<td class="listDetail" align="center" ><?=$objQL03->get_text09();?>&nbsp;</td>
				</tr>
				<tr>
					<td   class="listDetail"  >�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����</td>
					<td class="listDetail"   align="right"><?=$objQL01->get_text10();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL02->get_text10();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL03->get_text10();?>&nbsp;</td>
				</tr>
				<tr>
					<td    class="listDetail"  >����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����</td>
					<td class="listDetail"   align="right"><?=$objQL01->get_text11();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL02->get_text11();?>&nbsp;</td>
					<td class="listDetail"   align="right"><?=$objQL03->get_text11();?>&nbsp;</td>
				</tr>
				<tr>
					<td  class="listDetail" style="BORDER-BOTTOM: #7e7e7e 1px solid;">��Сѹ��Ǽ��Ѻ���</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><?=$objQL01->get_text12();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><?=$objQL02->get_text12();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><?=$objQL03->get_text12();?>&nbsp;</td>
				</tr>
				<?if($objIQ->get_text06() != ""){?>
				<tr>
					<td style="BORDER-BOTTOM: #7e7e7e 1px solid;BORDER-LEFT: #7e7e7e 1px solid;"><strong><?=$objIQ->get_text06()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text19();?>&nbsp;</td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text19();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text19();?>&nbsp;</td>
				</tr>
				<?}?>	
				<?if($objIQ->get_text07() != ""){?>
				<tr>
					<td style="BORDER-BOTTOM: #7e7e7e 1px solid;BORDER-LEFT: #7e7e7e 1px solid;"><strong><?=$objIQ->get_text07()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text20();?>&nbsp;</td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text20();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text20();?>&nbsp;</td>
				</tr>
				<?}?>	
				<?if($objIQ->get_text08() != ""){?>
				<tr>
					<td style="BORDER-BOTTOM: #7e7e7e 1px solid;BORDER-LEFT: #7e7e7e 1px solid;" ><strong><?=$objIQ->get_text08()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text21();?>&nbsp;</td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text21();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text21();?>&nbsp;</td>
				</tr>
				<?}?>	
				<?if($objIQ->get_text09() != ""){?>
				<tr>
					<td style="BORDER-BOTTOM: #7e7e7e 1px solid;BORDER-LEFT: #7e7e7e 1px solid;"><strong><?=$objIQ->get_text09()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text22();?>&nbsp;</td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text22();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text22();?>&nbsp;</td>
				</tr>
				<?}?>	
				
				<?if($objIQ->get_text01() != ""){?>
				<tr>
					<td align="center"  ><strong><?=$objIQ->get_text01()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text14();?>&nbsp;</td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text14();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text14();?>&nbsp;</td>
				</tr>
				<?}?>	
				<?if($objIQ->get_text02() != ""){?>
				<tr>
					<td  align="center"  ><strong><?=$objIQ->get_text02()?></strong></td>
					<td  class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text15();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text15();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL03->get_text15();?>&nbsp;</td>
				</tr>
				<?}?>	
				<?if($objIQ->get_text03() != ""){?>
				<tr>
					<td  align="center"   ><strong><?=$objIQ->get_text03()?></strong></td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL01->get_text16();?>&nbsp;</td>
					<td class="listDetail"   style="BORDER-BOTTOM: #7e7e7e 1px solid;" align="right"><strong><?=$objQL02->get_text16();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL03->get_text16();?></strong>&nbsp;</td>
				</tr>	
				<?}?>	
				<?if($objIQ->get_text04() != ""){?>
				<tr>
					<td  align="center"     ><strong><?=$objIQ->get_text04()?></strong></td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL01->get_text17();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL02->get_text17();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL03->get_text17();?></strong>&nbsp;</td>
				</tr>	
				<?}?>	
				<?if($objIQ->get_text05() != ""){?>
				<tr>
					<td  align="center"    ><strong><?=$objIQ->get_text05()?></strong></td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL01->get_text18();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL02->get_text18();?>&nbsp;</td>
					<td class="listDetail"  style="BORDER-BOTTOM: #7e7e7e 1px solid;"  align="right"><strong><?=$objQL03->get_text18();?></strong>&nbsp;</td>
				</tr>
				<?}?>	
				</table><br>
				<?if($objIQ->get_remark() != ""){?>
				<div align="center"><?=nl2br($objIQ->get_remark());?></div>
				<?}?>
				<table width="100%">
				<tr>
					<td align="center">
						<table>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td><strong><u>�͡��÷����㹡�÷ӻ�Сѹ���ö¹��</u></td>
						</tr>
						<tr>
							<td><strong>1) ���ҧ����������� �������͹�������</td>
						</tr>
						<tr>
							<td><strong>2) ������¡�è�����¹ö¹��</td>
						</tr>
						<tr>
							<td><strong>3) ����㺢Ѻ��� 2 ��ҹ (�ó� : �кؼ��Ѻ���)</td>
						</tr>
						</table>					
					</td>
					<td align="center" colspan="3">
						<table>
						<tr>
							<td align="center" height="70" valign="top">���ʴ������Ѻ���</td>
						</tr>
						<tr>
							<td align="center">( <?=$objMember->getFirstname()." ".$objMember->getLastname();?> )</td>
						</tr>
						<tr>
							<td align="center">��ѡ�ҹ��Сѹ���</td>
						</tr>
						</table>

					
					
					</td>
				</tr>							
				</table>
			
				
				<div align="center"></div>
			</td>
		</tr>
		</table>
		
</td>
</tr>
</table>
		
</body>
</html>
<?include "unset_all.php";?>
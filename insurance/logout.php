<?
include("conf.php");

include(PATH_CLASS."clUser.php");
include(PATH_CLASS."clAdmin.php");

session_save_path ("");
session_start();

$objUser = new Admin();

$objUser->logout();
session_destroy();
header("Location: index.php");
exit;
?>
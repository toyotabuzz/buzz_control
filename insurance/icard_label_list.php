<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureTeamList = new InsureTeamList();
$objInsureTeamList->setPageSize(0);
$objInsureTeamList->setSortDefault(" title ASC");
$objInsureTeamList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();


$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objCustomerList = New CustomerList;


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."') OR  ( C.office_tel LIKE '%".$hKeyword."')   )";	
}

if($hKeywordCar !="" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";
	}else{
		$pstrCondition .="  ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";	
	}
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hBuyCompany > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.buy_company = '".$hBuyCompany."' ";
	}else{
		$pstrCondition .=" IC.buy_company = '".$hBuyCompany."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" IC.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$Month02."-%' ";
	}else{
		$pstrCondition .=" IC.date_verify LIKE '%-".$Month02."-%'   ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" IC.company_id = '".$hCompanyId."' ";	
	}
}

if($hTeamId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.team_id = '".$hTeamId."' ";
	}else{
		$pstrCondition .=" IC.team_id = '".$hTeamId."' ";	
	}
}

if($hSetMonth != 0 and $hSetMonth != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$hSetMonth."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '%-".$hSetMonth."-%' ";	
	}
}

if($hSetYear != 0 and $hSetYear != ""){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '".$hSetYear."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '".$hSetYear."-%' ";	
	}
}

if($hRegisterYear > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.register_year = '".$hRegisterYear."' ";
	}else{
		$pstrCondition .=" IC.register_year = '".$hRegisterYear."' ";	
	}
}

if($hCheckc >= "0"){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.checkc = '".$hCheckc."' ";
	}else{
		$pstrCondition .=" IC.checkc = '".$hCheckc."' ";	
	}
}

if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  = '".$hStartDate."' ) ";
		}
	}
}


if($hSource != "all"){
	if($hSource == "import"){

	if($pstrCondition != ""){
		$pstrCondition .="AND IC.source <> '�١��� Buzz' and IC.order_id=0 ";
	}else{
		$pstrCondition .="  IC.source <> '�١��� Buzz' and IC.order_id=0  ";
	}
	
	}else{
	
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.source = '�١��� Buzz' ";
	}else{
		$pstrCondition .="  IC.source = '�١��� Buzz' ";
	}
	
	
	}
	
}


	if($pstrCondition != ""){
		$pstrCondition .="AND (   IC.operate = 'ccard' or  IC.operate = 'booking'    ) ";
	}else{
		$pstrCondition .="  (   IC.operate = 'ccard' or  IC.operate = 'booking'    )  ";
	}
	



}//end hSearch


//echo $pstrCondition;

$objCustomerList = new InsureCarList();
$objCustomerList->setFilter($pstrCondition);
$objCustomerList->setPageSize(PAGE_SIZE);
$objCustomerList->setPage($hPage);
$objCustomerList->setSortDefault(" date_verify ASC");
$objCustomerList->setSort($hSort);
if($hSearch != ""){
$objCustomerList->loadSearch();
}


$sSearchICardLabel = $pstrCondition;
session_register("sSearchICardLabel");

$pCurrentUrl =  "icard_label_list.php?hTeamId=$hTeamId&hSource=$hSource&hBuyCompany=$hBuyCompany&hCompanyId=$hCompanyId&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hSetMonth=$hSetMonth&hSetYear=$hSetYear&hCarType=$hCarType&hCarModel=$hCarModel&hRegisterYear=$hRegisterYear&hKeywordCar=$hKeywordCar&hKeyword=$hKeyword&hSaleId=$hSaleId&hSearch=$hSearch";
//$pCurrentUrl = "acardList.php?hKeywordCar=$hKeywordCar&hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType&Month02=$Month02&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hCarType=$hCarType&hCarModel=$hCarModel&hKeyword=$hKeyword&hSaleId=$hSaleId";
// for paging only (sort resets page viewing)
$pCurrentUrlLink = "icard_label_list.php?hTeamId=".$hTeamId."YYYhSource=".$hSource."YYYhBuyCompany=".$hBuyCompany."YYYhKeywordCar=".$hKeywordCar."YYYhSearch=".$hSearch."YYYhKeyword=".$hKeyword."YYYhType=".$hType."YYYMonth02=".$Month02."YYYDay=".$Day."YYYMonth=".$Month."YYYYear=".$Year."YYYDay01=".$Day01."YYYMonth01=".$Month01."YYYYear01=".$Year01."YYYhCarType=".$hCarType."YYYhCarModel=".$hCarModel."YYYhKeyword=".$hKeyword."YYYhSaleId=".$hSaleId."YYYhPage=".$hPage;

$pageTitle = "6. �к���§ҹ";
$strHead03 = "���Ң������١���";
$pageContent = "6.5 ����� LABEL I-CARD";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>
<tr>
	<td align="right">��� :</td>
	<td><?=$objInsureTeamList->printSelect("hTeamId",$hTeamId,"- �ء��� -");?></td>
</tr>
<tr>
	<td align="right"> ��ѡ�ҹ��Ңͧ��¡�� :</td>
	<td class="small">
     	<input type="hidden" name="hSaleId" value=""><input type="text" name="hSale" size="40" value="">		
	</td>
</tr>	
<tr>
	<td align="right">�ѹ��������ͧ : </td>
	<td>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
			<td class="error"></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="right">�����繪�ǧ��͹/�� : </td>
	<td>
		<select name="hSetMonth">
			<option value="0"> - �ء��͹ -
			<option value="01" <?if($hSetMonth == "01") echo "selected"?>>���Ҥ�
			<option value="02" <?if($hSetMonth == "02") echo "selected"?>>����Ҿѹ��
			<option value="03" <?if($hSetMonth == "03") echo "selected"?>>�չҤ�
			<option value="04" <?if($hSetMonth == "04") echo "selected"?>>����¹
			<option value="05" <?if($hSetMonth == "05") echo "selected"?>>����Ҥ�
			<option value="06" <?if($hSetMonth == "06") echo "selected"?>>�Զع�¹		
			<option value="07" <?if($hSetMonth == "07") echo "selected"?>>�á�Ҥ�
			<option value="08" <?if($hSetMonth == "08") echo "selected"?>>�ԧ�Ҥ�
			<option value="09" <?if($hSetMonth == "09") echo "selected"?>>�ѹ¹¹
			<option value="10" <?if($hSetMonth == "10") echo "selected"?>>���Ҥ�
			<option value="11" <?if($hSetMonth == "11") echo "selected"?>>��Ȩԡ�¹
			<option value="12" <?if($hSetMonth == "12") echo "selected"?>>�ѹ�Ҥ�																					
		</select>
		&nbsp;&nbsp;��&nbsp;&nbsp;
		<?$dateNow = date("Y");?>
		<select name="hSetYear">
			<option value="0"> - �ء�� -
			<?for($i=($dateNow-10);$i<=($dateNow+1);$i++){?>
			<option value="<?=$i?>" <?if($hSetYear==$i) echo "selected"?>><?=$i?>
			<?}?>				
		</select>		
		
	</td>
</tr>
<tr>
	<td align="right">���觷���� :</td>
	<td><input type="radio" value="import" name="hSource" checked> ¡��鹢����Ũҡ CRL <input type="radio" value="crl" name="hSource"> �����Ũҡ CRL <input type="radio" value="all" name="hSource"> �ء������ </td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
<div align="center">�� <?=$objCustomerList->mCount;?> ��¡��</div>
</form>	

<?if($hSearch){?>

<table border="0" width="1240" align="center" cellpadding="2" cellspacing="0">
<tr>
	<form name="hFrmLabel" action="icard_label_pdf.php" method="post" target="_blank" onsubmit="return check_label()">	
	<input type="hidden" name="hSearchICardLabel" value="<?=$sSearchICardLabel?>">
	<td align="right">�ӹǹ��¡�÷������ : <input type="text" name="hStart" size="5">   ������鹨ҡ��¡�÷�� : <input type="text" name="hEnd" size="5">&nbsp;&nbsp; <input type="submit" class="button" value="����� Label"></td>
	</form>
</tr>
<tr>
	<td >
	<?
		$objCustomerList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objCustomerList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objCustomerList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>

</tr>
</table>
<table border="0" width="1240" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="1%" align="center" class="insurance_tab" colspan="2">&nbsp;</td>
	<td width="3%" align="center" class="insurance_tab">��ö</td>
	<td width="10%" align="center" class="insurance_tab">�ѹ��������ͧ</td>
	<td width="20%" align="center" class="insurance_tab">����-���ʡ��/Tel</td>
	<td width="20%" align="center" class="insurance_tab">������ö/Ẻö</td>
	<td width="20%" align="center" class="insurance_tab">�Ţ����¹/�Ţ�ѧ</td>
	<td width="5%" align="center" class="insurance_tab">�Ң�</td>
	<td width="5%" align="center" class="insurance_tab">���</td>
	<td width="5%" align="center" class="insurance_tab">��</td>	
	<td width="10%" align="center" class="insurance_tab">���觷����</td>
	<td width="3%" align="center" class="insurance_tab"></td>	
</tr>
	<?
		$i=0;
		$checkYear =  date("Y")-1;
		forEach($objCustomerList->getItemList() as $objItem) {
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objItem->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objItem->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objItem->get_car_model_id());
		$objCarModel->load();
		
		$objTeam = new InsureTeam();
		$objTeam->setInsureTeamId($objItem->get_team_id());
		$objTeam->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objFund = new FundCompany();
		$objFund->setFundCompanyId($objItem->get_buy_company());
		$objFund->load();
		
		$objCompany = new Company();
		$objCompany->setCompanyId($objItem->get_company_id());
		$objCompany->load();
	?>
	<?if($objItem->get_duplicated() == 'yes'){?>
<tr style="background-color:#ffffcc; cursor:hand" >
	<?}else{?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?}?>
	<td align="center"  valign="top" ><?if($objItem->get_lock_sale() == 1){?><img src="images/lock_icon.jpg" alt="�����Ź��١ Lock ����Ѻ Sale" width="15" height="15" border="0"><?}else{?><?}?></td>	
	<td align="center"  valign="top" ><?if($objItem->get_fix_sale() > 0){?><img src="images/user_icon01.jpg" alt="�����Ź�����١�����Ңͧ Sale" width="15" height="15" border="0"><?}?></td>	
	<td valign="top" align="center" ><?=$objItem->get_register_year()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_date_verify())?></td>	
	<td valign="top"  ><?=$objCustomer->getCustomerTitleIdDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?>
	<br><font color="#ff6633"><?=$objCustomer->getHomeTel()?> <?=$objCustomer->getMobile()?></font>
	</td>
	<td valign="top"  align="center" >
	<?=$objCarType->getTitle()?>
	<br><font color="#ff6633"><?=$objCarModel->getTitle()?></font>
	</td>
	<td valign="top"  align="center" ><?=$objItem->get_code()?> <?if($objItem->get_province_code() != ""){?> ( <?=$objItem->get_province_code()?> ) <?}?><br><font color="#ff6633"><?=$objItem->get_car_number()?></font></td>
	<td valign="top"  align="center" ><?=$objCompany->getShortTitle()?></td>
	<td valign="top"  align="center" ><?=$objTeam->getTitle()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>
	<td valign="top" align="center"><?if($objItem->get_source()==""){ echo"��ѡ�ҹ�ѹ�֡"; }else{echo $objItem->get_source();}?></td>
	<td valign="top" align="center"><input type="button"   value="�ʴ�������" onclick="window.open('acardUpdate.php?hId=<?=$objItem->get_car_id()?>&hLink=<?=$pCurrentUrlLink?>');"></td>
</tr>



	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>


<?}?>

<script>
	function checkDelete(val){
		if(confirm('�س��ͧ��÷���ź������������� ?')){
			window.location = "acard_list.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
	
	
	function checkDuplicate(val,val1){
		if(confirm('�س��ͧ��÷��С�˹������ū�ӫ�͹��¡�ù��������� ?')){
			window.location = "acard_list.php?hDuplicated="+val1+"&hDuplicate="+val;
			return false;
		}else{
			return true;
		}	
	}
	
</script>
<?
	include("h_footer.php")
?>
<script>

	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});	
	
	
	function check_label(){
		if(document.hFrmLabel.hStart.value==""){
			alert("��س��кبӹǹ����ͧ��þ����");
			return false;
		}

		if(document.hFrmLabel.hEnd.value==""){
			alert("��س��кبش������鹷���ͧ��èо����");
			return false;
		}

		return true;
	}

</script>
<?include "unset_all.php";?>
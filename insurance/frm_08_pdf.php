<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,50,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


			
				$pdf->AddPage();
				$pdf->SetLeftMargin(80);

				$objInsure = new Insure();
				$objInsure->set_insure_id($hId);
				$objInsure->load();
				
				$objCompany = new Company();
				$objCompany->setCompanyId($sCompanyId);
				$objCompany->load();
				
				$objMember = new Member();
				$objMember->setMemberId($objInsure->get_sale_id());
				$objMember->load();
				
				$objSale = new Member();
				$objSale->setMemberId($objInsure->get_sale_id());
				$objSale->load();				
				
				$objInsureCar = new InsureCar();
				$objInsureCar->set_car_id($objInsure->get_car_id());
				$objInsureCar->load();
				
				$objCustomer = new InsureCustomer();
				$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
				$objCustomer->load();
				
				$objCarType = new CarType();
				$objCarType->setCarTypeId($objInsureCar->get_car_type());
				$objCarType->load();
				
				$objCarModel = new CarModel();
				$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
				$objCarModel->load();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objInsureCar->get_color());
				$objCarColor->load();
				
				$hStartDate= formatThaiDate($objInsure->get_date_protect());
				$hStartYear = substr($hStartDate,-4);
				$hEndYear = $hStartYear+1;
				$hEndDate = substr($hStartDate,0,(strlen($hStartDate)-4)).$hEndYear;
				
				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				
				$objInsureItem = new InsureItem();
				$objInsureItem->loadByCondition(" insure_id = $hId and payment_no=1 ");
				
				$objF = new InsureForm();
				$objF->loadByCondition(" insure_id= $hId and frm_type= '0801'  ");
				
				$pdf->SetFont('angsa','B',14);	
				$strtext = "INS-".date("ymd")."-".$hId;
				$pdf->SetXY(50,80+$hHeight);
				$pdf->Write(10,$strtext);


				$strtext = $objF->get_code();
				$pdf->SetXY(50,1000+$hHeight);
				$pdf->Write(10,$strtext);
				

				$strtext = formatThaiDate(date("Y-m-d"));
				$pdf->SetXY(400,80+$hHeight);
				$pdf->Write(10," �ѹ��� ".$strtext);
				

				$strtext = "����ͧ";
				$pdf->SetXY(50,140+$hHeight);
				$pdf->Write(10,$strtext);
				

				$strtext = "�������Ф�����»�Сѹ���ö¹��";
				$pdf->SetXY(80,140+$hHeight);
				$pdf->Write(10,$strtext);
				

				$strtext = "���¹";
				$pdf->SetXY(50,170+$hHeight);
				$pdf->Write(10,$strtext);
				

				if($objCustomer->getName03() != ""){
					$strtext = $objCustomer->getName03();
				}else{
					$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
				}
				$pdf->SetXY(80,170+$hHeight);
				$pdf->Write(10,$strtext);
				
				if($hAddressType == 1){
					$hAddress1 = $objCustomer->getAddressLabel();
					if($objCustomer->mProvince == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon." ࢵ ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
					}else{
						$hAddress2 = "�Ӻ� ".$objCustomer->mTumbon." ����� ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
					}					
				}
				if($hAddressType == 2){
					$hAddress1 = $objCustomer->getAddressLabel01();
					if($objCustomer->mProvince01 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon01." ࢵ ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon01." ����� ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
					}					
				}
				if($hAddressType == 3){
					$hAddress1 = $objCustomer->getAddressLabel02();
					if($objCustomer->mProvince02 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon02." ࢵ ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon02." ����� ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
					}					
				}
				if($hAddressType == 4){
					$hAddress1 = $objCustomer->getAddressLabel03();
					if($objCustomer->mProvince03 == "��ا෾��ҹ��"){
						$hAddress2 = "�ǧ ".$objCustomer->mTumbon03." ࢵ ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
					}else{
						$hAddress2 =  "�Ӻ� ".$objCustomer->mTumbon03." ����� ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
					}					
				}

				$pdf->SetXY(80,185+$hHeight);
				$pdf->Write(15,$hAddress1);
				$pdf->SetXY(80,205+$hHeight);
				$pdf->Write(15,$hAddress2);
				
				if($objInsure->get_k_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsure->get_k_prb_id());
				$objB->load();
				$hInsureCompany = $objB->getTitle();
				}				
				
				$hHeight =$hHeight+30;
				
				$bodytag = " <strong>          �������ҹ���� ���Ѻ����������ͧ ������������Сѹ���   <u><strong>         ".$hInsureCompany."       </u> <br> ����Ѻö    <u>  ".$objCarType->getTitle()."    ".$objCarModel->getTitle()."   </u>  ����¹  <u> ".$objInsureCar->get_code()."  </u>        ���������Ţ���   <u>     ".$objInsure->get_k_number()."      </u> <br> ������ͧ������ѹ���      <u>    ".formatThaiDate($objInsure->get_k_start_date())."   </u>     �֧     <u>     ".formatThaiDate($objInsure->get_k_end_date())."    </u>      ��˹������Թ����� <br> <u>    ".formatThaiDate($objInsure->get_k_start_date())." </u>   �ӹǹ�Թ  <u>  ".number_format($objInsure->get_k_num13(),2)."    </u> �ҷ (  <u>     ".currencyThai01($objInsure->get_k_num13())." </u> ) ���</strong>";
				
				$pdf->SetXY(82,210+$hHeight);
				$pdf->WriteHTML($bodytag);
				
				$pdf->SetFont('angsa','B',14);	
				$strtext= "          �Ѵ��� ����ѷ �ѧ������Ѻ������¢�ҧ�������ѧ���Ѻ�������ú��ǹ �֧������ҹ���Թ��ê��Ф������ \n�ӹǹ�ѧ��������ú��ǹ���͵Դ������˹�ҷ��  ��: ".$objLabel->getText04()."  ".$objSale->getFirstname()." (".$objSale->getNickname().")"." �·ѹ�� ";
				$pdf->SetXY(82,310+$hHeight);
				$pdf->WriteHTML($strtext);
				
				$strtext= "          ��駹���ҡ��ҹ�ԡ�� ���Դ��͡�Ѻ���������Թ��ê��е����������´��ҧ�� ����ѷ� �ӵ�ͧ��¡��ԡ ����������ͧ�����������ѧ����� ���� 7 �ѹ �Ѻ������ѹ����ҹ���Ѻ ˹ѧ��ͩ�Ѻ��� ���Ծѡ��ͧ �͡����ǡ�͹ �����������˹ѧ��͹�� ��˹ѧ��ͺ͡�������ԡ�ѭ�ҷѹ�� ����ͤú��˹����˹ѧ��͹��";
				$pdf->SetXY(80,380+$hHeight);
				$pdf->WriteHTML($strtext);
				

				$strtext= "          �֧���¹�����ͷ�Һ ����ô���Թ��õ��˹ѧ��͹���·ѹ��";
				$pdf->SetXY(80,460+$hHeight);
				$pdf->WriteHTML($strtext);

				$pdf->SetFont('angsa','B',14);
				$pdf->SetXY(400,600);
				//$pdf->Write(15,"���ʴ������Ѻ���");
				$pdf->Cell(150,20,"���ʴ������Ѻ���",0,0,"C");
				
				$pdf->SetFont('angsa','B',14);	
				$pdf->SetXY(400,650);
				$pdf->Cell(150,20,"(".$objLabel->getText07().")",0,0,"C");
				//$pdf->Write(15,"(".$objSale->getFirstname()." ".$objSale->getLastname().")");
				
				$pdf->SetFont('angsa','B',14);
				$pdf->SetXY(400,675);
				$pdf->Cell(150,20,$objLabel->getText09(),0,0,"C");
				//$pdf->Write(15,"��ѡ�ҹ��Сѹ���ö¹��");
				

				





	Unset($objCustomerList);
	$pdf->Output();	
	?>
	<?include "unset_all.php";?>
	<script>
	//window.close();
	</script>	
<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objMM = new Member();
$objMM->setMemberId($sMemberId);
$objMM->load();

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objCompanyList = new CompanyList();
$objCompanyList->setPageSize(0);
$objCompanyList->setSortDefault(" title ASC");
$objCompanyList->load();

$objInsureTeamList = new InsureTeamList();
$objInsureTeamList->setPageSize(0);
$objInsureTeamList->setSortDefault(" title ASC");
$objInsureTeamList->load();


$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .=" AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .=" AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .=" AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .=" AND P.k_start_date LIKE '$Year02-$Month02-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '$Year02-$Month02-%'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .=" AND P.k_start_date LIKE '%-".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '%-".$Month02."-%'   ";	
		}
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date  >= '".$hStartDate."' AND P.insure_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.insure_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.insure_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .=" AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.p_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_broker_id = '".$hInsureBrokerId."' ) ";
		}
}


if($hCompany != "" AND $hCompany != 0){
	if($pstrCondition != ""){
		$pstrCondition.= " AND P.company_id =  ".$hCompany;
	}else{
		$pstrCondition.= " P.company_id =  ".$hCompany;
	}
}

if($hInsureTeam != "" AND $hInsureTeam != 0){
	if($pstrCondition != ""){
		$pstrCondition.= " AND P.team_id =  ".$hInsureTeam;
	}else{
		$pstrCondition.= " P.team_id =  ".$hInsureTeam;
	}
}
  
  
  
if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.p_prb_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_prb_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hFrmType != "" ){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( I.frm_type  = '".$hFrmType."' ) ";
		}else{
			$pstrCondition .=" ( I.frm_type = '".$hFrmType."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.k_prb_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_prb_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}

$sesConditionS8 = $pstrCondition;
session_register("sesConditionS8",$sesConditionS8);

}//end hSearch

$objM = new Member();
$objM->setMemberId($sMemberId);
$objM->load();


if($sesConditionS8 == ""){
	$pstrCondition= "  ( sup_approve='���͹��ѵ�' OR admin_approve = '���͹��ѵ�' ) AND frm_type <> '0207'  AND P.sale_id = $sMemberId   ";
}else{
	$pstrCondition= "  ( sup_approve='���͹��ѵ�' OR admin_approve = '���͹��ѵ�' ) AND frm_type <> '0207'  AND P.sale_id = $sMemberId   AND ".$sesConditionS8;
}

echo $pstrCondition;

$objInsureFrmList = new InsureFormList();
$objInsureFrmList->setFilter($pstrCondition);
$objInsureFrmList->setPageSize(PAGE_SIZE);
$objInsureFrmList->setPage($hPage);
$objInsureFrmList->setSortDefault(" k_start_date ASC ");
$objInsureFrmList->setSort($hSort);
$objInsureFrmList->loadList();

$pCurrentUrl = "k_approve1.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "2. �к��ҹ���";
//$strHead03 = "���Ң������١���";
$pageContent = "2.8 ����������ҹ���͹��ѵ�";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>

<SCRIPT language=javascript>

function populate03(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCompanyList->getItemList() as $objItemCompany) {
			if($i==1) $hCompany1 = $objItemCompany->getCompanyId();
		?>
			var individualCategoryArray<?=$objItemCompany->getCompanyId();?> = new Array(
		<?
			$objInsureTeam = new InsureTeamList();
			$objInsureTeam->setFilter(" P.company_id = ".$objItemCompany->getCompanyId());
			$objInsureTeam->setPageSize(0);
			$objInsureTeam->setSortDefault(" title ASC");
			$objInsureTeam->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objInsureTeam->getItemList() as $objItemInsureTeam) {
				$text.= " \"('".$objItemInsureTeam->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCompanyList->getItemList() as $objItemCompany) {?>
			var individualCategoryArrayValue<?=$objItemCompany->getCompanyId();?> = new Array(
		<?
			$objInsureTeam = new InsureTeamList();
			$objInsureTeam->setFilter(" P.company_id = ".$objItemCompany->getCompanyId());
			$objInsureTeam->setPageSize(0);
			$objInsureTeam->setSortDefault(" title ASC");
			$objInsureTeam->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objInsureTeam->getItemList() as $objItemInsureTeam) {
				$text.= " \"('".$objItemInsureTeam->getInsureTeamId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCompanyList->getItemList() as $objItemCompany) {?>
	if (selected == '<?=$objItemCompany->getCompanyId()?>') 	{
		
		while (individualCategoryArray<?=$objItemCompany->getCompanyId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCompany->getCompanyId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCompany->getCompanyId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCompany->getCompanyId()?>[i]);			
		}
	}			
<?}?>

}


</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�յ������ : </td>
	<td>
			<table>
			<tr>
				<td>
					<select name="Month02">
					<option value="00" <?if($Month02 == "00") echo "selected"?>>- �ء��͹ -
					<option value="01" <?if($Month02 == "01") echo "selected"?>>���Ҥ�
					<option value="02" <?if($Month02 == "02") echo "selected"?>>����Ҿѹ��
					<option value="03" <?if($Month02 == "03") echo "selected"?>>�չҤ�
					<option value="04" <?if($Month02 == "04") echo "selected"?>>����¹
					<option value="05" <?if($Month02 == "05") echo "selected"?>>����Ҥ�
					<option value="06" <?if($Month02 == "06") echo "selected"?>>�Զع�¹		
					<option value="07" <?if($Month02 == "07") echo "selected"?>>�á�Ҥ�
					<option value="08" <?if($Month02 == "08") echo "selected"?>>�ԧ�Ҥ�
					<option value="09" <?if($Month02 == "09") echo "selected"?>>�ѹ¹¹
					<option value="10" <?if($Month02 == "10") echo "selected"?>>���Ҥ�
					<option value="11" <?if($Month02 == "11") echo "selected"?>>��Ȩԡ�¹
					<option value="12" <?if($Month02 == "12") echo "selected"?>>�ѹ�Ҥ�																					
				</select>				
				</td>
				<td>-</td>
				<td>
				<select name="Year02">
					<option value="0">- �ء�� -
					<?for($i=2007;$i<=date("Y")+3;$i++){?>
					<option value="<?=$i?>"><?=$i?>
					<?}?>				
				</select>				
				</td>
				<td class="error"></td>
			</tr>
			</table>
	</td>
</tr>		

<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	
<tr>
	<td align="right"> �Ҥ��Ѥ�� :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId","","����к�");?></td>
			<td>�Ţ��������</td>
			<td><input type="text" name="hKeywordPrb" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> �Ҥ�ѧ�Ѻ :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId01","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId01","","����к�");?></td>
			<td>�Ţ��������</td>
			<td><input type="text" name="hKeywordKom" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> ����������� :</td>
	<td class="small">
		<select name="hFrmType">
			<option>
			<option value="0601" >���������¹�ŧ
			<option value="0701">��¡��ԡ�.��Сѹ���
			<option value="0801">��¡��ԡ�١���
			<option value="0901">�駧ҹ�١���
		</select>
	</td>
</tr>	

<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureFrmList->mCount;?> ��¡��</div>


<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureFrmList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureFrmList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureFrmList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ�����</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">������</td>	
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ң�</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">���</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�յ������</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ������ͧ</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">��������</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">����</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ţ����¹</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��Ǩ�ͺ</td>
</tr>
	<?
		$i=0;
		forEach($objInsureFrmList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;
		
		$objCompany = new Company();
		$objCompany->setCompanyId($objInsureCar->get_company_id());
		$objCompany->load();
		
		$objInsureTeam = new InsureTeam();
		$objInsureTeam->setInsureTeamId($objInsureCar->get_team_id());
		$objInsureTeam->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsure->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();

		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objInsure->get_p_prb_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objInsure->get_k_prb_id());
		$objKomCompany->load();
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=(($hPage-1)*50)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_form_date())?><?=$objItem->get_insure_form_id()?></td>	
	<td valign="top"   >
	<?=$objItem->get_insure_form_id();?>
	<?if($objItem->get_frm_type() == "0601"){?>���������¹�ŧ<?}?>
	<?if($objItem->get_frm_type() == "0701"){?>��¡��ԡ�.��Сѹ���<?}?>
	<?if($objItem->get_frm_type() == "0801"){?>��¡��ԡ�١���<?}?>
	<?if($objItem->get_frm_type() == "0901"){?>�駧ҹ�١���<?}?>
	</td>
	<td valign="top"  align="center" ><?=$objCompany->getShortTitle()?></td>
	<td valign="top"  align="center" ><?=$objInsureTeam->getTitle()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>

	<td valign="top" align="center" ><?=$objInsure->get_p_year()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objInsure->get_date_protect())?></td>	
	<td valign="top" ><?=$objKomCompany->getTitle()?></td>		
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?></td>
	<td  valign="top" align="center"><input type="button" value="�ʴ���¡��" name="hDisplay" onclick="window.location='step06List.php?hInsureId=<?=$objItem->get_insure_id()?>';"></td>	

</tr>
	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?
	include("h_footer.php")
?>
<script>
	function confirm_delete(val){
		if(confirm("�س��ͧ��÷���ź�����Ź��������� ?")){
			window.location='k_approve1.php?hDelete='+val;
			return true;
		}	
	}
</script>
<?include "unset_all.php";?>
<?
include "common.php";
$objMember = new Member();

$hSearchKeyword01=str_replace( "@@@@@", "=", $hSearchKeyword);
$hSearchKeyword01=str_replace( "?????", "&", $hSearchKeyword01);

$objModuleList = new ModuleList();
$objModuleList->setFilter(" code ='insurance' ");
$objModuleList->setPageSize(0);
$objModuleList->setSortDefault(" title ASC");
$objModuleList->load();

$objDepartmentList = new DepartmentList();
$objDepartmentList->setFilter(" code = 'INS' ");
$objDepartmentList->setPageSize(0);
$objDepartmentList->setSortDefault(" title ASC");
$objDepartmentList->load();

$objTeamList = new InsureTeamList();
$objTeamList->setPageSize(0);
$objTeamList->setSortDefault(" title ASC");
$objTeamList->load();

$objCompanyList = new CompanyList();
$objCompanyList->setPageSize(0);
$objCompanyList->setSortDefault(" title ASC");
$objCompanyList->load();

if (empty($hSubmit)) {
	if ($hId != "") {
		$objMember->setMemberId($hId);
		$objMember->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objMember->getStartDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate01 = explode("-",$objMember->getEndDate());
		$Day01 = $arrDate01[2];
		$Month01 = $arrDate01[1];
		$Year01 = $arrDate01[0];

	} else {
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {

            $objMember->setMemberId($hMemberId);
			$objMember->setEmpCode($hEmpCode);
			$objMember->setFirstname($hFirstname);
			$objMember->setLastname($hLastname);
			$objMember->setNickname($hNickname);
			$objMember->setEmail($hEmail);
			$objMember->setTeam($hTeam);
			$objMember->setCompanyId($hCompanyId);
			$objMember->setIDCard($hIDCard);
			$objMember->setTel($hTel);
			$objMember->setAddress($hAddress);
						
			$hStartDate = $Year."-".$Month."-".$Day;
			$objMember->setStartDate($hStartDate);
			
			$hEndDate = $Year01."-".$Month01."-".$Day01;
			$objMember->setEndDate($hEndDate);
			
			$objMember->setUsername($hUsername);
			$objMember->setPassword($hPassword);
			$objMember->setPassword1($hPassword);
			$objMember->setStatus($hStatus);
			$objMember->setDepartment($hDepartment);
			$objMember->setPosition($hPosition);
			$objMember->setLevel($hLevel);
			$objMember->setDefaultProgram($hDefaultProgram);
			$pasrErr = $objMember->check($strMode);

			if (!isset($hRemoveImage0)){
				$objMember->setPicture($hImage0);
			}else{
				$objMember->deletePicture($hImage0);
			}			
			
			//check duplicate
			$objM = new Member();
			if($strMode== "Add"){
				if($objM->loadByCondition(" firstname='$hFirstname'  and lastname='$hLastname'  and status=1 ")){
					$pasrErr["duplicate"] = "��ª��ͫ�ӫ�͹��سҵ�Ǩ�ͺ";
				
				}
			}else{
				if($objM->loadByCondition(" firstname='$hFirstname'  and lastname='$hLastname'  and status=1 and  member_id <> $hId ")){
					$pasrErr["duplicate"] = "��ª��ͫ�ӫ�͹��سҵ�Ǩ�ͺ";
				
				}
			}			
			
			If ( Count($pasrErr) == 0 ){
			
				for ($j=0;$j<1;$j++){
				if ($hPhoto_name[$j] != "") {
					//$pic = $hPhoto_name[$j].date("YmdHis").$hPhoto_type[$i];
					$pic = date("YmdHis").$j.substr($hPhoto_name[$j],-4);
					switch ($j) {
						case 0:
							$objMember->deletePicture($hImage0);
							$objMember->setPicture($pic);
							break;
					}//end switch
					$objMember->uploadPicture($hPhoto[$j],$pic);
				}//end if
				}// end for				
			
				if ($strMode=="Update") {
					if( $hCodeTemp == "" OR $hDepartmentTemp != $hDepartment OR ($hTeamTemp != $hTeam) ){
						//generate code
						$objDepartment = new Department();
						$objDepartment->setDepartmentId($hDepartment);
						$objDepartment->load();
						$arrCode[0] = $objDepartment->getCode();
						
						$arrCode[1] = substr($Year,-2);
						
						$objTeam = new Team();
						$objTeam->setTeamId($hTeam);
						if($objTeam->load()){
							$arrCode[2] = $objTeam->getCode();
						}else{
							$arrCode[2] = "00";
						}
						
						$objMemberCheck = new Member();
						$auto = $objMemberCheck->loadMaxByCondition(" code LIKE '".$arrCode[0].$arrCode[1].$arrCode[2]."%'  ");
						if($auto != ""){
							$auto = substr($auto,-2)*1;
						}
						$auto = $auto+1;
						if($auto < 10) $auto="0".$auto;	
						$arrCode[3] = $auto;
						$code =$arrCode[0].$arrCode[1].$arrCode[2].$arrCode[3];
						$objMember->setCode($code);
						//end generate code	
						
						$strText = "              ".date("d-m-Y")." : ����¹���ʨҡ $hCodeTemp �� $code  ";
						$objMember->setRemark($hRemark.$strText);
						
					}
					
					$objMemberInsure = new Member();
					$objMemberInsure->setMemberId($hId);
					$objMemberInsure->setInsureStatus($hInsureStatus);
					$objMemberInsure->updateInsure();
					
					$objMember->update();
					
					
					//update insure car
					$objIC = new InsureCar();
					$objIC->updateSql(" update t_insure_car set team_id= '$hTeam' where sale_id='$hId'  ");
					
					//echo " update t_insure_car set team_id= '$hTeam' where sale_id='$hId'  ";


					
					for($i=0;$i<$hCountModule;$i++){
						$objMemberModule = new MemberModule();
						$objMemberModule->setMemberModuleId($hMemberModuleId[$i]);
						$objMemberModule->setMemberId($hId);
						$objMemberModule->setModuleType($hModuleType[$i]);
						$objMemberModule->setStatus($_POST["hAccess".$i]);
						$objMemberModule->setModuleId($hModuleId[$i]);
						$objMemberModule->setModuleCode($hModuleCode[$i]);
						if(isset($_POST["hFunctionAdd_".$i])){$arrAccess[0]=1;}else{$arrAccess[0]=0;}
						if(isset($_POST["hFunctionEdit_".$i])){$arrAccess[1]=1;}else{$arrAccess[1]=0;}
						if(isset($_POST["hFunctionDelete_".$i])){$arrAccess[2]=1;}else{$arrAccess[2]=0;}
						if(isset($_POST["hFunctionReport_".$i])){$arrAccess[3]=1;}else{$arrAccess[3]=0;}
						if(isset($_POST["hFunctionView_".$i])){$arrAccess[4]=1;}else{$arrAccess[4]=0;}
						if(isset($_POST["hFunctionExport_".$i])){$arrAccess[5]=1;}else{$arrAccess[5]=0;}
						$hAccess = implode(":",$arrAccess);	
						$objMemberModule->setAccess($hAccess);
						if($hMemberModuleId[$i] != ""){
							$objMemberModule->update();
						}else{
							$objMemberModule->add();
						}

					}
				} else {
					//generate code
					$objDepartment = new Department();
					$objDepartment->setDepartmentId($hDepartment);
					$objDepartment->load();
					$arrCode[0] = $objDepartment->getCode();
					
					$arrCode[1] = substr($Year,-2);
					
					$objTeam = new Team();
					$objTeam->setTeamId($hTeam);
					if($objTeam->load()){
						$arrCode[2] = $objTeam->getCode();
					}else{
						$arrCode[2] = "00";
					}
					
					$objMemberCheck = new Member();
					$auto = $objMemberCheck->loadMaxByCondition(" code LIKE '".$arrCode[0].$arrCode[1].$arrCode[2]."%'  ");
					if($auto != ""){
						$auto = substr($auto,-2)*1;
					}
					$auto = $auto+1;
					if($auto < 10) $auto="0".$auto;	
					$arrCode[3] = $auto;
					$code =$arrCode[0].$arrCode[1].$arrCode[2].$arrCode[3];
					$objMember->setCode($code);
					//end generate code				
				
				
					$pId=$objMember->add();
					for($i=0;$i<$hCountModule;$i++){
						$objMemberModule = new MemberModule();
						$objMemberModule->setMemberId($pId);
						$objMemberModule->setModuleType($hModuleType[$i]);
						$objMemberModule->setStatus($_POST["hAccess".$i]);
						$objMemberModule->setModuleId($hModuleId[$i]);
						$objMemberModule->setModuleCode($hModuleCode[$i]);
						if(isset($_POST["hFunctionAdd_".$i])){$arrAccess[0]=1;}else{$arrAccess[0]=0;}
						if(isset($_POST["hFunctionEdit_".$i])){$arrAccess[1]=1;}else{$arrAccess[1]=0;}
						if(isset($_POST["hFunctionDelete_".$i])){$arrAccess[2]=1;}else{$arrAccess[2]=0;}
						if(isset($_POST["hFunctionReport_".$i])){$arrAccess[3]=1;}else{$arrAccess[3]=0;}
						if(isset($_POST["hFunctionView_".$i])){$arrAccess[4]=1;}else{$arrAccess[4]=0;}
						if(isset($_POST["hFunctionExport_".$i])){$arrAccess[5]=1;}else{$arrAccess[5]=0;}
						$hAccess = implode(":",$arrAccess);						
						$objMemberModule->setAccess($hAccess);
						$objMemberModule->add();
					}
				}
				unset ($objMember);
				$hSearchKeyword=str_replace( "@@@@@", "=", $hSearchKeyword);
				$hSearchKeyword=str_replace( "?????", "&", $hSearchKeyword);

				
				header("location:memberList.php?$hSearchKeyword");
				exit;
			}else{
				$objMember->init();
			}//end check Count($pasrErr)
		}
}

$pageTitle = "1. �����ҹ��к�";
$pageContent = "1.1 �ѹ�֡�����ż����ҹ��к� ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
		if (document.forms.frm01.hCompanyId.value=="0")
		{
			alert("��س��кغ���ѷ");
			document.forms.frm01.hCompanyId.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hEmpCode.value=="")
		{
			alert("��س��к����ʾ�ѡ�ҹ");
			document.forms.frm01.hEmpCode.focus();
			return false;
		}

		if (document.forms.frm01.hFirstname.value=="")
		{
			alert("��س��кت���");
			document.forms.frm01.hFirstname.focus();
			return false;
		} 			
	
		if (document.forms.frm01.hLastname.value=="")
		{
			alert("��س��кع��ʡ��");
			document.forms.frm01.hLastname.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hNickname.value=="")
		{
			alert("��س��кت������");
			document.forms.frm01.hNickname.focus();
			return false;
		} 				
	
		if (document.forms.frm01.hIDCard.value=="")
		{
			alert("��س��кغѵû�ЪҪ�");
			document.forms.frm01.hIDCard.focus();
			return false;
		} 				

		if (document.forms.frm01.hDepartment.options[frm01.hDepartment.selectedIndex].value=="0")
		{
			alert("��س����͡Ἱ�");
			document.forms.frm01.hDepartment.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hPosition.options[frm01.hPosition.selectedIndex].value=="0")
		{
			alert("��س����͡���˹�");
			document.forms.frm01.hPosition.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hDefaultProgram.options[frm01.hDefaultProgram.selectedIndex].value=="0")
		{
			alert("��س����͡������������");
			document.forms.frm01.hDefaultProgram.focus();
			return false;
		} 	
	
		document.forms.frm01.submit();
		
	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<form name="frm01" action="memberUpdate.php?hId=<?=$hId?>" enctype="multipart/form-data"  method="POST" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hMemberId" value="<?=$hId?>">
	  <input type="hidden" name="hSearchKeyword" value="<?=$hSearchKeyword?>">	
 
						  <table width="90%"  align="center">
						  	<tr>
							<td colspan="2" ><strong>Member  Information</strong><hr size="1" color="#F4EFDF"></td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right" width="20%">ʶҹС����ҹ:</td>
							<td ><input type="checkbox" name="hStatus" value="1" <?if($objMember->getStatus() == 1) echo "checked"?>> �礶�ҵ�ͧ���͹��ѵԡ����ҹ</td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right" width="20%">�Ѵ��� :</td>
							<td ><input type="checkbox" name="hInsureStatus" value="1" <?if($objMember->getInsureStatus() == 1) echo "checked"?>> �礶�ҵ�ͧ���͹��ѵԨѴ���</td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">�Ң�:</td>
							<td >
								<?=$objCompanyList->printSelectScript("hCompanyId",$objMember->getCompanyId(),"��س����͡"," onchange='check_team();'  ");?>
						    </td>
							</tr>						
						  <?if($strMode == "Update"){?>
							<tr>
						    <td class="bodyhead" align="right" width="20%">����:</td>
							<td >
						        <input type="text" disabled name="hCode" size="12" maxlength="9" value="<?=$objMember->getCode();?>">
						    </td>
							</tr>
							<?}?>	
							<tr>
						    <td class="bodyhead" align="right" width="20%">���ʾ�ѡ�ҹ:</td>
							<td >
						        <input type="text" name="hEmpCode" size="30" value="<?=$objMember->getEmpCode();?>">
						    </td>
							</tr>							
							<tr>
						    <td class="bodyhead" align="right" width="20%">����:</td>
							<td >
						        <input type="text" name="hFirstname" size="30" value="<?=$objMember->getFirstname();?>">
						        <?printError($pasrErr["firstname"]);?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">���ʡ��:</td>
							<td >
						        <input type="text" name="hLastname" size="30" value="<?=$objMember->getLastname();?>">
						        <?printError($pasrErr["lastname"]);?>
						    </td>
							</tr>			
							<tr>
						    <td class="bodyhead" align="right">�������:</td>
							<td >
						        <input type="text" name="hNickname" size="30" value="<?=$objMember->getNickname();?>">						       
						    </td>
							</tr>					
							<tr>
						    <td class="bodyhead" align="right">�ѵû�ЪҪ�:</td>
							<td >
						        <input type="text" maxlength="13" name="hIDCard" size="30" value="<?=$objMember->getIDCard();?>">
						    </td>
							</tr>							
							<tr>
						    <td class="bodyhead" align="right">Email:</td>
							<td >
						        <input type="text" name="hEmail" size="30" value="<?=$objMember->getEmail();?>">
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">Ἱ�:</td>
							<td >
								<?=$objDepartmentList->printSelectScript("hDepartment",$objMember->getDepartment(),"��س����͡"," onchange='check_team();'  ");?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">���˹�:</td>
							<td >
								<select name="hPosition">
									<option value="0">��س����͡
									<option value="1" <?if($objMember->getPosition() == 1) echo "selected"?>>������ü��Ѵ���
									<option value="2" <?if($objMember->getPosition() == 2) echo "selected"?>>�ͧ������ü��Ѵ���
									<option value="3" <?if($objMember->getPosition() == 3) echo "selected"?>>���Ѵ���
									<option value="4" <?if($objMember->getPosition() == 4) echo "selected"?>>�����¼��Ѵ���
									<option value="5" <?if($objMember->getPosition() == 5) echo "selected"?>>���Ѵ����Ң�
									<option value="6" <?if($objMember->getPosition() == 6) echo "selected"?>>���˹��Ἱ�/���
									<option value="7" <?if($objMember->getPosition() == 7) echo "selected"?>>��ѡ�ҹ���
									<option value="8" <?if($objMember->getPosition() == 8) echo "selected"?>>��ѡ�ҹ��á��
									
								</select>
						    </td>
							</tr>
							<tr  id="d1" >
						    <td class="bodyhead" align="right">������:</td>
							<td >
						        <?=$objTeamList->printSelect("hTeam",$objMember->getTeam(),"��س����͡");?> <font class="error">��س����͡㹡ó��� Sale</font>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">������������:</td>
							<td >
								<?=$objModuleList->printSelect("hDefaultProgram",$objMember->getDefaultProgram(),"��س����͡");?> ������������˹���á Login
						    </td>
							</tr>
						    <tr>
						    <td class="title" align="right">�ٻ����:</td>
						    <td >	
								<?if (trim($objMember->getPicture()) != "") echo "<img src='".gDIR_IMG.$objMember->getPicture()."'><br>"?>	  	
								<input type="file" name="hPhoto[0]" size="50"><font color=red><?=$pasrErr[photo0];?></font>
								<input type="hidden" name="hImage0" size="50" value="<?=$objMember->getPicture();?>">
						    </td>
							</tr>								
							<tr>
						    <td class="bodyhead" align="right">�ѹ���������ҹ:</td>
							<td >
								  	<table cellspacing="0" cellpadding="0">
									<tr>
										<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
										<td>-</td>
										<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
										<td>-</td>
										<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
										<td>&nbsp;</td>
										<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
									</tr>
									</table>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">�ѹ������͡:</td>
							<td >
								  	<table cellspacing="0" cellpadding="0">
									<tr>
										<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
										<td>-</td>
										<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
										<td>-</td>
										<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
										<td>&nbsp;</td>
										<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
									</tr>
									</table>
						    </td>
							</tr>
							
							<tr>
						    <td class="bodyhead" align="right">Username:</td>
							<td >
						        <input type="text" name="hUsername" size="20" maxlength="10" value="<?=$objMember->getUsername();?>">
						        <?printError($pasrErr[username]);?>
						    </td>
							</tr>
							<tr>
						    <td class="bodyhead" align="right">Password:</td>
							<td >
						        <input type="text" name="hPassword" size="20" maxlength="10" value="<?=$objMember->getPassword();?>">
						        <?printError($pasrErr[password]);?>
						    </td>
							</tr>
						  	<tr>
							<td colspan="2" ><br><br><strong>�Է�ԡ�����������ѡ</strong><hr size="1" color="#F4EFDF"></td>
							</tr>
							<tr>						   
							<td colspan="2" >
								<table width="100%">
								<tr>
									<td class="ListTitle" width="20%"   align="center">�����</td>
									<td class="ListTitle" width="10%"  align="center">��˹��Է������</td>					
									<td class="ListTitle" width="10%"   align="center">Access</td>
									<td class="ListTitle" width="10%"   align="center">No Access </td>
											
								</tr>
	<?
		$i=0;
			forEach($objModuleList->getItemList() as $objItem) {
			$objMemberModule = new MemberModule();
			if(strtolower($strMode) == "update"){
				$objMemberModule->loadByCondition(" member_id = $hId AND module_type=0 AND MM.module_id = ".$objItem->getModuleId()."  ");
				$arrAccess = explode(":",$objMemberModule->getAccess());
			}
	?>
								<tr>
									<input type="hidden" name="hModuleType[<?=$i?>]" value="0">
									<input type="hidden" name="hModuleId[<?=$i?>]" value="<?=$objItem->getModuleId();?>">
									<input type="hidden" name="hMemberModuleId[<?=$i?>]" value="<?=$objMemberModule->getMemberModuleId();?>">
									<input type="hidden" name="hModuleCode[<?=$i?>]" value="<?=$objItem->getCode();?>">
									<td align="left" class="listDetail"><strong><?=$objItem->getTitle();?></strong></td>
									<td align="center" class="listDetail"><a href="memberPermission.php?hId=<?=$hId?>&hModuleSelectId=<?=$objItem->getModuleId()?>&hSearchKeyword=<?=$hSearchKeyword?>">��˹��Է������</a></td>
									<td align="center" class="listDetail"><input type="radio" name="hAccess<?=$i?>" value=1 <?if($objMemberModule->getStatus() == 1) echo "checked"?>></td>
									<td align="center" class="listDetail"><input type="radio" name="hAccess<?=$i?>" value=0 <?if($objMemberModule->getStatus() == 0) echo "checked"?>></td>						
									
								</tr>
								
	<?
		++$i;
		}
		Unset($objModuleList);
	?>
							<input type="hidden" name="hCountModule" value="<?=$i?>">
								</table>

						    </td>
							</tr>

						    <tr> 						      
						      <td colspan="2" align="center" > <br><br>
						        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
								<?if ($strMode == "Update"){?>
									<input type="button" name="hSubmit1" value="&nbsp;&nbsp;�ѹ�֡������&nbsp;&nbsp;" onclick="return check_submit();" >
								<?}else{?>
									<input type="button" name="hSubmit1" value="&nbsp;&nbsp;�ѹ�֡������&nbsp;&nbsp;" onclick="return check_submit();" >
								<?}?>
						       &nbsp;&nbsp;&nbsp; <input type="Button" name="hSubmit" value="¡��ԡ������" onclick="window.location='memberList.php?<?=$hSearchKeyword01?>'">
						      </td>
						    </tr>
					</table>	
</form>
<script>
	function checkAll(val){
		eval("document.frm01.hFunctionAdd_"+val+".checked = true");
		eval("document.frm01.hFunctionEdit_"+val+".checked = true");
		eval("document.frm01.hFunctionDelete_"+val+".checked = true");
		eval("document.frm01.hFunctionReport_"+val+".checked = true");
		eval("document.frm01.hFunctionView_"+val+".checked = true");
		
	}
	
	function checkAccess(val){
		if(document.getElementById("hFunctionAdd_"+val).checked == true  ||   document.getElementById("hFunctionEdit_"+val).checked == true  || document.getElementById("hFunctionDelete_"+val).checked == true  || document.getElementById("hFunctionReport_"+val).checked == true  || document.getElementById("hFunctionView_"+val).checked == true    ){
			//alert("dd");
			eval("document.frm01.hAccess"+val+"[0].checked = true");
		}else{
			eval("document.frm01.hAccess"+val+"[1].checked = true");
		}
	}
	
	
	function uncheckAll(val){		
		eval("document.frm01.hFunctionAdd_"+val+".checked = false");
		eval("document.frm01.hFunctionEdit_"+val+".checked = false");
		eval("document.frm01.hFunctionDelete_"+val+".checked = false");
		eval("document.frm01.hFunctionReport_"+val+".checked = false");
		eval("document.frm01.hFunctionView_"+val+".checked = false");
		
	}

	function check_team(){
		<?
		$objD = new Department();
		$objD->loadByCondition(" code = 'INS'  ");
		?>
	
		val_company = document.frm01.hCompanyId.value;
		val_department = document.frm01.hDepartment.value;
		if(val_company > 0 && val_department==<?=$objD->getDepartmentId()?>){
			document.getElementById("d1").style.display="";
			populate01(document.frm01,document.frm01.hCompanyId.options[document.frm01.hCompanyId.selectedIndex].value,document.frm01.hTeam);
		}else{
			document.getElementById("d1").style.display="none";
			document.frm01.hTeam.value=0;
		}	
	}
	
	function populate01(frm01, selected, objSelect2) {
	<?
			
			$i=1;
			forEach($objCompanyList->getItemList() as $objItemC) {
				if($i==1) $hCompanyId = $objItemC->getCompanyId();
			?>
				var individualCategoryArray<?=$objItemC->getCompanyId();?> = new Array(
			<?
				$objTeamList = new InsureTeamList();
				$objTeamList->setFilter(" C.company_id = ".$objItemC->getCompanyId());
				$objTeamList->setPageSize(0);
				$objTeamList->setSortDefault(" title ASC");
				$objTeamList->load();
				$text = "";
					$text.= " \"('- ����к� -')\",";
				forEach($objTeamList->getItemList() as $objItemCarModel) {
					$text.= " \"('".$objItemCarModel->getTitle()."')\",";
				}
				$text = substr($text,0,(strlen($text)-1));
				echo $text;
				echo ");\n";
				$i++;
			}
		?>
	
	<?
			forEach($objCompanyList->getItemList() as $objItemC) {?>
				var individualCategoryArrayValue<?=$objItemC->getCompanyId();?> = new Array(
			<?
				$objTeamList = new InsureTeamList();
				$objTeamList->setFilter(" C.company_id = ".$objItemC->getCompanyId());
				$objTeamList->setPageSize(0);
				$objTeamList->setSortDefault(" title ASC");
				$objTeamList->load();
				$text = "";
				$text.= " \"('0')\",";
				forEach($objTeamList->getItemList() as $objItemCarModel) {
					$text.= " \"('".$objItemCarModel->getTeamId()."')\",";
				}
				$text = substr($text,0,(strlen($text)-1));
				echo $text;
				echo ");\n";
			}
		?>	
		
		
	<?forEach($objCompanyList->getItemList() as $objItemC) {?>
		if (selected == '<?=$objItemC->getCompanyId()?>') 	{
	
			while (individualCategoryArray<?=$objItemC->getCompanyId()?>.length < objSelect2.options.length) {
				objSelect2.options[(objSelect2.options.length - 1)] = null;
			}
			for (var i=0; i < individualCategoryArray<?=$objItemC->getCompanyId()?>.length; i++) 	{
				eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemC->getCompanyId()?>[i]);
				eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemC->getCompanyId()?>[i]);			
			}
		}			
	<?}?>
	
	}	
	

</script>


<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
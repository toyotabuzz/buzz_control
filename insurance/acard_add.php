<?
error_reporting(1);
include("common.php");

$objInsureCar = new InsureCar();
$objInsureOld = new Insure();
$objInsureList = new InsureList();
$objOrder = new Order();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();


$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault("title ASC");
$objCarSeriesList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCustomer = new InsureCustomer();
$objEvent = new CustomerEvent();
$objMember = new Member();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new InsureCustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS'  ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();


$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

if(isset($hSubmitDelete)){
	$objCustomerContact = new CustomerContact();
	$objCustomerContact->setCustomerContactId($hSubmitDelete);
	$objCustomerContact->delete();
	unset($objCustomerContact);
}

if(isset($hDuplicateId)){
	$objCustomerDuplicate = new CustomerDuplicate();
	$objCustomerDuplicate->setCustomerId($hDuplicateId);
	$objCustomerDuplicate->setMemberId($sMemberId);
	$objCustomerDuplicate->add();
	unset($objCustomerDuplicate);
	header("location:acardUpdate.php");
	exit;
}

if(isset($hDuplicateDeleteId)){
	$objCustomerDuplicate = new CustomerDuplicate();
	$objCustomerDuplicate->setDuplicateId($hDuplicateDeleteId);
	$objCustomerDuplicate->delete();
	unset($objCustomerDuplicate);
	header("location:acardUpdate.php?hId=$hId");
	exit;
}

//load customer
if($hAddCustomerId != ""){
	$objCustomer->setInsureCustomerId($hAddCustomerId);
	$objCustomer->load();
	
	$arrDate = explode("-",$objCustomer->getInformationDate());
	$Day = $arrDate[2];
	$Month = $arrDate[1];
	$Year = $arrDate[0];
	
	$arrDate = explode("-",$objCustomer->getBirthDay());
	$Day01 = $arrDate[2];
	$Month01 = $arrDate[1];
	$Year01 = $arrDate[0];
	
	$objEvent = new CustomerEvent();
	$objEvent->setEventId($objCustomer->getEventId());
	$objEvent->load();
	
	$objMember = new Member();
	$objMember->setMemberId($sMemberId);
	$objMember->load();
	
	//extract mobile 
	$arrMobile = explode(",",$objCustomer->getMobile());
	$hMobile_1 = $arrMobile[0];
	$hMobile_2 = $arrMobile[1];
	$hMobile_3 = $arrMobile[2];
	
	$hCustomerId = $hAddCustomerId;
}

if (empty($hSubmit)) {
	if ($hId !="") {
		$objInsureList->setFilter(" car_id= $hId ");
		$objInsureList->setPageSize(0);
		$objInsureList->setSort(" insure_id DESC ");
		$objInsureList->load();
	
		$objInsureCar->set_car_id($hId);
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_registry_date());
		$DayRegister = $arrDate[2];
		$MonthRegister = $arrDate[1];
		$YearRegister = $arrDate[0];
		
		$arrDate = explode(" ",$objInsureCar->get_get_car_date());
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$DayGetCar = $arrDay[2];
		$MonthGetCar = $arrDay[1];
		$YearGetCar = $arrDay[0];
		
		$Hour2 = $arrTime[0];
		$Minute2 = $arrTime[1];		
		
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$DayProtect = $arrDate[2];
		$MonthProtect = $arrDate[1];
		$YearProtect = $arrDate[0];		
		
		$hInsureCustomerId = $objInsureCar->get_insure_customer_id();
		$hCustomerId = $objInsureCar->get_customer_id();
				
		$arrDate = explode("-",$objInsureCar->get_call_date());
		$DayCall = $arrDate[2];
		$MonthCall = $arrDate[1];
		$YearCall = $arrDate[0];
	
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$objEvent = new CustomerEvent();
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsureCar->get_sale_id());
		$objMember->load();
		
		//extract mobile 
		$arrMobile = explode(",",$objCustomer->getMobile());
		$hMobile_1 = $arrMobile[0];
		$hMobile_2 = $arrMobile[1];
		$hMobile_3 = $arrMobile[2];
		
		
		$objInsureOld->loadByCondition(" car_id=$hId  ");
		
		$hInsureId= $objInsureOld->get_insure_id();
		$hInsureCustomerId = $objInsureCar->get_insure_customer_id();
		
	} else {
		$strMode="Add";
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
		
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
	}
	
} else {
	if (!empty($hSubmit)) {
			$objCustomer = new InsureCustomer();
			$objCustomer->setInsureCustomerId($hInsureCustomerId);
            $objCustomer->setCustomerId($hCustomerId);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			if($hTypeId == "") $hTypeId = "A";
            $objCustomer->setTypeId($hTypeId);
			$objCustomer->setACard(1);
			$objCustomer->setBCard($hBCard);
			$objCustomer->setCCard($hCCard);
            $objCustomer->setGradeId($hGradeId);
            $objCustomer->setGroupId($hGroupId);
			$objCustomer->setEventId($hEventId);
			$hPostedDate = $Year."-".$Month."-".$Day;
            $objCustomer->setInformationDate($hPostedDate);
            $objCustomer->setSaleId($hSaleId);
			$hBirthday = $Year01."-".$Month01."-".$Day01;
            $objCustomer->setBirthday($hBirthday);
            $objCustomer->setTitle($hTitle);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setEmail($hEmail);
			$objCustomer->setIDCard($hIDCard);
			$objCustomer->setContactName($hContactName);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setAddress1($hAddress1);
			
			$objCustomer->setHomeNo($hHomeNo);
			$objCustomer->setCompanyName($hCompanyName);
			$objCustomer->setBuilding($hBuilding);
			$objCustomer->setMooNo($hMooNo);
			$objCustomer->setMooban($hMooban);
			$objCustomer->setSoi($hSoi);
			$objCustomer->setRoad($hRoad);
			
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setHomeTel($hHomeTel);
			$objCustomer->setRemark($hRemark);
			$objCustomer->setCompanyId($sCompanyId);
			
			$i=0;
			if($hMobile_1 != ""){ $arrMobile[$i] = $hMobile_1;$i++;}
			if($hMobile_2 != ""){ $arrMobile[$i] = $hMobile_2;$i++;}
			if($hMobile_3 != ""){ $arrMobile[$i] = $hMobile_3;$i++;}
			
			$hMobile = implode(",",$arrMobile);
			
			$objCustomer->setMobile($hMobile);
			$objCustomer->setOfficeTel($hOfficeTel);
			$objCustomer->setFax($hFax);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setAddress01($hAddress01);
			$objCustomer->setAddress011($hAddress011);
			
			$objCustomer->setHomeNo01($hHomeNo01);
			$objCustomer->setCompanyName01($hCompanyName01);
			$objCustomer->setBuilding01($hBuilding01);
			$objCustomer->setMooNo01($hMooNo01);
			$objCustomer->setMooban01($hMooban01);
			$objCustomer->setSoi01($hSoi01);
			$objCustomer->setRoad01($hRoad01);			
			
			$objCustomer->setTumbon01($hTumbon01);
			$objCustomer->setAmphur01($hAmphur01);
			$objCustomer->setProvince01($hProvince01);
			$objCustomer->setTumbonCode01($hTumbonCode01);
			$objCustomer->setAmphurCode01($hAmphurCode01);
			$objCustomer->setProvinceCode01($hProvinceCode01);
			$objCustomer->setZipCode01($hZipCode01);
			$objCustomer->setZip01($hZip01);
			$objCustomer->setTel01($hTel01);
			$objCustomer->setFax01($hFax01);
			
			$objCustomer->setCompany($hCompany);
			$objCustomer->setAddress02($hAddress02);
			$objCustomer->setAddress021($hAddress021);
			
			$objCustomer->setHomeNo02($hHomeNo02);
			$objCustomer->setCompanyName02($hCompanyName02);
			$objCustomer->setBuilding02($hBuilding02);
			$objCustomer->setMooNo02($hMooNo02);
			$objCustomer->setMooban02($hMooban02);
			$objCustomer->setSoi02($hSoi02);
			$objCustomer->setRoad02($hRoad02);			
			
			$objCustomer->setTumbon02($hTumbon02);
			$objCustomer->setAmphur02($hAmphur02);
			$objCustomer->setProvince02($hProvince02);
			$objCustomer->setTumbonCode02($hTumbonCode02);
			$objCustomer->setAmphurCode02($hAmphurCode02);
			$objCustomer->setProvinceCode02($hProvinceCode02);
			$objCustomer->setZipCode02($hZipCode02);
			$objCustomer->setZip02($hZip02);
			$objCustomer->setTel02($hTel02);
			$objCustomer->setFax02($hFax02);
			
			$objCustomer->setName03($hName03);
			$objCustomer->setAddress03($hAddress03);
			$objCustomer->setAddress031($hAddress031);
			
			$objCustomer->setHomeNo03($hHomeNo03);
			$objCustomer->setCompanyName03($hCompanyName03);
			$objCustomer->setBuilding03($hBuilding03);
			$objCustomer->setMooNo03($hMooNo03);
			$objCustomer->setMooban03($hMooban03);
			$objCustomer->setSoi03($hSoi03);
			$objCustomer->setRoad03($hRoad03);
			
			$objCustomer->setTumbon03($hTumbon03);
			$objCustomer->setAmphur03($hAmphur03);
			$objCustomer->setProvince03($hProvince03);
			$objCustomer->setTumbonCode03($hTumbonCode03);
			$objCustomer->setAmphurCode03($hAmphurCode03);
			$objCustomer->setProvinceCode03($hProvinceCode03);
			$objCustomer->setZipCode03($hZipCode03);
			$objCustomer->setZip03($hZip03);
			$objCustomer->setTel03($hTel03);
			$objCustomer->setFax03($hFax03);
			
			$objCustomer->setName04($hName04);
			$objCustomer->setAddress04($hAddress04);
			$objCustomer->setAddress041($hAddress041);
			$objCustomer->setTumbon04($hTumbon04);
			$objCustomer->setAmphur04($hAmphur04);
			$objCustomer->setProvince04($hProvince04);
			$objCustomer->setTumbonCode04($hTumbonCode04);
			$objCustomer->setAmphurCode04($hAmphurCode04);
			$objCustomer->setProvinceCode04($hProvinceCode04);
			$objCustomer->setZipCode04($hZipCode04);
			$objCustomer->setZip04($hZip04);
			$objCustomer->setTel04($hTel04);
			$objCustomer->setFax04($hFax04);
			
			
			$objCustomer->setMailback($hMailback);
			$objCustomer->setIncomplete($hIncomplete);
			$objCustomer->setMailback($hMailback);
			$hMailbackDate = $Year02."-".$Month02."-".$Day02;
            $objCustomer->setMailbackDate($hMailbackDate);
			$objCustomer->setMailbackRemark($hMailbackRemark);
			$objCustomer->setVerifyAddress($hVerifyAddress);
			$objCustomer->setVerifyPhone($hVerifyPhone);
			
			$objInsureCar->set_car_id($hId);
			$objInsureCar->set_sale_id($hInsureCarSaleId);

			$objM = new member();
			$objM->setMemberId($hInsureCarSaleId);
			$objM->load();
			$objInsureCar->set_team_id($objM->getTeam());
			
			$objInsureCar->set_source($hSource);
			$objInsureCar->set_car_type($hCarType);
			$objInsureCar->set_insure_customer_id($hInsureCustomerId);
			$objInsureCar->set_customer_id($hCustomerId);
			$objInsureCar->set_code($hCarCode);
			$objInsureCar->set_price($hCarPrice);
			$objInsureCar->set_car_model_id($hCarModel);
			$objInsureCar->set_car_series_id($hCarSeries);
			$objInsureCar->set_color($hCarColor);
			$objInsureCar->set_car_number($hCarNumber);
			$objInsureCar->set_engine_number($hEngineNumber);
			$objInsureCar->set_register_year($hRegisterYear);
			$objInsureCar->set_suggest_from($hSuggestFrom);
			$hCallDate = $YearCall."-".$MonthCall."-".$DayCall;
			$objInsureCar->set_call_date($hCallDate);
			$objInsureCar->set_call_remark($hCallRemark);
			$objInsureCar->set_date_add(date("Y-m-d"));
			$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;
			$objInsureCar->set_date_verify($hDateProtect);
			$objInsureCar->set_buy_type($hBuyType);
			$objInsureCar->set_buy_company($hBuyCompany);
			$objInsureCar->set_registry_date($YearRegister."-".$MonthRegister."-".$DayRegister);
			$objInsureCar->set_registry_tax_year($hRegistryTaxYear);
			$objInsureCar->set_registry_tax_year_number($hRegistryTaxYearNumber);
			$objInsureCar->set_get_car_date($YearGetCar."-".$MonthGetCar."-".$DayGetCar." ".$Hour2.":".$Minute2.":00");
			$objInsureCar->set_insure_company_id($hInsureCompany);
			$objInsureCar->set_info_type($hInfoType);
			$objInsureCar->set_company_id($sCompanyId);
			$objInsureCar->set_niti($hNiti);
			$objInsureCar->set_checkc($hCheckc);
			$objInsureCar->set_province_code($hPcode);
			$objInsureCar->set_temp_sale_id($sMemberId);
			$objInsureCar->set_temp_remark($hTempRemark);

			
    		$pasrErr = $objCustomer->check($hSubmit);
			
			//echo $hInfoType;
			
			
			
			if ($strMode=="Add") {

					$objCheck = new InsureCar();
					
					if($hCarNumber != ""){
					
					if($objCheck->loadByCondition("  car_number = '".$hCarNumber."'  ")){
						$pasrErr["Duplicate01"] = "�����Ţ�ѧ��ӫ�͹";
					}
					
					}
			}


			If ( Count($pasrErr) == 0 ){
				if ($strMode=="Update") {

					
				} else {
					//check exist customer
					if($hInsureCustomerId  < 1 ){
						$hInsureCustomerId=$objCustomer->add();
					}
					
					//add insure car
					//check existing car
					
					//add car
					$objInsureCar->set_date_verify($hDateProtect);
					$objInsureCar->set_insure_customer_id($hInsureCustomerId);
					$hId = $objInsureCar->add();
					
					//update lock sale
					$objInsureCar->set_lock_sale(1);
					$objInsureCar->updateLock();
				
				}//end check strMode
				
				
				unset ($objCustomer);
				//exit;
				if($strMode == "Update"){


					
				}else{
				
					if($hCheckPrb){
						header("location:prb_new.php?hCarId=".$hId);
						exit;
					}
				
					if($hSaleLink == "sale"){
						header("location:step05List.php?hId=$hId");
					}else{
						header("location:acard_list.php");
					}
				}
				//exit;
			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)
		}
}


$pageTitle = "1. �к��������١�����оú.";
$strHead03 = "�ѹ�֡������ ACARD";
if($strMode == "Add"){ $pageContent = "1.2 �ѹ�֡������ ACARD > ����������";}else{$pageContent = "1.2 �ѹ�֡������ ACARD > ��䢢�����";}
include("h_header.php");
?>
<script type="text/javascript" src="../function/check_mobile.js"></script>

<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList01 = new CarModelList();
			$objCarModelList01->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList01->setPageSize(0);
			$objCarModelList01->setSortDefault(" title ASC");
			$objCarModelList01->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList01->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList01 = new CarModelList();
			$objCarModelList01->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList01->setPageSize(0);
			$objCarModelList01->setSortDefault(" title ASC");
			$objCarModelList01->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList01->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {

			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList01 = new CarSeriesList();
			$objCarSeriesList01->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList01->setPageSize(0);
			$objCarSeriesList01->setSortDefault(" title ASC");
			$objCarSeriesList01->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList01->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList01 = new CarSeriesList();
			$objCarSeriesList01->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList01->setPageSize(0);
			$objCarSeriesList01->setSortDefault(" title ASC");
			$objCarSeriesList01->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList01->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

function get_car_price(val){
	<?forEach($objCarSeriesList->getItemList() as $objItem) {?>
	if(val == <?=$objItem->getCarSeriesId();?>){
		document.frm01.hCarPrice.value="<?=$objItem->getPrice()?>";
	}
	<?}?>
}


</script>

<div align="center" class="error" >
<?
while (list($key, $val) = each($pasrErr)) {
    echo "**** $key = $val **** <br>";
}
?>
</div>

<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td>


<form name="frm01" action="acard_add.php" method="POST" onsubmit="return check_submit();"    onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hInsureCustomerId" value="<?=$hInsureCustomerId?>">	  
	  <input type="hidden" name="hCustomerId" value="<?=$hCustomerId?>">
	  <input type="hidden" name="hTypeId" value="<?=$objCustomer->getTypeId();?>">
  	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">	  
	  <input type="hidden" name="hInsureId" value="<?=$objInsureOld->get_insure_id();?>">
	  <input type="hidden" name="hDateVerify" value="<?=$objInsureCar->get_date_verify();?>">	  
	  <input type="hidden" name="hLink" value="<?=$hLink;?>">
	  <input type="hidden" name="hLock" value="<?=$hLock;?>">
	  <input type="hidden" name="hSaleLink" value="<?=$hSale;?>">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_backgroundBlue">
	<table width="100%">
	<tr>
		<td width="1"><img src="../images/bullet.gif" alt="" width="19" height="19" border="0"></td>
		<td><strong>������ö</strong></td>
		<td align="right"><?if($objInsureCar->get_order_id() > 0){?><input type="button" value="�ʴ������� CRL" onclick="window.open('ccardView.php?hId=<?=$objInsureCar->get_order_id()?>','','');"><?}?></td>
	</tr>
	</table>	
	</td>
</tr>
</table>
<?
$objOrder = new Order();
if($objInsureCar->get_order_id() > 0 ){
	$objOrder->setOrderId($objInsureCar->get_order_id());
	$objOrder->load();
}
?>

<table width="100%" class="i_background02">
<tr>
	<td>
		<?if($objInsureCar->get_fix_sale() > 0){?><img src="images/user_icon01.jpg" alt="�����Ź�����١�����Ңͧ Sale" width="20" height="20" border="0"><?}?>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td><strong>Sale</strong></td>
			<td align="center">:</td>
			<td  class="i_background04" >
			<table cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="text" size="1" readonly name="hInsureCarSaleId" value="<?=$objInsureCar->get_sale_id()?>"></td>
				<td>
				
			<?if($hId > 0){?>
			<?
			$objInsureSale = new Member();
			$objInsureSale->setMemberId($objInsureCar->get_sale_id());
			$objInsureSale->load();			
			
			$objCompany = new Company();
			$objCompany->setCompanyId($objInsureSale->getCompanyId());
			$objCompany->load();
			?>
			<?if($objInsureCar->get_sale_id() > 0){?>
				<?if($objInsureCar->get_lock_sale() > 0 or $objInsureCar->get_team_id() == 0 ){?>
				<img src="images/lock_icon.jpg" alt="�����Ź��١ Lock ����Ѻ Sale" width="20" height="20" border="0" align="absmiddle"><?=$objInsureSale->getFirstname()." ".$objInsureSale->getLastname()." , ".$objInsureSale->getNickname()."(".$objCompany->getShortTitle().")"?>
				<?}else{?>
				<?=$objInsureSale->getFirstname()." ".$objInsureSale->getLastname()." , ".$objInsureSale->getNickname()."(".$objCompany->getShortTitle().")"?>
				<?}?>
			<?}else{?>
				<img src="images/lock_icon.jpg" alt="�����Ź��١ Lock ����Ѻ Sale" width="20" height="20" border="0" align="absmiddle">

			<?}?>
			
			
			<?}else{?>
			<input type="hidden" size="20" name="hInsureCarSale" value=""> �Դ��;�ѡ�ҹ data �����кؤ�������Ңͧ��¡��
			<?}?>
				

				
				</td>
			</tr>
			</table>

			

			
			</td>
			<td><strong>�ѹ��������ͧ����ش</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayProtect value="<?=$DayProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthProtect value="<?=$MonthProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearProtect" value="<?=$YearProtect?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearProtect,DayProtect, MonthProtect, YearProtect,popCal);return false"></td>		
				</tr>
				</table>	
			</td>				
			<td ><strong>��Сѹ���</strong></td>			
			<td align="center">:</td>			
			<td  class="i_background04" ><?$objInsureCompanyList->printSelect("hInsureCompany",$objInsureCar->get_insure_company_id(),"��س����͡");?></td>
		</tr>		
		<tr>
			<td><strong>������</strong></td>
			<td align="center">:</td>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType",$objInsureCar->get_car_type(),"����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td><strong>���</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel",$objInsureCar->get_car_model_id(),"����к�","populate02(document.frm01,document.frm01.hCarModel.options[document.frm01.hCarModel.selectedIndex].value,document.frm01.hCarSeries)");?>
			</td>
			
			<td><strong>Ẻ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?$objCarSeriesList->printSelectScript("hCarSeries",$objInsureCar->get_car_series_id(),"����к�","get_car_price(this.value);");?></td>
		</tr>
			<?if($strMode=="Update"){?>
			<script>
				populate01(document.frm01,<?=$objInsureCar->get_car_type()?>,document.frm01.hCarModel);
				document.frm01.hCarModel.value=<?=$objInsureCar->get_car_model_id()?>;
			</script>			
			<script>
				populate02(document.frm01,<?=$objInsureCar->get_car_model_id()?>,document.frm01.hCarSeries);
				document.frm01.hCarSeries.value=<?=$objInsureCar->get_car_series_id()?>;
			</script>		
			<?}?>
		<tr>
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?=$objCarColorList->printSelect("hCarColor",$objInsureCar->get_color(),"����к�");?></td>
			<td><strong>����¹</strong></td>
			<td align="center">:</td>			
			<?if($objInsureCar->get_code() == "") $objInsureCar->set_code($hBlackNumber);?>
			<input type="hidden" name="hCarCodeCheck" size="10" maxlength="7"  value="<?=$objInsureCar->get_code()?>">			
			<td  class="i_background04" >
				<table cellpadding="0" cellspacing="0">
					<tr>
					<td><input type="text" name="hCarCode" size="7" maxlength="7"  value="<?=$objInsureCar->get_code()?>">	</td>					
					<td><input type="text" name="hPcode" size="10" value="<?=$objInsureCar->get_province_code()?>"></td>
					</tr>
				</table>
					
			
			</td>	
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<select name="hRegisterYear">
				<option value="0000" >��س����͡
			<?for($i=date("Y");$i> (date("Y")-25);$i--){?>
				<option value="<?=$i?>" <?if($objInsureCar->get_register_year() == $i) echo "selected";?>><?=$i?>
			<?}?>
			</select>			
			</td>
		</tr>
		<tr>
			<td><strong>�Ţ����ͧ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hEngineNumber" size="15"   value="<?=$objInsureCar->get_engine_number()?>"></td>
			<td><strong>�Ţ�ѧ</strong></td>
			<td align="center">:</td>			
			<?if($objInsureCar->get_car_number() == "") $objInsureCar->set_car_number($hCarNumber);?>
			<input type="hidden" name="hCarNumberCheck" size="25"   value="<?=$objInsureCar->get_car_number()?>">
			<td  class="i_background04" ><input type="text" name="hCarNumber" size="25"   value="<?=$objInsureCar->get_car_number()?>"></td>	
			<td><strong>�Ҥ�ö</strong></td>
			<td align="center">:</td>			
			<?if($objInsureCar->get_price() == 0) $objInsureCar->set_price($objOrder->getOrderPrice());?>
			<td  class="i_background04" ><input type="text" name="hCarPrice" size="20"   value="<?=$objInsureCar->get_price()?>"></td>	
		</tr>		
		<tr>
			<td><strong>��������ë���</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="radio"  name="hBuyType" value=1 <?if($objInsureCar->get_buy_type() == 1 ) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio"  name="hBuyType" value=2 <?if($objInsureCar->get_buy_type() == 2 ) echo "checked"?>>�ṹ��&nbsp;&nbsp;</td>	
			<td><strong>�ṹ��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" colspan="4" ><?$objFundCompanyList->printSelect("hBuyCompany",$objInsureCar->get_buy_company(),"��س����͡");?></td>

		</tr>
		<tr>
			<td><strong>�ѹ��訴����¹</strong></td>
			<td align="center">:</td>						
			<td  class="i_background04" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayRegister value="<?=$DayRegister?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthRegister value="<?=$MonthRegister?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearRegister" value="<?=$YearRegister?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearRegister,DayRegister, MonthRegister, YearRegister,popCal);return false"></td>		
				</tr>
				</table>		
			</td>	
			<td><strong>�������</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hRegistryTaxYear" size="10"  value="<?=$objInsureCar->get_registry_tax_year()?>"> �ҷ</td>
			<td><strong>������ջ�Шӻ�</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hRegistryTaxYearNumber" size="5"  value="<?=$objInsureCar->get_registry_tax_year_number()?>"></td>	

		</tr>			
		<tr>
			<td valign="top"><strong>�ԵԺؤ�</strong></td>
			<td align="center" valign="top">:</td>			
			<td  class="i_background04" >
				<input type="radio" name="hNiti"  value='�ԵԺؤ��'  <?if($objInsureCar->get_niti() == '�ԵԺؤ��') echo "checked"?>> �ԵԺؤ��<br>
				<input type="radio" name="hNiti"  value='�ؤ�Ÿ�����'  <?if($objInsureCar->get_niti() == '�ؤ�Ÿ�����') echo "checked"?>> �ؤ�Ÿ�����
			</td>
			<td valign="top"><strong>����������ó�</strong></td>
			<td align="center" valign="top">:</td>			
			<td  class="i_background04" >
				<input type="radio" name="hInfoType"  value='0'  <?if($objInsureCar->get_info_type() == '0') echo "checked"?>> �����Ťú��ǹ����ó�<br>
				<input type="radio" name="hInfoType"  value='1'  <?if($objInsureCar->get_info_type() == '1') echo "checked"?>> �������������ó�
			
			</td>	
			<td valign="top"><strong>�ѹ����Ѻö��ԧ</strong></td>
			<td align="center" valign="top">:</td>			
			<td  class="i_background04" valign="top" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayGetCar value="<?=$DayGetCar?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthGetCar value="<?=$MonthGetCar?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearGetCar" value="<?=$YearGetCar?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearGetCar,DayGetCar, MonthGetCar, YearGetCar,popCal);return false"></td>		
					<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
					<td><INPUT align="middle" size="2" maxlength="2"  name=Hour2 value="<?=$Hour2?>"></td>
					<td width="2" align="center">:</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Minute2 value="<?=$Minute2?>"></td>								
				</tr>
				</table>			
			</td>	
		</tr>			
		</table>
	</td>
</tr>
</table>
<br> 
	  
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_backgroundBlue">
	<table width="100%">
	<tr>
		<td width="1"><img src="../images/bullet.gif" alt="" width="19" height="19" border="0"></td>
		<td><strong>�������١���</strong></td>
		<td align="right"><?if($objInsureCar->get_order_id() > 0){?><input type="button" value="����Ң���������" onclick="window.location='import_ccard.php?hSearch=<?=$objInsureCar->get_order_id()?>';"><?}?></td>
	</tr>
	</table>	
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>���觷���Ңͧ�١���</strong> </td>
			<td>
				<?$objCustomerGroupList->printSelect("hGroupId",$objCustomer->getGroupId(),"��س����͡");?> </td>
			<td class="i_background03"><strong>�ѹ�����������</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong> </td>
			<td>				
				<input type="hidden" size="3" name="hEventId"  value="<?=$objCustomer->getEventId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hEventId.value != '' ) frm01.hSale.focus();if(event.keyCode !=13 ) frm01.hEventId.value='';"   name="hEvent"  size=40 value="<?=$objEvent->getTitle()?>">
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td valign="top" >				
				<input type="text" readonly size="3" name="hSaleId"  value="<?=$objCustomer->getSaleId()?>">
				<?
				$objCustomerSale = new Member();
				if($objCustomer->getSaleId() > 0){
					$objCustomerSale->setMemberId($objCustomer->getSaleId());
					$objCustomerSale->load();
				}
				?>
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hSaleId.value='';"   name="hSale"  size=40 value="<?=$objCustomerSale->getFirstname()."  ".$objCustomerSale->getLastname()?>">
			</td>
		</tr>
		<tr>			
			<td width="150" class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><input type="text" name="hIDCard" size="30" maxlength="13"  value="<?=$objCustomer->getIDCard();?>"></td>
			<td width="120"class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGradeList->printSelect("hGradeId",$objCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>

		<tr>
			<td class="i_background03" valign="top"><strong>����-���ʡ��</strong> </td>
			<td valign="top">
		<?if($hLock == 1){?>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>					
					<INPUT type="hidden"  name="hCustomerName"  size=35 value="<?=$name?>"><INPUT disabled onKeyDown="if(event.keyCode==13) frm01.hCustomerTitleId.focus();"    size=35 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
					<?if($strMode == "Update"){?>
					<td valign="top"></td>
					<?}?>
				</tr>
				</table>	
		<?}else{?>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>					
					<INPUT onKeyDown="if(event.keyCode==13) frm01.hCustomerTitleId.focus();"   name="hCustomerName"  size=35 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
					<?if($strMode == "Update"){?>
					<td valign="top"></td>
					<?}?>
				</tr>
				</table>					
			<?}?>
				</td>
			<td class="i_background03" valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͼ��Դ���</strong> </td>
			<td valign="top"><input type="text" name="hContactName" size="50" maxlength="50"  value="<?=$objCustomer->getContactName();?>"></td>
			<td class="i_background03" valign="top"></td>
			<td  valign="top">
			</td>
		</tr>
		</table>

				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
					
				</tr>
				</table>
				
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>			

				<?//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++?>
					
				<table width="100%"  id=form_add01_detail >
				<tr>
					<td colspan="4" height="40" class="titleHeader01">�������Ѩ�غѹ</tD>
				</tr>
				<tr>
					<td valign="top"  class="titleHeader">�Ţ��� :</td>
					<td valign="top"  class="titleHeader">����ѷ : </td>
					<td valign="top"  class="titleHeader">�Ҥ�� :</td>
					<td valign="top"  class="titleHeader">������ :</td>
				</tr>
				<tr>
					<td valign="top"><INPUT  name="hHomeNo"  size=10 value="<?=$objCustomer->getHomeNo();?>"></td>					
					<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode"  size=3 value=""><INPUT  name="hCompanyName"  size=30 value="<?=$objCustomer->getCompanyName();?>"></td>
					<td valign="top"><INPUT type="hidden"  name="hBuildingCode"  size=3 value=""><INPUT  name="hBuilding"  size=30 value="<?=$objCustomer->getBuilding();?>"></td>
					<td valign="top"><INPUT  name="hMooNo"  size=10 value="<?=$objCustomer->getMooNo();?>"></td>					
				</tr>		
				<tr>
					<td valign="top" class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
					<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
					<td valign="top" class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
					<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
				</tr>
				<tr>
					<td valign="top"><INPUT type="hidden"   name="hMoobanCode"  size=3 value=""><INPUT  name="hMooban"  size=30 value="<?=$objCustomer->getMooban();?>"></td>					
					<td valign="top"><INPUT type="hidden"   name="hSoiCode"  size=3 value=""><INPUT  name="hSoi"  size=30 value="<?=$objCustomer->getSoi();?>"></td>
					<td valign="top"><INPUT type="hidden"   name="hRoadCode"   size=3 value=""><INPUT  name="hRoad"   size=30 value="<?=$objCustomer->getRoad();?>"></td>
					<td>
					<input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"><br>
					<input type="text"  maxlength="50"  name="hAddress1" maxlength="50" size="50"  value="<?=$objCustomer->getAddress1();?>">
					</td>
				</tr>
				<tr>
					<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
					<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
					<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
					<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
				</tr>
				<tr>							
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="20"  value="<?=$objCustomer->getTumbon();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="20"  value="<?=$objCustomer->getAmphur();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="20"  value="<?=$objCustomer->getProvince();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"></td><td><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="20"  value="<?=$objCustomer->getZip();?>"></td></tr></table></td>
				</tr>				
				<tr>
					<td class="titleHeader" valign="top">�������Ѿ���ҹ :</td>
					<td class="titleHeader" valign="top">�����÷��Դ����� 1:</td>					
					<td class="titleHeader" valign="top">�����÷��Դ����� 2:</td>
					<td class="titleHeader" valign="top">�����÷��Դ����� 3:</td>
				</tr>				
				<tr>					
					<td valign="top"><input type="text" name="hHomeTel" size="20"  value="<?=$objCustomer->getHomeTel();?>"></td>
					<td valign="top"><input type="text" name="hMobile_1" size="20"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"></td>
					<td valign="top"><input type="text" name="hMobile_2" size="20"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"></td>
					<td valign="top"><input type="text" name="hMobile_3" size="20"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"></td>
				</tr>		
				<tr>
					<td class="titleHeader" valign="top">���Ѿ����ӧҹ :</td>
					<td class="titleHeader" valign="top">ῡ��:</td>					
					<td class="titleHeader" valign="top">������:</td>
					<td class="titleHeader" valign="top"></td>
				</tr>				
				<tr>					
					<td valign="top"><input type="text" name="hOfficeTel" size="20"  value="<?=$objCustomer->getOfficeTel();?>"></td>
					<td valign="top"><input type="text" name="hFax" size="20"  value="<?=$objCustomer->getFax();?>"></td>
					<td valign="top"><input type="text" name="hEmail" size="20"  value="<?=$objCustomer->getEmail();?>"></td>
					<td valign="top"></td>
				</tr>			
				</table>

				
				<?//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++?>
					
				<table width="100%"  id=form_add02_detail style="display:none">
				<tr>
					<td colspan="4" height="40" class="titleHeader01">�������������¹��ҹ &nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace01" onclick="sameplace(1)"> ������ǡѺ�������Ѩ�غѹ</tD>
				</tr>
				<tr>
					<td valign="top" class="titleHeader">�Ţ��� :</td>
					<td valign="top"  class="titleHeader">����ѷ : </td>
					<td valign="top"  class="titleHeader">�Ҥ�� :</td>
					<td valign="top" class="titleHeader">������ :</td>
				</tr>
				<tr>
					<td valign="top"><INPUT  name="hHomeNo01"  size=10 value="<?=$objCustomer->getHomeNo01();?>"></td>					
					<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode01"  size=3 value=""><INPUT  name="hCompanyName01"  size=30 value="<?=$objCustomer->getCompanyName01();?>"></td>
					<td valign="top"><INPUT type="hidden"  name="hBuildingCode01"  size=3 value=""><INPUT  name="hBuilding01"  size=30 value="<?=$objCustomer->getBuilding01();?>"></td>
					<td valign="top"><INPUT  name="hMooNo01"  size=10 value="<?=$objCustomer->getMooNo01();?>"></td>					
				</tr>		
				<tr>
					<td valign="top" class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
					<td valign="top" class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
					<td valign="top" class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
					<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
				</tr>
				<tr>
					<td valign="top"><INPUT type="hidden"   name="hMoobanCode01"  size=3 value=""><INPUT  name="hMooban01"  size=30 value="<?=$objCustomer->getMooban01();?>"></td>					
					<td valign="top"><INPUT type="hidden"   name="hSoiCode01"  size=3 value=""><INPUT  name="hSoi01"  size=30 value="<?=$objCustomer->getSoi01();?>"></td>
					<td valign="top"><INPUT type="hidden"   name="hRoadCode01"   size=3 value=""><INPUT  name="hRoad01"   size=30 value="<?=$objCustomer->getRoad01();?>"></td>
					<td>
					<input type="text"  maxlength="50"  name="hAddress01" maxlength="50" size="50"  value="<?=$objCustomer->getAddress01();?>"><br>
					<input type="text"  maxlength="50"  name="hAddress011" maxlength="50" size="50"  value="<?=$objCustomer->getAddress011();?>">
					</td>
				</tr>
				<tr>
					<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
					<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
					<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
					<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
				</tr>
				<tr>							
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode01"  value="<?=$objCustomer->getTumbonCode01();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode01.value != '' ) frm01.hAmphur01.focus();if(event.keyCode !=13 ) frm01.hTumbonCode01.value='';" name="hTumbon01" size="20"  value="<?=$objCustomer->getTumbon01();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode01"  value="<?=$objCustomer->getAmphurCode01();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode01.value != '' ) frm01.hProvince01.focus();if(event.keyCode !=13 ) frm01.hAmphurCode01.value='';" name="hAmphur01" size="20"  value="<?=$objCustomer->getAmphur01();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode01"  value="<?=$objCustomer->getProvinceCode01();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvince01Code.value != '' ) frm01.hZip01.focus();if(event.keyCode !=13 ) frm01.hProvinceCode01.value='';" name="hProvince01" size="20"  value="<?=$objCustomer->getProvince01();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode01"  value="<?=$objCustomer->getZip01();?>"></td><td><input type="text" name="hZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode01.value != '' ) frm01.hHomeTel01.focus();if(event.keyCode !=13 ) frm01.hZipCode01.value='';" size="20"  value="<?=$objCustomer->getZip01();?>"></td></tr></table></td>
				</tr>				
				<tr>
					<td class="titleHeader" valign="top">���Ѿ�� :</td>
					<td class="titleHeader" valign="top">ῡ��:</td>					
					<td class="titleHeader" valign="top"></td>
					<td class="titleHeader" valign="top"></td>
				</tr>				
				<tr>					
					<td valign="top"><input type="text" name="hTel01" size="20"  value="<?=$objCustomer->getTel01();?>"></td>
					<td valign="top"><input type="text" name="hFax01" size="20"  value="<?=$objCustomer->getFax01();?>"></td>
					<td valign="top"></td>
					<td valign="top"></td>
				</tr>		
				</table>
				
				
				<?//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++?>
					
				<table width="100%"  id=form_add03_detail  style="display:none">
				<tr>
					<td colspan="4" height="40" class="titleHeader01">���������ӧҹ &nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace02" onclick="sameplace(2)"> ������ǡѺ�������Ѩ�غѹ</tD>
				</tr>
				<tr>
					<td valign="top"  class="titleHeader">�Ţ��� :</td>
					<td valign="top"  class="titleHeader">����ѷ : </td>
					<td valign="top" class="titleHeader">�Ҥ�� :</td>
					<td valign="top"  class="titleHeader">������ :</td>
				</tr>
				<tr>
					<td valign="top"><INPUT  name="hHomeNo02"  size=10 value="<?=$objCustomer->getHomeNo02();?>"></td>					
					<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode02"  size=3 value=""><INPUT  name="hCompanyName02"  size=30 value="<?=$objCustomer->getCompanyName02();?>"></td>
					<td valign="top"><INPUT type="hidden"  name="hBuildingCode02"  size=3 value=""><INPUT  name="hBuilding02"  size=30 value="<?=$objCustomer->getBuilding02();?>"></td>
					<td valign="top"><INPUT  name="hMooNo02"  size=10 value="<?=$objCustomer->getMooNo02();?>"></td>					
				</tr>		
				<tr>
					<td valign="top"  class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
					<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
					<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
					<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
				</tr>
				<tr>
					<td valign="top"><INPUT type="hidden"   name="hMoobanCode02"  size=3 value=""><INPUT  name="hMooban02"  size=30 value="<?=$objCustomer->getMooban02();?>"></td>					
					<td valign="top"><INPUT type="hidden"   name="hSoiCode02"  size=3 value=""><INPUT  name="hSoi02"  size=30 value="<?=$objCustomer->getSoi02();?>"></td>
					<td valign="top"><INPUT type="hidden"   name="hRoadCode02"   size=3 value=""><INPUT  name="hRoad02"   size=30 value="<?=$objCustomer->getRoad02();?>"></td>
					<td>
					<input type="text"  maxlength="50"  name="hCompany" maxlength="50" size="50"  value="<?=$objCustomer->getCompany();?>"><br>
					<input type="text"  maxlength="50"  name="hAddress02" maxlength="50" size="50"  value="<?=$objCustomer->getAddress02();?>"><br>
					<input type="text"  maxlength="50"  name="hAddress021" maxlength="50" size="50"  value="<?=$objCustomer->getAddress021();?>">
					</td>
				</tr>
				<tr>
					<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
					<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
					<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
					<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
				</tr>
				<tr>							
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode02"  value="<?=$objCustomer->getTumbonCode02();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm02.hTumbonCode02.value != '' ) frm02.hAmphur02.focus();if(event.keyCode !=13 ) frm02.hTumbonCode02.value='';" name="hTumbon02" size="20"  value="<?=$objCustomer->getTumbon02();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode02"  value="<?=$objCustomer->getAmphurCode02();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm02.hAmphurCode02.value != '' ) frm02.hProvince02.focus();if(event.keyCode !=13 ) frm02.hAmphurCode02.value='';" name="hAmphur02" size="20"  value="<?=$objCustomer->getAmphur02();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode02"  value="<?=$objCustomer->getProvinceCode02();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm02.hProvince02Code.value != '' ) frm02.hZip02.focus();if(event.keyCode !=13 ) frm02.hProvinceCode02.value='';" name="hProvince02" size="20"  value="<?=$objCustomer->getProvince02();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode02"  value="<?=$objCustomer->getZip02();?>"></td><td><input type="text" name="hZip02"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm02.hZipCode02.value != '' ) frm02.hHomeTel02.focus();if(event.keyCode !=13 ) frm02.hZipCode02.value='';" size="20"  value="<?=$objCustomer->getZip02();?>"></td></tr></table></td>
				</tr>				
				<tr>
					<td class="titleHeader" valign="top">���Ѿ�� :</td>
					<td class="titleHeader" valign="top">ῡ��:</td>					
					<td class="titleHeader" valign="top"></td>
					<td class="titleHeader" valign="top"></td>
				</tr>				
				<tr>					
					<td valign="top"><input type="text" name="hTel02" size="20"  value="<?=$objCustomer->getTel02();?>"></td>
					<td valign="top"><input type="text" name="hFax02" size="20"  value="<?=$objCustomer->getFax02();?>"></td>
					<td valign="top"></td>
					<td valign="top"></td>
				</tr>		
				</table>
				
				
				<?//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++?>
					
				<table width="100%"  id=form_add04_detail style="display:none">
				<tr>
					<td colspan="4" height="40" class="titleHeader01">������������͡��� &nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace03" onclick="sameplace(3)"> ������ǡѺ�������Ѩ�غѹ</tD>
				</tr>
				<tr>
					<td valign="top"  class="titleHeader">�Ţ��� :</td>
					<td valign="top"  class="titleHeader">����ѷ : </td>
					<td valign="top"  class="titleHeader">�Ҥ�� :</td>
					<td valign="top"  class="titleHeader">������ :</td>
				</tr>
				<tr>
					<td valign="top"><INPUT  name="hHomeNo03"  size=10 value="<?=$objCustomer->getHomeNo03();?>"></td>					
					<td valign="top"><INPUT type="hidden"  name="hCompanyNameCode03"  size=3 value=""><INPUT  name="hCompanyName03"  size=30 value="<?=$objCustomer->getCompanyName03();?>"></td>
					<td valign="top"><INPUT type="hidden"  name="hBuildingCode03"  size=3 value=""><INPUT  name="hBuilding03"  size=30 value="<?=$objCustomer->getBuilding03();?>"></td>
					<td valign="top"><INPUT  name="hMooNo03"  size=10 value="<?=$objCustomer->getMooNo03();?>"></td>					
				</tr>		
				<tr>
					<td valign="top"  class="titleHeader">�����ҹ : <span class="remark">����ͧ��� �. ���� �����ҹ</span></td>
					<td valign="top" class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>					
					<td valign="top"  class="titleHeader">��� : <span class="remark">����ͧ��� �. ���� ���</span></td>
					<td  class="titleHeader"><strong>�������Ѩ�غѹ (���)</strong></td>
				</tr>
				<tr>
					<td valign="top"><INPUT type="hidden"   name="hMoobanCode03"  size=3 value=""><INPUT  name="hMooban03"  size=30 value="<?=$objCustomer->getMooban03();?>"></td>					
					<td valign="top"><INPUT type="hidden"   name="hSoiCode03"  size=3 value=""><INPUT  name="hSoi03"  size=30 value="<?=$objCustomer->getSoi03();?>"></td>
					<td valign="top"><INPUT type="hidden"   name="hRoadCode03"   size=3 value=""><INPUT  name="hRoad03"   size=30 value="<?=$objCustomer->getRoad03();?>"></td>
					<td>
					<input type="text"  maxlength="50"  name="hAddress03" maxlength="50" size="50"  value="<?=$objCustomer->getAddress03();?>"><br>
					<input type="text"  maxlength="50"  name="hAddress031" maxlength="50" size="50"  value="<?=$objCustomer->getAddress031();?>">
					</td>
				</tr>
				<tr>
					<td class="titleHeader" valign="top"><strong>�Ӻ�/�ǧ</strong>:</td>
					<td class="titleHeader" valign="top"><strong>�����/ࢵ</strong></td>					
					<td class="titleHeader" valign="top"><strong>�ѧ��Ѵ</strong></td>					
					<td  class="titleHeader"valign="top"><strong>������ɳ���</strong></td>			
				</tr>
				<tr>							
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hTumbonCode03"  value="<?=$objCustomer->getTumbonCode03();?>"></td><td><input type="text" onKeyDown="if(event.keyCode==13 && frm03.hTumbonCode03.value != '' ) frm03.hAmphur03.focus();if(event.keyCode !=13 ) frm03.hTumbonCode03.value='';" name="hTumbon03" size="20"  value="<?=$objCustomer->getTumbon03();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hAmphurCode03"  value="<?=$objCustomer->getAmphurCode03();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm03.hAmphurCode03.value != '' ) frm03.hProvince03.focus();if(event.keyCode !=13 ) frm03.hAmphurCode03.value='';" name="hAmphur03" size="20"  value="<?=$objCustomer->getAmphur03();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hProvinceCode03"  value="<?=$objCustomer->getProvinceCode03();?>"></td><td><input type="text"  onKeyDown="if(event.keyCode==13 && frm03.hProvince03Code.value != '' ) frm03.hZip03.focus();if(event.keyCode !=13 ) frm03.hProvinceCode03.value='';" name="hProvince03" size="20"  value="<?=$objCustomer->getProvince03();?>"></td></tr></table></td>
					<td valign="top"><table cellpadding="0" cellpadding="0"><tr><td><input type="text" size="3" readonly name="hZipCode03"  value="<?=$objCustomer->getZip03();?>"></td><td><input type="text" name="hZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm03.hZipCode03.value != '' ) frm03.hHomeTel03.focus();if(event.keyCode !=13 ) frm03.hZipCode03.value='';" size="20"  value="<?=$objCustomer->getZip03();?>"></td></tr></table></td>
				</tr>				
				<tr>
					<td class="titleHeader" valign="top">���Ѿ�� :</td>
					<td class="titleHeader" valign="top">ῡ��:</td>					
					<td class="titleHeader" valign="top">���ͼ���Ѻ : <span class="remark">�óժ��ͼ���Ѻ��ҧ�ҡ������Ңͧö</span></td>
					<td class="titleHeader" valign="top"></td>
				</tr>				
				<tr>					
					<td valign="top"><input type="text" name="hTel03" size="20"  value="<?=$objCustomer->getTel03();?>"></td>
					<td valign="top"><input type="text" name="hFax03" size="20"  value="<?=$objCustomer->getFax03();?>"></td>
					<td valign="top"><input type="text"  maxlength="50"  name="hName03" maxlength="50" size="50"  value="<?=$objCustomer->getName03();?>"></td>
					<td valign="top"></td>
				</tr>	
				</table>

	</td>
</tr>
</table>	

<?
if($hId != ""){


$objInsureList = new InsureList();
$objInsureList->setFilter(" P.car_id = $hId ");
$objInsureList->setPageSize(0);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" date_protect DESC ");
$objInsureList->setSort($hSort);
$objInsureList->loadList();
?><br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_backgroundBlue">
	<table width="100%">
	<tr>
		<td width="1"><img src="../images/bullet.gif" alt="" width="19" height="19" border="0"></td>
		<td><strong>�����Ż�Сѹ���</strong></td>
		<td align="right"></td>
	</tr>
	</table>	
	</td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="1" cellspacing="1" class=search>
<tr>
	<td width="2%" rowspan="2" align="center" class="ListTitle">�ӴѺ</td>
	<td width="5%" rowspan="2"  align="center" class="ListTitle">�ѹ����駧ҹ</td>
	
	<td width="35%" align="center"  class="i_backgroundGreen" colspan="6">�Ҥ��Ѥ��</td>
	<td width="35%" align="center" class="i_backgroundPink" colspan="5">�Ҥ�ѧ�Ѻ</td>
	<td width="5%" rowspan="2"  align="center" class="ListTitle">SALE</td>	
	<td width="5%" rowspan="2"  align="center" class="ListTitle">ʶҹ�</td>	
</tr>
<tr>
			<td class="i_backgroundGreen">�á����</td>
			<td class="i_backgroundGreen">����ѷ</td>
			<td class="i_backgroundGreen" >�Ţ�Ѻ��</td>
			<td class="i_backgroundGreen">�����Ţ��������</td>
			<td class="i_backgroundGreen">�ѹ����������</td>
			<td class="i_backgroundGreen">�ѹ�������ش	</td>
			<td class="i_backgroundPink">�á����</td>
			<td class="i_backgroundPink">����ѷ</td>
			<td class="i_backgroundPink">�����Ţ��������</td>
			<td class="i_backgroundPink">�ѹ����������</td>
			<td class="i_backgroundPink">�ѹ�������ش	</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem) {
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objMember1= new Member();
		$objMember1->setMemberId($objItem->get_officer_id());
		$objMember1->load();
		
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objInsureCar->get_customer_id());
		$objCustomer->load();
		
		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objItem->get_k_prb_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objItem->get_p_prb_id());
		$objKomCompany->load();
		
		$objBroker = new InsureBroker();
		$objBroker->setInsureBrokerId($objItem->get_k_broker_id());
		$objBroker->load();
		
		$objBrokerKom = new InsureBroker();
		$objBrokerKom->setInsureBrokerId($objItem->get_p_broker_id());
		$objBrokerKom->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=(($hPage-1)*50)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_insure_date())?></td>	
	<?if($objItem->get_k_status() == "cancel"){?>
	<td valign="top" class="ListDetail03"  ><?=$objBroker->getTitle()?></td>	
	<td valign="top" class="ListDetail03"  ><?=$objPrbCompany->getTitle()?></td>
	<td valign="top" class="ListDetail03"  ><?=$objItem->get_p_rnum()?></td>
	<td valign="top" class="ListDetail03"  ><?=$objItem->get_k_number();?></td>	
	<td valign="top" class="ListDetail03"  ><?=formatShortDate($objItem->get_k_start_date())?></td>	
	<td valign="top" class="ListDetail03"  ><?=formatShortDate($objItem->get_k_end_date())?></td>	
	<?}else{?>
	<td valign="top" ><?=$objBroker->getTitle()?></td>	
	<td valign="top" ><?=$objPrbCompany->getTitle()?></td>
	<td valign="top" ><?=$objItem->get_p_rnum()?></td>
	<td valign="top" ><?=$objItem->get_k_number();?></td>	
	<td valign="top" ><?=formatShortDate($objItem->get_k_start_date())?></td>	
	<td valign="top" ><?=formatShortDate($objItem->get_k_end_date())?></td>	
	<?}?>
	
	
	<?if($objItem->get_p_status() == "cancel" or  $objItem->get_status() == "cancel" ){?>
	<td valign="top" class="ListDetail03" ><?=$objBrokerKom->getTitle()?></td>	
	<td valign="top" class="ListDetail03"  ><?=$objKomCompany->getTitle()?></td>
	<td valign="top" class="ListDetail03"  ><?=$objItem->get_p_number();?></td>	
	<td valign="top" class="ListDetail03"  ><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" class="ListDetail03"  ><?=formatShortDate($objItem->get_p_end_date())?></td>	
	<?}else{?>
	<td valign="top" ><?=$objBrokerKom->getTitle()?></td>	
	<td valign="top" ><?=$objKomCompany->getTitle()?></td>
	<td valign="top" ><?=$objItem->get_p_number();?></td>	
	<td valign="top" ><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" ><?=formatShortDate($objItem->get_p_end_date())?></td>	
	<?}?>
	

	<td valign="top"  align="center" ><?=$objMember1->getNickname()?>/<?=$objMember->getNickname()?></td>
	<td valign="top"  align="center" >
		<?=$objItem->get_k_status()?>
		<?if($objItem->get_k_status() == "new" ){?><a target="_parent" href="step02List.php?hInsureId=<?=$objItem->get_insure_id()?>">�ä��駷�� 1</a><?}?>
		<?if($objItem->get_k_status() == "call1"){?><a target="_parent"  href="step03List.php?hInsureId=<?=$objItem->get_insure_id()?>">��ʹ��Ҥ�</a><?}?>
		<?if($objItem->get_k_status() == "quotation"){?><a target="_parent"  href="step04List.php?hInsureId=<?=$objItem->get_insure_id()?>">�Դ��â��</a><?}?>
		<?if($objItem->get_k_status() == "call2"){?><a target="_parent"  href="step05List.php?hInsureId=<?=$objItem->get_insure_id()?>">�ѹ�֡��â��</a><?}?>
		<?if($objItem->get_k_status() == "booking"){?><a target="_parent"  href="step06List.php?hInsureId=<?=$objItem->get_insure_id()?>">�ͪ���</a><?}?>
		<?if($objItem->get_k_status() == "ccard" and $objItem->get_direct == '' ){?><a target="_blank" href="insure_ccard_detail.php?hCarId=<?=$objInsureCar->get_car_id()?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$objItem->get_insure_id()?>">��������´</a><?}?>	
		<?if($objItem->get_k_status() == "cancel" and $objItem->get_direct == '' ){?><a target="_blank" href="step06List.php?hInsureId=<?=$objItem->get_insure_id()?>">�ʴ���¡��</a><?}?>	
	</td>
	
	</td>

</tr>
	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
		
<?}else{?>
<?}?>		
		

<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button" name="hSubmit1" value="�ѹ�֡������" class="button" onclick="checkSubmit();"  >
<?}else{?>
	<input type="button" name="hSubmit1" value="�ѹ�֡������" class="button" onclick="checkSubmit();" >
<?}?>
<?$hNewLink = str_replace("YYY", "&", $hLink);?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button"  class="button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='<?=$hNewLink?>'">			
<?
$objM01 = new Member();
$objM01->setMemberId($sMemberId);
$objM01->load();

if($objM01->getInsureEditor() == "1" ){?>
	<input type="checkbox" name="hCheckPrb" value="1"> �ѹ�֡����͡�ú.
<?}?>

	<br><br>
	</td>
</tr>
</table>

	</td>
</tr>
</table>


<?
	include("h_footer.php")
?>


<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {
		return "acardAutoCustomer.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hCompanyName", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm01&hField=hCompanyNameCode&hField01=hBuilding&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm01&hField=hBuildingCode&hField01=hMooNo&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm01&hField=hMoobanCode&hField01=hSoi&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm01&hField=hSoiCode&hField01=hRoad&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm01&hField=hRoadCode&hField01=hTumbon&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm01&hField=hCompanyNameCode01&hField01=hBuilding01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm01&hField=hBuildingCode01&hField01=hMooNo01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm01&hField=hMoobanCode01&hField01=hSoi01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm01&hField=hSoiCode01&hField01=hRoad01&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm01&hField=hRoadCode01&hField01=hTumbon01&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm02&hField=hCompanyNameCode02&hField02=hBuilding02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm02&hField=hBuildingCode02&hField02=hMooNo02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm02&hField=hMoobanCode02&hField02=hSoi02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm02&hField=hSoiCode02&hField02=hRoad02&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm02&hField=hRoadCode02&hField02=hTumbon02&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm03&hField=hCompanyNameCode03&hField03=hBuilding03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm03&hField=hBuildingCode03&hField03=hMooNo03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm03&hField=hMoobanCode03&hField03=hSoi03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm03&hField=hSoiCode03&hField03=hRoad03&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm03&hField=hRoadCode03&hField03=hTumbon03&q=" + this.text.value;
		}
	})			
	
	
	
	
	
	
	new CAPXOUS.AutoComplete("hPcode", function() {
		var str = this.text.value;	
		if( str.length > 1){
			return "auto_province_code.php?hFrm=frm01&q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hEvent", function() {
		var str = this.text.value;	
		return "acardAutoEvent.php?q=" + this.text.value;		
	});		
		
	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hInsureCarSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&hField=hInsureCarSaleId&q=" + this.text.value;
		}
	});		
	


		new CAPXOUS.AutoComplete("hTumbon01", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur01.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince01.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon02", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur02.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince02.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon03", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur03.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince03.php?q=" + this.text.value;
		}
	});			
	

		
		
	
	

	function confirm_edit(){
		if(confirm("�س��ͧ��÷��кѹ�֡������������?")){
		
			return true;
		}
		return false;
	}
	
	function confirm_remark(){
		if(confirm("�س��ͧ��÷��кѹ�֡�����˵���������������?")){
		
			return true;
		}
		return false;
	}	

	function confirm_edit_delete(val){
		if(confirm("�س��ͧ��÷���ź�����š�â�����������?")){
			window.location='acard_update.php?hId=<?=$hId?>&hEditId='+val+'&hEditDelete=yes';
		}
		return false;
	}	
	
	function confirm_remark_delete(val){
		if(confirm("�س��ͧ��÷���ź�������������?")){
			window.location='acard_update.php?hId=<?=$hId?>&hEditId='+val+'&hEditDelete=yes';
		}
		return false;
	}		
	

	new CAPXOUS.AutoComplete("hCustomerName", function() {
		return "acardAutoCustomer.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hCompanyName", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm01&hField=hCompanyNameCode&hField01=hBuilding&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm01&hField=hBuildingCode&hField01=hMooNo&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm01&hField=hMoobanCode&hField01=hSoi&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm01&hField=hSoiCode&hField01=hRoad&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm01&hField=hRoadCode&hField01=hTumbon&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm01&hField=hCompanyNameCode01&hField01=hBuilding01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm01&hField=hBuildingCode01&hField01=hMooNo01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm01&hField=hMoobanCode01&hField01=hSoi01&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm01&hField=hSoiCode01&hField01=hRoad01&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad01", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm01&hField=hRoadCode01&hField01=hTumbon01&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm02&hField=hCompanyNameCode02&hField02=hBuilding02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm02&hField=hBuildingCode02&hField02=hMooNo02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm02&hField=hMoobanCode02&hField02=hSoi02&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm02&hField=hSoiCode02&hField02=hRoad02&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad02", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm02&hField=hRoadCode02&hField02=hTumbon02&q=" + this.text.value;
		}
	})			
	
	
	new CAPXOUS.AutoComplete("hCompanyName03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=company_name&hFrm=frm03&hField=hCompanyNameCode03&hField03=hBuilding03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hBuilding03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=building&hFrm=frm03&hField=hBuildingCode03&hField03=hMooNo03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hMooban03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=mooban&hFrm=frm03&hField=hMoobanCode03&hField03=hSoi03&q=" + this.text.value;
		}
	})	
	
	new CAPXOUS.AutoComplete("hSoi03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=soi&hFrm=frm03&hField=hSoiCode03&hField03=hRoad03&q=" + this.text.value;
		}
	})				
	
	new CAPXOUS.AutoComplete("hRoad03", function() {
		var str = this.text.value;	
		if( str.length >= 2){
			return "auto_other.php?hType=road&hFrm=frm03&hField=hRoadCode03&hField03=hTumbon03&q=" + this.text.value;
		}
	})			
	
	
	
	
	
	
	new CAPXOUS.AutoComplete("hPcode", function() {
		var str = this.text.value;	
		if( str.length > 1){
			return "auto_province_code.php?hFrm=frm01&q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hEvent", function() {
		var str = this.text.value;	
		return "acardAutoEvent.php?q=" + this.text.value;		
	});		
		
	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hInsureCarSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&hField=hInsureCarSaleId&q=" + this.text.value;
		}
	});		
	


		new CAPXOUS.AutoComplete("hTumbon01", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur01.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince01.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon02", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur02.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince02.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon03", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur03.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince03.php?q=" + this.text.value;
		}
	});			
	

		
		
	function checkSubmit(){	
	
	
	if(document.frm01.hInfoType[1].checked == false){
		//����������ó�
	
		if(document.frm01.hInsureCarSaleId.value == "0" ){
			alert("��س����͡��ѡ�ҹ���");
			change_tab("car");
			document.frm01.hInsureCarSaleId.focus();
			return false;
		}	
	
		if(document.frm01.hCarType.value ==0){
			alert("��س����͡������ö");
			change_tab("car");			
			document.frm01.hCarType.focus();
			return false;
		}	
	
		if(document.frm01.hCarModel.value ==0){
			alert("��س����͡���ö");
			change_tab("car");			
			document.frm01.hCarModel.focus();
			return false;
		}	
		
		if(document.frm01.hCarSeries.value ==0){
			alert("��س����͡Ẻö");
			change_tab("car");			
			document.frm01.hCarSeries.focus();
			return false;
		}				
	
		if(document.frm01.hCarCode.value ==0){
			alert("��س��кط���¹ö");
			change_tab("car");			
			document.frm01.hCarCode.focus();
			return false;
		}			
	
		if(document.frm01.hRegisterYear.value < 1 ){
			alert("��س����͡��");
			change_tab("car");			
			document.frm01.hRegisterYear.focus();
			return false;
		}			

		if(document.frm01.hCarNumber.value == ""){
			alert("��س��к��Ţ�ѧ");
			change_tab("car");			
			document.frm01.hCarNumber.focus();
			return false;
		}	
	
	
		if(document.frm01.hGroupId.value == ""){
			alert("��س��к����觷����");
			change_tab("customer");			
			document.frm01.hGroupId.focus();
			return false;
		}
	
		if(document.frm01.hSaleId.value == ""){
			alert("��س��кؾ�ѡ�ҹ�����������");
			change_tab("customer");						
			document.frm01.hSale.focus();
			return false;
		}	
	
	
		if(document.frm01.hIDCard.value == ""){
			alert("��س��к������Ţ�ѵû�ЪҪ�");
			change_tab("customer");
			document.frm01.hIDCard.focus();
			return false;
		}
	
		if(document.frm01.hCustomerName.value == ""){
			alert("��س��кت����١���");
			change_tab("customer");
			document.frm01.hCustomerName.focus();
			return false;
		}
	/*
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		*/
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س��кصӺ�");
			change_tab("customer");
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س��к������");
			change_tab("customer");
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س��кبѧ��Ѵ");
			change_tab("customer");
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س��к�������ɳ���");
			change_tab("customer");
			document.forms.frm01.hZip.focus();
			return false;
		} 			
		
		if ( (document.forms.frm01.hMobile_1.value=="" && document.forms.frm01.hMobile_2.value=="" && document.forms.frm01.hMobile_3.value=="" )    && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			change_tab("customer");
			return false;
		} 			
		

	
	/*
		if(document.frm01.hInsureCompanyId.value ==0){
			alert("��س����͡����ѷ��Сѹ");
			document.frm01.hInsureCompanyId.focus();
			return false;
		}
	
	
		if (document.forms.frm01.DayProtect.value=="" || document.forms.frm01.DayProtect.value=="00")
		{
			alert("��س��к��ѹ��������ͧ");
			document.forms.frm01.DayProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.DayProtect.value,1,31) == false) {
				document.forms.frm01.DayProtect.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.MonthProtect.value==""  || document.forms.frm01.MonthProtect.value=="00")
		{
			alert("��س��к���͹��������ͧ");
			document.forms.frm01.MonthProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.MonthProtect.value,1,12) == false){
				document.forms.frm01.MonthProtect.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.YearProtect.value==""  || document.forms.frm01.YearProtect.value=="0000")
		{
			alert("��س��кػշ�������ͧ");
			document.forms.frm01.YearProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.YearProtect.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.YearProtect.focus();
				return false;
			}
		} 				
	*/	
	
	
	}else{
		//�������������ó�
	}
	
	
	
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
	
	}
	

	
	function change_tab(val){
		document.getElementById("tab_car").style.display="none";
		document.getElementById("tab_customer").style.display="none";
		document.getElementById("tab_insurance").style.display="none";
		document.getElementById("tab_remark").style.display="none";
		if(val =="car"){document.getElementById("tab_car").style.display="";}
		if(val =="customer"){document.getElementById("tab_customer").style.display="";}
		if(val =="insurance"){document.getElementById("tab_insurance").style.display="";}
		if(val =="remark"){document.getElementById("tab_remark").style.display="";}
	}	

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			if(document.frm01.hCustomerSamePlace01.checked == true){
				document.frm01.hAddress01.value = document.frm01.hAddress.value;
				document.frm01.hAddress011.value = document.frm01.hAddress1.value;
				
				document.frm01.hHomeNo01.value = document.frm01.hHomeNo.value;
				document.frm01.hCompanyNameCode01.value = document.frm01.hCompanyNameCode.value;
				document.frm01.hCompanyName01.value = document.frm01.hCompanyName.value;
				document.frm01.hBuildingCode01.value = document.frm01.hBuildingCode.value;
				document.frm01.hBuilding01.value = document.frm01.hBuilding.value;
				document.frm01.hMooNo01.value = document.frm01.hMooNo.value;
				document.frm01.hMoobanCode01.value = document.frm01.hMoobanCode.value;
				document.frm01.hMooban01.value = document.frm01.hMooban.value;
				document.frm01.hSoiCode01.value = document.frm01.hSoiCode.value;
				document.frm01.hSoi01.value = document.frm01.hSoi.value;
				document.frm01.hRoadCode01.value = document.frm01.hRoadCode.value;
				document.frm01.hRoad01.value = document.frm01.hRoad.value;
				
				document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
				document.frm01.hProvince01.value = document.frm01.hProvince.value;
				document.frm01.hZip01.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode01.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode01.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode01.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode01.value = document.frm01.hZipCode.value;		
				document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax01.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress01.value = "";
				document.frm01.hAddress011.value = "";
				
				document.frm01.hHomeNo01.value = "";
				document.frm01.hCompanyNameCode01.value = "";
				document.frm01.hCompanyName01.value = "";
				document.frm01.hBuildingCode01.value = "";
				document.frm01.hBuilding01.value = "";
				document.frm01.hMooNo01.value ="";
				document.frm01.hMoobanCode01.value = "";
				document.frm01.hMooban01.value = "";
				document.frm01.hSoiCode01.value = "";
				document.frm01.hSoi01.value = "";
				document.frm01.hRoadCode01.value = "";
				document.frm01.hRoad01.value = "";
				
				document.frm01.hTumbon01.value = "";
				document.frm01.hAmphur01.value = "";
				document.frm01.hProvince01.value = "";
				document.frm01.hZip01.value = "";		
				document.frm01.hTumbonCode01.value = "";
				document.frm01.hAmphurCode01.value = "";
				document.frm01.hProvinceCode01.value = "";
				document.frm01.hZipCode01.value = "";		
				document.frm01.hTel01.value = "";		
				document.frm01.hFax01.value = "";		
			}
		}
   		if(val ==2){
			if(document.frm01.hCustomerSamePlace02.checked == true){
				document.frm01.hAddress02.value = document.frm01.hAddress.value;
				document.frm01.hAddress021.value = document.frm01.hAddress1.value;
				
				document.frm01.hHomeNo02.value = document.frm01.hHomeNo.value;
				document.frm01.hCompanyNameCode02.value = document.frm01.hCompanyNameCode.value;
				document.frm01.hCompanyName02.value = document.frm01.hCompanyName.value;
				document.frm01.hBuildingCode02.value = document.frm01.hBuildingCode.value;
				document.frm01.hBuilding02.value = document.frm01.hBuilding.value;
				document.frm01.hMooNo02.value = document.frm01.hMooNo.value;
				document.frm01.hMoobanCode02.value = document.frm01.hMoobanCode.value;
				document.frm01.hMooban02.value = document.frm01.hMooban.value;
				document.frm01.hSoiCode02.value = document.frm01.hSoiCode.value;
				document.frm01.hSoi02.value = document.frm01.hSoi.value;
				document.frm01.hRoadCode02.value = document.frm01.hRoadCode.value;
				document.frm01.hRoad02.value = document.frm01.hRoad.value;				
				
				
				document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
				document.frm01.hProvince02.value = document.frm01.hProvince.value;
				document.frm01.hZip02.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode02.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode02.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode02.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode02.value = document.frm01.hZipCode.value;		
				document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax02.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress02.value = "";
				document.frm01.hAddress021.value = "";
				
				document.frm01.hHomeNo02.value = "";
				document.frm01.hCompanyNameCode02.value = "";
				document.frm01.hCompanyName02.value = "";
				document.frm01.hBuildingCode02.value = "";
				document.frm01.hBuilding02.value = "";
				document.frm01.hMooNo02.value ="";
				document.frm01.hMoobanCode02.value = "";
				document.frm01.hMooban02.value = "";
				document.frm01.hSoiCode02.value = "";
				document.frm01.hSoi02.value = "";
				document.frm01.hRoadCode02.value = "";
				document.frm01.hRoad02.value = "";				
				
				document.frm01.hTumbon02.value = "";
				document.frm01.hAmphur02.value = "";
				document.frm01.hProvince02.value = "";
				document.frm01.hZip02.value = "";		
				document.frm01.hTumbonCode02.value = "";
				document.frm01.hAmphurCode02.value = "";
				document.frm01.hProvinceCode02.value = "";
				document.frm01.hZipCode02.value = "";		
				document.frm01.hTel02.value = "";		
				document.frm01.hFax02.value = "";		
			}
			
		}
   		if(val ==3){
			if(document.frm01.hCustomerSamePlace03.checked == true){
				document.frm01.hAddress03.value = document.frm01.hAddress.value;
				document.frm01.hAddress031.value = document.frm01.hAddress1.value;
				
				document.frm01.hHomeNo03.value = document.frm01.hHomeNo.value;
				document.frm01.hCompanyNameCode03.value = document.frm01.hCompanyNameCode.value;
				document.frm01.hCompanyName03.value = document.frm01.hCompanyName.value;
				document.frm01.hBuildingCode03.value = document.frm01.hBuildingCode.value;
				document.frm01.hBuilding03.value = document.frm01.hBuilding.value;
				document.frm01.hMooNo03.value = document.frm01.hMooNo.value;
				document.frm01.hMoobanCode03.value = document.frm01.hMoobanCode.value;
				document.frm01.hMooban03.value = document.frm01.hMooban.value;
				document.frm01.hSoiCode03.value = document.frm01.hSoiCode.value;
				document.frm01.hSoi03.value = document.frm01.hSoi.value;
				document.frm01.hRoadCode03.value = document.frm01.hRoadCode.value;
				document.frm01.hRoad03.value = document.frm01.hRoad.value;							
				
				document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
				document.frm01.hProvince03.value = document.frm01.hProvince.value;
				document.frm01.hZip03.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode03.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode03.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode03.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode03.value = document.frm01.hZipCode.value;		
				document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax03.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress03.value = "";
				document.frm01.hAddress031.value = "";
				
				document.frm01.hHomeNo03.value = "";
				document.frm01.hCompanyNameCode03.value = "";
				document.frm01.hCompanyName03.value = "";
				document.frm01.hBuildingCode03.value = "";
				document.frm01.hBuilding03.value = "";
				document.frm01.hMooNo03.value ="";
				document.frm01.hMoobanCode03.value = "";
				document.frm01.hMooban03.value = "";
				document.frm01.hSoiCode03.value = "";
				document.frm01.hSoi03.value = "";
				document.frm01.hRoadCode03.value = "";
				document.frm01.hRoad03.value = "";				
				
				document.frm01.hTumbon03.value = "";
				document.frm01.hAmphur03.value = "";
				document.frm01.hProvince03.value = "";
				document.frm01.hZip03.value = "";		
				document.frm01.hTumbonCode03.value = "";
				document.frm01.hAmphurCode03.value = "";
				document.frm01.hProvinceCode03.value = "";
				document.frm01.hZipCode03.value = "";		
				document.frm01.hTel03.value = "";		
				document.frm01.hFax03.value = "";		
			}
			
		}
   
   }	

</script>
<?include "unset_all.php";?>
<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objCustomerList = New CustomerList;


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hKeywordCar !="" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";
	}else{
		$pstrCondition .="  ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";	
	}
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" IC.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$Month02."-%' ";
	}else{
		$pstrCondition .=" IC.date_verify LIKE '%-".$Month02."-%'   ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" IC.company_id = '".$hCompanyId."' ";	
	}
}

if($hSetMonth != 0 and $hSetMonth != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$hSetMonth."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '%-".$hSetMonth."-%' ";	
	}
}

if($hSetYear != 0 and $hSetYear != ""){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '".$hSetYear."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '".$hSetYear."-%' ";	
	}
}

if($hRegisterYear > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.register_year = '".$hRegisterYear."' ";
	}else{
		$pstrCondition .=" IC.register_year = '".$hRegisterYear."' ";	
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  = '".$hStartDate."' ) ";
		}
	}
}

$objCustomerList = new InsureCarList();
$objCustomerList->setFilter($pstrCondition);
$objCustomerList->setPageSize(PAGE_SIZE);
$objCustomerList->setPage($hPage);
$objCustomerList->setSortDefault(" date_verify ASC");
$objCustomerList->setSort($hSort);
$objCustomerList->loadSearch();



}//end hSearch


$pCurrentUrl =  "acard_prb_list.php?hCompanyId=$hCompanyId&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hSetMonth=$hSetMonth&hSetYear=$hSetYear&hCarType=$hCarType&hCarModel=$hCarModel&hRegisterYear=$hRegisterYear&hKeywordCar=$hKeywordCar&hKeyword=$hKeyword&hSaleId=$hSaleId&hSearch=$hSearch";
//$pCurrentUrl = "acardList.php?hKeywordCar=$hKeywordCar&hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType&Month02=$Month02&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hCarType=$hCarType&hCarModel=$hCarModel&hKeyword=$hKeyword&hSaleId=$hSaleId";
// for paging only (sort resets page viewing)
$pCurrentUrlLink = "acard_prb_list.php?hKeywordCar=".$hKeywordCar."YYYhSearch=".$hSearch."YYYhKeyword=".$hKeyword."YYYhType=".$hType."YYYMonth02=".$Month02."YYYDay=".$Day."YYYMonth=".$Month."YYYYear=".$Year."YYYDay01=".$Day01."YYYMonth01=".$Month01."YYYYear01=".$Year01."YYYhCarType=".$hCarType."YYYhCarModel=".$hCarModel."YYYhKeyword=".$hKeyword."YYYhSaleId=".$hSaleId."YYYhPage=".$hPage;

$pageTitle = "4. ������ʵ�ͤ�ú.";
$strHead03 = "���Ң������١���";
$pageContent = "4.10 �͡�ú.";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>
<tr>
	<td align="right">�ѹ��������ͧ : </td>
	<td>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
			<td class="error"></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="right">��͹������� : </td>
	<td>
		<select name="hSetMonth">
			<option value="0"> - �ء��͹ -
			<option value="01" <?if($hSetMonth == "01") echo "selected"?>>���Ҥ�
			<option value="02" <?if($hSetMonth == "02") echo "selected"?>>����Ҿѹ��
			<option value="03" <?if($hSetMonth == "03") echo "selected"?>>�չҤ�
			<option value="04" <?if($hSetMonth == "04") echo "selected"?>>����¹
			<option value="05" <?if($hSetMonth == "05") echo "selected"?>>����Ҥ�
			<option value="06" <?if($hSetMonth == "06") echo "selected"?>>�Զع�¹		
			<option value="07" <?if($hSetMonth == "07") echo "selected"?>>�á�Ҥ�
			<option value="08" <?if($hSetMonth == "08") echo "selected"?>>�ԧ�Ҥ�
			<option value="09" <?if($hSetMonth == "09") echo "selected"?>>�ѹ¹¹
			<option value="10" <?if($hSetMonth == "10") echo "selected"?>>���Ҥ�
			<option value="11" <?if($hSetMonth == "11") echo "selected"?>>��Ȩԡ�¹
			<option value="12" <?if($hSetMonth == "12") echo "selected"?>>�ѹ�Ҥ�																					
		</select>
		&nbsp;&nbsp;�յ������&nbsp;&nbsp;
		<?$dateNow = date("Y");?>
		<select name="hSetYear">
			<option value="0"> - �ء�� -
			<?for($i=($dateNow-10);$i<=($dateNow+1);$i++){?>
			<option value="<?=$i?>" <?if($hSetYear==$i) echo "selected"?>><?=$i?>
			<?}?>				
		</select>		
		
	</td>
</tr>
<tr>
	<td align="right">������ö :</td>
	<td>
			<table>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType","","����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td>���ö :</td>
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel","","����к�")?>
			</td>
			<td>��ö :</td>
			<td  class="i_background04" >
			<select name="hRegisterYear">
				<option value="0000"> - ����к� -
			<?for($i=date("Y");$i> (date("Y")-25);$i--){?>
				<option value="<?=$i?>" ><?=$i?>
			<?}?>
			</select>		
			</td>

			</table>	
	</td>
</tr>
<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	
<tr>
	<td align="right"> ��ѡ�ҹ��Ңͧ��¡�� :</td>
	<td class="small">     	
		<input type="hidden" name="hSaleId" value=""><input type="text" name="hSale" size="40" value="">
	</td>
</tr>	
<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
<div align="center">�� <?=$objCustomerList->mCount;?> ��¡��</div>
</form>	

<?if($objCustomerList->mCount > 0){?>

<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objCustomerList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objCustomerList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objCustomerList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"><input type="button" value="��辺�����Ź���Ң����� CRL" onclick="window.location='allList.php'"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="10%" align="center" class="ListTitle">��ö</td>
	<td width="10%" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td width="15%" align="center" class="ListTitle">����</td>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="10%" align="center" class="ListTitle">������ö</td>
	<td width="15%" align="center" class="ListTitle">Ẻö</td>
	<td width="10%" align="center" class="ListTitle">�Ţ����¹</td>
	<td width="10%" align="center" class="ListTitle">Sale</td>
	<td width="5%" align="center" class="ListTitle">�͡�ú</td>
</tr>
	<?
		$i=0;
		$checkYear =  date("Y")-1;
		forEach($objCustomerList->getItemList() as $objItem) {
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objItem->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objItem->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objItem->get_car_model_id());
		$objCarModel->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objItem->get_car_series_id());
		$objCarSeries->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" align="center" ><?=$objItem->get_register_year()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_date_verify())?></td>	
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  ><?=$objCustomer->getHomeTel()?> <?=$objCustomer->getMobile()?></td>
	<td valign="top"  align="center" ><?=$objCarType->getTitle()?></td>
	<td valign="top"   ><?=$objCarModel->getTitle()?></td>
	<td valign="top"  align="center" ><?=$objItem->get_code()?></td>
	<td valign="top"  align="center" ><?=$objMember->getFirstname()?></td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="�͡�ú.����" onclick="window.location='prb_new.php?hCarId=<?=$objItem->get_car_id()?>';"></td>		
</tr>
<?
$pstrCondition = "";
if($pstrCondition != ""){
	$pstrCondition.=" and  direct = 'yes' and IC.car_id='".$objItem->get_car_id()."' ";
}else{
	$pstrCondition.="  direct = 'yes'  and IC.car_id='".$objItem->get_car_id()."'  ";
}


$objInsureList = new InsureList();
$objInsureList->setFilter($pstrCondition);
$objInsureList->setPageSize(PAGE_SIZE);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" date_protect DESC ");
$objInsureList->setSort($hSort);
$objInsureList->loadList();
if($objInsureList->mCount > 0){

?>
<tr>
	<td width="5%" align="center" class="i_backgroundBlue">�յ������</td>
	<td width="5%" align="center" class="i_backgroundBlue">�ѹ������ͧ</td>
	<td width="10%" align="center" class="i_backgroundBlue">�á����</td>
	<td width="10%" align="center" class="i_backgroundBlue">��Сѹ���</td>
	<td width="10%" align="center" class="i_backgroundBlue">�����Ţ���������</td>
	<td width="10%" align="center" class="i_backgroundBlue">�����Ţ�ú.</td>
	<td width="10%" align="center" class="i_backgroundBlue">ʶҹ�</td>	
	<td width="5%" align="center" class="i_backgroundBlue">SALE</td>	

	<td width="5%" align="center" class="i_backgroundBlue">��������´</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem01) {
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem01->get_car_id());
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;
		
		$objMember = new Member();
		$objMember->setMemberId($objItem01->get_sale_id());
		$objMember->load();
		
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objInsureCar->get_customer_id());
		$objCustomer->load();
		
		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objItem01->get_p_prb_id());
		$objPrbCompany->load();
		
		$objSI = new StockInsure();
		$objSI->set_stock_insure_id($objItem01->get_p_stock_id());
		$objSI->load();
		
		$objB = new InsureBroker();
		$objB->setInsureBrokerId($objSI->get_broker_id());
		$objB->load();
		
		$objA = new InsureCompany();
		$objA->setInsureCompanyId($objSI->get_insure_company_id());
		$objA->load();			
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=$objItem01->get_p_year()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem01->get_p_start_date())?></td>	
	<td valign="top" ><?=$objB->getTitle()?></td>		
	<td valign="top" ><?=$objA->getTitle()?></td>		
	<td valign="top" ><?=$objSI->get_title()?></td>		
	<td valign="top" ><?=$objSI->get_title01()?></td>		
	<td valign="top"  align="center" ><?=$objItem01->get_status()?></td>
	<td valign="top"  align="center" ><?=$objMember->getFirstname()?></td>

	<td  valign="top" align="center">
	<input type="button"  class="button"  value="��䢾ú." onclick="window.location='prb_new.php?hInsureId=<?=$objItem01->get_insure_id()?>&hCarId=<?=$objItem->get_car_id()?>';">
	</td>	
</tr>
	<?
		++$i;
			}
	Unset($objInsureList);
	?>
<tr><td colspan="9"><br><br></td></tr>
<?}?>


	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?}else{?>
<div align="center">	��辺������</div>

<?}?>


<script>
	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});		
	
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
</script>
<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
<?
header('Content-Type: text/html; charset=utf-8');
include "common.php";

$objAreaAmphurList = new AmphurList();
$objAreaAmphurList->setFilter(" amphur_name LIKE '%".trim($q)."%' ");
$objAreaAmphurList->setPageSize(0);
$objAreaAmphurList->setSortDefault(" amphur_name ASC");
$objAreaAmphurList->loadUTF8();

forEach($objAreaAmphurList->getItemList() as $objItem) {
?>
<div onSelect="this.text.value='<?=$objItem->getAmphurName()?>';document.<?=$hForm?>.<?=$hAmphurCode?>.value='<?=$objItem->getAmphurCode()?>';document.<?=$hForm?>.<?=$hAmphur?>.value='<?=$objItem->getAmphurName()?>';document.<?=$hForm?>.<?=$hProvince?>.value='<?=$objItem->getProvinceName()?>';document.<?=$hForm?>.<?=$hProvinceCode?>.value='<?=$objItem->getProvinceCode()?>';document.<?=$hForm?>.<?=$hFocus?>.focus();"><span class='informal'></span><span><?=$objItem->getAmphurName()." ,".$objItem->getProvinceName()?></span></div>
<?}

unset($objAreaAmphurList);
?>
<?include "unset_all.php";?>
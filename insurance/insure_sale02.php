<?
include("common.php");
define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);
$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();

$objInsureOld = new Insure();
$objInsureList = new InsureList();
$objOrder = new Order();
$objInsureItem = new InsureItem();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

if(isset($hSubmit)){
	
	$objInsureOld->set_sale_id($sMemberId);
	$objInsureOld->set_car_id($hCarId);
	$objInsureOld->set_customer_id($objInsureCar->get_customer_id());
	$objInsureOld->set_company_id($sCompanyId);
	$objInsureOld->set_year_extend($hYearExtend);
	$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;
	$objInsureOld->set_date_protect($hDateProtect);
	$objInsureOld->set_k_broker_id($hKBrokerId);
	$objInsureOld->set_k_prb_id($hKPrbId);
	$objInsureOld->set_k_stock_id($hKStockId);
	$objInsureOld->set_k_number($hKNumber);
	$hKStartDate = $hKStartYear."-".$hKStartMonth."-".$hKStartDay;
	$objInsureOld->set_k_start_date($hKStartDate);
	$hKEndDate = $hKEndYear."-".$hKEndMonth."-".$hKEndDay;
	$objInsureOld->set_k_end_date($hKEndDate);
	$objInsureOld->set_p_broker_id($hPBrokerId);
	$objInsureOld->set_p_prb_id($hPPrbId);
	$objInsureOld->set_p_stock_id($hPStockId);
	$objInsureOld->set_p_number($hPNumber);
	$hPStartDate = $hPStartYear."-".$hPStartMonth."-".$hPStartDay;
	$objInsureOld->set_p_start_date($hPStartDate);
	$hPEndDate = $hPEndYear."-".$hPEndMonth."-".$hPEndDay;
	$objInsureOld->set_p_end_date($hPEndDate);
	$objInsureOld->set_k_num01(str_replace(",", "",$hFund));
	$objInsureOld->set_k_num02(str_replace(",", "", $hBeasuti));
	$objInsureOld->set_k_num03(str_replace(",", "", $hArgon));
	$objInsureOld->set_k_num04(str_replace(",", "", $hPasi));
	$objInsureOld->set_k_num05(str_replace(",", "", $hTotal01));
	$objInsureOld->set_k_num06(str_replace(",", "", $hTotal05));
	$objInsureOld->set_k_num07(str_replace(",", "", $hTotal02));
	$objInsureOld->set_k_num08(str_replace(",", "", $hTotal03));
	$objInsureOld->set_k_num09(str_replace(",", "", $hDiscountPercentPrice));
	$objInsureOld->set_k_num10(str_replace(",", "", $hDiscountOtherPrice));
	$objInsureOld->set_k_num11(str_replace(",", "", $hPrbPrice01));
	$objInsureOld->set_k_num12(str_replace(",", "", $hStockProductTotal));
	$objInsureOld->set_k_num13(str_replace(",", "", $hTotal));
	$objInsureOld->set_k_niti($hNiti);
	$objInsureOld->set_k_dis_type_01($hDiscountType01);
	$objInsureOld->set_k_dis_type_02($hDiscountType02);
	$objInsureOld->set_k_dis_type_03($hDiscountType03);
	$objInsureOld->set_k_dis_con_01($hDiscountPercent);
	$objInsureOld->set_k_dis_con_02($hDiscountOtherRemark);
	$objInsureOld->set_k_free($hFreeRemark);
	$objInsureOld->set_k_type($hInsureType);
	$objInsureOld->set_k_type_remark($hInsureTypeRemark);
	$objInsureOld->set_k_fix($hRepair);
	$objInsureOld->set_p_car_type($hInsureFeeId);
	$objInsureOld->set_p_prb_price(str_replace(",", "", $hPrbPrice));
	$objInsureOld->set_p_year($hCarStatus);
	$hCallDate = $YearReportDate."-".$MonthReportDate."-".$DayReportDate;
	$hGetDate = $YearRecieveReport."-".$MonthRecieveReport."-".$DayRecieveReport;
	$objInsureOld->set_p_call_date($hCallDate);
	$objInsureOld->set_p_get_date($hGetDate);
	$objInsureOld->set_p_call_number($hAccNo);
	$objInsureOld->set_p_remark($hRemark);
	$objInsureOld->set_pay_number($hPaymentAmount);
	$objInsureOld->set_pay_type($hPaymentCondition);	
	$objInsureOld->set_status("booking");	
	
	$hInsureId = $objInsureOld->add();
	
	if($hKStockId > 0){
		$objSI = new StockInsure();
		$objSI->set_stock_insure_id($hKStockId);
		$objSI->set_insure_id($hInsureId);
		$objSI->set_status_sale("booking");
		$objSI->set_price(str_replace(",", "", $hTotal01));
		$objSI->update_status_step05();
	}
	
	if($hPStockId > 0){
		$objSI = new StockInsure();
		$objSI->set_stock_insure_id($hPStockId);
		$objSI->set_insure_id($hInsureId);
		$objSI->set_status_sale("booking");
		$objSI->set_price(str_replace(",", "", $hPrbPrice01));
		$objSI->update_status_step05();
	}	
	
	for($x=1;$x<=$hPaymentAmount;$x++){	

		$objInsureItem->set_insure_id($hInsureId);
		$hPaymentDate01 = $_POST["hYear_$x"]."-".$_POST["hMonth_$x"]."-".$_POST["hDay_$x"];
		$objInsureItem->set_payment_date($hPaymentDate01);
		$objInsureItem->set_payment_price(str_replace(",", "", $_POST["hPaymentPrice_$x"] ));
		$objInsureItem->set_remark($_POST["hInsureRemark_$x"]);
		$objInsureItem->set_payment_order($x);
		$hInsureItemId = $objInsureItem->add();
		
			for($y=1;$y<=5;$y++){	
				$objInsureItemDetail = new InsureItemDetail();
				if(str_replace(",", "", $_POST["hPrice_".$x."_".$y]) > 0){
	
					$objInsureItemDetail->setOrderId($hInsureItemId);
					$objInsureItemDetail->setOrderExtraId($hInsureId);
					$objInsureItemDetail->setPaymentSubjectId($_POST["hPaymentSubjectId_".$x."_".$y]);
					$objInsureItemDetail->setPrice(str_replace(",", "", $_POST["hPrice_".$x."_".$y]));
					$objInsureItemDetail->setQty($_POST["hQty_".$x."_".$y]);
					$objInsureItemDetail->setRemark($_POST["hRemark_".$x."_".$y]);
					$objInsureItemDetail->setPayin($_POST["hPayin_".$x."_".$y]);
					$objInsureItemDetail->add();
				}
			}	
	}
	
	for($x=1;$x<=$hSPCount;$x++){
		if(isset($_POST["hStockProductCheck_".$x])){
			$objInsureFree = new InsureFree();
			$objInsureFree->set_insure_id($hInsureId);
			$objInsureFree->set_stock_product_id($_POST["hStockProductId_".$x]);
			$objInsureFree->set_title($_POST["hStockProductTitle_".$x]);
			$objInsureFree->set_qty($_POST["hStockProductQty_".$x]);
			$objInsureFree->set_price(str_replace(",", "", $_POST["hStockProductPrice_".$x]));
			$objInsureFree->add();
		}
	}

	$objInsure = new InsureCar();
	$objInsure->set_car_id($hCarId);
	$objInsure->set_operate("booking");
	$objInsure->updateOperate();
	
	header("location:step06List.php?hInsureId=$hInsureId");
	break;

}


$pageTitle = "2. �к���õԴ����١���";
$pageContent = "2.5 �ѹ�֡��â�¡�˹�����";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<table width="100%">
<tr>
	<td height="450" width="250" valign="top">
	<iframe src="leftbar_acard.php?hStep=call2&hPage=step05List.php" width="250" height="500" scrolling="Yes"></iframe>	
	</td>
	<td valign="top" >
		<table cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td><a href="step02List.php" class="link01">�ä��駷�� 1</a></td>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td ><a href="step03List.php" class="link01">��ʹ��Ҥ�</a></td>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td ><a href="step04List.php" class="link01">�Դ��â��</a></td>
			<td><img src="../images/circle02.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td><a href="step05List.php" class="link02">�ѹ�֡��â��</a></td>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td ><a href="step06List.php" class="link01">�ѹ�֡�Ѻ����</a></td>


		</tr>
		</table>
<?if($hCarId != ""){?>				
		
		<?include "insureDetail.php"?>
		
		
		<form name="frm01" action="step05List.php" method="post" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
		<input type="hidden" name="hSaveForm" value="Yes">		
		<input type="hidden" name="hCarId" value="<?=$hCarId?>">
		<input type="hidden" name="hYearExtend" value="<?=$hYearExtend?>">
		<input type="hidden" name="hSubmit" value="Yes">
		
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background"><strong>�����Ż�Сѹ���</strong></td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>	
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td><strong>��ʹ��Ҥ�</strong>&nbsp;</td>			
			<td >
			
			<?
			$objQ = new InsureQuotation();
			if( $objQ->loadByCondition(" year_extend= '$hYearExtend'  and car_id = $hCarId ") ){
				echo "<a target='_blank' href='quotationPrint.php?hId=".$objQ->get_quotation_id()."'>".$objQ->get_quotation_number()."</a>";
			}
			?>
			<input type="hidden" name="hQuotationId" value="<?=$objQ->get_quotation_id()?>">
			
			</td>
			<td ></td>
			<td ></td>
			<td class="error" align="right"><strong>�ѹ��������ͧ</strong>&nbsp;</td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayProtect value="<?=$DayProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthProtect value="<?=$MonthProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearProtect" value="<?=$YearProtect?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearProtect,DayProtect, MonthProtect, YearProtect,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td></td>			
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr>
			<td class="listTitle"><strong>�Ҥ��Ѥ��</strong></td>			
			<td class="listDetail"><?$objBrokerList->printSelectScript("hKBrokerId",$objInsureOld->get_k_broker_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelectScript("hKPrbId",$objInsureOld->get_k_prb_id(),"����к�");?>	</td>
			<td class="listDetail"><input type="hidden" name="hKStockId"  size="5" value="<?=$hKStockId;?>"><input type="text" name="hKNumber"  size="20" value="<?=$hKNumber;?>"></td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKStartDay value="<?=$hKStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKStartMonth value="<?=$hKStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKStartYear" value="<?=$hKStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKStartYear,hKStartDay, hKStartMonth, hKStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKEndDay value="<?=$hKEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKEndMonth value="<?=$hKEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKEndYear" value="<?=$hKStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKEndYear,hKEndDay, hKEndMonth, hKEndYear,popCal);return false"></td>		
				</tr>
				</table>			
			
			</td>		
		</tr>
		<tr>
			<td class="listTitle">
			<strong>�Ҥ�ѧ�Ѻ</strong><br>			
			</td>
			<td class="listDetail"><?$objBrokerList->printSelectScript("hPBrokerId",$hPBrokerId,"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hPPrbId",$hPPrbId,"����к�");?>		</td>
			<td class="listDetail"><input type="hidden" name="hPStockId"  size="5" value="<?=$hPStockId;?>"><input type="text" name="hPNumber"  size="20" value="<?=$hPNumber;?>">	</td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPStartDay value="<?=$hPStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPStartMonth value="<?=$hPStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPStartYear" value="<?=$hPStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPStartYear,hPStartDay, hPStartMonth, hPStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPEndDay value="<?=$hPEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPEndMonth value="<?=$hPEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPEndYear" value="<?=$hPEndYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPEndYear,hPEndDay, hPEndMonth, hPEndYear,popCal);return false"></td>		
				</tr>
				</table>		
			</td>		
		</tr>
		</table>

	<br>

</table>		


<table width="100%" class="search">
<tr>
	<td width="60%" align="center" class="listTitle" height="25"> �Ҥ�ѧ�Ѻ</td>
	<td width="40%" align="center" class="listtitle">�ͧ��</td>
</tr>
<tr>
	<td valign="top">
	
		<table>
		<tr>
			<td>������ö��Т�Ҵö¹��</td>
			<td>
				<select name="hInsureFeeId" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsureOld->get_p_car_type() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_title();?>
				<?}?>
				</select>
			
			</td>
		</tr>
		<tr>
			<td>�Ҥ� �ú.</td>
			<td><input type="text" name="hPrbPrice" style="text-align=right;"  onblur="tryNumberFormat(this);"  size="10" value="<?=$objInsureOld->get_p_prb_price();?>"></td>
		</tr>
		<tr>
			<td>�շ���ͻ�Сѹ</td>
			<td>
				<select name="hCarStatus">
					<option value="0">- ��س����͡ -
					<option value="�շ�� 1"  <?if($objInsureOld->get_p_year() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsureOld->get_p_year() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsureOld->get_p_year() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsureOld->get_p_year() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsureOld->get_p_year() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsureOld->get_p_year() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsureOld->get_p_year() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsureOld->get_p_year() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsureOld->get_p_year() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsureOld->get_p_year() == "�շ�� 10") echo "selected"?>>�շ�� 10
				</select>
			</td>
		</tr>
		<tr>
			<td>�ѹ����駧ҹ</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayReportDate value="<?=$DayReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthReportDate value="<?=$MonthReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearReportDate" value="<?=$YearReportDate?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearReportDate,DayReportDate, MonthReportDate, YearReportDate, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�ѹ����Ѻ��</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayRecieveReport value="<?=$DayRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthRecieveReport value="<?=$MonthRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearRecieveReport" value="<?=$YearRecieveReport?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearRecieveReport,DayRecieveReport, MonthRecieveReport, YearRecieveReport, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�Ţ�Ѻ��</td>
			<td><input type="text" name="hAccNo"  size="10" value="<?=$objInsureOld->get_p_call_number();?>"></td>
		</tr>
		<tr>
			<td valign="top">�����˵�</td>
			<td><textarea rows="3" cols="30" name="hRemark"><?=$objInsureOld->get_p_remark();?></textarea></td>
		</tr>
		</table>
	
		<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" class="listTitle" height="25"> �Ҥ��Ѥ��</td>
		</tr>
		</table>
	
	
		<table width="100%">
		<tr>
			<td colspan="2">�ع��Сѹ</td>			
			<td align="right"><input type="text" name="hFund" style="text-align=right;"  onblur="tryNumberFormat(this);" size="15" value="<?=$hFund;?>"></td>
		</tr>
		<tr>
			<td colspan="2">�����ط��................................................1)</td>
			<td align="right"><input type="text" name="hBeasuti" style="text-align=right;"  onblur="sumBea();sumAll();tryNumberFormat(this);"  size="15" value="<?=$hBeasuti;?>"></td>
		</tr>
		<tr>
			<td colspan="2">�ҡ�.....................................................2)</td>
			<td align="right"><input type="text" name="hArgon" style="text-align=right;"  onblur="sumBea();sumAll();tryNumberFormat(this);"   size="15" value="<?=$hArgon;?>"></td>
		</tr>
		<tr>
			<td colspan="2">Vat.......................................................3)</td>
			<td align="right"><input type="text" name="hPasi" style="text-align=right;"  readonly size="15" value="<?=$hPasi?>"></td>
		</tr>
		<tr>
			<td colspan="2">���»�Сѹ������</td>
			<td align="right"><input type="text" name="hTotal01" style="text-align=right;"  readonly size="15"   onblur=""  value="<?=$hTotal01;?>"></td>
		</tr>

		<tr>
			<td rowspan="2" valign="top">��ǹŴ</td>
			<td>
					<input type="checkbox" value="1" onclick="checkDiscount01();sumAll();" name="hDiscountType01" <?if($hDiscountType01=="1") echo "checked"?>>��ǹŴ&nbsp;<input type="text" name="hDiscountPercent" onBlur="checkDiscount01();sumAll();"  size="2" value="<?=$hDiscountPercent?>"> % 
			</td>
			<td valign="top" align="right"><input type="text" style="text-align=right;"  readonly name="hDiscountPercentPrice"  size="15" value="<?=$hDiscountPercentPrice;?>"></td>
		</tr>
		<tr>			
			<td><input type="checkbox" value="1"  onclick="checkDiscount02(this.value);sumAll();"  name="hDiscountType02" <?if($hDiscountType02=="1") echo "checked"?>>��ǹŴ���� �к� <input type="text" name="hDiscountOtherRemark"  disabled   size="20" value="<?=$hDiscountOtherRemark?>"></td>
			<td valign="top" align="right"><input type="text" style="text-align=right;" disabled  onblur="sumAll();tryNumberFormat(this);"  name="hDiscountOtherPrice"  size="15" value="<?=$hDiscountOtherPrice?>"></td>
		</tr>
		<tr>
			

		</tr>
		<tr>
			<td >��� �ú.</td>
			<td>
					<input type="checkbox" value="1"  onclick="checkDiscount03(this.value);sumAll();"  name="hDiscountType03" <?if($hDiscountType03=="1") echo "checked"?>>���ú.
			</td>
			<td align="right"><input type="text" name="hPrbPrice01" style="text-align=right;"  onblur="" size="15" value="<?=$hPrbPrice01;?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�����͹�ѡ���� � ������</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal04" style="text-align=right;"  readonly size="15" value="<?=$hTotal04?>"></td>
		</tr>
		<tr>
			<td valign="top" >�ѡ���� � ������</td>
			<td>
				<input type="radio" value="�ؤ�Ÿ�����" name="hNiti"  onclick="sumNiti01();sumAll();"  <?if($hNiti=="�ؤ�Ÿ�����") echo "checked"?> >�ؤ�Ÿ�����&nbsp;&nbsp;
				<br><input type="radio" value="�ԵԺؤ�� �ѡ����" name="hNiti" onclick="sumNiti();sumAll();" <?if($hNiti=="�ԵԺؤ�� �ѡ����") echo "checked"?>>�ԵԺؤ�� �ѡ���� 
				<br><input type="radio" value="�ԵԺؤ�� ����ѡ����" name="hNiti" onclick="sumNiti01();sumAll();" <?if($hNiti=="�ԵԺؤ�� ����ѡ����") echo "checked"?>>�ԵԺؤ�� ����ѡ���� 
				</td>
			<td align="right" valign="top"><input type="text" name="hTotal02"  style="text-align=right;"  readonly size="15" value="<?=$hTotal02?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�������ͧ���з�����</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal" readonly style="text-align=right;"  size="15" value="<?=$hTotal;?>"></td>
		</tr>	
		<tr>
			<td>��������Сѹ���</td>
			<td>
			<?
			$objInsureTypeList = new  InsureTypeList();
			$objInsureTypeList->setPageSize(0);
			$objInsureTypeList->setSort(" insure_type_id ASC ");
			$objInsureTypeList->load();
			
			$objInsureTypeList->printSelect("hInsureType",$hInsureType,"��س����͡");
			?>
			

				<input type="text" name="hInsureTypeRemark"  size="10" value="<?=$hInsureTypeRemark;?>">
			</td>
		</tr>		
		<tr>
			<td>��������ë���</td>
			<td><input type="radio" value="������ҧ" name="hRepair"  <?if($hRepair=="������ҧ") echo "checked"?>>������ҧ&nbsp;&nbsp;<input type="radio" value="�������" name="hRepair" <?if($hRepair=="�������") echo "checked"?>>�������</td>
		</tr>
		</table>

	</td>
	<td valign="top">

		<?
		$objStockProductGroup = new StockProductGroup();
		$objStockProductGroup->loadByCondition(" title = '���������' ");
		
		$objStockProductType = new StockProductType();
		$objStockProductType->loadByCondition(" title = '���������' ");
		
		
		$objStockProductList = new StockProductList();
		$objStockProductList->setFilter(" SP.stock_product_group_id='".$objStockProductGroup->getStockProductGroupId()."' AND SP.stock_product_type_id='".$objStockProductType->getStockProductTypeId()."'   ");
		$objStockProductList->setPageSize(0);
		$objStockProductList->setSort(" title ASC ");
		$objStockProductList->load();
		?>
		<table>
		<tr>
			<td class="listTitle" align="center" ></td>
			<td class="listTitle" align="center" ><strong>��¡��</strong></td>
			<td class="listTitle" align="center" ><strong>�ӹǹ</strong></td>
			<td class="listTitle" align="center" ><strong>�Ҥ�</strong></td>
			<td class="listTitle" align="center" ><strong>���</strong></td>			
		</tr>
		<?
		$sp = 0;
		forEach($objStockProductList->getItemList() as $objItem) {?>
		<tr>					
			<td class="listDetail"><input type="checkbox" onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td class="listDetail"><input type="hidden" size="1" name="hStockProductId_<?=$sp?>" value="<?=$objItem->getStockProductId()?>"><?=$objItem->getTitle()?></td>
			<td class="listDetail"><input type="text"  style="text-align=right;" onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" readonly style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);"   name="hStockProductPrice_<?=$sp?>" size="5" value="<?=number_format($objItem->getRetail(),2)?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;}?>
		<tr>
			<td class="listDetail"><input type="checkbox"  onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td>��� � �к� <input type="name" size="20" name="hStockProductTitle_<?=$sp?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);" name="hStockProductPrice_<?=$sp?>" size="5" value=""></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;?>
		<tr>
			<td class="listDetail"><input type="checkbox" onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td>��� � �к� <input type="name" size="20" name="hStockProductTitle_<?=$sp?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);" name="hStockProductPrice_<?=$sp?>" size="5" value=""></td>
			<td class="listDetail"><input type="text" style="text-align=right;" readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;?>
		<tr>
			<td class="listDetail"><input type="checkbox" onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td>��� � �к� <input type="name" size="20" name="hStockProductTitle_<?=$sp?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);" name="hStockProductPrice_<?=$sp?>" size="5" value=""></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;?>
		<tr>
			<td class="listDetail"><input type="checkbox" onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td>��� � �к� <input type="name" size="20" name="hStockProductTitle_<?=$sp?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);" name="hStockProductPrice_<?=$sp?>" size="5" value=""></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;?>
		<tr>
			<td class="listDetail"><input type="checkbox" onclick="sum_free()" value="1" name="hStockProductCheck_<?=$sp?>"></td>	
			<td>��� � �к� <input type="name" size="20" name="hStockProductTitle_<?=$sp?>"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();" name="hStockProductQty_<?=$sp?>" size="1" value="1"></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  onblur="sum_free();tryNumberFormat(this);" name="hStockProductPrice_<?=$sp?>" size="5" value=""></td>
			<td class="listDetail"><input type="text" style="text-align=right;"  readonly  name="hStockProductPriceSum_<?=$sp?>" size="5" value=""></td>
		</tr>
		<?$sp++;?>
		<tr>
			<td colspan="4" class="listTitle">���������</td>	
			<td class="listTitle"><input type="text" style="text-align=right;"  readonly   name="hStockProductTotal" size="5" value=""></td>
		</tr>
		</table>
		<input type="hidden" name="hSPCount" value="<?=$sp?>">
		
		
	
	
	</td>
</tr>
</table>



<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�ê��Ф�һ�Сѹ��� ���з����� 
		<select name="hPaymentAmount"  onchange="checkDisplay(this.value)" >
			<option value="0">��س����͡
			<option value="1" <?if($objInsureOld->get_pay_number() == "1") echo "selected";?>>1
			<option value="2" <?if($objInsureOld->get_pay_number() == "2") echo "selected";?>>2
			<option value="3" <?if($objInsureOld->get_pay_number() == "3") echo "selected";?>>3
			<option value="4" <?if($objInsureOld->get_pay_number() == "4") echo "selected";?>>4
			<option value="5" <?if($objInsureOld->get_pay_number() == "5") echo "selected";?>>5
			<option value="6" <?if($objInsureOld->get_pay_number() == "6") echo "selected";?>>6
			<option value="7" <?if($objInsureOld->get_pay_number() == "7") echo "selected";?>>7
			<option value="8" <?if($objInsureOld->get_pay_number() == "8") echo "selected";?>>8
			<option value="9" <?if($objInsureOld->get_pay_number() == "9") echo "selected";?>>9

		</select>	
	  �Ǵ
	  &nbsp;&nbsp;&nbsp;<strong>���͹䢡�â��</strong>
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_pay_type() == "�������") echo "checked";?> value="�������"> �������
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_pay_type() == "�觨���") echo "checked";?> value="�觨���"> �觨���
	  </td>
</tr>
</table>


<table width="100%" cellpadding="3" cellspacing="1" class="i_background02" border="0">
<?for($i=1;$i<=9;$i++){?>
<tr id="var_<?=$i?>_1" style="display:none">
	<td class="listDetail" colspan="4"><strong>��������´����Ѻ���ЧǴ��� <?=$i?></strong></td>
	<td class="listDetail" align="right">�ѹ���Ѵ���� :</td>
	<td class="listDetail" align="center">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name="hDay_<?=$i?>" value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name="hMonth_<?=$i?>" value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hYear_<?=$i?>" value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear_<?=$i?>,hDay_<?=$i?>, hMonth_<?=$i?>, hYear_<?=$i?>,popCal);return false"></td>		
		</tr>
		</table>	
	</td>		
</tr>
<tr id="var_<?=$i?>_2" style="display:none">
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle" >�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle" >�ӹǹ�Թ</td>
</tr>
<?for($j=1;$j<=5;$j++){?>
<tr id="bar_<?=$i?>_<?=$j?>" style="display:none">
	<td align="center" class="ListDetail"><?=$j?></td>
	<td align="center class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$i."_".$j,"","��س����͡");?><input type="text" size="20" name="hRemark_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$i?>);"  size="5" id="hQty_<?=$i?>_<?=$j?>" name="hQty_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  id="hPayin_<?=$i?>_<?=$j?>" name="hPayin_<?=$i?>_<?=$j?>" value="1"  ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  size="15"  id="hPrice_<?=$i?>_<?=$j?>"  name="hPrice_<?=$i?>_<?=$j?>" value=""></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="15" id="hPriceSum_<?=$i?>_<?=$j?>" name="hPriceSum_<?=$i?>_<?=$j?>" value=""></td>
</tr>	
<?}?>
<tr id="var_<?=$i?>_3" style="display:none">
	<td class="listTitle" colspan="4">�����˵� : <input type="text" id="hInsureRemark_<?=$i?>" name="hInsureRemark_<?=$i?>"  size="50" value=""></td>
	<td class="listTitle" align="right">������ЧǴ��� 1 :</td>
	<td class="listTitle" align="center"><input type="text" readonly style="text-align=right;" id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>"  size="15" value=""></td>		
</tr>
<?}?>
<tr id="var_sum"  style="display:none" >
	<td colspan="4"></td>
	<td align="right">��������� :</td>
	<td align="center"><input type="text" readonly name="hInsurePrice" style="text-align=right;"  onblur="checkSum();"  size="15" value=""></td>
</tr>
</table>
<br><br>
		<div align="center"><input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();"></div>
		
		
		</form>


	
<?}else{?>	
		<table class="search" width="100%">
		<tr>
			<td height="300" align="center" valign="middle">
				<h5 class="error">��س����͡��¡�÷���ͧ��úѹ�֡�ҡ��ҹ�������</h5>
				
			</td>
		</tr>
		</table>
<?}?>

	
	</td>
</tr>
</table>

<?
	include("h_footer.php")
?>
<script>
	new CAPXOUS.AutoComplete("hKNumber", function() {
		return "auto_kom.php?hValue=hKStockId&hBrokerId="+document.frm01.hKBrokerId.value+"&hInsureCompanyId="+document.frm01.hKPrbId.value+"&q="+this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hPNumber", function() {
		return "auto_prb.php?hValue=hPStockId&hBrokerId="+document.frm01.hPBrokerId.value+"&hInsureCompanyId="+document.frm01.hPPrbId.value+"&q="+this.text.value;
	});	
</script>

<script>
	function checkSubmit(){

		if (document.forms.frm01.DayProtect.value=="" || document.forms.frm01.DayProtect.value=="00")
		{
			alert("��س��к��ѹ��������ͧ");
			document.forms.frm01.DayProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.DayProtect.value,1,31) == false) {
				document.forms.frm01.DayProtect.focus();
				return false;
			}
		} 		
	
		if (document.forms.frm01.MonthProtect.value==""  || document.forms.frm01.MonthProtect.value=="00")
		{
			alert("��س��к���͹��������ͧ");
			document.forms.frm01.MonthProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.MonthProtect.value,1,12) == false){
				document.forms.frm01.MonthProtect.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.YearProtect.value==""  || document.forms.frm01.YearProtect.value=="0000")
		{
			alert("��س��кػշ�������ͧ");
			document.forms.frm01.YearProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.YearProtect.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.YearProtect.focus();
				return false;
			}
		} 			
	
		if(document.frm01.hKPrbId.value ==0 && document.frm01.hPPrbId.value ==0 && document.frm01.hKBrokerId.value ==0  && document.frm01.hPBrokerId.value ==0 ){
			alert("��س����͡����ѷ��Сѹ���ҧ����ҧ˹��");
			document.frm01.hKBrokerId.focus();
			return false;
		}
	
		if(document.frm01.hKPrbId.value > 0 || document.frm01.hKBrokerId.value > 0){
			
			if(document.frm01.hKNumber.value == ""){
				alert("��س��к������Ţ��������");
				document.frm01.hKNumber.focus();
				return false;
			}
			
			if (document.forms.frm01.hKStartDay.value=="" || document.forms.frm01.hKStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hKStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartDay.value,1,31) == false) {
					document.forms.frm01.hKStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKStartMonth.value==""  || document.forms.frm01.hKStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hKStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartMonth.value,1,12) == false){
					document.forms.frm01.hKStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKStartYear.value==""  || document.forms.frm01.hKStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hKStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hKEndDay.value=="" || document.forms.frm01.hKEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hKEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndDay.value,1,31) == false) {
					document.forms.frm01.hKEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKEndMonth.value==""  || document.forms.frm01.hKEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hKEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndMonth.value,1,12) == false){
					document.forms.frm01.hKEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKEndYear.value==""  || document.forms.frm01.hKEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hKEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKEndYear.focus();
					return false;
				}
			} 						
			
		}
	
	
		if(document.frm01.hPPrbId.value > 0 || document.frm01.hPBrokerId.value > 0){
			
			if(document.frm01.hPNumber.value == ""){
				alert("��س��к������Ţ��������");
				document.frm01.hPNumber.focus();
				return false;
			}
			
			if (document.forms.frm01.hPStartDay.value=="" || document.forms.frm01.hPStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hPStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartDay.value,1,31) == false) {
					document.forms.frm01.hPStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPStartMonth.value==""  || document.forms.frm01.hPStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hPStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartMonth.value,1,12) == false){
					document.forms.frm01.hPStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPStartYear.value==""  || document.forms.frm01.hPStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hPStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hPEndDay.value=="" || document.forms.frm01.hPEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hPEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndDay.value,1,31) == false) {
					document.forms.frm01.hPEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPEndMonth.value==""  || document.forms.frm01.hPEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hPEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndMonth.value,1,12) == false){
					document.forms.frm01.hPEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPEndYear.value==""  || document.forms.frm01.hPEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hPEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPEndYear.focus();
					return false;
				}
			} 						
			
		}
	
	
		if(document.frm01.hFund.value <= 0){
			alert("��س��кطع��Сѹ");
			document.frm01.hFund.focus();
			return false;
		}		
	
		if(document.frm01.hBeasuti.value <=0){
			alert("��س��к������ط��");
			document.frm01.hBeasuti.focus();
			return false;
		}		
	
		if(document.frm01.hArgon.value <=0){
			alert("��س��к��ҡ�");
			document.frm01.hArgon.focus();
			return false;
		}			
	
		if(document.frm01.hNiti[0].checked == false && document.frm01.hNiti[1].checked == false && document.frm01.hNiti[2].checked == false ){
			alert("��س����͡������������");
			document.frm01.hNiti[0].focus();
			return false;		
		}
		
		if(document.frm01.hTotal.value <=0){
			alert("��������դ����ҧ");
			document.frm01.hTotal.focus();
			return false;
		}			
	
		if(document.frm01.hInsureType.value < 0 ){
			alert("��س����͡��������Сѹ���");
			document.frm01.hInsureType.focus();
			return false;		
		}
		
		if(document.frm01.hRepair[0].checked == false && document.frm01.hRepair[1].checked == false ){
			alert("��س����͡��������ë���");
			document.frm01.hRepair[0].focus();
			return false;		
		}				
		
		if(document.frm01.hInsureFeeId.value ==0){
			alert("��س����͡������ö��Т�Ҵö¹��");
			document.frm01.hInsureFeeId.focus();
			return false;
		}	
		
		if(document.frm01.hPrbPrice.value <=0){
			alert("��س��к��Ҥ� �ú.");
			document.frm01.hPrbPrice.focus();
			return false;
		}	
		
		if(document.frm01.hCarStatus.value ==0){
			alert("��س����͡�շ���ͻ�Сѹ");
			document.frm01.hCarStatus.focus();
			return false;
		}			
				
		if(document.frm01.hPaymentAmount.value ==0){
			alert("��س����͡�ӹǹ�Ǵ������");
			document.frm01.hPaymentAmount.focus();
			return false;
		}		

		if(document.frm01.hPaymentCondition[0].checked == false && document.frm01.hPaymentCondition[1].checked == false ){
			alert("��س����͡���͹䢡�â��");
			document.frm01.hPaymentCondition[0].focus();
			return false;		
		}		
		
		if(document.frm01.hInsurePrice.value != document.frm01.hTotal.value){
			alert("��¡�ê������ç�Ѻ��Ҿú");
			return false;
		}		
		
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
		
			document.frm01.hTotal01.disabled = false;
			document.frm01.hTotal02.disabled = false;
			document.frm01.hDiscountPercentPrice.disabled = false;
			document.frm01.hDiscountOtherPrice.disabled = false;
			document.frm01.hPrbPrice01.disabled = false;
			document.frm01.hTotal.disabled = false;
		
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
		
	}
		

	function checkDisplay(var_num){

			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "none";
			document.getElementById("var_<?=$i?>_2").style.display = "none";
			document.getElementById("var_<?=$i?>_3").style.display = "none";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "none";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "none";

		if(var_num ==1){
			<?for($i=1;$i<=1;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==2){
			<?for($i=1;$i<=2;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";	
		}
		if(var_num ==3){
			<?for($i=1;$i<=3;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";		
		}
		if(var_num ==4){
			<?for($i=1;$i<=4;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==5){
			<?for($i=1;$i<=5;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==6){
			<?for($i=1;$i<=6;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";		
		}
		if(var_num ==7){
			<?for($i=1;$i<=7;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==8){
			<?for($i=1;$i<=8;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==9){
			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		
		
	}
	
	function checkSum(){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		num06=0;
		num07=0;
		num08=0;
		num09=0;				
		if(document.frm01.hPaymentPrice_1){	num01 = rip_comma(document.frm01.hPaymentPrice_1.value)*1;	}
		if(document.frm01.hPaymentPrice_2){	num02 = rip_comma(document.frm01.hPaymentPrice_2.value)*1;	}
		if(document.frm01.hPaymentPrice_3){	num03 = rip_comma(document.frm01.hPaymentPrice_3.value)*1;	}
		if(document.frm01.hPaymentPrice_4){	num04 = rip_comma(document.frm01.hPaymentPrice_4.value)*1;	}
		if(document.frm01.hPaymentPrice_5){	num05 = rip_comma(document.frm01.hPaymentPrice_5.value)*1;	}
		if(document.frm01.hPaymentPrice_6){	num06 = rip_comma(document.frm01.hPaymentPrice_6.value)*1;	}
		if(document.frm01.hPaymentPrice_7){	num07 = rip_comma(document.frm01.hPaymentPrice_7.value)*1;	}
		if(document.frm01.hPaymentPrice_8){	num08 = rip_comma(document.frm01.hPaymentPrice_8.value)*1;	}
		if(document.frm01.hPaymentPrice_9){	num09 = rip_comma(document.frm01.hPaymentPrice_9.value)*1;	}														
				
		sumnum = num01+num02+num03+num04+num05+num06+num07+num08+num09;
		document.frm01.hInsurePrice.value=formatCurrency(sumnum);
	
	}	
	
	function sumBea(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		if(document.frm01.hBeasuti){
			num01 = rip_comma(document.frm01.hBeasuti.value)*1;
		}
		if(document.frm01.hArgon){
			num02 = rip_comma(document.frm01.hArgon.value)*1;
		}		
		num03 = (num01+num02)*(7/100);		
		document.frm01.hPasi.value=formatCurrency(num03);
		
		sumnum = num01+num02+num03;
		
		document.frm01.hTotal01.value=formatCurrency(sumnum);
		
		//sumtotal = (num01+num02)*0.01;
		//document.frm01.hTotal02.value=formatCurrency(sumtotal);
		
	}	
	
	function sumNiti(){
		var num01;
		
		num01=0;
		num02=0;
		if(document.frm01.hBeasuti){ num01 = rip_comma(document.frm01.hBeasuti.value)*1;}
		if(document.frm01.hArgon){num02 = rip_comma(document.frm01.hArgon.value)*1;}		
		
		sumtotal = (num01+num02)*0.01;
		document.frm01.hTotal02.value=formatCurrency(sumtotal);
	
	}	
	
	function sumNiti01(){

		document.frm01.hTotal02.disabled=false;
		document.frm01.hTotal02.value="0.00";
		document.frm01.hTotal02.disabled=true;
	}	
	
	
	function checkDiscount01(val){
		var num01, num02;
		val = document.frm01.hDiscountType01.checked;
		if(val == true){
			num01=0;
			num02=0;
			document.frm01.hDiscountPercent.disabled=false;				
			if(document.frm01.hDiscountPercent){ num01 = rip_comma(document.frm01.hDiscountPercent.value)*1;}
			if(document.frm01.hBeasuti){ num02 = rip_comma(document.frm01.hBeasuti.value)*1;}
			sumnum = (num02*num01)/100;
			document.frm01.hDiscountPercentPrice.value=formatCurrency(sumnum);
		}else{
			document.frm01.hDiscountPercent.value="";	
			document.frm01.hDiscountPercent.disabled=true;	
			document.frm01.hDiscountPercentPrice.value = "0.00";
			document.frm01.hDiscountPercentPrice.disabled=true;		
		}
		
		
		//document.frm01.hDiscountOtherRemark.value= "";
		//document.frm01.hDiscountOtherPrice.value = "0.00";
		//document.frm01.hDiscountOtherRemark.disabled=true;
		//document.frm01.hDiscountOtherPrice.disabled=true;		
	
	}	
	
	function checkDiscount02(val){
		val = document.frm01.hDiscountType02.checked;
		if(val == true){
			//document.frm01.hDiscountPercent.disabled=true;	
			document.frm01.hDiscountOtherRemark.disabled=false;
			document.frm01.hDiscountOtherPrice.disabled=false;
			//document.frm01.hDiscountPercent.value= "";
			//document.frm01.hDiscountPercentPrice.value= "0.00";
		}else{
			
			document.frm01.hDiscountOtherRemark.value="";
			document.frm01.hDiscountOtherPrice.value="0.00";		
			document.frm01.hDiscountOtherRemark.disabled=false;
			document.frm01.hDiscountOtherPrice.disabled=false;		
		}
	}	
	
	function checkDiscount03(val){
		if(val == true){

		}else{

		}	
		/*
		document.frm01.hDiscountPercent.disabled=true;	
		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		document.frm01.hDiscountOtherRemark.disabled=true;
		document.frm01.hDiscountOtherPrice.disabled=true;		
		*/
	}	
	
	function sumAll(){		
		num01=0;
		num02=0;
		num03=0;
		num04=0;	
		num05=0;	
		num06=0;	
		num07=0;	
		num08=0;	
		num09=0;
	
		sumBea();
		if(document.frm01.hTotal01){
			num01 = rip_comma(document.frm01.hTotal01.value)*1;
		}
		if(document.frm01.hTotal02){
			//���� � ������
			num02 = rip_comma(document.frm01.hTotal02.value)*1;
		}
		
		if(document.frm01.hDiscountPercentPrice){
			num04 = rip_comma(document.frm01.hDiscountPercentPrice.value)*1;
		}
		if(document.frm01.hDiscountOtherPrice){
			num05 = rip_comma(document.frm01.hDiscountOtherPrice.value)*1;
		}
		if(document.frm01.hPrbPrice01){
			num06 = rip_comma(document.frm01.hPrbPrice01.value)*1;
		}

		if(document.frm01.hDiscountType03.checked == true){
			//alert(num01+" "+num02+" "+num04+" "+num05+" "+num06);
		
		
			num07 = num01-num02-num04-num05;
			num10 = num01-num04-num05;
		}else{
			num07 = num01-num02-num04-num05+num06;
			num10 = num01-num04-num05+num06;
		}
		document.frm01.hTotal.value= formatCurrency(num07);
		document.frm01.hTotal04.value= formatCurrency(num10);

	}
	
	
	function same_prb(){
		var prb_id = document.frm01.hStockPrbId.value;
	
		if( prb_id > 0){
			document.frm01.hInsureCompanyId01.value = document.frm01.hInsureCompanyId.value;
			document.frm01.hStockKomId.value = document.frm01.hStockPrbId.value;
		
		}
	
	}
	
	function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPrbPrice.value = <?=$objItem->get_total()?>;
			document.frm01.hPrbPrice01.value = <?=$objItem->get_total()?>;
		}
	<?}?>
		sumAll();
	}
	
	<?if($objInsureOld->get_pay_number() > 0){?>
		checkDisplay(<?=$objInsureOld->get_pay_number()?>);
		checkSum();
	<?}?>
	
	
	function sum_var(val){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=false;
		
		for(i=1;i<=5;i++){
			
			
			num01 =	document.getElementById("hQty_"+val+"_"+i).value;
			num02 =	rip_comma(document.getElementById("hPrice_"+val+"_"+i).value);
			num05 = document.getElementById("hPayin_"+val+"_"+i).checked;
			if(num05 == true){
				num03=(num01*num02)*-1;
			}else{
				num03=(num01*num02);			
			}
			document.getElementById("hPriceSum_"+val+"_"+i).value= formatCurrency(num03);
			num04 = num04+num03;
		}
		document.getElementById("hPaymentPrice_"+val).value= formatCurrency(num04);
		
		checkSum();
		
	}
	
	function sum_free(){
		total=0;
		totalAll=0;
		
		for(i=0;i<<?=$sp?>;i++){
			if(document.getElementById("hStockProductCheck_"+i).checked == true){
				qty = document.getElementById("hStockProductQty_"+i).value;
				price = rip_comma(document.getElementById("hStockProductPrice_"+i).value);
				total = qty*price;
				document.getElementById("hStockProductPriceSum_"+i).value =formatCurrency(total);
			}else{
				document.getElementById("hStockProductPriceSum_"+i).value ="0.00";			
			}			
		}
	
		for(i=0;i<<?=$sp?>;i++){
			totalAll = totalAll+rip_comma(document.getElementById("hStockProductPriceSum_"+i).value);
		}
		document.getElementById("hStockProductTotal").value =formatCurrency(totalAll);
	}
	
	
</script>
<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
	
	</script>
	<?include "unset_all.php";?>
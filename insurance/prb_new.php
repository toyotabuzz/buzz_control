<?
include("common.php");
define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);

$objSale = new Member();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();

$objInsureOld = new Insure();
$objInsureList = new InsureList();
$objOrder = new Order();
$objInsureItem = new InsureItem();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort("  use_type, code, title ASC");
$objInsureFeeList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

if(!isset($hSubmit)){

if($hInsureId != ""){
	$objInsureOld->set_insure_id($hInsureId);
	$objInsureOld->load();
	
	$objSale->setMemberId($objInsureOld->get_sale_id());
	$objSale->load();
	
	$objOfficer = new Member();
	$objOfficer->setMemberId($objInsureOld->get_officer_id());
	$objOfficer->load();
	
	$arrDate = explode("-",$objInsureOld->get_officer_date());
	$hOfficerDay = $arrDate[2];
	$hOfficerMonth = $arrDate[1];
	$hOfficerYear = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_insure_date());
	$hInsureDay = $arrDate[2];
	$hInsureMonth = $arrDate[1];
	$hInsureYear = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_date_protect());
	$DayProtect = $arrDate[2];
	$MonthProtect = $arrDate[1];
	$YearProtect = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_p_start_date());
	$hPStartDay = $arrDate[2];
	$hPStartMonth = $arrDate[1];
	$hPStartYear = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_p_end_date());
	$hPEndDay = $arrDate[2];
	$hPEndMonth = $arrDate[1];
	$hPEndYear = $arrDate[0];

	$arrDate = explode("-",$objInsureOld->get_k_start_date());
	$hKStartDay = $arrDate[2];
	$hKStartMonth = $arrDate[1];
	$hKStartYear = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_k_end_date());
	$hKEndDay = $arrDate[2];
	$hKEndMonth = $arrDate[1];
	$hKEndYear = $arrDate[0];

	
	$arrDate = explode("-",$objInsureOld->get_p_rnum_date());
	$hPRnumDay = $arrDate[2];
	$hPRnumMonth = $arrDate[1];
	$hPRnumYear = $arrDate[0];
	
	$strMode = "Update";
}else{
	$strMode = "Add";
	$hInsureDay = date("d");
	$hInsureMonth = date("m");
	$hInsureYear = date("Y");
	
	$hOfficerDay = date("d");
	$hOfficerMonth = date("m");
	$hOfficerYear = date("Y");	
	
	$hOfficerId = $sMemberId;
	$objOfficer = new Member();
	$objOfficer->setMemberId($sMemberId);
	$objOfficer->load();
	$objInsureOld->set_officer_id($sMemberId);
	
}

}else{


if(isset($hSubmit)){
	$objInsureOld->set_insure_id($hInsureId);
	$objInsureOld->set_sale_id($hSaleId);
	$objInsureOld->set_car_id($hCarId);
	$objInsureOld->set_customer_id($objInsureCar->get_insure_customer_id());
	$objInsureOld->set_company_id($sCompanyId);
	
	$objM = new Member();
	$objM->setMemberId($hSaleId);
	$objM->load();
	
	$objTeam = new InsureTeam();
	$objTeam->setInsureTeamId($objM->getTeam());
	$objTeam->load();
	$objInsureOld->set_acc_company_id($objTeam->getAccCompanyId());
	
	
	$objInsureOld->set_year_extend($hInsureYear);
	$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;
	$objInsureOld->set_date_protect($hDateProtect);
	$objInsureOld->set_k_check($hSale02);
	$objInsureOld->set_k_broker_id($hKBrokerId);
	$objInsureOld->set_k_prb_id($hKPrbId);
	$objInsureOld->set_k_stock_id($hKStockId);
	$objInsureOld->set_k_number($hKNumber);
	$hKStartDate = $hKStartYear."-".$hKStartMonth."-".$hKStartDay;
	$objInsureOld->set_k_start_date($hKStartDate);
	$hKEndDate = $hKEndYear."-".$hKEndMonth."-".$hKEndDay;
	$objInsureOld->set_k_end_date($hKEndDate);
	$objInsureOld->set_k_frees($hKFrees);
	$objInsureOld->set_p_check($hSale01);
	$objInsureOld->set_p_broker_id($hPBrokerId);
	$objInsureOld->set_p_prb_id($hPPrbId);
	$objInsureOld->set_p_stock_id($hPStockId);
	$objInsureOld->set_p_number($hPNumber);
	$hPStartDate = $hPStartYear."-".$hPStartMonth."-".$hPStartDay;
	$objInsureOld->set_p_start_date($hPStartDate);
	$hPEndDate = $hPEndYear."-".$hPEndMonth."-".$hPEndDay;
	$objInsureOld->set_p_end_date($hPEndDate);
	$objInsureOld->set_p_frees($hPFrees);
	
	$objInsureOld->set_k_num01(str_replace(",", "",$hFund));
	$objInsureOld->set_k_num02(str_replace(",", "", $hBeasuti));
	$objInsureOld->set_k_num03(str_replace(",", "", $hArgon));
	$objInsureOld->set_k_num04(str_replace(",", "", $hPasi));
	$objInsureOld->set_k_num05(str_replace(",", "", $hTotal01));
	$objInsureOld->set_k_num06(str_replace(",", "", $hTotal05));
	$objInsureOld->set_k_num07(str_replace(",", "", $hTotal02));
	$objInsureOld->set_k_num08(str_replace(",", "", $hTotal03));
	$objInsureOld->set_k_num09(str_replace(",", "", $hDiscountPercentPrice));
	$objInsureOld->set_k_num10(str_replace(",", "", $hDiscountOtherPrice));
	$objInsureOld->set_k_num11(str_replace(",", "", $hPrbPrice01));
	$objInsureOld->set_k_num12(str_replace(",", "", $hStockProductTotal));
	$objInsureOld->set_k_num13(str_replace(",", "", $hTotal));
	$objInsureOld->set_k_niti($hNiti);
	$objInsureOld->set_k_dis_type_01($hDiscountType01);
	$objInsureOld->set_k_dis_type_02($hDiscountType02);
	$objInsureOld->set_k_dis_type_03($hDiscountType03);
	$objInsureOld->set_k_dis_con_01($hDiscountPercent);
	$objInsureOld->set_k_dis_con_02($hDiscountOtherRemark);
	$objInsureOld->set_k_free($hFreeRemark);
	$objInsureOld->set_k_type($hInsureType);
	$objInsureOld->set_k_type_remark($hInsureTypeRemark);
	$objInsureOld->set_k_fix($hRepair);
	$objInsureOld->set_p_car_type($hInsureFeeId);
	$objInsureOld->set_p_prb_price(str_replace(",", "", $hPrbPrice));
	$objInsureOld->set_p_year($hCarStatus);
	$hCallDate = $YearReportDate."-".$MonthReportDate."-".$DayReportDate;
	$hGetDate = $YearRecieveReport."-".$MonthRecieveReport."-".$DayRecieveReport;
	$objInsureOld->set_p_call_date($hCallDate);
	$objInsureOld->set_p_get_date($hGetDate);
	$objInsureOld->set_p_call_number($hAccNo);
	$objInsureOld->set_p_remark($hRemark);
	$objInsureOld->set_p_insure_vat(str_replace(",", "",$hPInsureVat));
	$objInsureOld->set_p_argon(str_replace(",", "",$hPArgon));
	$objInsureOld->set_p_total(str_replace(",", "",$hPTotal));
	
	$objInsureOld->set_pay_number($hPaymentAmount);
	$objInsureOld->set_pay_type($hPaymentCondition);
	$objInsureOld->set_status("booking");
	$objInsureOld->set_direct("yes");
	
	$hInsureDate = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
	$objInsureOld->set_insure_date($hInsureDate);
	$objInsureOld->set_p_rnum($hPRNum);
	$objInsureOld->set_k_new($hKNew);
	$objInsureOld->set_officer_id($hOfficerId);
	$hOfficerDate = $hOfficerYear."-".$hOfficerMonth."-".$hOfficerDay;
	$objInsureOld->set_officer_date($hOfficerDate);
	$objInsureOld->set_product_type($hProductType);	
	
	if($strMode == "Add"){	
		$hInsureId = $objInsureOld->add();
		//echo " insure_id=   ".$hInsureId;
		if($hKStockId > 0){
			$objSI = new StockInsure();
			$objSI->set_stock_insure_id($hKStockId);
			$objSI->set_insure_id($hInsureId);
			$objSI->set_status_sale("���");
			$objSI->set_price(str_replace(",", "", $hTotal01));
			$objSI->update_status_step05();
		}
		
		if($hPStockId > 0){
			$objSI = new StockInsure();
			$objSI->set_stock_insure_id($hPStockId);
			$objSI->set_insure_id($hInsureId);
			$objSI->set_status_sale("���");
			$objSI->set_price(str_replace(",", "", $hPrbPrice01));
			$objSI->update_status_step05();
		}	
		
		$objInsure = new InsureCar();
		$objInsure->set_car_id($hCarId);
		$objInsure->set_date_verify($hDateProtect);
		$objInsure->set_operate("ccard");
		$objInsure->set_sale_id($hSaleId);
		$objInsure->updateOperate();
	}else{
	 	$objInsureOld->update();
	}
	
	
	
	header("location:prb_new.php?hId=$hInsureId&hCarId=$hCarId&hInsureId=$hInsureId");
	exit;

}

}


$pageTitle = "1. �к��������١���";
$pageContent = "1.8 �ѹ�֡�Ѻ�ú. ö�������ᴧ�����";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<table width="100%">
<tr>
	<td valign="top" >

	
	
	<br><br>
		<div class="error" >ʶҹоú : <?=$objInsureOld->get_status();?>
		<?if($objInsureOld->get_status() == "cancel"){
		$objSI = new StockInsure();
		$objSI->set_stock_insure_id($objInsureOld->get_p_stock_id());
		$objSI->load();
		echo "  ʵ�ͤ�Ѩ�غѹ : ".$objSI->get_status();
		echo "  ʶҹ� : ".$objSI->get_status_sale();
		echo "  ��һ�Ѻ : ".$objSI->get_cprice()." �ҷ ";
		echo "  �ѹ���¡��ԡ : ".formatShortDate($objSI->get_cancel_date());
		echo "  �����˵� : ".$objSI->get_stock_remark();
		?>
		
		<?}?>
		</div>
	<br><br>
	
<?if($hCarId != ""){?>				
		
		<?include "insureDetail.php"?>
		
		
<form name="frm01" action="prb_new.php" method="post" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
<input type="hidden" name="hSaveForm" value="Yes">		
<input type="hidden" name="hCarId" value="<?=$hCarId?>">
<input type="hidden" name="hYearExtend" value="<?=$hYearExtend?>">
<input type="hidden" name="strMode" value="<?=$strMode?>">
<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
<input type="hidden" name="hSubmit" value="Yes">
		
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background"><strong>�����Ż�Сѹ���</strong></td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>	
		<table width="100%">
		<tr>
			<td>
			��¡�ù���ա���ʹ͢�´ѧ��� 
			<br>
			<input type="checkbox" name="hSale01" <?if($strMode=="update"){if($objInsureOld->get_p_check() == 1){?> checked<?}}else{?>checked<?}?> value="1" onclick="check_sale(1);"> ����Ҥ�ѧ�Ѻ 
			<br>
			<input type="checkbox" name="hSale02"  <?if($objInsureOld->get_k_check() == 1){?> checked<?}?> value="1"  onclick="check_sale(2);"> ����Ҥ��Ѥ��
			</td>
			<td align="right">
				<table>
				<tr>
					<td class="listTitle" align="center">��ѡ�ҹ��á��</td>
					<td class="listTitle" align="center">�ѹ����͡�ú.</td>
					<td class="listTitle" align="center">����Ңͧ��¡��</td>
					<td class="listTitle" align="center">�ѹ����駧ҹ</td>
					<td class="listTitle" align="center">�Ţ�Ѻ��</td>
				</tr>
				<tr>
					<td><input type="text" readonly size="2" name="hOfficerId" value="<?=$objInsureOld->get_officer_id()?>"><input type="text" name="hOfficer" size="20" value="<?=$objOfficer->getFirstname()." ".$objOfficer->getLastname()?>"></td>
					<td>
						<table>
						<td><INPUT align="middle" size="2" maxlength="2"   name=hOfficerDay value="<?=$hOfficerDay?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="2" maxlength="2"  name=hOfficerMonth value="<?=$hOfficerMonth?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hOfficerYear" value="<?=$hOfficerYear?>"></td>
						<td>&nbsp;</td>
						<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hOfficerYear,hOfficerDay, hOfficerMonth, hOfficerYear,popCal);return false"></td>							
						</table>
					</td>
					<td><input type="text" readonly size="2" name="hSaleId" value="<?=$objInsureOld->get_sale_id()?>"><input type="text" name="hSale" size="20" value="<?=$objSale->getFirstname()." ".$objSale->getLastname()?>"></td>
					<td>
						<table>
						<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay value="<?=$hInsureDay?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth value="<?=$hInsureMonth?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hInsureYear" value="<?=$hInsureYear?>"></td>
						<td>&nbsp;</td>
						<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear,hInsureDay, hInsureMonth, hInsureYear,popCal);return false"></td>							
						</table>
					</td>
					<td><INPUT align="middle" size="20" name="hPRNum" value="<?=$objInsureOld->get_p_rnum()?>"></td>
				</tr>
				</table>

				
			</td>			
		</tr>
		</table>

	
	
		
		 
	
	<table width="100%" id="sale_p" >
		<tr>
			<td bgcolor="#000080" height="25" ><font color=white><strong> �Ҥ�ѧ�Ѻ</td>
		</tr>
		<tr>
			<td>
			
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>	
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr >
			<td class="listDetail"><?$objBrokerList->printSelectScript("hPBrokerId",$objInsureOld->get_p_broker_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hPPrbId",$objInsureOld->get_p_prb_id(),"����к�");?>		</td>
			<td class="listDetail"><input type="text" readonly name="hPStockId"  size="3" value="<?=$objInsureOld->get_p_stock_id();?>"><input type="text" name="hPNumber"  size="30" value="<?=$objInsureOld->get_p_number();?>">	
			<br>
			<input type="checkbox" name="hDisplayBarcode" value="1"> ������Ţ��������
			</td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPStartDay value="<?=$hPStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPStartMonth value="<?=$hPStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPStartYear" value="<?=$hPStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPStartYear,hPStartDay, hPStartMonth, hPStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPEndDay value="<?=$hPEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPEndMonth value="<?=$hPEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPEndYear" value="<?=$hPEndYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPEndYear,hPEndDay, hPEndMonth, hPEndYear,popCal);return false"></td>		
				</tr>
				</table>		
			</td>		
		</tr>
		</table>
		
		
			</td>		
		</tr>
		<tr><td>
		
		
		<table width="100%" cellpadding="2" cellspacing="2" >

		<tr>
			<td class="listDetail">������ö��Т�Ҵö¹��</td>
			<td class="listDetail" align="right">
				<?
				$objInsureFee = new InsureFee();
				$objInsureFee->set_insure_fee_id($objInsureOld->get_p_car_type());
				$objInsureFee->load();
				?>
				<input type="text" name="hInsureFeeCode" size="40" onchange="checkPrb();" value="<?=$objInsureFee->get_code();?>">
				<select name="hInsureFeeId" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsureOld->get_p_car_type() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_use_type();?> >><?=$objItem->get_code();?>, <?=$objItem->get_title();?>, <font color=red>[<?=$objItem->get_total();?>]</font>
				<?}?>
				</select>
			
			</td>
		</tr>

		<tr>
			<td>�շ���ͻ�Сѹ</td>
			<td align="right">
				<select name="hCarStatus">
					<option value="0">- ��س����͡ -
					<option value="�շ�� 1"  <?if($objInsureOld->get_p_year() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsureOld->get_p_year() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsureOld->get_p_year() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsureOld->get_p_year() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsureOld->get_p_year() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsureOld->get_p_year() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsureOld->get_p_year() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsureOld->get_p_year() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsureOld->get_p_year() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsureOld->get_p_year() == "�շ�� 10") echo "selected"?>>�շ�� 10
				</select>
			</td>
		</tr>
		<tr>
			<td class="listDetail">��������â��</td>
			<td class="listDetail" align="right">
				<input type="radio" value=4 name="hPFrees" <?if($objInsureOld->get_p_frees() == "4") echo "checked"?> >�١��ҫ��� &nbsp;&nbsp;
				<input type="radio" value=1 name="hPFrees" <?if($objInsureOld->get_p_frees() == "1") echo "checked"?>>Dealer ��&nbsp;&nbsp;
				<input type="radio" value=2 name="hPFrees" <?if($objInsureOld->get_p_frees() == "2") echo "checked"?>>TMT ��&nbsp;&nbsp;
				<input type="radio" value=3 name="hPFrees" <?if($objInsureOld->get_p_frees() == "3") echo "checked"?>>F/N ��&nbsp;&nbsp;
				<input type="radio" value=5 name="hPFrees" <?if($objInsureOld->get_p_frees() == "5") echo "checked"?>>��Сѹ�����
			</td>
		</tr>
	    <tr> 
	      <td  valign="top">���»�Сѹ����ط��:</td>
	      <td  align="right"> 
	       	<input type="text" name="hPTotal"  style="text-align=right;"   onblur="tryNumberFormat(this);"  size="15" value="<?=$objInsureOld->get_p_total();?>">
	      </td>
	    </tr>
	    <tr> 
	      <td class="listDetail" valign="top">������Ť������:</td>
	      <td class="listDetail" align="right"> 
	       	<input type="text" name="hPInsureVat"  style="text-align=right;"   onblur="tryNumberFormat(this);"  size="15" value="<?=$objInsureOld->get_p_insure_vat();?>">
	      </td>
	    </tr>
	    <tr> 
	      <td valign="top">�ҡ��ʵ���:</td>
	      <td  align="right"> 
	       	<input type="text" name="hPArgon"  style="text-align=right;"   onblur="tryNumberFormat(this);"  size="15" value="<?=$objInsureOld->get_p_argon();?>">
	      </td>
	    </tr>

		<tr>
			<td class="listDetail02"><strong>����Թ�Ҥ�ѧ�Ѻ</strong></td>
			<td align="right" class="listDetail02"><input type="text" name="hPrbPrice" style="text-align=right;" onchange="document.frm01.hPrbPrice01.value=this.value;checkSum();"  onblur="tryNumberFormat(this);"  size="15" value="<?=$objInsureOld->get_p_prb_price();?>"></td>
		</tr>
		</table>
		<input type="hidden" value="<?=$objInsureOld->get_p_prb_price();?>" name="hPrbPrice01">
		
		
		
		
		
		</td></tr>
		
		
	</table>

	
	
	<table width="100%" id="sale_k" <?if($objInsureOld->get_k_check() == 1){?> checked<?}else{?>style="display:none"<?}?>>
		<tr>
			<td bgcolor="#2F630C" height="25" ><font color=white><strong>�Ҥ��Ѥ��</strong></td>
		</tr>
		<tr>
			<td>
			
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>	
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr >
			<td class="listDetail"><?$objBrokerList->printSelectScript("hKBrokerId",$objInsureOld->get_k_broker_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelectScript("hKPrbId",$objInsureOld->get_k_prb_id(),"����к�");?>	</td>
			<td class="listDetail"><input type="hidden" name="hKStockId"  size="5" value="<?=$objInsureOld->get_k_stock_id();?>"><input type="text" name="hKNumber"  size="20" value="<?=$objInsureOld->get_k_number();?>"></td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKStartDay value="<?=$hKStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKStartMonth value="<?=$hKStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKStartYear" value="<?=$hKStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKStartYear,hKStartDay, hKStartMonth, hKStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKEndDay value="<?=$hKEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKEndMonth value="<?=$hKEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKEndYear" value="<?=$hKEndYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKEndYear,hKEndDay, hKEndMonth, hKEndYear,popCal);return false"></td>		
				</tr>
				</table>			
			
			</td>		
		</tr>
		</table>
		
		
			</td>		
		</tr>
		<tr>
		<td>
		
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  align="center" class="listTitle" height="25" colspan="3"> �Ҥ��Ѥ��</td>
		</tr>
		<tr>
			<td class="listDetail"  >��û�Сѹ���</td>
			<td  class="listDetail"  colspan="2" align="right" >
				<input type="radio" value="��Сѹ�������" name="hKNew" <?if($objInsureOld->get_k_new() == "��Сѹ�������") echo "checked"?>>��Сѹ�������&nbsp;&nbsp;
				<input type="radio" value="�������" name="hKNew" <?if($objInsureOld->get_k_new() == "�������") echo "checked"?>>�������&nbsp;&nbsp;
			</td>
		</tr>		
		<tr>
			<td >��������â��</td>
			<td colspan="2" align="right" >
				<input type="radio" value=4 name="hKFrees" <?if($objInsureOld->get_k_frees() == "4") echo "checked"?>>�١��ҫ��� &nbsp;&nbsp;
				<input type="radio" value=1 name="hKFrees" <?if($objInsureOld->get_k_frees() == "1") echo "checked"?>>Dealer ��&nbsp;&nbsp;
				<input type="radio" value=2 name="hKFrees" <?if($objInsureOld->get_k_frees() == "2") echo "checked"?>>TMT ��&nbsp;&nbsp;
				<input type="radio" value=3 name="hKFrees" <?if($objInsureOld->get_k_frees() == "3") echo "checked"?>>F/N ��&nbsp;&nbsp;
				<input type="radio" value=5 name="hKFrees" <?if($objInsureOld->get_k_frees() == "5") echo "checked"?>>��Сѹ�����
			</td>
		</tr>
		<tr>
			<td class="listDetail" colspan="2">�ع��Сѹ</td>			
			<td class="listDetail"  align="right"><input type="text" name="hFund" style="text-align=right;"  onblur="tryNumberFormat(this);" size="15" value="<?=number_format($objInsureOld->get_k_num01(),2);?>"></td>
		</tr>
		<tr>
			<td colspan="2">�����ط��................................................1)</td>
			<td align="right"><input type="text" name="hBeasuti" style="text-align=right;"  onblur="tryNumberFormat(this);"  size="15" value="<?=number_format($objInsureOld->get_k_num02(),2);?>"></td>
		</tr>
		<tr>
			<td class="listDetail"  colspan="2">�ҡ�.....................................................2)</td>
			<td class="listDetail"  align="right"><input type="text" name="hArgon" style="text-align=right;"  onblur="tryNumberFormat(this);"   size="15" value="<?=number_format($objInsureOld->get_k_num03(),2);?>"></td>
		</tr>
		<tr>
			<td colspan="2">Vat.......................................................3)</td>
			<td align="right"><input type="text" name="hPasi" style="text-align=right;"  onblur="tryNumberFormat(this);"    size="15" value="<?=number_format($objInsureOld->get_k_num04(),2)?>"></td>
		</tr>
		<tr>
			<td class="listDetail"  colspan="2">���»�Сѹ������</td>
			<td class="listDetail"  align="right"><input type="text" name="hTotal01" style="text-align=right;" onblur="tryNumberFormat(this);sumAll();"    size="15"     value="<?=number_format($objInsureOld->get_k_num05(),2);?>"></td>
		</tr>

		<tr>			
			<td class="listDetail" colspan="2"><input type="checkbox" value="1"  onclick="checkDiscount02(this.value);sumAll();"  name="hDiscountType02" <?if($objInsureOld->get_k_dis_type_02()=="1") echo "checked"?>>��ǹŴ���� �к� <input type="text" name="hDiscountOtherRemark"  disabled   size="20" value="<?=$objInsureOld->get_k_dis_con_02();?>"></td>
			<td  class="listDetail" valign="top" align="right"><input type="text" style="text-align=right;" disabled  onblur="tryNumberFormat(this);sumAll();"  name="hDiscountOtherPrice"  size="15" value="<?=number_format($objInsureOld->get_k_num10(),2);?>"></td>
		</tr>
		<tr>
			

		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�����͹�ѡ���� � ������</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal04" style="text-align=right;"  readonly size="15" value="<?=number_format( ($objInsureOld->get_k_num13()+$objInsureOld->get_k_num07()),2);?>"></td>
		</tr>
		<tr>
			<td valign="top" >�ѡ���� � ������</td>
			<td>
				<input type="radio" value="�ؤ�Ÿ�����" name="hNiti"  onclick="sumNiti01();sumAll();"  <?if($objInsureOld->get_k_niti()=="�ؤ�Ÿ�����") echo "checked"?> >�ؤ�Ÿ�����
				&nbsp;&nbsp;<input type="radio" value="�ԵԺؤ�� �ѡ����" name="hNiti" onclick="sumNiti();sumAll();" <?if($objInsureOld->get_k_niti()=="�ԵԺؤ�� �ѡ����") echo "checked"?>>�ԵԺؤ�� �ѡ���� 
				&nbsp;&nbsp;<input type="radio" value="�ԵԺؤ�� ����ѡ����" name="hNiti" onclick="sumNiti01();sumAll();" <?if($objInsureOld->get_k_niti()=="�ԵԺؤ�� ����ѡ����") echo "checked"?>>�ԵԺؤ�� ����ѡ���� 
				</td>
			<td align="right" valign="top"><input type="text" name="hTotal02"  style="text-align=right;"  readonly size="15" value="<?=number_format($objInsureOld->get_k_num07(),2);?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�������ͧ���з�����</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal" readonly style="text-align=right;"  size="15" value="<?=number_format($objInsureOld->get_k_num13(),2);?>"></td>
		</tr>	
		<tr>
			<td>��������Сѹ���</td>
			<td>
			<?
			$objInsureTypeList = new  InsureTypeList();
			$objInsureTypeList->setPageSize(0);
			$objInsureTypeList->setSort(" insure_type_id ASC ");
			$objInsureTypeList->load();
			
			$objInsureTypeList->printSelect("hInsureType",$objInsureOld->get_k_type(),"��س����͡");
			?>
			

				<input type="text" name="hInsureTypeRemark"  size="10" value="<?=$objInsureOld->get_k_type_remark();?>">
			</td>
		</tr>		
		<tr>
			<td>��������ë���</td>
			<td><input type="radio" value="������ҧ" name="hRepair"  <?if($objInsureOld->get_k_fix()=="������ҧ") echo "checked"?>>������ҧ
					&nbsp;&nbsp;<input type="radio" value="�������" name="hRepair" <?if($objInsureOld->get_k_fix()=="�������") echo "checked"?>>�������
					&nbsp;&nbsp;<input type="radio" value="GOA" name="hRepair" <?if($objInsureOld->get_k_fix()=="GOA") echo "checked"?>>GOA
					
			</td>
		</tr>
		<tr>
			<td>�������������Ե�ѳ��</td>
			<td><input type="text" name="hProductType" size="30" value="<?=$objInsureOld->get_product_type()?>"></td>
		</tr>		
		</table>

		
		</td>
		</tr>		
	</table>





<?
$objM01 = new Member();
$objM01->setMemberId($sMemberId);
$objM01->load();
if($objM01->getInsureEditor() == "1" and $hInsureId > 0 ){
?>



<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($hInsureId > 0 ){?>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_p_send() == 0){?><input type="button" value="���ش" onclick="window.open('insure_p_damage.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_p_send() == 0){?><input type="button" value="¡��ԡ" onclick="window.open('insure_p_cancel.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?if($objInsureOld->get_status() == "ccard" or $objInsureOld->get_status() == "booking" ){?><input type="button" value="�׹ʶҹ���������" onclick="window.open('insure_c_cancel.php?hId=<?=$hInsureId?>',null,'height=450,width=800,status=yes,scrollbars=yes,toolbar=no,menubar=no,location=no')"><?}?>


<?}?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<br><br>
<table cellpadding="5">
<tr>
	<td>
	
	<table width="100%" align="center">
	<tr>
		<td><strong>���͡�������㹡���͡�ú</strong> </td>
	</tr>
	<tr>
		<td>
	<input type="radio" name="hAddressType" value=1 checked> �������Ѩ�غѹ
	<br><input type="radio" name="hAddressType" value=2> �������������¹��ҹ
	<br><input type="radio" name="hAddressType" value=3> ���������ӧҹ
	<br><input type="radio" name="hAddressType" value=4> ������������͡���
		
		</td>

	</tr>
	<tr>
		<Td>
	<?
			if($objInsureOld->get_p_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsureOld->get_p_prb_id());
				$objB->load();
				if(strtoupper($objB->getShortTitle()) =="BKI"){?>
				<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_02.php?hId=<?=$hInsureId?>&hFormType=0501')" >
				<?}else{?>
					<?if(strtoupper($objB->getShortTitle()) =="SMK"){?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_03.php?hId=<?=$hInsureId?>&hFormType=0501')" >
					<?}else{?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_01.php?hId=<?=$hInsureId?>&hFormType=0501')" >					
					<?}?>
				<?
			}}
	?>		
		</TD>

	</tr>
	</table>	
	
	</td>
	<td>
	<?if($objM01->getInsureEditor() == "1" and $hInsureId > 0 ){?>
	<table width="100%" align="center">
	<tr>
		<td><strong>���͡�������㹡���͡㺤Ӣ���һ�Сѹ���ö¹��</strong> </td>
	</tr>
	<tr>
		<td>
	<input type="radio" id="hAddressType1" name="hAddressType1" value=1 checked> �������Ѩ�غѹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=2> �������������¹��ҹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=3> ���������ӧҹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=4> ������������͡���
		
		</td>
	</tr>
	<tr>
		<Td>
	<input type="button" value="㺤Ӣ���һ�Сѹ���ö¹��" name="hInform" onclick="PrintDoc('frm_02_07.php?hInsureId=<?=$hInsureId?>&hCarId=<?=$hCarId?>')"  >
		</TD>
	</tr>
	</table>	
	<?}?>
	</td>
</tr>
</table>





<?}else{?>



	<?if( $hInsureId > 0 ){?>
<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				
<table cellpadding="5">
<tr>
	<td>
	
	<table width="100%" align="center">
	<tr>
		<td><strong>���͡�������㹡���͡�ú</strong> </td>
	</tr>
	<tr>
		<td>
	<input type="radio" name="hAddressType" value=1 checked> �������Ѩ�غѹ
	<br><input type="radio" name="hAddressType" value=2> �������������¹��ҹ
	<br><input type="radio" name="hAddressType" value=3> ���������ӧҹ
	<br><input type="radio" name="hAddressType" value=4> ������������͡���
		
		</td>

	</tr>
	<tr>
		<Td>
	<?
			if($objInsureOld->get_p_prb_id() > 0){
				$objB = new InsureCompany();
				$objB->setInsureCompanyId($objInsureOld->get_p_prb_id());
				$objB->load();
				if(strtoupper($objB->getShortTitle()) =="BKI"){?>
				<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_02.php?hId=<?=$hInsureId?>&hFormType=0501')" >
				<?}else{?>
					<?if(strtoupper($objB->getShortTitle()) =="SMK"){?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_03.php?hId=<?=$hInsureId?>&hFormType=0501')" >
					<?}else{?>
						<input type="button" value="����� �ú." onclick="PrintPrb('frm_05_01.php?hId=<?=$hInsureId?>&hFormType=0501')" >					
					<?}?>
				<?
			}}
	?>		
		</TD>

	</tr>
	</table>	
	
	</td>
	<td>
	<?if($objM01->getInsureEditor() == "1" and $hInsureId > 0 ){?>
	<table width="100%" align="center">
	<tr>
		<td><strong>���͡�������㹡���͡㺤Ӣ���һ�Сѹ���ö¹��</strong> </td>
	</tr>
	<tr>
		<td>
	<input type="radio" id="hAddressType1" name="hAddressType1" value=1 checked> �������Ѩ�غѹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=2> �������������¹��ҹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=3> ���������ӧҹ
	<br><input type="radio" id="hAddressType1" name="hAddressType1" value=4> ������������͡���
		
		</td>
	</tr>
	<tr>
		<Td>
	<input type="button" value="㺤Ӣ���һ�Сѹ���ö¹��" name="hInform" onclick="PrintDoc('frm_02_07.php?hInsureId=<?=$hInsureId?>&hCarId=<?=$hCarId?>')"  >
		</TD>
	</tr>
	</table>	
	<?}?>
	</td>
</tr>
</table>

	
	

	
	<?}else{?>
<br><br>
<div align="center">
<input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?}?>


<?}?>
<input type="button" value="��Ѻ�����¡��" name="hCheck" onclick="window.location='acard_prb_list.php';">
</div>
		
		
</form>




<?}else{?>	
		<table class="search" width="100%">
		<tr>
			<td height="300" align="center" valign="middle">
				<h5 class="error">��辺��¡�÷���ͧ���</h5>
				
			</td>
		</tr>
		</table>
<?}?>

	
	</td>
</tr>
</table>

<?
	include("h_footer.php")
?>


<script>
	new CAPXOUS.AutoComplete("hOfficer", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&hField=hOfficerId&q=" + this.text.value;
		}
	});		

	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hInsureFeeCode", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_fee.php?hFrm=frm01&q=" + this.text.value;
		}
	});			
/*
	new CAPXOUS.AutoComplete("hKNumber", function() {
		return "auto_kom.php?hValue=hKStockId&hBrokerId="+document.frm01.hKBrokerId.value+"&hInsureCompanyId="+document.frm01.hKPrbId.value+"&q="+this.text.value;
	});
	*/
	new CAPXOUS.AutoComplete("hPNumber", function() {
		return "auto_prb.php?hValue=hPStockId&hBrokerId="+document.frm01.hPBrokerId.value+"&hInsureCompanyId="+document.frm01.hPPrbId.value+"&q="+this.text.value;
	});	
</script>

<script>
	function checkSubmit(){

	
	
	
		if(document.frm01.hSale01.checked == false && document.frm01.hSale02.checked ==false){
			alert("��س����͡�ҹ��º���ѷ��Сѹ���ҧ����ҧ˹��");
			document.frm01.hSale01.focus();
			return false;
		}

		if(document.frm01.hOfficerId.value < 1 ){
			alert("��س����͡�ҹ��ѡ�ҹ���С��");
			document.frm01.hOfficer.focus();
			return false;
		}

		if(document.frm01.hCarStatus.value  == "�շ�� 1" &&  document.frm01.hSaleId.value < 1 ){
			alert("��س����͡�ҹ��ѡ�ҹ���");
			document.frm01.hSale.focus();
			return false;
		}
		
			if (document.forms.frm01.hInsureDay.value=="" || document.forms.frm01.hInsureDay.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureDay.value,1,31) == false) {
					document.forms.frm01.hInsureDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hInsureMonth.value==""  || document.forms.frm01.hInsureMonth.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureMonth.value,1,12) == false){
					document.forms.frm01.hInsureMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hInsureYear.value==""  || document.forms.frm01.hInsureYear.value=="0000")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hInsureYear.focus();
					return false;
				}
			} 				
	
	
	
	
		if(document.frm01.hSale01.checked== true){
		//�Ҥ�ѧ�Ѻ
		

			
			if (document.forms.frm01.hPStartDay.value=="" || document.forms.frm01.hPStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hPStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartDay.value,1,31) == false) {
					document.forms.frm01.hPStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPStartMonth.value==""  || document.forms.frm01.hPStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hPStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartMonth.value,1,12) == false){
					document.forms.frm01.hPStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPStartYear.value==""  || document.forms.frm01.hPStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hPStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hPEndDay.value=="" || document.forms.frm01.hPEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hPEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndDay.value,1,31) == false) {
					document.forms.frm01.hPEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPEndMonth.value==""  || document.forms.frm01.hPEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hPEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndMonth.value,1,12) == false){
					document.forms.frm01.hPEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPEndYear.value==""  || document.forms.frm01.hPEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hPEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPEndYear.focus();
					return false;
				}
			} 					
		
		
			if(document.frm01.hInsureFeeId.value ==0){
				alert("��س����͡������ö��Т�Ҵö¹��");
				document.frm01.hInsureFeeId.focus();
				return false;
			}	
			
			if(document.frm01.hPFrees[0].checked==false && document.frm01.hPFrees[1].checked==false && document.frm01.hPFrees[2].checked==false && document.frm01.hPFrees[3].checked==false){
				alert("��س����͡��������â��");
				document.frm01.hPFrees[0].focus();
				return false;
			}	
			
			if(document.frm01.hCarStatus.value ==0){
				alert("��س����͡�շ���ͻ�Сѹ");
				document.frm01.hCarStatus.focus();
				return false;
			}			
		
				if(document.frm01.hPInsureVat.value <= 0){
					alert("��س��к�������Ť������");
					document.frm01.hPInsureVat.focus();
					return false;
				}		
			
				if(document.frm01.hPTotal.value <=0){
					alert("��س��к������ط��");
					document.frm01.hPTotal.focus();
					return false;
				}		
			
				if(document.frm01.hPArgon.value <=0){
					alert("��س��к��ҡ�");
					document.frm01.hPArgon.focus();
					return false;
				}	
		
				if(document.frm01.hPrbPrice.value <=0){
					alert("��س��к��Ҥ� �ú.");
					document.frm01.hPrbPrice.focus();
					return false;
				}	
		

		
		}
		
		if(document.frm01.hSale02.checked==true){
			//�Ҥ��Ѥ��
			if(document.frm01.hKPrbId.value == "0"){
				alert("��س��кغ���ѷ��Сѹ���");
				document.frm01.hKPrbId.focus();
				return false;
			}
		
			if (document.forms.frm01.hKStartDay.value=="" || document.forms.frm01.hKStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hKStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartDay.value,1,31) == false) {
					document.forms.frm01.hKStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKStartMonth.value==""  || document.forms.frm01.hKStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hKStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartMonth.value,1,12) == false){
					document.forms.frm01.hKStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKStartYear.value==""  || document.forms.frm01.hKStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hKStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hKEndDay.value=="" || document.forms.frm01.hKEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hKEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndDay.value,1,31) == false) {
					document.forms.frm01.hKEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKEndMonth.value==""  || document.forms.frm01.hKEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hKEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndMonth.value,1,12) == false){
					document.forms.frm01.hKEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKEndYear.value==""  || document.forms.frm01.hKEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hKEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKEndYear.focus();
					return false;
				}
			} 				
		
			if(document.frm01.hKFrees[0].checked==false && document.frm01.hKFrees[1].checked==false && document.frm01.hKFrees[2].checked==false && document.frm01.hKFrees[3].checked==false){
				alert("��س����͡��������â��");
				document.frm01.hKFrees[0].focus();
				return false;
			}	
		
			/*
		
				if(document.frm01.hFund.value <= 0){
					alert("��س��кطع��Сѹ");
					document.frm01.hFund.focus();
					return false;
				}		
			
				if(document.frm01.hBeasuti.value <=0){
					alert("��س��к������ط��");
					document.frm01.hBeasuti.focus();
					return false;
				}		
			
				if(document.frm01.hArgon.value <=0){
					alert("��س��к��ҡ�");
					document.frm01.hArgon.focus();
					return false;
				}			
			
				if(document.frm01.hNiti[0].checked == false && document.frm01.hNiti[1].checked == false && document.frm01.hNiti[2].checked == false ){
					alert("��س����͡������������");
					document.frm01.hNiti[0].focus();
					return false;		
				}
				
				if(document.frm01.hTotal.value <=0){
					alert("��������դ����ҧ");
					document.frm01.hTotal.focus();
					return false;
				}			
				
				*/
			
				if(document.frm01.hInsureType.value == 0 ){
					alert("��س����͡��������Сѹ���");
					document.frm01.hInsureType.focus();
					return false;		
				}
				
				if(document.frm01.hRepair[0].checked == false && document.frm01.hRepair[1].checked == false  && document.frm01.hRepair[2].checked == false ){
					alert("��س����͡��������ë���");
					document.frm01.hRepair[0].focus();
					return false;		
				}			
				
		
		
		
		}
		

		
		
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
		
			document.frm01.hTotal01.disabled = false;
			document.frm01.hTotal02.disabled = false;
			document.frm01.hTotal.disabled = false;
		
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
		
	}
		

	function checkDisplay(var_num){

			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "none";
			document.getElementById("var_<?=$i?>_2").style.display = "none";
			document.getElementById("var_<?=$i?>_3").style.display = "none";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "none";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "none";

		if(var_num ==1){
			<?for($i=1;$i<=1;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==2){
			<?for($i=1;$i<=2;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";	
		}
		if(var_num ==3){
			<?for($i=1;$i<=3;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";		
		}
		if(var_num ==4){
			<?for($i=1;$i<=4;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==5){
			<?for($i=1;$i<=5;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==6){
			<?for($i=1;$i<=6;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";		
		}
		if(var_num ==7){
			<?for($i=1;$i<=7;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==8){
			<?for($i=1;$i<=8;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==9){
			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		
		
	}
	
	function checkSum(){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		num06=0;
		num07=0;
		num08=0;
		num09=0;				
		if(document.frm01.hPaymentPrice_1){	num01 = rip_comma(document.frm01.hPaymentPrice_1.value)*1;	}
		if(document.frm01.hPaymentPrice_2){	num02 = rip_comma(document.frm01.hPaymentPrice_2.value)*1;	}
		if(document.frm01.hPaymentPrice_3){	num03 = rip_comma(document.frm01.hPaymentPrice_3.value)*1;	}
		if(document.frm01.hPaymentPrice_4){	num04 = rip_comma(document.frm01.hPaymentPrice_4.value)*1;	}
		if(document.frm01.hPaymentPrice_5){	num05 = rip_comma(document.frm01.hPaymentPrice_5.value)*1;	}
		if(document.frm01.hPaymentPrice_6){	num06 = rip_comma(document.frm01.hPaymentPrice_6.value)*1;	}
		if(document.frm01.hPaymentPrice_7){	num07 = rip_comma(document.frm01.hPaymentPrice_7.value)*1;	}
		if(document.frm01.hPaymentPrice_8){	num08 = rip_comma(document.frm01.hPaymentPrice_8.value)*1;	}
		if(document.frm01.hPaymentPrice_9){	num09 = rip_comma(document.frm01.hPaymentPrice_9.value)*1;	}														
				
		sumnum = num01+num02+num03+num04+num05+num06+num07+num08+num09;
		document.frm01.hInsurePrice.value=formatCurrency(sumnum);
	
	}	
	
	function sumBea(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		if(document.frm01.hBeasuti){
			num01 = rip_comma(document.frm01.hBeasuti.value)*1;
		}
		if(document.frm01.hArgon){
			num02 = rip_comma(document.frm01.hArgon.value)*1;
		}		
		num03 = (num01+num02)*(7/100);		
		document.frm01.hPasi.value=formatCurrency(num03);
		
		sumnum = num01+num02+num03;
		
		document.frm01.hTotal01.value=formatCurrency(sumnum);
		
		//sumtotal = (num01+num02)*0.01;
		//document.frm01.hTotal02.value=formatCurrency(sumtotal);
		
	}	
	
	function sumNiti(){
		var num01;
		
		num01=0;
		num02=0;
		if(document.frm01.hBeasuti){ num01 = rip_comma(document.frm01.hBeasuti.value)*1;}
		if(document.frm01.hArgon){num02 = rip_comma(document.frm01.hArgon.value)*1;}		
		
		sumtotal = (num01+num02)*0.01;
		document.frm01.hTotal02.value=formatCurrency(sumtotal);
	
	}	
	
	function sumNiti01(){

		document.frm01.hTotal02.disabled=false;
		document.frm01.hTotal02.value="0.00";
		document.frm01.hTotal02.disabled=true;
	}	
	
	
	function checkDiscount01(val){
		var num01, num02;
		val = document.frm01.hDiscountType01.checked;
		if(val == true){
			num01=0;
			num02=0;
			document.frm01.hDiscountPercent.disabled=false;				
			if(document.frm01.hDiscountPercent){ num01 = rip_comma(document.frm01.hDiscountPercent.value)*1;}
			if(document.frm01.hBeasuti){ num02 = rip_comma(document.frm01.hBeasuti.value)*1;}
			sumnum = (num02*num01)/100;
			document.frm01.hDiscountPercentPrice.value=formatCurrency(sumnum);
		}else{
			document.frm01.hDiscountPercent.value="";	
			document.frm01.hDiscountPercent.disabled=true;	
			document.frm01.hDiscountPercentPrice.value = "0.00";
			document.frm01.hDiscountPercentPrice.disabled=true;		
		}
		
		
		//document.frm01.hDiscountOtherRemark.value= "";
		//document.frm01.hDiscountOtherPrice.value = "0.00";
		//document.frm01.hDiscountOtherRemark.disabled=true;
		//document.frm01.hDiscountOtherPrice.disabled=true;		
	
	}	
	
	function checkDiscount02(val){
		val = document.frm01.hDiscountType02.checked;
		if(val == true){
			//document.frm01.hDiscountPercent.disabled=true;	
			document.frm01.hDiscountOtherRemark.disabled=false;
			document.frm01.hDiscountOtherPrice.disabled=false;
			//document.frm01.hDiscountPercent.value= "";
			//document.frm01.hDiscountPercentPrice.value= "0.00";
		}else{
			
			document.frm01.hDiscountOtherRemark.value="";
			document.frm01.hDiscountOtherPrice.value="0.00";		
			document.frm01.hDiscountOtherRemark.disabled=false;
			document.frm01.hDiscountOtherPrice.disabled=false;		
		}
	}	
	
	function checkDiscount03(val){
		if(val == true){

		}else{

		}	
		/*
		document.frm01.hDiscountPercent.disabled=true;	
		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		document.frm01.hDiscountOtherRemark.disabled=true;
		document.frm01.hDiscountOtherPrice.disabled=true;		
		*/
	}	
	
	function sumAll(){		
		num01=0;
		num02=0;
		num03=0;
		num04=0;	
		num05=0;	
		num06=0;	
		num07=0;	
		num08=0;	
		num09=0;
	
		//sumBea();
		if(document.frm01.hTotal01){
			num01 = rip_comma(document.frm01.hTotal01.value)*1;
		}
		if(document.frm01.hTotal02){
			//���� � ������
			num02 = rip_comma(document.frm01.hTotal02.value)*1;
		}
		
		if(document.frm01.hDiscountPercentPrice){
			num04 = rip_comma(document.frm01.hDiscountPercentPrice.value)*1;
		}
		if(document.frm01.hDiscountOtherPrice){
			num05 = rip_comma(document.frm01.hDiscountOtherPrice.value)*1;
		}
		if(document.frm01.hPrbPrice01){
			num06 = rip_comma(document.frm01.hPrbPrice01.value)*1;
		}
		/*
		if(document.frm01.hDiscountType03.checked == true){
			//alert(num01+" "+num02+" "+num04+" "+num05+" "+num06);
		
		
			num07 = num01-num02-num04-num05;
			num10 = num01-num04-num05;
		}else{
			num07 = num01-num02-num04-num05+num06;
			num10 = num01-num04-num05+num06;
		}
		*/
		
			num07 = num01-num02-num04-num05+num06;
			num10 = num01-num04-num05+num06;		
		
		document.frm01.hTotal.value= formatCurrency(num07);
		document.frm01.hTotal04.value= formatCurrency(num10);

	}
	
	
	function same_prb(){
		var prb_id = document.frm01.hStockPrbId.value;
	
		if( prb_id > 0){
			document.frm01.hInsureCompanyId01.value = document.frm01.hInsureCompanyId.value;
			document.frm01.hStockKomId.value = document.frm01.hStockPrbId.value;
		
		}
	
	}
	
	function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPTotal.value = '<?=number_format($objItem->get_insure_fee(),2)?>';
			document.frm01.hPInsureVat.value = '<?=number_format($objItem->get_insure_vat(),2)?>';
			document.frm01.hPArgon.value = '<?=number_format($objItem->get_argon(),2)?>';
			document.frm01.hPrbPrice.value = '<?=number_format($objItem->get_total(),2)?>';
			document.frm01.hPrbPrice01.value = '<?=number_format($objItem->get_total(),2)?>';
		}
	<?}?>
		sumAll();
	}
	
	<?if($objInsureOld->get_pay_number() > 0){?>
		checkDisplay(<?=$objInsureOld->get_pay_number()?>);
		checkSum();
	<?}?>
	
	
	function sum_var(val){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=false;
		
		for(i=1;i<=5;i++){
			
			
			num01 =	document.getElementById("hQty_"+val+"_"+i).value;
			num02 =	rip_comma(document.getElementById("hPrice_"+val+"_"+i).value);
			num05 = document.getElementById("hPayin_"+val+"_"+i).checked;
			if(num05 == true){
				num03=(num01*num02)*-1;
			}else{
				num03=(num01*num02);			
			}
			document.getElementById("hPriceSum_"+val+"_"+i).value= formatCurrency(num03);
			num04 = num04+num03;
		}
		document.getElementById("hPaymentPrice_"+val).value= formatCurrency(num04);
		
		checkSum();
		
	}
	
	function sum_free(){
		total=0;
		totalAll=0;
	

		document.getElementById("hStockProductTotal").value =formatCurrency(totalAll);
	}
	
	
</script>
<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
	
	function check_sale(val,vals){
		
			if(document.frm01.hSale01.checked==true){
				document.getElementById("sale_p").style.display="";				
			}else{
				document.getElementById("sale_p").style.display="none";

			}
			if(document.frm01.hSale02.checked==true){
				document.getElementById("sale_k").style.display="";

			}else{
				document.getElementById("sale_k").style.display="none";

			}

			

	}
	
	
	function PrintPrb(frm_name){
		var barcode;
		if(document.frm01.hDisplayBarcode.checked == true){
			barcode = 1;
		}else{
			barcode = 0;
		}
		if(document.frm01.hAddressType[0].checked == true){	window.open(frm_name+'&hAddressType=1&hDisplayBarcode='+barcode);}
		if(document.frm01.hAddressType[1].checked == true){	window.open(frm_name+'&hAddressType=2&hDisplayBarcode='+barcode);}
		if(document.frm01.hAddressType[2].checked == true){	window.open(frm_name+'&hAddressType=3&hDisplayBarcode='+barcode);}
		if(document.frm01.hAddressType[3].checked == true){	window.open(frm_name+'&hAddressType=4&hDisplayBarcode='+barcode);}
		
	}	
	
		function PrintDoc(frm_name){
		if(document.frm01.hAddressType1[0].checked == true){	window.open(frm_name+'&hAddressType=1');}
		if(document.frm01.hAddressType1[1].checked == true){	window.open(frm_name+'&hAddressType=2');}
		if(document.frm01.hAddressType1[2].checked == true){	window.open(frm_name+'&hAddressType=3');}
		if(document.frm01.hAddressType1[3].checked == true){	window.open(frm_name+'&hAddressType=4');}
		
	}	
	
	</script>
	<?include "unset_all.php";?>
<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new InsureCustomer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;


$objLabel = new InsureMail();
$objLabel->setLabelId($hId);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,50,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


				$pdf->AddPage();
				$pdf->SetLeftMargin(50);
				
				/*
				$objInsure->setBookingMail(1);
				$objInsure->updateCrlBookingMail();
				*/
				$objInsureCar = new InsureCar();
				$objInsureCar->loadByCondition(" source='�١��� Buzz' and insure_customer_id > 0  ");

				$objCustomer = new InsureCustomer();
				$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
				$objCustomer->load();

				$objCarType = new CarType();
				$objCarType->setCarTypeId($objInsureCar->get_car_type());
				$objCarType->load();
				
				$objCarModel = new CarModel();
				$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
				$objCarModel->load();

				$objMember = new Member();
				$objMember->setMemberId($objInsureCar->get_sale_id());
				$objMember->load();
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = formatThaiDate(date("Y-m-d"));
				$pdf->SetXY(400,80+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = "����ͧ";
				$pdf->SetXY(50,120+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = $objLabel->getText01();
				$pdf->SetXY(80,120+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = "���¹          ";
				$pdf->SetXY(50,150+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','',16);	
				if($objCustomer->getName03() != ""){
					$strtext = $objCustomer->getName03;
				}else{
					$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
				}
				$pdf->SetXY(80,150+$hHeight);		
				$pdf->Write(10,$strtext);
					
				if($objCustomer->getAddress03() != ""){
					
				$strtext= $objCustomer->getAddress03();
				$pdf->SetXY(80,170+$hHeight);
				$pdf->Write(10,$strtext);	
					
				$strtext= $objCustomer->getTumbon03()." ".$objCustomer->getAmphur03();
				$pdf->SetXY(80,190+$hHeight);
				$pdf->Write(10,$strtext);
			
				$strtext= $objCustomer->getProvince03()." ".$objCustomer->getZip03();	
				$pdf->SetXY(80,210+$hHeight);
				$pdf->Write(10,$strtext);
				
				//$strtext= $objCustomer->getHomeTel()." ".$objCustomer->getMobile();
				$strtext= "";
				$pdf->SetXY(400,210+$hHeight);
				$pdf->Write(10,$strtext);
				
				}else{
				
				$strtext= $objCustomer->getAddress();
				$pdf->SetXY(80,170+$hHeight);
				$pdf->Write(10,$strtext);	
					
				$strtext= $objCustomer->getTumbon()." ".$objCustomer->getAmphur();
				$pdf->SetXY(80,190+$hHeight);
				$pdf->Write(10,$strtext);
			
				$strtext= $objCustomer->getProvince()." ".$objCustomer->getZip();	
				$pdf->SetXY(80,210+$hHeight);
				$pdf->Write(10,$strtext);
				
				//$strtext= $objCustomer->getHomeTel()." ".$objCustomer->getMobile();
				$strtext= "";
				$pdf->SetXY(400,210+$hHeight);
				$pdf->Write(10,$strtext);				
				
				}
				
				$hText = $objLabel->getText02();
				$hStartDate = formatThaiDate($objInsureCar->get_date_verify());
				$arrDate = explode("-",$objInsureCar->get_date_verify());
				$newDate = ($arrDate[0]+1)."-".$arrDate[1]."-".$arrDate[2];
				$hEndDate = formatThaiDate($newDate);
				
				$bodytag = str_replace("{date01}", $hStartDate, $hText);
				$bodytag = str_replace("{date02}", $hEndDate, $bodytag);
				$bodytag = str_replace("{brand}", $objCarType->getTitle(), $bodytag);
				$bodytag = str_replace("{model}", $objCarModel->getTitle(), $bodytag);
				$bodytag = str_replace("{carnumber}", $objInsureCar->get_car_number(), $bodytag);
				
				$pdf->SetXY(82,$objLabel->getH1()+$hHeight);
				$pdf->WriteHTML($bodytag);
				
				$pdf->SetFont('angsa','',16);	
				$strtext= nl2br($objLabel->getText03());
				$pdf->SetXY(82,$objLabel->getH2()+$hHeight);
				$pdf->WriteHTML($strtext);
				
				$strtext= nl2br($objLabel->getText04());
				$pdf->SetXY(80,$objLabel->getW1()+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','B',20);	
				$pdf->SetTextColor(220,0,0);
				$strtext= nl2br($objLabel->getText10());
				$pdf->SetXY(52,$objLabel->getW2()+$hHeight);
				$pdf->WriteHTML($strtext);
								
				$pdf->SetFont('angsa','',16);	
				$pdf->SetTextColor(0,0,0);
				$strtext= $objLabel->getText05();
				$pdf->SetXY(410,$objLabel->getW2()+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objLabel->getText06();
				$pdf->SetXY(390,$objLabel->getW2()+50+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objLabel->getText07();
				$pdf->SetXY(390,$objLabel->getW2()+70+$hHeight);
				$pdf->Write(10,$strtext);
				
				$pdf->SetFont('angsa','B',16);	
				$strtext= $objLabel->getText08();
				$pdf->SetXY(80,$objLabel->getW2()+140+$hHeight);
				$pdf->Write(10,$strtext);

				$pdf->SetFont('angsa','',16);	
				$pdf->SetLeftMargin(80);
				$strtext= nl2br($objLabel->getText09())."<br>�س ".$objMember->getFirstname()." ( ".$objMember->getNickname()." ) ".$objMember->getTel();
				$pdf->SetXY(80,$objLabel->getW2()+160+$hHeight);
				$pdf->WriteHTML($strtext);


	$pdf->Output();	
	?>
	<script>
	window.close();
	</script>	
	<?include "unset_all.php";?>
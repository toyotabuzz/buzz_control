<?
include("common.php");
$objInsure = new Insure();
$objInsure->set_insure_id($hId);
$objInsure->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();

$objCustomer = new Customer();
$objCustomer->setCustomerId($objInsure->get_customer_id());
$objCustomer->load();

$objMember = new Member();
$objMember->setMemberId($objInsure->get_sale_id());
$objMember->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$arrDate = explode("-",$objInsure->get_date_protect());
$hNextYear = $arrDate[0]+1;
$hNewDate = $hNextYear."-".$arrDate[1]."-".$arrDate[2];

$hDiscount = $objInsure->get_k_num09()+$objInsure->get_k_num10()+$objInsure->get_k_num11();


?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Toyota Buzz  : Control Center System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_report_quotation.css">
</head>

<body>

<table width="700">
<tr>
	<td>
		<table width="98%">
		<tr>
			<td width="60%"></td>
			<td width="40%" align="right" >
			<table  style="BORDER: #000000 1px solid;" >
			<tr>
				<td>�ѹ��� <?=formatThaiDate(date("Y-m-d"))?></td>
			</tr>
			<tr>
				<td>ö��� <?=$objCarType->getTitle()?>/<?=$objCarModel->getTitle()?></td>
			</tr>
			<tr>
				<td>����¹ <?=$objInsureCar->get_code()?></td>
			</tr>
			<tr>
				<td>��ѡ�ҹ <?=$objMember->getFirstname()." ".$objMember->getLastname()?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		
	<table align="center" width="98%">
	<tr>
		<td><img src="../images/spacer.gif" alt="" width="40" height="10" border="0">��Ҿ��� (���/�ҧ/�ҧ���)  <?=$objCustomer->getFirstname()."  ".$objCustomer->getLastname();?> ���� <img src="../images/spacer.gif" alt="" width="40" height="1" border="0">  ��</td>
	</tr>
	<tr>
		<td>������� <?=$objCustomer->getAddress03()." ".$objCustomer->getTumbon03()." ".$objCustomer->getAmphur03()." ".$objCustomer->getProvince03()." ".$objCustomer->getZip03();?></td>
	</tr>
	<tr>
		<td>���ӧҹ         Tel.         Fax.       Mobile      </td>
	</tr>
	<tr>
		<td>���駤���������ͧ������������Сѹ���ö¹�� ����ѷ 					
		<?
					if($objInsure->get_p_prb_id() > 0){
						$objB = new InsureCompany();
						$objB->setInsureCompanyId($objInsure->get_p_prb_id());
						$objB->load();
						echo $objB->getTitle();
					}
					?>	
					��ҹ ��µ�� �����
					
		</td>
	</tr>
		<tr>
		<td>������ͧ�����    <strong><?=formatThaiDate($objInsure->get_date_protect())?></strong> �֧   <strong><?=formatThaiDate($hNewDate)?></strong>   �������    �ҷ      �ҷ </td>
	</tr>
		<tr>
		<td>(  ) �դ������ʧ����觪��Ф��������   �Ǵ ��Т�Ҿ��Ңͪ����ҤҴѧ���</td>
	</tr>
	<tr>
		<td>
		<table width="100%">
		<?for($i=1;$i<=$objInsure->get_pay_number();$i++){
			$objInsureItem = new InsureItem();
			$objInsureItem->loadByCondition(" insure_id = $hId AND payment_order=$i  ");
		?>
		<tr>
			<td>�Ǵ��� <?=$i?> �����ѹ���</td>
			<td><?=formatThaiDate($objInsureItem->get_payment_date())?></td>
			<td>�ʹ����</td>
			<td align="right"><?=number_format($objInsureItem->get_payment_price(),2)?></td>
			<td>�ҷ �Ţ��������</td>
			<td><img src="../images/spacer.gif" alt="" width="40" height="10" border="0"></td>
		</tr>
		<?}?>		
		<tr>
			<td></td>
			<td></td>
			<td>���</td>
			<td align="right"><?=number_format($objInsure->get_k_num13(),2)?></td>
			<td>�ҷ</td>
			<td></td>
		</tr>		
		</table>
		</td>
	</tr>
		<tr>
		<td><img src="../images/spacer.gif" alt="" width="40" height="10" border="0">��Ҿ����Ѻ��Һ�������������Ң�Ҿ������Ѻ����������ͧ����������ҷ���˹� �ҡ��Ҿ�������Ф�����¶١��ͧ��� ��˹��Ǵ���������Դ�Ѵ ��駹���ҡ��ҧ���ҼԹѴ�����§�Ǵ˹�觧Ǵ� ��Ҿ����Թ������ ����ѷ ��µ�� ����� �ӡѴ �ӡ��¡��ԡ�������� ���Ծѡ��ͧ�͡����ǡ�͹ ��Т�Ҿ���
		�Թ������Է�����¡��ͧ㹡�âͤ׹�Թ����ҡ��Ҿ������������������<br><br></td>
	</tr>
		<tr>
		<td><img src="../images/spacer.gif" alt="" width="40" height="10" border="0">��Ҿ�������ҹ����������͹䢴����Ǩ֧ŧ�����ͪ��͵���Ӣ͹��<br><br></td>
	</tr>	
	<tr>
		<td align="center">ŧ����...............................................................�����蹤Ӣ�</td>
	</tr>
	<tr>
		<td align="center">(...............................................................)</td>
	</tr>	
	</table>
	
	<table align="center" width="98%" style="BORDER: #000000 1px solid;">
	<tr>
		<td align="center">����ѷ � ��Ԩ�óҤӢ͢�ҧ�������դ������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/spacer.gif" alt="" width="10" height="10" border="1">  ͹��ѵ�&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/spacer.gif" alt="" width="10" height="10" border="1">   ���͹��ѵ�   ����Ӣ͹��</td>
	</tr>
	<tr>
		<td align="center">ŧ����...............................................................�����蹤Ӣ�</td>
	</tr>
	<tr>
		<td align="center">(...............................................................)</td>
	</tr>		
	</table>

		<br><br><br>
		<table>
		<tr>
			<td colspan="2">�Ըա�ê����Թ</td>
		</tr>
		<tr>
			<td align="right">1.</td>
			<td>���� ���Թʴ/�ѵ��ôԵ/�� ���µ��ͧ������ѷ ( Zone �ٹ���ԡ�� )</td>
		</tr>
		<tr>
			<td align="right">2.</td>
			<td>�����¡���͹�Թ��ҹ�ѭ�ո�Ҥ�� �ѧ���</td>
		</tr>
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� ��ا�����ظ�� ( �آ��Ժ�� 2 ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 296-1-14599-5</td>
		</tr>	
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� ��ا෾ ( �Ң��ǹ���� ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 192-0-99548-5</td>
		</tr>	
		<tr>
			<td align="right"></td>
			<td>�ѭ�ո�Ҥ�� �¾ҳԪ�� ( �Ң��ǹ���� ) �ѭ�������Ѿ�� <br>���ͺѭ�� �.��µ�� ����� ��. �Ţ��� 143-224515-9</td>
		</tr>						
		</table>
<br><br>
		�����˵� �ó��͹�Թ ��س�ῡ�� � Pay in �ҷ�� FAX: 02-375-4771 �֧ �ԡҹ�� ���ͷҧ����ѷ� �����͡ ������Ѻ�Թ����ҹ
</td>
</tr>
</table>
		
</body>
</html>

<?
include "common.php";
$q = trim($q);

$date1 = mktime(0,0,0,date("m"),(date("d")-180),date("Y"));
$date2 = date("Y-m-d",$date1);

$strCondition = "  IC.code LIKE '%".$q."%'  and (k_start_date > '".$date2."'  or  p_start_date > '".$date2."'  )  ";

$objInsureList = new InsureList();
$objInsureList->setFilter($strCondition);
$objInsureList->setPageSize(0);
$objInsureList->setSortDefault(" IC.car_number ASC");
$objInsureList->loadUTF8();
forEach($objInsureList->getItemList() as $objItem) {

		//if($objItem->get_p_rnum() != ""){

			$objInsureCar = new InsureCar();
			$objInsureCar->set_car_id($objItem->get_car_id());
			$objInsureCar->load();
			
			$objInsureCustomer = new InsureCustomer();
			$objInsureCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
			$objInsureCustomer->load();
			
			$objSale = new Member();
			$objSale->setMemberId($objItem->get_sale_id());
			$objSale->load();
	
			$objCompany = new Company();
			$objCompany->setCompanyId($objItem->get_company_id());
			$objCompany->load();
			
			$objCS = new CarSeries();
			$objCS->setCarSeriesId($objInsureCar->get_car_series_id());
			$objCS->load();
		
			$objCM = new CarModel();
			$objCM->setCarModelId($objInsureCar->get_car_model_id());
			$objCM->load();	
			
			$objCT = new CarType();
			$objCT->setCarTypeId($objInsureCar->get_car_type());
			$objCT->load();						
		
		if($objItem->get_p_check() == 1){
			$hPCompany ="";
			$hPNumber = "";
			$hPStartDate = "";
			$hPEndDate = "";
			$hPPrice = "";
			$hPPayment = "";
		
			$objKomCompany = new InsureCompany();
			$objKomCompany->setInsureCompanyId($objItem->get_p_prb_id());
			$objKomCompany->load();
			
			$hPCompany = $objKomCompany->getShortTitle();
			$hPNumber = $objItem->get_p_number();
			$hPStartDate = formatShortDate($objItem->get_p_start_date());
			$hPEndDate = formatShortDate($objItem->get_p_end_date());
			$hPPrice = $objItem->get_p_prb_price();
				
			$objInsureItemDetail = new InsureItemDetail();
			$hPPayment= $objInsureItemDetail->loadSumInsure("  order_extra_id = '".$objItem->get_insure_id()."' and payment_subject_id = 39  ");
		}

		if($objItem->get_k_check() == 1){
			$hKCompany ="";
			$hKNumber = "";
			$hKStartDate = "";
			$hKEndDate = "";
			$hKPrice = "";
			$hKPayment = "";
		
			$objKomCompany = new InsureCompany();
			$objKomCompany->setInsureCompanyId($objItem->get_k_prb_id());
			$objKomCompany->load();
			
			$hKCompany = $objKomCompany->getShortTitle();
			$hKNumber = $objItem->get_k_number();
			$hKStartDate = formatShortDate($objItem->get_k_start_date());
			$hKEndDate = formatShortDate($objItem->get_k_end_date());
			$hKPrice = $objItem->get_k_num13();
				
			$objInsureItemDetail = new InsureItemDetail();
			$hKSum = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=39 or payment_subject_id=40   )   ");
			$hKSum1 = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=72 or  payment_subject_id=73  )   ");
			$hKPayment = $hKSum-$hKSum1;
		}
		
		$hStartDate = $hKStartDate;
		if($hStartDate == "")  $hStartDate =$hPStartDate;		
		
			?>
<div onSelect="this.text.value='<?=$objInsureCar->get_code()?>';
document.getElementById('hCusName_<?=$num?>').innerHTML='<?=$objInsureCustomer->getFirstname()." ".$objInsureCustomer->getLastname()?>';
document.getElementById('hCusSale_<?=$num?>').innerHTML='<?=$objSale->getFirstname().'/'.$objCompany->getShortTitleEn();?>';
document.getElementById('hCusCompany1_<?=$num?>').innerHTML='<?=$hKCompany;?>';
document.getElementById('hCusNumber1_<?=$num?>').innerHTML='<?=$hKNumber?>';
document.getElementById('hCusEndDate1_<?=$num?>').innerHTML='<?=$hKEndDate;?>';
document.getElementById('hCusStartDate1_<?=$num?>').innerHTML='<?=$hKStartDate;?>';
document.getElementById('hCusPrice1_<?=$num?>').innerHTML='<?=number_format($hKPrice,2);?>';
document.getElementById('hCusPayment1_<?=$num?>').innerHTML='<?=number_format($hKPayment,2);?>';
document.getElementById('hCusCompany2_<?=$num?>').innerHTML='<?=$hPCompany?>';
document.getElementById('hCusNumber2_<?=$num?>').innerHTML='<?=$hPNumber?>';
document.getElementById('hCusEndDate2_<?=$num?>').innerHTML='<?=$hPEndDate;?>';
document.getElementById('hCusStartDate2_<?=$num?>').innerHTML='<?=$hPStartDate;?>';
document.getElementById('hCusPrice2_<?=$num?>').innerHTML='<?=number_format($hPPrice,2);?>';
document.getElementById('hCusPayment2_<?=$num?>').innerHTML='<?=number_format($hPPayment,2);?>';
document.getElementById('hCarType_<?=$num?>').innerHTML='<?=$objCT->getTitle()?>';
document.getElementById('hCarModel_<?=$num?>').innerHTML='<?=$objCM->getTitle()?>';
document.getElementById('hCarCode_<?=$num?>').innerHTML='<?=$objInsureCar->get_code()?>';
document.getElementById('hCarNumber1_<?=$num?>').innerHTML='<?=$objInsureCar->get_car_number()?>';
document.getElementById('hCarEngine_<?=$num?>').innerHTML='<?=$objInsureCar->get_engine_number()?>';
document.getElementById('hAddress_<?=$num?>').innerHTML='<?=$objInsureCustomer->getName03()?> <?=$objInsureCustomer->getAddressAll03()?>';
document.frm01.hInsureId_<?=$num?>.value='<?=$objItem->get_insure_id()?>';"><span class='informal'></span><span><?=$objInsureCar->get_code()?>/<?=$objInsureCustomer->getFirstname()?>/<?=$hStartDate?></span></div>
	<?
		//}
}
unset($objInsureList);
?>
<?
include("common.php");
define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);

// GET PARAMS URL
$URL		 = $_SERVER['REQUEST_URI'];
$parsedURL	 = parse_url($URL);
$URL_GET	 = $parsedURL['query'];

if(isset($hSubmitApprove)){
	$objF = new InsureForm();
	$objF->set_insure_form_id($hFormId);
	$objF->set_sup_approve($hApprove);
	$objF->set_sup_remark($hApproveRemark);
	$objF->set_sup_id($sMemberId);
	$objF->set_sup_date(date("Y-m-d"));
	$objF->update_sup();
	header("location:k_approve1.php");
	exit;
}


if(isset($hSubmitUpload)){
		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsureId);
		$objInsure->load();
		
		//************************************************************ FTP ********************************************
	    $ftp = ftp_connect('10.21.193.246');
		ftp_login($ftp,'administrator','TbZ,o.s/123');
		ftp_chdir($ftp, '/storage/upload/insurance');
		//************************************************************ FTP ********************************************
		$j=1;
		if ($_FILES['hFile0']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile0']['name'],-4);
			if($hImage0 != ""){ ftp_delete($ftp,$hImage0); }
			$objInsure->set_doc01($pic);
			ftp_put($ftp,$pic,$_FILES['hFile0']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile5']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile5']['name'],-4);
			if($hImage5 != ""){ ftp_delete($ftp,$hImage5); }
			$objInsure->set_doc06($pic);
			ftp_put($ftp,$pic,$_FILES['hFile5']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile1']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile1']['name'],-4);
			if($hImage1 != ""){ ftp_delete($ftp,$hImage1); }
			$objInsure->set_doc02($pic);
			ftp_put($ftp,$pic,$_FILES['hFile1']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile6']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile6']['name'],-4);
			if($hImage6 != ""){ ftp_delete($ftp,$hImage6); }
			$objInsure->set_doc07($pic);
			ftp_put($ftp,$pic,$_FILES['hFile6']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile2']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile2']['name'],-4);
			if($hImage2 != ""){ ftp_delete($ftp,$hImage2); }
			$objInsure->set_doc03($pic);
			ftp_put($ftp,$pic,$_FILES['hFile2']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile7']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile7']['name'],-4);
			if($hImage7 != ""){ ftp_delete($ftp,$hImage7); }
			$objInsure->set_doc08($pic);
			ftp_put($ftp,$pic,$_FILES['hFile7']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile3']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile3']['name'],-4);
			if($hImage3 != ""){ ftp_delete($ftp,$hImage3); }
			$objInsure->set_doc04($pic);
			ftp_put($ftp,$pic,$_FILES['hFile3']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
		
		if ($_FILES['hFile4']['name'] != "") {
			$pic = "INS-".date("YmdHis").$j.substr($_FILES['hFile4']['name'],-4);
			if($hImage4 != ""){ ftp_delete($ftp,$hImage4); }
			$objInsure->set_doc05($pic);
			ftp_put($ftp,$pic,$_FILES['hFile4']['tmp_name'], FTP_BINARY);
			$j++;
		}//end if
				
		
		$objInsure->updateScan();


}


$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$objF = new InsureForm();
$objF->set_insure_form_id($hFormId);
$objF->load();

$hCarId = $objInsure->get_car_id();
$hYearExtend = $objInsure->get_year_extend();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objBookBankList = new BookBankList();
$objBookBankList->setFilter(" company_id=14 ");
$objBookBankList->setPageSize(0);
$objBookBankList->setSortDefault(" bank ASC ");
$objBookBankList->load();

if($hInsureId > 0){
	$objInsure = new Insure();
	$objInsure->set_insure_id($hInsureId);
	$objInsure->load();
}

$pageTitle = "5. ��������";
$pageContent = "5.1 ��������͹��ѵ����˹�ҷ�� > ����䢧Ǵ������С�ê���";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
	
<?include("k_insure_detail01.php");?>
			
					<table cellpadding="5" cellspacing="1">			
					<tr>	
						<td colspan="2" >
						�ѹ������駧ҹ : <?=formatShortDate($objF->get_form_date())?>
						</td>
					</tr>	

					<?if($objInsure->get_k_niti() == "�ؤ�Ÿ�����"){?>
					<form target="_blank" action="quotation_pdf02.php" method="post">
					<?}else{?>
					<form target="_blank" action="quotation_pdf01.php" method="post">
					<?}?>
					<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
					<input type="hidden" name="hId" value="<?=$objInsure->get_quotation_id()?>">
					<tr>
						<td colspan="2" ><strong>�������ʹ��Ҥ�</strong></td>
					</tr>
					<tr>	
						<td colspan="2">							
								<select name="hAddressType">
								
								<option value=1 >�������Ѩ�غѹ
								<option value=2 >�������������¹��ҹ
								<option value=3 >���������ӧҹ
								<option value=4 >������������͡���					
							</select><input type="submit" value="�����">
						</td>						
					</tr>
					</form>			
					<form target="_blank" action="frm_04_pdf.php" method="post">
					<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
					<tr>
						<td colspan="2" ><strong>����쨴���¢ͺ�س</strong></td>
					</tr>
					<tr>
						<td colspan="2">							
								<select name="hAddressType">
								
								<option value=1 >�������Ѩ�غѹ
								<option value=2 >�������������¹��ҹ
								<option value=3 >���������ӧҹ
								<option value=4 >������������͡���					
							</select><input type="submit" value="�����">
						</td>
					</tr>
					</form>
					<form target="_blank" action="frm_05_pdf.php" method="post">
					<input type="hidden" name="hId" value="<?=$hInsureId?>">
					<tr>
						<td colspan="2" ><strong>Ẻ�Ӣ��觨��¤������</strong></td>
					</tr>
					<tr>
						<td>							
								<select name="hAddressType">
								
								<option value=1 >�������Ѩ�غѹ
								<option value=2 >�������������¹��ҹ
								<option value=3 >���������ӧҹ
								<option value=4 >������������͡���					
							</select><input type="submit" value="�����">
						</td>
					</tr>
					</form>		
					</table>
					
					<table width="100%" align="center" class="search" bgcolor="#FFD7D7">
					<form target="_blank" action="k_approve1_1001.php?<?= $URL_GET; ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
					<input type="hidden" name="hFormId" value="<?=$hFormId?>">
					<input type="hidden" name="hSubmitUpload" value="submit">
					<tr>
						<td colspan="2" ><strong>1. ��Ǩ�ͺ�͡���</strong></td>
					</tr>
					<tr>	
						<td colspan="2" >1. ���ҷ���¹ö</td>
					</tr>	
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile0"><input type="hidden" name="hImage0" value="<?=$objInsure->get_doc01()?>">
							<?if($objInsure->get_doc01() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc01())){?>
								<?if (trim($objInsure->get_doc01()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc01()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc01()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc01()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile5"><input type="hidden" name="hImage5" value="<?=$objInsure->get_doc06()?>">
							<?if($objInsure->get_doc06() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc06())){?>
								<?if (trim($objInsure->get_doc06()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc06()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc06()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc06()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" >2. ���� �������� ��Ѻ���</td>
					</tr>		
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile1"><input type="hidden" name="hImage1" value="<?=$objInsure->get_doc02()?>">
							<?if($objInsure->get_doc02() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc02())){?>
								<?if (trim($objInsure->get_doc02()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc02()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc02()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc02()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile6"><input type="hidden" name="hImage6" value="<?=$objInsure->get_doc07()?>">
							<?if($objInsure->get_doc07() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc07())){?>
								<?if (trim($objInsure->get_doc07()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc07()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc07()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc07()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" >3. ���Һѵû�ЪҪ�</td>
					</tr>		
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile2"><input type="hidden" name="hImage2" value="<?=$objInsure->get_doc03()?>">
							<?if($objInsure->get_doc03() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc03())){?>
								<?if (trim($objInsure->get_doc03()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc03()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc03()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc03()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile7"><input type="hidden" name="hImage7" value="<?=$objInsure->get_doc08()?>">
							<?if($objInsure->get_doc08() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc08())){?>
								<?if (trim($objInsure->get_doc08()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc08()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc08()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc08()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<?
					$objIQ = new InsureQuotation();
					$objIQ->loadByCondition(" insure_id = $hInsureId and selected = 'yes' ");
					
					if($objIQ->get_text02()=="�кؼ��Ѻ���"){
					?>					
					<tr>	
						<td colspan="2" >4. ����㺢Ѻ���</td>
					</tr>
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile3"><input type="hidden" name="hImage3" value="<?=$objInsure->get_doc04()?>">
							<?if($objInsure->get_doc04() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc04())){?>
								<?if (trim($objInsure->get_doc04()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc04()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc04()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc04()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<tr>	
						<td colspan="2" align="center"><input type="file" name="hFile4"><input type="hidden" name="hImage4" value="<?=$objInsure->get_doc05()?>">
							<?if($objInsure->get_doc05() != ""){?>						
								<?if(file_exists(PATH_UPLOAD_INS.$objInsure->get_doc05())){?>
								<?if (trim($objInsure->get_doc05()) != "") echo "<a href='".PATH_UPLOAD_INS.$objInsure->get_doc05()."' target=_blank>View file</a>"?>
								<?}else{?>
								<?if (trim($objInsure->get_doc05()) != "") echo "<a href='http://10.21.193.246/upload/insurance/".$objInsure->get_doc05()."' target=_blank>View file</a>"?>
								<?}?>
							<?}?>
						</td>
					</tr>	
					<?}?>
					<tr>	
						<td colspan="2" align="center"><input type="submit" name="hSubmit" value="�Ѿ��Ŵ�͡���"></td>
					</tr>
					</form>
					</table>
					<br>
					<form method="post" action="k_approve1_0901.php?<?= $URL_GET; ?>" name="hFrm01">
					<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
					<input type="hidden" name="hFormId" value="<?=$hFormId?>">
					<input type="hidden" name="hSubmitApprove" value="submit">
					<table width="100%" align="center" class="search" bgcolor="#FFD7D7">
					<tr>
						<td colspan="2" ><strong>2. �ӡ��������͹��ѵ���¡��</strong></td>
					</tr>
					<tr>
						<td colspan="2" align="center" ><input type="button" name="hButton" value="��䢢�����" onclick="window.open('step05List_edit.php?hInsureId=<?=$objInsure->get_insure_id()?>')"></td>
					</tr>	
					<tr>
						<td valign="top" height="30" align="center">
						<input type="radio"  name="hApprove" value="͹��ѵ�"> ͹��ѵ���¡��&nbsp;&nbsp;
						<input type="radio"  name="hApprove" value="���͹��ѵ�">���͹��ѵ���¡��
						</td>
					</tr>
					<tr>	
						<td valign="top">�����˵� :</td>
					</tr>
					<tr>	
						<td align="center"><textarea rows="5" cols="35" name="hApproveRemark"><?=$objF->get_sup_remark()?></textarea></td>
					
					</tr>
					<tr>
						<td align="center" height="30">
						<input type="button" value="�ѹ�֡��¡��" name="hCheck" onclick="return checkSubmit();"> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="¡��ԡ��¡��" name="hCancel" onclick="window.location='k_approve1.php?<?= $URL_GET; ?>'"> 
						</td>
					</tr>
					</table>
					</form>
			
			
<?include("k_insure_detail02.php");?>
<?include("insure_detail_payment.php");?>
<?include("h_footer.php")?>
<?unset($objInsure);?>
<script>
function checkSubmit(){
	if(document.hFrm01.hApprove[0].checked == false && document.hFrm01.hApprove[1].checked == false ){
		alert("��س����͡���������͹��ѵ�");
		return false;
	}

	if(confirm("�س��ͧ��úѹ�֡��¡��������� ?")){
		document.hFrm01.submit();
	}
	
	return true;
}
</script>

<script>

	new CAPXOUS.AutoComplete("hPNumber", function() {
		return "auto_prb.php?hValue=hPStockId&hBrokerId="+document.frm01.hPBrokerId.value+"&hInsureCompanyId="+document.frm01.hPPrbId.value+"&q="+this.text.value;
	});	
</script>
<?include "unset_all.php";?>
<?
include "common.php";

$objInsureBeaList = new InsureAcardList();
$objInsureBeaList->setFilter(" insure_acard_id > 0 ");
$objInsureBeaList->setPageSize(10000);
$objInsureBeaList->setPage($hPage);
$objInsureBeaList->setSortDefault(" t1 ASC");
$objInsureBeaList->setSort($hSort);
$objInsureBeaList->load();
forEach($objInsureBeaList->getItemList() as $objItem) {
	
	$objInsureCar = new InsureCar();
	$objInsureCar->set_car_id($objItem->get_car_id());
	
	$arrDate = explode("/",$objItem->get_t3());
	if($arrDate[1] < 10) $arrDate[1] = "0".$arrDate[1];
	if($arrDate[0] < 10) $arrDate[0] = "0".$arrDate[0];
	
	$new_date = $arrDate[2]."-".$arrDate[1]."-".$arrDate[0];
	
	$objInsureCar->set_date_verify($new_date);
	$objInsureCar->updateDateVerify();

}
?>
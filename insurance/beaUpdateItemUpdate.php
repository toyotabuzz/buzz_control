<?
include("common.php");

$objInsureBea = new InsureBea();
$objInsureBea->set_insure_bea_id($hId);
$objInsureBea->load();

$objInsureCompany = new InsureCompany();
$objInsureCompany->setInsureCompanyId($objInsureBea->get_insure_company_id());
$objInsureCompany->load();

$objInsureBeaItem = new InsureBeaItem();

if (empty($hSubmit)) {
		$strMode="Update";
} else {
	if (!empty($hSubmit)) {

			$objInsureBeaItem->set_insure_bea_item_id($hInsureBeaItemId);
			$objInsureBeaItem->set_insure_bea_id($hId);
			$hStartDate = $hOfficerYear."-".$hOfficerMonth."-".$hOfficerDay;
			$objInsureBeaItem->set_start_date($hStartDate);
			$hEndDate = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
			$objInsureBeaItem->set_end_date($hEndDate);

			
			$objInsureBeaItem->set_plan0101($hPlan0101);
			$objInsureBeaItem->set_plan0102($hPlan0102);
			$objInsureBeaItem->set_plan0103(str_replace(",", "",$hPlan0103));
			$objInsureBeaItem->set_plan0104(str_replace(",", "",$hPlan0104));
			$objInsureBeaItem->set_plan0105(str_replace(",", "",$hPlan0105));
			$objInsureBeaItem->set_plan0106(str_replace(",", "",$hPlan0106));
			$objInsureBeaItem->set_plan0107(str_replace(",", "",$hPlan0107));
			$objInsureBeaItem->set_plan0108($hPlan0108);
			$objInsureBeaItem->set_plan0109(str_replace(",", "",$hPlan0109));
			$objInsureBeaItem->set_plan0110(str_replace(",", "",$hPlan0110));
			$objInsureBeaItem->set_plan0111(str_replace(",", "",$hPlan0111));
			$objInsureBeaItem->set_plan0112(str_replace(",", "",$hPlan0112));
			

			
    		$pasrErr = $objInsureBeaItem->check($hSubmit);

			If ( Count($pasrErr) == 0 ){

				$pId=$objInsureBeaItem->update();
				
				for($i=0;$i<$hCount;$i++){


					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_detail_id($_POST["hInsureBeaItemDetailId01_".$i]);
					$objIBD->set_total(str_replace(",", "",$_POST["hTotal_".$i]));
					$objIBD->set_price1(str_replace(",", "",$_POST["hPrice1_".$i]));
					$objIBD->set_price2(str_replace(",", "",$_POST["hPrice2_".$i]));
					$objIBD->set_price3(str_replace(",", "",$_POST["hPrice3_".$i]));
					$objIBD->set_price4(str_replace(",", "",$_POST["hPrice4_".$i]));
					$objIBD->set_driver_age($hDriverAge01);
					$objIBD->set_plan(1);
					$objIBD->update();
					
					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_detail_id($_POST["hInsureBeaItemDetailId02_".$i]);
					$objIBD->set_total(str_replace(",", "",$_POST["hTotal_".$i]));
					$objIBD->set_price1(str_replace(",", "",$_POST["hPrice5_".$i]));
					$objIBD->set_price2(str_replace(",", "",$_POST["hPrice6_".$i]));
					$objIBD->set_price3(str_replace(",", "",$_POST["hPrice7_".$i]));
					$objIBD->set_price4(str_replace(",", "",$_POST["hPrice8_".$i]));
					$objIBD->set_driver_age($hDriverAge02);
					$objIBD->set_plan(2);
					$objIBD->update();
					
					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_detail_id($_POST["hInsureBeaItemDetailId03_".$i]);
					$objIBD->set_total(str_replace(",", "",$_POST["hTotal_".$i]));
					$objIBD->set_price1(str_replace(",", "",$_POST["hPrice9_".$i]));
					$objIBD->set_price2(str_replace(",", "",$_POST["hPrice10_".$i]));
					$objIBD->set_price3(str_replace(",", "",$_POST["hPrice11_".$i]));
					$objIBD->set_price4(str_replace(",", "",$_POST["hPrice12_".$i]));
					$objIBD->set_driver_age($hDriverAge03);
					$objIBD->set_plan(3);
					$objIBD->update();
					
					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_detail_id($_POST["hInsureBeaItemDetailId04_".$i]);
					$objIBD->set_total(str_replace(",", "",$_POST["hTotal_".$i]));
					$objIBD->set_price1(str_replace(",", "",$_POST["hPrice13_".$i]));
					$objIBD->set_price2(str_replace(",", "",$_POST["hPrice14_".$i]));
					$objIBD->set_price3(str_replace(",", "",$_POST["hPrice15_".$i]));
					$objIBD->set_price4(str_replace(",", "",$_POST["hPrice16_".$i]));
					$objIBD->set_driver_age($hDriverAge04);
					$objIBD->set_plan(4);
					$objIBD->update();
					
					$objIBD = new InsureBeaItemDetail();
					$objIBD->set_insure_bea_item_detail_id($_POST["hInsureBeaItemDetailId05_".$i]);
					$objIBD->set_total(str_replace(",", "",$_POST["hTotal_".$i]));
					$objIBD->set_price1(str_replace(",", "",$_POST["hPrice17_".$i]));
					$objIBD->set_price2(str_replace(",", "",$_POST["hPrice18_".$i]));
					$objIBD->set_price3(str_replace(",", "",$_POST["hPrice19_".$i]));
					$objIBD->set_price4(str_replace(",", "",$_POST["hPrice20_".$i]));
					$objIBD->set_driver_age($hDriverAge05);
					$objIBD->set_plan(5);
					$objIBD->update();
					
				}

				unset ($objInsureBeaItem);

				header("location:beaUpdateItem.php?hId=$hId");
				exit;
			}else{
				$objInsureBea->init();
			}//end check Count($pasrErr)
		}
}


$pageTitle = "3. �����������ѡ";
$strHead03="�ѹ�֡�����ź���ѷ��Сѹ���";
$pageContent =  " 3.14 �����Ť�����»��.�Ҥ��Ѥ�� > �Ҥ����»�Сѹ��� ";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<br>
<form name="hFrm03" method="post" action="beaUpdateItem.php">
<input type="hidden" name="hSearch1" value="yes">
<table cellspacing="2" cellpadding="2" border="0" class="search" width="800">
<tr>
	<td class="listTitle01"  align="center">Ẻ�ҹ�������Ǣ�ͧ�Ѻ��Сѹ��� <strong><?=$objInsureCompany->getTitle();?></strong></td>
</tr>
<tr>	
	<td >
		<?
		$objIBList = new InsureBeaList();
		$objIBList->setFilter(" insure_company_id = ".$objInsureBea->get_insure_company_id());
		$objIBList->setSort(" title ASC ");
		$objIBList->setPageSize(0);
		$objIBList->load();
		?>
		<select name="hId">		
		<?
		forEach($objIBList->getItemList() as $objItem) {
		?>
		<option value="<?=$objItem->get_insure_bea_id()?>" <?if($hId == $objItem->get_insure_bea_id()) echo "selected"?>><?=$objItem->get_title();?>
	<?}?>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="hSubmit" value="����">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="hSubmit" value="��Ѻ˹����¡�����" onclick="window.location='beaList.php';">
	</td>

</tr>
    <tr> 

      <td> 
	  		<strong>������ö</strong> : <?=$objInsureBea->get_personal_type()?> &nbsp;&nbsp;
			<strong>��������ë���</strong> : <?=$objInsureBea->get_fix_type()?> &nbsp;&nbsp;
			<strong>��ǹŴ����ѵԴ�</strong> : <?=$objInsureBea->get_percent()?> % &nbsp;&nbsp;
			<strong>��ö</strong> : <?=$objInsureBea->get_car_year()?> &nbsp;&nbsp;
			<?
		$objInsureType = new InsureType();
		$objInsureType->setInsureTypeId($objItem->get_insure_type_id());
		$objInsureType->load();
			?>
			<strong>������</strong> : <?=$objInsureType->getTitle()?> &nbsp;&nbsp;			
			<strong>���ʧҹ</strong> : 
				<?
				$objInsureFee = new InsureFee1();
				$objInsureFee->set_insure_fee_id($objInsureBea->get_fee_code());
				$objInsureFee->load();
				?>			
				<?=$objInsureFee->get_code();?>&nbsp;&nbsp;
			<strong>��������</strong>  : <?=$objInsureBea->get_group_type()?> 
			
      </td>
    </tr>		
</table>
</form>

<br>
<?
$objInsureBeaItem = new InsureBeaItem();
$objInsureBeaItem->set_insure_bea_item_id($hInsureBeaItemId);
$objInsureBeaItem->load();

$arrDate = explode("-",$objInsureBeaItem->get_start_date());
$hOfficerDay = $arrDate[2];
$hOfficerMonth = $arrDate[1];
$hOfficerYear = $arrDate[0];

$arrDate = explode("-",$objInsureBeaItem->get_end_date());
$hInsureDay = $arrDate[2];
$hInsureMonth = $arrDate[1];
$hInsureYear = $arrDate[0];		

?>
<form name="frm01" enctype="multipart/form-data" action="beaUpdateItemUpdate.php?hId=<?=$hId?>" method="POST"  >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hInsureBeaItemId" value="<?=$hInsureBeaItemId?>">
<table  cellspacing="2" cellpadding="2" border="0" class="search"  >
<tr>
	<td class="listTitle01"  height="30" colspan="21" >
	
	
			<table>
			<tr>
			<td>������������ҧ�ѹ���</td>
			<td>
				<table>
				<td><INPUT align="middle" size="2" maxlength="2"   name=hOfficerDay value="<?=$hOfficerDay?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hOfficerMonth value="<?=$hOfficerMonth?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hOfficerYear" value="<?=$hOfficerYear?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hOfficerYear,hOfficerDay, hOfficerMonth, hOfficerYear,popCal);return false"></td>							
				</table>
			</td>
			<td>
				<table>
				<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay value="<?=$hInsureDay?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth value="<?=$hInsureMonth?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hInsureYear" value="<?=$hInsureYear?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear,hInsureDay, hInsureMonth, hInsureYear,popCal);return false"></td>							
				</table>
			</td>			
			</tr>
			</table>
	
	
	</td>
</tr>

<tr >
	<td rowspan="" align="center"><strong>��������´����������ͧ</strong></td>	
	<td colspan="20"  >
	<input type="text" size="40" name="hPlan0101" value="<?=$objInsureBeaItem->get_plan0101()?>"><br>
	<input type="text" size="40" name="hPlan0102" value="<?=$objInsureBeaItem->get_plan0102()?>">
	</td>

	
</tr>

<tr >
	<td  class="listTitle"  align="center"><strong>�����Ѻ�Դ�ͺ��ͺؤ����¹͡</strong></td>
	<td  class="listTitle"  colspan="20"></td>
</tr>
<tr >
	<td align="left" colspan="1">����������µ�ͪ��Ե��ҧ��� ����͹������ǹ�Թ �ú./��/����</td>
	<td  class="listDetail"  colspan="20"><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0103" value="<?=number_format($objInsureBeaItem->get_plan0103(),2)?>"></td>
</tr>
<tr >
	<td  class="listDetail"  align="right">�����ͤ���</td>
	<td  class="listDetail"  colspan="20"><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0104" value="<?=number_format($objInsureBeaItem->get_plan0104(),2)?>"></td>

</tr>
<tr >
	<td align="left">����������µ�ͷ�Ѿ���Թ���ó�</td>
	<td colspan="20"  align="center"><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0105" value="<?=number_format($objInsureBeaItem->get_plan0105(),2)?>"></td>

</tr>
<tr >
	<td  class="listTitle" align="center">����������ͧö¹������һ�Сѹ���</td>
	<td  class="listTitle"  colspan="20"></td>
</tr>
<tr >
	<td align="left">����������µ��ö¹������һ�Сѹ���</td>
	<td colspan="20"  align="center"><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0106" value="<?=number_format($objInsureBeaItem->get_plan0106(),2)?>"></td>

</tr>
<tr >
	<td   class="listDetail" align="left">ö¹���٭���/�����</td>
	<td  class="listDetail"  colspan="20"  ><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0107" value="<?=number_format($objInsureBeaItem->get_plan0107(),2)?>"></td>

</tr>
<tr >
	<td   class="listDetail" align="left">�������������ǹ�á</td>
	<td  class="listDetail"  colspan="20"  ><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0112" value="<?=number_format($objInsureBeaItem->get_plan0112(),2)?>"></td>

</tr>
<tr >
	<td   class="listTitle" align="center"><strong>����������ͧ����͡���Ṻ����</strong></td>
	<td class="listTitle" colspan="20"  ><input type="text" size="40" name="hPlan0108" value="<?=$objInsureBeaItem->get_plan0108()?>"></td>

</tr>
<tr >
	<td  class="listDetail"  align="left">�غѵ��˵���ǹ�ؤ�� / �ؾ���Ҿ���� ���Ѻ��� 1 �������� ... ��/����</td>
	<td  class="listDetail"  colspan="20"  ><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0109" value="<?=number_format($objInsureBeaItem->get_plan0109(),2)?>"></td>

</tr>
<tr >
	<td  align="left">����ѡ�Ҿ�ҺҺ���غѵ��˵����Ф��� ���Ѻ��� 1 �������� ... ��/����</td>
	<td colspan="20"  ><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0110" value="<?=number_format($objInsureBeaItem->get_plan0110(),2)?>"></td>

</tr>
<tr >
	<td  class="listDetail"  align="left">��Сѹ��Ǽ��Ѻ���</td>
	<td  class="listDetail"  colspan="20"  ><input type="text" size="40" onblur="tryNumberFormat(this)"  name="hPlan0111" value="<?=number_format($objInsureBeaItem->get_plan0111(),2)?>"></td>

</tr>
</table>
<br>
<table>
<tr>
	<td></td>
	<td class="listTitle" align="center" colspan="4">
	
<?
$objInsureBeaItemDetail01 = new InsureBeaItemDetail();
$objInsureBeaItemDetail01->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=1 ");

$objInsureBeaItemDetail02 = new InsureBeaItemDetail();
$objInsureBeaItemDetail02->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=2 ");

$objInsureBeaItemDetail03 = new InsureBeaItemDetail();
$objInsureBeaItemDetail03->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=3 ");

$objInsureBeaItemDetail04 = new InsureBeaItemDetail();
$objInsureBeaItemDetail04->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=4 ");

$objInsureBeaItemDetail05 = new InsureBeaItemDetail();
$objInsureBeaItemDetail05->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=5 ");


?>
	  		<select name="hDriverAge01">
				<option value="" <?if($objInsureBeaItemDetail01->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail01->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail01->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail01->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail01->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail01->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>	
	</td>
	<td class="listTitle" align="center" colspan="4">
	  		<select name="hDriverAge02">
				<option value="" <?if($objInsureBeaItemDetail02->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail02->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail02->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail02->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail02->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail02->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>
	<td class="listTitle" align="center" colspan="4">
	  		<select name="hDriverAge03">
				<option value="" <?if($objInsureBeaItemDetail03->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail03->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail03->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail03->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail03->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail03->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>
	<td class="listTitle" align="center" colspan="4">
	  		<select name="hDriverAge04">
				<option value="" <?if($objInsureBeaItemDetail04->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail04->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail04->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail04->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail04->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail04->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>
	<td class="listTitle" align="center" colspan="4">
	  		<select name="hDriverAge05">
				<option value="" <?if($objInsureBeaItemDetail05->get_driver_age()=="")echo "selected"?>>
				<option value="����кؼ��Ѻ���" <?if($objInsureBeaItemDetail05->get_driver_age()=="����кؼ��Ѻ���")echo "selected"?>>����кؼ��Ѻ���
				<option value="18-24" <?if($objInsureBeaItemDetail05->get_driver_age()=="18-24")echo "selected"?>>18-24
				<option value="25-35" <?if($objInsureBeaItemDetail05->get_driver_age()=="25-35")echo "selected"?>>25-35
				<option value="36-50" <?if($objInsureBeaItemDetail05->get_driver_age()=="36-50")echo "selected"?>>36-50
				<option value="�Թ 50" <?if($objInsureBeaItemDetail05->get_driver_age()=="�Թ 50")echo "selected"?>>�Թ 50
			</select>
	</td>
	
	
</tr>
<tr>
	<td class="listTitle" align="center">�ع��Сѹ���</td>
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	

	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	
</tr>
<?

$objCList = new InsureBeaItemDetailList();
$objCList->setFilter(" insure_bea_item_id= $hInsureBeaItemId  and plan=1 ");
$objCList->setPageSize(0);
$objCList->setSort(" total ASC ");
$objCList->load();
$i=0;
forEach($objCList->getItemList() as $objItem) {
$class="listDetail02";
?>

<tr  >
	<td  class="<?=$class?>" align="center"><input type="hidden" size="10" name="hInsureBeaItemDetailId01_<?=$i?>"  value="<?=$objItem->get_insure_bea_item_detail_id()?>"><input type="text" size="10" name="hTotal_<?=$i?>"  onblur="tryNumberFormat(this)"    value="<?=number_format($objItem->get_total(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10" name="hPrice1_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objItem->get_price1(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice2_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objItem->get_price2(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice3_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objItem->get_price3(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice4_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objItem->get_price4(),2)?>"></td>
<?
$class="listDetail03";
$objC = new InsureBeaItemDetail();
$objC->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=2 and total='".$objItem->get_total()."' ");
?><input type="hidden" size="10" name="hInsureBeaItemDetailId02_<?=$i?>"  value="<?=$objC->get_insure_bea_item_detail_id()?>">
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice5_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC->get_price1(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice6_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC->get_price2(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice7_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC->get_price3(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10" name="hPrice8_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC->get_price4(),2)?>"></td>
<?
$class="listTitle";
$objC1 = new InsureBeaItemDetail();
$objC1->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=3  and total='".$objItem->get_total()."' ");
?><input type="hidden" size="10" name="hInsureBeaItemDetailId03_<?=$i?>"  value="<?=$objC1->get_insure_bea_item_detail_id()?>">
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice9_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC1->get_price1(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice10_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC1->get_price2(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice11_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC1->get_price3(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice12_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC1->get_price4(),2)?>"></td>
	<?$class="listDetail02";?>
<?
$objC2 = new InsureBeaItemDetail();
$objC2->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=4  and total='".$objItem->get_total()."' ");
?><input type="hidden" size="10" name="hInsureBeaItemDetailId04_<?=$i?>"  value="<?=$objC2->get_insure_bea_item_detail_id()?>">
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice13_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC2->get_price1(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice14_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC2->get_price2(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice15_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC2->get_price3(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice16_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC2->get_price4(),2)?>"></td>
<?
$class="listDetail02";
$objC3 = new InsureBeaItemDetail();
$objC3->loadByCondition(" insure_bea_item_id= $hInsureBeaItemId  and plan=5  and total='".$objItem->get_total()."' ");
?>
	<?$class="";?><input type="hidden" size="10" name="hInsureBeaItemDetailId05_<?=$i?>"  value="<?=$objC3->get_insure_bea_item_detail_id()?>">
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice17_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC3->get_price1(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice18_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC3->get_price2(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="5"  name="hPrice19_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC3->get_price3(),2)?>"></td>
	<td class="<?=$class?>" align="center"><input type="text" size="10"  name="hPrice20_<?=$i?>"  onblur="tryNumberFormat(this)" value="<?=number_format($objC3->get_price4(),2)?>"></td>

</tr>


<?if(fmod(($i+1),20) == 0){?>
<tr>
	<td class="listTitle" align="center">�ع��Сѹ���</td>
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	

	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	
	<td class="listTitle" align="center">�ط��</td>
	<td class="listTitle" align="center">�ҡ�</td>
	<td class="listTitle" align="center">Vat</td>
	<td class="listTitle" align="center">�������</td>
	

</tr>
<?}?>
<?$i++;}?>

<tr>       
      <td colspan="21" > 
	  <br><br><br><br>
	   	<input type="hidden" name="hCount" value="<?=$i?>">
        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
		<?if ($strMode == "Update"){?>
			<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" onclick="return check_form();">
		<?}else{?>
			<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" onclick="return check_form();">
		<?}?>
        <input type="Button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='beaUpdateItem.php?hId=<?=$hId?>'">
      </td>
    </tr>
  </table>
</form>

<br><br><br><br>
<?
	include("h_footer.php")
?>
<script>
	new CAPXOUS.AutoComplete("hInsureFeeCode", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_fee.php?hFrm=frm01&q=" + this.text.value;
		}
	});		


	function check_form(){

			if (document.forms.frm01.hOfficerDay.value=="" || document.forms.frm01.hOfficerDay.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerDay.value,1,31) == false) {
					document.forms.frm01.hOfficerDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hOfficerMonth.value==""  || document.forms.frm01.hOfficerMonth.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerMonth.value,1,12) == false){
					document.forms.frm01.hOfficerMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hOfficerYear.value==""  || document.forms.frm01.hOfficerYear.value=="0000")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hOfficerYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hOfficerYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hOfficerYear.focus();
					return false;
				}
			} 				
		
	
	
			if (document.forms.frm01.hInsureDay.value=="" || document.forms.frm01.hInsureDay.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureDay.value,1,31) == false) {
					document.forms.frm01.hInsureDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hInsureMonth.value==""  || document.forms.frm01.hInsureMonth.value=="00")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureMonth.value,1,12) == false){
					document.forms.frm01.hInsureMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hInsureYear.value==""  || document.forms.frm01.hInsureYear.value=="0000")
			{
				alert("��س��к��ѹ������¡��");
				document.forms.frm01.hInsureYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hInsureYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hInsureYear.focus();
					return false;
				}
			} 				


		document.forms.frm01.submit();
	
	}
	

	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
		

</script>
<?include "unset_all.php";?>
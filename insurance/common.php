<?php
include "../conf_inside.php";

define("URL_BASE","insurance");
define("PATH_BASE","../");
define("PATH_CLASS",PATH_BASE."class/");
define("PATH_UPLOAD_INS",PATH_BASE."upload_insure/");

// print_r(PATH_CLASS);exit();

include PATH_CLASS."clUser.php";
include PATH_CLASS."clAdmin.php";
include PATH_CLASS."clMember.php";
include PATH_CLASS."clDepartment.php";
include PATH_CLASS."clModule.php";
include PATH_CLASS."clMemberModule.php";
include PATH_CLASS."clMemberLog.php";
include PATH_CLASS."clCustomerGroup.php";
include PATH_CLASS."clCustomerGrade.php";
include PATH_CLASS."clCustomerEvent.php";
include PATH_CLASS."clContact.php";
include PATH_CLASS."clCustomer.php";
include PATH_CLASS."clCustomerContact.php";
include PATH_CLASS."clCustomerDuplicate.php";
include PATH_CLASS."clCustomerTitle.php";

include PATH_CLASS."clSale.php";
include PATH_CLASS."clTeam.php";
include PATH_CLASS."clStockRed.php";
include PATH_CLASS."clCarType.php";
include PATH_CLASS."clCarModel.php";
include PATH_CLASS."clCarSeries.php";
include PATH_CLASS."clCarColor.php";

include PATH_CLASS."clAreaProvince.php";
include PATH_CLASS."clAreaAmphur.php";
include PATH_CLASS."clAreaTumbon.php";
include PATH_CLASS."clAreaPostcode.php";

include PATH_CLASS."clOrder.php";
include PATH_CLASS."clOrderStock.php";
include PATH_CLASS."clOrderStockOther.php";
include PATH_CLASS."clCompany.php";
include PATH_CLASS."clDealer.php";

include PATH_CLASS."clFundCompany.php";
include PATH_CLASS."clInsureCompany.php";
include PATH_CLASS."clPrbCompany.php";

include PATH_CLASS."clInsure.php";
include PATH_CLASS."clInsureCar.php";
//include PATH_CLASS."clInsureCompany.php";
include PATH_CLASS."clInsureItem.php";
include PATH_CLASS."clInsureQuotation.php";
include PATH_CLASS."clInsureQuotationItem.php";
include PATH_CLASS."clInsureTrack.php";
include PATH_CLASS."clInsureVerify.php";
include PATH_CLASS."clInsureFee.php";
include PATH_CLASS."clInsureFee1.php";
include PATH_CLASS."clInsureMail.php";
include PATH_CLASS."clInsureType.php";
include PATH_CLASS."clInsureItemTrack.php";

include PATH_CLASS."clStockCar.php";
include PATH_CLASS."clStockInsure.php";
include PATH_CLASS."clStockInsureDetail.php";
include PATH_CLASS."clStockPremium.php";
include PATH_CLASS."clStockPremiumItem.php";
include PATH_CLASS."clStockProductPack.php";

include PATH_CLASS."clRegistryCondition.php";

include PATH_CLASS."clErr.php";
include PATH_CLASS."clMonitor.php";
include PATH_CLASS."clMsg.php";
include PATH_CLASS."clMsgReply.php";

include PATH_CLASS."clLabel.php";
include PATH_CLASS."clOrderPrice.php";
include PATH_CLASS."clOrderExtra.php";
include PATH_CLASS."clOrderPayment.php";

include PATH_CLASS."utilityFunction.php";
include PATH_CLASS."incUtility.php";
include PATH_INCLUDE."utilityString.php";

include PATH_CLASS."clInsureBroker.php";
include PATH_CLASS."clInsureQuotationSet.php";
include PATH_CLASS."clPaymentSubject.php";
include PATH_CLASS."clInsureItemDetail.php";
include PATH_CLASS."clStockProductGroup.php";
include PATH_CLASS."clStockProductType.php";
include PATH_CLASS."clStockProduct.php";
include PATH_CLASS."clInsureFree.php";
include PATH_CLASS."clBank.php";
include PATH_CLASS."clInsureForm.php";
include PATH_CLASS."clInsureCustomerGroup.php";
include PATH_CLASS."clInsureCustomer.php";
include PATH_CLASS."clBookBank.php";
include PATH_CLASS."clInsurePayment.php";
include PATH_CLASS."clPaymentType.php";
include PATH_CLASS."clAreaOther.php";

include PATH_CLASS."clInsureBea.php";
include PATH_CLASS."clInsureBeaCar.php";
include PATH_CLASS."clInsureBeaItem.php";
include PATH_CLASS."clInsureBeaItemDetail.php";

include PATH_CLASS."clInsureAcard.php";
include PATH_CLASS."clInsureTeam.php";
include PATH_CLASS."clModuleMenu.php";
include PATH_CLASS."clInsureFormApprove.php";

include PATH_CLASS."clInsureStock.php";
include PATH_CLASS."clInsureStockItem.php";
include PATH_CLASS."clInsureStockItemDetail.php";

include PATH_CLASS."clInsureMailItem.php";
include PATH_CLASS."clInsureMailItemDetail.php";

include PATH_CLASS."clTempInsure.php";
include PATH_CLASS."clTempInsureItem.php";
include PATH_CLASS."clTempInsureItemDetail.php";
include PATH_CLASS."clTempInsurePayment.php";
include PATH_CLASS."clTempInsureQuotation.php";
include PATH_CLASS."clTempInsureQuotationItem.php";

include PATH_CLASS."clCustomerUseCar.php";
include PATH_CLASS."clInsureEdit.php";

include PATH_CLASS."clInsureViriyaCode.php";
include PATH_CLASS."clInsureViriyaTitle.php";
include PATH_CLASS."clInsureViriyaProvince.php";
include PATH_CLASS."clInsureViriyaOccupy.php";
include PATH_CLASS."clInsureViriyaColor.php";
include PATH_CLASS."clInsureViriyaBody.php";
include PATH_CLASS."clInsureViriyaCar.php";
include PATH_CLASS."clInsureViriyaProduct.php";

include PATH_CLASS."clInsureStockPrb.php";
include PATH_CLASS."clInsureStockPrbItem.php";
include PATH_CLASS."clInsureStockPrbItemDetail.php";

include PATH_CLASS."clInsureAdler.php";
include PATH_CLASS."clInsurexQuotation.php";
include PATH_CLASS."clInsurexQuotationItem.php";
include PATH_CLASS."clInsureBill.php";

include PATH_CLASS."clQuotationAccessories.php";
include PATH_CLASS."clQuotationDetail.php";
include PATH_CLASS."clJmQuotationSendLog.php";
include PATH_CLASS."clInsurePaymentSendLog.php";
include PATH_CLASS."clJmQuotationItemDetail.php";
include PATH_CLASS."clInsurePaymentLog.php";
include PATH_CLASS."clPaymentStyle.php";

include(PATH_CLASS."clQuotationItemAddon.php");
include(PATH_CLASS."clQuotationAddonMatch.php");

include(PATH_CLASS."clTmProduct.php");
include(PATH_CLASS."clInsureReceipt.php");

define("PATH_WEB",PATH_BASE."insurance/");

$objUser = new Member();
if (!$objUser->isLogged()){
	header("Location: ../index.php?hMsg=session+expire");
	exit;
}else{
	//check right access
	if( $sRightAccessCrl == "0:0:0:0:0:0" ){
		header("Location: ../index.php?hMsg=������Է����ҹ����� ��سҵԴ��ͼ������к�");
		exit;
	}
}


$currentFile = $_SERVER["PHP_SELF"];
$parts = Explode('/', $currentFile);
if(strpos($parts[count($parts) - 1],"Auto")){
	header('Content-Type: text/html; charset=utf-8');
}

if(strpos($parts[count($parts) - 1],"auto_")){
	header('Content-Type: text/html; charset=utf-8');
}
?>
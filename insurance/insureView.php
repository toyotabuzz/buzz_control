<?
include("common.php");

$objInsureOld = new Insure();
$objInsureItem01 = new InsureItem();
$objInsureItem02 = new InsureItem();
$objInsureItem03 = new InsureItem();
$objInsureItem04 = new InsureItem();
$objInsureItem05 = new InsureItem();
$objInsureItem06 = new InsureItem();
$total_price=0;

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();


$objStockPrbList = new StockPrbList();
$objStockPrbList->setFilter(" use_status_01 = 0 ");
$objStockPrbList->setPageSize(0);
$objStockPrbList->setSort(" title ASC");
$objStockPrbList->load();

$objStockKomList = new StockPrbList();
$objStockKomList->setFilter(" use_status_02 = 0 ");
$objStockKomList->setPageSize(0);
$objStockKomList->setSort(" title ASC");
$objStockKomList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault("title ASC");
$objCarSeriesList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCustomer = new Customer();
$objEvent = new CustomerEvent();
$objMember = new Member();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

if (empty($hSubmit)) {
	if ($hId !="") {
		
		$objInsureOld = new Insure();
		$objInsureOld->set_insure_id($hId);
		$objInsureOld->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsureOld->get_car_id());
		$objInsureCar->load();
		
		$hCustomerId = $objInsureCar->get_customer_id();
		
		$arrDate = explode("-",$objInsureCar->get_call_date());
		$DayCall = $arrDate[2];
		$MonthCall = $arrDate[1];
		$YearCall = $arrDate[0];
	
		$objCustomer->setCustomerId($objInsureCar->get_customer_id());
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$objEvent = new CustomerEvent();
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objCustomer->getSaleId());
		$objMember->load();
		
		//extract mobile 
		$arrMobile = explode(",",$objCustomer->getMobile());
		$hMobile_1 = $arrMobile[0];
		$hMobile_2 = $arrMobile[1];
		$hMobile_3 = $arrMobile[2];
		
		
		$arrDate = explode("-",$objInsureOld->get_date_protect());
		$DayProtect = $arrDate[2];
		$MonthProtect = $arrDate[1];
		$YearProtect = $arrDate[0];
		
		
		$objQIList = new InsureItemList();
		$objQIList->setFilter(" insure_id = $hId ");
		$objQIList->setPageSize(0);
		$objQIList->setSort(" insure_item_id ASC ");
		$objQIList->load();
		$i=0;
		forEach($objQIList->getItemList() as $objItem) {
			if($i==0){
				$objInsureItem01->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem01->load();
				
				$arrDate = explode("-",$objInsureItem01->get_payment_date());
				$Day01 = $arrDate[2];
				$Month01 = $arrDate[1];
				$Year01 = $arrDate[0];
			}
			
			if($i==1){
				$objInsureItem02->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem02->load();
				
				$arrDate = explode("-",$objInsureItem02->get_payment_date());
				$Day02 = $arrDate[2];
				$Month02 = $arrDate[1];
				$Year02 = $arrDate[0];
				
			}
	
			if($i==2){
				$objInsureItem03->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem03->load();
				
				$arrDate = explode("-",$objInsureItem03->get_payment_date());
				$Day03 = $arrDate[2];
				$Month03 = $arrDate[1];
				$Year03 = $arrDate[0];
				
			}
			$i++;
		}		
		
		
		
	} else {
		$strMode="Add";
	}

}


if(isset($hId)){
	$objInsureOld->set_insure_id($hId);
	$objInsureOld->load();

	$arrDate = explode("-",$objInsureOld->get_date_protect());
	$DayProtect = $arrDate[2];
	$MonthProtect = $arrDate[1];
	$YearProtect = $arrDate[0];
	
	$arrDate = explode("-",$objInsureOld->get_date_start_prb());
	$DayStartPrb = $arrDate[2];
	$MonthStartPrb = $arrDate[1];
	$YearStartPrb = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_date_end_prb());
	$DayEndPrb = $arrDate[2];
	$MonthEndPrb = $arrDate[1];
	$YearEndPrb = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_date_start_kom());
	$DayStartKom = $arrDate[2];
	$MonthStartKom = $arrDate[1];
	$YearStartKom = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_date_end_kom());
	$DayEndKom = $arrDate[2];
	$MonthEndKom = $arrDate[1];
	$YearEndKom = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_acc_date());
	$DayReportDate = $arrDate[2];
	$MonthReportDate = $arrDate[1];
	$YearReportDate = $arrDate[0];	
	
	$arrDate = explode("-",$objInsureOld->get_acc_recieve_date());
	$DayRecieveReport = $arrDate[2];
	$MonthRecieveReport = $arrDate[1];
	$YearRecieveReport = $arrDate[0];	
	
	$objQIList = new InsureItemList();
	$objQIList->setFilter(" insure_id = $hId ");
	$objQIList->setPageSize(0);
	$objQIList->setSort(" insure_item_id ASC ");
	$objQIList->load();
	$i=0;
	forEach($objQIList->getItemList() as $objItem) {
		if($i==0){
			$objInsureItem01->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem01->load();
			
			$arrDate = explode("-",$objInsureItem01->get_payment_date());
			$Day01 = $arrDate[2];
			$Month01 = $arrDate[1];
			$Year01 = $arrDate[0];
		}
		
		if($i==1){
			$objInsureItem02->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem02->load();
			
			$arrDate = explode("-",$objInsureItem02->get_payment_date());
			$Day02 = $arrDate[2];
			$Month02 = $arrDate[1];
			$Year02 = $arrDate[0];
			
		}

		if($i==2){
			$objInsureItem03->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem03->load();
			
			$arrDate = explode("-",$objInsureItem03->get_payment_date());
			$Day03 = $arrDate[2];
			$Month03 = $arrDate[1];
			$Year03 = $arrDate[0];
			
		}
		
		if($i==3){
			$objInsureItem04->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem04->load();
			
			$arrDate = explode("-",$objInsureItem04->get_payment_date());
			$Day04 = $arrDate[2];
			$Month04 = $arrDate[1];
			$Year04 = $arrDate[0];
			
		}
		
		if($i==4){
			$objInsureItem05->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem05->load();
			
			$arrDate = explode("-",$objInsureItem05->get_payment_date());
			$Day05 = $arrDate[2];
			$Month05 = $arrDate[1];
			$Year05 = $arrDate[0];
			
		}
		
		if($i==5){
			$objInsureItem06->set_insure_item_id($objItem->get_insure_item_id());
			$objInsureItem06->load();
			
			$arrDate = explode("-",$objInsureItem06->get_payment_date());
			$Day06 = $arrDate[2];
			$Month06 = $arrDate[1];
			$Year06 = $arrDate[0];
			
		}
		
		$i++;
	}

}


function diff_date($begin, $end){
   	$rbegin = is_string($begin) ? strtotime(strval($begin)) : $begin;
   	$rend = is_string($end) ? strtotime(strval($end)) : $end;
	
	$begin =  strtotime(strval($begin));
	$end = strtotime(strval($end));
	
   	$difftime = $end - $begin;
  	$diffdays = floor($difftime / (24 * 60 * 60)) + 1;
	return $diffdays;	
}




$pageTitle = "1. �к��������١���";
$strHead03 = "�ѹ�֡�������١���";
$page_title = "�ʴ������� ICARD";
include("../Header/incHeaderView.php");
?>
<script type="text/javascript" src="../function/check_mobile.js"></script>
<script language="JavaScript">
	
	<?if($objCustomer->getTypeId() == 'A' OR $objCustomer->getTypeId() == '' ){?>
	function check_submit()
	{

		if (document.forms.frm01.hGroupId.options[frm01.hGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡���觷���Ңͧ�١���");
			document.forms.frm01.hGroupId.focus();
			return false;
		} 	

		if (document.forms.frm01.hEventId.value=="")
		{
			alert("��س��кت��ͧҹ�͡ʶҹ���");
			document.forms.frm01.hEvent.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hSaleId.value=="")
		{
			alert("��س��кت��ͼ����������");
			document.forms.frm01.hSale.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت���-���ʡ��");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 			
	
		if(document.forms.frm01.hIncomplete.checked == false){
			
	
			if (document.forms.frm01.hAddress.value=="")
			{
				alert("��س��кط������");
				document.forms.frm01.hAddress.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hTumbonCode.value=="")
			{
				alert("��س��кصӺ�");
				document.forms.frm01.hTumbon.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hAmphurCode.value=="")
			{
				alert("��س��к������");
				document.forms.frm01.hAmphur.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hProvinceCode.value=="")
			{
				alert("��س��кبѧ��Ѵ");
				document.forms.frm01.hProvince.focus();
				return false;
			} 
			
			if (document.forms.frm01.hZipCode.value=="")
			{
				alert("��س��к�������ɳ���");
				document.forms.frm01.hZip.focus();
				return false;
			} 							

		}
		
	}
	
	<?}else{?>
	function check_submit()
	{
	
		if (document.forms.frm01.hGroupId.options[frm01.hGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡���觷���Ңͧ�١���");
			document.forms.frm01.hGroupId.focus();
			return false;
		} 	

		if (document.forms.frm01.hEventId.value=="")
		{
			alert("��س��кت��ͧҹ�͡ʶҹ���");
			document.forms.frm01.hEvent.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hSaleId.value=="")
		{
			alert("��س��кت��ͼ����������");
			document.forms.frm01.hSale.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت���-���ʡ��");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 			
	
		if(document.forms.frm01.hIncomplete.checked == false){
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			Set_Display_Type('1');
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س��кصӺ�");
			Set_Display_Type('1');
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س��к������");
			Set_Display_Type('1');
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س��кبѧ��Ѵ");
			Set_Display_Type('1');
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س��к�������ɳ���");
			Set_Display_Type('1');
			document.forms.frm01.hZip.focus();
			return false;
		} 						
		
		if ( (document.forms.frm01.hMobile_1.value=="" && document.forms.frm01.hMobile_2.value=="" && document.forms.frm01.hMobile_3.value=="" )    && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			Set_Display_Type('1');
			document.forms.frm01.hHomeTel.focus();
			return false;
		} 	
		
		
		if (document.forms.frm01.hAddress03.value=="")
		{
			alert("��س��кط�����������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAddress03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode03.value=="")
		{
			alert("��س��кصӺŷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hTumbon03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode03.value=="")
		{
			alert("��س��к�����ͷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAmphur03.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode03.value=="")
		{
			alert("��س��кبѧ��Ѵ������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hProvince03.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode03.value=="")
		{
			alert("��س��к�������ɳ��������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hZip03.focus();
			return false;
		} 								

		}


	}

	
function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			if(document.frm01.hCustomerSamePlace01.checked == true){
				document.frm01.hAddress01.value = document.frm01.hAddress.value;
				document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
				document.frm01.hProvince01.value = document.frm01.hProvince.value;
				document.frm01.hZip01.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode01.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode01.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode01.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode01.value = document.frm01.hZipCode.value;		
				document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax01.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress01.value = "";
				document.frm01.hTumbon01.value = "";
				document.frm01.hAmphur01.value = "";
				document.frm01.hProvince01.value = "";
				document.frm01.hZip01.value = "";		
				document.frm01.hTumbonCode01.value = "";
				document.frm01.hAmphurCode01.value = "";
				document.frm01.hProvinceCode01.value = "";
				document.frm01.hZipCode01.value = "";		
				document.frm01.hTel01.value = "";		
				document.frm01.hFax01.value = "";		
			}
		}
   		if(val ==2){
			if(document.frm01.hCustomerSamePlace02.checked == true){
				document.frm01.hAddress02.value = document.frm01.hAddress.value;
				document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
				document.frm01.hProvince02.value = document.frm01.hProvince.value;
				document.frm01.hZip02.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode02.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode02.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode02.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode02.value = document.frm01.hZipCode.value;		
				document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax02.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress02.value = "";
				document.frm01.hTumbon02.value = "";
				document.frm01.hAmphur02.value = "";
				document.frm01.hProvince02.value = "";
				document.frm01.hZip02.value = "";		
				document.frm01.hTumbonCode02.value = "";
				document.frm01.hAmphurCode02.value = "";
				document.frm01.hProvinceCode02.value = "";
				document.frm01.hZipCode02.value = "";		
				document.frm01.hTel02.value = "";		
				document.frm01.hFax02.value = "";		
			}
			
		}
   		if(val ==3){
			if(document.frm01.hCustomerSamePlace03.checked == true){
				document.frm01.hAddress03.value = document.frm01.hAddress.value;
				document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
				document.frm01.hProvince03.value = document.frm01.hProvince.value;
				document.frm01.hZip03.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode03.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode03.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode03.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode03.value = document.frm01.hZipCode.value;		
				document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax03.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress03.value = "";
				document.frm01.hTumbon03.value = "";
				document.frm01.hAmphur03.value = "";
				document.frm01.hProvince03.value = "";
				document.frm01.hZip03.value = "";		
				document.frm01.hTumbonCode03.value = "";
				document.frm01.hAmphurCode03.value = "";
				document.frm01.hProvinceCode03.value = "";
				document.frm01.hZipCode03.value = "";		
				document.frm01.hTel03.value = "";		
				document.frm01.hFax03.value = "";		
			}
			
		}
   
   }
	

	<?}?>
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>

<table align="center" width="98%">
<tr>
	<td><h4>ʶҹл�Сѹ��»Ѩ�غѹ : <?=$objInsureOld->get_status_detail()?> </h4></td>
	<td align="right"><h4>�ż���Ѻ�Դ�ͺ : <?=$objMember->getFirstname()."  ".$objMember->getLastname()?></h4></td>
</tr>
</table>
<form name="frm01" action="insureUpdate.php" method="POST" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hCustomerId" value="<?=$hCustomerId?>">
  	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">	  
	  <input type="hidden" name="hInsureId" value="<?=$objInsureOld->get_insure_id();?>">
	  <input type="hidden" name="hInsureStatus" value="<?=$objInsureOld->get_status();?>">
<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">
	<table width="100%">
	<tr>
		<td width="1"><img src="../images/console05.gif" alt="" width="12" height="12" border="0"></td>
		<td><strong>�������١���</strong></td>
		<td align="right"></td>
	</tr>
	</table>	
	</td>
</tr>
</table>

<table width="98%" class="i_background02" align="center">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>���觷���Ңͧ�١���</strong> </td>
			<td>
				<?$objCustomerGroupList->printSelect("hGroupId",$objCustomer->getGroupId(),"��س����͡");?> <input type="checkbox" name="hFollow" value=1 <?if($objCustomer->getFollowUp() ==1) echo "checked"?>> ���١��ҵԴ���
			</td>
			<td class="i_background03"><strong>�ѹ�����������</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong> </td>
			<td>				
				<input type="hidden" size="3" name="hEventId"  value="<?=$objCustomer->getEventId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hEventId.value != '' ) frm01.hSale.focus();if(event.keyCode !=13 ) frm01.hEventId.value='';"   name="hEvent"  size=40 value="<?=$objEvent->getTitle()?>">
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td valign="top" >				
				<input type="hidden" name="hSaleId"  value="<?=$objCustomer->getSaleId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hSaleId.value='';"   name="hSale"  size=40 value="<?=$objMember->getFirstname()."  ".$objMember->getLastname()?>">
			</td>
		</tr>
		<tr>			
			<td width="150" class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td width="175"><input type="text" name="hIDCard" size="30" maxlength="13"  value="<?=$objCustomer->getIDCard();?>"></td>
			<td width="120"class="i_background03"><strong>�ô</strong></td>
			<td width="205">
				<?$objCustomerGradeList->printSelect("hGradeId",$objCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>����-���ʡ��</strong> </td>
			<td valign="top">
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<INPUT onKeyDown="if(event.keyCode==13) frm01.hCustomerTitleId.focus();"   name="hCustomerName"  size=35 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
					<?if($strMode == "Update"){?>
					<td valign="top"><input type="button"  class="button" value="��ӫ�͹" onclick="return checkDuplicate(<?=$hId?>);" ></td>
					<?}?>
				</tr>
				</table>					

				</td>
			<td class="i_background03" valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<?if($objCustomer->getTypeId() == 'B' OR $objCustomer->getTypeId() == 'C' ){?>
		<table width="98%" cellpadding="0" cellspacing="0" align="center">
		<tr><td align="right">		
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>				
		</td></tr>
		<tr>
		<td  class="i_background">
		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="50"  value="<?=$objCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ���ҹ</strong> </td>
			<td valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"></td>
			<td class="i_background03" valign="top"><strong>��Ͷ��</strong></td>
			<td>
			1. <input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"><br>
			2. <input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"><br>
			3. <input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"><br>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hMailback" value="1" <?if($objCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>	
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress01" maxlength="50" size="50"  value="<?=$objCustomer->getAddress01();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace01" onclick="sameplace(1)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode01"  value="<?=$objCustomer->getTumbonCode01();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode01.value != '' ) frm01.hAmphur01.focus();if(event.keyCode !=13 ) frm01.hTumbonCode01.value='';" name="hTumbon01" size="50"  value="<?=$objCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode01"  value="<?=$objCustomer->getAmphurCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode01.value != '' ) frm01.hProvince01.focus();if(event.keyCode !=13 ) frm01.hAmphurCode01.value='';" name="hAmphur01" size="30"  value="<?=$objCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode01"  value="<?=$objCustomer->getProvinceCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode01.value != '' ) frm01.hZip01.focus();if(event.keyCode !=13 ) frm01.hProvinceCode01.value='';" name="hProvince01" size="30"  value="<?=$objCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode01"  value="<?=$objCustomer->getZip01();?>"><input type="text" name="hZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode01.value != '' ) frm01.hTel01.focus();if(event.keyCode !=13 ) frm01.hZipCode01.value='';" size="30"  value="<?=$objCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel01" size="30"  value="<?=$objCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax01" size="30"  value="<?=$objCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress02" maxlength="50" size="50"  value="<?=$objCustomer->getAddress02();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace02" onclick="sameplace(2)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="text" size="3" readonly name="hTumbonCode02"  value="<?=$objCustomer->getTumbonCode02();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode02.value != '' ) frm01.hAmphur02.focus();if(event.keyCode !=13 ) frm01.hTumbonCode02.value='';" name="hTumbon02" size="50"  value="<?=$objCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="text" size="3" readonly name="hAmphurCode02"  value="<?=$objCustomer->getAmphurCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode02.value != '' ) frm01.hProvince02.focus();if(event.keyCode !=13 ) frm01.hAmphurCode02.value='';" name="hAmphur02" size="30"  value="<?=$objCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="text" size="3" readonly name="hProvinceCode02"  value="<?=$objCustomer->getProvinceCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode02.value != '' ) frm01.hZip02.focus();if(event.keyCode !=13 ) frm01.hProvinceCode02.value='';" name="hProvince02" size="30"  value="<?=$objCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="text" size="3" readonly name="hZipCode02"  value="<?=$objCustomer->getZip02();?>"><input type="text" name="hZip02"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode02.value != '' ) frm01.hTel02.focus();if(event.keyCode !=13 ) frm01.hZipCode02.value='';" size="30"  value="<?=$objCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel02" size="30"  value="<?=$objCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax02" size="30"  value="<?=$objCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress03" maxlength="50" size="50"  value="<?=$objCustomer->getAddress03();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace03" onclick="sameplace(3)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="text" size="3" readonly name="hTumbonCode03"  value="<?=$objCustomer->getTumbonCode03();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode03.value != '' ) frm01.hAmphur03.focus();if(event.keyCode !=13 ) frm01.hTumbonCode03.value='';" name="hTumbon03" size="50"  value="<?=$objCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="text" size="3" readonly name="hAmphurCode03"  value="<?=$objCustomer->getAmphurCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode03.value != '' ) frm01.hProvince03.focus();if(event.keyCode !=13 ) frm01.hAmphurCode03.value='';" name="hAmphur03" size="30"  value="<?=$objCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="text" size="3" readonly name="hProvinceCode03"  value="<?=$objCustomer->getProvinceCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode03.value != '' ) frm01.hZip03.focus();if(event.keyCode !=13 ) frm01.hProvinceCode03.value='';" name="hProvince03" size="30"  value="<?=$objCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="text" size="3" readonly name="hZipCode03"  value="<?=$objCustomer->getZip03();?>"><input type="text" name="hZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode03.value != '' ) frm01.hTel03.focus();if(event.keyCode !=13 ) frm01.hZipCode03.value='';" size="30"  value="<?=$objCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel03" size="30"  value="<?=$objCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax03" size="30"  value="<?=$objCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>
		<?}else{?>
		<table width="100%">
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="50"  value="<?=$objCustomer->getTumbon();?>"> <a href="areaTumbonUpdate.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ���ҹ</strong> </td>
			<td class="small" valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"><br>��. 02-222-3333</td>
			<td class="i_background03" valign="top"><strong>��Ͷ��</strong></td>
			<td class="small">
			1. <input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"><br>
			2. <input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"><br>
			3. <input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"><br>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ����ӧҹ</strong></td>
			<td class="small"><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"><br>��. 02-222-3333</td>
			<td class="i_background03" valign="top"><strong>ῡ��</strong></td>
			<td class="small"><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"><br>��. 02-222-3333</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hMailback" value="1" <?if($objCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>	
		<tr>
			<td class="i_background03"><?if($objCustomer->getVerifyAddress() > 0) {?><strong>Check Address</strong><?}?></td>
			<td><?if($objCustomer->getVerifyAddress() > 0) {?><input type="radio" name="hVerifyAddress" value="1" <?if($objCustomer->getVerifyAddress() == 1) echo "checked";?>> ����ó� &nbsp;&nbsp;<input type="radio" name="hVerifyAddress" value="2" <?if($objCustomer->getVerifyAddress() == 2) echo "checked";?>> �������ó� <?}?></td>
			<td class="i_background03"><?if($objCustomer->getVerifyPhone() > 0) {?><strong>Check Phone</strong><?}?></td>
			<td><?if($objCustomer->getVerifyPhone() > 0) {?><input type="radio" name="hVerifyPhone" value="1" <?if($objCustomer->getVerifyPhone() == 1) echo "checked";?>> ����ó� &nbsp;&nbsp;<input type="radio" name="hVerifyPhone" value="2" <?if($objCustomer->getVerifyPhone() == 2) echo "checked";?>> �������ó� <?}?></td>
		</tr>	
		</table>
		<?}?>
		
	</td>
</tr>
</table>
		<br>
	</td>
</tr>
</table>

<br>
<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">���ö�����</td>
</tr>
</table>
<table width="98%" align="center" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td><strong>������</strong></td>
			<td align="center">:</td>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType",$objInsureCar->get_car_type(),"����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td><strong>���</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel",$objInsureCar->get_car_model_id(),"����к�","populate02(document.frm01,document.frm01.hCarModel.options[document.frm01.hCarModel.selectedIndex].value,document.frm01.hCarSeries)");?>
			</td>
			<td><strong>Ẻ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?$objCarSeriesList->printSelect("hCarSeries",$objInsureCar->get_car_series_id(),"����к�");?></td>
		</tr>
		<tr>
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?=$objCarColorList->printSelect("hCarColor",$objInsureCar->get_color(),"����к�");?></td>
			<td><strong>����¹</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hCarCode" size="5" maxlength="7"  value="<?=$objInsureCar->get_code()?>"></td>	
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<select name="hRegisterYear">
			<?for($i=date("Y");$i> (date("Y")-25);$i--){?>
				<option value="<?=$i?>" <?if($objInsureCar->get_register_year() == $i) echo "selected";?>><?=$i?>
			<?}?>
			</select>			
			</td>
		</tr>
		<tr>
			<td><strong>�Ţ����ͧ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hEngineNumber" size="20"   value="<?=$objInsureCar->get_engine_number()?>"></td>
			<td><strong>�Ţ�ѧ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hCarNumber" size="30"   value="<?=$objInsureCar->get_car_number()?>"></td>	
			<td><strong>���觷����</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
				<select name="hSource">
					<option value="">- ��س����͡ -
					<option value="�й�" <?if($objInsureCar->get_source() == "�й�") echo "selected";?>>�١����й�
					<option value="CCARD" <?if($objInsureCar->get_source() == "CCARD") echo "selected";?>>�١��� CCARD
					<option value="BP" <?if($objInsureCar->get_source() == "BP") echo "selected";?>>�١��� BP
					<option value="Walk In" <?if($objInsureCar->get_source() == "Walk In") echo "selected";?>>�١��� Walk In
				</select>			
			</td>
		</tr>		
		<tr>
			<td><strong>�ѹ����õ��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayCall value="<?=$DayCall?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthCall value="<?=$MonthCall?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearCall value="<?=$YearCall?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearCall,DayCall, MonthCall, YearCall,popCal);return false"></td>		
				</tr>
				</table>			
			
			</td>
			<td><strong>�����˵��õ��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hCallRemark" size="15" ></td>	
			<td><strong>�йӨҡ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hSuggestFrom" size="15" ></td>			
				
		</tr>		
		</table>
	</td>
</tr>
</table>
<br>
<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">ʶҹС�÷ӧҹ</td>
</tr>
</table>
<table width="98%" align="center" class="i_background02">
<tr>
	<td valign="top" width="100"><strong>��������͹</strong></td>
	<td><a href="mailPrintPdfOne.php?hOrderCount=1&hSelect=<?=$hId?>" target="_blank">����쨴����</a></td>
</tr>
<tr>
	<td colspan="2"><hr size="1" color="#C0C0C0"></td>
</tr>
<tr>
	<td valign="top"><strong>�ä��駷�� 1</strong></td>
	<td>
		<?
		$objTrackList = new InsureTrackList();
		$objTrackList->setFilter(" insure_id = $hId AND tracking_type= 'call 1' ");
		$objTrackList->setPagesize(0);
		$objTrackList->setSort(" insure_track_id ASC ");
		$objTrackList->load();
		
		if($objTrackList->mCount > 0){
		?>
		
		<table width="100%" cellpadding="3">
	<?
	$i=1;
	forEach($objTrackList->getItemList() as $objItem) {
	?>
		<tr>
			<td valign="top" width="30%">�ä��駷�� <?=$i?> 	�ѹ���/���� <?=$objItem->get_date_add()?></td>
			<td valign="top" width="30%">��õͺ�Ѻ : <?=$objItem->get_feedback();?></td>
			<td>�����˵� : <?=$objItem->get_remark();?></td>
		</tr>	
	<?
	$i++;
	}?>
		</table>
		<?}?>	
	</td>
</tr>
<tr>
	<td colspan="2"><hr size="1" color="#C0C0C0"></td>
</tr>
<tr>
	<td valign="top"><strong>��ʹ��Ҥ�</strong></td>
	<td>
	
		<?
		$objQIList = new InsureQuotationList();
		$objQIList->setFilter(" insure_id = $hId  ");
		$objQIList->setPagesize(0);
		$objQIList->setSort(" quotation_id ASC ");
		$objQIList->load();
		
		if($objQIList->mCount > 0){
		?>
		
		<table cellpadding="3" class="search">
		<tr>
			<td width="20%" class="listTitle" align="center">��¡�÷�����͡</td>
			<td width="20%" class="listTitle" align="center">�ѹ���</td>
			<td width="30%" class="listTitle" align="center">�Ţ���</td>
			<td width="10%" class="listTitle" align="center">�����</td>
		</tr>
	<?
	$i=1;
	forEach($objQIList->getItemList() as $objItem) {
	?>
		<tr>
			<td class="listDetail" align="center"><?if($objItem->get_quotation_id() == $objInsureOld->get_quotation_id()) {?><img src="../images/check_yes.gif" alt="" width="11" height="12" border="0"><?}?></td>
			<td class="listDetail" align="center"><?=formatShortDate($objItem->get_date_add())?></td>
			<td class="listDetail" align="center"><?=$objItem->get_quotation_number()?></td>			
			<td class="listDetail" align="center"><a href="quotationPrint.php?hInsureId=<?=$hId?>&hId=<?=$objItem->get_quotation_id()?>" target="_blank">�����</a></td>
		</tr>
	<?
	$i++;
	}?>
		</table>
		<?}?>			
	</td>
</tr>
<tr>
	<td colspan="2"><hr size="1" color="#C0C0C0"></td>
</tr>
<tr>
	<td valign="top"><strong> �Դ��â��</strong></td>
	<td>

		<?
		$objTrackList = new InsureTrackList();
		$objTrackList->setFilter(" insure_id = $hId AND tracking_type= 'call 2' ");
		$objTrackList->setPagesize(0);
		$objTrackList->setSort(" insure_track_id ASC ");
		$objTrackList->load();
		
		if($objTrackList->mCount > 0){
		?>
		
		<table width="100%" cellpadding="3">
	<?
	$i=1;
	forEach($objTrackList->getItemList() as $objItem) {
	?>
		<tr>
			<td valign="top" width="30%">�ä��駷�� <?=$i?> 	�ѹ���/���� <?=$objItem->get_date_add()?></td>
			<td valign="top" width="30%">��õͺ�Ѻ : <?=$objItem->get_feedback();?></td>
			<td>�����˵� : <?=$objItem->get_remark();?></td>
		</tr>	
	<?
	$i++;
	}?>
		</table>
		<?}?>	
	
	</td>
</tr>
</table>


<?if($objInsureOld->get_status() == "set up" OR $objInsureOld->get_status() == "tracking"  OR $objInsureOld->get_status() == "complete" OR $objInsureOld->get_status() == "cancel"   ){?>
<br>
<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">�����Ż�Сѹ���</td>
</tr>
</table>
<table width="98%" class="i_background02" align="center">
<tr>
	<td>	
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td></td>			
			<td></td>			
			<td ></td>
			<td ></td>
			<td class="error" align="right"><strong>�ѹ��������ͧ</strong>&nbsp;</td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayProtect value="<?=$DayProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthProtect value="<?=$MonthProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearProtect" value="<?=$YearProtect?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearProtect,DayProtect, MonthProtect, YearProtect,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td></td>			
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr>
			<td class="listTitle"><strong>�Ҥ��Ѥ��</strong></td>			
			<td class="listDetail"><?$objInsureBrokerList->printSelect("hInsureBrokerId",$objInsureOld->get_insure_broker_id(),"����к�");?>	</td>			
			<td class="listDetail"><?$objInsureCompanyList->printSelectScript("hInsureCompanyId",$objInsureOld->get_insure_company_id(),"����к�","populate01(document.frm01,document.frm01.hInsureCompanyId.options[document.frm01.hInsureCompanyId.selectedIndex].value,document.frm01.hStockPrbId)");?>	</td>
			<td class="listDetail"><?$objStockPrbList->printSelect("hStockPrbId",$objInsureOld->get_stock_prb_id(),"����к�");?>	

			<script>
				populate01(document.frm01,document.frm01.hInsureCompanyId.options[document.frm01.hInsureCompanyId.selectedIndex].value,document.frm01.hStockPrbId);
				document.frm01.hStockPrbId.value=<?=$objInsureOld->get_stock_prb_id()?>;		
			</script>		

			
			</td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayStartPrb value="<?=$DayStartPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthStartPrb value="<?=$MonthStartPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearStartPrb" value="<?=$YearStartPrb?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearStartPrb,DayStartPrb, MonthStartPrb, YearStartPrb,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayEndPrb value="<?=$DayEndPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthEndPrb value="<?=$MonthEndPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearEndPrb" value="<?=$YearEndPrb?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearEndPrb,DayEndPrb, MonthEndPrb, YearEndPrb,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>		
		</tr>
		<tr>
			<td class="listTitle">
			<strong>�Ҥ�ѧ�Ѻ</strong>
			</td>
			<td class="listDetail"><?$objInsureBrokerList->printSelect("hInsureBrokerKomId",$objInsureOld->get_insure_broker_kom_id(),"����к�");?>	</td>			
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hStockKomId",$objInsureOld->get_stock_kom_id(),"����к�");?>		</td>
			<td class="listDetail"><input type="text" name="hKomNumber"  size="30" value="<?=$objInsureOld->get_kom_number();?>">	</td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayStartKom value="<?=$DayStartKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthStartKom value="<?=$MonthStartKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearStartKom" value="<?=$YearStartKom?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearStartKom,DayStartKom, MonthStartKom, YearStartKom,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayEndKom value="<?=$DayEndKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthEndKom value="<?=$MonthEndKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearEndKom" value="<?=$YearEndKom?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearEndKom,DayEndKom, MonthEndKom, YearEndKom,popCal);return false"></td>		
				</tr>
				</table>						
			
			</td>		
		</tr>
		</table>

	<br>

</table>		
<table width="98%" class="search" align="center" cellpadding="1" cellspacing="0">
<tr>
	<td width="50%" align="center" class="listTitle">�Ҥ��Ѥ��</td>
	<td width="50%" align="center" class="listtitle">�Ҥ�ѧ�Ѻ</td>
</tr>
<tr>
	<td class="i_background02">
		<table width="100%">
		<tr>
			<td colspan="2">�ع��Сѹ</td>			
			<td align="right"><input type="text" name="hFund" style="text-align=right;"   onblur=""  size="15" value="<?=$objInsureOld->get_fund();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�����ط��</td>
			<td align="right"><input type="text" name="hBeasuti" style="text-align=right;"  onblur="sumBea();sumAll();"  size="15" value="<?=$objInsureOld->get_beasuti();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�ҡ�</td>
			<td align="right"><input type="text" name="hArgon" style="text-align=right;"  onblur="sumBea();sumAll();"   size="15" value="<?=$objInsureOld->get_argon();?>"></td>
		</tr>
		<tr>
			<td colspan="2">����</td>
			<td align="right"><input type="text" name="hPasi" style="text-align=right;"  onblur="sumBea();sumAll();"  size="15" value="<?=$objInsureOld->get_pasi();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�������</td>
			<td align="right"><input type="text" name="hTotal01" style="text-align=right;"  disabled size="15"   onblur=""  value="<?=$objInsureOld->get_beasuti()+$objInsureOld->get_argon()+$objInsureOld->get_pasi();?>"></td>
		</tr>
		<tr>
			<td >������������</td>
			<td><input type="radio" value="1" name="hNiti"  onclick="sumNiti01();sumAll();"  <?if($objInsureOld->get_niti()=="1") echo "checked"?> >�ؤ�Ÿ�����&nbsp;&nbsp;<input type="radio" value="2" name="hNiti" onclick="sumNiti();sumAll();" <?if($objInsureOld->get_niti()=="2") echo "checked"?>>�ԵԺؤ��</td>
			<td align="right"><input type="text" name="hTotal02"  style="text-align=right;"  onblur=""  disabled  size="15" value="<?=$objInsureOld->get_beasuti()*0.01;?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">���</td>
			<td align="right" class="listDetail02"><input type="text" style="text-align=right;"  disabled name="hTotal03"  onblur=""   size="15" value=""></td>
		</tr>

		<tr>
			<td rowspan="2" valign="top">��ǹŴ</td>
			<td>
					<input type="radio" value="1" onclick="checkDiscount01();sumAll();" name="hDiscountType" <?if($objInsureOld->get_discount_type()=="1") echo "checked"?>>��ǹŴ % &nbsp;<input type="text" name="hDiscountPercent" onBlur="checkDiscount01();sumAll();"  size="10" value="<?=$objInsureOld->get_discount_percent()?>">
			</td>
			<td valign="top" align="right"><input type="text" style="text-align=right;"  onblur=""  disabled name="hDiscountPercentPrice"  size="15" value=""></td>
		</tr>
		<tr>			
			<td><input type="radio" value="2"  onclick="checkDiscount02();sumAll();"  name="hDiscountType" <?if($objInsureOld->get_discount_type()=="2") echo "checked"?>>��ǹŴ���� <input type="text" name="hDiscountOtherRemark"  size="10" value="<?=$objInsureOld->get_discount_other_remark()?>"></td>
			<td valign="top" align="right"><input type="text" style="text-align=right;" onblur="sumAll();"   onblur=""  name="hDiscountOtherPrice"  size="15" value="<?=$objInsureOld->get_discount_other_price();?>"></td>
		</tr>
		<tr>
			

		</tr>
		<tr>
			<td >��� �ú.</td>
			<td>
					<input type="radio" value="3"  onclick="checkDiscount03();sumAll();"  name="hDiscountType" <?if($objInsureOld->get_discount_type()=="3") echo "checked"?>>���ú.
			</td>
			<td align="right"><input type="text" name="hPrbPrice01" style="text-align=right;"  onblur=""  disabled size="15" value="<?=$objInsureOld->get_prb_price();?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�������ͧ���з�����</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal"  style="text-align=right;"  onblur=""  size="15" value="<?=$objInsureOld->get_price();?>"></td>
		</tr>
		<tr>
			<td>��������Сѹ���</td>
			<td>
				<select name="hInsureType">
					<option value="GOA" <?if($objInsureOld->get_insure_type()=="GOA") echo "selected"?>>GOA
					<option value="� 1" <?if($objInsureOld->get_insure_type()=="� 1") echo "selected"?>>� 1
					<option value="� 2" <?if($objInsureOld->get_insure_type()=="� 2") echo "selected"?>>� 2
					<option value="� 3" <?if($objInsureOld->get_insure_type()=="� 3") echo "selected"?>>� 3
					<option value="����" <?if($objInsureOld->get_insure_type()=="����") echo "selected"?>>����
				</select><input type="text" name="hInsureTypeRemark"  size="10" value="<?=$objInsureOld->get_insure_type_remark();?>">
			</td>
		</tr>		
		<tr>
			<td>�ͧ��</td>
			<td colspan="2"><input type="text" name="hFreeRemark"  size="30" value="<?=$objInsureOld->get_free_remark();?>"></td>
		</tr>
		<tr>
			<td>��������ë���</td>
			<td><input type="radio" value="������ҧ" name="hRepair"  <?if($objInsureOld->get_repair()=="������ҧ") echo "checked"?>>������ҧ&nbsp;&nbsp;<input type="radio" value="�������" name="hRepair" <?if($objInsureOld->get_repair()=="�������") echo "checked"?>>�������</td>
		</tr>
		</table>

	
	
	
	</td>
	<td valign="top" class="i_background02">
		<table>
		<tr>
			<td>������ö��Т�Ҵö¹��</td>
			<td>
				<select name="hInsureFeeId" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsureOld->get_insure_fee_id() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_title();?>
				<?}?>
				</select>
			
			</td>
		</tr>
		<tr>
			<td>�Ҥ� �ú.</td>
			<td><input type="text" name="hPrbPrice" style="text-align=right;"  onblur=""  size="10" value="<?=$objInsureOld->get_prb_price();?>"></td>
		</tr>
		<tr>
			<td>�շ���ͻ�Сѹ</td>
			<td>
				<select name="hCarStatus">
					<option value="�շ�� 1"  <?if($objInsureOld->get_car_status() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsureOld->get_car_status() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsureOld->get_car_status() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsureOld->get_car_status() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsureOld->get_car_status() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsureOld->get_car_status() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsureOld->get_car_status() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsureOld->get_car_status() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsureOld->get_car_status() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsureOld->get_car_status() == "�շ�� 10") echo "selected"?>>�շ�� 10
				</select>
			</td>
		</tr>
		<tr>
			<td>�ѹ����駧ҹ</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayReportDate value="<?=$DayReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthReportDate value="<?=$MonthReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearReportDate" value="<?=$YearReportDate?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearReportDate,DayReportDate, MonthReportDate, YearReportDate, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�ѹ����Ѻ��</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayRecieveReport value="<?=$DayRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthRecieveReport value="<?=$MonthRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name="YearRecieveReport" value="<?=$YearRecieveReport?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearRecieveReport,DayRecieveReport, MonthRecieveReport, YearRecieveReport, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�Ţ�Ѻ��</td>
			<td><input type="text" name="hAccNo"  size="10" value="<?=$objInsureOld->get_acc_no();?>"></td>
		</tr>
		<tr>
			<td valign="top">�����˵�</td>
			<td><textarea rows="3" cols="30" name="hRemark"><?=$objInsureOld->get_remark();?></textarea></td>
		</tr>
		</table>

	
	
	</td>
</tr>
</table>



<br>
<table width="98%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">�����š�ê��Ф�һ�Сѹ��� ���з����� 
		<select name="hPaymentAmount"  onchange="checkDisplay(this.value)" >
			<option value="0">��س����͡
			<option value="1" <?if($objInsureOld->get_payment_amount() == "1") echo "selected";?>>1
			<option value="2" <?if($objInsureOld->get_payment_amount() == "2") echo "selected";?>>2
			<option value="3" <?if($objInsureOld->get_payment_amount() == "3") echo "selected";?>>3
			<option value="4" <?if($objInsureOld->get_payment_amount() == "4") echo "selected";?>>4
			<option value="5" <?if($objInsureOld->get_payment_amount() == "5") echo "selected";?>>5
			<option value="6" <?if($objInsureOld->get_payment_amount() == "6") echo "selected";?>>6
		</select>	
	  �Ǵ
	  &nbsp;&nbsp;&nbsp;<strong>���͹䢡�â��</strong>
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_payment_condition() == "�������") echo "checked";?> value="�������"> �������
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_payment_condition() == "�觨���") echo "checked";?> value="�觨���"> �觨���
	  </td>
</tr>
</table>
<input type="hidden" name="hInsureItemId01" value="<?=$objInsureItem01->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId02" value="<?=$objInsureItem02->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId03" value="<?=$objInsureItem03->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId04" value="<?=$objInsureItem04->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId05" value="<?=$objInsureItem05->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId06" value="<?=$objInsureItem06->get_insure_item_id();?>">
<table width="98%" cellpadding="3" cellspacing="1" class="i_background02" align="center">
<tr id="pay00" style="display:none">
	<td class="listTitle" width="5%" align="center">�Ǵ���</td>
	<td class="listTitle" width="15%" align="center">�ѹ����˹�����</td>
	<td class="listTitle" width="15%" align="center">��������ê���</td>
	<td class="listTitle" width="10%" align="center">�ӹǹ</td>	
	<td class="listTitle" width="15%" align="center">��������</td>
	<td class="listTitle" width="35%" align="center">��¡�ê���</td>
	<td class="listTitle" width="15%" align="center">�����˵�</td>	
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem01->get_payment_status() == 0 OR $objInsureItem01->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem01->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay01" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">1</td>
	<td bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType01" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem01->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem01->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem01->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice01" onblur="checkSum();"  size="10" value="<?=$objInsureItem01->get_payment_price();?>"></td>	
	<td class="listDetail" valign="top">
	<?
	if($objInsureItem01->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem01->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem01->get_payment_status() == "0") echo "��ҧ����";
	?>
	
	</td>
	<td class="listDetail" align="center" valign="top">



		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 38 and OP.price > 0 and OP.order_id = $hId ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark01"  size="20" value="<?=$objInsureItem01->get_remark();?>">	</td>		
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem02->get_payment_status() == 0 OR $objInsureItem02->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem02->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay02" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">2</td>
	<td bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day02 value="<?=$Day02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType02" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem02->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem02->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem02->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice02" onblur="checkSum();"  size="10" value="<?=$objInsureItem02->get_payment_price();?>"></td>
	<td class="listDetail"  valign="top">
	<?
	if($objInsureItem02->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem02->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem02->get_payment_status() == "0") echo "��ҧ����";
	?>
	</td>
	<td class="listDetail" align="center" valign="top">
		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 39  and OP.price > 0 and OP.order_id = $hId  ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>	
	
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark02"  size="20" value="<?=$objInsureItem02->get_remark();?>">	</td>		
	
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem03->get_payment_status() == 0 OR $objInsureItem03->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem03->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay03" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">3</td>
	<td  bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day03 value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month03 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year03 value="<?=$Year03?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year03,Day03, Month03, Year03,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType03" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem03->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem03->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem03->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice03" onblur="checkSum();"  size="10" value="<?=$objInsureItem03->get_payment_price();?>"></td>
	<td class="listDetail"  valign="top">
	<?
	if($objInsureItem03->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem03->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem03->get_payment_status() == "0") echo "��ҧ����";
	?>
	</td>
	<td class="listDetail" align="center" valign="top">
		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 40  and OP.price > 0 and OP.order_id = $hId ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>	
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark03"  size="20" value="<?=$objInsureItem03->get_remark();?>">	</td>		
	
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem04->get_payment_status() == 0 OR $objInsureItem04->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem04->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay05" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">4</td>
	<td  bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day04 value="<?=$Day04?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month04 value="<?=$Month04?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year04 value="<?=$Year04?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year04,Day04, Month04, Year04,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType04" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem04->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem04->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem04->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice04" onblur="checkSum();"  size="10" value="<?=$objInsureItem04->get_payment_price();?>"></td>
	<td class="listDetail"  valign="top">
	<?
	if($objInsureItem04->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem04->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem04->get_payment_status() == "0") echo "��ҧ����";
	?>
	</td>
	<td class="listDetail" align="center" valign="top">
		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 41  and OP.price > 0 and OP.order_id = $hId ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>	
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark04"  size="20" value="<?=$objInsureItem04->get_remark();?>">	</td>		
	
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem05->get_payment_status() == 0 OR $objInsureItem05->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem05->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay06" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">5</td>
	<td  bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day05 value="<?=$Day05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month05 value="<?=$Month05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year05 value="<?=$Year05?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year05,Day05, Month05, Year05,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType05" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem05->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem05->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem05->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice05" onblur="checkSum();"  size="10" value="<?=$objInsureItem05->get_payment_price();?>"></td>
	<td class="listDetail"  valign="top">
	<?
	if($objInsureItem05->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem05->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem05->get_payment_status() == "0") echo "��ҧ����";
	?>
	</td>
	<td class="listDetail" align="center" valign="top">
		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 42  and OP.price > 0 and OP.order_id = $hId ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>	
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark05"  size="20" value="<?=$objInsureItem05->get_remark();?>">	</td>		
	
</tr>
<?
//check payment date
$bgcolor="#f7f7f7";	
if($objInsureItem06->get_payment_status() == 0 OR $objInsureItem06->get_payment_status() == 2){
	$datediff = diff_date( date("Y-m-d"), $objInsureItem06->get_payment_date());
	if($datediff < 0) $bgcolor = "#FF0000";
	if($datediff < 4 AND $datediff > 0 ) $bgcolor = "#FF8000";
	if($datediff <= 7 AND $datediff > 3 ) $bgcolor = "#FFFF80";
}
?>
<tr id="pay07" style="display:none">
	<td class="listDetail" align="center" align="center" valign="top">6</td>
	<td  bgcolor="<?=$bgcolor?>" align="center" align="center" valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day06 value="<?=$Day06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month06 value="<?=$Month06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="6" onblur="checkYear(this,this.value);"  name=Year06 value="<?=$Year06?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year06,Day06, Month06, Year06,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center" valign="top">
		<select name="hPaymentType06" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem06->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem06->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem06->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center" valign="top"><input type="text" name="hPaymentPrice06" onblur="checkSum();"  size="10" value="<?=$objInsureItem06->get_payment_price();?>"></td>
	<td class="listDetail"  valign="top">
	<?
	if($objInsureItem06->get_payment_status() == "1") echo "���Фú";
	if($objInsureItem06->get_payment_status() == "2") echo "���кҧ��ǹ";
	if($objInsureItem06->get_payment_status() == "0") echo "��ҧ����";
	?>
	</td>
	<td class="listDetail" align="center" valign="top">
		<?
		$objOrderPriceList = new OrderPriceList();
		$objOrderPriceList->setFilter(" OP.payment_subject_id = 43  and OP.price > 0 and OP.order_id = $hId ");
		$objOrderPriceList->setPageSize(0);
		$objOrderPriceList->load();
		if($objOrderPriceList->mCount > 0){?>
		<table>
		<tr>
			<td class="listTitle" align="center">�Ţ��������</td>
			<td class="listTitle" align="center">�ѹ������</td>
			<td class="listTitle" align="center">�ӹǹ</td>
		</tr>
		<?forEach($objOrderPriceList->getItemList() as $objItem) {
			$objOrderExtra = new OrderExtra();
			$objOrderExtra->setOrderExtraId($objItem->getOrderExtraId());
			$objOrderExtra->load();
			$total_price = $total_price+ $objItem->getPrice();
		?>
		<tr>
			<td class="listDetail" align="center"><?=$objOrderExtra->getBookingNumber();?></td>
			<td class="listDetail" align="center"><?=$objOrderExtra->getExtraDate();?></td>
			<td class="listDetail" align="right"><?=number_format($objItem->getPrice(),2)?></td>
		</tr>	
		<?}?>
		</table>
		<?}?>	
	</td>
	<td class="listDetail" align="center" valign="top"><input type="text" name="hRemark06"  size="20" value="<?=$objInsureItem06->get_remark();?>">	</td>		
	
</tr>
<tr id="pay04" style="display:none">
	<td></td>
	<td></td>
	<td align="right">������Թ</td>
	<td align="center"><input type="text" name="hInsurePrice" onblur="checkSum();"  size="10" value="<?=$objInsureOld->get_price();?>"></td>
	<td align="right"></td>
	<td align="center" colspan="2">������Шҡ����Թ <input type="text"  size="10" value="<?=number_format($total_price,2);?>">&nbsp;&nbsp;&nbsp; �ʹ��ҧ���� <input type="text"  size="10" value="<?=number_format(($objInsureOld->get_price() - $total_price),2);?>"> </td>
	
</tr>
</table>




<script>
	sumAll();


	function checkSubmit(){

		if(document.frm01.hInsureCompanyId.value ==0){
			alert("��س����͡����ѷ��Сѹ");
			document.frm01.hInsureCompanyId.focus();
			return false;
		}
	
		if(document.frm01.hStockPrbId.value ==0){
			alert("��س����͡ �ú");
			document.frm01.hStockPrbId.focus();
			return false;
		}
		
		if(document.frm01.hInsureCompanyId01.value ==0){
			alert("��س����͡����ѷ��Сѹ");
			document.frm01.hInsureCompanyId01.focus();
			return false;
		}
	
		if(document.frm01.hStockKomId.value ==0){
			alert("��س����͡ ���������");
			document.frm01.hStockKomId.focus();
			return false;
		}
	
	
		if (document.forms.frm01.DayProtect.value=="" || document.forms.frm01.DayProtect.value=="00")
		{
			alert("��س��к��ѹ��������ͧ");
			document.forms.frm01.DayProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.DayProtect.value,1,31) == false) {
				document.forms.frm01.DayProtect.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.MonthProtect.value==""  || document.forms.frm01.MonthProtect.value=="00")
		{
			alert("��س��к���͹��������ͧ");
			document.forms.frm01.MonthProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.MonthProtect.value,1,12) == false){
				document.forms.frm01.MonthProtect.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.YearProtect.value==""  || document.forms.frm01.YearProtect.value=="0000")
		{
			alert("��س��кػշ�������ͧ");
			document.forms.frm01.YearProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.YearProtect.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.YearProtect.focus();
				return false;
			}
		} 				
		
	
		if(document.frm01.hInsureType[0].checked == false && document.frm01.hInsureType[1].checked == false && document.frm01.hInsureType[2].checked == false && document.frm01.hInsureType[3].checked == false ){
			alert("��س����͡��������Сѹ���");
			document.frm01.hInsureType[0].focus();
			return false;		
		}
		
		if(document.frm01.hPaymentCondition[0].checked == false && document.frm01.hPaymentCondition[1].checked == false ){
			alert("��س����͡���͹䢡�â��");
			document.frm01.hPaymentCondition[0].focus();
			return false;		
		}		

		if(document.frm01.hLoneType.checked == false && document.frm01.hLoneType01.checked == false  ){
			alert("��س����͡�������");
			document.frm01.hLoneType.focus();
			return false;		
		}		
		
		if(document.frm01.hPaymentType[0].checked == false && document.frm01.hPaymentType[1].checked == false && document.frm01.hPaymentType[2].checked == false ){
			alert("��س����͡�Ըժ���");
			document.frm01.hPaymentType[0].focus();
			return false;		
		}				
		
		if(document.frm01.hPaymentAmount.value ==0){
			alert("��س����͡�ӹǹ�Ǵ������");
			document.frm01.hPaymentAmount.focus();
			return false;
		}		
		
		if(document.frm01.hPaymentAmount.value ==0){
			alert("��س����͡�ӹǹ�Ǵ������");
			document.frm01.hPaymentAmount.focus();
			return false;
		}				
		
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
		
	}
	
	function checkDisplay(var_num){
		if(var_num ==0){
			document.getElementById("pay00").style.display = "none";
			document.getElementById("pay01").style.display = "none";
			document.getElementById("pay02").style.display = "none";
			document.getElementById("pay03").style.display = "none";								
			document.getElementById("pay04").style.display = "none";			
			document.getElementById("pay05").style.display = "none";			
			document.getElementById("pay06").style.display = "none";			
			document.getElementById("pay07").style.display = "none";			
		}
		if(var_num ==1){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "none";
			document.getElementById("pay03").style.display = "none";		
			document.getElementById("pay05").style.display = "none";		
			document.getElementById("pay06").style.display = "none";		
			document.getElementById("pay07").style.display = "none";								
			document.getElementById("pay04").style.display = "";			
		}
		if(var_num ==2){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "none";			
			document.getElementById("pay05").style.display = "none";		
			document.getElementById("pay06").style.display = "none";		
			document.getElementById("pay07").style.display = "none";												
			document.getElementById("pay04").style.display = "";			
		}
		if(var_num ==3){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";			
			document.getElementById("pay05").style.display = "none";			
			document.getElementById("pay06").style.display = "none";			
			document.getElementById("pay07").style.display = "none";			
		}
		if(var_num ==4){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";			
			document.getElementById("pay05").style.display = "";			
			document.getElementById("pay06").style.display = "none";			
			document.getElementById("pay07").style.display = "none";					
		}
		if(var_num ==5){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";		
			document.getElementById("pay05").style.display = "";			
			document.getElementById("pay06").style.display = "";			
			document.getElementById("pay07").style.display = "none";						
		}
		if(var_num ==6){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";		
			document.getElementById("pay05").style.display = "";			
			document.getElementById("pay06").style.display = "";			
			document.getElementById("pay07").style.display = "";						
		}
		
		
		
		
	}
	
	
	function checkSum(){
		var num01, num02, num03, num04, num05, num06;
		
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		num06=0;
		
		if(document.frm01.hPaymentPrice01){
			num01 = document.frm01.hPaymentPrice01.value*1;
		}
		if(document.frm01.hPaymentPrice02){
			num02 = document.frm01.hPaymentPrice02.value*1;
		}		
		if(document.frm01.hPaymentPrice03){
			num03 = document.frm01.hPaymentPrice03.value*1;
		}		
		if(document.frm01.hPaymentPrice04){
			num04 = document.frm01.hPaymentPrice04.value*1;
		}		
		if(document.frm01.hPaymentPrice05){
			num05 = document.frm01.hPaymentPrice05.value*1;
		}		
		if(document.frm01.hPaymentPrice06){
			num06 = document.frm01.hPaymentPrice06.value*1;
		}								
		sumnum = num01+num02+num03+num04+num05+num06;
		document.frm01.hInsurePrice.value=sumnum.toFixed(2);
	
	}	
	
	function sumBea(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		if(document.frm01.hBeasuti){
			num01 = document.frm01.hBeasuti.value*1;
		}
		if(document.frm01.hArgon){
			num02 = document.frm01.hArgon.value*1;
		}		
		if(document.frm01.hPasi){
			num03 = document.frm01.hPasi.value*1;
		}		
		
		sumnum = num01+num02+num03;

		document.frm01.hTotal01.value=sumnum.toFixed(2);
	
	}	
	
	function sumNiti(){
		var num01;
		
		num01=0;
		if(document.frm01.hBeasuti){
			num01 = document.frm01.hBeasuti.value*1;
		}
		
		num02=0;
		if(document.frm01.hArgon){
			num02 = document.frm01.hArgon.value*1;
		}

		sumnum = ( (num01*1) + (num02*1) )/100;
		document.frm01.hTotal02.value=sumnum.toFixed(2);
	
	}	
	
	function sumNiti01(){
		document.frm01.hTotal02.value="";	
	}	
	
	
	function checkDiscount01(){
		var num01, num02;
		
		num01=0;
		num02=0;
		if(document.frm01.hDiscountPercent){
			num01 = document.frm01.hDiscountPercent.value*1;
		}
		if(document.frm01.hTotal03){
			num02 = document.frm01.hTotal03.value*1;
		}
		sumnum = (num02*num01)/100;
		document.frm01.hDiscountPercentPrice.value=sumnum.toFixed(2);
		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
	
	}	
	
	function checkDiscount02(){

		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		
	}	
	
	function checkDiscount03(){

		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		
	}	
	
	function sumAll(){		
		num01=0;
		num02=0;
		num03=0;
		num04=0;	
		num05=0;	
		num06=0;	
		num07=0;	
	
		sumBea();
		//sum niti
		if(document.frm01.hNiti[0].checked == true){
			sumNiti01();
		}else{
			sumNiti();
		}
		
		
		if(document.frm01.hTotal01){
			num01 = document.frm01.hTotal01.value*1;
		}
		if(document.frm01.hTotal02){
			num02 = document.frm01.hTotal02.value*1;
		}
		
		num03 = num01-num02;
		document.frm01.hTotal03.value= num03.toFixed(2);
		
		if(document.frm01.hDiscountType[0].checked == true){
			checkDiscount01();
			num04 = document.frm01.hDiscountPercentPrice.value*1;
		}
		if(document.frm01.hDiscountType[1].checked == true){
			checkDiscount02();
			num05 = document.frm01.hDiscountOtherPrice.value*1;
		}
		
		if(document.frm01.hPrbPrice01.value > 0 ){
			num06 = document.frm01.hPrbPrice01.value*1;
		}
		
		if(document.frm01.hDiscountType[2].checked == true){
			num07 = num03-num04-num05;
		}else{
			num07 = num03-num04-num05+num06;
		}
		document.frm01.hTotal.value= num07.toFixed(2);
	}
	
	
	function same_prb(){
		var prb_id = document.frm01.hStockPrbId.value;
	
		if( prb_id > 0){
			document.frm01.hInsureCompanyId01.value = document.frm01.hInsureCompanyId.value;
			document.frm01.hStockKomId.value = document.frm01.hStockPrbId.value;
		
		}
	
	}
	
	function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPrbPrice.value = <?=$objItem->get_total()?>;
			document.frm01.hPrbPrice01.value = <?=$objItem->get_total()?>;
		}
	<?}?>
		sumAll();
	}
	
	<?if($objInsureOld->get_payment_amount() > 0){?>
		checkDisplay(<?=$objInsureOld->get_payment_amount()?>);
		checkSum();
	<?}?>
	
</script>	


<?}?>
<?include "unset_all.php";?>
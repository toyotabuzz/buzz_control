<?php

include('src/BarcodeGenerator.php');
include('src/BarcodeGeneratorPNG.php');
include('src/BarcodeGeneratorSVG.php');
include('src/BarcodeGeneratorHTML.php');

$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
$generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();
$generatorHTML = new Picqer\Barcode\BarcodeGeneratorHTML();

//echo $generatorHTML->getBarcode('081231723897', $generatorPNG::TYPE_CODE_128);
//echo $generatorSVG->getBarcode('081231723897', $generatorPNG::TYPE_EAN_13);
//$code = "1111".char(13)."2222";
/* Often this is more useful */

$str = "|010555308043500".chr(13)."0047076882".chr(13)."00214429".chr(13)."0";
//$str = "|010555308043500".chr(13)."0047076882";

echo '<br><br><br><br><br><br><img src="data:image/png;base64,' . base64_encode($generatorPNG->getBarcode($str, $generatorPNG::TYPE_CODE_128,1,50)) . '">';
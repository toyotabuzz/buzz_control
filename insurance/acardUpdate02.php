<?
include("common.php");

$objInsureCar = new InsureCar();
$objInsureOld = new Insure();
$objInsureList = new InsureList();
$objOrder = new Order();
$objInsureItem01 = new InsureItem();
$objInsureItem02 = new InsureItem();
$objInsureItem03 = new InsureItem();
$objInsureItem04 = new InsureItem();
$objInsureItem05 = new InsureItem();
$objInsureItem06 = new InsureItem();
$objInsureItem07 = new InsureItem();
$objInsureItem08 = new InsureItem();
$objInsureItem09 = new InsureItem();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();


$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault("title ASC");
$objCarSeriesList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCustomer = new Customer();
$objEvent = new CustomerEvent();
$objMember = new Member();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objStockPrbList = new StockPrbList();
$objStockPrbList->setFilter(" use_status_01 = 0 ");
$objStockPrbList->setPageSize(0);
$objStockPrbList->setSort(" title ASC");
$objStockPrbList->load();

$objStockKomList = new StockPrbList();
$objStockKomList->setFilter(" use_status_02 = 0 ");
$objStockKomList->setPageSize(0);
$objStockKomList->setSort(" title ASC");
$objStockKomList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' and M.company_id= $sCompanyId ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();


$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();



if(isset($hSubmitDelete)){
	$objCustomerContact = new CustomerContact();
	$objCustomerContact->setCustomerContactId($hSubmitDelete);
	$objCustomerContact->delete();
	unset($objCustomerContact);
}

if(isset($hDuplicateId)){
	$objCustomerDuplicate = new CustomerDuplicate();
	$objCustomerDuplicate->setCustomerId($hDuplicateId);
	$objCustomerDuplicate->setMemberId($sMemberId);
	$objCustomerDuplicate->add();
	unset($objCustomerDuplicate);
	header("location:acardUpdate.php");
	exit;
}

if(isset($hDuplicateDeleteId)){
	$objCustomerDuplicate = new CustomerDuplicate();
	$objCustomerDuplicate->setDuplicateId($hDuplicateDeleteId);
	$objCustomerDuplicate->delete();
	unset($objCustomerDuplicate);
	header("location:acardUpdate.php?hId=$hId");
	exit;
}

//load customer
if($hAddCustomerId != ""){
	$objCustomer->setCustomerId($hAddCustomerId);
	$objCustomer->load();
	
	$arrDate = explode("-",$objCustomer->getInformationDate());
	$Day = $arrDate[2];
	$Month = $arrDate[1];
	$Year = $arrDate[0];
	
	$arrDate = explode("-",$objCustomer->getBirthDay());
	$Day01 = $arrDate[2];
	$Month01 = $arrDate[1];
	$Year01 = $arrDate[0];
	
	$objEvent = new CustomerEvent();
	$objEvent->setEventId($objCustomer->getEventId());
	$objEvent->load();
	
	$objMember = new Member();
	$objMember->setMemberId($sMemberId);
	$objMember->load();
	
	//extract mobile 
	$arrMobile = explode(",",$objCustomer->getMobile());
	$hMobile_1 = $arrMobile[0];
	$hMobile_2 = $arrMobile[1];
	$hMobile_3 = $arrMobile[2];
	
	$hCustomerId = $hAddCustomerId;
}

if (empty($hSubmit)) {
	if ($hId !="") {
		$objInsureList->setFilter(" car_id= $hId ");
		$objInsureList->setPageSize(0);
		$objInsureList->setSort(" insure_date DESC ");
		$objInsureList->load();
	
		$objInsureCar->set_car_id($hId);
		$objInsureCar->load();
		
		$hCustomerId = $objInsureCar->get_customer_id();
		
		$arrDate = explode("-",$objInsureCar->get_call_date());
		$DayCall = $arrDate[2];
		$MonthCall = $arrDate[1];
		$YearCall = $arrDate[0];
	
		$objCustomer->setCustomerId($objInsureCar->get_customer_id());
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$objEvent = new CustomerEvent();
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsureCar->get_sale_id());
		$objMember->load();
		
		//extract mobile 
		$arrMobile = explode(",",$objCustomer->getMobile());
		$hMobile_1 = $arrMobile[0];
		$hMobile_2 = $arrMobile[1];
		$hMobile_3 = $arrMobile[2];
		
		
		$objInsureOld->loadByCondition(" car_id=$hId  ");
		
		$arrDate = explode("-",$objInsureOld->get_date_protect());
		$DayProtect = $arrDate[2];
		$MonthProtect = $arrDate[1];
		$YearProtect = $arrDate[0];
		
		$hInsureId= $objInsureOld->get_insure_id();
		
	} else {
		$strMode="Add";
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
		
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
	}

	
	if(isset($hInsureId)){
		$objInsureOld->set_insure_id($hInsureId);
		$objInsureOld->load();
	
		$arrDate = explode("-",$objInsureOld->get_date_protect());
		$DayProtect = $arrDate[2];
		$MonthProtect = $arrDate[1];
		$YearProtect = $arrDate[0];
		
		$arrDate = explode("-",$objInsureOld->get_date_start_prb());
		$DayStartPrb = $arrDate[2];
		$MonthStartPrb = $arrDate[1];
		$YearStartPrb = $arrDate[0];	
		
		$arrDate = explode("-",$objInsureOld->get_date_end_prb());
		$DayEndPrb = $arrDate[2];
		$MonthEndPrb = $arrDate[1];
		$YearEndPrb = $arrDate[0];	
		
		$arrDate = explode("-",$objInsureOld->get_date_start_kom());
		$DayStartKom = $arrDate[2];
		$MonthStartKom = $arrDate[1];
		$YearStartKom = $arrDate[0];	
		
		$arrDate = explode("-",$objInsureOld->get_date_end_kom());
		$DayEndKom = $arrDate[2];
		$MonthEndKom = $arrDate[1];
		$YearEndKom = $arrDate[0];	
		
		$arrDate = explode("-",$objInsureOld->get_acc_date());
		$DayReportDate = $arrDate[2];
		$MonthReportDate = $arrDate[1];
		$YearReportDate = $arrDate[0];	
		
		$arrDate = explode("-",$objInsureOld->get_acc_recieve_date());
		$DayRecieveReport = $arrDate[2];
		$MonthRecieveReport = $arrDate[1];
		$YearRecieveReport = $arrDate[0];	
		
		$objQIList = new InsureItemList();
		$objQIList->setFilter(" insure_id = $hInsureId ");
		$objQIList->setPageSize(0);
		$objQIList->setSort(" insure_item_id ASC ");
		$objQIList->load();
		$i=0;
		forEach($objQIList->getItemList() as $objItem) {
			if($i==0){
				$objInsureItem01->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem01->load();
				
				$arrDate = explode("-",$objInsureItem01->get_payment_date());
				$Day01 = $arrDate[2];
				$Month01 = $arrDate[1];
				$Year01 = $arrDate[0];
			}
			
			if($i==1){
				$objInsureItem02->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem02->load();
				
				$arrDate = explode("-",$objInsureItem02->get_payment_date());
				$Day02 = $arrDate[2];
				$Month02 = $arrDate[1];
				$Year02 = $arrDate[0];
				
			}
	
			if($i==2){
				$objInsureItem03->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem03->load();
				
				$arrDate = explode("-",$objInsureItem03->get_payment_date());
				$Day03 = $arrDate[2];
				$Month03 = $arrDate[1];
				$Year03 = $arrDate[0];
				
			}
			
			if($i==3){
				$objInsureItem04->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem04->load();
				
				$arrDate = explode("-",$objInsureItem04->get_payment_date());
				$Day04 = $arrDate[2];
				$Month04 = $arrDate[1];
				$Year04 = $arrDate[0];
				
			}
			
			if($i==4){
				$objInsureItem05->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem05->load();
				
				$arrDate = explode("-",$objInsureItem05->get_payment_date());
				$Day05 = $arrDate[2];
				$Month05 = $arrDate[1];
				$Year05 = $arrDate[0];
				
			}
			
			if($i==5){
				$objInsureItem06->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem06->load();
				
				$arrDate = explode("-",$objInsureItem06->get_payment_date());
				$Day06 = $arrDate[2];
				$Month06 = $arrDate[1];
				$Year06 = $arrDate[0];
				
			}
			
			if($i==6){
				$objInsureItem07->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem07->load();
				
				$arrDate = explode("-",$objInsureItem07->get_payment_date());
				$Day07 = $arrDate[2];
				$Month07 = $arrDate[1];
				$Year07 = $arrDate[0];
				
			}
			
			if($i==7){
				$objInsureItem08->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem08->load();
				
				$arrDate = explode("-",$objInsureItem08->get_payment_date());
				$Day08 = $arrDate[2];
				$Month08 = $arrDate[1];
				$Year08 = $arrDate[0];
				
			}
			
			if($i==8){
				$objInsureItem09->set_insure_item_id($objItem->get_insure_item_id());
				$objInsureItem09->load();
				
				$arrDate = explode("-",$objInsureItem09->get_payment_date());
				$Day09 = $arrDate[2];
				$Month09 = $arrDate[1];
				$Year09 = $arrDate[0];
				
			}
			
			$i++;
		}
	
	}
	
	
	
} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hCustomerId);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
            $objCustomer->setTypeId($hTypeId);
			$objCustomer->setACard($hAcard);
			$objCustomer->setBCard($hBCard);
			$objCustomer->setCCard($hCCard);
			$objCustomer->setICard(1);
            $objCustomer->setGradeId($hGradeId);
            $objCustomer->setGroupId($hGroupId);
			$objCustomer->setEventId($hEventId);
			$hPostedDate = $Year."-".$Month."-".$Day;
            $objCustomer->setInformationDate($hPostedDate);
            $objCustomer->setInsureMember($hSaleId);
			$hBirthday = $BirthYear01."-".$BirthMonth01."-".$BirthDay01;
            $objCustomer->setBirthday($hBirthday);
            $objCustomer->setTitle($hTitle);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setEmail($hEmail);
			$objCustomer->setIDCard($hIDCard);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setHomeTel($hHomeTel);
			
			$i=0;
			if($hMobile_1 != ""){ $arrMobile[$i] = $hMobile_1;$i++;}
			if($hMobile_2 != ""){ $arrMobile[$i] = $hMobile_2;$i++;}
			if($hMobile_3 != ""){ $arrMobile[$i] = $hMobile_3;$i++;}
			
			$hMobile = implode(",",$arrMobile);
			
			$objCustomer->setMobile($hMobile);
			$objCustomer->setOfficeTel($hOfficeTel);
			$objCustomer->setFax($hFax);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setAddress01($hAddress01);
			$objCustomer->setTumbon01($hTumbon01);
			$objCustomer->setAmphur01($hAmphur01);
			$objCustomer->setProvince01($hProvince01);
			$objCustomer->setTumbonCode01($hTumbonCode01);
			$objCustomer->setAmphurCode01($hAmphurCode01);
			$objCustomer->setProvinceCode01($hProvinceCode01);
			$objCustomer->setZipCode01($hZipCode01);
			$objCustomer->setZip01($hZip01);
			$objCustomer->setTel01($hTel01);
			$objCustomer->setFax01($hFax01);
			
			$objCustomer->setCompany($hCompany);
			$objCustomer->setAddress02($hAddress02);
			$objCustomer->setTumbon02($hTumbon02);
			$objCustomer->setAmphur02($hAmphur02);
			$objCustomer->setProvince02($hProvince02);
			$objCustomer->setTumbonCode02($hTumbonCode02);
			$objCustomer->setAmphurCode02($hAmphurCode02);
			$objCustomer->setProvinceCode02($hProvinceCode02);
			$objCustomer->setZipCode02($hZipCode02);
			$objCustomer->setZip02($hZip02);
			$objCustomer->setTel02($hTel02);
			$objCustomer->setFax02($hFax02);
			
			$objCustomer->setName03($hName03);
			$objCustomer->setAddress03($hAddress03);
			$objCustomer->setTumbon03($hTumbon03);
			$objCustomer->setAmphur03($hAmphur03);
			$objCustomer->setProvince03($hProvince03);
			$objCustomer->setTumbonCode03($hTumbonCode03);
			$objCustomer->setAmphurCode03($hAmphurCode03);
			$objCustomer->setProvinceCode03($hProvinceCode03);
			$objCustomer->setZipCode03($hZipCode03);
			$objCustomer->setZip03($hZip03);
			$objCustomer->setTel03($hTel03);
			$objCustomer->setFax03($hFax03);
			$objCustomer->setMailback($hMailback);
			$objCustomer->setIncomplete($hIncomplete);
			$objCustomer->setMailback($hMailback);
			$objCustomer->setVerifyAddress($hVerifyAddress);
			$objCustomer->setVerifyPhone($hVerifyPhone);
			
			$objInsureCar->set_car_id($hId);
			$objInsureCar->set_sale_id($sMemberId);
			$objInsureCar->set_source($hSource);
			$objInsureCar->set_car_type($hCarType);
			$objInsureCar->set_customer_id($hCustomerId);
			$objInsureCar->set_code($hCarCode);
			$objInsureCar->set_price($hCarPrice);
			$objInsureCar->set_car_model_id($hCarModel);
			$objInsureCar->set_car_series_id($hCarSeries);
			$objInsureCar->set_color($hCarColor);
			$objInsureCar->set_car_number($hCarNumber);
			$objInsureCar->set_engine_number($hEngineNumber);
			$objInsureCar->set_register_year($hRegisterYear);
			$objInsureCar->set_suggest_from($hSuggestFrom);
			$hCallDate = $YearCall."-".$MonthCall."-".$DayCall;
			$objInsureCar->set_call_date($hCallDate);
			$objInsureCar->set_call_remark($hCallRemark);
			$objInsureCar->set_date_add(date("Y-m-d"));
			
			$objInsureOld->set_insure_id($hInsureId);
			$objInsureOld->set_sale_id($hSaleId);
			$objInsureOld->set_car_id($hCarId);
			$objInsureOld->set_verify_code($hVerifyCode);
			$objInsureOld->set_insure_old($hInsureOld);
			$objInsureOld->set_stock_prb_id($hStockPrbId);
			$objInsureOld->set_stock_kom_id($hStockKomId);
			$objInsureOld->set_insure_date($hInsureDate);
			$objInsureOld->set_insure_company_id($hInsureCompanyId);
			
			$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;
			$objInsureOld->set_date_protect($hDateProtect);
			$hDateStartPrb = $YearStartPrb."-".$MonthStartPrb."-".$DayStartPrb;
		
			$objInsureOld->set_date_start_prb($hDateStartPrb);
			$hDateEndPrb = $YearEndPrb."-".$MonthEndPrb."-".$DayEndPrb;
			$objInsureOld->set_date_end_prb($hDateEndPrb);
			$hDateStartKom = $YearStartKom."-".$MonthStartKom."-".$DayStartKom;
			$objInsureOld->set_date_start_kom($hDateStartKom);
			$hDateEndKom = $YearEndKom."-".$MonthEndKom."-".$DayEndKom;
			$objInsureOld->set_date_end_kom($hDateEndKom);
		
			$objInsureOld->set_insure_type($hInsureType);
			$objInsureOld->set_insure_type_remark($hInsureTypeRemark);
			$objInsureOld->set_prb($hPrb);
			$objInsureOld->set_prb_remark($hPrbRemark);
			$objInsureOld->set_payment_condition($hPaymentCondition);
			$objInsureOld->set_payment_condition_remark($hPaymentConditionRemark);
			$objInsureOld->set_payment_amount($hPaymentAmount);
			$objInsureOld->set_lone_type($hLoneType);
			$objInsureOld->set_lone_remark($hLoneRemark);
			$objInsureOld->set_lone_type01($hLoneType01);
			$objInsureOld->set_lone_type01_remark($hLoneType01Remark);
			$objInsureOld->set_payment_type($hPaymentType);
			$objInsureOld->set_status("old");
			$objInsureOld->set_price($hPrice);
			$objInsureOld->set_remark($hRemark);
			$objInsureOld->set_send_mail($hSendMail);
			$objInsureOld->set_quotation_id($hQuotationId);
			$objInsureOld->set_discount($hDiscount);
			$objInsureOld->set_discount_remark($hDiscountRemark);
			$objInsureOld->set_free($hFree);
			$objInsureOld->set_free_remark($hFreeRemark);
			$objInsureOld->set_fund($hFund);
			$objInsureOld->set_insure_fee_id($hInsureFeeId);
			$objInsureOld->set_prb_price($hPrbPrice);
			$objInsureOld->set_acc_no($hAccNo);
			$objInsureOld->set_car_status($hCarStatus);
			$objInsureOld->set_beasuti($hBeasuti);
			$objInsureOld->set_pasi($hPasi);
			$objInsureOld->set_argon($hArgon);
			$objInsureOld->set_niti($hNiti);
			$objInsureOld->set_kom_number($hKomNumber);
			$hAccDate = $YearReportDate."-".$MonthReportDate."-".$DayReportDate;
			$objInsureOld->set_acc_date($hAccDate);
			$hAccRecieveDate = $YearRecieveReport."-".$MonthRecieveReport."-".$DayRecieveReport;
			$objInsureOld->set_acc_recieve_date($hAccRecieveDate);
			$objInsureOld->set_discount_type($hDiscountType);
			$objInsureOld->set_discount_other_remark($hDiscountOtherRemark);
			$objInsureOld->set_discount_other_price($hDiscountOtherPrice);
			$objInsureOld->set_discount_percent($hDiscountPercent);
			$objInsureOld->set_repair($hRepair);
			$objInsureOld->set_insure_old(1);
			$objInsureOld->set_prb_number($hPrbNumber);
			
    		$pasrErr = $objCustomer->check($hSubmit);

			If ( Count($pasrErr) == 0 ){
				if ($strMode=="Update") {
					$objInsureCar->set_date_verify($hDateVerify);
				
					//check exist customer
					if($hCustomerId  < 1 ){
						$objCustomer->setTypeId("I");
						$hCustomerId=$objCustomer->addInsurance();
					}else{
						if($hTypeId = "I"){
							$objCustomer->updateInsurance();
						}
					}
					//update car
					$objInsureCar->set_customer_id($hCustomerId);
					$objInsureCar->update();
					
					//update insure old
					$objInsureOld->set_car_id($hId);
					$objInsureOld->set_sale_id($sMemberId);
					$objInsureOld->set_insure_old(1);
					if($hInsureId > 0){
						$objInsureOld->update();
					
					}else{
						$hInsureId = $objInsureOld->add();
					}
					
						$objInsureItem01->set_insure_item_id($hInsureItemId01);
						$objInsureItem01->set_insure_id($hInsureId);
						$objInsureItem01->set_payment_type($hPaymentType01);
						$hPaymentDate01 = $Year01."-".$Month01."-".$Day01;
						$objInsureItem01->set_payment_date($hPaymentDate01);
						$objInsureItem01->set_payment_price($hPaymentPrice01);
						$objInsureItem01->set_payment_status($hPaymentStatus01);
						$objInsureItem01->set_payment_number($hPaymentNumber01);
						$objInsureItem01->set_insure_date($hInsureDate01);
						$objInsureItem01->set_remark($hRemark01);
						
						if($hInsureItemId01 > 0){
							$objInsureItem01->update();
						}else{
							$objInsureItem01->add();
						}
						
						if($hPaymentAmount > 1 ){
							$objInsureItem02->set_insure_item_id($hInsureItemId02);
							$objInsureItem02->set_insure_id($hInsureId);
							$objInsureItem02->set_payment_type($hPaymentType02);
							$hPaymentDate02 = $Year02."-".$Month02."-".$Day02;
							$objInsureItem02->set_payment_date($hPaymentDate02);
							$objInsureItem02->set_payment_price($hPaymentPrice02);
							$objInsureItem02->set_payment_status($hPaymentStatus02);
							$objInsureItem02->set_payment_number($hPaymentNumber02);
							$objInsureItem02->set_insure_date($hInsureDate02);
							$objInsureItem02->set_remark($hRemark02);
							
							if($hInsureItemId02 > 0){
								$objInsureItem02->update();
							}else{
								$objInsureItem02->add();
							}
						}
						
						if($hPaymentAmount > 2 ){
							$objInsureItem03->set_insure_item_id($hInsureItemId03);
							$objInsureItem03->set_insure_id($hInsureId);
							$objInsureItem03->set_payment_type($hPaymentType03);
							$hPaymentDate03 = $Year03."-".$Month03."-".$Day03;
							$objInsureItem03->set_payment_date($hPaymentDate03);
							$objInsureItem03->set_payment_price($hPaymentPrice03);
							$objInsureItem03->set_payment_status($hPaymentStatus03);
							$objInsureItem03->set_payment_number($hPaymentNumber03);
							$objInsureItem03->set_insure_date($hInsureDate03);
							$objInsureItem03->set_remark($hRemark03);
							
							if($hInsureItemId03 > 0){
								$objInsureItem03->update();
							}else{
								$objInsureItem03->add();
							}
						}
						
						if($hPaymentAmount == 1 ){
							if($hInsureItemId02 > 0){
								$objInsureItem02->set_insure_item_id($hInsureItemId02);
								$objInsureItem02->delete();
							}
							if($hInsureItemId03 > 0){
								$objInsureItem03->set_insure_item_id($hInsureItemId03);
								$objInsureItem03->delete();
							}
						}
						
						if($hPaymentAmount == 2 ){
							if($hInsureItemId03 > 0){
								$objInsureItem03->set_insure_item_id($hInsureItemId03);
								$objInsureItem03->delete();
							}
						}
					
					
				} else {
					//check exist customer
					if($hCustomerId  < 1 ){
						$objCustomer->setTypeId("I");
						$hCustomerId=$objCustomer->addInsurance();
					}else{
						//$objCustomer->updateInsurance();
					}
					
					//add insure car
					//check existing car
						
						
					//add car
					$objInsureCar->set_date_verify($hDateProtect);
					$objInsureCar->set_customer_id($hCustomerId);
					$hId = $objInsureCar->add();
					
					//add insure old
					$objInsureOld->set_car_id($hId);
					$objInsureOld->set_sale_id($sMemberId);
					$objInsureOld->set_insure_old(1);
					$hInsureId = $objInsureOld->add();
					
						$objInsureItem01->set_insure_item_id($hInsureItemId01);
						$objInsureItem01->set_insure_id($hInsureId);
						$objInsureItem01->set_payment_type($hPaymentType01);
						$hPaymentDate01 = $Year01."-".$Month01."-".$Day01;
						$objInsureItem01->set_payment_date($hPaymentDate01);
						$objInsureItem01->set_payment_price($hPaymentPrice01);
						$objInsureItem01->set_payment_status($hPaymentStatus01);
						$objInsureItem01->set_payment_number($hPaymentNumber01);
						$objInsureItem01->set_insure_date($hInsureDate01);
						$objInsureItem01->set_remark($hRemark01);
						
						if($hInsureItemId01 > 0){
							$objInsureItem01->update();
						}else{
							$objInsureItem01->add();
						}
						
						if($hPaymentAmount > 1 ){
							$objInsureItem02->set_insure_item_id($hInsureItemId02);
							$objInsureItem02->set_insure_id($hInsureId);
							$objInsureItem02->set_payment_type($hPaymentType02);
							$hPaymentDate02 = $Year02."-".$Month02."-".$Day02;
							$objInsureItem02->set_payment_date($hPaymentDate02);
							$objInsureItem02->set_payment_price($hPaymentPrice02);
							$objInsureItem02->set_payment_status($hPaymentStatus02);
							$objInsureItem02->set_payment_number($hPaymentNumber02);
							$objInsureItem02->set_insure_date($hInsureDate02);
							$objInsureItem02->set_remark($hRemark02);
							
							if($hInsureItemId02 > 0){
								$objInsureItem02->update();
							}else{
								$objInsureItem02->add();
							}
						}
						
						if($hPaymentAmount > 2 ){
							$objInsureItem03->set_insure_item_id($hInsureItemId03);
							$objInsureItem03->set_insure_id($hInsureId);
							$objInsureItem03->set_payment_type($hPaymentType03);
							$hPaymentDate03 = $Year03."-".$Month03."-".$Day03;
							$objInsureItem03->set_payment_date($hPaymentDate03);
							$objInsureItem03->set_payment_price($hPaymentPrice03);
							$objInsureItem03->set_payment_status($hPaymentStatus03);
							$objInsureItem03->set_payment_number($hPaymentNumber03);
							$objInsureItem03->set_insure_date($hInsureDate03);
							$objInsureItem03->set_remark($hRemark03);
							
							if($hInsureItemId03 > 0){
								$objInsureItem03->update();
							}else{
								$objInsureItem03->add();
							}
						}
						
						if($hPaymentAmount == 1 ){
							if($hInsureItemId02 > 0){
								$objInsureItem02->set_insure_item_id($hInsureItemId02);
								$objInsureItem02->delete();
							}
							if($hInsureItemId03 > 0){
								$objInsureItem03->set_insure_item_id($hInsureItemId03);
								$objInsureItem03->delete();
							}
						}
						
						if($hPaymentAmount == 2 ){
							if($hInsureItemId03 > 0){
								$objInsureItem03->set_insure_item_id($hInsureItemId03);
								$objInsureItem03->delete();
							}
						}
					
					
					
					
				}
				unset ($objCustomer);
				if($strMode == "Update"){
					$hNewLink = str_replace("YYY", "&", $hLink);
					header("location:".$hNewLink);
				}else{
					header("location:acardListVerify.php");
				}
				//exit;
			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)
		}
}


$pageTitle = "1. �к��������١���";
$strHead03 = "�ѹ�֡�������١���";
if($strMode == "Add"){ $pageContent = "1.2 �ѹ�֡�������١��� > �����������١���";}else{$pageContent = "1.2 �ѹ�֡�������١��� > ��䢢������١���";}
include("h_header.php");
?>
<script type="text/javascript" src="../function/check_mobile.js"></script>
<script language="JavaScript">
	
	<?if($objCustomer->getTypeId() == 'A' OR $objCustomer->getTypeId() == '' ){?>
	function check_submit()
	{

		if (document.forms.frm01.hGroupId.options[frm01.hGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡���觷���Ңͧ�١���");
			document.forms.frm01.hGroupId.focus();
			return false;
		} 	

		if (document.forms.frm01.hEventId.value=="")
		{
			alert("��س��кت��ͧҹ�͡ʶҹ���");
			document.forms.frm01.hEvent.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hSaleId.value=="")
		{
			alert("��س��кت��ͼ����������");
			document.forms.frm01.hSale.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت���-���ʡ��");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 			
	
		if(document.forms.frm01.hIncomplete.checked == false){
			
	
			if (document.forms.frm01.hAddress.value=="")
			{
				alert("��س��кط������");
				document.forms.frm01.hAddress.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hTumbonCode.value=="")
			{
				alert("��س��кصӺ�");
				document.forms.frm01.hTumbon.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hAmphurCode.value=="")
			{
				alert("��س��к������");
				document.forms.frm01.hAmphur.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hProvinceCode.value=="")
			{
				alert("��س��кبѧ��Ѵ");
				document.forms.frm01.hProvince.focus();
				return false;
			} 
			
			if (document.forms.frm01.hZipCode.value=="")
			{
				alert("��س��к�������ɳ���");
				document.forms.frm01.hZip.focus();
				return false;
			} 							

		}
		

		
	}
	
	<?}else{?>
	function check_submit()
	{
	
		if (document.forms.frm01.hGroupId.options[frm01.hGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡���觷���Ңͧ�١���");
			document.forms.frm01.hGroupId.focus();
			return false;
		} 	

		if (document.forms.frm01.hEventId.value=="")
		{
			alert("��س��кت��ͧҹ�͡ʶҹ���");
			document.forms.frm01.hEvent.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hSaleId.value=="")
		{
			alert("��س��кت��ͼ����������");
			document.forms.frm01.hSale.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت���-���ʡ��");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 			
	
		if(document.forms.frm01.hIncomplete.checked == false){
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			Set_Display_Type('1');
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س��кصӺ�");
			Set_Display_Type('1');
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س��к������");
			Set_Display_Type('1');
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س��кبѧ��Ѵ");
			Set_Display_Type('1');
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س��к�������ɳ���");
			Set_Display_Type('1');
			document.forms.frm01.hZip.focus();
			return false;
		} 						
		
		if ( (document.forms.frm01.hMobile_1.value=="" && document.forms.frm01.hMobile_2.value=="" && document.forms.frm01.hMobile_3.value=="" )    && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			Set_Display_Type('1');
			document.forms.frm01.hHomeTel.focus();
			return false;
		} 	
		
		
		if (document.forms.frm01.hAddress03.value=="")
		{
			alert("��س��кط�����������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAddress03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode03.value=="")
		{
			alert("��س��кصӺŷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hTumbon03.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode03.value=="")
		{
			alert("��س��к�����ͷ�����͡���");
			Set_Display_Type('4');
			document.forms.frm01.hAmphur03.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode03.value=="")
		{
			alert("��س��кبѧ��Ѵ������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hProvince03.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode03.value=="")
		{
			alert("��س��к�������ɳ��������͡���");
			Set_Display_Type('4');
			document.forms.frm01.hZip03.focus();
			return false;
		} 								

		}



	}

	
function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			if(document.frm01.hCustomerSamePlace01.checked == true){
				document.frm01.hAddress01.value = document.frm01.hAddress.value;
				document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
				document.frm01.hProvince01.value = document.frm01.hProvince.value;
				document.frm01.hZip01.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode01.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode01.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode01.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode01.value = document.frm01.hZipCode.value;		
				document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax01.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress01.value = "";
				document.frm01.hTumbon01.value = "";
				document.frm01.hAmphur01.value = "";
				document.frm01.hProvince01.value = "";
				document.frm01.hZip01.value = "";		
				document.frm01.hTumbonCode01.value = "";
				document.frm01.hAmphurCode01.value = "";
				document.frm01.hProvinceCode01.value = "";
				document.frm01.hZipCode01.value = "";		
				document.frm01.hTel01.value = "";		
				document.frm01.hFax01.value = "";		
			}
		}
   		if(val ==2){
			if(document.frm01.hCustomerSamePlace02.checked == true){
				document.frm01.hAddress02.value = document.frm01.hAddress.value;
				document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
				document.frm01.hProvince02.value = document.frm01.hProvince.value;
				document.frm01.hZip02.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode02.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode02.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode02.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode02.value = document.frm01.hZipCode.value;		
				document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax02.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress02.value = "";
				document.frm01.hTumbon02.value = "";
				document.frm01.hAmphur02.value = "";
				document.frm01.hProvince02.value = "";
				document.frm01.hZip02.value = "";		
				document.frm01.hTumbonCode02.value = "";
				document.frm01.hAmphurCode02.value = "";
				document.frm01.hProvinceCode02.value = "";
				document.frm01.hZipCode02.value = "";		
				document.frm01.hTel02.value = "";		
				document.frm01.hFax02.value = "";		
			}
			
		}
   		if(val ==3){
			if(document.frm01.hCustomerSamePlace03.checked == true){
				document.frm01.hAddress03.value = document.frm01.hAddress.value;
				document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
				document.frm01.hProvince03.value = document.frm01.hProvince.value;
				document.frm01.hZip03.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode03.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode03.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode03.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode03.value = document.frm01.hZipCode.value;		
				document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax03.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress03.value = "";
				document.frm01.hTumbon03.value = "";
				document.frm01.hAmphur03.value = "";
				document.frm01.hProvince03.value = "";
				document.frm01.hZip03.value = "";		
				document.frm01.hTumbonCode03.value = "";
				document.frm01.hAmphurCode03.value = "";
				document.frm01.hProvinceCode03.value = "";
				document.frm01.hZipCode03.value = "";		
				document.frm01.hTel03.value = "";		
				document.frm01.hFax03.value = "";		
			}
			
		}
   
   }
	

	<?}?>
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList01 = new CarModelList();
			$objCarModelList01->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList01->setPageSize(0);
			$objCarModelList01->setSortDefault(" title ASC");
			$objCarModelList01->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList01->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList01 = new CarModelList();
			$objCarModelList01->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList01->setPageSize(0);
			$objCarModelList01->setSortDefault(" title ASC");
			$objCarModelList01->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList01->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {

			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList01 = new CarSeriesList();
			$objCarSeriesList01->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList01->setPageSize(0);
			$objCarSeriesList01->setSortDefault(" title ASC");
			$objCarSeriesList01->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList01->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList01 = new CarSeriesList();
			$objCarSeriesList01->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList01->setPageSize(0);
			$objCarSeriesList01->setSortDefault(" title ASC");
			$objCarSeriesList01->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList01->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

function get_car_price(val){
	<?forEach($objCarSeriesList->getItemList() as $objItem) {?>
	if(val == <?=$objItem->getCarSeriesId();?>){
		document.frm01.hCarPrice.value="<?=$objItem->getPrice()?>";
	}
	<?}?>
}


</script>
<form name="frm01" action="acardUpdate.php" method="POST" onsubmit="return check_submit();"    onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hCustomerId" value="<?=$hCustomerId?>">
	  <input type="hidden" name="hTypeId" value="<?=$objCustomer->getTypeId();?>">
  	  <input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">	  
	  <input type="hidden" name="hInsureId" value="<?=$objInsureOld->get_insure_id();?>">
	  <input type="hidden" name="hDateVerify" value="<?=$objInsureCar->get_date_verify();?>">	  
	  <input type="hidden" name="hLink" value="<?=$hLink;?>">
	  <input type="hidden" name="hLock" value="<?=$hLock;?>">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">���ö�����</td>
</tr>
</table>
<?
$objOrder = new Order();
if($objInsureCar->get_order_id() > 0 ){
	
	$objOrder->setOrderId($objInsureCar->get_order_id());
	$objOrder->load();
}
?>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td><strong>Sale</strong></td>
			<td align="center">:</td>
			<td  class="i_background04" ><?=$objMemberList->printSelect("hSaleId",$objCustomer->getInsureMember(),"��س����͡");?></td>
			<td></td>
			<td align="center"></td>			
			<td  class="i_background04" >
			
			</td>
			
			<td></td>
			<td align="center"></td>			
			<td  class="i_background04" ></td>
		</tr>		
		<tr>
			<td><strong>������</strong></td>
			<td align="center">:</td>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType",$objInsureCar->get_car_type(),"����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td><strong>���</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel",$objInsureCar->get_car_model_id(),"����к�","populate02(document.frm01,document.frm01.hCarModel.options[document.frm01.hCarModel.selectedIndex].value,document.frm01.hCarSeries)");?>
			</td>
			
			<td><strong>Ẻ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?$objCarSeriesList->printSelectScript("hCarSeries",$objInsureCar->get_car_series_id(),"����к�","get_car_price(this.value);");?></td>
		</tr>
			<?if($strMode=="Update"){?>
			<script>
				populate01(document.frm01,<?=$objInsureCar->get_car_type()?>,document.frm01.hCarModel);
				document.frm01.hCarModel.value=<?=$objInsureCar->get_car_model_id()?>;
			</script>			
			<script>
				populate02(document.frm01,<?=$objInsureCar->get_car_model_id()?>,document.frm01.hCarSeries);
				document.frm01.hCarSeries.value=<?=$objInsureCar->get_car_series_id()?>;
			</script>		
			<?}?>
		<tr>
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><?=$objCarColorList->printSelect("hCarColor",$objInsureCar->get_color(),"����к�");?></td>
			<td><strong>����¹</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hCarCode" size="10" maxlength="7"  value="<?=$objInsureCar->get_code()?>"></td>	
			<td><strong>��</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" >
			<select name="hRegisterYear">
				<option value="0000" >��س����͡
			<?for($i=date("Y");$i> (date("Y")-25);$i--){?>
				<option value="<?=$i?>" <?if($objInsureCar->get_register_year() == $i) echo "selected";?>><?=$i?>
			<?}?>
			</select>			
			</td>
		</tr>
		<tr>
			<td><strong>�Ţ����ͧ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hEngineNumber" size="15"   value="<?=$objInsureCar->get_engine_number()?>"></td>
			<td><strong>�Ţ�ѧ</strong></td>
			<td align="center">:</td>			
			<td  class="i_background04" ><input type="text" name="hCarNumber" size="25"   value="<?=$objInsureCar->get_car_number()?>"></td>	
			<td><strong>�Ҥ�ö</strong></td>
			<td align="center">:</td>			
			<?if($objInsureCar->get_price() == 0) $objInsureCar->set_price($objOrder->getOrderPrice());?>
			<td  class="i_background04" ><input type="text" name="hCarPrice" size="20"   value="<?=$objInsureCar->get_price()?>"></td>	
		</tr>		

		</table>
	</td>
</tr>
</table>
<?if($objInsureCar->get_order_id() > 0 ){
	$objOrder = new Order();
	$objOrder->setOrderId($objInsureCar->get_order_id());
	$objOrder->load();
?>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">��������´����¹��л�Сѹ��¨ҡ CCARD</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
<table width="100%">
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  colspan="2">
	<input type="radio" disabled  name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 ) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio"  disabled  name="hBuyType" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��&nbsp;&nbsp;
	<input type="text"  disabled  name="hBuyTime" size="5"  value="<?if($objOrder->getBuyTime() > 0) echo $objOrder->getBuyTime();?>"> �Ǵ&nbsp;&nbsp;
	�Ǵ�� <input type="text"  disabled  onblur="sum_all()" onfocus="sum_all()" name="hBuyPrice" size="15"  value="<?if($objOrder->getBuyPrice() > 0) echo $objOrder->getBuyPrice();?>">&nbsp;&nbsp;
	�͡���� <input type="text" disabled  name="hBuyFee" size="5"  value="<?if($objOrder->getBuyFee() > 0) echo $objOrder->getBuyFee();?>"> %  &nbsp;&nbsp;
	�.�ṹ�� (���.) <?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?>
	</td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   valign="top" >���»�Сѹ (1)</td>
	<td >
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>��Сѹ����Ҩҡ</strong> </td>
			<td colspan="3">
				<input type="Radio"  disabled onclick="checkInsure();sum_all();" name="hInsureFrom" value="1" <?if($objOrder->getInsureFrom() ==1) echo "checked"?>>���
				<label id="insure" <?if($objOrder->getInsureFrom() !=1) echo "style='display:none'"?>>�� <input type="text" name="hInsureFromDetail" size="30"  value="<?=$objOrder->getInsureFromDetail()?>"></label>
				<input type="Radio"  disabled  onclick="checkInsure();sum_all();"  name="hInsureFrom" value="2" <?if($objOrder->getInsureFrom() ==2) echo "checked"?>>�� TMT
				<input type="Radio"  disabled  onclick="checkInsure();sum_all();"  name="hInsureFrom" value="3" <?if($objOrder->getInsureFrom() ==3) echo "checked"?>>�� BUZZ
				
			</td>			
		</tr>		
		<tr>
			<td class="i_background03"><strong>����ѷ��Сѹ���</strong> </td>
			<td colspan="3">
				<?$objInsureCompanyList->printSelect("hInsureCompany",$objOrder->getInsureCompany(),"��س����͡");?>
			</td>	


		</tr>
		<tr>

			<td class="i_background03"><strong>������ͧ����ѹ���</strong></td>
			<td colspan="3" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" disabled  maxlength="2"   name=Day06 value="<?=$Day06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" disabled  maxlength="2"  name=Month06 value="<?=$Month06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" disabled  maxlength="4" onblur="checkYear(this,this.value);"  name=Year06 value="<?=$Year06?>"></td>
					<td>&nbsp;</td>
					<td></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>������»�Сѹ���</strong></td>
			<td ><input type="text" disabled  onblur="sum_all()" onfocus="sum_all()" name="hInsurePrice" size="10"  value="<?if($objOrder->getInsurePrice() > 0) echo $objOrder->getInsurePrice();?>"> �ҷ</td>
			<td class="i_background03"><strong>������</strong> </td>
			<td >
				<select  disabled  name="hInsureType">
					<option value="1" <?if($objOrder->getInsureType() ==1) echo "selected"?>>��Сѹ��ª�� 1
					<option value="2" <?if($objOrder->getInsureType() ==2) echo "selected"?>>��Сѹ��ª�� 2
					<option value="3" <?if($objOrder->getInsureType() ==3) echo "selected"?>>��Сѹ��ª�� 3
					<option value="4" <?if($objOrder->getInsureType() ==4) echo "selected"?>>GOA
				</select>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>��Сѹ�շ��</strong>   </td>
			<td ><input disabled  type="text" name="hInsureYear" size="10"  value="<?if($objOrder->getInsureYear() > 0) echo $objOrder->getInsureYear();?>"> </td>
			<td class="i_background03"><strong>�ع��Сѹ</strong>   </td>
			<td ><input disabled  type="text" name="hInsureBudget" size="10"  value="<?if($objOrder->getInsureBudget() > 0) echo $objOrder->getInsureBudget();?>"> �ҷ</td>
		</tr>
		<tr>			
			<td valign="top" class="i_background03"><strong>�١��Ҩ�������</strong>   </td>
			<td valign="top" ><input disabled  type="text"  onblur="sum_all()" onfocus="sum_all()"  name="hInsureCustomerPay" size="10"  value="<?if($objOrder->getInsureCustomerPay() > 0) echo $objOrder->getInsureCustomerPay();?>"> �ҷ </td>
			<td valign="top" class="i_background03"><strong>�����˵�</strong></td>
			<td ><textarea disabled  name="hInsureRemark" cols="30" rows="3"><?=$objOrder->getInsureRemark()?></textarea></td>
		</tr>	
		</table>	
	
	</td>
	
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >���� �.�.�.</td>
	<td >����ѷ&nbsp;<?$objPrbCompanyList->printSelect("hPrbCompany",$objOrder->getPrbCompany(),"��س����͡");?>		</td>
	
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >����¹</td>
	<td ><input type="radio" disabled  name="hRegistryPerson" value=1 <?if($objOrder->getRegistryPerson()==1)  echo "checked"?>> �ؤ�� &nbsp;&nbsp;<input type="radio" disabled  name="hRegistryPerson" value=2 <?if($objOrder->getRegistryPerson()==2)  echo "checked"?>> �ԵԺؤ��</td>
	
</tr>
</table>
<table>
<?
$date = $objOrder->getBlackCodeDate();
$arrDate = explode("-",$date);
$Day = $arrDate[2];
$Month = $arrDate[1];
$Year = $arrDate[0];
?>
		<table width="100%" bgcolor="#FFB18C">
		<tr>
			<td colspan=2>����¹���´�</td>
			<td colspan=2><input type="text" disabled  name="hBlackCodeNumber" size="10"  value="<?=$objOrder->getBlackCodeNumber()?>"></td>
			<td colspan=2>�ѹ��訴����¹�ҡ����</td>
			<td colspan=2>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT disabled  align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT  disabled  align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT  disabled  align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>		
					<td></td>			
				</tr>
				</table>
			</td>
			<td >������ջշ��</td>
			<td><input type="text" disabled  name="hRegistryTaxYearNumber" size="5"  value="<?=$objOrder->getRegistryTaxYearNumber()?>"></td>
			<td >������ջ�Шӻ�</td>			
			<td><input type="text" disabled  name="hRegistryTaxYear" size="10"  value="<?=$objOrder->getRegistryTaxYear()?>"> �ҷ</td>
		</tr>

</table>
	
	
	</td>
</tr>
</table>	



<?}?>
<br>
	  
	  
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">
	<table width="100%">
	<tr>
		<td width="1"><img src="../images/console05.gif" alt="" width="12" height="12" border="0"></td>
		<td><strong>�������١���</strong></td>
		<td align="right"></td>
	</tr>
	</table>	
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>���觷���Ңͧ�١���</strong> </td>
			<td>
				<?$objCustomerGroupList->printSelect("hGroupId",$objCustomer->getGroupId(),"��س����͡");?> <input type="checkbox" name="hFollow" value=1 <?if($objCustomer->getFollowUp() ==1) echo "checked"?>> ���١��ҵԴ���
			</td>
			<td class="i_background03"><strong>�ѹ�����������</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong> </td>
			<td>				
				<input type="hidden" size="3" name="hEventId"  value="<?=$objCustomer->getEventId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hEventId.value != '' ) frm01.hSale.focus();if(event.keyCode !=13 ) frm01.hEventId.value='';"   name="hEvent"  size=40 value="<?=$objEvent->getTitle()?>">
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td valign="top" >				
				<input type="hidden" name="hSaleId"  value="<?=$objCustomer->getSaleId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hSaleId.value='';"   name="hSale"  size=40 value="<?=$objMember->getFirstname()."  ".$objMember->getLastname()?>">
			</td>
		</tr>
		<tr>			
			<td width="150" class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td width="175"><input type="text" name="hIDCard" size="30" maxlength="13"  value="<?=$objCustomer->getIDCard();?>"></td>
			<td width="120"class="i_background03"><strong>�ô</strong></td>
			<td width="205">
				<?$objCustomerGradeList->printSelect("hGradeId",$objCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>

		<tr>
			<td class="i_background03" valign="top"><strong>����-���ʡ��</strong> </td>
			<td valign="top">
		<?if($hLock == 1){?>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>					
					<INPUT type="hidden"  name="hCustomerName"  size=35 value="<?=$name?>"><INPUT disabled onKeyDown="if(event.keyCode==13) frm01.hCustomerTitleId.focus();"    size=35 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
					<?if($strMode == "Update"){?>
					<td valign="top"><input type="button"  class="button" value="��ӫ�͹" onclick="window.location='acardDuplicateConfirm.php?hId=<?=$objCustomer->getCustomerId()?>'"></td>
					<?}?>
				</tr>
				</table>	
		<?}else{?>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}?>					
					<INPUT onKeyDown="if(event.keyCode==13) frm01.hCustomerTitleId.focus();"   name="hCustomerName"  size=35 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
					<?if($strMode == "Update"){?>
					<td valign="top"><input type="button"  class="button" value="��ӫ�͹" onclick="window.location='acardDuplicateConfirm.php?hId=<?=$objCustomer->getCustomerId()?>'"></td>
					<?}?>
				</tr>
				</table>					
			<?}?>
				</td>
			<td class="i_background03" valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͼ��Դ���</strong> </td>
			<td valign="top"><input type="text" name="hContactName" size="50" maxlength="50"  value="<?=$objCustomer->getContactName();?>"></td>
			<td class="i_background03" valign="top"></td>
			<td  valign="top">
			</td>
		</tr>
		</table>
		<?if($objCustomer->getTypeId() == 'B' OR $objCustomer->getTypeId() == 'C' ){?>
		<table width="98%" cellpadding="0" cellspacing="0" align="center">
		<tr><td align="right">		
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>				
		</td>
		</tr>
		<tr>
		<td  class="i_background">
		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ 1</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ 2</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress1" maxlength="50" size="50"  value="<?=$objCustomer->getAddress1();?>"></td>
		</tr>		
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="40"  value="<?=$objCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ���ҹ</strong> </td>
			<td valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"></td>
			<td class="i_background03" valign="top"><strong>��Ͷ��</strong></td>
			<td>
			1. <input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"><br>
			2. <input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"><br>
			3. <input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"><br>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td>
				<table>
				<tr>
					<td valign="top">���ꡡóը����µա�Ѻ</td>
					<td><input type="checkbox" name="hMailback" value="1" <?if($objCustomer->getMailback() == 1) echo "checked";?>></td>
				</tr>
				<tr>
					<td valign="top">�ѹ���</td>
					<td>
				  	<table cellspacing="0" cellpadding="0">
					<tr>
						<td><INPUT align="middle" size="2" maxlength="2"   name=Day02 value="<?=$Day02?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
						<td>&nbsp;</td>
						<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false"></td>		
					</tr>
					</table>					
					</td>
				</tr>
				<tr>
					<td valign="top">���˵�</td>
					<td><textarea rows="3" cols="30" name="hMailbackRemark"><?=$objCustomer->getMailbackRemark()?></textarea></td>
				</tr>
				</table>					
			
			</td>
		</tr>	
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ 1</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress01" maxlength="50" size="50"  value="<?=$objCustomer->getAddress01();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace01" onclick="sameplace(1)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ 2</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress011" maxlength="50" size="50"  value="<?=$objCustomer->getAddress011();?>"></td>
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode01"  value="<?=$objCustomer->getTumbonCode01();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode01.value != '' ) frm01.hAmphur01.focus();if(event.keyCode !=13 ) frm01.hTumbonCode01.value='';" name="hTumbon01" size="40"  value="<?=$objCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode01"  value="<?=$objCustomer->getAmphurCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode01.value != '' ) frm01.hProvince01.focus();if(event.keyCode !=13 ) frm01.hAmphurCode01.value='';" name="hAmphur01" size="30"  value="<?=$objCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode01"  value="<?=$objCustomer->getProvinceCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode01.value != '' ) frm01.hZip01.focus();if(event.keyCode !=13 ) frm01.hProvinceCode01.value='';" name="hProvince01" size="30"  value="<?=$objCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode01"  value="<?=$objCustomer->getZip01();?>"><input type="text" name="hZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode01.value != '' ) frm01.hTel01.focus();if(event.keyCode !=13 ) frm01.hZipCode01.value='';" size="30"  value="<?=$objCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel01" size="30"  value="<?=$objCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax01" size="30"  value="<?=$objCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>����ѷ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hCompany" maxlength="100" size="50"  value="<?=$objCustomer->getCompany();?>">
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ 1</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress02" maxlength="50" size="50"  value="<?=$objCustomer->getAddress02();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace02" onclick="sameplace(2)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ 2</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress021" maxlength="50" size="50"  value="<?=$objCustomer->getAddress021();?>"></td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="text" size="3" readonly name="hTumbonCode02"  value="<?=$objCustomer->getTumbonCode02();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode02.value != '' ) frm01.hAmphur02.focus();if(event.keyCode !=13 ) frm01.hTumbonCode02.value='';" name="hTumbon02" size="40"  value="<?=$objCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="text" size="3" readonly name="hAmphurCode02"  value="<?=$objCustomer->getAmphurCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode02.value != '' ) frm01.hProvince02.focus();if(event.keyCode !=13 ) frm01.hAmphurCode02.value='';" name="hAmphur02" size="30"  value="<?=$objCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="text" size="3" readonly name="hProvinceCode02"  value="<?=$objCustomer->getProvinceCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode02.value != '' ) frm01.hZip02.focus();if(event.keyCode !=13 ) frm01.hProvinceCode02.value='';" name="hProvince02" size="30"  value="<?=$objCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="text" size="3" readonly name="hZipCode02"  value="<?=$objCustomer->getZip02();?>"><input type="text" name="hZip02"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode02.value != '' ) frm01.hTel02.focus();if(event.keyCode !=13 ) frm01.hZipCode02.value='';" size="30"  value="<?=$objCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel02" size="30"  value="<?=$objCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax02" size="30"  value="<?=$objCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>���ͼ���Ѻ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hName03" maxlength="50" size="50"  value="<?=$objCustomer->getName03();?>">&nbsp;&nbsp;<span class=error>�����ͼ���Ѻ�óյ�ҧ�ҡ������Ңͧö</span>
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡��� 1</strong></td>
			<td colspan="3">
			<input type="text"  maxlength="50"  name="hAddress03" maxlength="50" size="50"  value="<?=$objCustomer->getAddress03();?>">&nbsp;&nbsp;<input type="checkbox" name="hCustomerSamePlace03" onclick="sameplace(3)"> ������ǡѺ�������Ѩ�غѹ
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡��� 2</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress031" maxlength="50" size="50"  value="<?=$objCustomer->getAddress031();?>"></td>
		</tr>

		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="text" size="3" readonly name="hTumbonCode03"  value="<?=$objCustomer->getTumbonCode03();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode03.value != '' ) frm01.hAmphur03.focus();if(event.keyCode !=13 ) frm01.hTumbonCode03.value='';" name="hTumbon03" size="40"  value="<?=$objCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="text" size="3" readonly name="hAmphurCode03"  value="<?=$objCustomer->getAmphurCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode03.value != '' ) frm01.hProvince03.focus();if(event.keyCode !=13 ) frm01.hAmphurCode03.value='';" name="hAmphur03" size="30"  value="<?=$objCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="text" size="3" readonly name="hProvinceCode03"  value="<?=$objCustomer->getProvinceCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode03.value != '' ) frm01.hZip03.focus();if(event.keyCode !=13 ) frm01.hProvinceCode03.value='';" name="hProvince03" size="30"  value="<?=$objCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="text" size="3" readonly name="hZipCode03"  value="<?=$objCustomer->getZip03();?>"><input type="text" name="hZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode03.value != '' ) frm01.hTel03.focus();if(event.keyCode !=13 ) frm01.hZipCode03.value='';" size="30"  value="<?=$objCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hTel03" size="30"  value="<?=$objCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hFax03" size="30"  value="<?=$objCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>
		
<br>
	</td>
</tr>
</table>		
		
		<?}else{?>
		<table width="100%">
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ 1</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress" maxlength="50" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ 2</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hAddress1" maxlength="50" size="50"  value="<?=$objCustomer->getAddress1();?>"></td>
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';" name="hTumbon" size="40"  value="<?=$objCustomer->getTumbon();?>"> <a href="areaTumbonUpdate.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';" name="hAmphur" size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';" name="hProvince" size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hHomeTel.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';" size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ���ҹ</strong> </td>
			<td class="small" valign="top"><input type="text" name="hHomeTel" size="30"  value="<?=$objCustomer->getHomeTel();?>"><br>��. 02-222-3333</td>
			<td class="i_background03" valign="top"><strong>��Ͷ��</strong></td>
			<td class="small">
			1. <input type="text" name="hMobile_1" size="30"   onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" value="<?=$arrMobile[0];?>"><br>
			2. <input type="text" name="hMobile_2" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[1];?>"><br>
			3. <input type="text" name="hMobile_3" size="30"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"  value="<?=$arrMobile[2];?>"><br>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���Ѿ����ӧҹ</strong></td>
			<td class="small"><input type="text" name="hOfficeTel" size="30"  value="<?=$objCustomer->getOfficeTel();?>"><br>��. 02-222-3333</td>
			<td class="i_background03" valign="top"><strong>ῡ��</strong></td>
			<td class="small"><input type="text" name="hFax" size="30"  value="<?=$objCustomer->getFax();?>"><br>��. 02-222-3333</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hEmail" size="30"  value="<?=$objCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		<tr>
			<td class="i_background03" valign="top"><strong>�������������ó�</strong></td>
			<td valign="top"><input type="checkbox" name="hIncomplete" value="1" <?if($objCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03" valign="top" rowspan="2"><strong>�����µա�Ѻ</strong></td>
			<td rowspan="2">
				<table>
				<tr>
					<td valign="top">���ꡡóը����µա�Ѻ</td>
					<td><input type="checkbox" name="hMailback" value="1" <?if($objCustomer->getMailback() == 1) echo "checked";?>></td>
				</tr>
				<tr>
					<td valign="top">�ѹ���</td>
					<td>
				  	<table cellspacing="0" cellpadding="0">
					<tr>
						<td><INPUT align="middle" size="2" maxlength="2"   name=Day02 value="<?=$Day02?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
						<td>-</td>
						<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
						<td>&nbsp;</td>
						<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false"></td>		
					</tr>
					</table>					
					</td>
				</tr>
				<tr>
					<td valign="top">���˵�</td>
					<td><textarea rows="3" cols="50" name="hMailbackRemark"><?=$objCustomer->getMailbackRemark()?></textarea></td>
				</tr>
				</table>		
			
			</td>
		</tr>			
		<tr>
			<td class="i_background03" valign="top"><strong>�����˵��������</strong></td>
			<td  valign="top"><textarea rows="3" cols="50" name="hRemark"><?=$objCustomer->getRemark()?></textarea></td>

		</tr>			
		<tr>
			<td class="i_background03"><?if($objCustomer->getVerifyAddress() > 0) {?><strong>Check Address</strong><?}?></td>
			<td><?if($objCustomer->getVerifyAddress() > 0) {?><input type="radio" name="hVerifyAddress" value="1" <?if($objCustomer->getVerifyAddress() == 1) echo "checked";?>> ����ó� &nbsp;&nbsp;<input type="radio" name="hVerifyAddress" value="2" <?if($objCustomer->getVerifyAddress() == 2) echo "checked";?>> �������ó� <?}?></td>
			<td class="i_background03"><?if($objCustomer->getVerifyPhone() > 0) {?><strong>Check Phone</strong><?}?></td>
			<td><?if($objCustomer->getVerifyPhone() > 0) {?><input type="radio" name="hVerifyPhone" value="1" <?if($objCustomer->getVerifyPhone() == 1) echo "checked";?>> ����ó� &nbsp;&nbsp;<input type="radio" name="hVerifyPhone" value="2" <?if($objCustomer->getVerifyPhone() == 2) echo "checked";?>> �������ó� <?}?></td>
		</tr>			
		</table>
		
<br>
	</td>
</tr>
</table>		
		<?}?>

<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����Ż�Сѹ������</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>	
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td></td>			
			<td ></td>
			<td ></td>
			<td class="error" align="right"><strong>�ѹ��������ͧ</strong>&nbsp;</td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayProtect value="<?=$DayProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthProtect value="<?=$MonthProtect?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearProtect" value="<?=$YearProtect?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearProtect,DayProtect, MonthProtect, YearProtect,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td></td>			
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�����Ţ��������</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr>
			<td class="listTitle"><strong>�Ҥ��Ѥ��</strong></td>			
			<td class="listDetail"><?$objBrokerList->printSelectScript("hBrokerId",$objInsureOld->get_insure_broker_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelectScript("hInsureCompanyId",$objInsureOld->get_insure_company_id(),"����к�");?>	</td>
			<td class="listDetail"><input type="text" name="hPrbNumber"  size="20" value="<?=$objInsureOld->get_prb_number();?>"></td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayStartPrb value="<?=$DayStartPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthStartPrb value="<?=$MonthStartPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearStartPrb" value="<?=$YearStartPrb?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearStartPrb,DayStartPrb, MonthStartPrb, YearStartPrb,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayEndPrb value="<?=$DayEndPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthEndPrb value="<?=$MonthEndPrb?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearEndPrb" value="<?=$YearEndPrb?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearEndPrb,DayEndPrb, MonthEndPrb, YearEndPrb,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>		
		</tr>
		<tr>
			<td class="listTitle">
			<strong>�Ҥ�ѧ�Ѻ</strong><br>			
			</td>
			<td class="listDetail"><?$objBrokerList->printSelectScript("hBrokerKomId",$objInsureOld->get_insure_broker_kom_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hStockKomId",$objInsureOld->get_stock_kom_id(),"����к�");?>		</td>
			<td class="listDetail"><input type="text" name="hKomNumber"  size="20" value="<?=$objInsureOld->get_kom_number();?>">	</td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayStartKom value="<?=$DayStartKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthStartKom value="<?=$MonthStartKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearStartKom" value="<?=$YearStartKom?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearStartKom,DayStartKom, MonthStartKom, YearStartKom,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayEndKom value="<?=$DayEndKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthEndKom value="<?=$MonthEndKom?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearEndKom" value="<?=$YearEndKom?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearEndKom,DayEndKom, MonthEndKom, YearEndKom,popCal);return false"></td>		
				</tr>
				</table>						
			
			</td>		
		</tr>
		</table>

	<br>

</table>		
<table width="100%" class="search">
<tr>
	<td width="50%" align="center" class="listTitle">�Ҥ��Ѥ��</td>
	<td width="50%" align="center" class="listtitle">�Ҥ�ѧ�Ѻ</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td colspan="2">�ع��Сѹ</td>			
			<td align="right"><input type="text" name="hFund" style="text-align=right;"   onblur=""  size="15" value="<?=$objInsureOld->get_fund();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�����ط��................................................1)</td>
			<td align="right"><input type="text" name="hBeasuti" style="text-align=right;"  onblur="sumBea();sumAll();"  size="15" value="<?=$objInsureOld->get_beasuti();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�ҡ�.....................................................2)</td>
			<td align="right"><input type="text" name="hArgon" style="text-align=right;"  onblur="sumBea();sumAll();"   size="15" value="<?=$objInsureOld->get_argon();?>"></td>
		</tr>
		<tr>
			<td colspan="2">Vat.......................................................3)</td>
			<td align="right"><input type="text" name="hPasi" style="text-align=right;"  onblur="sumBea();sumAll();"  size="15" value="<?=($objInsureOld->get_beasuti()+$objInsureOld->get_argon())*0.07?>"></td>
		</tr>
		<tr>
			<td colspan="2">���»�Сѹ������</td>
			<td align="right"><input type="text" name="hTotal01" style="text-align=right;"  disabled size="15"   onblur=""  value="<?=$objInsureOld->get_beasuti()+$objInsureOld->get_argon()+$objInsureOld->get_pasi();?>"></td>
		</tr>
		<tr>
			<td colspan="2">�ѡ���� � ������</td>
			<td align="right"><input type="text" name="hTotal05" style="text-align=right;"  disabled size="15"   onblur=""  value="<?=($objInsureOld->get_beasuti()+$objInsureOld->get_argon())*0.01?>"></td>
		</tr>		
		<tr>
			<td >������������</td>
			<td>
				<input type="radio" value="1" name="hNiti"  onclick="sumNiti01();sumAll();"  <?if($objInsureOld->get_niti()=="1") echo "checked"?> >�ؤ�Ÿ�����&nbsp;&nbsp;
				<input type="radio" value="2" name="hNiti" onclick="sumNiti();sumAll();" <?if($objInsureOld->get_niti()=="2") echo "checked"?>>�ԵԺؤ�� �ѡ���� 
				<input type="radio" value="3" name="hNiti" onclick="sumNiti01();sumAll();" <?if($objInsureOld->get_niti()=="3") echo "checked"?>>�ԵԺؤ�� ����ѡ���� 
				</td>
			<td align="right"><input type="text" name="hTotal02"  style="text-align=right;"  onblur=""  disabled  size="15" value="<?=($objInsureOld->get_beasuti()+$objInsureOld->get_argon()+$objInsureOld->get_pasi())*0.01;?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">���</td>
			<td align="right" class="listDetail02"><input type="text" style="text-align=right;"  disabled name="hTotal03"  onblur=""   size="15" value=""></td>
		</tr>

		<tr>
			<td rowspan="2" valign="top">��ǹŴ</td>
			<td>
					<input type="radio" value="1" onclick="checkDiscount01();sumAll();" name="hDiscountType" <?if($objInsureOld->get_discount_type()=="1") echo "checked"?>>��ǹŴ % &nbsp;<input type="text" name="hDiscountPercent" onBlur="checkDiscount01();sumAll();"  size="10" value="<?=$objInsureOld->get_discount_percent()?>">
			</td>
			<td valign="top" align="right"><input type="text" style="text-align=right;"  onblur=""  disabled name="hDiscountPercentPrice"  size="15" value=""></td>
		</tr>
		<tr>			
			<td><input type="radio" value="2"  onclick="checkDiscount02();sumAll();"  name="hDiscountType" <?if($objInsureOld->get_discount_type()=="2") echo "checked"?>>��ǹŴ���� <input type="text" name="hDiscountOtherRemark"  size="10" value="<?=$objInsureOld->get_discount_other_remark()?>"></td>
			<td valign="top" align="right"><input type="text" style="text-align=right;" onblur="sumAll();"   onblur=""  name="hDiscountOtherPrice"  size="15" value="<?=$objInsureOld->get_discount_other_price();?>"></td>
		</tr>
		<tr>
			

		</tr>
		<tr>
			<td >��� �ú.</td>
			<td>
					<input type="radio" value="3"  onclick="checkDiscount03();sumAll();"  name="hDiscountType" <?if($objInsureOld->get_discount_type()=="3") echo "checked"?>>���ú.
			</td>
			<td align="right"><input type="text" name="hPrbPrice01" style="text-align=right;"  onblur=""  disabled size="15" value="<?=$objInsureOld->get_prb_price();?>"></td>
		</tr>
		<tr>
			<td colspan="2" class="listDetail02">�������ͧ���з�����</td>
			<td align="right" class="listDetail02"><input type="text" name="hTotal"  style="text-align=right;"  onblur=""  size="15" value="<?=$objInsureOld->get_price();?>"></td>
		</tr>
		<tr>
			<td>��������Сѹ���</td>
			<td>
				<select name="hInsureType">
					<option value="GOA" <?if($objInsureOld->get_insure_type()=="GOA") echo "selected"?>>GOA
					<option value="� 1" <?if($objInsureOld->get_insure_type()=="� 1") echo "selected"?>>� 1
					<option value="� 2" <?if($objInsureOld->get_insure_type()=="� 2") echo "selected"?>>� 2
					<option value="� 3" <?if($objInsureOld->get_insure_type()=="� 3") echo "selected"?>>� 3
					<option value="����" <?if($objInsureOld->get_insure_type()=="����") echo "selected"?>>����
				</select><input type="text" name="hInsureTypeRemark"  size="10" value="<?=$objInsureOld->get_insure_type_remark();?>">
			</td>
		</tr>		
		<tr>
			<td>�ͧ��</td>
			<td colspan="2"><input type="text" name="hFreeRemark"  size="30" value="<?=$objInsureOld->get_free_remark();?>"></td>
		</tr>
		<tr>
			<td>��������ë���</td>
			<td><input type="radio" value="������ҧ" name="hRepair"  <?if($objInsureOld->get_repair()=="������ҧ") echo "checked"?>>������ҧ&nbsp;&nbsp;<input type="radio" value="�������" name="hRepair" <?if($objInsureOld->get_repair()=="�������") echo "checked"?>>�������</td>
		</tr>
		</table>

	
	
	
	</td>
	<td valign="top">
		<table>
		<tr>
			<td>������ö��Т�Ҵö¹��</td>
			<td>
				<select name="hInsureFeeId" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsureOld->get_insure_fee_id() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_title();?>
				<?}?>
				</select>
			
			</td>
		</tr>
		<tr>
			<td>�Ҥ� �ú.</td>
			<td><input type="text" name="hPrbPrice" style="text-align=right;"  onblur=""  size="10" value="<?=$objInsureOld->get_prb_price();?>"></td>
		</tr>
		<tr>
			<td>�շ���ͻ�Сѹ</td>
			<td>
				<select name="hCarStatus">
					<option value="�շ�� 1"  <?if($objInsureOld->get_car_status() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsureOld->get_car_status() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsureOld->get_car_status() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsureOld->get_car_status() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsureOld->get_car_status() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsureOld->get_car_status() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsureOld->get_car_status() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsureOld->get_car_status() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsureOld->get_car_status() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsureOld->get_car_status() == "�շ�� 10") echo "selected"?>>�շ�� 10
				</select>
			</td>
		</tr>
		<tr>
			<td>�ѹ����駧ҹ</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayReportDate value="<?=$DayReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthReportDate value="<?=$MonthReportDate?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearReportDate" value="<?=$YearReportDate?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearReportDate,DayReportDate, MonthReportDate, YearReportDate, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�ѹ����Ѻ��</td>
			<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayRecieveReport value="<?=$DayRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthRecieveReport value="<?=$MonthRecieveReport?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="YearRecieveReport" value="<?=$YearRecieveReport?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearRecieveReport,DayRecieveReport, MonthRecieveReport, YearRecieveReport, popCal);return false"></td>		
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td>�Ţ�Ѻ��</td>
			<td><input type="text" name="hAccNo"  size="10" value="<?=$objInsureOld->get_acc_no();?>"></td>
		</tr>
		<tr>
			<td valign="top">�����˵�</td>
			<td><textarea rows="3" cols="30" name="hRemark"><?=$objInsureOld->get_remark();?></textarea></td>
		</tr>
		</table>

	
	
	</td>
</tr>
</table>



<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�ê��Ф�һ�Сѹ��� ���з����� 
		<select name="hPaymentAmount"  onchange="checkDisplay(this.value)" >
			<option value="0">��س����͡
			<option value="1" <?if($objInsureOld->get_payment_amount() == "1") echo "selected";?>>1
			<option value="2" <?if($objInsureOld->get_payment_amount() == "2") echo "selected";?>>2
			<option value="3" <?if($objInsureOld->get_payment_amount() == "3") echo "selected";?>>3
			<option value="4" <?if($objInsureOld->get_payment_amount() == "4") echo "selected";?>>4
			<option value="5" <?if($objInsureOld->get_payment_amount() == "5") echo "selected";?>>5
			<option value="6" <?if($objInsureOld->get_payment_amount() == "6") echo "selected";?>>6
			<option value="7" <?if($objInsureOld->get_payment_amount() == "7") echo "selected";?>>7
			<option value="8" <?if($objInsureOld->get_payment_amount() == "8") echo "selected";?>>8
			<option value="9" <?if($objInsureOld->get_payment_amount() == "9") echo "selected";?>>9

		</select>	
	  �Ǵ
	  &nbsp;&nbsp;&nbsp;<strong>���͹䢡�â��</strong>
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_payment_condition() == "�������") echo "checked";?> value="�������"> �������
	  <input type="radio"  name="hPaymentCondition" <?if($objInsureOld->get_payment_condition() == "�觨���") echo "checked";?> value="�觨���"> �觨���
	  </td>
</tr>
</table>
<input type="hidden" name="hInsureItemId01" value="<?=$objInsureItem01->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId02" value="<?=$objInsureItem02->get_insure_item_id();?>">
<input type="hidden" name="hInsureItemId03" value="<?=$objInsureItem03->get_insure_item_id();?>">
<table width="100%" cellpadding="3" cellspacing="1" class="i_background02">
<tr id="pay00" style="display:none">
	<td class="listTitle" width="5%" align="center">�Ǵ���</td>
	<td class="listTitle" width="15%" align="center">�ѹ���Ѵ����</td>
	<td class="listTitle" width="15%" align="center">��������ê���</td>
	<td class="listTitle" width="10%" align="center">�ӹǹ�Թ</td>	
	<td class="listTitle" width="15%" align="center">��ê���</td>
	<td class="listTitle" width="15%" align="center">�Ţ��������</td>
	<td class="listTitle" width="35%" align="center">�����˵�</td>	
</tr>
<tr id="pay01" style="display:none">
	<td class="listDetail" align="center" align="center">1</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType01" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem01->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem01->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem01->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice01" onblur="checkSum();"  size="10" value="<?=$objInsureItem01->get_payment_price();?>"></td>	
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus01" value="1"  <?if($objInsureItem01->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus01" value="0" <?if($objInsureItem01->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber01"  size="10" value="<?=$objInsureItem01->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark01"  size="20" value="<?=$objInsureItem01->get_remark();?>">	</td>		
</tr>
<tr id="pay02" style="display:none">
	<td class="listDetail" align="center" align="center">2</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day02 value="<?=$Day02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType02" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem02->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem02->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem02->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice02" onblur="checkSum();"  size="10" value="<?=$objInsureItem02->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus02" value="1"  <?if($objInsureItem02->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus02" value="0" <?if($objInsureItem02->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber02"  size="10" value="<?=$objInsureItem02->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark02"  size="20" value="<?=$objInsureItem02->get_remark();?>">	</td>		
	
</tr>
<tr id="pay03" style="display:none">
	<td class="listDetail" align="center" align="center">3</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day03 value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month03 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year03 value="<?=$Year03?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year03,Day03, Month03, Year03,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType03" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem03->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem03->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem03->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice03" onblur="checkSum();"  size="10" value="<?=$objInsureItem03->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus03" value="1"  <?if($objInsureItem03->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus03" value="0" <?if($objInsureItem03->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber03"  size="10" value="<?=$objInsureItem03->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark03"  size="20" value="<?=$objInsureItem03->get_remark();?>">	</td>		
	
</tr>

<tr id="pay04" style="display:none">
	<td class="listDetail" align="center" align="center">4</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day04 value="<?=$Day04?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month04 value="<?=$Month04?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year04 value="<?=$Year04?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year04,Day04, Month04, Year04,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType04" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem04->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem04->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem04->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice04" onblur="checkSum();"  size="10" value="<?=$objInsureItem04->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus04" value="1"  <?if($objInsureItem04->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus04" value="0" <?if($objInsureItem04->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber04"  size="10" value="<?=$objInsureItem04->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark04"  size="20" value="<?=$objInsureItem04->get_remark();?>">	</td>		
	
</tr>

<tr id="pay05" style="display:none">
	<td class="listDetail" align="center" align="center">5</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day05 value="<?=$Day05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month05 value="<?=$Month05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year05 value="<?=$Year05?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year05,Day05, Month05, Year05,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType05" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem05->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem05->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem05->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice05" onblur="checkSum();"  size="10" value="<?=$objInsureItem05->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus05" value="1"  <?if($objInsureItem05->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus05" value="0" <?if($objInsureItem05->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber05"  size="10" value="<?=$objInsureItem05->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark05"  size="20" value="<?=$objInsureItem05->get_remark();?>">	</td>		
	
</tr>

<tr id="pay06" style="display:none">
	<td class="listDetail" align="center" align="center">6</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day06 value="<?=$Day06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month06 value="<?=$Month06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year06 value="<?=$Year06?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year06,Day06, Month06, Year06,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType06" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem06->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem06->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem06->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice06" onblur="checkSum();"  size="10" value="<?=$objInsureItem06->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus06" value="1"  <?if($objInsureItem06->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus06" value="0" <?if($objInsureItem06->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber06"  size="10" value="<?=$objInsureItem06->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark06"  size="20" value="<?=$objInsureItem06->get_remark();?>">	</td>		
	
</tr>

<tr id="pay07" style="display:none">
	<td class="listDetail" align="center" align="center">7</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day07 value="<?=$Day07?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month07 value="<?=$Month07?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year07 value="<?=$Year07?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year07,Day07, Month07, Year07,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType07" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem07->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem07->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem07->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice07" onblur="checkSum();"  size="10" value="<?=$objInsureItem07->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus07" value="1"  <?if($objInsureItem07->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus07" value="0" <?if($objInsureItem07->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber07"  size="10" value="<?=$objInsureItem07->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark07"  size="20" value="<?=$objInsureItem07->get_remark();?>">	</td>		
	
</tr>

<tr id="pay08" style="display:none">
	<td class="listDetail" align="center" align="center">8</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day08 value="<?=$Day08?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month08 value="<?=$Month08?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year08 value="<?=$Year08?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year08,Day08, Month08, Year08,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType08" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem08->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem08->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem08->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice08" onblur="checkSum();"  size="10" value="<?=$objInsureItem08->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus08" value="1"  <?if($objInsureItem08->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus08" value="0" <?if($objInsureItem08->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber08"  size="10" value="<?=$objInsureItem08->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark08"  size="20" value="<?=$objInsureItem08->get_remark();?>">	</td>		
	
</tr>

<tr id="pay09" style="display:none">
	<td class="listDetail" align="center" align="center">9</td>
	<td class="listDetail" align="center" align="center">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day09 value="<?=$Day09?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month09 value="<?=$Month09?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year09 value="<?=$Year09?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year09,Day09, Month09, Year09,popCal);return false"></td>		
				</tr>
				</table>	
	</td>
	<td class="listDetail"  align="center">
		<select name="hPaymentType09" >
			<option value="">��س����͡
			<option value="�Թʴ" <?if($objInsureItem09->get_payment_type() == "�Թʴ") echo "selected";?>>�Թʴ
			<option value="�ѵ��ôԵ" <?if($objInsureItem09->get_payment_type() == "�ѵ��ôԵ") echo "selected";?>>�ѵ��ôԵ
			<option value="pay in" <?if($objInsureItem09->get_payment_type() == "pay in") echo "selected";?>>pay in
		</select>
	</td>
	<td class="listDetail"  align="center"><input type="text" name="hPaymentPrice09" onblur="checkSum();"  size="10" value="<?=$objInsureItem09->get_payment_price();?>"></td>
	<td class="listDetail" align="center"><input type="radio" name="hPaymentStatus09" value="1"  <?if($objInsureItem09->get_payment_status() == "1") echo "checked";?>>��������<br><input type="radio" name="hPaymentStatus09" value="0" <?if($objInsureItem09->get_payment_status() == "0") echo "checked";?>>��ҧ����</td>
	<td class="listDetail" align="center"><input type="text" name="hPaymentNumber09"  size="10" value="<?=$objInsureItem09->get_payment_number();?>"></td>
	<td class="listDetail" align="center"><input type="text" name="hRemark09"  size="20" value="<?=$objInsureItem09->get_remark();?>">	</td>		
	
</tr>




<tr id="pay10" style="display:none">
	<td></td>
	<td></td>
	<td align="right">������Թ</td>
	<td align="center"><input type="text" name="hInsurePrice" onblur="checkSum();"  size="10" value="<?=$objInsureOld->get_price();?>"></td>
</tr>
</table>


<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<?if($objInsureCar->get_sale_id() == $sMemberId OR $objInsureCar->get_sale_id() == 0 ){?>
	<input type="button" name="hSubmit1" value="�ѹ�֡������" class="button" onclick="checkSubmit();"  >
	<?}?>
<?}else{?>
	<input type="button" name="hSubmit1" value="�ѹ�֡������" class="button" onclick="checkSubmit();" >
<?}?>
<?$hNewLink = str_replace("YYY", "&", $hLink);?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button"  class="button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='<?=$hNewLink?>'">			
	<br><br>
	</td>
</tr>
</table>




<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {
		return "acardAutoCustomer.php?hId=<?=$hId?>&q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hTumbon", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoAmphur.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoTumbon.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hEvent", function() {
		var str = this.text.value;	
		return "acardAutoEvent.php?q=" + this.text.value;		
	});		
		
	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "acardAutoSale.php?q=" + this.text.value;
		}
	});		
	
	<?if($objCustomer->getTypeId() == 'B' OR $objCustomer->getTypeId() == 'C' ){?>

		new CAPXOUS.AutoComplete("hTumbon01", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur01.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon01.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince01", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince01.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon02", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur02.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon02.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince02", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince02.php?q=" + this.text.value;
		}
	});		
	
	
	
	
	new CAPXOUS.AutoComplete("hTumbon03", function() {
		var str = this.text.value;	
		if( str.length > 2){
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});	
	
	new CAPXOUS.AutoComplete("hAmphur03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoAmphur03.php?q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hZip03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoTumbon03.php?q=" + this.text.value;
		}
	});			
	
	new CAPXOUS.AutoComplete("hProvince03", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "bcardAutoProvince03.php?q=" + this.text.value;
		}
	});			
	
	<?}?>
		
		
	function checkSubmit(){	
	
		if(document.frm01.hCustomerName.value == ""){
			alert("��س��кت����١���");
			document.frm01.hCustomerName.focus();
			return false;
		}
	
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س��кصӺ�");
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س��к������");
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س��кبѧ��Ѵ");
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س��к�������ɳ���");
			document.forms.frm01.hZip.focus();
			return false;
		} 			
		
		if ( (document.forms.frm01.hMobile_1.value=="" && document.forms.frm01.hMobile_2.value=="" && document.forms.frm01.hMobile_3.value=="" )    && document.forms.frm01.hHomeTel.value=="")
		{
			alert("��س��к��������Ѿ���ҹ ������Ͷ�� ���ҧ����ҧ˹��");
			return false;
		} 			
		
		if(document.frm01.hCarType.value ==0){
			alert("��س����͡������ö");
			document.frm01.hCarType.focus();
			return false;
		}	
	
		if(document.frm01.hCarModel.value ==0){
			alert("��س����͡���ö");
			document.frm01.hCarModel.focus();
			return false;
		}	
		
		if(document.frm01.hCarSeries.value ==0){
			alert("��س����͡Ẻö");
			document.frm01.hCarSeries.focus();
			return false;
		}				
	
		if(document.frm01.hCarCode.value ==0){
			alert("��س����͡����¹");
			document.frm01.hCarCode.focus();
			return false;
		}			
	
		if(document.frm01.hRegisterYear.value < 1 ){
			alert("��س����͡��");
			document.frm01.hRegisterYear.focus();
			return false;
		}			

		if(document.frm01.hCarNumber.value == ""){
			alert("��س��к��Ţ�ѧ");
			document.frm01.hCarNumber.focus();
			return false;
		}	
	
	
		if(document.frm01.hInsureCompanyId.value ==0){
			alert("��س����͡����ѷ��Сѹ");
			document.frm01.hInsureCompanyId.focus();
			return false;
		}
	
	
		if (document.forms.frm01.DayProtect.value=="" || document.forms.frm01.DayProtect.value=="00")
		{
			alert("��س��к��ѹ��������ͧ");
			document.forms.frm01.DayProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.DayProtect.value,1,31) == false) {
				document.forms.frm01.DayProtect.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.MonthProtect.value==""  || document.forms.frm01.MonthProtect.value=="00")
		{
			alert("��س��к���͹��������ͧ");
			document.forms.frm01.MonthProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.MonthProtect.value,1,12) == false){
				document.forms.frm01.MonthProtect.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.YearProtect.value==""  || document.forms.frm01.YearProtect.value=="0000")
		{
			alert("��س��кػշ�������ͧ");
			document.forms.frm01.YearProtect.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.YearProtect.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.YearProtect.focus();
				return false;
			}
		} 				
		
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
	
	}
	
</script>

<?
	include("h_footer.php")
?>

<script>
	sumAll();


	function checkDisplay(var_num){
		if(var_num ==0){
			document.getElementById("pay00").style.display = "none";
			document.getElementById("pay01").style.display = "none";
			document.getElementById("pay02").style.display = "none";
			document.getElementById("pay03").style.display = "none";								
			document.getElementById("pay04").style.display = "none";	
			document.getElementById("pay05").style.display = "none";	
			document.getElementById("pay06").style.display = "none";	
			document.getElementById("pay07").style.display = "none";	
			document.getElementById("pay08").style.display = "none";	
			document.getElementById("pay09").style.display = "none";
			document.getElementById("pay10").style.display = "none";
		}
		if(var_num ==1){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";						
			document.getElementById("pay10").style.display = "";			
		}
		if(var_num ==2){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay10").style.display = "";			
		}
		if(var_num ==3){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay10").style.display = "";			
		}
		if(var_num ==4){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";		
			document.getElementById("pay10").style.display = "";		
		}
		if(var_num ==5){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";			
			document.getElementById("pay05").style.display = "";	
			document.getElementById("pay10").style.display = "";	
		}
		if(var_num ==6){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";			
			document.getElementById("pay05").style.display = "";	
			document.getElementById("pay06").style.display = "";	
			document.getElementById("pay10").style.display = "";				
		}
		if(var_num ==7){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";		
			document.getElementById("pay05").style.display = "";
			document.getElementById("pay06").style.display = "";	
			document.getElementById("pay07").style.display = "";	
			document.getElementById("pay10").style.display = "";			
		}
		if(var_num ==8){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";			
			document.getElementById("pay05").style.display = "";	
			document.getElementById("pay06").style.display = "";	
			document.getElementById("pay07").style.display = "";	
			document.getElementById("pay08").style.display = "";	
			document.getElementById("pay10").style.display = "";	
		}
		if(var_num ==9){
			document.getElementById("pay00").style.display = "";
			document.getElementById("pay01").style.display = "";
			document.getElementById("pay02").style.display = "";
			document.getElementById("pay03").style.display = "";								
			document.getElementById("pay04").style.display = "";		
			document.getElementById("pay05").style.display = "";	
			document.getElementById("pay06").style.display = "";	
			document.getElementById("pay07").style.display = "";	
			document.getElementById("pay08").style.display = "";	
			document.getElementById("pay09").style.display = "";	
			document.getElementById("pay10").style.display = "";	
		}
		
		
	}
	
	function checkSum(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		if(document.frm01.hPaymentPrice01){
			num01 = document.frm01.hPaymentPrice01.value*1;
		}
		if(document.frm01.hPaymentPrice02){
			num02 = document.frm01.hPaymentPrice02.value*1;
		}		
		if(document.frm01.hPaymentPrice03){
			num03 = document.frm01.hPaymentPrice03.value*1;
		}		
		
		sumnum = num01+num02+num03;
		document.frm01.hInsurePrice.value=sumnum.toFixed(2);
	
	}	
	
	function sumBea(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		if(document.frm01.hBeasuti){
			num01 = document.frm01.hBeasuti.value*1;
		}
		if(document.frm01.hArgon){
			num02 = document.frm01.hArgon.value*1;
		}		
		num03 = (num01+num02)*(7/100);		
		document.frm01.hPasi.value=num03.toFixed(2);
		
		sumnum = num01+num02+num03;
		
		document.frm01.hTotal01.value=sumnum.toFixed(2);
		
		sumtotal = (num01+num02)*0.01;
		document.frm01.hTotal05.value=sumtotal.toFixed(2);
		
	}	
	
	function sumNiti(){
		var num01;
		
		num01=0;
		if(document.frm01.hTotal01){
			num01 = document.frm01.hTotal01.value*1;
		}
		
		sumnum = (num01*1)/100;
		document.frm01.hTotal02.value=sumnum.toFixed(2);
	
	}	
	
	function sumNiti01(){
		document.frm01.hTotal02.value="";	
	}	
	
	
	function checkDiscount01(){
		var num01, num02;
		
		num01=0;
		num02=0;
		if(document.frm01.hDiscountPercent){
			num01 = document.frm01.hDiscountPercent.value*1;
		}
		if(document.frm01.hTotal03){
			num02 = document.frm01.hTotal03.value*1;
		}
		sumnum = (num02*num01)/100;
		document.frm01.hDiscountPercentPrice.value=sumnum.toFixed(2);
		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
	
	}	
	
	function checkDiscount02(){

		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		
	}	
	
	function checkDiscount03(){

		document.frm01.hDiscountOtherRemark.value= "";
		document.frm01.hDiscountOtherPrice.value = "0.00";
		document.frm01.hDiscountPercent.value= "";
		document.frm01.hDiscountPercentPrice.value= "0.00";
		
	}	
	
	function sumAll(){		
		num01=0;
		num02=0;
		num03=0;
		num04=0;	
		num05=0;	
		num06=0;	
		num07=0;	
		num08=0;	
	
		sumBea();
		if(document.frm01.hTotal01){
			num01 = document.frm01.hTotal01.value*1;
		}
		if(document.frm01.hTotal02){
			num02 = document.frm01.hTotal02.value*1;
		}
		if(document.frm01.hTotal05){
			num08 = document.frm01.hTotal05.value*1;
		}
		num03 = num01-num02-num08;
		document.frm01.hTotal03.value= num03.toFixed(2);
		
		if(document.frm01.hDiscountPercentPrice){
			num04 = document.frm01.hDiscountPercentPrice.value*1;
		}
		if(document.frm01.hDiscountOtherPrice){
			num05 = document.frm01.hDiscountOtherPrice.value*1;
		}
		if(document.frm01.hPrbPrice01){
			num06 = document.frm01.hPrbPrice01.value*1;
		}
		
		if(document.frm01.hDiscountType[2].checked == true){
			num07 = num03-num04-num05;
		}else{
			num07 = num03-num04-num05+num06;
		}
		document.frm01.hTotal.value= num07.toFixed(2);
	}
	
	
	function same_prb(){
		var prb_id = document.frm01.hStockPrbId.value;
	
		if( prb_id > 0){
			document.frm01.hInsureCompanyId01.value = document.frm01.hInsureCompanyId.value;
			document.frm01.hStockKomId.value = document.frm01.hStockPrbId.value;
		
		}
	
	}
	
	function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPrbPrice.value = <?=$objItem->get_total()?>;
			document.frm01.hPrbPrice01.value = <?=$objItem->get_total()?>;
		}
	<?}?>
		sumAll();
	}
	
	<?if($objInsureOld->get_payment_amount() > 0){?>
		checkDisplay(<?=$objInsureOld->get_payment_amount()?>);
		checkSum();
	<?}?>
	
</script>
<?include "unset_all.php";?>
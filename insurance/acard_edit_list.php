<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();


$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objCustomerList = New CustomerList;


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."') OR  ( C.office_tel LIKE '%".$hKeyword."')   )";	
}

if($hKeywordCar !="" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";
	}else{
		$pstrCondition .="  ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";	
	}
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hBuyCompany > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.buy_company = '".$hBuyCompany."' ";
	}else{
		$pstrCondition .=" IC.buy_company = '".$hBuyCompany."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" IC.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$Month02."-%' ";
	}else{
		$pstrCondition .=" IC.date_verify LIKE '%-".$Month02."-%'   ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" IC.company_id = '".$hCompanyId."' ";	
	}
}

if($hSetMonth != 0 and $hSetMonth != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$hSetMonth."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '%-".$hSetMonth."-%' ";	
	}
}

if($hSetYear != 0 and $hSetYear != ""){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '".$hSetYear."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '".$hSetYear."-%' ";	
	}
}

if($hRegisterYear > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.register_year = '".$hRegisterYear."' ";
	}else{
		$pstrCondition .=" IC.register_year = '".$hRegisterYear."' ";	
	}
}

if($hCheckc >= "0"){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.checkc = '".$hCheckc."' ";
	}else{
		$pstrCondition .=" IC.checkc = '".$hCheckc."' ";	
	}
}

if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  = '".$hStartDate."' ) ";
		}
	}
}


if($hEditCheckYes > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND admin_approve='Yes'  ";
	}else{
		$pstrCondition .="  admin_approve='Yes'  ";	
	}
}

if($hEditCheckNo > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND admin_approve=''  ";
	}else{
		$pstrCondition .="  admin_approve=''  ";	
	}
}


}//end hSearch


if (!empty($hDelete) )   {  //Request to delete some CashierReserve release

    $objInsureCar = New InsureCar();
	$objInsureCar->set_car_id($hDelete);
	
	$objInsureList = new InsureList();
	$objInsureList->setFilter(" car_id= ".$hDelete);
	$objInsureList->setPageSize(0);
	$objInsureList->load();
	forEach($objInsureList->getItemList() as $objItem) {
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
	
		//��ʹ��Ҥ�
		$objInsureQuotationList = new InsureQuotationList();
		$objInsureQuotationList->setFilter(" insure_id= ".$objItem->get_insure_id());
		$objInsureQuotationList->setPageSize(0);
		$objInsureQuotationList->load();
		forEach($objInsureQuotationList->getItemList() as $objItemQuotation) {
			$objInsureQuotation = new InsureQuotation();
			$objInsureQuotation->set_quotation_id($objItemQuotation->get_quotation_id());
			$objInsureQuotation->delete();
		}
		
		//Ẻ�����
		$objInsureForm = new InsureForm();
		$objInsureForm->deleteByCondition(" insure_id = ".$objItem->get_insure_id());
		
		//��úѹ�֡�Դ���
		$objInsureTrack = new InsureTrack();
		$objInsureTrack->deleteByCondition(" insure_id = ".$objItem->get_insure_id());
		
		//��ê����Թ
		$objInsureItem = new InsureItem();
		$objInsureItem->deleteByCondition(" insure_id = ".$objItem->get_insure_id());
		
		$objInsureItemDetail = new InsureItemDetail();
		$objInsureItemDetail->deleteByCondition(" order_extra_id = ".$objItem->get_insure_id());
		
		$objInsurePayment = new InsurePayment();
		$objInsurePayment->deleteByCondition(" order_extra_id = ".$objItem->get_insure_id());

		$objInsure->delete();

	}
	
	$objInsureCar->delete();
	++$intCountDelete;
    if ($pstrMsg == "" ) $pstrMsg .= " ź������ " .$intCountDelete . " ��¡�� ";
    unset($objCustomer);
} else {
    $pstrMsg = $hMsg;
}






if (!empty($hDuplicate) )   {  //Request to delete some CashierReserve release
    $objInsureCar = New InsureCar();
	$objInsureCar->set_car_id($hDuplicate);
	if($hDuplicated == "yes"){
		$hDuplicated = "";
	}else{
		$hDuplicated = "yes";
	}
	$objInsureCar->set_duplicated($hDuplicated);
	$objInsureCar->updateDuplicated();
    unset($objInsureCar);
}

if($pstrCondition == ""){
	$pstrCondition = " admin_approve=''    ";
}


echo $pstrCondition;

$objCustomerList = new InsureEditList();
$objCustomerList->setFilter($pstrCondition);
$objCustomerList->setPageSize(PAGE_SIZE);
$objCustomerList->setPage($hPage);
$objCustomerList->setSortDefault(" edit_date ASC");
$objCustomerList->setSort($hSort);
$objCustomerList->loadSearch();

$pCurrentUrl =  "acard_edit_list.php?hBuyCompany=$hBuyCompany&hCompanyId=$hCompanyId&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hSetMonth=$hSetMonth&hSetYear=$hSetYear&hCarType=$hCarType&hCarModel=$hCarModel&hRegisterYear=$hRegisterYear&hKeywordCar=$hKeywordCar&hKeyword=$hKeyword&hSaleId=$hSaleId&hSearch=$hSearch";
//$pCurrentUrl = "acardList.php?hKeywordCar=$hKeywordCar&hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType&Month02=$Month02&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hCarType=$hCarType&hCarModel=$hCarModel&hKeyword=$hKeyword&hSaleId=$hSaleId";
// for paging only (sort resets page viewing)
$pCurrentUrlLink = "acard_add_list.php?hBuyCompany=".$hBuyCompany."YYYhKeywordCar=".$hKeywordCar."YYYhSearch=".$hSearch."YYYhKeyword=".$hKeyword."YYYhType=".$hType."YYYMonth02=".$Month02."YYYDay=".$Day."YYYMonth=".$Month."YYYYear=".$Year."YYYDay01=".$Day01."YYYMonth01=".$Month01."YYYYear01=".$Year01."YYYhCarType=".$hCarType."YYYhCarModel=".$hCarModel."YYYhKeyword=".$hKeyword."YYYhSaleId=".$hSaleId."YYYhPage=".$hPage;

$pageTitle = "1. �к��������١���";
$strHead03 = "���Ң������١���";
$pageContent = "1.9 ��Ǩ�ͺ������ ACARD ���";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>
<tr>
	<td align="right"> ��ѡ�ҹ��Ңͧ��¡�� :</td>
	<td class="small">
     	<input type="hidden" name="hSaleId" value=""><input type="text" name="hSale" size="40" value="">		
	</td>
</tr>	
<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ� , �������Ѿ��
	</td>
</tr>	
<tr>
	<td align="right"> ʶҹС����� :</td>
	<td class="small">
        <input type="checkbox" name="hEditCheckYes" value="1">�������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" name="hEditCheckNo" value="1">�ѧ��������
	</td>
</tr>	
<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
<div align="center">�� <?=$objCustomerList->mCount;?> ��¡��</div>
</form>	


<form name="frm" action="acard_add_list.php" method="POST">
<table border="0" width="98%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objCustomerList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objCustomerList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objCustomerList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="98%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="2%" align="center" class="ListTitle">�ѹ�������</td>
	<td width="10%" align="center" class="ListTitle">����-���ʡ�� �١���</td>
	<td width="10%" align="center" class="ListTitle">������ö/Ẻö</td>
	<td width="10%" align="center" class="ListTitle">�Ţ����¹/�Ţ�ѧ</td>
	<td width="5%" align="center" class="ListTitle">�Ң�</td>
	<td width="5%" align="center" class="ListTitle">���</td>
	<td width="5%" align="center" class="ListTitle">��</td>	
	<td width="20%" align="center" class="ListTitle">��䢨ҡ</td>	
	<td width="20%" align="center" class="ListTitle">�����</td>	
	<td width="5%" align="center" class="ListTitle">�ѹ������</td>
	<td width="20%" align="center" class="ListTitle">�����˵ء�����</td>
	
	<td width="5%" align="center" class="ListTitle">��Ǩ�ͺ</td>
</tr>
	<?
		$i=0;
		$checkYear =  date("Y")-1;
		forEach($objCustomerList->getItemList() as $objItem) {
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();
		
		$objCarType = new CarType();
		$objCarType->setCarTypeId($objInsureCar->get_car_type());
		$objCarType->load();
		
		$objCarModel = new CarModel();
		$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
		$objCarModel->load();

		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objTeam = new InsureTeam();
		$objTeam->setInsureTeamId($objMember->getTeam());
		$objTeam->load();
		
		$objFund = new FundCompany();
		$objFund->setFundCompanyId($objInsureCar->get_buy_company());
		$objFund->load();
		
		$objCompany = new Company();
		$objCompany->setCompanyId($objInsureCar->get_company_id());
		$objCompany->load();
	?>
	<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_edit_date())?></td>	
	<td valign="top"  ><?=$objCustomer->getCustomerTitleIdDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  align="center" ><?=$objCarType->getTitle()?><br><font color="#ff6633"><?=$objCarModel->getTitle()?></font></td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?> <?if($objInsureCar->get_province_code() != ""){?> ( <?=$objInsureCar->get_province_code()?> ) <?}?><br><font color="#ff6633"><?=$objInsureCar->get_car_number()?></font></td>
	<td valign="top"  align="center" ><?=$objCompany->getShortTitle()?></td>
	<td valign="top"  align="center" ><?=$objTeam->getTitle()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>
	<td valign="top"  align="left" ><?=$objItem->get_old_message()?></td>
	<td valign="top"  align="left" ><?=$objItem->get_new_message()?></td>
	<td valign="top"  align="left" ><?=$objItem->get_admin_date()?></td>
	<td valign="top"  align="left" ><?=$objItem->get_admin_remark()?></td>
	
	<td  valign="top" align="center"><input type="button"   value="�����¡��" onclick="window.location='acard_check.php?hEditId=<?=$objItem->get_insure_edit_id()?>&hId=<?=$objInsureCar->get_car_id()?>&hLink=<?=$pCurrentUrlLink?>';"></td>	
</tr>
	<?
	++$i;
	}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>


<script>
	function checkDelete(val){
		if(confirm('�س��ͧ��÷���ź������������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
	
	
	function checkDuplicate(val,val1){
		if(confirm('�س��ͧ��÷��С�˹������ū�ӫ�͹��¡�ù��������� ?')){
			window.location = "acardList.php?hDuplicated="+val1+"&hDuplicate="+val;
			return false;
		}else{
			return true;
		}	
	}
	
</script>
<?
	include("h_footer.php")
?>
<script>

	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});	

</script>
<?include "unset_all.php";?>
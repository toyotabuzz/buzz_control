<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$objLabel = new Label();
$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '��˹��ٻẺ�ú.����' ");
$arr = explode(",",$objLabel->getText01());

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsure = new Insure();
$objInsure->set_insure_id($hId);
$objInsure->load();

$objCompany = new Company();
$objCompany->setCompanyId($sCompanyId);
$objCompany->load();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objMember = new Member();
$objMember->setMemberId($objInsureCar->get_sale_id());
$objMember->load();

$objIF = new InsureFee();
$objIF->set_insure_fee_id($objInsure->get_p_car_type());
$objIF->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

				$hHeight = 0;
				$pdf=new PDF("P","pt","A4");
				$pdf->AliasNbPages();
				$pdf->SetMargins(0,0,0);
				$pdf->SetAutoPageBreak(false,0.5);
				//Data loading
				$pdf->AddFont('angsa','','angsa.php'); 
				$pdf->AddFont('angsa','B','angsab.php'); 

				
				$pdf->AddPage();
				$pdf->SetLeftMargin(0);
				
				$image1 = "0001.jpg";

				$pdf->Image($image1, 0, null, 600);
				
				/*
				$objInsure->setBookingMail(1);
				$objInsure->updateCrlBookingMail();
				*/

				$pdf->SetFont('angsa','',14);
				
				if($hDisplayBarcode== 1){
						$strtext = $objInsure->get_p_number();
						$pdf->SetXY($arr[52],$arr[53]+$hHeight);		
						$pdf->Write(10,$strtext);
				}
				
				
				if($hAddressType == 1){
						
						$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
						$cname = $strtext;
						
						$pdf->SetXY($arr[0],$arr[1]+$hHeight);		
						$pdf->Write(10,$strtext);

						$strtext= $objCustomer->getAddress()."  �ǧ/�Ӻ� ".$objCustomer->getTumbon();
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
					
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur()."  �ѧ��Ѵ ".$objCustomer->getProvince()." ".$objCustomer->getZip();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
				
				}
				
				if($hAddressType == 2){
						

						$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
						$cname = $strtext;
						$pdf->SetXY($arr[0],$arr[1]+$hHeight);		
						$pdf->Write(10,$strtext);
							
						if($objCustomer->getAddress01() != ""){
							
						$strtext= $objCustomer->getAddress01()." �ǧ/�Ӻ� ".$objCustomer->getTumbon01()." ".
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
				
				
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur01()." �ѧ��Ѵ ".$objCustomer->getProvince01()." ".$objCustomer->getZip01();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						
						}else{
						
						$strtext= $objCustomer->getAddress()."  �ǧ/�Ӻ� ".$objCustomer->getTumbon();
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
					
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur()."  �ѧ��Ѵ ".$objCustomer->getProvince()." ".$objCustomer->getZip();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						}
				
				}
				
				if($hAddressType == 3){
						

						$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
						$cname = $strtext;

						$pdf->SetXY($arr[0],$arr[1]+$hHeight);		
						$pdf->Write(10,$strtext);
							
						if($objCustomer->getAddress02() != ""){
							
						$strtext= $objCustomer->getCompany()." ".$objCustomer->getAddress02()." �ǧ/�Ӻ� ".$objCustomer->getTumbon02()." ".
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
				
				
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur02()." �ѧ��Ѵ ".$objCustomer->getProvince02()." ".$objCustomer->getZip02();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						
						}else{
						
						$strtext= $objCustomer->getAddress()."  �ǧ/�Ӻ� ".$objCustomer->getTumbon();
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
					
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur()."  �ѧ��Ѵ ".$objCustomer->getProvince()." ".$objCustomer->getZip();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						}
				
				}
				
				if($hAddressType == 4){
						
						if($objCustomer->getName03() != ""){
							$strtext = $objCustomer->getName03();
							$cname = $strtext;
						}else{
							$strtext = $objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname();
							$cname = $strtext;
						}
						$pdf->SetXY($arr[0],$arr[1]+$hHeight);		
						$pdf->Write(10,$strtext);
							
						if($objCustomer->getAddress03() != ""){
							
						$strtext= $objCustomer->getAddress03()." �ǧ/�Ӻ� ".$objCustomer->getTumbon03()." ".
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
				
				
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur03()." �ѧ��Ѵ ".$objCustomer->getProvince03()." ".$objCustomer->getZip03();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						
						}else{
						
						$strtext= $objCustomer->getAddress()."  �ǧ/�Ӻ� ".$objCustomer->getTumbon();
						$pdf->SetXY($arr[2],$arr[3]+$hHeight);
						$pdf->Write(10,$strtext);	
					
						$strtext= " ࢵ/����� ".$objCustomer->getAmphur()."  �ѧ��Ѵ ".$objCustomer->getProvince()." ".$objCustomer->getZip();	
						$pdf->SetXY($arr[4],$arr[5]+$hHeight);
						$pdf->Write(10,$strtext);
						}
				
				}				

				
				
				
				
				
				
				$strtext= formatThaiDate($objInsure->get_p_start_date());
				$pdf->SetXY($arr[6],$arr[7]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= formatThaiDate($objInsure->get_p_end_date());
				$pdf->SetXY($arr[8],$arr[9]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= $objIF->get_code();
				$pdf->SetXY($arr[10],$arr[11]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= $objCarType->getTitle();
				$pdf->SetXY($arr[12],$arr[13]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objCarModel->getTitle();
				$pdf->SetXY($arr[14],$arr[15]+$hHeight);
				$pdf->Write(10,$strtext);

				if($objInsureCar->get_code() != ""){
					 $strtext= $objInsureCar->get_code();
				}else{
					if($objInsureCar->get_register_year = date("Y")){
						$strtext = "ö����";
					}else{
						$strtext = "����ᴧ";
					}
				}
				$pdf->SetXY($arr[16],$arr[17]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objInsureCar->get_province_code();
				$pdf->SetXY($arr[56],$arr[57]+$hHeight);
				$pdf->Write(10,$strtext);		
				
				$strtext= $objInsureCar->get_car_number();
				$pdf->SetXY($arr[18],$arr[19]+$hHeight);
				$pdf->Write(10,$strtext);				
				
				$strtext= $objInsureCar->get_engine_number();
				$pdf->SetXY($arr[20],$arr[21]+$hHeight);
				$pdf->Write(10,$strtext);				
				
				if($objCarModel->getSaleModel01() == "HIACE") $strtext = "���";
				if($objCarModel->getSaleModel01() == "P/C") $strtext = "��";
				if($objCarModel->getSaleModel01() == "C/C") $strtext = "�к�";
				$pdf->SetXY($arr[22],$arr[23]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= $objCarSeries->getCC();
				$pdf->SetXY($arr[24],$arr[25]+$hHeight);
				$pdf->Write(10,$strtext);
				
				
				
				
				$strtext= $objInsure->get_p_total();
				$pdf->SetXY($arr[26],$arr[27]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objInsure->get_p_total();
				$pdf->SetXY($arr[28],$arr[29]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objInsure->get_p_argon();
				$pdf->SetXY($arr[30],$arr[31]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= $objInsure->get_p_insure_vat();
				$pdf->SetXY($arr[32],$arr[33]+$hHeight);
				$pdf->Write(10,$strtext);

				$strtext= number_format($objInsure->get_p_prb_price(),2);
				$pdf->SetXY($arr[34],$arr[35]+$hHeight);
				$pdf->Write(10,$strtext);

				
				
				
				if($objIF->get_use_type() == "��ǹ�ؤ��") $strtext = "����ö��ǹ�ؤ��������Ѻ��ҧ����������" ;
				if($objIF->get_use_type() == "�Ѻ��ҧ") $strtext = "����ö�Ѻ��ҧ����������" ;
				$pdf->SetXY($arr[36],$arr[37]+$hHeight);
				$pdf->Write(10,$strtext);
				
				
				
				$strtext= formatThaiDate(date("Y-m-d"));
				$pdf->SetXY($arr[38],$arr[39]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= formatThaiDate(date("Y-m-d"));
				$pdf->SetXY($arr[40],$arr[41]+$hHeight);
				$pdf->Write(10,$strtext);




				
				if($objInsureCar->get_code() != ""){
					 $strtext= $objInsureCar->get_code();
				}else{
					if($objInsureCar->get_register_year = date("Y")){
						$strtext = "ö����";
					}else{
						$strtext = "����ᴧ";
					}
				}
				$pdf->SetXY($arr[42],$arr[43]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= $objInsureCar->get_car_number();
				$pdf->SetXY($arr[44],$arr[45]+$hHeight);
				$pdf->Write(10,$strtext);
				
				
				
				
				$strtext= formatThaiDate($objInsure->get_p_start_date());
				$pdf->SetXY($arr[48],$arr[49]+$hHeight);
				$pdf->Write(10,$strtext);
				
				$strtext= formatThaiDate($objInsure->get_p_end_date());
				$pdf->SetXY($arr[50],$arr[51]+$hHeight);
				$pdf->Write(10,$strtext);				
				
				$pdf->SetLeftMargin($arr[46]);
				$strtext= $cname;
				$pdf->SetXY($arr[46],$arr[47]+$hHeight);
				$pdf->Write(10,$strtext);
				
				if($hDisplayBarcode== 1){
						$strtext = $objInsure->get_p_number();
						$pdf->SetXY($arr[54],$arr[55]+$hHeight);		
						$pdf->Write(10,$strtext);
				}
				
				$strtext= $objInsureCar->get_province_code();
				$pdf->SetXY($arr[58],$arr[59]+$hHeight);
				$pdf->Write(10,$strtext);
				
				
				$pdf->Output();	
	?>
	<?include "unset_all.php";?>
	<script>
	window.close();
	</script>	
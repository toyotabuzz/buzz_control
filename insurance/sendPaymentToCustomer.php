<?php
include("common.php");

if (!isset($hInsureId)||!isset($hInsureItemId)) {
    echo '�Դ��ͼԴ��Ҵ! �����źҧ��ǹ�Ҵ����';
    echo '<br><br>';
    echo '�ô���ѡ�����к����ѧ��͹��Ѻ�˹����ѡ';
    echo '<meta http-equiv="refresh" content="3;url=main.php">';
    exit;
}

// �����Ż�Сѹ���
$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

// �����ŧǴ�觪�������
$objInsureItem = new InsureItem();
$objInsureItem->set_insure_item_id($hInsureItemId);
$objInsureItem->load();

if ($objInsure->get_insure_id()!=$objInsureItem->get_insure_id()) {
    echo '�Դ��ͼԴ��Ҵ! ���������ç�������к���ͧ���';
    echo '<br><br>';
    echo '�ô���ѡ�����к����ѧ��͹��Ѻ�˹����ѡ';
    echo '<meta http-equiv="refresh" content="3;url=main.php">';
    exit;
} else if ($objInsureItem->get_payment_style_id()!=3) {
    echo '�Դ��ͼԴ��Ҵ! ��¡�ê����Թ "'.(($objInsureItem->get_payment_style_title()=='')?'�ѧ����˹�':$objInsureItem->get_payment_style_title()).'" ���͹حҵ�����ҹ�ѧ���蹹��';
    echo '<br><br>';
    echo '�ô���ѡ�����к����ѧ��͹��Ѻ�˹����ѡ';
    echo '<meta http-equiv="refresh" content="3;url=main.php">';
    exit; 
}

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();

// �������١���
$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();
$customerName = $objCustomer->getTitleDetail().' '.$objCustomer->getFirstname().' '.$objCustomer->getlastname();
// ��˹�����������
// $linkPayment = 'https://bit.ly/3m4VPma';
$customerTel = $objCustomer->getHomeTel();
$customerMobile = '';
$customerEmail = '';

// �����š����� Quotaion ����١���
$objQuotationSendLogList = new JmQuotationSendLogList();
$objQuotationSendLogList->setInsureQuotationId($objInsure->get_quotation_id());
$objQuotationSendLogList->load();
// $objQuotationSendLogList->getItemList();
if ($objQuotationSendLogList->getItemList()!='') {
    foreach ($objQuotationSendLogList->getItemList() as $log) {
        if ($log->mQuotationSendType==1&&$log->mQuotationSendText!='') { // ������
            $customerMobile = $log->mQuotationSendText;
        } else if ($log->mQuotationSendType==2&&$log->mQuotationSendText!='') {
            $customerEmail = $log->mQuotationSendText;
        }
    }
}
if ($customerMobile=='') {
    if ($objCustomer->getMobile()!='') {
        $_arrMobile = explode(',', $objCustomer->getMobile());
		$arrMobile  = array();
		if ($_arrMobile) {
			foreach ($_arrMobile as $val) {
				preg_match_all('!\d+!', trim($val), $matches);
				if ($matches[0]!='') {
					$arrMobile[] = implode($matches[0]);
				}
			}
			if ($arrMobile) {
				$customerMobile = implode(',', $arrMobile);
			}
		}
    }
}
$linkPayment = '';
if ($customerMobile!='') {
    $_arrMobileNo = explode(',', $customerMobile);
    $customerTel = (isset($_arrMobileNo[0])&&($_arrMobileNo[0]!=''))?$_arrMobileNo[0]:'0123456789';
    $linkPayment = getPaymentLink($hInsureItemId, $objInsureItem->get_payment_price(), trim($customerName), trim($customerTel));
}
// echo '>>>'.$linkPayment;
// exit;
// ����͡��ѹ�֡������
if (isset($hSubmit)) {
    $isError = 0;
    $errorMessage = '';
    // echo'<pre>';print_r($_POST);
    // ��Ǩ�ͺ������
    if (!isset($_POST)) {
        $isError++;
        $errorMessage = '�ô��Ǩ�ͺ�Ըա���觢�����';
    } else if ((!isset($_POST['hCustMobileNo'])||($_POST['hCustMobileNo']!='on'))&&(!isset($_POST['hCustEmail'])||($_POST['hCustEmail']!='on'))&&(!isset($_POST['hCustLink'])||($_POST['hCustLink']!='on'))) {
        $isError++;
        $errorMessage = '�ô���͡�Ըա�����ԧ������Թ����١���';
    } else {
        // �֧�ӹǹ��������ش�����������
        $objInsurePaymentSendLog = new InsurePaymentSendLog();
        $objInsurePaymentSendLog->setInsureId($hInsureId);
        $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
        $objInsurePaymentSendLog->load();
        $maxSendNo = $objInsurePaymentSendLog->getSendNo() ? $objInsurePaymentSendLog->getSendNo()+1:1;

        // ���͡�觼�ҹ SMS
        if (isset($_POST['hCustMobileNo'])&&($_POST['hCustMobileNo']=='on')) {
            if (!isset($_POST['hCustMobileNoDesc'])||($_POST['hCustMobileNoDesc']=='')) {
                $isError++;
                $errorMessage .= $isError ? '<br>':'';
                $errorMessage .= $isError.'. �ô�к��������Ѿ��';
            } else {
                $_customerMobile = $customerMobile; // �������Ѿ��ҡ�к�
                $customerMobile = $_POST['hCustMobileNoDesc']; // ����ҡ�к���з���к�����
                $allCustomerMobile = $_customerMobile ? $_customerMobile.','.$customerMobile:$customerMobile; // ������������

                $arrOldMobile = explode(',', $_customerMobile);
                $arrMobileNo = explode(',', $allCustomerMobile);
                $_arrMobileNo = array();
                foreach ($arrMobileNo as $mobileNo) {
                    preg_match_all('!\d+!', trim($mobileNo), $matches);
                    if ($matches[0]!='') {
                        $mobileNo = implode($matches[0]);
                    }

                    if ($mobileNo==''||strlen($mobileNo)!=10) {
                        $isError++;
                        $errorMessage .= $isError ? '<br>':'';
                        $errorMessage .= '�������Ѿ�� '.$mobileNo.' ���١�� sms';
                    } else {
                        if (!in_array($mobileNo, $_arrMobileNo)) {
                            $_arrMobileNo[] = $mobileNo;
                        }
                    }
                }
            }
        }
        // ���͡�觼�ҹ Email
        if (isset($_POST['hCustEmail'])&&($_POST['hCustEmail']=='on')) {
            if (!isset($_POST['hCustEmailDesc'])||($_POST['hCustEmailDesc']=='')) {
                $isError++;
                $errorMessage .= $isError ? '<br>':'';
                $errorMessage .= $isError.'. �ô�к� Email';
            } else {
                $customerEmail = $_POST['hCustEmailDesc'];
                $arrEmail = explode(',', str_replace(' ', '', $customerEmail));
                foreach ($arrEmail as $email) {
                    if ($email==''||!verifyEmail($email)) {
                        $isError++;
                        $errorMessage .= $isError ? '<br>':'';
                        $errorMessage .= $isError.'. ���������١��ͧ '.$email.' �ô��Ǩ�ͺ�����ա���� email invalid';
                    }
                }
            }
        }
        // �ѹ�֡����������� link ���������͡
        $objInsureCar = new InsureCar();
        $objInsureCar->set_car_id($objInsure->get_car_id());
        $objInsureCar->load();
        $carLicense = $objInsureCar->get_code();
        $periodNumDesc = $objInsureItem->get_payment_order().'/'.$objInsure->get_pay_number();
        $paymentAmt = number_format($objInsureItem->get_payment_price(), 2);

        // ���͡�觼�ҹ SMS
        $isSendSMS = 0;
        if (isset($_POST['hCustMobileNo'])&&($_POST['hCustMobileNo']=='on')) {
            $arrDiffMobile = array_diff($_arrMobileNo, $arrOldMobile);
            // echo '<pre>';
            // echo '��������ҡ�к� : ';print_r($arrOldMobile);
            // echo '����ҡ��ä��� : ';print_r($_arrMobileNo);
            // echo '���� Diff : ';print_r($arrDiffMobile);

            // �������ͧ����� link �����Թ����������Ѿ�����������к�������ç�Ѻ�������Ѿ����к��ͧ�١���
            if ($arrDiffMobile) {
                foreach ($arrDiffMobile as $mobileNo) {
                    $text = "��һ�Сѹ���ö¹�� ����¹ ".$carLicense;
                    if($objInsure->get_pay_number() > 1){
                        $text .= " �Ǵ��� ".$periodNumDesc;
                    }
                    $text .= " �ӹǹ�Թ ".$paymentAmt." �. ���ԧ�������ͪ����Թ ".$linkPayment;
                    sendSMS ('insurance_payment', $mobileNo, $text);
                    $objInsurePaymentSendLog = new InsurePaymentSendLog();
                    $objInsurePaymentSendLog->setInsureId($hInsureId);
                    $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
                    $objInsurePaymentSendLog->setSendType(1); // SMS
                    $objInsurePaymentSendLog->setSendTo($mobileNo);
                    $objInsurePaymentSendLog->setSendText($text);
                    $objInsurePaymentSendLog->setSendNo($maxSendNo);
                    $objInsurePaymentSendLog->add();
                    $isSendSMS++;
                }
            }

            // �������Ѿ������ͧ�١��Ҩҡ��������Ѻ�к�
            // �ó��������Ѿ�����������к� "�ç" �Ѻ�������Ѿ����к� �觢�ͤ��� "link �����Թ"
            // �ó��������Ѿ�����������к� "���ç" �Ѻ�������Ѿ����к� �觢�ͤ��� "����͹"
            if ($arrOldMobile) {
                foreach ($arrOldMobile as $mobileNo) {
                    if ($arrDiffMobile) {
                        $text = "���Ѻ�駢���������´���Ф�һ�Сѹ���ö¹�� ����¹ ".$carLicense." �ҡ�����Ţ��� �ҡ�����ա���駢���¡�ôѧ������ô�Դ��ͷѹ�շ�� 027019999";
                    } else {
                        $text = "��һ�Сѹ���ö¹�� ����¹ ".$carLicense;
                        if($objInsure->get_pay_number() > 1){
                            $text .= " �Ǵ��� ".$periodNumDesc;
                        }
                        $text .= " �ӹǹ�Թ ".$paymentAmt." �. ���ԧ�������ͪ����Թ ".$linkPayment;
                    }
                    sendSMS ('insurance_payment', $mobileNo, $text);
                    $objInsurePaymentSendLog = new InsurePaymentSendLog();
                    $objInsurePaymentSendLog->setInsureId($hInsureId);
                    $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
                    $objInsurePaymentSendLog->setSendType(1); // SMS
                    $objInsurePaymentSendLog->setSendTo($mobileNo);
                    $objInsurePaymentSendLog->setSendText($text);
                    $objInsurePaymentSendLog->setSendNo($maxSendNo);
                    $objInsurePaymentSendLog->add();
                    $isSendSMS++;
                }
            }
        }

        if (!$isError||$isSendSMS) {
            // if ($customerMobile!='') {
            //     $customerTel = (isset($_arrMobileNo[0])&&($_arrMobileNo[0]!=''))?$_arrMobileNo[0]:'0123456789';
            // }
            // $linkPayment = getPaymentLink($hInsureItemId, $objInsureItem->get_payment_price(), trim($customerName), trim($customerTel));
            // echo '>>>'.$linkPayment;
            // exit;
            // $objInsureCar = new InsureCar();
            // $objInsureCar->set_car_id($objInsure->get_car_id());
            // $objInsureCar->load();
            // $carLicense = $objInsureCar->get_code();
            // $periodNumDesc = $objInsureItem->get_payment_order().'/'.$objInsure->get_pay_number();
            // $paymentAmt = number_format($objInsureItem->get_payment_price(), 2);

            // // ���͡�觼�ҹ SMS
            // if (isset($_POST['hCustMobileNo'])&&($_POST['hCustMobileNo']=='on')) {
            //     $arrDiffMobile = array_diff($_arrMobileNo, $arrOldMobile);
            //     // echo '<pre>';
            //     // echo '��������ҡ�к� : ';print_r($arrOldMobile);
            //     // echo '����ҡ��ä��� : ';print_r($_arrMobileNo);
            //     // echo '���� Diff : ';print_r($arrDiffMobile);

            //     // �������ͧ����� link �����Թ����������Ѿ�����������к�������ç�Ѻ�������Ѿ����к��ͧ�١���
            //     if ($arrDiffMobile) {
            //         foreach ($arrDiffMobile as $mobileNo) {
            //             $text = "��һ�Сѹ���ö¹�� ����¹ ".$carLicense;
            //             if($objInsure->get_pay_number() > 1){
            //                 $text .= " �Ǵ��� ".$periodNumDesc;
            //             }
            //             $text .= " �ӹǹ�Թ ".$paymentAmt." �. ���ԧ�������ͪ����Թ ".$linkPayment;
            //             sendSMS ('insurance_payment', $mobileNo, $text);
            //             $objInsurePaymentSendLog = new InsurePaymentSendLog();
            //             $objInsurePaymentSendLog->setInsureId($hInsureId);
            //             $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
            //             $objInsurePaymentSendLog->setSendType(1); // SMS
            //             $objInsurePaymentSendLog->setSendTo($mobileNo);
            //             $objInsurePaymentSendLog->setSendText($text);
            //             $objInsurePaymentSendLog->setSendNo($maxSendNo);
            //             $objInsurePaymentSendLog->add();
            //         }
            //     }

            //     // �������Ѿ������ͧ�١��Ҩҡ��������Ѻ�к�
            //     // �ó��������Ѿ�����������к� "�ç" �Ѻ�������Ѿ����к� �觢�ͤ��� "link �����Թ"
            //     // �ó��������Ѿ�����������к� "���ç" �Ѻ�������Ѿ����к� �觢�ͤ��� "����͹"
            //     if ($arrOldMobile) {
            //         foreach ($arrOldMobile as $mobileNo) {
            //             if ($arrDiffMobile) {
            //                 $text = "���Ѻ�駢���������´���Ф�һ�Сѹ���ö¹�� ����¹ ".$carLicense." �ҡ�����Ţ��� �ҡ�����ա���駢���¡�ôѧ������ô�Դ��ͷѹ�շ�� 027019999";
            //             } else {
            //                 $text = "��һ�Сѹ���ö¹�� ����¹ ".$carLicense;
            //                 if($objInsure->get_pay_number() > 1){
            //                     $text .= " �Ǵ��� ".$periodNumDesc;
            //                 }
            //                 $text .= " �ӹǹ�Թ ".$paymentAmt." �. ���ԧ�������ͪ����Թ ".$linkPayment;
            //             }
            //             sendSMS ('insurance_payment', $mobileNo, $text);
            //             $objInsurePaymentSendLog = new InsurePaymentSendLog();
            //             $objInsurePaymentSendLog->setInsureId($hInsureId);
            //             $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
            //             $objInsurePaymentSendLog->setSendType(1); // SMS
            //             $objInsurePaymentSendLog->setSendTo($mobileNo);
            //             $objInsurePaymentSendLog->setSendText($text);
            //             $objInsurePaymentSendLog->setSendNo($maxSendNo);
            //             $objInsurePaymentSendLog->add();
            //         }
            //     }
            // }

            // ���͡�觼�ҹ Email
            if (isset($_POST['hCustEmail'])&&($_POST['hCustEmail']=='on')) {
                $text = "��һ�Сѹ���ö¹�� ����¹ ".$carLicense." �Ǵ��� ".$periodNumDesc." �ӹǹ�Թ ".$paymentAmt." �. ���ԧ�������ͪ����Թ ".$linkPayment;
                foreach ($arrEmail as $email) {
                    sendEmailViaBuzzSure ($email, 'SURE INSURE', $text);
                    $objInsurePaymentSendLog = new InsurePaymentSendLog();
                    $objInsurePaymentSendLog->setInsureId($hInsureId);
                    $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
                    $objInsurePaymentSendLog->setSendType(2); // email
                    $objInsurePaymentSendLog->setSendTo($email);
                    $objInsurePaymentSendLog->setSendText($text);
                    $objInsurePaymentSendLog->setSendNo($maxSendNo);
                    $objInsurePaymentSendLog->add();
                }
            }

            // ���͡�¡�� copy link
            if (isset($_POST['hCustLink'])&&($_POST['hCustLink']=='on')) {
                $objInsurePaymentSendLog = new InsurePaymentSendLog();
                $objInsurePaymentSendLog->setInsureId($hInsureId);
                $objInsurePaymentSendLog->setInsureItemId($hInsureItemId);
                $objInsurePaymentSendLog->setSendType(3); // copy link
                $objInsurePaymentSendLog->setSendTo('');
                $objInsurePaymentSendLog->setSendText($linkPayment);
                $objInsurePaymentSendLog->setSendNo($maxSendNo);
                $objInsurePaymentSendLog->add();
            }

            echo '��й���к������ԧ��仵���ٻẺ����ҹ��˹���кѹ�֡���������º��������';
            echo '<br><br>';
            echo '�ô���ѡ�����к����ѧ��͹��Ѻ�˹����ѡ';
            echo '<meta http-equiv="refresh" content="3;url=main.php">';
            exit;
        }
    }
}

$pageTitle = "�ٻẺ������ԧ������Թ";
$pageContent = "���ԧ������Թ����١���";
include "h_header.php";
?>
<style>
    .vcenter {
        text-align: center;
        vertical-align: middle;
    }
    .vleft {
        text-align: left;
        vertical-align: middle;
    }
    .vright {
        text-align: right;
        vertical-align: middle;
    }
    .form-box {
        width: 500px;
        margin: 0 auto;
        border: 1px solid #ccc;
        text-align: center;
    }
    .form-table {
        width: 100%;
    }
    .form-input {
        width: 100%;
    }
    .box-error {
        width: 500px;
        margin: 0 auto;
    }
    .box-error h4 {
        margin: 0 0 10px 0;
        padding: 5px;
        background-color: #f00;
        color: #fff;
    }
    .box-error p {
        padding: 5px;
        margin-bottom: 30px;
    }
</style>
<!-- if(event.keyCode==13) event.keyCode=9; ��� ������ Enter -->
<form id="form-send-payment-to-customer" name="form-send-payment-to-customer" action="sendPaymentToCustomer.php?hInsureId=<?=$hInsureId?>&hInsureItemId=<?=$hInsureItemId?>" method="post" onKeyDown="">
    <?php if (isset($isError)&&($isError>0)) { ?>
        <div class="box-error">
            <h4>�Դ��ͼԴ��Ҵ</h4>
            <p><?=$errorMessage?></p>
        </div>
    <?php } ?>
    <div class="form-box">
        <table class="form-table">
            <thead>
                <tr style="background: #dbdbdb;">
                    <th colspan="3" class="vleft" style="font-size: 12px;padding: 5px;">�ٻẺ������ԧ������Թ</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="vcenter" colspan="2" style="padding: 10px 0px 10px 0px">LINK</th>
                    <th class="vleft" colspan="2" style="padding: 10px 0px 10px 0px"><p id="copyText" style="width: 75%; float:left; word-break: break-all;"><?=$linkPayment;?></p> 
                    <input type="button" id="demo" onclick="copyToClipboard('<?=$linkPayment?>')" style="float: right; margin-top: 80px; margin-right: 6px;" value="Copy Link">
                </tr>
                <tr>
                    <td class="vcenter"><input type="checkbox" name="hCustMobileNo" <?=(isset($_POST['hCustMobileNo'])&&($_POST['hCustMobileNo']=='on'))?'checked':''?>></td>
                    <td class="vleft" style="width: 20%;">�� SMS</td>
                    <td class="vleft" style="padding-right: 4px;"><input type="text" class="form-input" name="hCustMobileNoDesc" value="<?=$customerMobile?>"></td>
                </tr>
                <tr>
                    <td class="vcenter"><input type="checkbox" name="hCustEmail" <?=(isset($_POST['hCustEmail'])&&($_POST['hCustEmail']=='on'))?'checked':''?>></td>
                    <td class="vleft">�� Email</td>
                    <td class="vleft" style="padding-right: 4px;"><input type="text" class="form-input" name="hCustEmailDesc" value="<?=$customerEmail?>"></td>
                </tr>
                <tr>
                    <td class="vcenter" style="padding-bottom: 10px;"><input type="checkbox" name="hCustLink" <?=(isset($_POST['hCustLink'])&&($_POST['hCustLink']=='on'))?'checked':''?>></td>
                    <td class="vleft" style="padding-bottom: 10px;">�� line (copy link)</td>
                    <td style="padding-bottom: 10px;"></td>
                </tr>
                <tr>
                    <td colspan="3" class="vcenter" style="padding: 10px;border-top: 1px solid #ccc;">
                        <input type="submit" id="hSubmit" name="hSubmit" onclick="" value="�ѹ�֡������" style="cursor: pointer;">
                    </td>
                </tr>
            </tbody>
        </table>
        <p style="color: red; font-weight: bold; font-size: 18px;"><u>��͹!</u> �����ҧ���ͺ�к�<br><br>��س���������þ�Ѿ����ѡ�ͧ�١���������������Ѿ������Ѻ���ͺ��ҹ��</p>
    </div>
</form>

<?php
include "h_footer.php";
include "unset_all.php";
?>
<script>
    function copyToClipboard(text) {
        var input = document.createElement('input');
        input.setAttribute('value', text);
        document.body.appendChild(input);
        input.select();
        input.focus();
        var result = document.execCommand('copy');
        document.body.removeChild(input);
        return result;
  }
</script>
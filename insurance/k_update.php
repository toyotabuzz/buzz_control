<?
include("common.php");
define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",50);

$objInsure = new Insure();
$objInsure->loadByCondition("  insure_id = '$hInsureId'  ");

if($objInsure->get_quotation_id() ==0){
	header("location:step03List.php?hInsureId=$hInsureId&hErr=quotation");
	exit;
}


$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($hCarId);
$objInsureCar->load();

$objInsure = new Insure();
$objInsure->loadByCondition("  insure_id = '$hInsureId'  ");
$objInsureList = new InsureList();
$objOrder = new Order();
$objInsureItem = new InsureItem();

$arrDate = explode("-",$objInsure->get_date_protect());
$DayProtect = $arrDate[2];
$MonthProtect = $arrDate[1];
$YearProtect = $arrDate[0];

$arrDate = explode("-",$objInsure->get_p_start_date());
$hPStartDay = $arrDate[2];
$hPStartMonth = $arrDate[1];
$hPStartYear = $arrDate[0];

$arrDate = explode("-",$objInsure->get_p_end_date());
$hPEndDay = $arrDate[2];
$hPEndMonth = $arrDate[1];
$hPEndYear = $arrDate[0];

$arrDate = explode("-",$objInsure->get_k_start_date());
$hKStartDay = $arrDate[2];
$hKStartMonth = $arrDate[1];
$hKStartYear = $arrDate[0];

$arrDate = explode("-",$objInsure->get_k_end_date());
$hKEndDay = $arrDate[2];
$hKEndMonth = $arrDate[1];
$hKEndYear = $arrDate[0];

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort("  use_type, code, title ASC");
$objInsureFeeList->load();

$objInsureFee1List = new InsureFee1List();
$objInsureFee1List->setPageSize(0);
$objInsureFee1List->setSort("  use_type, code, title ASC");
$objInsureFee1List->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" rank ASC ");
$objPaymentSubjectList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objInsureFeeList = new InsureFeeList();
$objInsureFeeList->setPageSize(0);
$objInsureFeeList->setSort(" title ASC");
$objInsureFeeList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objBrokerList = new InsureBrokerList();
$objBrokerList->setPageSize(0);
$objBrokerList->setSort(" title ASC");
$objBrokerList->load();

$objPaymentTypeList = new PaymentTypeList();
$objPaymentTypeList->setPageSize(0);
$objPaymentTypeList->setSortDefault(" title ASC ");
$objPaymentTypeList->load();

$objPaymentSubjectList = new PaymentSubjectList();
$objPaymentSubjectList->setFilter(" type_id = 4");
$objPaymentSubjectList->setPageSize(0);
$objPaymentSubjectList->setSortDefault(" title ASC ");
$objPaymentSubjectList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objBookBankList = new BookBankList();
$objBookBankList->setPageSize(0);
$objBookBankList->setSortDefault(" title ASC ");
$objBookBankList->load();


if(isset($hSubmit)){
	
	$objInsure->set_insure_id($hInsureId);
	$objInsure->set_sale_id($sMemberId);
	$objInsure->set_car_id($hCarId);
	$objInsure->set_customer_id($objInsureCar->get_insure_customer_id());
	$objInsure->set_company_id($sCompanyId);
	$objInsure->set_year_extend($hInsureYear);
	$hDateProtect = $YearProtect."-".$MonthProtect."-".$DayProtect;
	$objInsure->set_date_protect($hDateProtect);
	$objInsure->set_k_check($hSale02);
	$objInsure->set_k_broker_id($hKBrokerId);
	$objInsure->set_k_prb_id($hKPrbId);
	$objInsure->set_k_stock_id($hKStockId);
	$objInsure->set_k_number($hKNumber);
	$hKStartDate = $hKStartYear."-".$hKStartMonth."-".$hKStartDay;
	$objInsure->set_k_start_date($hKStartDate);
	$hKEndDate = $hKEndYear."-".$hKEndMonth."-".$hKEndDay;
	$objInsure->set_k_end_date($hKEndDate);
	$objInsure->set_k_frees($hKFrees);
	$objInsure->set_p_check($hSale01);
	$objInsure->set_p_broker_id($hPBrokerId);
	$objInsure->set_p_prb_id($hPPrbId);
	$objInsure->set_p_stock_id($hPStockId);
	$objInsure->set_p_number($hPNumber);
	$hPStartDate = $hPStartYear."-".$hPStartMonth."-".$hPStartDay;
	$objInsure->set_p_start_date($hPStartDate);
	$hPEndDate = $hPEndYear."-".$hPEndMonth."-".$hPEndDay;
	$objInsure->set_p_end_date($hPEndDate);
	$objInsure->set_p_frees($hPFrees);
	
	$objInsure->set_k_num01(str_replace(",", "",$hFund));
	$objInsure->set_k_num02(str_replace(",", "", $hKBeasuti));
	$objInsure->set_k_num03(str_replace(",", "", $hKArgon));
	$objInsure->set_k_num04(str_replace(",", "", $hKInsureVat));
	$objInsure->set_k_num05(str_replace(",", "", $hKPrice));
	$objInsure->set_k_num06(str_replace(",", "", $hKSum01));
	$objInsure->set_k_num07(str_replace(",", "", $hKVat));
	$objInsure->set_k_num08(str_replace(",", "", $hKSum02));
	$objInsure->set_k_num09(str_replace(",", "", $hPSum01));
	$objInsure->set_k_num10(str_replace(",", "", $hDiscountOtherPrice));
	$objInsure->set_k_num11(str_replace(",", "", $hPSum02));
	$objInsure->set_k_num12(str_replace(",", "", $hKSum03));
	$objInsure->set_k_num13(str_replace(",", "", $hTotal));
	$objInsure->set_k_niti($hKNiti);
	$objInsure->set_k_dis_type_01($hDiscountType01);
	$objInsure->set_k_dis_type_02($hDiscountType02);
	$objInsure->set_k_dis_type_03($hDiscountType03);
	$objInsure->set_k_dis_con_01($hMorePrice);
	$objInsure->set_k_dis_con_02($hDiscountOtherRemark);
	$objInsure->set_k_free($hMoreRemark);
	$objInsure->set_k_type($hInsureType);
	$objInsure->set_k_type_remark($hInsureTypeRemark);
	$objInsure->set_k_fix($hRepair);
	$objInsure->set_k_car_type($hKCarType);
	$objInsure->set_p_car_type($hInsureFeeId);
	
	$objInsure->set_p_prb_price(str_replace(",", "", $hPPrice));
	$objInsure->set_p_year($hCarStatus);
	$hCallDate = $YearReportDate."-".$MonthReportDate."-".$DayReportDate;
	$hGetDate = $YearRecieveReport."-".$MonthRecieveReport."-".$DayRecieveReport;
	$objInsure->set_p_call_date($hCallDate);
	$objInsure->set_p_get_date($hGetDate);
	$objInsure->set_p_call_number($hAccNo);
	$objInsure->set_p_remark($hRemark);
	$objInsure->set_p_insure_vat(str_replace(",", "",$hPInsureVat));
	$objInsure->set_p_argon(str_replace(",", "",$hPArgon));
	$objInsure->set_p_total(str_replace(",", "",$hPBeasuti));
	$objInsure->set_p_vat(str_replace(",", "",$hPVat));
	
	$objInsure->set_pay_number($hPaymentAmount);
	$objInsure->set_pay_type($hPaymentCondition);
	$objInsure->set_status("booking");
	$objInsure->set_direct("yes");
	
	$hInsureDate = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
	$objInsure->set_insure_date($hInsureDate);
	$objInsure->set_p_rnum($hPRNum);
	$objInsure->set_k_new($hKNew);
	$objInsure->set_officer_id($hOfficerId);
	$hOfficerDate = $hOfficerYear."-".$hOfficerMonth."-".$hOfficerDay;
	$objInsure->set_officer_date($hOfficerDate);
	
	$objInsure->update();
	
	$objInsure->set_k_status("booking");
	$objInsure->updateKStatus();

	for($x=1;$x<=$hPaymentAmount;$x++){	

		$objInsureItem->set_insure_id($hInsureId);
		$hPaymentDate01 = $_POST["hYear_$x"]."-".$_POST["hMonth_$x"]."-".$_POST["hDay_$x"];
		$objInsureItem->set_payment_date($hPaymentDate01);
		$objInsureItem->set_payment_price(str_replace(",", "", $_POST["hPaymentPrice_$x"] ));
		$objInsureItem->set_remark($_POST["hInsureRemark_$x"]);
		$objInsureItem->set_payment_order($x);
		$hInsureItemId = $objInsureItem->add();
		
			for($y=1;$y<=5;$y++){	
				$objInsureItemDetail = new InsureItemDetail();
				if(str_replace(",", "", $_POST["hPrice_".$x."_".$y]) > 0){
	
					$objInsureItemDetail->setOrderId($hInsureItemId);
					$objInsureItemDetail->setOrderExtraId($hInsureId);
					$objInsureItemDetail->setPaymentSubjectId($_POST["hPaymentSubjectId_".$x."_".$y]);
					$objInsureItemDetail->setPrice(str_replace(",", "", $_POST["hPrice_".$x."_".$y]));
					$objInsureItemDetail->setQty($_POST["hQty_".$x."_".$y]);
					$objInsureItemDetail->setRemark($_POST["hRemark_".$x."_".$y]);
					$objInsureItemDetail->setPayin($_POST["hPayin_".$x."_".$y]);
					$objInsureItemDetail->add();
				}
			}	
	}
	
	for($x=1;$x<=$hSPCount;$x++){
		if(isset($_POST["hStockProductCheck_".$x])){
			$objInsureFree = new InsureFree();
			$objInsureFree->set_insure_id($hInsureId);
			$objInsureFree->set_stock_product_id($_POST["hStockProductId_".$x]);
			$objInsureFree->set_title($_POST["hStockProductTitle_".$x]);
			$objInsureFree->set_qty($_POST["hStockProductQty_".$x]);
			$objInsureFree->set_price(str_replace(",", "", $_POST["hStockProductPrice_".$x]));
			$objInsureFree->add();
		}
	}

	$objInsure = new InsureCar();
	$objInsure->set_car_id($hCarId);
	$objInsure->set_operate("booking");
	$objInsure->updateOperate();
	
	header("location:step06List.php?hInsureId=$hInsureId");
	break;

}


$pageTitle = "2. �к���õԴ����١���";
$pageContent = "2.5 �ѹ�֡��â�¡�˹�����";
include("h_header.php");
?>
	<script type="text/javascript" src="../include/numberFormat154.js"></script>

	<script type="text/javascript">
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	
	function tryNumberFormatValue(val)
	{
		person = new Object()
		person.name = "Tim Scarfe"
		person.height = "6Ft"
		person.value = val;

		person.value = val;
		if(person.value != ""){
			person.value = new NumberFormat(person.value).toFormatted();
			return person.value;
		}else{
			return "0.00";
		}
	}
	//-->
	</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs_present.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<table width="100%">
<tr>
	<td colspan="2" align="right">
		<table cellpadding="3" cellspacing="0">
		<tr>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td><a href="step02List.php?hCarId=<?=$hCarId?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$hInsureId?>" class="link02">�ä��駷�� 1</a></td>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td ><a href="step03List.php?hCarId=<?=$hCarId?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$hInsureId?>" class="link01">��ʹ��Ҥ�</a></td>
			<td><img src="../images/circle01.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td ><a href="step04List.php?hCarId=<?=$hCarId?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$hInsureId?>" class="link01">�Դ��â��</a></td>
			<td><img src="../images/circle02.gif" alt="" width="15" height="15" border="0" align="absmiddle"></td>
			<td><a href="step05List.php?hCarId=<?=$hCarId?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$hInsureId?>" class="link01">�ѹ�֡��â��</a></td>

		</tr>
		</table>	
	</td>
</tr>
<tr>
	<td valign="top" >
<?if($hInsureId != ""){
	$objInsure = new Insure();
	$objInsure->set_insure_id($hInsureId);
	$objInsure->load();
	
	$hCarId = $objInsure->get_car_id();
?>				
		
		<?include "insureDetail.php"?>
		
		
		<form name="frm01" action="step05List.php" method="post" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
		<input type="hidden" name="hSaveForm" value="Yes">		
		<input type="hidden" name="hCarId" value="<?=$hCarId?>">
		<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
		<input type="hidden" name="hYearExtend" value="<?=$hYearExtend?>">
		<input type="hidden" name="hSubmit" value="Yes">
		
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background"><strong>�����Ż�Сѹ���</strong></td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>	
		��¡�ù���ա���ʹ͢�´ѧ��� <input type="checkbox" name="hSale01" value="1" onclick="check_sale(1);"  <?if($objInsure->get_p_check()==1) echo "checked"?>> 
		����Ҥ�ѧ�Ѻ &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="hSale02" value="1"  onclick="check_sale(2);"  <?if($objInsure->get_k_check()==1) echo "checked"?>> ����Ҥ��Ѥ��
		&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="hSale03" value="1"  onclick="check_sale(3);"> ����¡�âͧ��
		 
	
	<table width="100%" id="sale_p" >
		<tr>
			<td bgcolor="#9dc6f7" height="25" ><strong> �Ҥ�ѧ�Ѻ</td>
		</tr>
		<tr>
			<td>
			
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>	
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr >
			<td class="listDetail"><?$objBrokerList->printSelectScript("hPBrokerId",$objInsure->get_p_broker_id(),"��µç");?></td>
			<td class="listDetail"><?$objInsureCompanyList->printSelect("hPPrbId",$objInsure->get_p_prb_id(),"����к�");?><input type="hidden" name="hPStockId"  size="5" value="<?=$objInsure->get_p_stock_id();?>"><input type="hidden" name="hPNumber"  size="30" value="<?=$objInsure->get_p_number();?>"></td>
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPStartDay value="<?=$hPStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPStartMonth value="<?=$hPStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPStartYear" value="<?=$hPStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPStartYear,hPStartDay, hPStartMonth, hPStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hPEndDay value="<?=$hPEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hPEndMonth value="<?=$hPEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hPEndYear" value="<?=$hPEndYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hPEndYear,hPEndDay, hPEndMonth, hPEndYear,popCal);return false"></td>		
				</tr>
				</table>		
			</td>		
		</tr>
		</table>
		
		
			</td>		
		</tr>
		<tr><td>
		
		
		<table width="100%" cellpadding="2" cellspacing="2" >

		<tr>
			<td class="listDetail" width="40%">������ö��Т�Ҵö¹��</td>
			<td class="listDetail" >
				<?
				$objInsureFee = new InsureFee();
				$objInsureFee->set_insure_fee_id($objInsure->get_p_car_type());
				$objInsureFee->load();
				?>
				<input type="text" name="hInsureFeeCode" size="40" onchange="checkPrb();" value="<?=$objInsureFee->get_code();?>"><br>

			
			</td>
		</tr>
		<tr>

			<td colspan="2" class="listDetail" align="right" >
				<style type="text/css">
				<!--
				.listheader {
					color: #990000;
					background: #ffff99;
					height:24px;
					font-size: 14px;
				}
				.listdata {
					color: #000000;
					height:24px;
					font-size: 14px;
				}
				-->
				</style>

			
				<select name="hInsureFeeId" style="width:460px;height:25px" onchange="checkPrb();">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFeeList->getItemList() as $objItem) {
					$class="listdata";
					if(fmod($v,2) == 0) $class = "listheader";
				?>
					<option class="<?=$class?>" value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsure->get_p_car_type() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_use_type();?>-- <color=blue><?=$objItem->get_code();?></color> <font color=red>[<?=$objItem->get_total();?>]</font> <?=$objItem->get_title();?>, 
				<?$v++;}?>
				</select>
			
			</td>
		</tr>

		<tr>
			<td class="listDetail">��������â�� �ú.</td>
			<td class="listDetail">
				<input type="radio" value=4 name="hPFrees" <?if($objInsure->get_p_frees() == "4") echo "checked"?> >�١��ҫ��� &nbsp;&nbsp;
				<input type="radio" value=1 name="hPFrees" <?if($objInsure->get_p_frees() == "1") echo "checked"?>>Dealer ��&nbsp;&nbsp;
				<input type="radio" value=2 name="hPFrees" <?if($objInsure->get_p_frees() == "2") echo "checked"?>>TMT ��&nbsp;&nbsp;
				<input type="radio" value=3 name="hPFrees" <?if($objInsure->get_p_frees() == "3") echo "checked"?>>F/N ��&nbsp;&nbsp;
				<input type="radio" value=5 name="hPFrees" <?if($objInsure->get_p_frees() == "5") echo "checked"?>>��Сѹ�����
			</td>
		</tr>
	    <tr> 
	      <td  valign="right">1) ���� �ú. �ط�� </td>
	      <td  > 
	       	<input type="text" name="hPBeasuti"  style="text-align=right;"  onblur="tryNumberFormat(this);sumPrb();"  size="15" value="<?=number_format($objInsure->get_p_total(),2);?>">
	      </td>
	    </tr>
	    <tr> 
	      <td valign="top" class="listDetail">2) �ҡ� </td>
	      <td class="listDetail" > 
	       	<input type="text" name="hPArgon"  style="text-align=right;"   onblur="tryNumberFormat(this);sumPrb();"  size="15" value="<?=number_format($objInsure->get_p_argon(),2);?>">
	      </td>
	    </tr>		
	    <tr> 
	      <td  valign="top">���� �ú. �ط�� + �ҡ� </td>
	      <td  > 
	       	<input type="text" name="hPSum01" readonly  style="text-align=right;"   onblur="tryNumberFormat(this);"  size="15" value="<?=number_format($objInsure->get_k_num09(),2);?>">
	      </td>
	    </tr>
		<tr>
			<td valign="top" class="listDetail">�ѡ���� � ������ 1%  
				<input type="radio" value="�ؤ�Ÿ�����" name="hPNiti"  onclick="sumPNiti01();sumPrb();sumAll();"  <?if($objInsure->get_k_niti()=="�ؤ�Ÿ�����") echo "checked"?> >�ؤ�Ÿ�����
				<input type="radio" value="�ԵԺؤ�� �ѡ����" name="hPNiti" onclick="sumPNiti();sumPrb();sumAll();" <?if($objInsure->get_k_niti()=="�ԵԺؤ�� �ѡ����") echo "checked"?>>�ԵԺؤ�� �ѡ���� 
				<input type="radio" value="�ԵԺؤ�� ����ѡ����" name="hPNiti" onclick="sumPNiti01();sumPrb();sumAll();" <?if($objInsure->get_k_niti()=="�ԵԺؤ�� ����ѡ����") echo "checked"?>>�ԵԺؤ�� ����ѡ���� 
				</td>
			<td class="listDetail" valign="top"><input type="text" name="hPVat"  style="text-align=right;"  readonly size="15" value="<?=number_format($objInsure->get_p_vat(),2);?>"></td>
		</tr>	
		
	    <tr> 
	      <td valign="top">3) �������</td>
	      <td > 
	       	<input type="text" name="hPInsureVat"  style="text-align=right;"   onblur="tryNumberFormat(this);sumPrb();"  size="15" value="<?=number_format($objInsure->get_p_insure_vat(),2);?>">
	      </td>
	    </tr>


		<tr>
			<td class="listDetail">4) ������������ҡ�</td>
			<td class="listDetail"><input type="text" name="hPPrice" readonly style="text-align=right;" onblur="tryNumberFormat(this);"  size="15" value="<?=number_format($objInsure->get_p_prb_price(),2);?>"></td>
		</tr>
		<tr>
			<td class="listDetail01"><strong>������ѧ�ѡ � ������ 1%</strong></td>
			<td class="listDetail01"><input type="text" name="hPSum02" readonly style="text-align=right;" onblur="tryNumberFormat(this);"  size="15" value="<?=number_format($objInsure->get_k_num11() ,2);?>"></td>
		</tr>		
		</table>
		
		</td>
		</tr>		
	</table>

	
	
	<table width="100%" id="sale_k" <?if($objInsure->get_k_check() == 1){?> checked<?}else{?>style="display:none"<?}?>>
		<tr>
			<td bgcolor="#80bfff"  height="25" ><strong>�Ҥ��Ѥ��</strong></td>
		</tr>
		<tr>
			<td>
			
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>	
			<td class="listTitle">�á����</td>
			<td class="listTitle">����ѷ</td>
			<td class="listTitle">�ѹ����������</td>
			<td class="listTitle">�ѹ�������ش	</td>
		</tr>
		<tr >
			<td class="listDetail"><?$objBrokerList->printSelectScript("hKBrokerId",$objInsure->get_k_broker_id(),"��µç");?>	</td>
			<td class="listDetail"><?$objInsureCompanyList->printSelectScript("hKPrbId",$objInsure->get_k_prb_id(),"����к�");?>	
			<input type="hidden" name="hKStockId"  size="5" value="<?=$objInsure->get_k_stock_id();?>"><input type="hidden" name="hKNumber"  size="20" value="<?=$objInsure->get_k_number();?>"></td>
			<td class="listDetail">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKStartDay value="<?=$hKStartDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKStartMonth value="<?=$hKStartMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKStartYear" value="<?=$hKStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKStartYear,hKStartDay, hKStartMonth, hKStartYear,popCal);return false"></td>		
				</tr>
				</table>					
			
			</td>			
			<td class="listDetail"   >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=hKEndDay value="<?=$hKEndDay?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=hKEndMonth value="<?=$hKEndMonth?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hKEndYear" value="<?=$hKStartYear?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hKEndYear,hKEndDay, hKEndMonth, hKEndYear,popCal);return false"></td>		
				</tr>
				</table>			
			
			</td>		
		</tr>
		</table>
		
		
			</td>		
		</tr>
		<tr>
		<td>
		
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="listDetail" width="40%"  >��û�Сѹ���</td>
			<td  class="listDetail" >
				<input type="radio" value="��Сѹ�������" name="hKNew" <?if($objInsure->get_k_new() == "��Сѹ�������") echo "checked"?>>��Сѹ�������&nbsp;&nbsp;
				<input type="radio" value="�������" name="hKNew" <?if($objInsure->get_k_new() == "�������") echo "checked"?>>�������&nbsp;&nbsp;
			</td>
		</tr>		
		<tr>
			<td>��������Сѹ���</td>
			<td>
			<?
			$objInsureTypeList = new  InsureTypeList();
			$objInsureTypeList->setPageSize(0);
			$objInsureTypeList->setSort(" insure_type_id ASC ");
			$objInsureTypeList->load();			
			$objInsureTypeList->printSelect("hInsureType",$objInsure->get_k_type(),"��س����͡");
			?>
			<input type="text" name="hInsureTypeRemark"  size="10" value="<?=$objInsure->get_k_type_remark();?>">
			</td>
		</tr>		
		<tr>
			<td>�շ���ͻ�Сѹ</td>
			<td >
				<select name="hCarStatus">
					<option value="0">- ��س����͡ -
					<option value="�շ�� 1"  <?if($objInsure->get_p_year() == "�շ�� 1") echo "selected"?>>�շ�� 1 (����ᴧ)
					<option value="�շ�� 2" <?if($objInsure->get_p_year() == "�շ�� 2") echo "selected"?>>�շ�� 2
					<option value="�շ�� 3" <?if($objInsure->get_p_year() == "�շ�� 3") echo "selected"?>>�շ�� 3
					<option value="�շ�� 4" <?if($objInsure->get_p_year() == "�շ�� 4") echo "selected"?>>�շ�� 4
					<option value="�շ�� 5" <?if($objInsure->get_p_year() == "�շ�� 5") echo "selected"?>>�շ�� 5
					<option value="�շ�� 6" <?if($objInsure->get_p_year() == "�շ�� 6") echo "selected"?>>�շ�� 6
					<option value="�շ�� 7" <?if($objInsure->get_p_year() == "�շ�� 7") echo "selected"?>>�շ�� 7
					<option value="�շ�� 8" <?if($objInsure->get_p_year() == "�շ�� 8") echo "selected"?>>�շ�� 8
					<option value="�շ�� 9" <?if($objInsure->get_p_year() == "�շ�� 9") echo "selected"?>>�շ�� 9
					<option value="�շ�� 10" <?if($objInsure->get_p_year() == "�շ�� 10") echo "selected"?>>�շ�� 10
					<option value="�շ�� 1-5" <?if($objInsure->get_p_year() == "�շ�� 1-5") echo "selected"?>>�շ�� 1-5
					<option value="�շ�� 1-10" <?if($objInsure->get_p_year() == "�շ�� 1-10") echo "selected"?>>�շ�� 1-10
					<option value="�շ�� 1-15" <?if($objInsure->get_p_year() == "�շ�� 1-15") echo "selected"?>>�շ�� 1-15
					<option value="�ء��" <?if($objInsure->get_p_year() == "�ء��") echo "selected"?>>�ء��

					
				</select>
			</td>
		</tr>
		<tr>
			<td class="listDetail">���ʤ������</td>
			<td class="listDetail" >	
				<select name="hKCarType">
					<option value=0> - ��س����͡ -
				<?forEach($objInsureFee1List->getItemList() as $objItem) {?>
					<option value="<?=$objItem->get_insure_fee_id();?>" <?if($objInsure->get_k_car_type() == $objItem->get_insure_fee_id()) echo "selected"?>><?=$objItem->get_code();?>, <?=$objItem->get_title();?>
				<?}?>
				</select>
			
			</td>
		</tr>		
		<tr>
			<td >��������â��</td>
			<td >
				<input type="radio" value=4 name="hKFrees" <?if($objInsure->get_k_frees() == "4") echo "checked"?>>�١��ҫ��� &nbsp;&nbsp;
				<input type="radio" value=1 name="hKFrees" <?if($objInsure->get_k_frees() == "1") echo "checked"?>>Dealer ��&nbsp;&nbsp;
				<input type="radio" value=2 name="hKFrees" <?if($objInsure->get_k_frees() == "2") echo "checked"?>>TMT ��&nbsp;&nbsp;
				<input type="radio" value=3 name="hKFrees" <?if($objInsure->get_k_frees() == "3") echo "checked"?>>F/N ��&nbsp;&nbsp;
				<input type="radio" value=5 name="hKFrees" <?if($objInsure->get_k_frees() == "5") echo "checked"?>>��Сѹ�����
			</td>
		</tr>

		<tr>
			<td>��������ë���</td>
			<td><input type="radio" value="������ҧ" name="hRepair"  <?if($objInsure->get_k_fix()=="������ҧ") echo "checked"?>>������ҧ
					&nbsp;&nbsp;<input type="radio" value="�������" name="hRepair" <?if($objInsure->get_k_fix()=="�������") echo "checked"?>>�������
					&nbsp;&nbsp;<input type="radio" value="GOA" name="hRepair" <?if($objInsure->get_k_fix()=="GOA") echo "checked"?>>GOA
					
			</td>
		</tr>
		<tr>
			<td class="listDetail" >�ع��Сѹ</td>			
			<td class="listDetail" ><input type="text" name="hFund" style="text-align=right;"  onblur="tryNumberFormat(this);" size="15" value="<?=number_format($objInsure->get_k_num01(),2);?>"></td>
		</tr>
		<tr>
			<td>1) ���»�Сѹ����ط��</td>
			<td ><input type="text" name="hKBeasuti" style="text-align=right;"  onblur="tryNumberFormat(this);sumAll();"  size="15" value="<?=number_format($objInsure->get_k_num02(),2);?>"></td>
		</tr>
		<tr>
			<td class="listDetail">2) �ҡ�</td>
			<td class="listDetail"><input type="text" name="hKArgon" style="text-align=right;"  onblur="tryNumberFormat(this);sumAll();"   size="15" value="<?=number_format($objInsure->get_k_num03(),2);?>"></td>
		</tr>
		<tr>
			<td >���»�Сѹ����ط�� + �ҡ�</td>
			<td ><input type="text" name="hKSum01" style="text-align=right;"  onblur="tryNumberFormat(this);"   size="15" value="<?=number_format($objInsure->get_k_num06(),2);?>"></td>
		</tr>		
		<tr>
			<td class="listDetail" valign="top" >�ѡ���� � ������  
				<input type="radio" value="�ؤ�Ÿ�����" name="hKNiti"  onclick="sumKNiti01();sumAll();"  <?if($objInsure->get_k_niti()=="�ؤ�Ÿ�����") echo "checked"?> >�ؤ�Ÿ�����
				<input type="radio" value="�ԵԺؤ�� �ѡ����" name="hKNiti" onclick="sumKNiti();sumAll();" <?if($objInsure->get_k_niti()=="�ԵԺؤ�� �ѡ����") echo "checked"?>>�ԵԺؤ�� �ѡ���� 
				<input type="radio" value="�ԵԺؤ�� ����ѡ����" name="hKNiti" onclick="sumKNiti01();sumAll();" <?if($objInsure->get_k_niti()=="�ԵԺؤ�� ����ѡ����") echo "checked"?>>�ԵԺؤ�� ����ѡ���� 
			</td>
			<td class="listDetail" valign="top"><input type="text" name="hKVat"  style="text-align=right;"  readonly size="15" value="<?=number_format($objInsure->get_k_num07(),2);?>"></td>
		</tr>		
		<tr>
			<td >3) �������</td>
			<td ><input type="text" name="hKInsureVat" style="text-align=right;"  onblur="tryNumberFormat(this);sumAll();"    size="15" value="<?=number_format($objInsure->get_k_num04(),2)?>"></td>
		</tr>
		<tr>
			<td class="listDetail" >4) ������������ҡ�</td>
			<td class="listDetail" ><input type="text" name="hKPrice" readonly style="text-align=right;" onblur="tryNumberFormat(this);sumAll();"    size="15"     value="<?=number_format($objInsure->get_k_num05(),2);?>"></td>
		</tr>
		<tr>
			<td class="listDetail01">������ѧ�ѡ � ������ 1%</td>
			<td class="listDetail01"><input type="text" name="hKSum02" readonly style="text-align=right;" onblur="tryNumberFormat(this);sumAll();"    size="15"     value="<?=number_format( ($objInsure->get_k_num08()) ,2);?>"></td>
		</tr>		
		<tr>
			<td colspan="2"><hr size="1"></td>
		</tr>				
		<tr>
			<td >�����͹�ѡ��ǹŴ</td>
			<td ><input type="text" readonly name="hKSum03" readonly style="text-align=right;" onblur="tryNumberFormat(this);sumAll();"    size="15"     value="<?=number_format( ($objInsure->get_k_num12()) ,2);?>"></td>
		</tr>		
		<tr>			
			<td >��ǹŴ����� �к� <input type="text" name="hDiscountOtherRemark"  size="20" value="<?=$objInsure->get_k_dis_con_02();?>"></td>
			<td ><input type="text" style="text-align=right;" onblur="tryNumberFormat(this);sumAll();"  name="hDiscountOtherPrice"  size="15" value="<?=number_format($objInsure->get_k_num10(),2);?>"></td>
		</tr>
		<tr>		
		<tr>
			<td bgcolor="#FF4242" >�������ͧ���з�����</td>
			<td bgcolor="#FF4242"  ><input type="text" name="hTotal" readonly style="text-align=right;"  size="15" value="<?=number_format($objInsure->get_k_num13(),2);?>"></td>
		</tr>	
		</table>

		
		</td>
		</tr>		
	</table>




<input type="hidden" name="hSaveForm" value="Yes">		
<input type="hidden" name="hCarId" value="<?=$hCarId?>">
<input type="hidden" name="hYearExtend" value="<?=$hYearExtend?>">
<input type="hidden" name="hSubmit" value="Yes">
<input type="hidden" name="hInsureId" value="<?=$hInsureId?>">
		 

	


<br>
<table width="100%" cellpadding="3" cellspacing="0" align="center">
<tr>
	<td class="i_background">�����š�ê��Ф�һ�Сѹ��� ���з����� 
		<select name="hPaymentAmount"  onchange="checkDisplay(this.value)" >
			<option value="0">��س����͡
			<?
			$objInsureItem = new InsureItem();
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=1 AND payment_status='��������' ")){
			?>
			<option value="1" <?if($objInsure->get_pay_number() == "1") echo "selected";?>>1
		<?}?>
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="2" <?if($objInsure->get_pay_number() == "2") echo "selected";?>>2
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="3" <?if($objInsure->get_pay_number() == "3") echo "selected";?>>3
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="4" <?if($objInsure->get_pay_number() == "4") echo "selected";?>>4
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="5" <?if($objInsure->get_pay_number() == "5") echo "selected";?>>5
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="6" <?if($objInsure->get_pay_number() == "6") echo "selected";?>>6
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="7" <?if($objInsure->get_pay_number() == "7") echo "selected";?>>7
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="8" <?if($objInsure->get_pay_number() == "8") echo "selected";?>>8
		<?}?>			
			<?
			if(!$objInsureItem->loadByCondition(" insure_id = $hInsureId AND payment_order=2 AND payment_status='��������' ")){
			?>
			<option value="9" <?if($objInsure->get_pay_number() == "9") echo "selected";?>>9
		<?}?>			

		</select>	
	  �Ǵ
	  &nbsp;&nbsp;&nbsp;<strong>���͹䢡�â��</strong>
	  <?=$objInsure->get_pay_type()?>
	  </td>
</tr>
</table>


<table width="100%" cellpadding="3" cellspacing="1" class="i_background02" border="0" align="center">
<?for($i=1;$i<=9;$i++){?>
<?if($i > $objInsure->get_pay_number()){
	$sum_acc=0;
	$sum_remain=0;
?>

<input type="hidden" name="hInsureItemId_<?=$i?>" value="">
<input type="hidden" name="hPaymentStatus_<?=$i?>" value="">
<tr id="var_<?=$i?>_1" >
	<td class="listDetail" colspan="4"><strong>��������´����Ѻ���Ф��駷�� <?=$i?></strong></td>
	<td class="listDetail" align="right" colspan="2">�ѹ���Ѵ���� :</td>
	<td class="listDetail" align="center">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name="hDay_<?=$i?>" value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name="hMonth_<?=$i?>" value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hYear_<?=$i?>" value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear_<?=$i?>,hDay_<?=$i?>, hMonth_<?=$i?>, hYear_<?=$i?>,popCal);return false"></td>		
		</tr>
		</table>	
	</td>		
</tr>
<tr id="var_<?=$i?>_2" >
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle" >�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle" >�ӹǹ�Թ<br>����ͧ����</td>
	<td align="center" class="listTitle" ></td>
	<td align="center" class="listTitle" ></td>
</tr>
<?for($j=1;$j<=5;$j++){?>
<tr id="bar_<?=$i?>_<?=$j?>" >
	<td align="center" class="ListDetail"><?=$j?></td>
	<td align="center class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$i."_".$j,"","��س����͡");?><input type="text" size="20" name="hRemark_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$i?>)"  size="5" id="hQty_<?=$i?>_<?=$j?>" name="hQty_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$i?>)"  id="hPayin_<?=$i?>_<?=$j?>" name="hPayin_<?=$i?>_<?=$j?>" value="1"  ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  size="10"  id="hPrice_<?=$i?>_<?=$j?>"  name="hPrice_<?=$i?>_<?=$j?>" value=""></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$i?>_<?=$j?>" name="hPriceSum_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"></td>
	<td align="center" class="ListDetail"></td>
	
</tr>	
<?}//end for 5?>
<tr id="var_<?=$i?>_3" >
	<td class="listTitle" colspan="4">�����˵� : <input type="text" id="hInsureRemark_<?=$i?>" name="hInsureRemark_<?=$i?>"  size="50" value=""></td>
	<td class="listTitle" align="right">������Ф��駷�� <?=$i?> :</td>
	<td class="listTitle" align="center"><input type="text" style="text-align=right;"  id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" readonly  size="10" value=""></td>		
	<td align="center" class="ListTitle"></td>
	<td align="center" class="ListTitle"></td>
</tr>


<?}else{?>
<?
$objIT = new InsureItem();
$objIT->loadByCondition(" insure_id = $hInsureId and payment_order = $i ");

$sum_acc=0;
$sum_remain=0;
if($objIT->get_payment_status() == "��������"){

?>
<input type="hidden" name="hInsureItemId_<?=$i?>" value="<?=$objIT->get_insure_item_id()?>">
<input type="hidden" name="hPaymentStatus_<?=$i?>" value="<?=$objIT->get_payment_status()?>">
<tr id="var_<?=$i?>_1" >
	<td class="listDetail" colspan="5"><strong>��������´����Ѻ���Ф��駷�� <?=$i?></strong><a name="no<?=$i?>"></a></td>
	<td class="listDetail" align="right" colspan="2">�ѹ���Ѵ���� :</td>
	<td class="listDetail" align="center">
		<?=$objIT->get_payment_date();
		$arrDate = explode("-",$objIT->get_payment_date());
		?>
		<INPUT type="hidden" align="middle" size="2" maxlength="2"   name="hDay_<?=$i?>" value="<?=$arrDate[2]?>">
		<INPUT type="hidden" align="middle" size="2" maxlength="2"   name="hMonth_<?=$i?>" value="<?=$arrDate[1]?>">
		<INPUT type="hidden" align="middle" size="2" maxlength="2"   name="hYear_<?=$i?>" value="<?=$arrDate[0]?>">
	</td>		
</tr>
<tr id="var_<?=$i?>_4" >
	<td class="listDetail" colspan="5">�Ţ�������� : <?=$objIT->get_payment_no()?></td>
	<td class="listDetail" align="right" colspan="2">�ѹ������ :</td>
	<td class="listDetail" align="center">
		<?=$objIT->get_insure_date()?>
	</td>		
	
</tr>
<tr id="var_<?=$i?>_2">
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle" >�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle" >�ӹǹ�Թ<br>�����Ш�ԧ</td>
	<td align="center" class="listTitle" >�ӹǹ�Թ<br>����ͧ����</td>
	<td align="center" class="listTitle" >����ҧ</td>
</tr>
<?
$objITList = new InsureItemDetailList();
$objITList->setFilter(" OP.order_id =".$objIT->get_insure_item_id()." and OP.order_extra_id= $hInsureId ");
$objITList->setPageSize(0);
$objITList->setSort(" order_price_id ASC ");
$objITList->load();
$j=1;
forEach($objITList->getItemList() as $objItem) {
?>
<tr  id="bar_<?=$i?>_<?=$j?>">
	<td align="center" class="ListDetail"><?=$j?></td>
	<td align="center class="ListDetail"><?=$objItem->getPaymentSubjectTitle();?> <?=$objItem->getRemark()?></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" readonly size="5" id="hQty_<?=$i?>_<?=$j?>" name="hQty_<?=$i?>_<?=$j?>" value="<?=$objItem->getQty()?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox" readonly id="hPayin_<?=$i?>_<?=$j?>" name="hPayin_<?=$i?>_<?=$j?>" value="1"  <?if($objItem->getPayin() == 1) echo "checked";?> ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  readonly  size="10"  id="hPrice_<?=$i?>_<?=$j?>"  name="hPrice_<?=$i?>_<?=$j?>" value="<?=number_format($objItem->getPrice(),2)?>"></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$i?>_<?=$j?>" name="hPriceSum_<?=$i?>_<?=$j?>" value="<?=number_format(($objItem->getPriceAcc()*$objItem->getQty()),2)?>"></td>
	<td align="center" class="ListDetail"><?=number_format( ($objItem->getPrice()*$objItem->getQty()),2)?></td>
	<td align="center" class="ListDetail"><?=number_format((($objItem->getPrice()-$objItem->getPriceAcc())*$objItem->getQty()),2)?></td>
</tr>	
<?
$sum_acc=$sum_acc+($objItem->getPriceAcc()*$objItem->getQty());
$sum_remain = $sum_remain+( ($objItem->getPrice()-$objItem->getPriceAcc())*$objItem->getQty() );

$sum_acc_total = $sum_acc_total+$sum_acc;
$sum_remain_total  = $sum_remain_total+$sum_remain;

$j++;}//end for each?>
<?for($x=$j;$x<=5;$x++){?>
<tr    id="bar_<?=$i?>_<?=$x?>">
	<td align="center" class="ListDetail" colspan="8">&nbsp;</td>
</tr>	
<?}?>
<tr id="var_<?=$i?>_3" >
	<td class="listTitle" colspan="4">�����˵� : <?=$objIT->get_remark();?></td>
	<td class="listTitle" align="right">������Ф��駷�� <?=$i?> :</td>
	<td class="listTitle" align="center"><input type="text" readonly style="text-align=right;"  id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" size="10" value="<?=number_format($sum_acc,2)?>"></td>		
	<td align="center" class="ListTitle"><?=number_format($objIT->get_payment_price(),2);?></td>
	<td align="center" class="ListTitle"><?=number_format($sum_remain,2)?></td>
</tr>

<?}else{?>
<?
$arrDate01 = explode("-",$objIT->get_payment_date());
$Day01=$arrDate01[2];
$Month01=$arrDate01[1];
$Year01=$arrDate01[0];
?>
<input type="hidden" name="hInsureItemId_<?=$i?>" value="<?=$objIT->get_insure_item_id()?>">
<input type="hidden" name="hPaymentStatus_<?=$i?>" value="<?=$objIT->get_payment_status()?>">
<tr id="var_<?=$i?>_1" >
	<td class="listDetail" colspan="4"><strong>��������´����Ѻ���Ф��駷�� <?=$i?></strong><a name="no<?=$i?>"></a></td>
	<td class="listDetail" align="right" colspan="2">�ѹ���Ѵ���� :</td>
	<td class="listDetail" align="center">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name="hDay_<?=$i?>" value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name="hMonth_<?=$i?>" value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name="hYear_<?=$i?>" value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear_<?=$i?>,hDay_<?=$i?>, hMonth_<?=$i?>, hYear_<?=$i?>,popCal);return false"></td>		
		</tr>
		</table>	
	</td>		
</tr>
<tr id="var_<?=$i?>_2" >
	<td align="center"  class="listTitle">�ӴѺ</td>
	<td align="center"  class="listTitle">��¡��</td>
	<td align="center"  class="listTitle" >�ӹǹ</td>
	<td align="center" class="listTitle">�Դź</td>
	<td align="center" class="listTitle">˹�����</td>
	<td align="center" class="listTitle" >�ӹǹ�Թ<br>����ͧ����</td>
	<td align="center" class="listTitle" ></td>
	<td align="center" class="listTitle" ></td>	
</tr>
<?
$objITList = new InsureItemDetailList();
$objITList->setFilter(" OP.order_id ='".$objIT->get_insure_item_id()."' and OP.order_extra_id= $hInsureId ");
$objITList->setPageSize(0);
$objITList->setSort(" order_price_id ASC ");
$objITList->load();
$j=1;
forEach($objITList->getItemList() as $objItem) {
?>
<input type="hidden" name="hOrderPriceId_<?=$i?>_<?$j?>" value="<?=$objItem->getOrderPriceId()?>">
<tr id="bar_<?=$i?>_<?=$j?>" >
	<td align="center" class="ListDetail"><?=$j?></td>
	<td align="center class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$i."_".$j,$objItem->getPaymentSubjectId(),"��س����͡");?><input type="text" size="20" name="hRemark_<?=$i?>_<?=$j?>" value="<?=$objItem->getRemark()?>"></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$i?>)"  size="5" id="hQty_<?=$i?>_<?=$j?>" name="hQty_<?=$i?>_<?=$j?>" value="<?=$objItem->getQty()?>"></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$i?>)"  id="hPayin_<?=$i?>_<?=$j?>" name="hPayin_<?=$i?>_<?=$j?>" value="1"  <?if($objItem->getPayin() == 1) echo "checked";?> ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  size="10"  id="hPrice_<?=$i?>_<?=$j?>"  name="hPrice_<?=$i?>_<?=$j?>" value="<?=number_format($objItem->getPrice(),2)?>"></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$i?>_<?=$j?>" name="hPriceSum_<?=$i?>_<?=$j?>" value="<?=number_format( ($objItem->getPrice()*$objItem->getQty()),2)?>"></td>
	<td align="center" class="ListDetail"></td>
	<td align="center" class="ListDetail"></td>
</tr>	

<?$j++;}//end for each?>
<?
$m=$j;
for($j=$m;$j<=5;$j++){?>
<tr id="bar_<?=$i?>_<?=$j?>" >
	<td align="center" class="ListDetail"><?=$j?></td>
	<td align="center class="ListDetail"><?=$objPaymentSubjectList->printSelect("hPaymentSubjectId_".$i."_".$j,"","��س����͡");?><input type="text" size="20" name="hRemark_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;" onblur="sum_var(<?=$i?>)"  size="5" id="hQty_<?=$i?>_<?=$j?>" name="hQty_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"><input type="checkbox"  onblur="sum_var(<?=$i?>)"  id="hPayin_<?=$i?>_<?=$j?>" name="hPayin_<?=$i?>_<?=$j?>" value="1"  ></td>
	<td align="center" class="ListDetail"><input type="text" style="text-align=right;"  onblur="sum_var(<?=$i?>);tryNumberFormat(this);"  size="10"  id="hPrice_<?=$i?>_<?=$j?>"  name="hPrice_<?=$i?>_<?=$j?>" value=""></td>	
	<td align="center" class="ListDetail"><input type="text" readonly style="text-align=right;"   size="10" id="hPriceSum_<?=$i?>_<?=$j?>" name="hPriceSum_<?=$i?>_<?=$j?>" value=""></td>
	<td align="center" class="ListDetail"></td>
	<td align="center" class="ListDetail"></td>
</tr>	
<?}//end for 5?>
<tr id="var_<?=$i?>_3" >
	<td class="listTitle" colspan="4">�����˵� : <input type="text" id="hInsureRemark_<?=$i?>" name="hInsureRemark_<?=$i?>"  size="50" value="<?=$objIT->get_remark()?>"></td>
	<td class="listTitle" align="right">������Ф��駷�� <?=$i?> :</td>
	<td class="listTitle" align="center"><input type="text" readonly style="text-align=right;"  id="hPaymentPrice_<?=$i?>" name="hPaymentPrice_<?=$i?>" size="10" value="<?=number_format($objIT->get_payment_price(),2);?>"></td>		
	<td align="center" class="ListTitle"></td>
	<td align="center" class="ListTitle"></td>
</tr>
<tr>
	<td colspan="8" bgcolor="#D2FB95">
	
	
		<table class="search" cellpadding="1" cellspacing="1" width="100%" border=0>
		<tr>
			<td class="listTitle" align="center">��������ê���</td>
			<td class="listTitle" align="center">�ѭ�� Buzz</td>
			<td class="listTitle" align="center">�͹�ҡ��Ҥ��</td>
			<td class="listTitle" align="center">�͹�ҡ�Ң�</td>
			<td class="listTitle" align="center">�����Ţ��<br>/��Ի ATM</td>
			<td class="listTitle" align="center">�ӹǹ�Թ</td>
			<td class="listTitle" align="center">�ѹ���</td>
			<td class="listTitle" align="center">�����˵�</td>
		</tr>
		<?for($x=1;$x<=5;$x++){
			$objInsurePayment =  new InsurePayment();
			$objInsurePayment->loadByCondition("  order_id= ".$objIT->get_insure_item_id()."  and order_extra_id = $hInsureId and payment_no= $x   ");
		?>
			<input type="hidden" name="hInsurePaymentId_<?=$i?>_<?=$x?>" value="<?=$objInsurePayment->getInsurePaymentId()?>">
			<input type="hidden" name="hPaymentNo_<?=$i?>_<?=$x?>" value="<?=$x?>">
		<tr>
			<td ><?$objPaymentTypeList->printSelect("hPaymentType_".$i."_".$x,$objInsurePayment->getPaymentTypeId(),"���͡��������ê���");?></td>
			<td ><?$objBookBankList->printSelect("hPaymentBookBank_".$i."_".$x,$objInsurePayment->getBookBankId(),"���͡�ѭ�� Buzz");?></td>
			<td ><?$objBankList->printSelect("hPaymentBank_".$i."_".$x,$objInsurePayment->getBankId(),"���͡��¡�ø�Ҥ��");?></td>
			<td align="center"><input type="text" name="hPaymentBranch_<?=$i?>_<?=$x?>" size="10"  value="<?=$objInsurePayment->getBranch()?>"></td>
			<td align="center"><input type="text" name="hPaymentCheckNo_<?=$i?>_<?=$x?>" size="10"  value="<?=$objInsurePayment->getCheckNo()?>"></td>
			<td><input type="text" style="text-align=right;"  onblur="tryNumberFormat(this);"   name="hPaymentPrice_<?=$i?>_<?=$x?>" size="15"  value="<?=number_format($objInsurePayment->getPrice(),2)?>"></td>
			<td>
			<?$arrTDate = explode("-",$objInsurePayment->getCheckDate())?>
		  	<table cellspacing="0" cellpadding="0">
			<tr>
				<td><INPUT align="middle" size="2" maxlength="2"   name=Day01_<?=$i?>_<?=$x?>  value="<?=$arrTDate[2]?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=Month01_<?=$i?>_<?=$x?> value="<?=$arrTDate[1]?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01_<?=$i?>_<?=$x?> value="<?=$arrTDate[0]?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01_<?=$i?>_<?=$x?>,Day01_<?=$i?>_<?=$x?>, Month01_<?=$i?>_<?=$x?>, Year01_<?=$i?>_<?=$x?>, popCal);return false"></td>			
			</tr>
			</table>
			</td>
			<td><input type="text" name="hPaymentRemark_<?=$i?>_<?=$x?>" size="20"  value="<?=$objInsurePayment->getRemark()?>"></td>
		</tr>
		<?}?>

		</table>

	
	</td>
</tr>
<?
$objITList = new InsureItemTrackList();
$objITList->setFilter(" insure_item_id = ".$objIT->get_insure_item_id()."   ");
$objITList->setPageSize(0);
$objITList->setSort(" track_date ASC  ");
$objITList->load();
if($objITList->mCount > 0){?>
<tr>
	<td colspan="8" bgcolor="#d3d3d3">
		��õԴ�����ê����Թ

		<tr>
			<td bgcolor="<?=$bgColor?>">&nbsp;</td>
			<td  align="center" class="ListTitle">�ѹ���Դ���</td>
			<td  align="center" class="ListTitle">ʶҹС�õԴ���</td>
			<td  align="center" class="ListTitle">�ѹ�������͹�Ѵ</td>
			<td  colspan="2" align="center" class="ListTitle">�����˵�</td>

		</tr>
		<?
		forEach($objITList->getItemList() as $objItemTrack) {
		?>
		<tr>
			<td bgcolor="<?=$bgColor?>">&nbsp;</td>
			<td valign="top" align="center"><?=formatShortDate($objItemTrack->get_track_date());?></td>
			<td valign="top" align="center"><?=$objItemTrack->get_track_status();?></td>
			<td valign="top" align="center"><?if($objItemTrack->get_track_status() == "����͹�ѹ����") echo formatShortDate($objItemTrack->get_track_new_date());?></td>
			<td valign="top" colspan="2"><?=$objItemTrack->get_track_remark();?></td>

		</tr>
		<?}?>



	
	</td>
</tr>
<?}?>

<?}//end check ��������?>


<?}//end check �ҡ���Ҩӹǹ�Ǵ����˹����?>
<?}//end for 9?>
<tr id="var_sum"   >
	<td align="right" bgcolor="#F16758" colspan="5">��������� :</td>
	<td align="center" bgcolor="#F16758"><input type="text" name="hInsurePrice" style="text-align=right;"  onblur="checkSum();"  size="10" value=""><input type="hidden" name="hTotal" style="text-align=right;"  value="<?=number_format($objInsure->get_k_num13(),2);?>"></td>
	<td align="center" bgcolor="#F16758" colspan="2">&nbsp;</td>
</tr>
</table>
<br><br>
<div align="center"><input type="button" value="�ѹ�֡������" name="hCheck" onclick="return checkSubmit();"></div>


		
		
		</form>

	
<?}else{?>	
		<table class="search" width="100%">
		<tr>
			<td height="300" align="center" valign="middle">
				<h5 class="error">��س����͡��¡�÷���ͧ��úѹ�֡�ҡ��ҹ�������</h5>
				
			</td>
		</tr>
		</table>
<?}?>

	
	</td>
</tr>
</table>

<?
	include("h_footer.php")
?>

<script>
	new CAPXOUS.AutoComplete("hOfficer", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&hField=hOfficerId&q=" + this.text.value;
		}
	});		

	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_sale.php?hFrm=frm01&q=" + this.text.value;
		}
	});		
	
	new CAPXOUS.AutoComplete("hInsureFeeCode", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "auto_fee.php?hFrm=frm01&q=" + this.text.value;
		}
	});			


</script>

<script>
	function checkSubmit(){

		if (document.forms.frm01.hInsurePrice01.value != document.forms.frm01.hTotal.value)
		{
			alert("������������ҡѺ�ӹǹ�Թ�������ЧǴ");
			return false;
		}
	
	
		if(document.frm01.hSale01.checked== true){
		//�Ҥ�ѧ�Ѻ
		

			
			if (document.forms.frm01.hPStartDay.value=="" || document.forms.frm01.hPStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hPStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartDay.value,1,31) == false) {
					document.forms.frm01.hPStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPStartMonth.value==""  || document.forms.frm01.hPStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hPStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartMonth.value,1,12) == false){
					document.forms.frm01.hPStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPStartYear.value==""  || document.forms.frm01.hPStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hPStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hPEndDay.value=="" || document.forms.frm01.hPEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hPEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndDay.value,1,31) == false) {
					document.forms.frm01.hPEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hPEndMonth.value==""  || document.forms.frm01.hPEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hPEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndMonth.value,1,12) == false){
					document.forms.frm01.hPEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hPEndYear.value==""  || document.forms.frm01.hPEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hPEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hPEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hPEndYear.focus();
					return false;
				}
			} 					
		
		
			if(document.frm01.hInsureFeeId.value ==0){
				alert("��س����͡������ö��Т�Ҵö¹��");
				document.frm01.hInsureFeeId.focus();
				return false;
			}	
			
			if(document.frm01.hPFrees[0].checked==false && document.frm01.hPFrees[1].checked==false && document.frm01.hPFrees[2].checked==false && document.frm01.hPFrees[3].checked==false){
				alert("��س����͡��������â��");
				document.frm01.hPFrees[0].focus();
				return false;
			}	
			
			if(document.frm01.hCarStatus.value ==0){
				alert("��س����͡�շ���ͻ�Сѹ");
				document.frm01.hCarStatus.focus();
				return false;
			}			
		
				if(document.frm01.hPInsureVat.value <= 0){
					alert("��س��к�������Ť������");
					document.frm01.hPInsureVat.focus();
					return false;
				}		
			
				if(document.frm01.hPBeasuti.value <=0){
					alert("��س��к������ط��");
					document.frm01.hPBeasuti.focus();
					return false;
				}		
			
				if(document.frm01.hPArgon.value <=0){
					alert("��س��к��ҡ�");
					document.frm01.hPArgon.focus();
					return false;
				}	
		
				if(document.frm01.hPPrice.value <=0){
					alert("��س��к��Ҥ� �ú.");
					document.frm01.hPrbPrice.focus();
					return false;
				}	
		

		
		}
		
		if(document.frm01.hSale02.checked==true){
			//�Ҥ��Ѥ��
			if(document.frm01.hKPrbId.value == "0"){
				alert("��س��кغ���ѷ��Сѹ���");
				document.frm01.hKPrbId.focus();
				return false;
			}
		
			if (document.forms.frm01.hKStartDay.value=="" || document.forms.frm01.hKStartDay.value=="00")
			{
				alert("��س��к��ѹ�������");
				document.forms.frm01.hKStartDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartDay.value,1,31) == false) {
					document.forms.frm01.hKStartDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKStartMonth.value==""  || document.forms.frm01.hKStartMonth.value=="00")
			{
				alert("��س��к���͹�������");
				document.forms.frm01.hKStartMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartMonth.value,1,12) == false){
					document.forms.frm01.hKStartMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKStartYear.value==""  || document.forms.frm01.hKStartYear.value=="0000")
			{
				alert("��س��кػ��������");
				document.forms.frm01.hKStartYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKStartYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKStartYear.focus();
					return false;
				}
			} 			

			if (document.forms.frm01.hKEndDay.value=="" || document.forms.frm01.hKEndDay.value=="00")
			{
				alert("��س��к��ѹ����ش");
				document.forms.frm01.hKEndDay.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndDay.value,1,31) == false) {
					document.forms.frm01.hKEndDay.focus();
					return false;
				}
			} 		
		
			if (document.forms.frm01.hKEndMonth.value==""  || document.forms.frm01.hKEndMonth.value=="00")
			{
				alert("��س��к���͹����ش");
				document.forms.frm01.hKEndMonth.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndMonth.value,1,12) == false){
					document.forms.frm01.hKEndMonth.focus();
					return false;
				}
			} 			
			
			if (document.forms.frm01.hKEndYear.value==""  || document.forms.frm01.hKEndYear.value=="0000")
			{
				alert("��س��кػ�����ش");
				document.forms.frm01.hKEndYear.focus();
				return false;
			}else{
				if(checkNum(document.forms.frm01.hKEndYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					document.forms.frm01.hKEndYear.focus();
					return false;
				}
			} 				
			
			if(document.frm01.hKNew[0].checked==false && document.frm01.hKNew[1].checked==false ){
				alert("��س����͡��û�Сѹ���");
				document.frm01.hKNew[0].focus();
				return false;
			}				
			
			if(document.frm01.hCarStatus.value ==0 ){
				alert("��س����͡�շ���ͻ�Сѹ");
				document.frm01.hCarStatus.focus();
				return false;
			}			
			
			if(document.frm01.hKCarType.value ==0 ){
				alert("��س����͡���ʤ������");
				document.frm01.hKCarType.focus();
				return false;
			}						
		
			if(document.frm01.hKFrees[0].checked==false && document.frm01.hKFrees[1].checked==false && document.frm01.hKFrees[2].checked==false && document.frm01.hKFrees[3].checked==false){
				alert("��س����͡��������â��");
				document.frm01.hKFrees[0].focus();
				return false;
			}	
		
				if(document.frm01.hInsureType.value == 0 ){
					alert("��س����͡��������Сѹ���");
					document.frm01.hInsureType.focus();
					return false;		
				}
				
				if(document.frm01.hRepair[0].checked == false && document.frm01.hRepair[1].checked == false  && document.frm01.hRepair[2].checked == false ){
					alert("��س����͡��������ë���");
					document.frm01.hRepair[0].focus();
					return false;		
				}			
		
				if(document.frm01.hFund.value <= 0){
					alert("��س��кطع��Сѹ");
					document.frm01.hFund.focus();
					return false;
				}		
			
				if(document.frm01.hKBeasuti.value <=0){
					alert("��س��к������ط��");
					document.frm01.hKBeasuti.focus();
					return false;
				}		
			
				if(document.frm01.hKArgon.value <=0){
					alert("��س��к��ҡ�");
					document.frm01.hKArgon.focus();
					return false;
				}			
			
				if(document.frm01.hKNiti[0].checked == false && document.frm01.hKNiti[1].checked == false && document.frm01.hKNiti[2].checked == false ){
					alert("��س����͡������������");
					document.frm01.hNiti[0].focus();
					return false;		
				}
				
				if(document.frm01.hTotal.value <=0){
					alert("��������դ����ҧ");
					document.frm01.hTotal.focus();
					return false;
				}			
				

				
		
		
		
		}
		
		day="";
		month="";
		year="";
		num_val = document.frm01.hPaymentAmount.value;
		for(v=1;v<=num_val;v++){
			eval("day = document.frm01.hDay_"+v+".value");
			eval("month = document.frm01.hMonth_"+v+".value");
			eval("year = document.frm01.hYear_"+v+".value");
			
			if (day=="" || day =="00")
			{
				alert("��س��к��ѹ�Ѵ���ЧǴ��� "+v);
				return false;
			}else{
				if(checkNum(day,1,31) == false) {
					alert("��س��к��ѹ�Ѵ���ЧǴ��� "+v);
					return false;
				}
			} 		
		
			if (month==""  || month=="00")
			{
				alert("��س��к���͹�Ѵ���ЧǴ��� "+v);				
				return false;
			}else{
				if(checkNum(month,1,12) == false){
					alert("��س��к���͹�Ѵ���ЧǴ��� "+v);			
					return false;
				}
			} 			
			
			if (year==""  || year=="0000")
			{
				alert("��س��кػչѴ���ЧǴ��� "+v);				
				return false;
			}else{
				if(checkNum(year,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
					alert("��س��кػչѴ���ЧǴ��� "+v);
					return false;
				}
			} 			

			
			eval('price=document.frm01.hPaymentPrice_'+v+'.value');
			if (price==""  || price < "0.00"){
					alert("��س��кبӹǹ�Թ���ЧǴ��� "+v);
					return false;
			}
			
		}
		
		
		if(confirm("�س�׹�ѹ���кѹ�֡��¡�õ������к�������� ?")){
		
			document.frm01.hTotal01.disabled = false;
			document.frm01.hTotal02.disabled = false;
			document.frm01.hTotal.disabled = false;
		
			document.frm01.submit();
			return true;
		}else{
			return false;			
		}
		
	}
		

	function checkDisplay(var_num){

			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "none";
			document.getElementById("var_<?=$i?>_2").style.display = "none";
			document.getElementById("var_<?=$i?>_3").style.display = "none";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "none";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "none";
			document.getElementById("var_sum01").style.display = "none";
			document.getElementById("var_sum02").style.display = "none";

		if(var_num ==1){
			<?for($i=1;$i<=1;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
			document.getElementById("var_sum01").style.display = "";
			document.getElementById("var_sum02").style.display = "";
		}
		if(var_num ==2){
			<?for($i=1;$i<=2;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
			document.getElementById("var_sum01").style.display = "";
			document.getElementById("var_sum02").style.display = "";
		}
		if(var_num ==3){
			<?for($i=1;$i<=3;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
			document.getElementById("var_sum01").style.display = "";
			document.getElementById("var_sum02").style.display = "";
		}
		if(var_num ==4){
			<?for($i=1;$i<=4;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==5){
			<?for($i=1;$i<=5;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==6){
			<?for($i=1;$i<=6;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";		
		}
		if(var_num ==7){
			<?for($i=1;$i<=7;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==8){
			<?for($i=1;$i<=8;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num ==9){
			<?for($i=1;$i<=9;$i++){?>
			document.getElementById("var_<?=$i?>_1").style.display = "";
			document.getElementById("var_<?=$i?>_2").style.display = "";
			document.getElementById("var_<?=$i?>_3").style.display = "";
				<?for($j=1;$j<=5;$j++){?>
				document.getElementById("bar_<?=$i?>_<?=$j?>").style.display = "";
				<?}?>
			<?}?>
			document.getElementById("var_sum").style.display = "";
		}
		if(var_num > 0){
			document.getElementById("var_sum").style.display = "";
			document.getElementById("var_sum01").style.display = "";
			document.getElementById("var_sum02").style.display = "";
		}else{
			document.getElementById("var_sum").style.display = "none";
			document.getElementById("var_sum01").style.display = "none";
			document.getElementById("var_sum02").style.display = "none";
		}

	}
	
	function checkSum(){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		num06=0;
		num07=0;
		num08=0;
		num09=0;				
		if(document.frm01.hPaymentPrice_1){	
			num01 = rip_comma(document.frm01.hPaymentPrice_1.value)*1;	
			num0101 = rip_comma(document.frm01.hPaymentPrice01_1.value)*1;	
			num0102 = rip_comma(document.frm01.hPaymentPrice02_1.value)*1;	
		}
		if(document.frm01.hPaymentPrice_2){	
			num02 = rip_comma(document.frm01.hPaymentPrice_2.value)*1;	
			num0201 = rip_comma(document.frm01.hPaymentPrice01_2.value)*1;	
			num0202 = rip_comma(document.frm01.hPaymentPrice02_2.value)*1;	
		}
		if(document.frm01.hPaymentPrice_3){
			num03 = rip_comma(document.frm01.hPaymentPrice_3.value)*1;	
			num0301 = rip_comma(document.frm01.hPaymentPrice01_3.value)*1;	
			num0302 = rip_comma(document.frm01.hPaymentPrice02_3.value)*1;	
		}
		if(document.frm01.hPaymentPrice_4){	
			num04 = rip_comma(document.frm01.hPaymentPrice_4.value)*1;	
			num0401 = rip_comma(document.frm01.hPaymentPrice01_4.value)*1;	
			num0402 = rip_comma(document.frm01.hPaymentPrice02_4.value)*1;	
		}
		if(document.frm01.hPaymentPrice_5){
			num05 = rip_comma(document.frm01.hPaymentPrice_5.value)*1;	
			num0501 = rip_comma(document.frm01.hPaymentPrice01_5.value)*1;	
			num0502 = rip_comma(document.frm01.hPaymentPrice02_5.value)*1;	
		}
		if(document.frm01.hPaymentPrice_6){
			num06 = rip_comma(document.frm01.hPaymentPrice_6.value)*1;	
			num0601 = rip_comma(document.frm01.hPaymentPrice01_6.value)*1;	
			num0602 = rip_comma(document.frm01.hPaymentPrice02_6.value)*1;	
		}
		if(document.frm01.hPaymentPrice_7){
			num07 = rip_comma(document.frm01.hPaymentPrice_7.value)*1;	
			num0701 = rip_comma(document.frm01.hPaymentPrice01_7.value)*1;	
			num0702 = rip_comma(document.frm01.hPaymentPrice02_7.value)*1;	
		}
		if(document.frm01.hPaymentPrice_8){
			num08 = rip_comma(document.frm01.hPaymentPrice_8.value)*1;	
			num0801 = rip_comma(document.frm01.hPaymentPrice01_8.value)*1;	
			num0802 = rip_comma(document.frm01.hPaymentPrice02_8.value)*1;	
		}
		if(document.frm01.hPaymentPrice_9){	
			num09 = rip_comma(document.frm01.hPaymentPrice_9.value)*1;	
			num0901 = rip_comma(document.frm01.hPaymentPrice01_9.value)*1;	
			num0902 = rip_comma(document.frm01.hPaymentPrice02_9.value)*1;	
		}														
				
		sumnum = num01+num02+num03+num04+num05+num06+num07+num08+num09;
		document.frm01.hInsurePrice.value=formatCurrency(sumnum);
		
		sumnum01 = num0101+num0201+num0301+num0401+num0501+num0601+num0701+num0801+num09;
		document.frm01.hInsurePrice01.value=formatCurrency(sumnum01);		
	
		sumnum02 = num0102+num0202+num0302+num0402+num0502+num0602+num0702+num0802+num09;
		document.frm01.hInsurePrice02.value=formatCurrency(sumnum02);
	
	}	
	
	function sumPrb(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		
		if(document.frm01.hPBeasuti){
			num01 = rip_comma(document.frm01.hPBeasuti.value)*1;
		}
		if(document.frm01.hPArgon){
			num02 = rip_comma(document.frm01.hPArgon.value)*1;
		}		
		num04 = num01+num02;
		document.frm01.hPSum01.value=formatCurrency(num04);
		
		if(document.frm01.hPNiti[1].checked == true){
		
			num05 = (num01+num02)*0.01;
			document.frm01.hPVat.value=formatCurrency(num05);
		
		}else{
			num05 =0;
			document.frm01.hPVat.value="0.00";
		}
		
		
		num03 = (num01+num02)*(7/100);		
		document.frm01.hPInsureVat.value=formatCurrency(num03);
		
		sumnum = num01+num02+num03;
		
		document.frm01.hPPrice.value=formatCurrency(sumnum);
		
		sumall = sumnum-num05;
		document.frm01.hPSum02.value=formatCurrency(sumall);

		
	}		
	
	function sumPNiti(){
		var num01;
		
		num01=0;
		num02=0;
		if(document.frm01.hPTotal){ num01 = rip_comma(document.frm01.hPTotal.value)*1;}
		if(document.frm01.hPArgon){num02 = rip_comma(document.frm01.hPArgon.value)*1;}		
		
		sumtotal = (num01+num02)*0.01;
		document.frm01.hPVat.value=formatCurrency(sumtotal);
	
	}	
	
	function sumPNiti01(){
		document.frm01.hPVat.value="0.00";
	}	
			
	
	function sumK(){
		var num01, num02, num03;
		
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=0;
		
		if(document.frm01.hKBeasuti){
			num01 = rip_comma(document.frm01.hKBeasuti.value)*1;
		}
		if(document.frm01.hKArgon){
			num02 = rip_comma(document.frm01.hKArgon.value)*1;
		}		
		num04 = num01+num02;
		document.frm01.hKSum01.value=formatCurrency(num04);
		
		if(document.frm01.hKNiti[1].checked == true){
		
			num05 = (num01+num02)*0.01;
			document.frm01.hKVat.value=formatCurrency(num05);
		
		}else{
			num05 =0;
			document.frm01.hKVat.value="0.00";
		}
		
		
		num03 = (num01+num02)*(7/100);		
		document.frm01.hKInsureVat.value=formatCurrency(num03);
		
		sumnum = num01+num02+num03;
		
		document.frm01.hKPrice.value=formatCurrency(sumnum);
		
		sumall = sumnum-num05;
		document.frm01.hKSum02.value=formatCurrency(sumall);

		
	}		
	
	
	function sumKNiti(){
		var num01;
		
		num01=0;
		num02=0;
		if(document.frm01.hKTotal){ num01 = rip_comma(document.frm01.hKTotal.value)*1;}
		if(document.frm01.hKArgon){num02 = rip_comma(document.frm01.hKArgon.value)*1;}		
		
		sumtotal = (num01+num02)*0.01;
		document.frm01.hKVat.value=formatCurrency(sumtotal);
	
	}	
	
	function sumKNiti01(){
		document.frm01.hKVat.value="0.00";
	}	
	

	function sumAll(){		
		num01=0;
		num02=0;
		num03=0;
		num04=0;	
		num05=0;	
		num06=0;	
		num07=0;	
		num08=0;	
		num09=0;
	
		sumPrb();
		sumK();

		if(document.frm01.hPSum02){
			num01 = rip_comma(document.frm01.hPSum02.value)*1;
		}		

		if(document.frm01.hKSum02){
			num02 = rip_comma(document.frm01.hKSum02.value)*1;
		}		
	
		if(document.frm01.hDiscountOtherPrice){
			num03 = rip_comma(document.frm01.hDiscountOtherPrice.value)*1;
		}				

		num05 = num01+num02;
		document.frm01.hKSum03.value=formatCurrency(num05);
		
		num04 = num01+num02-num03;		
		document.frm01.hTotal.value=formatCurrency(num04);		
	}
	
	
	function same_prb(){
		var prb_id = document.frm01.hStockPrbId.value;
	
		if( prb_id > 0){
			document.frm01.hInsureCompanyId01.value = document.frm01.hInsureCompanyId.value;
			document.frm01.hStockKomId.value = document.frm01.hStockPrbId.value;
		
		}
	
	}
	
	function checkPrb(){
	
	<?forEach($objInsureFeeList->getItemList() as $objItem) {?>
		if(document.frm01.hInsureFeeId.value == "<?=$objItem->get_insure_fee_id()?>") {
			document.frm01.hPBeasuti.value = '<?=number_format($objItem->get_insure_fee(),2)?>';
			document.frm01.hPInsureVat.value = '<?=number_format($objItem->get_insure_vat(),2)?>';
			document.frm01.hPArgon.value = '<?=number_format($objItem->get_argon(),2)?>';
			document.frm01.hPPrice.value = '<?=number_format($objItem->get_total(),2)?>';
			document.frm01.hInsureFeeCode.value ='<?=$objItem->get_code();?>';
		}
	<?}?>
		sumAll();
	}
	
	<?if($objInsure->get_pay_number() > 0){?>
		checkDisplay(<?=$objInsure->get_pay_number()?>);
		checkSum();
	<?}?>
	
	
	function sum_var(val){
		num01=0;
		num02=0;
		num03=0;
		num04=0;
		num05=false;
		num06=0;
		num07=0;
		
		for(i=1;i<=5;i++){
			
			
			num01 =	document.getElementById("hQty_"+val+"_"+i).value;
			num02 =	rip_comma(document.getElementById("hPrice_"+val+"_"+i).value);
			num05 = document.getElementById("hPayin_"+val+"_"+i).checked;
			if(num05 == true){
				num03=(num01*num02)*-1;
			}else{
				num03=(num01*num02);			
			}
			
			if(document.getElementById("hPaymentSubjectId_"+val+"_"+i).value == 40 || document.getElementById("hPaymentSubjectId_"+val+"_"+i).value == 39  || document.getElementById("hPaymentSubjectId_"+val+"_"+i).value == 72  || document.getElementById("hPaymentSubjectId_"+val+"_"+i).value == 73){
				num06=num06+num03;			
			}else{
				num07=num07+num03;		
			}
			
			document.getElementById("hPriceSum_"+val+"_"+i).value= formatCurrency(num03);
			num04 = num04+num03;
		}
		document.getElementById("hPaymentPrice_"+val).value= formatCurrency(num04);
		document.getElementById("hPaymentPrice01_"+val).value= formatCurrency(num06);
		document.getElementById("hPaymentPrice02_"+val).value= formatCurrency(num07);
		
		checkSum();
		
	}
	
	function sum_free(){
		total=0;
		totalAll=0;
		document.getElementById("hStockProductTotal").value =formatCurrency(totalAll);
	}
	
	<!--
	function tryNumberFormat(obj)
	{
		if(obj.value != ""){
			obj.value = new NumberFormat(obj.value).toFormatted();
		}
	}
	//-->
	
		function rip_comma(hVal){
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");
		hVal = hVal.replace(",","");			
		if(hVal==""){
			return 0;
		}else{			
			return parseFloat(hVal);	
		}
	}
	
	function formatCurrency(num) {
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '' + num + '.' + cents);
	}
	
	
	function check_sale(val,vals){
		
			if(document.frm01.hSale01.checked==true){
				document.getElementById("sale_p").style.display="";				
			}else{
				document.getElementById("sale_p").style.display="none";

			}
			if(document.frm01.hSale02.checked==true){
				document.getElementById("sale_k").style.display="";

			}else{
				document.getElementById("sale_k").style.display="none";

			}
			if(document.frm01.hSale03.checked==true){
				document.getElementById("sale_r").style.display="";

			}else{
				document.getElementById("sale_r").style.display="none";

			}
			if(document.frm01.hSale01.checked==true || document.frm01.hSale02.checked==true ){ 
				document.getElementById("t_payment").style.display="";		
			}else{
				document.getElementById("t_payment").style.display="none";		
			}
			

			

	}
	
	</script>
	<?include "unset_all.php";?>
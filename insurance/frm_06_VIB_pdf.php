<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}
				$objLabel = new Label();
				$strText = "���������¹�ŧ������";
				$strText01= "���������¹�ŧ������";	
				
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				$arr = explode(",",$objLabel->getText01());
				
				
				$hHeight = 50;
				$pdf=new PDF("P","pt","A4");
				$pdf->AliasNbPages();
				$pdf->SetMargins(20,20,20);
				$pdf->SetAutoPageBreak(false,0.5);
				//Data loading
				$pdf->AddFont('angsa','','angsa.php'); 
				$pdf->AddFont('angsa','B','angsab.php'); 

				$pdf->AddPage();
				$pdf->SetLeftMargin(30);
				$pdf->SetXY($arr[56],$arr[57]);
				$objInsure = new Insure();
				$objInsure->set_insure_id($hInsureId);
				$objInsure->load();
				
				$objForm = new InsureForm();
				$objForm->set_insure_form_id($hFormId);
				$objForm->load();
				
				$objCompany = new Company();
				$objCompany->setCompanyId($sCompanyId);
				$objCompany->load();
				
				$objMember = new Member();
				$objMember->setMemberId($objInsure->get_sale_id());
				$objMember->load();
				
				$objSale = new Member();
				$objSale->setMemberId($objInsure->get_sale_id());
				$objSale->load();				
				
				$objInsureCar = new InsureCar();
				$objInsureCar->set_car_id($objInsure->get_car_id());
				$objInsureCar->load();
				
				$objCustomer = new InsureCustomer();
				$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
				$objCustomer->load();
				
				$objCarType = new CarType();
				$objCarType->setCarTypeId($objInsureCar->get_car_type());
				$objCarType->load();
				
				$objCarModel = new CarModel();
				$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
				$objCarModel->load();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objInsureCar->get_color());
				$objCarColor->load();
				
				$hStartDate= formatThaiDate($objInsure->get_date_protect());
				$hStartYear = substr($hStartDate,-4);
				$hEndYear = $hStartYear+1;
				$hEndDate = substr($hStartDate,0,(strlen($hStartDate)-4)).$hEndYear;
				
				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				$pdf->SetFont('angsa','B',14);
				
				$pdf->Cell(30,30,"",0,0,"C");
				$pdf->Cell(140,30,$objForm->get_t01(),0,0,"C");
				$pdf->Cell(50,30,"",0,0,"C");
				$pdf->Cell(130,30,$objForm->get_t02(),0,0,"C");
				$pdf->Cell(50,30,"",0,0,"C");
				$pdf->Cell(100,30,$objForm->get_t03(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(120,20,$objForm->get_t04(),0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(100,20,$objForm->get_t05(),0,0,"C");
				$pdf->Cell(70,20,"",0,0,"C");
				$pdf->Cell(20,20,$objForm->get_t59(),0,0,"C");
				$pdf->Cell(50,20,$objForm->get_t60(),0,0,"C");
				$pdf->Cell(20,20,$objForm->get_t61(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(20,20,"",0,0,"C");
				$pdf->Cell(150,20,$objForm->get_t07(),0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(130,20,$objForm->get_t08(),0,0,"C");
				$pdf->Cell(80,20,"",0,0,"C");
				$pdf->Cell(20,20,$objForm->get_t62(),0,0,"C");
				$pdf->Cell(40,20,$objForm->get_t63(),0,0,"C");
				$pdf->Cell(20,20,$objForm->get_t64(),0,0,"C");
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Cell(30,30,"",0,0,"C");


				//$pdf->line(123,384,142,365);
				$pdf->SetXY($arr[58],$arr[59]);
				$pdf->Cell(30,19,"",0,0,"C");
				$pdf->Cell(25,19,"1",0,0,"C");
				$pdf->Cell(60,19,"",0,0,"L");
				$pdf->Cell(450,19,$objForm->get_t38(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(70,19,"",0,0,"C");
				$pdf->Cell(550,19,$objForm->get_t39(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(20,19,"",0,0,"C");
				$pdf->Cell(600,19,$objForm->get_t40(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(35,19,"",0,0,"C");
				$pdf->Cell(550,19,$objForm->get_t41(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(100,19,"",0,0,"C");
				$pdf->Cell(200,19,$objForm->get_t42(),0,0,"L");
				$pdf->Cell(100,19,"",0,0,"C");
				$pdf->Cell(200,19,$objForm->get_t43(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(30,7,"",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(30,19,"",0,0,"C");
				if($objForm->get_t44() != ""){
				$pdf->Cell(25,19,"2",0,0,"C");
				}else{
				$pdf->Cell(25,19,"",0,0,"C");
				}
				$pdf->Cell(60,19,"",0,0,"L");
				$pdf->Cell(450,19,$objForm->get_t44(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(70,19,"",0,0,"C");
				$pdf->Cell(550,19,$objForm->get_t45(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(20,19,"",0,0,"C");
				$pdf->Cell(600,19,$objForm->get_t46(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(35,19,"",0,0,"C");
				$pdf->Cell(550,19,$objForm->get_t47(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(100,19,"",0,0,"C");
				$pdf->Cell(200,19,$objForm->get_t48(),0,0,"L");
				$pdf->Cell(100,19,"",0,0,"C");
				$pdf->Cell(200,19,$objForm->get_t49(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(370,18,"",0,0,"C");
				$pdf->Cell(200,18,$objForm->get_t53(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,18,"",0,0,"L");
				$pdf->Cell(550,18,$objForm->get_t54(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(390,17,"",0,0,"C");
				$pdf->Cell(200,17,$objForm->get_t58(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(390,8,"",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(200,19,$objForm->get_t65(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(390,19,"",0,0,"C");
				$pdf->Cell(200,19,$objForm->get_t66(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(320,19,"",0,0,"C");
				$pdf->Cell(70,19,$objForm->get_t59(),0,0,"C");
				$pdf->Cell(55,19,$objForm->get_t60(),0,0,"C");
				$pdf->Cell(70,19,$objForm->get_t61(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(370,45,"",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(60,5,"",0,0,"L");
				$pdf->Cell(150,5,$objForm->get_t67(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(60,19,"",0,0,"C");
				$pdf->Cell(50,19,$objForm->get_t59(),0,0,"C");
				$pdf->Cell(50,19,$objForm->get_t60(),0,0,"C");
				$pdf->Cell(40,19,$objForm->get_t61(),0,0,"C");
				
				if($objForm->get_t10() == 1){$pdf->line($arr[52],$arr[53],$arr[52]+8,$arr[53]-8);}
				if($objForm->get_t11() == 1){$pdf->line($arr[54],$arr[55],$arr[54]+8,$arr[55]-8);}
				if($objForm->get_t12() == 1){$pdf->line($arr[0],$arr[1],$arr[0]+8,$arr[1]-8);}
				if($objForm->get_t13() == 1){$pdf->line($arr[2],$arr[3],$arr[2]+8,$arr[3]-8);}
				if($objForm->get_t14() == 1){$pdf->line($arr[4],$arr[5],$arr[4]+8,$arr[5]-8);}
				if($objForm->get_t15() == 1){$pdf->line($arr[6],$arr[7],$arr[6]+8,$arr[7]-8);}
				if($objForm->get_t16() == 1){$pdf->line($arr[8],$arr[9],$arr[8]+8,$arr[9]-8);}
				if($objForm->get_t17() == 1){$pdf->line($arr[10],$arr[11],$arr[10]+8,$arr[11]-8);}
				if($objForm->get_t18() == 1){$pdf->line($arr[12],$arr[13],$arr[12]+8,$arr[13]-8);}
				if($objForm->get_t19() == 1){$pdf->line($arr[14],$arr[15],$arr[14]+8,$arr[15]-8);}
				if($objForm->get_t20() == 1){$pdf->line($arr[16],$arr[17],$arr[16]+8,$arr[17]-8);}
				if($objForm->get_t21() == 1){$pdf->line($arr[18],$arr[19],$arr[18]+8,$arr[19]-8);}
				if($objForm->get_t22() == 1){$pdf->line($arr[20],$arr[21],$arr[20]+8,$arr[21]-8);}
				if($objForm->get_t23() == 1){$pdf->line($arr[22],$arr[23],$arr[22]+8,$arr[23]-8);}
				if($objForm->get_t24() == 1){$pdf->line($arr[24],$arr[25],$arr[24]+8,$arr[25]-8);}
				if($objForm->get_t25() == 1){$pdf->line($arr[26],$arr[27],$arr[26]+8,$arr[27]-8);}
				if($objForm->get_t26() == 1){$pdf->line($arr[28],$arr[29],$arr[28]+8,$arr[29]-8);}
				if($objForm->get_t27() == 1){$pdf->line($arr[30],$arr[31],$arr[30]+8,$arr[31]-8);}
				if($objForm->get_t28() == 1){$pdf->line($arr[32],$arr[33],$arr[32]+8,$arr[33]-8);}
				if($objForm->get_t29() == 1){$pdf->line($arr[34],$arr[35],$arr[34]+8,$arr[35]-8);}
				if($objForm->get_t30() == 1){$pdf->line($arr[36],$arr[37],$arr[36]+8,$arr[37]-8);}
				if($objForm->get_t31() == 1){$pdf->line($arr[38],$arr[39],$arr[38]+8,$arr[39]-8);}
				if($objForm->get_t32() == 1){$pdf->line($arr[40],$arr[41],$arr[40]+8,$arr[41]-8);}
				if($objForm->get_t33() == 1){$pdf->line($arr[42],$arr[43],$arr[42]+8,$arr[43]-8);}
				if($objForm->get_t34() == 1){$pdf->line($arr[44],$arr[45],$arr[44]+8,$arr[45]-8);}
				if($objForm->get_t35() == 1){$pdf->line($arr[46],$arr[47],$arr[46]+8,$arr[47]-8);}
				if($objForm->get_t36() == 1){$pdf->line($arr[48],$arr[49],$arr[48]+8,$arr[49]-8);}
				if($objForm->get_t37() == 1){$pdf->line($arr[50],$arr[51],$arr[50]+8,$arr[51]-8);}

				if($objForm->get_t50() == 1){$pdf->line($arr[60],$arr[61],$arr[60]+8,$arr[61]-8);}
				if($objForm->get_t51() == 1){$pdf->line($arr[62],$arr[63],$arr[62]+8,$arr[63]-8);}
				if($objForm->get_t52() == 1){$pdf->line($arr[64],$arr[65],$arr[64]+8,$arr[65]-8);}
				
				if($objForm->get_t55() == 1){$pdf->line($arr[66],$arr[67],$arr[66]+8,$arr[67]-8);}
				if($objForm->get_t56() == 1){$pdf->line($arr[68],$arr[69],$arr[68]+8,$arr[69]-8);}
				if($objForm->get_t57() == 1){$pdf->line($arr[70],$arr[71],$arr[70]+8,$arr[71]-8);}
				
				
	Unset($objCustomerList);
	$pdf->Output();	
	?>
	<?include "unset_all.php";?>
	<script>
	//window.close();
	</script>	
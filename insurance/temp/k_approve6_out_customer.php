<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);


if (isset($hSubmitStock) )   {  //Request to delete some StockProduct release

	$e = sizeof($sArrCutStockKom);
	for($i=0;$i<$hCountTotal;$i++){
			if(isset($_POST["hStock_".$i])){
				if($sArrCutStockKom[$_POST["hStock_".$i]] == "" or $sArrCutStockKom[$_POST["hStock_".$i]] == 0 ){
					$sArrCutStockKom[$_POST["hStock_".$i]] = 1;
					$e++;
				}
			}else{
				
					$sArrCutStockKom[$hInsureStockId[$i]] = 0;
				
			}
	}
	session_register("sArrCutStockKom");
}


$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')     )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}

if(isset($hCheckKom)){
	if($pstrCondition != ""){
		$pstrCondition .="AND k_number <> ''  ";
	}else{
		$pstrCondition .=" k_number <> ''  ";	
	}
}

if(isset($hCheckPrb)){
	if($pstrCondition != ""){
		$pstrCondition .="AND p_stock_id > 0  ";
	}else{
		$pstrCondition .=" p_stock_id > 0  ";	
	}
}

if(isset($hKSendCode)){
	if($pstrCondition != ""){
		$pstrCondition .="AND k_send_code LIKE '%$hKSendCode%'  ";
	}else{
		$pstrCondition .="  k_send_code LIKE '%$hKSendCode%'  ";	
	}
}


if($hKSendDay != "" AND $hKSendMonth != "" AND $hKSendYear != ""){
	$hKStartDate = $hKSendYear."-".$hKSendMonth."-".$hKSendDay;
}

if($hKSendDay01 != "" AND $hKSendMonth01 != "" AND $hKSendYear01 != ""){
	$hKEndDate = $hKSendYear01."-".$hKSendMonth01."-".$hKSendDay01;
}

if($hKStartDate != ""){
	if($hKEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_send_date  >= '".$hKStartDate."' AND P.k_send_date <= '".$hKEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_send_date  >= '".$hKStartDate."' AND P.k_send_date <= '".$hKEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_send_date  = '".$hKStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_send_date = '".$hKStartDate."' ) ";
		}
	}
}



if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '$Year02-$Month02-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '$Year02-$Month02-%'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '%-".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '%-".$Month02."-%'   ";	
		}
	}
}


if($hDay != "" AND $hMonth != "" AND $hYear != ""){
	$hStartDate = $hYear."-".$hMonth."-".$hDay;
}

if($hDay01 != "" AND $hMonth01 != "" AND $hYear01 != ""){
	$hEndDate = $hYear01."-".$hMonth01."-".$hDay01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( I.k_ins_date  >= '".$hStartDate."' AND I.k_ins_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( I.k_ins_date  >= '".$hStartDate."' AND I.k_ins_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( I.k_ins_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( I.k_ins_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}


if($hCompanyId > 0 ){
	if($pstrCondition != ""){
		$pstrCondition.=" AND P.company_id = $hCompanyId ";
	}else{
		$pstrCondition=" P.company_id = $hCompanyId ";
	}
}


if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}

$sesConditionK6InStock = $pstrCondition;
session_register("sesConditionK6InStock",$sesConditionK6InStock);

}else{//end hSearch



}

$objM = new Member();
$objM->setMemberId($sMemberId);
$objM->load();


if($sesConditionK6InStock == ""){
	$pstrCondition= "  ( O.status = '�Ѻ��Ҩҡ�.��Сѹ���' or O.status= '������觤׹�١���'  ) AND ( cancel ='' and changes='' ) AND k_send > 0  ";
}else{
	$pstrCondition= "  ( O.status = '�Ѻ��Ҩҡ�.��Сѹ���' or O.status= '������觤׹�١���'  ) AND ( cancel ='' and changes='' ) AND k_send > 0  AND ".$sesConditionK6InStock;
}

echo $pstrCondition;

$objInsureStockList = new InsureStockList();
$objInsureStockList->setFilter($pstrCondition);
$objInsureStockList->setPageSize(PAGE_SIZE);
$objInsureStockList->setPage($hPage);
$objInsureStockList->setSortDefault(" stock_date ASC ");
$objInsureStockList->setSort($hSort);
$objInsureStockList->load();

$pCurrentUrl = "k_approve6_out_customer.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "5. ��������";
//$strHead03 = "���Ң������١���";
$pageContent = "5.13 ��¡�����͡�����١���";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="post">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right"> �ѹ���ѹ�֡�����š������� :</td>
	<td class="small">
	  	<table cellspacing="0" cellpadding="0">
		<tr>			
			<td><INPUT align="middle" size="2" maxlength="2"   name=hDay value="<?=$hDay?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hMonth value="<?=$hMonth?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hYear value="<?=$hYear?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear,hDay, hMonth, hYear,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=hDay01 value="<?=$hDay01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hMonth01 value="<?=$hMonth01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hYear01 value="<?=$hYear01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear01,hDay01, hMonth01, hYear01,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
</tr>	
<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>
<tr>
		<td align="right">�Ң� :</td>
		<td>
			<?
			$objCList = new CompanyList();
			$objCList->setPageSize(0);
			$objCList->load();
			$objCList->printSelect("hCompanyId",$hCompanyId,"��س����͡�Ң�");
			?>
		</td>
</tr>

<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureStockList->mCount;?> ��¡��</div>


<form name="frm" action="k_approve6_in_stock.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="80%" >
	<?
		$objInsureStockList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureStockList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureStockList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="right">

	</td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" rowspan="2" width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td valign="top" rowspan="2" width="5%" align="center" class="ListTitle">�ѹ�������</td>
	<td valign="top" colspan="3"  align="center" class="ListTitle04">ʵ�ͤ������������ش</td>
	<td valign="top" colspan="8"  align="center" class="ListTitle05">�����Ż�Сѹ���</td>


	<td valign="top" rowspan="2"  width="5%" align="center" class="ListTitle">��Ǩ�ͺ</td>

</tr>
<tr>
	<td valign="top" width="7%" align="center" class="ListTitle04">�ѹ����͹����</td>
	<td valign="top" width="5%" align="center" class="ListTitle04">�Ң�</td>
	<td valign="top" width="10%" align="center" class="ListTitle04">Ἱ�</td>
	
	<td valign="top" width="5%" align="center" class="ListTitle05">�Ң�</td>	
	<td valign="top"  width="5%" align="center" class="ListTitle05">Sale</td>
	<td valign="top"  width="15%" align="center" class="ListTitle05">�����Ţ���������</td>
	<td valign="top"  width="7%" align="center" class="ListTitle05">�ѹ������ͧ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle05">�.��Сѹ���</td>
	<td valign="top"  width="20%" align="center" class="ListTitle05">����-���ʡ��<br>�Ţ����¹ö<br>�����Ţ�ѧ</td>
	<td valign="top"  width="7%" align="center" class="ListTitle05">�������</td>
	<td valign="top"  width="7%" align="center" class="ListTitle05">ʶҹС�ê���</td>
	
</tr>
	<?
		$i=0;
		forEach($objInsureStockList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objInsure->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();

		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objItem->get_insure_company_id());
		$objKomCompany->load();
		
		$objCompany = new Company();
		$objCompany->setCompanyId($objItem->get_status_company_id());
		$objCompany->load();
		
		$objCompany1 = new Company();
		$objCompany1->setCompanyId($objMember->getCompanyId());
		$objCompany1->load();
		
		$objDepartment = new Department();
		$objDepartment->setDepartmentId($objItem->get_status_department_id());
		$objDepartment->load();
		
		
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=(($hPage-1)*PAGE_SIZE)+$i+1?></td>
	<td valign="top" align="center" ><?=formatShortDateTime($objItem->get_stock_date())?></td>	
	<td valign="top" align="center" class="listDetail04" ><?=formatShortDateTime($objItem->get_status_date())?></td>	
	<td valign="top" align="center" class="listDetail04"  ><?=$objCompany->getShortTitleEn()?></td>	
	<td valign="top" align="center" class="listDetail04"  ><?=$objDepartment->getTitle()?></td>	

	<td valign="top"  align="center" class="listDetail05"  ><?=$objCompany1->getShortTitleEn()?></td>
	<td valign="top"  align="center" class="listDetail05"  ><?=$objMember->getNickname()?></td>		
	<td valign="top" align="center" class="listDetail05" ><?=$objInsure->get_k_number()?></td>
	<td valign="top" align="center" class="listDetail05" ><?=formatShortDate($objInsure->get_k_start_date())?></td>	
	<td valign="top" align="center" class="listDetail05"  ><?=$objKomCompany->getShortTitle()?></td>		
	<td valign="top"  class="listDetail05"  >
	<?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?><br>
	<?=$objInsureCar->get_code()?><br>
	<?=$objInsureCar->get_car_number()?>	
	</td>
	<td valign="top"  align="right" class="listDetail05"  ><?=number_format($objInsure->get_k_num13() ,2);?></td>
	<td valign="top"  align="center" class="listDetail05"  ><?if($objInsure->get_status() == "ccard") echo "��������";?></td>
	<td  valign="top" align="center">
	<input type="button" name="hFix" value="��Ǩ�ͺ" onclick="window.location='k_approve6_view.php?hInsureId=<?=$objItem->get_insure_id()?>'">
	</td>	
</tr>
	<?
		++$i;
	}
	Unset($objInsureStockList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?
	include("h_footer.php")
?>
<script>
	function checkall(check){
		if(check == true){
	
		val = document.frm.hCountTotal.value;
		for(i=0;i<val;i++){
			if(document.getElementById("hStock_"+i)){
			//document.getElementById("hStock_"+i).checked=true;
			eval("document.frm.hStock_"+i+".checked = true");
			}
		}	
		
		}else{
		
		val = document.frm.hCountTotal.value;
		for(i=0;i<val;i++){
			if(document.getElementById("hStock_"+i)){
			//document.getElementById("hStock_"+i).checked=true;
			eval("document.frm.hStock_"+i+".checked = false");
			}
		}	
		
		}
		
		
	}
	
</script>
<?include "unset_all.php";?>
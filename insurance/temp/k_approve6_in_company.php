<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);


if (isset($hSubmitStock) )   {  //Request to delete some StockProduct release

	$e = sizeof($sArrCutStockKom);
	for($i=0;$i<$hCountTotal;$i++){
			if(isset($_POST["hStock_".$i])){
				if($sArrCutStockKom[$_POST["hStock_".$i]] == "" or $sArrCutStockKom[$_POST["hStock_".$i]] == 0 ){
					$sArrCutStockKom[$_POST["hStock_".$i]] = 1;
					$e++;
				}
			}else{
				
					$sArrCutStockKom[$hInsureStockId[$i]] = 0;
				
			}
	}
	session_register("sArrCutStockKom");
}


$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}

if(isset($hCheckKom)){
	if($pstrCondition != ""){
		$pstrCondition .="AND k_number <> ''  ";
	}else{
		$pstrCondition .=" k_number <> ''  ";	
	}
}

if(isset($hCheckPrb)){
	if($pstrCondition != ""){
		$pstrCondition .="AND p_stock_id > 0  ";
	}else{
		$pstrCondition .=" p_stock_id > 0  ";	
	}
}

if(isset($hKSendCode)){
	if($pstrCondition != ""){
		$pstrCondition .="AND k_send_code LIKE '%$hKSendCode%'  ";
	}else{
		$pstrCondition .="  k_send_code LIKE '%$hKSendCode%'  ";	
	}
}


if($hKSendDay != "" AND $hKSendMonth != "" AND $hKSendYear != ""){
	$hKStartDate = $hKSendYear."-".$hKSendMonth."-".$hKSendDay;
}

if($hKSendDay01 != "" AND $hKSendMonth01 != "" AND $hKSendYear01 != ""){
	$hKEndDate = $hKSendYear01."-".$hKSendMonth01."-".$hKSendDay01;
}

if($hKStartDate != ""){
	if($hKEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_send_date  >= '".$hKStartDate."' AND P.k_send_date <= '".$hKEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_send_date  >= '".$hKStartDate."' AND P.k_send_date <= '".$hKEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_send_date  = '".$hKStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_send_date = '".$hKStartDate."' ) ";
		}
	}
}



if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '$Year02-$Month02-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '$Year02-$Month02-%'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '%-".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '%-".$Month02."-%'   ";	
		}
	}
}


if($hDay != "" AND $hMonth != "" AND $hYear != ""){
	$hStartDate = $hYear."-".$hMonth."-".$hDay;
}

if($hDay01 != "" AND $hMonth01 != "" AND $hYear01 != ""){
	$hEndDate = $hYear01."-".$hMonth01."-".$hDay01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_ins_date  >= '".$hStartDate."' AND P.k_ins_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_ins_date  >= '".$hStartDate."' AND P.k_ins_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_ins_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_ins_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_broker_id = '".$hInsureBrokerId."' ) ";
		}
}

if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_prb_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_prb_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hFrmType != "" ){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( I.frm_type  = '".$hFrmType."' ) ";
		}else{
			$pstrCondition .=" ( I.frm_type = '".$hFrmType."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_prb_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_prb_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

	
if($hCompanyId > 0 ){
	if($pstrCondition != ""){
		$pstrCondition.=" AND P.company_id = $hCompanyId ";
	}else{
		$pstrCondition=" P.company_id = $hCompanyId ";
	}
}


if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}

$sesConditionK6 = $pstrCondition;
session_register("sesConditionK6",$sesConditionK6);

}else{//end hSearch



}

$objM = new Member();
$objM->setMemberId($sMemberId);
$objM->load();


if($sesConditionK6 == ""){
	$pstrCondition= "  ( sup_approve = '͹��ѵ�' and admin_approve= '͹��ѵ�' ) AND P.k_stock_id = 0 AND frm_type = '0901' AND P.insure_date <> '0000-00-00' ";
}else{
	$pstrCondition= "  ( sup_approve = '͹��ѵ�' and admin_approve= '͹��ѵ�' ) AND P.k_stock_id = 0 AND frm_type = '0901' AND P.insure_date <> '0000-00-00'  AND ".$sesConditionK6;
}

echo $pstrCondition;

$objInsureFrmList = new InsureFormList();
$objInsureFrmList->setFilter($pstrCondition);
$objInsureFrmList->setPageSize(PAGE_SIZE);
$objInsureFrmList->setPage($hPage);
$objInsureFrmList->setSortDefault(" k_start_date ASC ");
$objInsureFrmList->setSort($hSort);
if(isset($hSearch)){
	$objInsureFrmList->loadList();
}
$pCurrentUrl = "k_approve6_in_company.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "5. ��������";
//$strHead03 = "���Ң������١���";
$pageContent = "5.6 �Ѻ��ҡ�������ҡ�.��Сѹ���";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="post">
<table align="center" class="search" cellpadding="3">

<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	

<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureFrmList->mCount;?> ��¡��</div>

<table width="950">
<tr>
	<td align="center">
	  <input type="button" value="�Ѻ�������������¡��" onclick="window.location='insure_stock_kom_multi.php';">
	  &nbsp;&nbsp;
	  <input type="button" value="�����ʵ�ͤ��������" onclick="window.location='insure_stock_kom_label.php';">
	  &nbsp;&nbsp;
	  <input type="button" value="�����ʵ�ͤ�ú." onclick="window.location='insure_stock_prb_label.php';">	  
	  
	</td>
</tr>
</table>



<form name="frm" action="k_approve6_in_company.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="80%" >
	<?
		$objInsureFrmList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureFrmList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureFrmList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="right">
	
	</td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ����駧ҹ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ţ����Ѻ��</td>
	<td valign="top"  width="15%" align="center" class="ListTitle">�����Ţ �ú.</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�յ������</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">
	�ѹ������ͧ<br>
	��������
	<font color="#993300">(�ú.)</font>
	</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�.��Сѹ���</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">����-���ʡ��</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ţ����¹</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��ѡ�ҹ���</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ�֡�Ѻ</td>
</tr>
	<?
		$i=0;
		forEach($objInsureFrmList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureForm = new InsureForm();
		$objInsureForm->loadByCondition(" insure_id = ".$objItem->get_insure_id()."  and frm_type= '0901' ");
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;
		
		$objMember = new Member();
		$objMember->setMemberId($objInsure->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();

		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objInsure->get_p_prb_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objInsure->get_k_prb_id());
		$objKomCompany->load();
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=(($hPage-1)*PAGE_SIZE)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objInsure->get_insure_date())?></td>	
	<td valign="top" align="center" ><?=$objInsure->get_p_rnum()?></td>	
	<td valign="top" align="center">
		<font color="#006600"><strong><?=$objInsure->get_p_number()?></strong></font>
	</td>
	<td valign="top" align="center" ><?=$objInsure->get_p_year()?></td>	
	<td valign="top" align="center" >
	<?=formatShortDate($objInsure->get_k_start_date())?>
	<?if ( ($objInsure->get_k_start_date() != $objInsure->get_p_start_date() )  and $objInsure->get_p_check() ==1 ) {?>
	<br><font color="#993300">(<?=formatShortDate($objInsure->get_p_start_date())?>)</font>
	<?}?>
	</td>	
	<td valign="top" align="center" ><?=$objKomCompany->getShortTitle()?></td>		
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>
	<td  valign="top" align="center">
	<?if($objInsure->get_k_number() == ""){?><?if($objItem->get_frm_type() == "0901"){?><input type="button" name="hFix" value="�ѹ�֡�Ѻ" onclick="window.location='k_approve6_in_company_add.php?hInsureId=<?=$objItem->get_insure_id()?>&hFormId=<?=$objItem->get_insure_form_id()?>'"><?}?><?}?>
	</td>	
</tr>
	<?
		++$i;
			}
	Unset($objCustomerList);
	?>

</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?
	include("h_footer.php")
?>
<script>
	function checkall(check){
		if(check == true){
	
		val = document.frm.hCountTotal.value;
		for(i=0;i<val;i++){
			if(document.getElementById("hStock_"+i)){
			//document.getElementById("hStock_"+i).checked=true;
			eval("document.frm.hStock_"+i+".checked = true");
			}
		}	
		
		}else{
		
		val = document.frm.hCountTotal.value;
		for(i=0;i<val;i++){
			if(document.getElementById("hStock_"+i)){
			//document.getElementById("hStock_"+i).checked=true;
			eval("document.frm.hStock_"+i+".checked = false");
			}
		}	
		
		}
		
		
	}
	
</script>
<?include "unset_all.php";?>
<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",1000);

if(isset($hSubmit1)){
	
	for($i=0;$i<$hCount1;$i++){
	
		if(isset($hApproveK[$i])){
			if($hInsureIdK[$i] > 0){
				$objInsureWsVendor = new InsureWsVendor();
				$objInsureWsVendor->set_insure_id($hInsureIdK[$i]);
				$objInsureWsVendor->set_broker_id($hBrokerK[$i]);
				$objInsureWsVendor->set_prb_id($hPrbK[$i]);
				$objInsureWsVendor->set_ws_vendor_no($hDocunoK[$i]);
				$objInsureWsVendor->set_ws_vendor_name($hNameK[$i]);
				$objInsureWsVendor->set_ws_vendor_date($hDateK[$i]);
				$objInsureWsVendor->set_ws_vendor_date1($hInsureDate[$i]);
				$objInsureWsVendor->set_ws_vendor_price($hPriceK[$i]);
				$objInsureWsVendor->set_ws_vendor_type("K");
				$objInsureWsVendor->set_ws_vendor_status("");
				$objInsureWsVendor->add();
				$hVendorK = "Y";
			}else{
				$hVendorK = $hOldK[$i];
			}
		}else{
			if(isset($hCancelK[$i])){
				$hVendorK = "N";
			}else{
				$hVendorK = $hOldK[$i];
			}
		}
		
		
		if(isset($hApproveP[$i])){
			if($hInsureIdP[$i] > 0){
				$objInsureWsVendor = new InsureWsVendor();
				$objInsureWsVendor->set_insure_id($hInsureIdP[$i]);
				$objInsureWsVendor->set_broker_id($hBrokerP[$i]);
				$objInsureWsVendor->set_prb_id($hPrbP[$i]);
				$objInsureWsVendor->set_ws_vendor_no($hDocunoP[$i]);
				$objInsureWsVendor->set_ws_vendor_name($hNameP[$i]);
				$objInsureWsVendor->set_ws_vendor_date($hDateP[$i]);
				$objInsureWsVendor->set_ws_vendor_date1($hInsureDate[$i]);
				$objInsureWsVendor->set_ws_vendor_price($hPriceP[$i]);
				$objInsureWsVendor->set_ws_vendor_type("P");
				$objInsureWsVendor->set_ws_vendor_status("");
				$objInsureWsVendor->add();
				$hVendorP = "Y";
			}else{
				$hVendorP = $hOldP[$i];
			}				
		}else{
			if(isset($hCancelP[$i])){
				$hVendorP = "N";
			}else{
				$hVendorP = $hOldP[$i];
			}
		}
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsure[$i]);
		$objInsure->set_ws_vendor_k($hVendorK);
		$objInsure->set_ws_vendor_p($hVendorP);	
		$objInsure->updateVendor();
	}
	
	header("location:insure_winspeed_vendor_renew.php?hType=$hType&hInsureDay=$hInsureDay&hInsureMonth=$hInsureMonth&hInsureYear=$hInsureYear&hInsureDay1=$hInsureDay1&hInsureMonth1=$hInsureMonth1&hInsureYear1=$hInsureYear1&hCompanyId=$hCompanyId&hSearch=$hSearch");
	exit;	
	
}






if(isset($hSubmit2)){
	
	for($i=0;$i<$hCount2;$i++){
	
		if(isset($hApproveK[$i])){
			if($hInsureIdK[$i] > 0){
				$objInsureWsVendor = new InsureWsVendor();
				$objInsureWsVendor->set_insure_id($hInsureIdK[$i]);
				$objInsureWsVendor->set_broker_id($hBrokerK[$i]);
				$objInsureWsVendor->set_prb_id($hPrbK[$i]);
				$objInsureWsVendor->set_ws_vendor_no($hDocunoK[$i]);
				$objInsureWsVendor->set_ws_vendor_name($hNameK[$i]);
				$objInsureWsVendor->set_ws_vendor_date($hDateK[$i]);
				$objInsureWsVendor->set_ws_vendor_date1($hInsureDate[$i]);
				$objInsureWsVendor->set_ws_vendor_price($hPriceK[$i]);
				$objInsureWsVendor->set_ws_vendor_type("K");
				$objInsureWsVendor->set_ws_vendor_status("");
				$objInsureWsVendor->add();
				$hVendorK = "Y";
			}else{
				$hVendorK = $hOldK[$i];
			}
		}else{
			if(isset($hCancelK[$i])){
				$hVendorK = "N";
			}else{
				$hVendorK =  $hOldK[$i];
			}
		}
		
		
		if(isset($hApproveP[$i])){
			if($hInsureIdP[$i] > 0){
				$objInsureWsVendor = new InsureWsVendor();
				$objInsureWsVendor->set_insure_id($hInsureIdP[$i]);
				$objInsureWsVendor->set_broker_id($hBrokerP[$i]);
				$objInsureWsVendor->set_prb_id($hPrbP[$i]);
				$objInsureWsVendor->set_ws_vendor_no($hDocunoP[$i]);
				$objInsureWsVendor->set_ws_vendor_name($hNameP[$i]);
				$objInsureWsVendor->set_ws_vendor_date($hDateP[$i]);
				$objInsureWsVendor->set_ws_vendor_date1($hInsureDate[$i]);
				$objInsureWsVendor->set_ws_vendor_price($hPriceP[$i]);
				$objInsureWsVendor->set_ws_vendor_type("P");
				$objInsureWsVendor->set_ws_vendor_status("");
				$objInsureWsVendor->add();
				$hVendorP = "Y";
			}else{
				$hVendorP = $hOldP[$i];
			}				
		}else{
			if(isset($hCancelP[$i])){
				$hVendorP = "N";
			}else{
				$hVendorP =  $hOldP[$i];
			}
		}
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($hInsure[$i]);
		$objInsure->set_ws_vendor_k($hVendorK);
		$objInsure->set_ws_vendor_p($hVendorP);	
		$objInsure->updateVendor();
	}
	
	header("location:insure_winspeed_vendor_renew.php?hType=$hType&hInsureDay=$hInsureDay&hInsureMonth=$hInsureMonth&hInsureYear=$hInsureYear&hInsureDay1=$hInsureDay1&hInsureMonth1=$hInsureMonth1&hInsureYear1=$hInsureYear1&hCompanyId=$hCompanyId&hSearch=$hSearch");
	exit;	
	
	
}






if(isset($hSubmit3)){
	
	for($i=0;$i<$hCount3;$i++){
	
		if(isset($hCancel[$i])){
			if($hInsure[$i] > 0){
				$objInsureWsVendor = new InsureWSVendor();
				$objInsureWsVendor->set_ws_vendor_id($hWsVendorId[$i]);
				$objInsureWsVendor->delete();
			
				if($hWsVendorType[$i] == "K"){
					$objInsure = new Insure();
					$objInsure->set_insure_id($hInsure[$i]);
					$objInsure->set_ws_vendor_k("");
					$objInsure->updateVendorK();
				}else{
					$objInsure = new Insure();
					$objInsure->set_insure_id($hInsure[$i]);
					$objInsure->set_ws_vendor_p("");
					$objInsure->updateVendorP();
				}
			}


		}else{
		//update row
				$objInsureWsVendor = new InsureWsVendor();
				$objInsureWsVendor->set_ws_vendor_id($hWsVendorId[$i]);
				$objInsureWsVendor->set_ws_vendor_no($hDocuno[$i]);
				$date = $_POST["Year09_".$i]."-".$_POST["Month09_".$i]."-".$_POST["Day09_".$i];
				$objInsureWsVendor->set_ws_vendor_date($date);
				$objInsureWsVendor->set_ws_vendor_status($hStatus[$i]);
				$objInsureWsVendor->update();
		}

	}
	
	header("location:insure_winspeed_vendor_renew.php?hType=$hType&hInsureDay=$hInsureDay&hInsureMonth=$hInsureMonth&hInsureYear=$hInsureYear&hInsureDay1=$hInsureDay1&hInsureMonth1=$hInsureMonth1&hInsureYear1=$hInsureYear1&hCompanyId=$hCompanyId&hSearch=$hSearch");
	exit;	
	
	
}





$pCurrentUrl = "rp_acc01.php?";
	// for paging only (sort resets page viewing)
$pageTitle = "6. �к��ѭ�ջ�Сѹ���";
$strHead03 = "6.6 Export Winspeed";
$pageContent = "6.6 Export Winspeed > ��¡�����˹��յ��";
	
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
	


		if (document.forms.frm01.hInsureDay.value=="" || document.forms.frm01.hInsureDay.value=="00")
		{
			alert("��س��к��ѹ�������");
			document.forms.frm01.hInsureDay.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.hInsureDay.value,1,31) == false) {
				document.forms.frm01.hInsureDay.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.hInsureMonth.value==""  || document.forms.frm01.hInsureMonth.value=="00")
		{
			alert("��س��к���͹�������");
			document.forms.frm01.hInsureMonth.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.hInsureMonth.value,1,12) == false){
				document.forms.frm01.hInsureMonth.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.hInsureYear.value==""  || document.forms.frm01.hInsureYear.value=="0000")
		{
			alert("��س��кػշ���������");
			document.forms.frm01.hInsureYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.hInsureYear.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.hInsureYear.focus();
				return false;
			}
		} 						
		
			

	}
	


</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT><br>
<div align="center" class="error"><?=$pstrMsg?></div>


<table align="center" cellspacing="2" cellpadding="2" border="0" class="search">
<form  name="frm01" action="insure_winspeed_vendor_renew.php" method="get" onKeyDown="if(event.keyCode==13) event.keyCode=9;" onsubmit="return check_submit();" >
<tr>
	<td valign="top">�ѹ��� :</td>
	<td align="center">	
	  	<table cellspacing="0" cellpadding="0">
		<tr>			
			<td>
				<input type="radio" name="hType" value=1 <?if($hType == 1 or $hType=="") echo "checked";?>> �ѹ����駧ҹ<br>
				<input type="radio" name="hType" value=2 <?if($hType == 2 ) echo "checked";?>> �ѹ��������ͧ<br>
			</td>
			<td valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>			
				<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay value="<?=$hInsureDay?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth value="<?=$hInsureMonth?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hInsureYear value="<?=$hInsureYear?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear,hInsureDay, hInsureMonth, hInsureYear,popCal);return false"></td>		
				<td>&nbsp;&nbsp;�֧&nbsp;&nbsp;</td>
				<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay1 value="<?=$hInsureDay1?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth1 value="<?=$hInsureMonth1?>"></td>
				<td>-</td>
				<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hInsureYear1 value="<?=$hInsureYear1?>"></td>
				<td>&nbsp;</td>
				<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear1,hInsureDay1, hInsureMonth1, hInsureYear1,popCal);return false"></td>		
				</tr>		
				</table>	
		</td>	
		</tr>		
		</table>			
		
	</td>
	</tr>
	<tr>
	<td>�Ң� : </td>
	<td>
			<?
			$objCList = new CompanyList();
			$objCList->setPageSize(0);
			$objCList->load();
			$objCList->printSelect("hCompanyId",$hCompanyId,"��س����͡�Ң�");
			?>	
	
	</td>
	</tr>			
	<tr>
	<td></td>
	<td><input type="submit" name="hSearch" value="Search"></td>
	</tr>		

</table>

</form>	
<?if($hSearch){
	$addDay= date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")+15, date("Y")));

	$hInsureSDate = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
	$hInsureEDate = $hInsureYear1."-".$hInsureMonth1."-".$hInsureDay1;
	
	if($hInsureSDate != "--"){
		if($hType == 1){
			$strDate = " �ѹ����駧ҹ ".$hInsureDay."-".$hInsureMonth."-".$hInsureYear." �֧ ".$hInsureDay1."-".$hInsureMonth1."-".$hInsureYear1;
			$strCondition="   insure_date >= '$hInsureSDate' and insure_date <= '$hInsureEDate' and  (p_year <>'�շ�� 1' ) and P.status <> 'cancel'  ";
			$strCondition2="   insure_date >= '2013-07-01' and   insure_date < '$hInsureSDate'  and  (p_year <> '�շ�� 1' )  and P.status <> 'cancel'   ";
			$strCondition3="  P.insure_date >= '$hInsureSDate' and P.insure_date <= '$hInsureEDate' and  (p_year <> '�շ�� 1' )  and P.status <> 'cancel' ";
		}

		if($hType == 2){
			$strDate = " �ѹ��������ͧ ".$hInsureDay."-".$hInsureMonth."-".$hInsureYear." �֧ ".$hInsureDay1."-".$hInsureMonth1."-".$hInsureYear1;
			$strCondition=" (  (  k_start_date >= '$hInsureSDate' and k_start_date <= '$hInsureEDate' ) or ( p_start_date >= '$hInsureSDate' and p_start_date <= '$hInsureEDate' )   )  and  (p_year <> '�շ�� 1' ) and P.status <> 'cancel'   ";
			$strCondition2="   (  (  k_start_date >= '2013-07-01' and k_start_date < '$hInsureSDate' ) or ( p_start_date >= '2013-07-01' and p_start_date < '$hInsureSDate' )   )  and  (p_year <> '�շ�� 1' ) and P.status <> 'cancel'   ";
			$strCondition3=" (  (  k_start_date >= '$hInsureSDate' and k_start_date <= '$hInsureEDate' ) or ( p_start_date >= '$hInsureSDate' and p_start_date <= '$hInsureEDate' )   )  and  (p_year <> '�շ�� 1' ) and P.status <> 'cancel'  ";
		}
	
	}
	
	
	
	if($hCompanyId != ""){
		if($strCondition != ""){
			$strCondition.=" and  (  P.company_id =  $hCompanyId  )  ";
		}else{
			$strCondition = " (  P.company_id =  $hCompanyId  )  ";
		}
		$sqlCompany =" and (  P.company_id =  $hCompanyId  )  ";
	}

	if($hType==1){
	
	//**************************************************************************************************************  $hType = 1 ********************************************************************************************************************************
	
	$objInsureList = new InsureList();
	$objInsureList->setFilter($strCondition);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect ASC ");
	$objInsureList->loadWinspeed();	
	
	if($objInsureList->mCount > 0){
		$hCheck1=0;
		forEach($objInsureList->getItemList() as $objItem) {
			if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" ){
				$hCheck1++;
			}
			if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""  ){
				$hCheck1++;
			}
		}
	
	?>
<br><br>
<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">

<h2>��¡���駧ҹ���˹��յ���ѧ������Ǩ�ͺ (<?=$hCheck1?>)</h2>

<?if($hCheck1 > 0){?>

<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">�������<br>���˹��</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ<br>����͹</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ����駧ҹ</td>
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">price</td>
	<td valign="top"  width="" align="center" class="ListTitle">��¡�ê���</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����������/�ú.</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����Ѻ��</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem) {

	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
?>
<input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="hidden" name="hInsureDate[<?=$i?>]" value="<?=$objItem->get_insure_date()?>"><input type="hidden" name="hOldK[<?=$i?>]" value="<?=$objItem->get_ws_vendor_k()?>"><input type="hidden" name="hOldP[<?=$i?>]" value="<?=$objItem->get_ws_vendor_p()?>">
<?	
	if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_k_prb_id());
	$objInsureCompany->load();	
	
	$objInsureItemList = new InsureItemList();
	$objInsureItemList->setFilter(" P.insure_id = '".$objItem->get_insure_id()."' and  P.payment_status = '��������' ");
	$objInsureItemList->setPageSize(0);
	$objInsureItemList->setSort(" payment_order ASC ");
	$objInsureItemList->load();	
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdK[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_insure_date())?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerK[<?=$i?>]" value="<?=$objItem->get_k_broker_id()?>"><input type="hidden" name="hPrbK[<?=$i?>]" value="<?=$objItem->get_k_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateK[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameK[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceK[<?=$i?>]" size="20" value="<?=$objItem->get_k_num05();?>"><?=number_format($objItem->get_k_num05(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<table width="250" border="1">
		<?forEach($objInsureItemList->getItemList() as $objItem01) {
		
				$objInsureItemDetail = new InsureItemDetail();
				$hKSum = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=40   )  and price_acc > 0 ");
		?>
		<tr>
			<td width="100" align="center"><?=formatShortDate($objItem01->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objItem01->get_insure_item_id()?>&hInsureId=<?=$objItem01->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($hKSum,2);?></a></td>
			<td width="50" align="center"><?=$objItem01->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		<?}?>	
		</table>
	</td>	
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_k_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>

<?
	if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""  ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_p_prb_id());
	$objInsureCompany->load();	
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdP[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_insure_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerP[<?=$i?>]" value="<?=$objItem->get_p_broker_id()?>"><input type="hidden" name="hPrbP[<?=$i?>]" value="<?=$objItem->get_p_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateP[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameP[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceP[<?=$i?>]" size="20" value="<?=$objItem->get_p_prb_price();?>"><?=number_format($objItem->get_p_prb_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail">
		<?
		$objInsureItemDetail = new InsureItemDetail();
		if($objInsureItemDetail->loadByCondition("  order_extra_id = '".$objItem->get_insure_id()."' and payment_subject_id = 39 and price_acc > 0  ")){
			$objInsureItem = new InsureItem();
			$objInsureItem->set_insure_item_id($objInsureItemDetail->getOrderId());
			$objInsureItem->load();
		?>
	
		<table width="250" border="1">
		<tr>
			<td width="100" align="center"><?=formatShortDate($objInsureItem->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objInsureItemDetail->getOrderId()?>&hInsureId=<?=$objInsureItem->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($objInsureItemDetail->getPriceAcc(),2);?></a></td>
			<td width="50" align="center"><?=$objInsureItem->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		</table>
		
	<?}?>					
	</td>	
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_p_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>
<?$i++;}//for each?>
<tr>
	<td colspan="11" align="center" height="30"><input type="hidden" name="hCount1" value="<?=$i?>"><input type="submit" name="hSubmit1" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>
<?}//hCheck1?>


<?}//count insure?>

<?
	$objInsureList = new InsureList();
	$objInsureList->setFilter($strCondition2.$sqlCompany);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect ASC ");
	$objInsureList->loadWinspeed();	
	if($objInsureList->mCount > 0){
		$hCheck2=0;
		forEach($objInsureList->getItemList() as $objItem) {
			if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" ){
				$hCheck2++;
			}
			if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""  ){
				$hCheck2++;
			}
		}	
	
	?>

<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">

<h2>��¡���駧ҹ���˹��յ�ͤ�ҧ��Ǩ�ͺ������ (<?=$hCheck2?>)</h2>

<?if($hCheck2 > 0){?>

<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">�������<br>���˹��</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ<br>����͹</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ����駧ҹ</td>	
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">price</td>
	<td valign="top"  width="" align="center" class="ListTitle">��¡�ê���</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����������/�ú.</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����Ѻ��</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem) {

	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
	
?>
<input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="hidden" name="hInsureDate[<?=$i?>]" value="<?=$hInsureSDate?>"><input type="hidden" name="hOldK[<?=$i?>]" value="<?=$objItem->get_ws_vendor_k()?>"><input type="hidden" name="hOldP[<?=$i?>]" value="<?=$objItem->get_ws_vendor_p()?>">
<?	
	if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_k_prb_id());
	$objInsureCompany->load();	
	
	$objInsureItemList = new InsureItemList();
	$objInsureItemList->setFilter(" P.insure_id = '".$objItem->get_insure_id()."' and  P.payment_status = '��������' ");
	$objInsureItemList->setPageSize(0);
	$objInsureItemList->setSort(" payment_order ASC ");
	$objInsureItemList->load();		
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdK[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_insure_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerK[<?=$i?>]" value="<?=$objItem->get_k_broker_id()?>"><input type="hidden" name="hPrbK[<?=$i?>]" value="<?=$objItem->get_k_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateK[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameK[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceK[<?=$i?>]" size="20" value="<?=$objItem->get_k_num05();?>"><?=number_format($objItem->get_k_num05(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<table width="250" border="1">
		<?forEach($objInsureItemList->getItemList() as $objItem01) {
		
				$objInsureItemDetail = new InsureItemDetail();
				$hKSum = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=40   )  and price_acc > 0 ");
		?>
		<tr>
			<td width="100" align="center"><?=formatShortDate($objItem01->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objItem01->get_insure_item_id()?>&hInsureId=<?=$objItem01->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($hKSum,2);?></a></td>
			<td width="50" align="center"><?=$objItem01->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		<?}?>	
		</table>
	</td>		
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_k_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>

<?
	if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""  ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_p_prb_id());
	$objInsureCompany->load();	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdP[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_insure_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerP[<?=$i?>]" value="<?=$objItem->get_p_broker_id()?>"><input type="hidden" name="hPrbP[<?=$i?>]" value="<?=$objItem->get_p_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateP[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameP[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceP[<?=$i?>]" size="20" value="<?=$objItem->get_p_prb_price();?>"><?=number_format($objItem->get_p_prb_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail">
		<?
		$objInsureItemDetail = new InsureItemDetail();
		if($objInsureItemDetail->loadByCondition("  order_extra_id = '".$objItem->get_insure_id()."' and payment_subject_id = 39 and price_acc > 0  ")){
			$objInsureItem = new InsureItem();
			$objInsureItem->set_insure_item_id($objInsureItemDetail->getOrderId());
			$objInsureItem->load();
		?>
	
		<table width="250" border="1">
		<tr>
			<td width="100" align="center"><?=formatShortDate($objInsureItem->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objInsureItemDetail->getOrderId()?>&hInsureId=<?=$objInsureItem->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($objInsureItemDetail->getPriceAcc(),2);?></a></td>
			<td width="50" align="center"><?=$objInsureItem->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		</table>
		
	<?}?>					
	</td>		
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_p_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?&hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>
<?$i++;}//for each?>
<tr>
	<td colspan="11" align="center" height="30"><input type="hidden" name="hCount2" value="<?=$i?>"><input type="submit" name="hSubmit2" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>

<?}//hCheck2?>

<?}//count insure?>










<?
	$objInsureList = new InsureWSVendorList();
	$objInsureList->setFilter($strCondition3.$sqlCompany);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect, P.insure_id ASC ");
	$objInsureList->load();	

	if($objInsureList->mCount > 0){
	?>


<form target="_blank" action="insure_winspeed_vendor_renew_export.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td><h2>��¡���駧ҹ��Ǩ�ͺ�����������˹��յ�� (<?=$objInsureList->mCount?>)</h2></td>
	<td align="right"><input type="submit" name="hExport" value="Export Excel"></td>
</tr>
</table>
</form>

<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">


<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">͹��ѵ�</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ����駧ҹ</td>	
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docuno</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipdate</td>
	<td valign="top"  width="" align="center" class="ListTitle">inv no</td>
	<td valign="top"  width="" align="center" class="ListTitle">inv date</td>
	<td valign="top"  width="" align="center" class="ListTitle">credit day</td>
	<td valign="top"  width="" align="center" class="ListTitle">pay date</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">Qty</td>
	<td valign="top"  width="" align="center" class="ListTitle">Price</td>
	<td valign="top"  width="" align="center" class="ListTitle">amount</td>
	<td valign="top"  width="" align="center" class="ListTitle">inven</td>
	<td valign="top"  width="" align="center" class="ListTitle">location</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem1) {

	$objItem = new Insure();
	$objItem->set_insure_id($objItem1->get_insure_id());
	$objItem->load();
	
	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem1->get_prb_id());
	$objInsureCompany->load();
	
	$objCompany = new Company();
	$objCompany->setCompanyId($objItem->get_company_id());
	$objCompany->load();	
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
	
	$arrD09 = explode("-",$objItem1->get_ws_vendor_date());
	
	$Day09 = $arrD09[2];
	$Month09 = $arrD09[1];
	$Year09 = $arrD09[0];
	$addDay= date("d/m/Y", mktime(0, 0, 0, $Month09, $Day09+15, $Year09));
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hStatus[<?=$i?>]" value="Y" <?if($objItem1->get_ws_vendor_status() == "Y") echo "checked";?>></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hWsVendorId[<?=$i?>]" value="<?=$objItem1->get_ws_vendor_id()?>"><input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hCancel[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_insure_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="text" name="hDocuno[<?=$i?>]" size="20" value="<?=$objItem1->get_ws_vendor_no();?>"></td>
	<td valign="top" align="center" class="ListDetail1">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle"  size="2" maxlength="2"   name=Day09_<?=$i?> value="<?=$Day09?>"></td>
			<td>/</td>
			<td><INPUT align="middle"  size="2" maxlength="2"  name=Month09_<?=$i?> value="<?=$Month09?>"></td>
			<td>/</td>
			<td><INPUT align="middle"  size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year09_<?=$i?> value="<?=$Year09?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year09_<?=$i?>,Day09_<?=$i?>, Month09_<?=$i?>, Year09_<?=$i?>,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
	<td valign="top" align="center" class="ListDetail1"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<input type="hidden" name="hWsVendorType[<?=$i?>]" value="<?=$objItem1->get_ws_vendor_type()?>">
	<?if($objItem1->get_ws_vendor_type() == "K"){?>
	<td valign="top" align="center" class="ListDetail1"><?=$Day09?>/<?=$Month09?>/<?=$Year09?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCustomer->getFirstname()." ".$objIC->get_car_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$addDay;?></td>
	<td valign="top" align="center" class="ListDetail1">15</td>
	<td valign="top" align="center" class="ListDetail1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1">1</td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getInven();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getLocation();?></td>	
	<?}else{?>
	<td valign="top" align="center" class="ListDetail1"><?=$Day09?>/<?=$Month09?>/<?=$Year09?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCustomer->getFirstname()." ".$objIC->get_car_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$addDay;?></td>
	<td valign="top" align="center" class="ListDetail1">15</td>
	<td valign="top" align="center" class="ListDetail1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1">1</td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getInven();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getLocation();?></td>	
	<?}?>

</tr>	
<?$i++;}//for each?>
<tr>
	<td colspan="19" align="center" height="30"><input type="hidden" name="hCount3" value="<?=$i?>"><input type="submit" name="hSubmit3" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>



<?}//count insure?>



<?}//end type1?>

<?	//**************************************************************************************************************  $hType = 1 ********************************************************************************************************************************   ?>



<?
	if($hType==2){
	
	//**************************************************************************************************************  $hType = 2 ********************************************************************************************************************************
	
	$objInsureList = new InsureList();
	$objInsureList->setFilter($strCondition);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect ASC ");
	$objInsureList->loadWinspeed();	
	
	if($objInsureList->mCount > 0){
		$hCheck1=0;
		forEach($objInsureList->getItemList() as $objItem) {
			if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == ""  and ( $objItem->get_k_start_date() >= $hInsureSDate  and $objItem->get_k_start_date() <= $hInsureEDate  )   ){
				$hCheck1++;
			}
			if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""    and ( $objItem->get_p_start_date() >= $hInsureSDate  and $objItem->get_p_start_date() <= $hInsureEDate  )   ){
				$hCheck1++;
			}
		}
	
	?>
<br><br>
<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">

<h2>��¡���駧ҹ���˹��յ���ѧ������Ǩ�ͺ (<?=$hCheck1?>)</h2>

<?if($hCheck1 > 0){?>

<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">�������<br>���˹��</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ<br>����͹</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">price</td>
	<td valign="top"  width="" align="center" class="ListTitle">��¡�ê���</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����������/�ú.</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����Ѻ��</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem) {

	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
?>
<input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="hidden" name="hInsureDate[<?=$i?>]" value="<?=$objItem->get_insure_date()?>"><input type="hidden" name="hOldK[<?=$i?>]" value="<?=$objItem->get_ws_vendor_k()?>"><input type="hidden" name="hOldP[<?=$i?>]" value="<?=$objItem->get_ws_vendor_p()?>">
<?	
	if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" and ( $objItem->get_k_start_date() >= $hInsureSDate  and $objItem->get_k_start_date() <= $hInsureEDate  ) ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_k_prb_id());
	$objInsureCompany->load();	
	
	$objInsureItemList = new InsureItemList();
	$objInsureItemList->setFilter(" P.insure_id = '".$objItem->get_insure_id()."' and  P.payment_status = '��������' ");
	$objInsureItemList->setPageSize(0);
	$objInsureItemList->setSort(" payment_order ASC ");
	$objInsureItemList->load();		
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdK[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_k_start_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerK[<?=$i?>]" value="<?=$objItem->get_k_broker_id()?>"><input type="hidden" name="hPrbK[<?=$i?>]" value="<?=$objItem->get_k_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateK[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameK[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceK[<?=$i?>]" size="20" value="<?=$objItem->get_k_num05();?>"><?=number_format($objItem->get_k_num05(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<table width="250" border="1">
		<?forEach($objInsureItemList->getItemList() as $objItem01) {
				$objInsureItemDetail = new InsureItemDetail();
				$hKSum = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=40   ) and price_acc > 0  ");
		?>
		<tr>
			<td width="100" align="center"><?=formatShortDate($objItem01->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objItem01->get_insure_item_id()?>&hInsureId=<?=$objItem01->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($hKSum,2);?></a></td>
			<td width="50" align="center"><?=$objItem01->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		<?}?>	
		</table>
	</td>
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_k_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>

<?
	if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""  and ( $objItem->get_p_start_date() >= $hInsureSDate  and $objItem->get_p_start_date() <= $hInsureEDate  ) ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_p_prb_id());
	$objInsureCompany->load();	
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdP[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerP[<?=$i?>]" value="<?=$objItem->get_p_broker_id()?>"><input type="hidden" name="hPrbP[<?=$i?>]" value="<?=$objItem->get_p_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateP[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameP[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceP[<?=$i?>]" size="20" value="<?=$objItem->get_p_prb_price();?>"><?=number_format($objItem->get_p_prb_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<?
		$objInsureItemDetail = new InsureItemDetail();
		if($objInsureItemDetail->loadByCondition("  order_extra_id = '".$objItem->get_insure_id()."' and payment_subject_id = 39  and price_acc > 0 ")){
			$objInsureItem = new InsureItem();
			$objInsureItem->set_insure_item_id($objInsureItemDetail->getOrderId());
			$objInsureItem->load();
		?>
	
		<table width="250" border="1">
		<tr>
			<td width="100" align="center"><?=formatShortDate($objInsureItem->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objInsureItemDetail->getOrderId()?>&hInsureId=<?=$objInsureItem->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($objInsureItemDetail->getPriceAcc(),2);?></a></td>
			<td width="50" align="center"><?=$objInsureItem->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		</table>
		
	<?}?>					
	</td>		
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_p_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>
<?$i++;}//for each?>
<tr>
	<td colspan="11" align="center" height="30"><input type="hidden" name="hCount1" value="<?=$i?>"><input type="submit" name="hSubmit1" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>
<?}//hCheck1?>


<?}//count insure?>

<?
	$objInsureList = new InsureList();
	$objInsureList->setFilter($strCondition2.$sqlCompany);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect ASC ");
	$objInsureList->loadWinspeed();	
	if($objInsureList->mCount > 0){
		$hCheck2=0;
		forEach($objInsureList->getItemList() as $objItem) {
			if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == ""  and ( $objItem->get_k_start_date() >= '2013-07-01'  and $objItem->get_k_start_date() < $hInsureSDate  )   ){
				$hCheck2++;
			}
			if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == ""    and ( $objItem->get_p_start_date() >= '2013-07-01'   and $objItem->get_p_start_date() < $hInsureSDate  )   ){
				$hCheck2++;
			}
		}	
	
	?>

<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">

<h2>��¡���駧ҹ���˹��յ�ͤ�ҧ��Ǩ�ͺ������ (<?=$hCheck2?>)</h2>

<?if($hCheck2 > 0){?>

<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">�������<br>���˹��</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ<br>����͹</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">price</td>
	<td valign="top"  width="" align="center" class="ListTitle">��¡�ê���</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����������/�ú.</td>
	<td valign="top"  width="" align="center" class="ListTitle">�Ţ����Ѻ��</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem) {

	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
	
?>
<input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="hidden" name="hInsureDate[<?=$i?>]" value="<?=$hInsureSDate?>"><input type="hidden" name="hOldK[<?=$i?>]" value="<?=$objItem->get_ws_vendor_k()?>"><input type="hidden" name="hOldP[<?=$i?>]" value="<?=$objItem->get_ws_vendor_p()?>">
<?	
	if($objItem->get_k_check() == 1  and $objItem->get_ws_vendor_k() == "" and ( $objItem->get_k_start_date() >= '2013-07-01'  and $objItem->get_k_start_date() < $hInsureSDate  ) ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_k_prb_id());
	$objInsureCompany->load();	
	
	$objInsureItemList = new InsureItemList();
	$objInsureItemList->setFilter(" P.insure_id = '".$objItem->get_insure_id()."' and  P.payment_status = '��������' ");
	$objInsureItemList->setPageSize(0);
	$objInsureItemList->setSort(" payment_order ASC ");
	$objInsureItemList->load();		
	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdK[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelK[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_k_start_date())?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerK[<?=$i?>]" value="<?=$objItem->get_k_broker_id()?>"><input type="hidden" name="hPrbK[<?=$i?>]" value="<?=$objItem->get_k_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateK[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameK[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceK[<?=$i?>]" size="20" value="<?=$objItem->get_k_num05();?>"><?=number_format($objItem->get_k_num05(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<table width="250" border="1">
		<?forEach($objInsureItemList->getItemList() as $objItem01) {
				$objInsureItemDetail = new InsureItemDetail();
				$hKSum = $objInsureItemDetail->loadSumInsure(" order_extra_id = ".$objItem->get_insure_id()."  and ( payment_subject_id=40   )  and price_acc > 0 ");
		
		?>
		<tr>
			<td width="100" align="center"><?=formatShortDate($objItem01->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objItem01->get_insure_item_id()?>&hInsureId=<?=$objItem01->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($hKSum,2);?></a></td>
			<td width="50" align="center"><?=$objItem01->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		<?}?>	
		</table>
	</td>		
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_k_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>

<?
	if($objItem->get_p_check() == 1   and $objItem->get_ws_vendor_p() == "" and ( $objItem->get_p_start_date() >= '2013-07-01'   and $objItem->get_p_start_date() < $hInsureSDate  )  ){
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem->get_p_prb_id());
	$objInsureCompany->load();	
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hInsureIdP[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hApproveP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hCancelP[<?=$i?>]" value="1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hBrokerP[<?=$i?>]" value="<?=$objItem->get_p_broker_id()?>"><input type="hidden" name="hPrbP[<?=$i?>]" value="<?=$objItem->get_p_prb_id()?>""><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hDateP[<?=$i?>]" size="20" value="<?=$objItem->get_insure_date()?>"><?=formatShortDate($objItem->get_insure_date());?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hNameP[<?=$i?>]" size="20" value="<?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?>"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hPriceP[<?=$i?>]" size="20" value="<?=$objItem->get_p_prb_price();?>"><?=number_format($objItem->get_p_prb_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1">
		<?
		$objInsureItemDetail = new InsureItemDetail();
		if($objInsureItemDetail->loadByCondition("  order_extra_id = '".$objItem->get_insure_id()."' and payment_subject_id = 39  and price_acc > 0 ")){
			$objInsureItem = new InsureItem();
			$objInsureItem->set_insure_item_id($objInsureItemDetail->getOrderId());
			$objInsureItem->load();
		?>
	
		<table width="250" border="1">
		<tr>
			<td width="100" align="center"><?=formatShortDate($objInsureItem->get_payment_date())?></td>
			<td width="100" align="right"><a href="../cashier/icardPrintPreviewTemp.php?hInsureItemId=<?=$objInsureItemDetail->getOrderId()?>&hInsureId=<?=$objInsureItem->get_insure_id()?>&hCustomerId=<?=$objItem->get_customer_id()?>" target="_blank"><?=number_format($objInsureItemDetail->getPriceAcc(),2);?></a></td>
			<td width="50" align="center"><?=$objInsureItem->get_payment_order()?>/<?=$objItem->get_pay_number()?></td>
		</tr>
		</table>
		
	<?}?>					
	</td>		
	<td valign="top" align="center" class="ListDetail1"><?=$objItem->get_p_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><a target="_blank" href="../insurance/frm_02_07_pdf.php?hAddressType=4&hId=<?=$objItem->get_insure_id()?>"><?=$objItem->get_p_rnum();?></a></td>
</tr>	
<?}?>
<?$i++;}//for each?>
<tr>
	<td colspan="11" align="center" height="30"><input type="hidden" name="hCount2" value="<?=$i?>"><input type="submit" name="hSubmit2" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>

<?}//hCheck2?>

<?}//count insure?>










<?
	$objInsureList = new InsureWSVendorList();
	$objInsureList->setFilter($strCondition3.$sqlCompany);
	$objInsureList->setPageSize(0);
	$objInsureList->setSort(" date_protect, P.insure_id ASC ");
	$objInsureList->load();	

	if($objInsureList->mCount > 0){
	?>


<form target="_blank" action="insure_winspeed_vendor_renew_export.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td><h2>��¡���駧ҹ��Ǩ�ͺ�����������˹��յ�� (<?=$objInsureList->mCount?>)</h2></td>
	<td align="right"><input type="submit" name="hExport" value="Export Excel"></td>
</tr>
</table>
</form>

<form action="insure_winspeed_vendor_renew.php" method="post">
<input type="hidden" name="hSearch" value="search">
<input type="hidden" name="hInsureDay" value="<?=$hInsureDay?>">
<input type="hidden" name="hInsureMonth" value="<?=$hInsureMonth?>">
<input type="hidden" name="hInsureYear" value="<?=$hInsureYear?>">
<input type="hidden" name="hInsureDay1" value="<?=$hInsureDay1?>">
<input type="hidden" name="hInsureMonth1" value="<?=$hInsureMonth1?>">
<input type="hidden" name="hInsureYear1" value="<?=$hInsureYear1?>">
<input type="hidden" name="hType" value="<?=$hType?>">
<input type="hidden" name="hCompanyId" value="<?=$hCompanyId?>">


<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="" align="center" class="ListTitle">͹��ѵ�</td>
	<td valign="top" width="" align="center" class="ListTitle">¡��ԡ</td>
	<td valign="top" width="" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td valign="top" width="" align="center" class="ListTitle">vendor</td>
	<td valign="top"  width="" align="center" class="ListTitle">docuno</td>
	<td valign="top"  width="" align="center" class="ListTitle">docudate</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipno</td>
	<td valign="top"  width="" align="center" class="ListTitle">shipdate</td>
	<td valign="top"  width="" align="center" class="ListTitle">inv no</td>
	<td valign="top"  width="" align="center" class="ListTitle">inv date</td>
	<td valign="top"  width="" align="center" class="ListTitle">credit day</td>
	<td valign="top"  width="" align="center" class="ListTitle">pay date</td>
	<td valign="top"  width="" align="center" class="ListTitle">part no</td>
	<td valign="top"  width="" align="center" class="ListTitle">part name</td>
	<td valign="top"  width="" align="center" class="ListTitle">Qty</td>
	<td valign="top"  width="" align="center" class="ListTitle">Price</td>
	<td valign="top"  width="" align="center" class="ListTitle">amount</td>
	<td valign="top"  width="" align="center" class="ListTitle">inven</td>
	<td valign="top"  width="" align="center" class="ListTitle">location</td>
</tr>	
<?
	$i=0;
	forEach($objInsureList->getItemList() as $objItem1) {

	$objItem = new Insure();
	$objItem->set_insure_id($objItem1->get_insure_id());
	$objItem->load();
	
	$objCar = new InsureCar();
	$objCar->set_car_id($objItem->get_car_id());
	$objCar->load();
	
	$objOrder = new Order();
	if($objCar->get_order_id() > 0){
		$objOrder->setOrderId($objCar->get_order_id());
		$objOrder->load();
	}
	
	$objInsureCompany = new InsureCompany();
	$objInsureCompany->setInsureCompanyId($objItem1->get_prb_id());
	$objInsureCompany->load();
	
	$objCompany = new Company();
	$objCompany->setCompanyId($objItem->get_company_id());
	$objCompany->load();	
	
	$objInsureCustomer = new InsureCustomer();
	$objInsureCustomer->setInsureCustomerId($objItem->get_customer_id());
	$objInsureCustomer->load();
	
	$objIC = new InsureCar();
	$objIC->set_car_id($objItem->get_car_id());
	$objIC->load();
	
	$arrD09 = explode("-",$objItem1->get_ws_vendor_date());
	
	$Day09 = $arrD09[2];
	$Month09 = $arrD09[1];
	$Year09 = $arrD09[0];
	$addDay= date("d/m/Y", mktime(0, 0, 0, $Month09, $Day09+15, $Year09));
?>
<tr <?if(fmod($i,2)==0){?>bgcolor="#e8e8e8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#e8e8e8';" <?}else{?>bgcolor="#f8f8f8"  onMouseOver="this.bgColor='#c7d6f8';" onMouseOut="this.bgColor='#f8f8f8';" <?}?>>
	<td valign="top" align="center" class="ListDetail1"><input type="checkbox" name="hStatus[<?=$i?>]" value="Y" <?if($objItem1->get_ws_vendor_status() == "Y") echo "checked";?>></td>
	<td valign="top" align="center" class="ListDetail1"><input type="hidden" name="hWsVendorId[<?=$i?>]" value="<?=$objItem1->get_ws_vendor_id()?>"><input type="hidden" name="hInsure[<?=$i?>]" value="<?=$objItem->get_insure_id()?>"><input type="checkbox" name="hCancel[<?=$i?>]" value="1"></td>
	<?if($objItem1->get_ws_vendor_type() == "K"){?>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_k_start_date())?></td>
	<?}else{?>
	<td valign="top" align="center" class="ListDetail1"><?=formatShortDate($objItem->get_p_start_date())?></td>
	<?}?>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getVendor()?></td>
	<td valign="top" align="center" class="ListDetail1"><input type="text" name="hDocuno[<?=$i?>]" size="20" value="<?=$objItem1->get_ws_vendor_no();?>"></td>
	<td valign="top" align="center" class="ListDetail1">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle"  size="2" maxlength="2"   name=Day09_<?=$i?> value="<?=$Day09?>"></td>
			<td>/</td>
			<td><INPUT align="middle"  size="2" maxlength="2"  name=Month09_<?=$i?> value="<?=$Month09?>"></td>
			<td>/</td>
			<td><INPUT align="middle"  size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year09_<?=$i?> value="<?=$Year09?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year09_<?=$i?>,Day09_<?=$i?>, Month09_<?=$i?>, Year09_<?=$i?>,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
	<td valign="top" align="center" class="ListDetail1"><a href="insure_detail.php?hInsureId=<?=$objItem->get_insure_id()?>&hCarId=<?=$objIC->get_car_id()?>" target="_blank"><?=$objIC->get_code()." ".$objInsureCustomer->getFirstname();?></a></td>
	<input type="hidden" name="hWsVendorType[<?=$i?>]" value="<?=$objItem1->get_ws_vendor_type()?>">
	<?if($objItem1->get_ws_vendor_type() == "K"){?>
	<td valign="top" align="center" class="ListDetail1"><?=$Day09?>/<?=$Month09?>/<?=$Year09?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCustomer->getFirstname()." ".$objIC->get_car_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$addDay;?></td>
	<td valign="top" align="center" class="ListDetail1">15</td>
	<td valign="top" align="center" class="ListDetail1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo();?></td>
	<td valign="top" align="center" class="ListDetail1">��Сѹ���</td>
	<td valign="top" align="center" class="ListDetail1">1</td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getInven();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getLocation();?></td>	
	<?}else{?>
	<td valign="top" align="center" class="ListDetail1"><?=$Day09?>/<?=$Month09?>/<?=$Year09?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCustomer->getFirstname()." ".$objIC->get_car_number();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$addDay;?></td>
	<td valign="top" align="center" class="ListDetail1">15</td>
	<td valign="top" align="center" class="ListDetail1"></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objInsureCompany->getPartNo1();?></td>
	<td valign="top" align="center" class="ListDetail1">�ú</td>
	<td valign="top" align="center" class="ListDetail1">1</td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=number_format($objItem1->get_ws_vendor_price(),2);?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getInven();?></td>
	<td valign="top" align="center" class="ListDetail1"><?=$objCompany->getLocation();?></td>	
	<?}?>

</tr>	
<?$i++;}//for each?>
<tr>
	<td colspan="19" align="center" height="30"><input type="hidden" name="hCount3" value="<?=$i?>"><input type="submit" name="hSubmit3" value="�ѹ�֡��¡��"></td>
</tr>
</table>
</form>



<?}//count insure?>



<?}//end type2?>

	<?//**************************************************************************************************************  $hType = 2 ********************************************************************************************************************************?>









<?}//end search?>



<?
	include("h_footer.php")
?>

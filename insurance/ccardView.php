<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objSendingCustomer = new Customer();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();
$objCarSeries = new CarSeries();
$objSendingEvent = new CustomerEvent();

$objEvent = new CustomerEvent();
$objOrderStockList = new OrderStockList();
$objOrderStockPremiumList = new OrderStockList();
$objOrderStockOtherList = new OrderStockOtherList();
$objOrderStockOtherPremiumList = new OrderStockOtherList();
$objStockProductList = new StockProductList();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomer = new Customer();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objCustomerEventList = new CustomerEventList();
$objCustomerEventList->setPageSize(0);
$objCustomerEventList->setSortDefault("title ASC");
$objCustomerEventList->load();

$objCarTypeList = new CarTypeList();
$objCarTypeList->setPageSize(0);
$objCarTypeList->setSortDefault("title ASC");
$objCarTypeList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModel1List = new CarSeriesList();
$objCarModel1List->setPageSize(0);
$objCarModel1List->setSortDefault(" model ASC");
$objCarModel1List->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objStockPremiumList = new StockPremiumList();
$objStockPremiumList->setPageSize(0);
$objStockPremiumList->setSortDefault(" title ASC");
$objStockPremiumList->setSort($hSort);
$objStockPremiumList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objStockPremiumList = new StockProductList();
$objStockPremiumList->setFilter(" SPT.code= 'F001' ");
$objStockPremiumList->setPageSize(0);
$objStockPremiumList->setSortDefault(" SP.tmt,  SP.title ASC");
$objStockPremiumList->setSort($hSort);
$objStockPremiumList->loadCRL();

$objStockProductList = new StockProductList();



$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();		
		
		$arrDate = explode("-",$objOrder->getSendingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getPrbExpire());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		if(isset($hSearchSendingCustomerId)){
			$hCustomerId = $hSearchSendingCustomerId;		
			$objSendingCustomer->setCustomerId($hSearchSendingCustomerId);
			$objSendingCustomer->load();
			
			$objOrder->setSendingCustomerId($hSearchSendingCustomerId);
			
			$strMode="Update";		
		}else{
			$hCustomerId = $objOrder->getSendingCustomerId();		
			$objSendingCustomer->setCustomerId($objOrder->getSendingCustomerId());
			$objSendingCustomer->load();
			$strMode="Update";		
		}
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();		
				
		
		$arrDate = explode("-",$objBookingCustomer->getInformationDate());
		$BookingDay = $arrDate[2];
		$BookingMonth = $arrDate[1];
		$BookingYear = $arrDate[0];
		
		$arrDate = explode("-",$objBookingCustomer->getBirthDay());
		$BookingDay01 = $arrDate[2];
		$BookingMonth01 = $arrDate[1];
		$BookingYear01 = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getInformationDate());
		$DaySending = $arrDate[2];
		$MonthSending = $arrDate[1];
		$YearSending = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getBirthDay());
		$Day01Sending = $arrDate[2];
		$Month01Sending = $arrDate[1];
		$Year01Sending = $arrDate[0];		
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();				
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSendingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();		
		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
		$objCarSeries->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		$objSendingEvent->setEventId($objSendingCustomer->getEventId());
		$objSendingEvent->load();

		$objOrderStockList->setFilter(" SPT.code= 'A001' AND order_id = ".$hId);
		$objOrderStockList->setPageSize(0);
		$objOrderStockList->setSortDefault(" SP.tmt, SP.title DESC");
		$objOrderStockList->setSort($hSort);
		$objOrderStockList->load();
		
		$objOrderStockPremiumList->setFilter(" SPT.code= 'F001'  AND order_id = ".$hId);
		$objOrderStockPremiumList->setPageSize(0);
		$objOrderStockPremiumList->setSortDefault("  SP.tmt, SP.title DESC");
		$objOrderStockPremiumList->setSort($hSort);
		$objOrderStockPremiumList->load();		
		
		$objOrderStockOtherList->setFilter(" type=1 AND order_id = ".$hId);
		$objOrderStockOtherList->setPageSize(0);
		$objOrderStockOtherList->setSortDefault(" title DESC");
		$objOrderStockOtherList->setSort($hSort);
		$objOrderStockOtherList->load();
		
		$objOrderStockOtherPremiumList->setFilter(" type=2 AND order_id = ".$hId);
		$objOrderStockOtherPremiumList->setPageSize(0);
		$objOrderStockOtherPremiumList->setSortDefault(" title DESC");
		$objOrderStockOtherPremiumList->setSort($hSort);
		$objOrderStockOtherPremiumList->load();		
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {
		$objOrder->setOrderId($hId);
		$objOrder->setCheckAccountSending(1);
		$objOrder->setCheckAccountSendingRemark($hRemark);
		$objOrder->setCheckAccountSendingCom($hCom);
		$objOrder->setCheckAccountSendingBy($sMemberId);
		$objOrder->setOrderStatus(5);
		$objOrder->updateAccountSending();
		header("location:ccardNewList.php");
		exit;

	}
}

$pageTitle = "1. �к��������١���";
$pageContent = "1.5 ����Ң����� ACARD &raquo;  �ʴ��������١��Ҩҡ CRL";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<script language=JavaScript >

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			document.frm01.hAddress01.value = document.frm01.hAddress.value;
			document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
			document.frm01.hProvince01.value = document.frm01.hProvince.value;
			document.frm01.hZip01.value = document.frm01.hZip.value;		
			document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax01.value = document.frm01.hFax.value;		
		}
   		if(val ==2){
			document.frm01.hAddress02.value = document.frm01.hAddress.value;
			document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
			document.frm01.hProvince02.value = document.frm01.hProvince.value;
			document.frm01.hZip02.value = document.frm01.hZip.value;		
			document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax02.value = document.frm01.hFax.value;		
		}
   		if(val ==3){
			document.frm01.hAddress03.value = document.frm01.hAddress.value;
			document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
			document.frm01.hProvince03.value = document.frm01.hProvince.value;
			document.frm01.hZip03.value = document.frm01.hZip.value;		
			document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax03.value = document.frm01.hFax.value;		
		}
   
   }

function Set_Display_TypeSendingCustomer(typeVal){
	document.getElementById("form_add01SendingCustomer").style.display = "none";
	document.getElementById("form_add02SendingCustomer").style.display = "none";
	document.getElementById("form_add03SendingCustomer").style.display = "none";
	document.getElementById("form_add04SendingCustomer").style.display = "none";
	document.getElementById("form_add01_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add02_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add03_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add04_detailSendingCustomer").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01SendingCustomer").style.display = "";
			document.getElementById("form_add01_detailSendingCustomer").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02SendingCustomer").style.display = "";
			document.getElementById("form_add02_detailSendingCustomer").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03SendingCustomer").style.display = "";
			document.getElementById("form_add03_detailSendingCustomer").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04SendingCustomer").style.display = "";
			document.getElementById("form_add04_detailSendingCustomer").style.display = "";
			break;
	}//switch
}


   function sameplaceSendingCustomer(val){
   		if(val ==1){
			document.frm01.hSendingCustomerAddress01.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon01.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur01.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince01.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip01.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel01.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax01.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==2){
			document.frm01.hSendingCustomerAddress02.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon02.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur02.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince02.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip02.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel02.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax02.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==3){
			document.frm01.hSendingCustomerAddress03.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon03.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur03.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince03.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip03.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel03.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax03.value = document.frm01.hSendingCustomerFax.value;		
		}
   
   }   
   
</script>

<br>


<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="ccardUpdate.php" method="POST"  onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objSendingCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objSendingCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objSendingCustomer->getCCard();?>">
	  <input type="hidden" name="hCustomerId"  value="<?=$objCustomer->getCustomerId();?>">
	  <input type="hidden" name="hSendingNumber" value="<?=$objOrder->getSendingNumber()?>">
	  <input type="hidden" name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>">	  	
	  <input type="hidden" name="hOrderStatus" value="1">
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingSaleId" value="<?=$objOrder->getBookingSaleId()?>">
	  <input type="hidden" name="hBookingCarColor" value="<?=$objOrder->getBookingCarColor()?>">
	  <input type="hidden" name="hBookingCarSeries" value="<?=$objOrder->getBookingCarSeries()?>">
	  <input type="hidden" name="hKeyword" value="<?=$hKeyword?>">
	  <input type="hidden" name="hSearch" value="<?=$hSearch?>">
	  <input type="hidden" name="hPage" value="<?=$hPage?>">

<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="95%" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><input type="text" readonly  name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ������ͺ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >
				<input type="hidden" readonly size="2" name="hSendingSaleId"  value="<?=$objOrder->getSendingSaleId();?>">
				<input type="text" readonly name="hSendingSaleName" size="30"   onKeyDown="if(event.keyCode==13 && frm01.hSendingSaleId.value != '' ) frm01.hOrderPrice.focus();if(event.keyCode !=13 ) frm01.hSendingSaleId.value='';"  value="<?=$hSendingSaleName?>">
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">
				<input type="checkbox" value="1" disabled=true name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>> ���ꡡó�����¹��ѡ�ҹ���
			</td>			
		</tr>
	
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>�ҤҢ��</strong></td>
			<td width="30%" valign="top" ><input type="text" readonly name="hOrderPrice" size="30"  value="<?=$objOrder->getOrderPrice()?>"></td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top"></td>	
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>������١���</strong> </td>
			<td width="30%">
				<?
				$objGroup  = new CustomerGroup();
				$objGroup->setGroupId($objBookingCustomer->getGroupId());
				$objGroup->load();
				
				echo $objGroup->getTitle();
				unset($objGroup);
				?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
				<?=$objBookingCustomer->getInformationDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���ʧҹ</strong> </td>
			<td valign="top">
				<?
				$objEvent  = new CustomerEvent();
				$objEvent->setEventId($objBookingCustomer->getEventId());
				$objEvent->load();
				
				echo $objEvent->getTitle();
				unset($objEvent);
				?>
			</td>
			<td class="i_background03"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td ><?=$hBookingSaleName?></td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><?=$objBookingCustomer->getIDCard();?></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?
				$objGrade  = new CustomerGrade();
				$objGrade->setGradeId($objBookingCustomer->getGradeId());
				$objGrade->load();
				
				echo $objGrade->getTitle();
				unset($objGrade);
				?>			
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table>
				<tr>
					<td valign="top">
						<?
						if($objBookingCustomer->getCustomerTitleId() > 0 ){
							echo $objBookingCustomer->getCustomerTitleDetail();
						}else{
							if($objBookingCustomer->getTitle() == ""){
									echo "�س";
							}else{ 
									echo $objBookingCustomer->getTitle(); 
							}
						}
						?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<?=$name?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getBirthDay()?></td>
		</tr>
		<tr>
			<td colspan="4" align="right">				
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>	

		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0" class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><?=$objBookingCustomer->getAmphur();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><?=$objBookingCustomer->getHomeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><?=$objBookingCustomer->getMobile();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><?=$objBookingCustomer->getOfficeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><?=$objBookingCustomer->getEmail();?>&nbsp;</td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress01();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince01();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip01();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel01();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax01();?>&nbsp;</td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress02();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon02();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur02();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince02();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip02();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel02();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax02();?>&nbsp;</td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince03();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip03();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel03();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax03();?>&nbsp;</td>
		</tr>		
		</table>
		
				
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
</span>
<table width="95%" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCustomer" onclick="setCustomerValue()" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹����Ѻ�ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>���觷���Ңͧ�١���</strong> </td>
			<td width="30%">
				<?$objCustomerGroupList->printSelect("hSendingCustomerGroupId",$objSendingCustomer->getGroupId(),"��س����͡");?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly  name=DaySending value="<?=$DaySending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2" readonly  name=MonthSending value="<?=$MonthSending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly onblur="checkYear(this,this.value);"  name=YearSending value="<?=$YearSending?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong></td>
			<td valign="top">
				<input type="hidden" size=3 name="hSendingEventId"  value="<?=$objSendingCustomer->getEventId()?>">
				<INPUT readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingEventId.value != '' ) frm01.hSendingCustomerSaleName.focus();if(event.keyCode !=13 ) frm01.hSendingEventId.value='';"   name="hEvent"  size=40 value="<?=$objSendingEvent->getTitle()?>">
				
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td >
				<input type="text" readonly size="3" name="hSendingCustomerSaleId"  value="<?=$objSendingCustomer->getSaleId();?>">
				<input readonly type="text" name="hSendingCustomerSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerSaleName.value != '' ) frm01.hSendingCustomerIDCard.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerSaleId.value='';"    value="<?=$hSendingSaleName?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><input readonly type="text" name="hSendingCustomerIDCard" size="30" maxlength="13"  value="<?=$objSendingCustomer->getIDCard();?>"></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGradeList->printSelect("hSendingCustomerGradeId",$objSendingCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hSendingCustomerTitle",$objSendingCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objSendingCustomer->getFirstname() != ""){
						$name = $objSendingCustomer->getFirstname()."  ".$objSendingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<INPUT readonly  name="hSendingCustomerName"  size="20"  size=30 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hSendingCustomerTitleId",$objSendingCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly  name=Day01Sending value="<?=$Day01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2" readonly  name=Month01Sending value="<?=$Month01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly  onblur="checkYear(this,this.value);"  name=Year01Sending value="<?=$Year01Sending?>"></td>
					<td>&nbsp;</td>				
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="right">
				
				<table id="form_add01SendingCustomer" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>					
					
					
		<table id=form_add01_detailSendingCustomer width="100%" cellpadding="2" cellspacing="0"  class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3"  name="hSendingCustomerTumbonCode"  value="<?=$objSendingCustomer->getTumbonCode();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode.value != '' ) frm01.hSendingCustomerAmphur.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode.value='';" name="hSendingCustomerTumbon" size="50"  value="<?=$objSendingCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3"  name="hSendingCustomerAmphurCode"  value="<?=$objSendingCustomer->getAmphurCode();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode.value != '' ) frm01.hSendingCustomerProvince.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode.value='';" name="hSendingCustomerAmphur" size="30"  value="<?=$objSendingCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode"  value="<?=$objSendingCustomer->getProvinceCode();?>"><input type="text" readonly  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode.value != '' ) frm01.hSendingCustomerZip.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode.value='';" name="hSendingCustomerProvince" size="30"  value="<?=$objSendingCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode"  value="<?=$objSendingCustomer->getZip();?>"><input type="text" readonly name="hSendingCustomerZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode.value != '' ) frm01.hSendingCustomerHomeTel.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode.value='';" size="30"  value="<?=$objSendingCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerHomeTel" size="30"  value="<?=$objSendingCustomer->getHomeTel();?>"></td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerMobile" size="30"  value="<?=$objSendingCustomer->getMobile();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" readonly name="hSendingCustomerOfficeTel" size="30"  value="<?=$objSendingCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax" size="30"  value="<?=$objSendingCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" readonly name="hSendingCustomerEmail" size="30"  value="<?=$objSendingCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hSendingCustomerIncomplete" value="1" <?if($objSendingCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hSendingCustomerMailback" value="1" <?if($objSendingCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>		
		</table>
		<table  id=form_add02_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress01" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode01"  value="<?=$objSendingCustomer->getTumbonCode01();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode01.value != '' ) frm01.hSendingCustomerAmphur01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode01.value='';" name="hSendingCustomerTumbon01" size="50"  value="<?=$objSendingCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode01"  value="<?=$objSendingCustomer->getAmphurCode01();?>"><input type="text" readonly  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode01.value != '' ) frm01.hSendingCustomerProvince01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode01.value='';" name="hSendingCustomerAmphur01" size="30"  value="<?=$objSendingCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode01"  value="<?=$objSendingCustomer->getProvinceCode01();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode01.value != '' ) frm01.hSendingCustomerZip01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode01.value='';" name="hSendingCustomerProvince01" size="30"  value="<?=$objSendingCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode01"  value="<?=$objSendingCustomer->getZip01();?>"><input type="text"  readonly name="hSendingCustomerZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode01.value != '' ) frm01.hSendingCustomerTel01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode01.value='';" size="30"  value="<?=$objSendingCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel01" size="30"  value="<?=$objSendingCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax01" size="30"  value="<?=$objSendingCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><input type="text" readonly maxlength="50"  name="hSendingCustomerAddress02" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress02();?>"></td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode02"  value="<?=$objSendingCustomer->getTumbonCode02();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode02.value != '' ) frm01.hSendingCustomerAmphur02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode02.value='';" name="hSendingCustomerTumbon02" size="50"  value="<?=$objSendingCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode02"  value="<?=$objSendingCustomer->getAmphurCode02();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode02.value != '' ) frm01.hSendingCustomerProvince02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode02.value='';" name="hSendingCustomerAmphur02" size="30"  value="<?=$objSendingCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode02"  value="<?=$objSendingCustomer->getProvinceCode02();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode02.value != '' ) frm01.hSendingCustomerZip02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode02.value='';" name="hSendingCustomerProvince02" size="30"  value="<?=$objSendingCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode02"  value="<?=$objSendingCustomer->getZip02();?>"><input type="text" name="hSendingCustomerZip02" readonly  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode02.value != '' ) frm01.hSendingCustomerTel02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode02.value='';" size="30"  value="<?=$objSendingCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel02" size="30"  value="<?=$objSendingCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax02" size="30"  value="<?=$objSendingCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress03" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress03();?>"></td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode03"  value="<?=$objSendingCustomer->getTumbonCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode03.value != '' ) frm01.hSendingCustomerAmphur03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode03.value='';" name="hSendingCustomerTumbon03" size="50"  value="<?=$objSendingCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode03"  value="<?=$objSendingCustomer->getAmphurCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode03.value != '' ) frm01.hSendingCustomerProvince03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode03.value='';" name="hSendingCustomerAmphur03" size="30"  value="<?=$objSendingCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode03"  value="<?=$objSendingCustomer->getProvinceCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode03.value != '' ) frm01.hSendingCustomerZip03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode03.value='';" name="hSendingCustomerProvince03" size="30"  value="<?=$objSendingCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode03"  value="<?=$objSendingCustomer->getZip03();?>"><input type="text"  readonly name="hSendingCustomerZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode03.value != '' ) frm01.hSendingCustomerTel03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode03.value='';" size="30"  value="<?=$objSendingCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel03" size="30"  value="<?=$objSendingCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax03" size="30"  value="<?=$objSendingCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>					
					
					
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="95%" class="i_background02" align=center>
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="20%"><strong>���ö</strong> </td>
			<td valign="top" width="30%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="20%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="30%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>

<table width="95%" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="95%" class="i_background02" align=center>
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="20%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>			
			<td width="30%" ><input type="hidden" readonly size=3 name="hStockCarId" value="<?=$objOrder->getStockCarId()?>"><input type="text" readonly name="hCarNumber" size="30"  value="<?=$objStockCar->getCarNumber()?>"></td>
			<td width="20%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><label id="hEngineNumber"><?=$objStockCar->getEngineNumber()?></label></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<label id="hCarModel01"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<label id="hCarSeries01"><?=$objCarSeries->getTitle()?></label>								
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hGear">
					<option value="Auto" <?if($objStockCar->getGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objStockCar->getGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<select  name="hCarType">
					<option value="1" <?if($objCarSeries->getCarType() == "1") echo "selected"?>>ö��
					<option value="2" <?if($objCarSeries->getCarType() == "2") echo "selected"?>>ö�к�
					<option value="3" <?if($objCarSeries->getCarType() == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($objCarSeries->getCarType() == "4") echo "selected"?>>ö���
				</select>			
			</td>		
			<td class="i_background03" width="20%"></td>
			<td width="30%">
				
			</td>
		</tr>			
		<tr>			
			<td class="i_background03"><strong>�ѹ����˹����ͺ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT readonly align="middle" size="2" maxlength="2"   name=Day03 value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="2" maxlength="2"  name=Month03 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year03 value="<?=$Year03?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>		
			<td class="i_background03" width="20%" valign="top"><strong>����¹����ᴧ</strong> </td>
			<td width="30%" valign="top">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
							<input type="text"  readonly name="hStockRedId" size="2"  value="<?=$objOrder->getStockRedId();?>">	
							<input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  disabled  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>

				
			</td>				
		</tr>
		<tr>			
			<td class="i_background03"><strong>�Ѵ�ӻ���ᴧ</strong></td>
			<td ><input type="text" readonly name="hRedCodePrice" size="20"  value="<?=$objOrder->getRedCodePrice()?>"> �ҷ</td>		
			<td class="i_background03" valign="top"><strong>�ѹ��� TBR</strong></td>
			<td ><input type="text" readonly name="hTMBNumber" size="10"  value="<?=$objOrder->getTMBNumber()?>"></td>

		</tr>

		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" ><?=nl2br($objOrder->getSendingCarRemark())?></td>
		</tr>
		</table>
	</td>
</tr>
</table>


<br>
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background_pink">��ǹŴ</td>
</tr>
</table>
<table width="95%" class="i_background02" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td>
	
<table width="100%" class="i_background05" cellpadding="1" cellspacing="0">
<tr>
	<td class="ListDetail02" width="50%" align="center">��¡�âͧ��</td>
	<td class="ListDetail02" width="50%" align="center">�ͧ Premium</td>
</tr>
<tr>
	<td valign="top">
<?if($strMode =="Update"){?>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td width="10%" class="listTitle"></td>
			<td width="50%" class="listTitle" align="center">��¡��</td>
			<td width="10%" class="listTitle" align="center">�ӹǹ</td>		
			<td width="20%" class="listTitle" align="center">˹�����</td>
			<td width="10%" class="listTitle" align="center" >TMT</td>						
		</tr>
	<?
		$i=0;
		forEach($objOrderStockList->getItemList() as $objItem) {
		
		$objStockProduct = new StockProduct();
		$objStockProduct->setStockProductId($objItem->getStockProductId());
		$objStockProduct->load();
		
		if($objStockProduct->getTMT() ==1){
			$bgColor= "f2f2f2";
		}else{
			$bgColor= "#ffefd5";
		}
		
		$objStockProductGroup = new StockProductGroup();
		$objStockProductGroup->setStockProductGroupId($objStockProduct->getStockProductGroupId());
		$objStockProductGroup->load();
		
		$objStockProductType = new StockProductType();
		$objStockProductType->setStockProductTypeId($objStockProduct->getStockProductTypeId());
		$objStockProductType->load();
		
		$objStockProductPack = new StockProductPack();
		$objStockProductPack->setStockProductPackId($objStockProduct->getStockProductPackId());
		$objStockProductPack->load();
		
		
		
		//check
		$objOrderStock = new OrderStock();
		if($objOrderStock->loadByCondition(" stock_product_id = ".$objItem->getStockProductId()." AND order_id = ".$hId)){
			$found = 1;
		}else{
			$found = 0;
		}
	?>					
			<tr>
			<input type="hidden" name="hOrderProductId<?=$i?>" value="<?if($found == 1) echo $objOrderStock->getOrderStockId()?>">
				
			<td  bgcolor="<?=$bgColor?>"><input type="hidden" size=2 name="hOrderProductCheck<?=$i?>" value="">		<input type="checkbox"  disabled   name="hOrderProduct<?=$i?>" value="<?=$objStockProduct->getStockProductId()?>" <?if($found==1) echo "checked"?>  <?if(isset($_POST["hOrderProduct$i"])) echo "checked";?>></td>
			<td  bgcolor="<?=$bgColor?>"><?=$objStockProduct->getTitle()?></td>
			<td align="center"   bgcolor="<?=$bgColor?>"><input type="text" size="3" style="text-align:right"   readonly  name="hOrderProductQty<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<input type="hidden" size="10" name="hOrderProductPriceTemp<?=$i?>" value="<?=$objStockProduct->getPrice()?>">
			<td  bgcolor="<?=$bgColor?>"><input type="text" size="10" style="text-align:right" readonly  name="hOrderProductPrice<?=$i?>" value="<?=number_format($objStockProduct->getPrice(),2)?>"></td>			
			<td align="center"  bgcolor="<?=$bgColor?>" ><input type="checkbox" disabled  name="hOrderProductTMT<?=$i?>" value="1" <?if($objOrderStock->getTMT() == 1) echo "checked";?>></td>
			<?$sumPrice = $sumPrice + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==1) $sumTMT = $sumTMT + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==0) $sumBuzz = $sumBuzz + ($objStockProduct->getPrice()*$objItem->getQty())?>
			</tr>
<?
		$i++;	
		}?>
		<input type="hidden" name="hCountProduct" value="<?=$i?>">
	<?
		$i=0;
		forEach($objOrderStockOtherList->getItemList() as $objItem) {?>		
		<input type="hidden" name="hOtherProductId<?=$i?>" value="<?=$objItem->getOrderStockOtherId()?>">
		<tr>			
			<td class="i_background03" valign="top"><strong>����</strong></td>
			<td ><input type="text" readonly name="hOtherProductTitle<?=$i?>" size="30"  value="<?=$objItem->getTitle()?>"></td>
			<td align="center"  ><input type="text" readonly size="3" style="text-align:right"   name="hOtherProductQty<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td ><input type="text" size="10" readonly  style="text-align:right"   name="hOtherProductPrice<?=$i?>" value="<?=$objItem->getPrice()?>"></td>			
			<td align="center"  ><input type="checkbox" disabled name="hOtherProductTMT<?=$i?>"  value="1" <?if($objItem->getTMT() == 1) echo "checked"?>></td>
		</tr>
			<?$sumPrice = $sumPrice + ($objItem->getPrice()*$objItem->getQty())?>
			<?if($objItem->getTMT() ==1) $sumTMT = $sumTMT + ($objItem->getPrice()*$objItem->getQty())?>
			<?if($objItem->getTMT() ==0) $sumBuzz = $sumBuzz + ($objItem->getPrice()*$objItem->getQty())?>		
	<?$i++;}?>	
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>��ǹŴ�ͧ Buzz</strong></td>
			<td ><input type="text" size="10" style="text-align:right"  readonly name="hOrderProductPriceTotalBuzz" value="<?=$sumBuzz?>"></td>
		</tr>				
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>��ǹŴ�ͧ TMT</strong></td>
			<td ><input type="text" size="10" style="text-align:right" readonly name="hOrderProductPriceTotalTMT" value="<?=$sumTMT?>"></td>
		</tr>		
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>�����ǹŴ�ͧ��</strong></td>
			<td colspan="2" ><input type="text" readonly size="10" style="text-align:right" name="hOrderProductPriceTotal" value="<?=$sumPrice?>"></td>
		</tr>			
		</table>

	</td>
</tr>
</table>
<?}?>	
	
	
	
	</td>
	<td valign="top">
	
<?if($strMode =="Update"){?>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td width="10%" class="listTitle"></td>
			<td width="50%" class="listTitle" align="center">��¡��</td>
			<td width="10%" class="listTitle" align="center">�ӹǹ</td>		
			<td width="20%" class="listTitle" align="center">˹�����</td>
			<td width="10%" class="listTitle" align="center" >TMT</td>						
		</tr>
	<?	
		$i=0;
		forEach($objOrderStockPremiumList->getItemList() as $objItem) {
		
		$objStockProduct = new StockProduct();
		$objStockProduct->setStockProductId($objItem->getStockProductId());
		$objStockProduct->load();
		
		if($objStockProduct->getTMT() ==1){
			$bgColor= "f2f2f2";
		}else{
			$bgColor= "#ffefd5";
		}
		
		$objStockProductGroup = new StockProductGroup();
		$objStockProductGroup->setStockProductGroupId($objStockProduct->getStockProductGroupId());
		$objStockProductGroup->load();
		
		$objStockProductType = new StockProductType();
		$objStockProductType->setStockProductTypeId($objStockProduct->getStockProductTypeId());
		$objStockProductType->load();
		
		$objStockProductPack = new StockProductPack();
		$objStockProductPack->setStockProductPackId($objStockProduct->getStockProductPackId());
		$objStockProductPack->load();		
		
		//check
		$objOrderPremium = new OrderStock();
		if($objOrderPremium->loadByCondition(" stock_product_id = ".$objItem->getStockProductId()." AND order_id = ".$hOrderId)){
			$found = 1;
		}else{
			$found = 0;
		}
	?>					
			<tr>
			<input type="hidden" name="hOrderPremiumId<?=$i?>" value="<?if($found == 1) echo $objOrderPremium->getOrderStockId()?>">
			<input type="hidden" name="hOrderPremiumCheck<?=$i?>" value="">
			<td bgcolor="<?=$bgColor?>" ><input type="checkbox" disabled  name="hOrderPremium<?=$i?>" value="<?=$objItem->getStockProductId()?>" <?if($found==1) echo "checked"?>  <?if(isset($_POST["hOrderPremium$i"])) echo "checked";?>></td>
			<td bgcolor="<?=$bgColor?>" ><?=$objStockProduct->getTitle()?></td>
			<input type="hidden" size="10" style="text-align:right" name="hOrderPremiumPriceTemp<?=$i?>" value="<?=$objStockProduct->getPrice()?>">
			<td align="center" bgcolor="<?=$bgColor?>" ><input type="text" size="3"  readonly style="text-align:right"   name="hOrderPremiumQty<?=$i?>" value="1"></td>
			<td bgcolor="<?=$bgColor?>" ><input type="text" size="10" style="text-align:right" readonly  name="hOrderPremiumPrice<?=$i?>" value="<?=number_format($objItem->getPrice(),2)?>"></td>	
			<td align="center" bgcolor="<?=$bgColor?>" ><input type="checkbox" disabled   name="hOrderPremiumTMT<?=$i?>" value="1" <?if($objOrderPremium->getTMT() == 1) echo "checked";?>></td>					
			</tr>
			<?$sumPrice01 = $sumPrice01 + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==1) $sumTMT01 = $sumTMT01 + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==0) $sumBuzz01 = $sumBuzz01 + ($objStockProduct->getPrice()*$objItem->getQty())?>				
			
<?
		$i++;	
		}?>
				<input type="hidden" name="hCountPremium" value="<?=$i?>">
	<?
		$i=0;
		forEach($objOrderStockOtherPremiumList->getItemList() as $objItem) {?>		
		<input type="hidden" name="hOtherPremiumId<?=$i?>" value="<?=$objItem->getOrderStockOtherId()?>">
		<tr>			
			<td class="i_background03" valign="top"><strong>����</strong></td>
			<td ><input type="text"  readonly name="hOtherPremiumTitle<?=$i?>" size="30"  value="<?=$objItem->getTitle()?>"></td>
			<td align="center"  ><input type="text"  readonly size="3" style="text-align:right"   name="hOtherPremiumQty<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td ><input type="text" size="10"   readonly style="text-align:right"   name="hOtherPremiumPrice<?=$i?>" value="<?=$objItem->getPrice()?>"></td>			
			<td align="center"  ><input type="checkbox" disabled name="hOtherPremiumTMT<?=$i?>"  value="1" <?if($objItem->getTMT() == 1) echo "checked"?>></td>
		</tr>
			<?$sumPrice01 = $sumPrice01 + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==1) $sumTMT01 = $sumTMT01 + ($objStockProduct->getPrice()*$objItem->getQty())?>
			<?if($objOrderStock->getTMT() ==0) $sumBuzz01 = $sumBuzz01 + ($objStockProduct->getPrice()*$objItem->getQty())?>				
		
	<?$i++;}?>	
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>��ǹŴ�ͧ Buzz</strong></td>
			<td ><input type="text"  readonly size="10" style="text-align:right" name="hOrderPremiumPriceTotalBuzz" value="<?=$sumBuzz01?>"></td>
		</tr>				
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>��ǹŴ�ͧ TMT</strong></td>
			<td ><input type="text"  readonly size="10" style="text-align:right" name="hOrderPremiumPriceTotalTMT" value="<?=$sumTMT01?>"></td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top" align="right" colspan="3"><strong>�����ǹŴ�ͧ Premium</strong></td>
			<td ><input type="text"  readonly size="10" style="text-align:right" name="hOrderPremiumPriceTotal" value="<?=$sumPrice01?>"></td>
		</tr>		
		</table>

	</td>
</tr>
</table>
<?}?>	
	
	
	</td>
</tr>
<tr>
	<td colspan="2" align="right">����ͧ�� <input style="text-align:right" readonly  type="text" name="hOrderProductPriceGrandTotal" value="<?=$sumPrice?>"> �ҷ</td>
</tr>
<tr>
	<td colspan="2" align="right">����ͧ Premium <input style="text-align:right" readonly  type="text" name="hOrderPremiumPriceGrandTotal" value="<?=$sumPrice01?>"> �ҷ</td>
</tr>
<tr>
	<td colspan="2" align="right">��ǹŴ�Թʴ <input style="text-align:right"  readonly    type="text" name="hDiscountPrice" value="<?=$objOrder->getDiscountPrice()?>"> �ҷ</td>	
</tr>
<tr>
	<td colspan="2" align="right">��ǹŴ Subdown <input style="text-align:right"  readonly    type="text" name="hDiscountSubdown" value="<?=$objOrder->getDiscountSubdown()?>"> �ҷ</td>	
</tr>
<?$grand = $sumPrice+$sumPrice01+$objOrder->getDiscountPrice()+$objOrder->getDiscountSubdown();?>
<tr>
	<td colspan="2" align="right">��ǹŴ��ԧ <input style="text-align:right" readonly  type="text" name="hOrderDiscountGrandTotal" value="<?=$grand?>"> �ҷ</td>
</tr>
<tr>
	<td colspan="2" align="right"><br><br>��ǹŴ��� Margin <input style="text-align:right"  readonly  type="text" name="hDiscount" value="<?=$objCarSeries->getMargin()?>"> �ҷ</td>
</tr>
</table>

	</td>
</tr>
</table>
<br>

<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background">�����š�ë���</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��������ë���</strong></td>
			<td width="30%" valign="top" ><input type="radio" disabled  name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 ) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" disabled name="hBuyType" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��</td>
			<td class="i_background03" width="20%" valign="top"><strong>����ѷ�ṹ��</strong> </td>
			<td width="30%" valign="top">
				<?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?>
			</td>			
		</tr>			
		<tr>
			<td class="i_background03"><strong>�͡����</strong> </td>
			<td ><input type="text" readonly name="hBuyFee" size="10"  value="<?if($objOrder->getBuyFee() > 0) echo $objOrder->getBuyFee();?>"> %</td>
			<td class="i_background03"><strong>��������</strong>   </td>
			<td ><input type="text" readonly  name="hBuyTime" size="10"  value="<?if($objOrder->getBuyTime() > 0) echo $objOrder->getBuyTime();?>"> ��͹</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>��ҧǴ</strong> </td>
			<td ><input type="text" readonly  name="hBuyPrice" size="20"  value="<?if($objOrder->getBuyPrice() > 0) echo $objOrder->getBuyPrice();?>"> �ҷ</td>
			<td class="i_background03"><strong>�Ҥ�ö</strong>   </td>
			<td ><input type="text" readonly  name="hBuyTotal" size="20"  value="<?if($objOrder->getBuyTotal() > 0) echo $objOrder->getBuyTotal();?>"> �ҷ</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>��ǹ�</strong></td>
			<td ><input type="text" readonly  name="hBuyDown" size="20"  value="<?if($objOrder->getBuyDown() > 0) echo $objOrder->getBuyDown();?>"> �ҷ</td>
			<td   class="i_background03"><strong>�ʹ�Ѵ</strong></td>
			<td ><input type="text" readonly  name="hBuyTotal" size="20"  value="<?if($objOrder->getBuyTotal() > 0) echo $objOrder->getBuyTotal();?>"> �ҷ</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3"><input type="text"  readonly  name="hBuyRemark" size="80"  value="<?=$objOrder->getBuyRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="95%" cellpadding="2" align="center">
<tr>
	<td class="i_background02" width="40%" valign="top">
		<table width="100%" cellpadding="3" cellspacing="0">
		<tr>
			<td class="i_background" align="center">�����žú</td>
		</tr>
		</table>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="30%" class="i_background03"><strong>����ѷ�ú</strong> </td>
			<td >
				<?$objPrbCompanyList->printSelect("hPrbCompany",$objOrder->getPrbCompany(),"��س����͡");?>				
			</td>
			
		</tr>
		<tr>
			<td  class="i_background03"><strong>�ѹ������ؾú</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle"  readonly  size="2" maxlength="2"   name=Day05 value="<?=$Day05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" readonly  size="2" maxlength="2"  name=Month05 value="<?=$Month05?>"></td>
					<td>-</td>
					<td><INPUT align="middle"  readonly  size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year05 value="<?=$Year05?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>��Ҿú</strong></td>
			<td  ><input type="text"  readonly  name="hPrbPrice" size="10"  value="<?if($objOrder->getPrbPrice() > 0) echo $objOrder->getPrbPrice();?>"> �ҷ</td>
		</tr>
		</table>
	</td>
	<td class="i_background02" valign="top">
		<table width="100%" cellpadding="3" cellspacing="0">
		<tr>
			<td class="i_background" align="center">�����Ż�Сѹ���</td>
		</tr>
		</table>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>��Сѹ����Ҩҡ</strong> </td>
			<td colspan="3">
				<input type="Radio" disabled  onclick="checkInsure()" name="hInsureFrom" value="1" <?if($objOrder->getInsureFrom() ==1) echo "checked"?>>���
				<label id="insure" <?if($objOrder->getInsureFrom() !=1) echo "style='display:none'"?>>�� <input type="text" name="hInsureFromDetail" size="30"  value="<?=$objOrder->getInsureFromDetail()?>"></label>
				<input type="Radio" disabled onclick="checkInsure()"  name="hInsureFrom" value="2" <?if($objOrder->getInsureFrom() ==2) echo "checked"?>>�� TMT
				<input type="Radio" disabled  onclick="checkInsure()"  name="hInsureFrom" value="3" <?if($objOrder->getInsureFrom() ==3) echo "checked"?>>�� BUZZ
				
			</td>			
		</tr>			
		<tr>
			<td class="i_background03"><strong>����ѷ��Сѹ���</strong> </td>
			<td>
				<?$objInsureCompanyList->printSelect("hInsureCompany",$objOrder->getInsureCompany(),"��س����͡");?>
			</td>	
			<td class="i_background03"><strong>�ѹ������ػ�Сѹ���</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT readonly align="middle" size="2" maxlength="2"   name=Day06 value="<?=$Day06?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="2" maxlength="2"  name=Month06 value="<?=$Month06?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year06 value="<?=$Year06?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>������»�Сѹ���</strong></td>
			<td ><input type="text" readonly name="hInsurePrice" size="10"  value="<?if($objOrder->getInsurePrice() > 0) echo $objOrder->getInsurePrice();?>"> �ҷ</td>
			<td class="i_background03"><strong>������</strong> </td>
			<td >
				<select  name="hInsureType">
					<option value="1" <?if($objOrder->getInsureType() ==1) echo "selected"?>>��Сѹ��ª�� 1
					<option value="2" <?if($objOrder->getInsureType() ==2) echo "selected"?>>��Сѹ��ª�� 2
					<option value="3" <?if($objOrder->getInsureType() ==3) echo "selected"?>>��Сѹ��ª�� 3
				</select>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>��Сѹ�շ��</strong>   </td>
			<td ><input type="text" readonly name="hInsureYear" size="10"  value="<?if($objOrder->getInsureYear() > 0) echo $objOrder->getInsureYear();?>"> </td>
			<td class="i_background03"><strong>�ع��Сѹ</strong>   </td>
			<td ><input type="text" readonly name="hInsureBudget" size="10"  value="<?if($objOrder->getInsureBudget() > 0) echo $objOrder->getInsureBudget();?>"> �ҷ </td>
		</tr>
	
		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" ><input type="text" readonly name="hInsureRemark" size="50"  value="<?=$objOrder->getInsureRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background_pink">ʶҹШҡ�Ѵ����</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td>

		<table >
		<tr>
			<td>�к�ʶҹ�</td>
			<td><input type="radio" name="hStatus01" value=1 <?if($objOrder->getCheckPurchase() ==1 ) echo "checked"?>> ��ҹ���͹��ѵ� &nbsp;&nbsp;<input type="radio" name="hStatus01" value=2 <?if($objOrder->getCheckPurchase() ==2 ) echo "checked"?>> ����ҹ���͹��ѵ�&nbsp;&nbsp;  </td>
		</tr>
		<tr>
			<td valign="top">�����˵�</td>
			<td><textarea rows="5" cols="100" name="hRemark"><?=$objOrder->getCheckPurchaseRemark()?></textarea></td>
		</tr>
		</table>

	</td>
</tr>
</table>

<br>
<table width="95%" cellpadding="3" cellspacing="0" align=center>
<tr>
	<td class="i_background_pink">ʶҹШҡ�ѭ��</td>
</tr>
</table>
<table width="95%" class="i_background02" align=center>
<tr>
	<td>

<table >
<tr>
	<td>��Ǩ���ͺ</td>
	<td><input type="radio" name="hStatus" value=1 <?if($objOrder->getCheckAccountSending() ==1 ) echo "checked"?>> ��ҹ���͹��ѵ� &nbsp;&nbsp;<input type="radio" name="hStatus" value=2 <?if($objOrder->getCheckAccountSending() ==2 ) echo "checked"?>> ����ҹ���͹��ѵ�&nbsp;&nbsp;  </td>
</tr>
<tr>
	<td>����Ԫ���</td>
	<td><input type="radio" name="hCom" value=1 <?if($objOrder->getCheckAccountSendingCom() ==1 ) echo "checked"?>> NO COM &nbsp;&nbsp;<input type="radio" name="hCom" value=2 <?if($objOrder->getCheckAccountSendingCom() ==2 ) echo "checked"?>> HALF COM&nbsp;&nbsp;  <input type="radio" name="hCom" value=3 <?if($objOrder->getCheckAccountSendingCom() ==3 ) echo "checked"?>> FULL COM </td>
</tr>
<tr>
	<td valign="top">�����˵�</td>
	<td><textarea rows="5" cols="100" name="hRemark"><?=$objOrder->getCheckAccountSendingRemark()?></textarea></td>
</tr>
</table>

	</td>
</tr>
</table>
<br>
<table width="95%" cellpadding="0" cellspacing="0" align="center">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">�����Ũ�����¹</td>
			<td width="65%"></td>
			<td align="right" >

			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="95%" class="i_background02" align="center">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">			
		<tr>			
			<td class="i_background03" width="20%" valign="top"><strong>����¹����ᴧ</strong> </td>
			<td width="30%" valign="top">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>							
							<input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  disabled  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>

				
			</td>		
			<td class="i_background03" width="20%" ><strong>�Ѵ�ӻ���ᴧ</strong></td>
			<td width="30%" ><input type="text" readonly name="hRedCodePrice" size="15"  value="<?=$objOrder->getRedCodePrice()?>"> �ҷ</td>			
		</tr>
		<tr>			
			<td class="i_background03"><strong>��Ҩ�����¹</strong></td>
			<td ><input type="text" r name="hRegistryPrice" size="15"  value="<?=$objOrder->getRegistryPrice()?>"> �ҷ</td>		
			<td class="i_background03"><strong>�ѹ��� TBR</strong></td>
			<td >
				<input type="text" name="hTMBNumber" size="10"  value="<?=$objOrder->getTMBNumber()?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>����¹���´�</strong></td>
			<td >
				<table cellspacing="0" cellpadding="0">
				<tr>
					<td><input type="text" name="hBlackCodeNumber" size="10"  value="<?=$objOrder->getBlackCodeNumber()?>"></td>
				</tr>
				</table>
			</td>
			<td class="i_background03"><strong>�ѹ��訴����¹���´�</strong></td>
			<td >
				<?=$objOrder->getBlackCodeDate()?>
			</td>
		</tr>		
		<tr>			
			<td class="i_background03" valign="top"><strong>������ջշ��</strong></td>
			<td ><input type="text" name="hRegistryTaxYearNumber" size="10"  value="<?=$objOrder->getRegistryTaxYearNumber()?>"></td>
		
			<td class="i_background03" valign="top"><strong>������ջ�Шӻ�</strong></td>
			<td ><input type="text" name="hRegistryTaxYear" size="15"  value="<?=$objOrder->getRegistryTaxYear()?>"> �ҷ</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>ʶҹС�è�����¹</strong></td>
			<td ><input type="radio" name="hRegistryStatus" value=1 <?if($objOrder->getRegistryStatus() == 1 ) echo "checked"?>>Yes&nbsp;&nbsp;<input type="radio" name="hRegistryStatus" value=2  <?if($objOrder->getRegistryStatus() == 2 OR $objOrder->getRegistryStatus() == 0  ) echo "checked"?>>No</td>
		
			<td class="i_background03" valign="top"><strong>�ѹ����觨�����¹</strong></td>
			<td >
				<?=$objOrder->getRegistrySendDate()?>
			</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�͡��÷��Ҵ</strong></td>
			<td ><input type="text"  name="hRegistryMissDocument" size="50"  value="<?=$objOrder->getRegistryMissDocument()?>"></td>
		
			<td class="i_background03" valign="top"><strong>���͹䢡�è�����¹</strong></td>
			<td >
				<?
				$objRegistryCondition = new RegistryCondition();
				$objRegistryCondition->setRegistryConditionId($objOrder->getRegistryConditionId());
				$objRegistryCondition->load();
				
				echo $objRegistryCondition->getTitle();
				?>
			

			</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�Ѻ����</strong></td>
			<td ><input type="radio" name="hRegistryTakeCode" value=1 <?if($objOrder->getRegistryTakeCode() == 1 ) echo "checked"?>>Yes&nbsp;&nbsp;<input type="radio" name="hRegistryTakeCode" value=2 <?if($objOrder->getRegistryTakeCode() == 2 OR $objOrder->getRegistryTakeCode() == 0 ) echo "checked"?>>No</td>
		
			<td class="i_background03" valign="top"><strong>�ѹ����Ѻ���·���¹</strong></td>
			<td >
				<?=$objOrder->getRegistryTakeCodeDate()?>
			</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�Ѻ����</strong></td>
			<td ><input type="radio" name="hRegistryTakeBook" value=1 <?if($objOrder->getRegistryTakeBook() == 1  ) echo "checked"?>>Yes&nbsp;&nbsp;<input type="radio" name="hRegistryTakeBook" value=2 <?if($objOrder->getRegistryTakeBook() == 2 OR $objOrder->getRegistryTakeBook() == 0 ) echo "checked"?>>No</td>
		
			<td class="i_background03" valign="top"><strong>�ѹ����Ѻ��������¹</strong></td>
			<td >
				<?=$objOrder->getRegistryTakeBookDate()?>
			</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" >
				<textarea rows="5" cols="100" name="hRegistryRemark"><?=$objOrder->getRegistryRemark();?></textarea>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
      <input type="Button" name="hSubmit" value="��Ѻ�����¡��" onclick="window.location='ccardList.php?hKeyword=<?=$hKeyword?>&hSearch=<?=$hSearch?>&hPage=<?=$hPage?>'">			
	<br><br>
	</td>
</tr>
</table>
<?if ($strMode == "Update"){
$objMemberTemp = new Member();
$objMemberTemp->setMemberId($objOrder->getEditBy());
$objMemberTemp->load();
?>		
<table width="100%" >
<tr>
	<td align="right">��䢢���������ش�� :   <?=$objMemberTemp->getNickname()?>     �ѹ���    <?=$objOrder->getEditDate()?> </td>
</tr>
</table>
<?
unset($objMemberTemp);
}?>		
</form>


<script>
	
	function showBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "";	
		document.getElementById("showBookingCustomer").style.display = "none";		
		document.getElementById("hideBookingCustomer").style.display = "";		
	}

	function hideBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "none";	
		document.getElementById("showBookingCustomer").style.display = "";			
		document.getElementById("hideBookingCustomer").style.display = "none";
	}

	function showBookingOrder(){
		document.getElementById("bookingOrder").style.display = "";	
		document.getElementById("showBookingOrder").style.display = "none";		
		document.getElementById("hideBookingOrder").style.display = "";		
	}

	function hideBookingOrder(){
		document.getElementById("bookingOrder").style.display = "none";	
		document.getElementById("showBookingOrder").style.display = "";			
		document.getElementById("hideBookingOrder").style.display = "none";
	}
	
	function showBookingCar(){
		document.getElementById("bookingCar").style.display = "";	
		document.getElementById("showBookingCar").style.display = "none";		
		document.getElementById("hideBookingCar").style.display = "";		
	}

	function hideBookingCar(){
		document.getElementById("bookingCar").style.display = "none";	
		document.getElementById("showBookingCar").style.display = "";			
		document.getElementById("hideBookingCar").style.display = "none";
	}

</script>


<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
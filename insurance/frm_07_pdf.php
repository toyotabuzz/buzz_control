<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",500);

$objCustomer = new Customer();
$objInsure = new Insure();
$objCarSeries = new CarSeries();
$objCarColor = new CarColor();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objInsureList = New InsureList;

$objLabel = new Label();
$objLabel->setLabelId(3);
$objLabel->load();

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(50,50,50);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 


			
				$pdf->AddPage();
				$pdf->SetLeftMargin(80);

				$objInsure = new Insure();
				$objInsure->set_insure_id($hInsureId);
				$objInsure->load();
				
				$objForm = new InsureForm();
				$objForm->set_insure_form_id($hFormId);
				$objForm->load();
				
				$objCompany = new Company();
				$objCompany->setCompanyId($sCompanyId);
				$objCompany->load();
				
				$objMember = new Member();
				$objMember->setMemberId($objInsure->get_sale_id());
				$objMember->load();
				
				$objSale = new Member();
				$objSale->setMemberId($objInsure->get_sale_id());
				$objSale->load();				
				
				$objInsureCar = new InsureCar();
				$objInsureCar->set_car_id($objInsure->get_car_id());
				$objInsureCar->load();
				
				$objCustomer = new InsureCustomer();
				$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
				$objCustomer->load();
				
				$objCarType = new CarType();
				$objCarType->setCarTypeId($objInsureCar->get_car_type());
				$objCarType->load();
				
				$objCarModel = new CarModel();
				$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
				$objCarModel->load();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objInsureCar->get_color());
				$objCarColor->load();
				
				$hStartDate= formatThaiDate($objInsure->get_date_protect());
				$hStartYear = substr($hStartDate,-4);
				$hEndYear = $hStartYear+1;
				$hEndDate = substr($hStartDate,0,(strlen($hStartDate)-4)).$hEndYear;
				
				$objLabel = new Label();
				$strText01= "��˹������š�þ������ʹ��Ҥ�";	
				$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
				
				$pdf->SetFont('angsa','B',16);	
				$strtext = $objLabel->getText01();
				$pdf->SetLeftMargin(22);
				$pdf->Cell(550,20,"",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(550,20,$objLabel->getText01(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(550,20,$objLabel->getText06(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(550,20,"�� ".$objLabel->getText04()." ῡ�� ".$objLabel->getText05(),0,0,"C");
				$pdf->Ln();
				$pdf->Ln();
				$pdf->SetFont('angsa','B',14);	
				$pdf->Cell(550,20,"Ẻ����������¡��ԡ��¡�û�Сѹ���",0,0,"C");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(50,20,"�ѹ����駻�Сѹ",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t01(),0,0,"C");
				$pdf->Cell(50,20,"����Ѻ��",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t02(),0,0,"C");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(50,20,"���ͼ����",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t03(),0,0,"C");
				$pdf->Cell(50,20,"����Դ���",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t04(),0,0,"C");
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(150,20,"��¡�÷���¡��ԡ",0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,"����¹",0,0,"L");
				$pdf->Cell(100,20,$objForm->get_t05(),0,0,"L");
				$pdf->Cell(100,20,"�Ţ�Ѻ��",0,0,"L");
				$pdf->Cell(100,20,$objForm->get_t06(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,"�Ţ�����������Сѹ���",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t07(),0,0,"C");
				$pdf->Cell(100,20,"�Ţ����������ú.",0,0,"L");
				$pdf->Cell(100,20,$objForm->get_t08(),0,0,"L");
				$pdf->Ln();			
				$pdf->Cell(50,20,"",0,0,"C");	
				$pdf->Cell(100,20,"���ͼ����һ�Сѹ���",0,0,"L");
				$pdf->Cell(100,20,$objForm->get_t09(),0,0,"L");
				$pdf->Ln();

				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,"�˵ؼ�㹡��¡��ԡ",0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,substr($objForm->get_t10(),0,120),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,substr($objForm->get_t10(),120,200),0,0,"L");
				$pdf->Ln();


				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,20,"�������㹡�èѴ���͡���",0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(100,40,$objForm->get_t13(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(50,20,"��Ҿ��Ҫ���",0,0,"L");
				$pdf->Cell(150,20,$objForm->get_t14(),0,0,"C");
				$pdf->Cell(300,20,"�������Ѻ�ͧ���  ��������¡�ôѧ����Ǣ�ҧ���繨�ԧ�ء��С�����",0,0,"L");
				$pdf->Ln();
				
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(300,20,"��Ҿ��Ң��׹�ѹ����ҡ�Դ�����Դ��Ҵ��Դ��� ��Ҿ��Ң��Ѻ�Դ�ء��С�õ�����������",0,0,"L");
				$pdf->Ln();
				$pdf->Ln();
				
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(50,20,"ŧ����",0,0,"R");
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(30,20,"�������",0,0,"L");
				
				$pdf->Cell(50,20,"ŧ����",0,0,"R");
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(100,20,$objLabel->getText09(),0,0,"L");
				$pdf->Ln();
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(50,20,"(",0,0,"R");
				$pdf->Cell(100,20,$objForm->get_t15(),0,0,"C");
				$pdf->Cell(30,20,")",0,0,"L");
				
				$pdf->Cell(50,20,"(",0,0,"R");
				$pdf->Cell(100,20,$objLabel->getText07(),0,0,"C");
				$pdf->Cell(100,20,")",0,0,"L");
				$pdf->Ln();
				
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(50,20,"�ѹ���",0,0,"R");
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(30,20,"",0,0,"L");
				
				$pdf->Cell(50,20,"�ѹ���",0,0,"R");
				$pdf->Cell(100,20,"",0,0,"C");
				$pdf->Cell(100,20,"",0,0,"L");
				$pdf->Ln();
				
				
				
				$pdf->line(50,141,550,141);
				$pdf->line(140,190,270,190);
				$pdf->line(320,190,550,190);
				$pdf->line(140,210,270,210);
				$pdf->line(320,210,550,210);
				
				$pdf->line(110,270,265,270);
				$pdf->line(320,270,550,270);
				$pdf->line(180,290,320,290);
				$pdf->line(410,290,550,290);
				
				$pdf->line(150,310,550,310);

				$pdf->line(70,370,550,370);
				$pdf->line(70,425,550,425);
			
				$pdf->line(120,450,270,450);
				
				$pdf->line(170,525,270,525);
				$pdf->line(350,525,450,525);
				
				$pdf->line(170,550,270,550);
				$pdf->line(350,550,450,550);
				
	Unset($objCustomerList);
	$pdf->Output();	
	?>
	<?include "unset_all.php";?>
	<script>
	//window.close();
	</script>	
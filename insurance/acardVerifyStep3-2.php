<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",1000);


$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

$objCustomerList = New CustomerList;


if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hKeywordCar !="" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";
	}else{
		$pstrCondition .="  ( IC.code LIKE '%".$hKeywordCar."%' OR  IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'  )  ";	
	}
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hBuyCompany > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.buy_company = '".$hBuyCompany."' ";
	}else{
		$pstrCondition .=" IC.buy_company = '".$hBuyCompany."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" IC.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$Month02."-%' ";
	}else{
		$pstrCondition .=" IC.date_verify LIKE '%-".$Month02."-%'   ";	
	}
}

if($hCompanyId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.company_id = '".$hCompanyId."' ";
	}else{
		$pstrCondition .=" IC.company_id = '".$hCompanyId."' ";	
	}
}

if($hSetMonth != 0 and $hSetMonth != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '%-".$hSetMonth."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '%-".$hSetMonth."-%' ";	
	}
}

if($hSetYear != 0 and $hSetYear != ""){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.date_verify LIKE '".$hSetYear."-%' ";
	}else{
		$pstrCondition .="  IC.date_verify LIKE '".$hSetYear."-%' ";	
	}
}

if($hRegisterYear > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.register_year = '".$hRegisterYear."' ";
	}else{
		$pstrCondition .=" IC.register_year = '".$hRegisterYear."' ";	
	}
}


if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  >= '".$hStartDate."' AND IC.date_verify <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.date_verify  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( IC.date_verify  = '".$hStartDate."' ) ";
		}
	}
}


}//end hSearch


if(isset($hUpdate)){
	for($i=0;$i<$hCountTotal;$i++){
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($hInsureCarId[$i]);
		
		if($hSale[$i] == 9999 ){
			$objInsureCar->set_sale_id(0);
			$objInsureCar->updateSale();
			$objInsureCar->set_junk(1);
			$objInsureCar->updateJunk();
		}else{
			$objInsureCar->set_sale_id($hSale[$i]);
			$objInsureCar->updateSale();
		}
		
	}
}


if (!empty($hDelete) )   {  //Request to delete some CashierReserve release
    $objInsureCar = New InsureCar();
	$objInsureCar->set_car_id($hDelete);
	
	$objInsureList = new InsureList();
	$objInsureList->setFilter(" car_id= ".$hDelete);
	$objInsureList->setPageSize(0);
	$objInsureList->load();
	forEach($objInsureList->getItemList() as $objItem) {
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
	
		$objInsureQuotationList = new InsureQuotationList();
		$objInsureQuotationList->setFilter(" insure_id= ".$objItem->get_insure_id());
		$objInsureQuotationList->setPageSize(0);
		$objInsureQuotationList->load();
		forEach($objInsureQuotationList->getItemList() as $objItemQuotation) {
			$objInsureQuotation = new InsureQuotation();
			$objInsureQuotation->set_quotation_id($objItemQuotation->get_quotation_id());
			$objInsureQuotation->delete();
		}
		$objInsure->delete();

	}
	
	$objInsureCar->delete();
	++$intCountDelete;
    if ($pstrMsg == "" ) $pstrMsg .= " ź������ " .$intCountDelete . " ��¡�� ";
    unset($objCustomer);
} else {
    $pstrMsg = $hMsg;
}

//echo $pstrCondition;

$objCustomerList = new InsureCarList();
$objCustomerList->setFilter($pstrCondition);
$objCustomerList->setPageSize(PAGE_SIZE);
$objCustomerList->setPage($hPage);
$objCustomerList->setSortDefault(" niti, C.firstname, C.lastname, date_verify ASC");
$objCustomerList->setSort($hSort);
$objCustomerList->loadSearch();

$pCurrentUrl = "acardVerifyStep3.php?hBuyCompany=$hBuyCompany&hSearch=$hSearch&hKeyword=$hKeyword&hType=$hType&Month02=$Month02&Day=$Day&Month=$Month&Year=$Year&Day01=$Day01&Month01=$Month01&Year01=$Year01&hCarType=$hCarType&hCarModel=$hCarModel&hKeyword=$hKeyword&hSaleId=$hSaleId&hSetMonth=$hSetMonth&hSetYear=$hSetYear";
	// for paging only (sort resets page viewing)
$pCurrentUrlLink = "acardVerifyStep3.php?hBuyCompany=".$hBuyCompany."YYYhSearch=".$hSearch."YYYhKeyword=".$hKeyword."YYYhType=".$hType."YYYMonth02=".$Month02."YYYDay=".$Day."YYYMonth=".$Month."YYYYear=".$Year."YYYDay01=".$Day01."YYYMonth01=".$Month01."YYYYear01=".$Year01."YYYhCarType=".$hCarType."YYYhCarModel=".$hCarModel."YYYhKeyword=".$hKeyword."YYYhSaleId=".$hSaleId."YYYhPage=".$hPage;

	

$pageTitle = "1. �����Ż�Сѹ���";
$strHead03 = "�Ѵ����١��� ACARD ";
$pageContent = "1.2 �Ѵ����١��� ACARD ";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<table  class="search" cellpadding="3" width="95%" align="center">
<tr>
	<td width="100" align="right"></td>
	<td>3. ��Ǩ�ͺ��ª��͡�èѴ����١������͹ �չҤ� 2544 �ӹǹ������ 120 ��¡�� </td>
</tr>
</table>

<?if(isset($hSearch) and $objCustomerList->mCount > 0 ){?>

<form name="frm" action="<?=$pCurrentUrl?>" method="POST">
<table border="0" width="95%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">

	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="95%" align="center" cellpadding="2" cellspacing="2">

<tr>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="5%" align="center" class="ListTitle">��ö</td>
	<td width="7%" align="center" class="ListTitle">�ѹ��������ͧ</td>
	<td width="15%" align="center" class="ListTitle">����</td>
	<td width="10%" align="center" class="ListTitle">������</td>
	<td width="5%" align="center" class="ListTitle">�Ţ����¹</td>

	<td width="5%" align="center" class="ListTitle">��Сѹ���</td>
	<td width="5%" align="center" class="ListTitle">�ṹ��</td>
	<td width="10%" align="center" class="ListTitle">�ʴ�������</td>

</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" colspan="8" class="listTitle" >��ѡ�ҹ A </td>	
	<td  valign="top" class="listTitle"  align="center">8 ��¡��</td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ԵԺؤ��</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >11-12-07</td>	
	<td valign="top"  >�����  �໤������ʷ� </td>

	<td valign="top"  >081-7503705</td>
	<td valign="top"  align="center" >��-5458</td>
	<td valign="top"  ></td>
	<td valign="top"  >Tisco</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1784&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ԵԺؤ��</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >14-12-07</td>	
	<td valign="top"  >�ʹ�� �ô </td>
	<td valign="top"  ></td>
	<td valign="top"  align="center" >��-3238</td>
	<td valign="top"  ></td>
	<td valign="top"  >TNB</td>

	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=28&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >05-12-07</td>	
	<td valign="top"  >�Ե� �������Թ���</td>
	<td valign="top"  >083-8885160,089-2332999</td>

	<td valign="top"  align="center" >Ψ-5666</td>
	<td valign="top"  ></td>
	<td valign="top"  >AYCL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1814&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >15-12-07</td>	
	<td valign="top"  >�Ŵ��� �Ӻح</td>

	<td valign="top"  ></td>
	<td valign="top"  align="center" >��-5839</td>
	<td valign="top"  ></td>
	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1603&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >28-12-09</td>	
	<td valign="top"  >��Ŵ� ���ྪ��ѵ��</td>

	<td valign="top"  >084-0007812</td>
	<td valign="top"  align="center" >��-4608</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=242&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" colspan="8" class="listTitle" >��ѡ�ҹ B </td>	
	<td  valign="top" class="listTitle"  align="center">8 ��¡��</td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >07-12-07</td>	
	<td valign="top"  >�پ��� ����¾ѹ��</td>
	<td valign="top"  ></td>
	<td valign="top"  align="center" >��-9967</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>

	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=989&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >06-12-07</td>	
	<td valign="top"  >᫹��� ���ʴ��ѧ��ä�</td>
	<td valign="top"  ></td>

	<td valign="top"  align="center" >��-3961</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1710&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >05-12-07</td>	
	<td valign="top"  >���ԡ� ����ٵ�</td>

	<td valign="top"  >087-0823372</td>
	<td valign="top"  align="center" >��-3932</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1997&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >25-12-07</td>	
	<td valign="top"  >�ͧ�� ���ºح��</td>
	<td valign="top"  >089-2765491</td>
	<td valign="top"  align="center" >��-7102</td>
	<td valign="top"  ></td>

	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1699&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" colspan="8" class="listTitle" >��ѡ�ҹ C </td>	
	<td  valign="top" class="listTitle"  align="center">8 ��¡��</td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >03-12-07</td>	
	<td valign="top"  >�ĵ ��ʺ��</td>

	<td valign="top"  >081-0111403</td>
	<td valign="top"  align="center" >��-3971</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1719&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >25-12-07</td>	
	<td valign="top"  >��Թ�ѵ�� ����������</td>
	<td valign="top"  ></td>
	<td valign="top"  align="center" >��-7089</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>

	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1648&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >07-12-07</td>	
	<td valign="top"  >�筨��ó ���ʧ</td>
	<td valign="top"  ></td>

	<td valign="top"  align="center" >��-3957</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1674&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >16-12-07</td>	
	<td valign="top"  >�øԴ� ��Ѥ���</td>

	<td valign="top"  >085-9197688</td>
	<td valign="top"  align="center" >��-7907</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1271&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >11-12-07</td>	
	<td valign="top"  >�Ѫ�� ������</td>
	<td valign="top"  >085-1200444,087-2795614</td>
	<td valign="top"  align="center" >Ψ-5990</td>
	<td valign="top"  ></td>

	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1337&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" colspan="8" class="listTitle" >��ѡ�ҹ D </td>	
	<td  valign="top" class="listTitle"  align="center">8 ��¡��</td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >11-12-07</td>	
	<td valign="top"  >�Ѫ�� Ἱʷ�ҹ</td>

	<td valign="top"  >089-5268011</td>
	<td valign="top"  align="center" >��-5110</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1918&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >07-12-07</td>	
	<td valign="top"  >����� ����ǡ</td>
	<td valign="top"  >086-5146936</td>
	<td valign="top"  align="center" >��-3097</td>
	<td valign="top"  ></td>

	<td valign="top"  >TLT</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=644&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >17-12-07</td>	
	<td valign="top"  >�Ѱ���� ���⤵�</td>

	<td valign="top"  ></td>
	<td valign="top"  align="center" >��-4437</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1461&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >25-12-07</td>	
	<td valign="top"  >�Ѵ�� ������٭</td>

	<td valign="top"  >081-8349795</td>
	<td valign="top"  align="center" >��-6279</td>
	<td valign="top"  ></td>
	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1696&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >25-12-07</td>	
	<td valign="top"  >���ž ��പ</td>
	<td valign="top"  >087-4823267</td>
	<td valign="top"  align="center" >��-4009</td>
	<td valign="top"  ></td>

	<td valign="top"  >Tisco</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1493&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" colspan="8" class="listTitle" >��ѡ�ҹ E </td>	
	<td  valign="top" class="listTitle"  align="center">8 ��¡��</td>
</tr>

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >20-12-07</td>	
	<td valign="top"  >�Է�� ����ѹ����</td>

	<td valign="top"  >089-9625867,083-7124093</td>
	<td valign="top"  align="center" >��-8434</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1821&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >01-12-07</td>	
	<td valign="top"  >�Թ�� ������പ</td>
	<td valign="top"  >081-6442455</td>
	<td valign="top"  align="center" >��-8357</td>
	<td valign="top"  ></td>

	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1908&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >27-12-09</td>	
	<td valign="top"  >���о��� ����͹���</td>

	<td valign="top"  >086-8257301</td>
	<td valign="top"  align="center" >��-4603</td>
	<td valign="top"  ></td>
	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=149&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >16-12-07</td>	
	<td valign="top"  >��á���� �ѹ����ŻԹ</td>
	<td valign="top"  >086-9916440</td>
	<td valign="top"  align="center" >��-449</td>
	<td valign="top"  >��ا෾</td>

	<td valign="top"  >KTB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1915&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >06-12-07</td>	
	<td valign="top"  >ʹ��� ����ǧ��</td>

	<td valign="top"  >089-6788663</td>
	<td valign="top"  align="center" >��-4008</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1341&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >06-12-07</td>	
	<td valign="top"  >����ѵ �Ե��Դ</td>
	<td valign="top"  >085-3576058,081-9061279</td>
	<td valign="top"  align="center" >��-4060</td>
	<td valign="top"  ></td>

	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1874&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >07-12-07</td>	
	<td valign="top"  >����� �ع�</td>

	<td valign="top"  >089-5251847</td>
	<td valign="top"  align="center" >��-6189</td>
	<td valign="top"  ></td>
	<td valign="top"  ></td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1910&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >05-12-07</td>	
	<td valign="top"  >����¹�� �þ�</td>

	<td valign="top"  >085-9698449</td>
	<td valign="top"  align="center" >��-3974</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1994&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >03-12-07</td>	
	<td valign="top"  >�تҴ� ⵸������è��</td>
	<td valign="top"  >081-6292964</td>
	<td valign="top"  align="center" >��-3689</td>
	<td valign="top"  ></td>

	<td valign="top"  ></td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1713&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >01-12-07</td>	
	<td valign="top"  >�تҵ� ��Ѿ����ԭ</td>

	<td valign="top"  >085-3347955</td>
	<td valign="top"  align="center" >��-7093</td>
	<td valign="top"  ></td>
	<td valign="top"  >SPL</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=847&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">

	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >28-12-09</td>	
	<td valign="top"  >�ط�ȹ� �չС�ó</td>
	<td valign="top"  >087-4949874</td>
	<td valign="top"  align="center" >��-6690</td>
	<td valign="top"  ></td>

	<td valign="top"  >TNB</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=46&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	

<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >�ؤ�Ÿ�����</td>	
	<td valign="top" align="center" >2007</td>	
	<td valign="top" align="center" >23-12-07</td>	
	<td valign="top"  >����ѡ��� ���µ�С��</td>

	<td valign="top"  >083-2501335,084-0907825</td>
	<td valign="top"  align="center" >��-893</td>
	<td valign="top"  ></td>
	<td valign="top"  >Tisco</td>
	<td  valign="top" align="center"><input type="button"  class="button"  value="���" onclick="window.open('acardUpdate.php?hId=1721&hLink=acardVerifyStep3.php?hBuyCompany=YYYhSearch=1. ���͡��͹����ͧ��èѴ���YYYhKeyword=YYYhType=YYYMonth02=YYYDay=YYYMonth=YYYYear=YYYDay01=YYYMonth01=YYYYear01=YYYhCarType=YYYhCarModel=YYYhKeyword=YYYhSaleId=YYYhPage=');"></td>
</tr>

	</table>

<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?}//end check search?>
<br><br>

<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			//window.location = "acardVerifyStep3.php?hDelete="+val+"&hCompanyId=<?=$hCompanyId?>&Day=<?=$Day?>&Month=<?=$Month?>&Year=<?=$Year?>&Day01=<?=$Day01?>&Month01=<?=$Month01?>&Year01=&hSetMonth=10&hSetYear=0&hCarType=0&hCarModel=0&hRegisterYear=0000&hKeywordCar=&hKeyword=&hSaleId=0&hSearch=%A4%E9%B9%CB%D2%A2%E9%CD%C1%D9%C5";
			
			window.location= "<?=$pCurrentUrl?>&hDelete="+val; 
			return false;
		}else{
			return true;
		}	
	}
</script>
<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
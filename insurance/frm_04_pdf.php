<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');

$objIQ = new InsureQuotation();
$objQL01 = new InsureQuotationItem();
$objQL02 = new InsureQuotationItem();
$objQL03 = new InsureQuotationItem();

$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$hId = $objInsure->get_quotation_id();

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objInsure->get_car_id());
$objInsureCar->load();
		
$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$objCarType = new CarType();
$objCarType->setCarTypeId($objInsureCar->get_car_type());
$objCarType->load();

$objCarModel = new CarModel();
$objCarModel->setCarModelId($objInsureCar->get_car_model_id());
$objCarModel->load();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objInsureCar->get_car_series_id());
$objCarSeries->load();

$objCarColor = new CarColor();
$objCarColor->setCarColorId($objInsureCar->get_color());
$objCarColor->load();

$objSale = new Member();
$objSale->setMemberId($objInsure->get_sale_id());
$objSale->load();

$objCompany = new Company();
$objCompany->setCompanyId($objInsure->get_company_id());
$objCompany->load();

$objIC = new InsureCompany();
$objIC->setInsureCompanyId($objInsure->get_k_prb_id());
$objIC->load();

$objIQ->set_quotation_id($hId);
$objIQ->load();
$strMode="Update";

$objInsureCar = new InsureCar();
$objInsureCar->set_car_id($objIQ->get_car_id());
$objInsureCar->load();

$objCustomer = new InsureCustomer();
$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
$objCustomer->load();

$arrDate = explode("-",$objIQ->get_date_add());
$Day02 = $arrDate[2];
$Month02 = $arrDate[1];
$Year02 = $arrDate[0];

$objQIList = new InsureQuotationItemList();
$objQIList->setFilter(" quotation_id = $hId ");
$objQIList->setPageSize(0);
$objQIList->setSort(" quotation_item_id ASC ");
$objQIList->load();
$i=0;
forEach($objQIList->getItemList() as $objItem) {
	if($i==0){
		$objQL01->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL01->load();
	}
	
	if($i==1){
		$objQL02->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL02->load();
	}

	if($i==2){
		$objQL03->set_quotation_item_id($objItem->get_quotation_item_id());
		$objQL03->load();
	}
	$i++;
}

$objInsureList = New InsureList;

$objLabel = new Label();
$strText01= "��˹������š�þ������ʹ��Ҥ�";	
$objLabel->loadByCondition(" company_id=$sCompanyId and mtype = '$strText01' ");
define("IMG_INSURE","../images/misc/".$objLabel->getText02());

$page_title = "����쨴������͹������ػ�Сѹ���"; 

class PDF extends FPDF
{

//Page header
function Header()
{
	//$this->Image(IMG_INSURE,20,10,180);
	//$this->Cell(230,20,"",1,0,"C");
}

//Page footer
function Footer()
{

    //Position at 1.5 cm from bottom
    $this->SetY(0);
    //Arial italic 8
    //$this->SetFont('Arial','I',8);
    //Page number
    //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function WriteHTML($html)
{
    //HTML parser
    $html=str_replace("\n",' ',$html);
    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            //Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(20,$e);
        }
        else
        {
            //Tag
            if($e{0}=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                //Extract attributes
                $a2=explode(' ',$e);
                $tag=strtoupper(array_shift($a2));
                $attr=array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])]=$a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag,$attr)
{
    //Opening tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF=$attr['HREF'];
    if($tag=='BR')
        $this->Ln(20);
}

function CloseTag($tag)
{
    //Closing tag
    if($tag=='B' or $tag=='I' or $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF='';
}

function SetStyle($tag,$enable)
{
    //Modify style and select corresponding font
    $this->$tag+=($enable ? 1 : -1);
    $style='';
    foreach(array('B','I','U') as $s)
        if($this->$s>0)
            $style.=$s;
    $this->SetFont('',$style);
}

function PutLink($URL,$txt)
{
    //Put a hyperlink
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(15,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}


}

$hHeight = 20;
$pdf=new PDF("P","pt","A4");
$pdf->AliasNbPages();
$pdf->SetMargins(10,10,10);
$pdf->SetAutoPageBreak(false,0.5);
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddFont('angsa','B','angsab.php'); 

$pdf->AddPage();
$pdf->SetLeftMargin(20);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,25);
$pdf->Cell(180,15,$objIC->getTitle(),0,0,"C");
$pdf->Ln();
$pdf->SetXY(400,40);
$pdf->Cell(180,15,"�����Ѻ���˵� ".$objIC->getTitle02(),0,0,"C");
$pdf->Ln();
$pdf->SetXY(420,20);
$pdf->Cell(160,40,"",1,0,"L");

$extend1= 20;

$pdf->SetFont('angsa','B',16);	
$pdf->SetXY(230,50+$extend1);
$pdf->Write(10,"��������´����駵�ͻ�Сѹ���ö¹��");

$extend1= $extend1+40;

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,70+$extend1);
$pdf->Write(10,"");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,85+$extend1);
$pdf->Write(10,$objLabel->getText01());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,100+$extend1);
$pdf->Write(10,"�Ţ����͹حҵ : ".$objLabel->getText03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(400,115+$extend1);
$pdf->Write(10,"���Դ��� : ".$objSale->getFirstname()." ".$objSale->getLastname()." (".$objSale->getNickname().")");

$pdf->SetFont('angsa','B',14);	
$pdf->SetXY(400,130+$extend1);
$pdf->Write(10,"�� : ".$objLabel->getText04()." ῡ�� : ".$objLabel->getText05());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,85+$extend1);
$pdf->Write(10,"���¹  ".$objCustomer->getTitleLabel()." ".$objCustomer->getFirstname()."  ".$objCustomer->getLastname());

if($hAddressType==1 or $hAddressType=="" ){
	$hAddress1 = $objCustomer->getAddressLabel();
	if($objCustomer->mProvince == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon." ࢵ ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon." ����� ".$objCustomer->mAmphur." ".$objCustomer->mProvince." ".$objCustomer->mZip;
	}	
}
if($hAddressType==2){
	$hAddress1 = $objCustomer->getAddressLabel01();
	if($objCustomer->mProvince01 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon01." ࢵ ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon01." ����� ".$objCustomer->mAmphur01." ".$objCustomer->mProvince01." ".$objCustomer->mZip01;
	}	
}
if($hAddressType==3){
	$hAddress1 = $objCustomer->getAddressLabel02();
	if($objCustomer->mProvince02 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon02." ࢵ ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon02." ����� ".$objCustomer->mAmphur02." ".$objCustomer->mProvince02." ".$objCustomer->mZip02;
	}	
}
if($hAddressType==4){
	$hAddress1 = $objCustomer->getAddressLabel03();
	if($objCustomer->mProvince03 == "��ا෾��ҹ��"){
	$hAddress2= " �ǧ ".$objCustomer->mTumbon03." ࢵ ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
	}else{
	$hAddress2=  " �Ӻ� ".$objCustomer->mTumbon03." ����� ".$objCustomer->mAmphur03." ".$objCustomer->mProvince03." ".$objCustomer->mZip03;
	}	
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,100+$extend1);
$pdf->Write(10,"�������  ".$hAddress1);

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(35,115+$extend1);
$pdf->Write(10,$hAddress2);

$extend1= $extend1+10;

$image1 = "check.jpg";
$image2 = "un_check.jpg";
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,140+$extend1);
$pdf->Cell(560,20,"����ѷ��Сѹ��� : ".$objIC->getTitle(),1,0,"L");

if($objInsure->get_k_fix()=="GOA") { $pdf->Image($image1, 350,146+$extend1,7); }else{$pdf->Image($image2, 350, 146+$extend1,7); }
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(360,145+$extend1);
$pdf->Write(10,"������ 1 (GOA) ");

if($objInsure->get_k_fix()=="������ҧ") { $pdf->Image($image1, 425,146+$extend1,7); }else{$pdf->Image($image2, 425, 146+$extend1,7); }
$pdf->SetXY(430,145+$extend1);
$pdf->Write(10,"������ 1 �����ٹ�� ");

if($objInsure->get_k_fix()=="�������") { $pdf->Image($image1, 500,146+$extend1,7); }else{$pdf->Image($image2, 500, 146+$extend1,7); }
$pdf->SetXY(510,145+$extend1);
$pdf->Write(10,"������1 �������  ");

$extend1=$extend1+19;

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,145+$extend1);
$pdf->Write(10,"�����һ�Сѹ��� : ".$objIQ->get_text01());

if($objIQ->get_text02()=="����кؼ��Ѻ���") { $pdf->Image($image1, 350,146+$extend1,7); }else{$pdf->Image($image2, 350, 146+$extend1,7); }
$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(360,145+$extend1);
$pdf->Write(10,"����кؼ��Ѻ��� ");

if($objIQ->get_text02()=="�кؼ��Ѻ���") { $pdf->Image($image1, 425,146+$extend1,7); }else{$pdf->Image($image2, 425, 146+$extend1,7); }
$pdf->SetXY(430,145+$extend1);
$pdf->Write(10,"�кؼ��Ѻ��� ");

if($objIQ->get_text02()=="����") { $pdf->Image($image1, 500,146+$extend1,7); }else{$pdf->Image($image2, 500, 146+$extend1,7); }
$pdf->SetXY(510,145+$extend1);
$pdf->Write(10,"����  ".$objIQ->get_text03());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,165+$extend1);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text04());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(170,165+$extend1);
if($objIQ->get_text05() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatThaiDate1($objIQ->get_text05()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(290,165+$extend1);
$pdf->Write(10,"�͹حҵ� : ".$objIQ->get_text06());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(370,165+$extend1);
if($objIQ->get_text25() != "--"){
$pdf->Write(10,"�ѹ͹حҵ : ".formatThaiDate1($objIQ->get_text25()));
}else{
$pdf->Write(10,"�ѹ͹حҵ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(470,165+$extend1);
if($objIQ->get_text07() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatThaiDate1($objIQ->get_text07()));
}else{
$pdf->Write(10,"�ѹ������� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,180+$extend1);
$pdf->Write(10,"���Ѻ��� : ".$objIQ->get_text08());


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(170,180+$extend1);
if($objIQ->get_text09() != "--"){
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ".formatThaiDate1($objIQ->get_text09()));
}else{
$pdf->Write(10,"�ѹ/��͹/�� �Դ : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(290,180+$extend1);
$pdf->Write(10,"�͹حҵ� : ".$objIQ->get_text10());

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(370,180+$extend1);
if($objIQ->get_text26() != "--"){
$pdf->Write(10,"�ѹ͹حҵ : ".formatThaiDate1($objIQ->get_text26()));
}else{
$pdf->Write(10,"�ѹ͹حҵ : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(470,180+$extend1);
if($objIQ->get_text11() != "--"){
$pdf->Write(10,"�ѹ������� : ".formatThaiDate1($objIQ->get_text11()));
}else{
$pdf->Write(10,"�ѹ������� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,200+$extend1);
if($objIQ->get_text12() != "--"){
$pdf->Write(10,"�������һ�Сѹ��� : ".formatThaiDate($objIQ->get_text12()));
}else{
$pdf->Write(10,"�������һ�Сѹ��� : ");
}


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(200,200+$extend1);
if($objIQ->get_text13() != "--"){
$pdf->Write(10,"����ش : ".formatThaiDate($objIQ->get_text13()));
}else{
$pdf->Write(10,"����ش : ");
}

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(330,200+$extend1);
$pdf->Write(10,"���� : ".$objIQ->get_text14()." �.");



$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(260,220+$extend1);
$pdf->Write(10,"��¡��ö¹����һ�Сѹ���");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(20,236+$extend1);

$pdf->Cell(25,20,"�ӴѺ",1,0,"C");
$pdf->Cell(25,20,"����",1,0,"C");
$pdf->Cell(120,20,"����ö¹��/���",1,0,"C");
$pdf->Cell(90,20,"�Ţ����¹",1,0,"C");
$pdf->Cell(130,20,"�Ţ��Ƕѧ",1,0,"C");
$pdf->Cell(45,20,"�����",1,0,"C");
$pdf->Cell(50,20,"������ö",1,0,"C");
$pdf->Cell(75,20,"�ӹǹ/��Ҵ/���˹ѡ",1,0,"C");
$pdf->Ln();
$pdf->Cell(25,20,$objIQ->get_text15(),0,0,"C");
$pdf->Cell(25,20,$objIQ->get_text16(),0,0,"C");
$pdf->Cell(120,20,$objIQ->get_text17(),0,0,"C");
$pdf->Cell(90,20,$objIQ->get_text19(),0,0,"C");
$pdf->Cell(130,20,$objIQ->get_text21(),0,0,"C");
$pdf->Cell(45,20,$objIQ->get_text22(),0,0,"C");
$pdf->Cell(50,20,$objIQ->get_text23(),0,0,"C");
$pdf->Cell(75,20,$objIQ->get_text24(),0,0,"C");
$pdf->Ln();
$pdf->Cell(25,15,"",0,0,"C");
$pdf->Cell(25,15,"",0,0,"C");
$pdf->Cell(120,15,$objIQ->get_text18(),0,0,"C");
$pdf->Cell(90,15,$objIQ->get_text20(),0,0,"C");
$pdf->Cell(130,15,"",0,0,"C");
$pdf->Cell(45,15,"",0,0,"C");
$pdf->Cell(50,15,"",0,0,"C");
$pdf->Cell(75,15,"",0,0,"C");
$pdf->Ln();
$pdf->Cell(25,-35,"",1,0,"C");
$pdf->Cell(25,-35,"",1,0,"C");
$pdf->Cell(120,-35,"",1,0,"C");
$pdf->Cell(90,-35,"",1,0,"C");
$pdf->Cell(130,-35,"",1,0,"C");
$pdf->Cell(45,-35,"",1,0,"C");
$pdf->Cell(50,-35,"",1,0,"C");
$pdf->Cell(75,-35,"",1,0,"C");

$pdf->line(20,141+$extend1,580,141+$extend1);
$pdf->line(20,161+$extend1,580,161+$extend1);
$pdf->line(20,196+$extend1,580,196+$extend1);
$pdf->line(20,216+$extend1,580,216+$extend1);
$pdf->line(20,236+$extend1,580,236+$extend1);
$pdf->line(20,256+$extend1,580,256+$extend1);
//$pdf->line(20,290+$extend1,580,290+$extend1);

$pdf->line(20,141+$extend1,20,290+$extend1);
$pdf->line(580,141+$extend1,580,290+$extend1);
/*
$pdf->line(45,236+$extend1,45,290+$extend1);
$pdf->line(70,236+$extend1,70,290+$extend1);
$pdf->line(190,236+$extend1,190,290+$extend1);
$pdf->line(260,236+$extend1,260,290+$extend1);
$pdf->line(390,236+$extend1,390,290+$extend1);
$pdf->line(430,236+$extend1,430,290+$extend1);
$pdf->line(480,236+$extend1,480,290+$extend1);
*/
$extend = 100;

$pdf->SetFont('angsa','B',13);	
$pdf->SetXY(20,295+$extend);
$pdf->Cell(200,20,"�����Ѻ�Դ�ͺ��ͺؤ����¹͡",1,0,"C");
$pdf->Cell(160,20,"ö¹��������� �٭��� �����",1,0,"C");
$pdf->Cell(200,20,"����������ͧ����͡���Ṻ����",1,0,"C");
$pdf->Ln();

$pdf->Cell(200,20,"1.) ����������µ�ͪ��Ե ��ҧ�������͹����",0,0,"L");
$pdf->Cell(160,20,"1.) ����������µ�͵��ö¹��",0,0,"L");
$pdf->Cell(200,20,"1.) �غѵ��˵���ǹ�ؤ��",0,0,"L");
$pdf->Ln();

$objInsure = new Insure();
$objInsure->set_insure_id($hInsureId);
$objInsure->load();

$objQL = new InsureQuotationItem();
$objQL->set_quotation_item_id($objInsure->get_quotation_item_id());
$objQL->load();

$objIQ = new InsureQuotation();
$objIQ->set_quotation_id($objInsure->get_quotation_id());
$objIQ->load();



$pdf->Cell(200,20,number_format(str_replace(",","",$objQL->get_text03()))."  �ҷ/��",0,0,"C");

$pdf->Cell(160,20,number_format(str_replace(",","",$objQL->get_text07()))."  �ҷ/����",0,0,"C");
$pdf->Cell(200,20,"          1.1) ���ª��Ե �٭���������� �ؾ���Ҿ����",0,0,"L");
$pdf->Ln();

$pdf->Cell(200,20,number_format(str_replace(",","",$objQL->get_text04()))."  �ҷ/����",0,0,"C");
$pdf->Cell(160,20,"1.1) �������������ǹ�á",0,0,"C");
$pdf->Cell(200,20,"          �. ���Ѻ���         ".number_format(str_replace(",","",$objQL->get_text10()))."   �ҷ",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,"2) ����������µ�ͷ�Ѿ���Թ",0,0,"L");
$pdf->Cell(160,20,"- �ҷ/����",0,0,"C");

$arrText = explode("+",$objQL->get_text09());
$sum_num = $arrText[0]+$arrText[1];
$pdf->Cell(200,20,"          �. �������� ".$arrText[1]." ��        ".number_format(str_replace(",","",$objQL->get_text11()))."   �ҷ/��",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,number_format(str_replace(",","",$objQL->get_text05()))." �ҷ/����",0,0,"C");
$pdf->Cell(160,20,"",0,0,"C");
$pdf->Cell(200,20,"          1.2 �ؾ���Ҿ���Ǥ���",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,"2.1) �������������ǹ�á",0,0,"C");
$pdf->Cell(160,20,"2.) ö¹���٭��� / �����",0,0,"L");
$pdf->Cell(200,20,"          �. ���Ѻ���      -      �ҷ",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,number_format(str_replace(",","",$objQL->get_text06()))."    �ҷ/����",0,0,"C");
$pdf->Cell(160,20,number_format(str_replace(",","",$objQL->get_text08()))."    �ҷ",0,0,"C");
$pdf->Cell(200,20,"          �. �������� -  ��        -   �ҷ",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,"",0,0,"C");
$pdf->Cell(160,20,"",0,0,"C");
$pdf->Cell(200,20,"2.) ����ѡ�Ҿ�Һ��  ".$sum_num."   ��    ".number_format(str_replace(",","",$objQL->get_text11()))."    �ҷ/��",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,"",0,0,"C");
$pdf->Cell(160,20,"",0,0,"C");
$pdf->Cell(200,20,"3.) ��û�Сѹ��Ǽ��Ѻ���       ".number_format(str_replace(",","",$objQL->get_text12()))."   �ҷ/����",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,-200,"",1,0,"C");
$pdf->Cell(160,-200,"",1,0,"C");
$pdf->Cell(200,-200,"",1,0,"L");
$pdf->Ln();
$pdf->Cell(200,200,"",0,0,"C");
$pdf->Cell(160,200,"",0,0,"C");
$pdf->Cell(200,200,"",0,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,"���»�Сѹ���",1,0,"C");
$pdf->Cell(160,20,"�ú.",1,0,"C");
$pdf->Cell(200,20,"���»�Сѹ������ �ú.",1,0,"L");
$pdf->Ln();
$pdf->Cell(200,20,number_format($objInsure->get_k_num08(),2),1,0,"C");
if($objInsure->get_p_check() ==1){
$pdf->Cell(160,20,number_format($objInsure->get_k_num11(),2),1,0,"C");
}else{
$pdf->Cell(160,20,"-",1,0,"C");
}
$pdf->Cell(200,20,number_format($objInsure->get_k_num12(),2),1,0,"C");
$pdf->Ln();
$pdf->Cell(360,20,"�ʹ����",1,0,"R");
$pdf->Cell(200,20,number_format($objInsure->get_k_num13(),2),1,0,"C");
$pdf->Ln();
$pdf->Cell(560,20,"�����ö¹�� : ����ǹ�ؤ�� ������Ѻ��ҧ����������                                                                                                     ����ѷ �������� �Թ����ѹ�� �á���� �ӡѴ",1,0,"C");
$pdf->Ln();
$pdf->Cell(560,20,"�͡��ù��������Ѻ��Ǩ�ͺ�����š�â���һ�Сѹ���ö¹�� ����������ͧ������ó��������ͷ�ҹ���Ǩ�ͺ����������ͧ Ṻ�͡��û�Сͺ",0,0,"C");
$pdf->Ln();
$pdf->Cell(560,20,"(˹�ҵ��ҧ������������շ���������㺢Ѻ���ó��кؼ��Ѻ���) ��Ъ��Ф�����»�Сѹ��������ѹ���  ".formatThaiDate($objInsure->get_k_start_date())."  ���»�Сѹ����Ҩ�ա������¹�ŧ㹡ó�",0,0,"L");
$pdf->Ln();
$pdf->Cell(560,20,"�����һ�Сѹ�ա�����¡��ͧ����Թ�˹��᷹���� ����ѷ� �͢ͺ��Фس�����ҧ�٧�� � �͡�ʹ��",0,0,"L");
$pdf->Ln();


$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,730);
//$pdf->Write(15,"���ʴ������Ѻ���");
$pdf->Cell(150,20,"���ʴ������Ѻ���",0,0,"C");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,770);
$pdf->Cell(150,20,"(".$objLabel->getText07().")",0,0,"C");
//$pdf->Write(15,"(".$objSale->getFirstname()." ".$objSale->getLastname().")");

$pdf->SetFont('angsa','B',12);	
$pdf->SetXY(450,790);
$pdf->Cell(150,20,$objLabel->getText09(),0,0,"C");
//$pdf->Write(15,"��ѡ�ҹ��Сѹ���ö¹��");

$pdf->Output();	
?>
<?include "unset_all.php";?>
<script>window.close();</script>	
<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",300);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}

if($Month02 != "00" and $Year02 != "0000" ){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '$Year02-$Month02-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '$Year02-$Month02-%'   ";
		}
}

if($Month02 != "00" and $Year02 == "0000"){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '%-$Month02-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '%-$Month02-%'   ";
		}
}

if($Month02 == "00" and $Year02 != "0000"){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '$Year02-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '$Year02-%'   ";
		}
}

if($Day != "" AND $Month != "" AND $Year != ""){
	$hStartDate = $Year."-".$Month."-".$Day;
}

if($Day01 != "" AND $Month01 != "" AND $Year01 != ""){
	$hEndDate = $Year01."-".$Month01."-".$Day01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_start_date  >= '".$hStartDate."' AND P.p_start_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.p_start_date  >= '".$hStartDate."' AND P.p_start_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_start_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.p_start_date = '".$hStartDate."' ) ";
		}
	}
}


if($Month03 != "00" and $Year03 != "0000" ){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '$Year03-$Month03-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '$Year03-$Month03-%'   ";
		}
}

if($Month03 != "00" and $Year03 == "0000"){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '%-$Month03-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '%-$Month03-%'   ";
		}
}

if($Month03 == "00" and $Year03 != "0000"){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.p_start_date LIKE '$Year03-%' ";
		}else{
			$pstrCondition .=" P.p_start_date LIKE '$Year03-%'   ";
		}
}

if($Day05 != "" AND $Month05 != "" AND $Year05 != ""){
	$hStartDate = $Year05."-".$Month05."-".$Day05;
}

if($Day06 != "" AND $Month06 != "" AND $Year06 != ""){
	$hEndDate = $Year06."-".$Month06."-".$Day06;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_start_date  >= '".$hStartDate."' AND P.k_start_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_start_date  >= '".$hStartDate."' AND P.k_start_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_start_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_start_date = '".$hStartDate."' ) ";
		}
	}
}


if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_broker_id = '".$hInsureBrokerId."' ) ";
		}
}

if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_prb_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_prb_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_prb_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_prb_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}

if($hRegisterYear != "0000"){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( IC.register_year =  '".$hRegisterYear."' ) ";
		}else{
			$pstrCondition .=" ( IC.register_year = '".$hRegisterYear."' ) ";
		}
}


$sesCondition = $pstrCondition;
session_register("sesCondition",$sesCondition);

}//end hSearch

/*
if($pstrCondition == ""){
	$pstrCondition= $sesCondition;
}else{
	$pstrCondition= " AND ".$sesCondition;
}
*/

echo $pstrCondition;

$objInsureList = new InsureList();
$objInsureList->setFilter($pstrCondition);
$objInsureList->setPageSize(PAGE_SIZE);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" p_start_date, k_start_date ASC ");
$objInsureList->setSort($hSort);
$objInsureList->loadList();

$pCurrentUrl = "insure_sale_list.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "2. �к��ҹ��»�Сѹ���";
$strHead03 = "���Ң������١���";
$pageContent = " ��¡�çҹ��»�Сѹ���";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="get">
<table align="center" class="search" cellpadding="3">
<tr>
	<td align="right">�Ң� :</td>
	<td><?=$objC->printSelect("hCompanyId",$hCompanyId,"- �ء�Ң� -");?></td>
</tr>
<tr>
	<td align="right">�ѹ��������ͧ�Ҥ�ѧ�Ѻ : </td>
	<td>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
			<td class="error">&nbsp;&nbsp;&nbsp;&nbsp;�����繪�ǧ��͹�� :</td>
			<td class="error">
				<table>
				<tr>
					<td>
						<select name="Month02">
						<option value="00" >- �ء��͹ -
						<option value="01" >���Ҥ�
						<option value="02" >����Ҿѹ��
						<option value="03" >�չҤ�
						<option value="04" >����¹
						<option value="05" >����Ҥ�
						<option value="06" >�Զع�¹		
						<option value="07" >�á�Ҥ�
						<option value="08" >�ԧ�Ҥ�
						<option value="09" >�ѹ¹¹
						<option value="10" >���Ҥ�
						<option value="11" >��Ȩԡ�¹
						<option value="12" >�ѹ�Ҥ�																					
					</select>				
					</td>
					<td>-</td>
					<td>
					<select name="Year02">
						<option value="0000">- �ء�� -
						<?for($i=2007;$i<=date("Y")+3;$i++){?>
						<option value="<?=$i?>"><?=$i?>
						<?}?>				
					</select>				
					</td>
					<td class="error"></td>
				</tr>
				</table>
			</td>
			<td class="error">&nbsp;</td>
			
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="right">	�ѹ��������ͧ�Ҥ��Ѥ�� : </td>
	<td>
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day05 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month05 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year05 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year05,Day05, Month05, Year05,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day06 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month06 value=""></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year06 value=""></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year06,Day06, Month06, Year06,popCal);return false"></td>		
			<td class="error">&nbsp;&nbsp;&nbsp;&nbsp;�����繪�ǧ��͹�� :</td>
			<td class="error">
				<table>
				<tr>
					<td>
						<select name="Month03">
						<option value="00" >- �ء��͹ -
						<option value="01" >���Ҥ�
						<option value="02" >����Ҿѹ��
						<option value="03" >�չҤ�
						<option value="04" >����¹
						<option value="05" >����Ҥ�
						<option value="06" >�Զع�¹		
						<option value="07" >�á�Ҥ�
						<option value="08" >�ԧ�Ҥ�
						<option value="09" >�ѹ¹¹
						<option value="10" >���Ҥ�
						<option value="11" >��Ȩԡ�¹
						<option value="12" >�ѹ�Ҥ�																					
					</select>				
					</td>
					<td>-</td>
					<td>
					<select name="Year03">
						<option value="0000">- �ء�� -
						<?for($i=2007;$i<=date("Y")+3;$i++){?>
						<option value="<?=$i?>"><?=$i?>
						<?}?>				
					</select>				
					</td>
					<td class="error"></td>
				</tr>
				</table>
			</td>
			<td class="error">&nbsp;</td>
			
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td align="right">������ö :</td>
	<td>
			<table>
			<td  class="i_background04" ><?=$objCarType01->printSelectScript("hCarType","","����к�","populate01(document.frm01,document.frm01.hCarType.options[document.frm01.hCarType.selectedIndex].value,document.frm01.hCarModel)");?></td>
			<td>���ö :</td>
			<td  class="i_background04" >
			<?=$objCarModelList->printSelectScript("hCarModel","","����к�")?>
			</td>
			<td>��ö :</td>
			<td  class="i_background04" >
			<select name="hRegisterYear">
				<option value="0000">
			<?for($i=date("Y");$i> (date("Y")-25);$i--){?>
				<option value="<?=$i?>" ><?=$i?>
			<?}?>
			</select>		
			</td>

			</table>	
	</td>
</tr>
<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	
<tr>
	<td align="right"> �Ҥ��Ѥ�� :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId","","����к�");?></td>
			<td>�Ţ��������</td>
			<td><input type="text" name="hKeywordPrb" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> �Ҥ�ѧ�Ѻ :</td>
	<td class="small">
		<table>
		<tr>
			<td>Broker</td>
			<td><?=$objInsureBrokerList->printSelect("hInsureBrokerId01","","����к�");?></td>
			<td>��Сѹ���</td>
			<td><?$objInsureCompanyList->printSelect("hInsureCompanyId01","","����к�");?></td>
			<td>�Ţ��������</td>
			<td><input type="text" name="hKeywordKom" value=""></td>			
		</tr>
		</table>
	</td>
</tr>	
<tr>
	<td align="right"> ��ѡ�ҹ��Ңͧ��¡�� :</td>
	<td class="small">
     	<?=$objMemberList->printSelect("hSaleId",$hSaleId,"������");?>
	</td>
</tr>	
<tr>
	<td align="right"> ʶҹС�÷ӧҹ :</td>
	<td class="small">
		<select name="hStatus">
			<option value="">- �ءʶҹ� -
			<option value="new">1. �觨�����
			<option value="call 1">2. �ä��駷�� 1 �ʹͺ�ԡ��
			<option value="quotation">3. �͡�͡�����ʹ��Ҥ�
			<option value="call 2">4. �ä��駷�� 2 �Դ��â��
			<option value="set up">5. ���͡��úѹ�֡�â�¡�˹�����
			<option value="tracking">6. �ä��駷�� 3 �Ѵ���¡�ê���
			<option value="complete">7. ���ͺ���������
			<option value="cancel">8. ¡��ԡ���������
		</select>
	</td>
</tr>	
<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch">&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="��૵������" name="hReset"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureList->getCount();?> ��¡��</div>


<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td width="5%" align="center" class="ListTitle">�ѹ������ͧ</td>
	<td width="7%" align="center" class="ListTitle">��������â��</td>
	<td width="15%" align="center" class="ListTitle">�Ţ���������</td>
	<td width="12%" align="center" class="ListTitle">����-���ʡ��</td>
	<td width="5%" align="center" class="ListTitle">�Ţ����¹</td>


	<td width="5%" align="center" class="ListTitle">Broker</td>
	<td width="15%" align="center" class="ListTitle">��Сѹ���</td>

	<td width="5%" align="center" class="ListTitle">��������´</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem) {
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objItem->get_car_id());
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objItem->get_customer_id());
		$objCustomer->load();

		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objItem->get_p_prb_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objItem->get_k_prb_id());
		$objKomCompany->load();
		
		$objPrbBroker = new InsureBroker();
		$objPrbBroker->setInsureBrokerId($objItem->get_p_broker_id());
		$objPrbBroker->load();
		
		$objKomBroker = new InsureBroker();
		$objKomBroker->setInsureBrokerId($objItem->get_k_broker_id());
		$objKomBroker->load();
		
		$objPrbStock = new StockInsure();
		$objPrbStock->set_stock_insure_id($objItem->get_p_stock_id());
		$objPrbStock->load();
		
		$objKomStock = new StockInsure();
		$objKomStock->set_stock_insure_id($objItem->get_k_stock_id());
		$objKomStock->load();
		if($hPage==0)$hPage=1;
	?>
<?if($objItem->get_p_check() == 1 or  $objItem->get_p_start_date() != "0000-00-00" ){?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" align="center" ><?=(($hPage-1)*50)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" align="center"  >�Ҥ�ѧ�Ѻ</td>	
	<td valign="top" ><?=$objPrbStock->get_title()?></td>
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>	
	<td valign="top" align="center" ><?=$objInsureCar->get_code()?></td>
	<td valign="top" align="center" ><?=$objPrbBroker->getTitle()?></td>		
	<td valign="top" ><?=$objPrbCompany->getTitle()?></td>	

	<td  valign="top" align="center">
	<a target="_blank" href="step06List.php?hCarId=<?=$objInsureCar->get_car_id()?>&hYearExtend=<?=$hYearExtend?>&hInsureId=<?=$objItem->get_insure_id()?>">��������´</a>
	</td>	
</tr>
<?}?>
<?if($objItem->get_k_check() == 1 or  $objItem->get_k_start_date() != "0000-00-00" ){?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($objItem->get_k_check() == 1){?>
	<td valign="top" align="center" colspan="3" ></td>	
	<?}else{?>
	<td valign="top" align="center" ><?=(($hPage-1)*50)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objItem->get_p_start_date())?></td>	
	<td valign="top" align="center"  >�Ҥ��Ѥ��</td>	
	<td valign="top" ><?=$objKomStock->get_title()?></td>
	<td valign="top" ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>	
	<td valign="top" align="center" ><?=$objInsureCar->get_code()?></td>
	<?}?>
	<td valign="top" align="center" ><?=$objKomBroker->getTitle()?></td>		
	<td valign="top" ><?=$objKomCompany->getTitle()?></td>	

	<td  valign="top" align="center">
	<a target="_blank" href="step06List.php?hCarId=<?=$objInsureCar->get_car_id()?>&hYearExtend=<?=$objItem->get_year_extend()?>&hInsureId=<?=$objItem->get_insure_id()?>">��������´</a>
	</td>	
</tr>
<?}?>
	<?
		++$i;
			}
	Unset($objInsureList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>
<script>
	function checkDelete(val){
		if(confirm('�����ŵ�ҧ � �������ѹ��Ѻ��¡�èж١ź��駷����� �س��ͧ��÷���ź��¡�ù��������� ?')){
			window.location = "acardList.php?hDelete="+val;
			return false;
		}else{
			return true;
		}	
	}
</script>
<?
	include("h_footer.php")
?>
<?include "unset_all.php";?>
<?
include("common.php");

$objInsureList = new InsureAdlerList();
$objInsureList->setFilter(" P.status = 'checked'   ");
$objInsureList->setPageSize(0);
$objInsureList->setPage($hPage);
$objInsureList->setSort(" insure_adler_id ASC ");
$objInsureList->load();
$i=0;
$m = 1;
forEach($objInsureList->getItemList() as $objItem) {
	//update adler status
	$objQA = new InsureAdler();
	$objQA->set_insure_adler_id($objItem->get_insure_adler_id());
	$objQA->set_status("exported");
	$objQA->set_export_date(date("Y-m-d"));
	$objQA->update_export();
	++$i;
}
unset($objInsureList);
unset($objQA);
header("location:counter_adler_exported.php");
exit;
?>

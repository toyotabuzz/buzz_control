<?
/*********************************************************
		Class :					Stock Red

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_red table

*********************************************************/
 
class StockRed extends DB{

	var $TABLE="t_stock_red";

	var $mStockRedId;
	function getStockRedId() { return $this->mStockRedId; }
	function setStockRedId($data) { $this->mStockRedId = $data; }
	
	var $mStockDate;
	function getStockDate() { return htmlspecialchars($this->mStockDate); }
	function setStockDate($data) { $this->mStockDate = $data; }	
	
	var $mStockBy;
	function getStockBy() { return htmlspecialchars($this->mStockBy); }
	function setStockBy($data) { $this->mStockBy = $data; }	
	
	var $mStockName;
	function getStockName() { return $this->mStockName; }
	function setStockName($data) { $this->mStockName = $data; }	
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	function getStatusDetail() { 
	
				switch ($this->mStatus){
					case 0:
						return "��ҧ";
						break;
					case 1:
						return "�����ҧ";
						break;
				}
 	}
 
	var $mPrice;
	function getPrice() { return $this->mPrice; }
	function setPrice($data) { $this->mPrice = $data; }	 
	
	var $mReal;
	function getReal() { return $this->mReal; }
	function setReal($data) { $this->mReal = $data; }	 	
	function getRealDetail() { 
		if( $this->mReal == 1 ) return "����ᴧ�ҡ����";
		if( $this->mReal == 2 ) return "����ᴧ buzz";
		if( $this->mReal == 3 ) return "���¨ҡ�١���";
	}

	
	var $mStockCarId;
	function getStockCarId() { return $this->mStockCarId; }
	function setStockCarId($data) { $this->mStockCarId = $data; }	 	
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }	
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }		
	

	function StockRed($objData=NULL) {
        If ($objData->stock_red_id !="") {
            $this->setStockRedId($objData->stock_red_id);
			$this->setStockDate($objData->stock_date);
			$this->setStockBy($objData->stock_by);
			$this->setStockName($objData->stock_name);
			$this->setStatus($objData->status);
			$this->setPrice($objData->price);
			$this->setReal($objData->reals);
			$this->setStockCarId($objData->stock_car_id);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
			$this->setCompanyId($objData->company_id);
        }
    }

	function init(){
		$this->setStockName(stripslashes($this->mStockName));
	}
		
	function load() {

		if ($this->mStockRedId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_red_id =".$this->mStockRedId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockRed($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function loadUTF8() {

		if ($this->mStockRedId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_red_id =".$this->mStockRedId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockRed($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockRed($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_date, stock_by, stock_name, price, reals, stock_car_id, company_id, status ) "
						." VALUES ( '".$this->mStockDate
						."' , '".$this->mStockBy
						."', '".$this->mStockName
						."', '".$this->mPrice
						."', '".$this->mReal
						."', '".$this->mStockCarId
						."', '".$this->mCompanyId
						."', '".$this->mStatus."'  ) ";
        $this->getConnection();		
        If ($result = $this->query($strSql)) { 
            $this->mStockRedId = mysql_insert_id();
			$this->unsetConnection();
            return $this->mStockRedId;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_date = '".$this->mStockDate."'  "
						." , stock_by = '".$this->mStockBy."'  "
						." , stock_name = '".$this->mStockName."'  "
						." , status = '".$this->mStatus."'  "
						." , reals = '".$this->mReal."'  "
						." , stock_car_id = '".$this->mStockCarId."'  "
						." , price = '".$this->mPrice."'  "
						." WHERE  stock_red_id = ".$this->mStockRedId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
		
	}

	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." WHERE  stock_red_Id = ".$this->mStockRedId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStockCarId(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_car_id = '".$this->mStockCarId."' , "
						." status = 1 "
						." WHERE  stock_red_id = ".$this->mStockRedId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateDelete(){
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  stock_red_id = ".$this->mStockRedId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_red_id=".$this->mStockRedId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_stock_red_item  "
        . " WHERE stock_red_id =".$this->mStockRedId." ";
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function checkDelete(){
        if ($this->mOrderId == 0)
       {
			return true;
        }
		return false;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockName == "") {
			$asrErrReturn["stock_name"] = '��س��кط���¹����ᴧ';
		}else{
			if(!$this->checkUniqueStockName(strtolower($Mode) )){
				//echo "dd";
			
				$asrErrReturn["stock_name"] = '���ʻ���ᴧ��ӫ�͹';
			}		
		}				
        Return $asrErrReturn;
    }
	
    function checkUniqueStockName($strMode)
    {
        $strSql  = "SELECT stock_name FROM t_stock_red ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE stock_name = ".trim(strtoupper($this->convStr4SQL($this->mStockName)));
		}else{
			$strSql .= " WHERE stock_name = ".trim(strtoupper($this->convStr4SQL($this->mStockName)))." and stock_red_id != ".$this->mStockRedId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
}

/*********************************************************
		Class :				Customer Stock Red List

		Last update :		22 Mar 02

		Description:		Customer Stock Red List

*********************************************************/

class StockRedList extends DataList {
	var $TABLE = "t_stock_red";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_red_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockRed($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();		
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.stock_red_id) as rowCount FROM ".$this->TABLE." P  "
			." LEFT JOIN t_stock_red_item SI ON SI.stock_red_id = P.stock_red_id  "
			." LEFT JOIN t_stock_car SC ON SC.stock_car_id = SI.stock_car_id  "
			." LEFT JOIN t_order O ON O.order_id = SC.order_id  "
			." LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT  DISTINCT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_stock_red_item SI ON SI.stock_red_id = P.stock_red_id  "
			." LEFT JOIN t_stock_car SC ON SC.stock_car_id = SI.stock_car_id  "
			." LEFT JOIN t_order O ON O.order_id = SC.order_id  "
			." LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockRed($row);
			}
			$result->freeResult();
			$this->unsetConnection();			
			return true;
		} else {
			$this->unsetConnection();		
			return false;
		}
    }
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_red_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockRed($row);
			}
			$result->freeResult();
			$this->unsetConnection();			
			return true;
		} else {
			$this->unsetConnection();		
			return false;
		}
    }	
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockRedId()."\"");
			if (($defaultId != null) && ($objItem->getStockRedId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getStockName()."</option>");
		}
		echo("</select>");
	}			
	
}
<?
/*********************************************************
		Class :					Stock Red Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_red_item table

*********************************************************/
 
class StockRedItem extends DB{

	var $TABLE="t_stock_red_item";

	var $mStockRedItemId;
	function getStockRedItemId() { return $this->mStockRedItemId; }
	function setStockRedItemId($data) { $this->mStockRedItemId = $data; }
	
	var $mStockRedId;
	function getStockRedId() { return $this->mStockRedId; }
	function setStockRedId($data) { $this->mStockRedId = $data; }
	
	var $mStockDate;
	function getStockDate() { return $this->mStockDate; }
	function setStockDate($data) { $this->mStockDate = $data; }	
	
	var $mStockTime;
	function getStockTime() { return $this->mStockTime; }
	function setStockTime($data) { $this->mStockTime = $data; }	
	
	var $mStockBy;
	function getStockBy() { return $this->mStockBy; }
	function setStockBy($data) { $this->mStockBy = $data; }	
	
	var $mSaleId;
	function getSaleId() { return $this->mSaleId; }
	function setSaleId($data) { $this->mSaleId = $data; }	
	
	var $mStockNumber;
	function getStockNumber() { return $this->mStockNumber; }
	function setStockNumber($data) { $this->mStockNumber = $data; }	
	
	var $mStockCarId;
	function getStockCarId() { return $this->mStockCarId; }
	function setStockCarId($data) { $this->mStockCarId = $data; }
	
	var $mStockReasonId;
	function getStockReasonId() { return $this->mStockReasonId; }
	function setStockReasonId($data) { $this->mStockReasonId = $data; }	
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mStockDateReturn;
	function getStockDateReturn() { return $this->mStockDateReturn; }
	function setStockDateReturn($data) { $this->mStockDateReturn = $data; }	
	
	var $mStockTimeReturn;
	function getStockTimeReturn() { return $this->mStockTimeReturn; }
	function setStockTimeReturn($data) { $this->mStockTimeReturn = $data; }	
	
	var $mStockByReturn;
	function getStockByReturn() { return $this->mStockByReturn; }
	function setStockByReturn($data) { $this->mStockByReturn = $data; }	
	
	var $mSaleIdReturn;
	function getSaleIdReturn() { return $this->mSaleIdReturn; }
	function setSaleIdReturn($data) { $this->mSaleIdReturn = $data; }	
	
	var $mStockNumberReturn;
	function getStockNumberReturn() { return $this->mStockNumberReturn; }
	function setStockNumberReturn($data) { $this->mStockNumberReturn = $data; }	
	
	var $mStockBlack;
	function getStockBlack() { return $this->mStockBlack; }
	function setStockBlack($data) { $this->mStockBlack = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }	
	
	var $mNewStockRedId;
	function getNewStockRedId() { return $this->mNewStockRedId; }
	function setNewStockRedId($data) { $this->mNewStockRedId = $data; }	
	
	var $mRemarkReturn;
	function getRemarkReturn() { return $this->mRemarkReturn; }
	function setRemarkReturn($data) { $this->mRemarkReturn = $data; }	
	
	var $mRecieveBy;
	function getRecieveBy() { return $this->mRecieveBy; }
	function setRecieveBy($data) { $this->mRecieveBy = $data; }		
	
	var $mRecieveMoneyBy;
	function getRecieveMoneyBy() { return $this->mRecieveMoneyBy; }
	function setRecieveMoneyBy($data) { $this->mRecieveMoneyBy = $data; }
	
	var $mPrice;
	function getPrice() { return $this->mPrice; }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mReturnNumber;
	function getReturnNumber() { return $this->mReturnNumber; }
	function setReturnNumber($data) { $this->mReturnNumber = $data; }	
	
	var $mReturnStatus;
	function getReturnStatus() { return $this->mReturnStatus; }
	function setReturnStatus($data) { $this->mReturnStatus = $data; }		
	
	var $mRecieveType;
	function getRecieveType() { return $this->mRecieveType; }
	function setRecieveType($data) { $this->mRecieveType = $data; }		
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	function getStatusDetail() { 
	
				switch ($this->mStatus){
					case 0:
						return "��ҧ";
						break;
					case 1:
						return "�����ҧ";
						break;
				}
 	}	
	
	function StockRedItem($objData=NULL) {
        If ($objData->stock_red_item_id !="") {
            $this->setStockRedItemId($objData->stock_red_item_id);
			$this->setStockRedId($objData->stock_red_id);
			$this->setStockDate($objData->stock_date);
			$this->setStockTime($objData->stock_time);
			$this->setStockBy($objData->stock_by);
			$this->setSaleId($objData->sale_id);
			$this->setStockNumber($objData->stock_number);
			$this->setStockCarId($objData->stock_car_id);
			$this->setStockReasonId($objData->stock_reason_id);
			$this->setRemark($objData->remark);
			$this->setStockDateReturn($objData->stock_date_return);
			$this->setStockTimeReturn($objData->stock_time_return);
			$this->setStockByReturn($objData->stock_by_return);
			$this->setSaleIdReturn($objData->sale_id_return);
			$this->setStockNumberReturn($objData->stock_number_return);
			$this->setStockBlack($objData->stock_black);
			$this->setOrderId($objData->order_id);
			$this->setNewStockRedId($objData->new_stock_red_id);
			$this->setRemarkReturn($objData->remark_return);
			$this->setRecieveBy($objData->recieve_by);
			$this->setRecieveMoneyBy($objData->recieve_money_by);
			$this->setPrice($objData->price);
			$this->setReturnNumber($objData->return_number);
			$this->setRecieveType($objData->recieve_type);
			$this->setReturnStatus($objData->return_status);
			$this->setStatus($objData->status);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
		$this->setStockCarId(stripslashes($this->mStockCarId));
	}
	
	function load() {

		if ($this->mStockRedItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_red_item_id =".$this->mStockRedItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockRedItem($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockRedItem($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_red_id,stock_date, stock_time, order_id, stock_reason_id, stock_number, stock_by, sale_id, stock_car_id, status, price, remark ) "
						." VALUES ( '".$this->mStockRedId
						."' , '".$this->mStockDate
						."', '".$this->mStockTime
						."', '".$this->mOrderId
						."' ,  '".$this->mStockReasonId
						."' ,  '".$this->mStockNumber
						."' ,  '".$this->mStockBy
						."' , '".$this->mSaleId
						."' , '".$this->mStockCarId
						."' , '".$this->mStatus
						."' , '".$this->mPrice
						."' , '".$this->mRemark."' ) ";
        $this->getConnection();		
        If ($result = $this->query($strSql)) { 
            $this->mStockRedItemId = mysql_insert_id();		
			$this->unsetConnection();
            return $this->mStockRedItemId;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_red_id = '".$this->mStockRedId."'  "
						." , stock_date_return = '".$this->mStockDateReturn."'  "
						." , stock_number_return = '".$this->mStockNumberReturn."'  "
						." , stock_time_return = '".$this->mStockTimeReturn."'  "
						." , stock_by_return = '".$this->mStockByReturn."'  "
						." , sale_id_return = '".$this->mSaleIdReturn."'  "
						." , stock_black = '".$this->mStockBlack."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , new_stock_red_id = '".$this->mNewStockRedId."'  "
						." , status = '".$this->mStatus."'  "
						." , remark_return = '".$this->mRemarkReturn."'  "
						." , recieve_by = '".$this->mRecieveBy."'  "
						." , return_number = '".$this->mReturnNumber."'  "
						." , return_status = '".$this->mReturnStatus."'  "
						." , recieve_type = '".$this->mRecieveType."'  "
						." , recieve_money_by = '".$this->mRecieveMoneyBy."'  "
						." WHERE  stock_red_item_id = ".$this->mStockRedItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_red_item_id=".$this->mStockRedItemId." ";
        $this->getConnection();
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockRedId == "") $asrErrReturn["stock_red_id"] = "��س��к�";		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Red Item List

		Last update :		22 Mar 02

		Description:		Customer Stock Red Item List

*********************************************************/

class StockRedItemList extends DataList {
	var $TABLE = "t_stock_red_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_red_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockRedItem($row);
			}
    	    $result->freeResult();
			$this->unsetConnection();

			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadCompany() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_red_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_stock_red O ON O.stock_red_id = P.stock_red_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT  DISTINCT  P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_stock_red O ON O.stock_red_id = P.stock_red_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockRedItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
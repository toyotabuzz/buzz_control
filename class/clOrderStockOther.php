<?
/*********************************************************
		Class :					Order Stock Other

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_stock_other table

*********************************************************/
 
class OrderStockOther extends DB{

	var $TABLE="t_order_stock_other";

	var $mOrderStockOtherId;
	function getOrderStockOtherId() { return $this->mOrderStockOtherId; }
	function setOrderStockOtherId($data) { $this->mOrderStockOtherId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }

	var $mStockProductId;
	function getStockProductId() { return $this->mStockProductId; }
	function setStockProductId($data) { $this->mStockProductId = $data; }
	
	var $mType;
	function getType() { return htmlspecialchars($this->mType); }
	function setType($data) { $this->mType = $data; }		
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }	

	var $mQty;
	function getQty() { return htmlspecialchars($this->mQty); }
	function setQty($data) { $this->mQty = $data; }
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mTMT;
	function getTMT() { return htmlspecialchars($this->mTMT); }
	function setTMT($data) { $this->mTMT = $data; }	
	
	var $mStock;
	function getStock() { return htmlspecialchars($this->mStock); }
	function setStock($data) { $this->mStock = $data; }		
	
	function OrderStockOther($objData=NULL) {
        If ($objData->order_stock_other_id!="") {
            $this->setOrderStockOtherId($objData->order_stock_other_id);
			$this->setOrderId($objData->order_id);
			$this->setType($objData->type);
			$this->setTitle($objData->title);
			$this->setQty($objData->qty);
			$this->setPrice($objData->price);
			$this->setTMT($objData->tmt);
			$this->setStock($objData->stock);
        }
    }

	function init(){
		$this->setQty(stripslashes($this->mQty));
	}
		
	function load() {

		if ($this->mOrderStockOtherId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE order_stock_other_id=".$this->mOrderStockOtherId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderStockOther($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderStockOther($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( qty, title, type, price, tmt, order_id, stock ) "
						." VALUES ( '".$this->mQty."','".$this->mTitle."','".$this->mType."','".$this->mPrice."','".$this->mTMT."','".$this->mOrderId."','".$this->mStock."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderStockOtherId = mysql_insert_id();
            return $this->mOrderStockOtherId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET qty = '".$this->mQty."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , title = '".$this->mTitle."'  "
						." , type = '".$this->mType."'  "
						." , tmt = '".$this->mTMT."'  "
						." , stock = '".$this->mStock."'  "
						." , price = '".$this->mPrice."'  "
						." WHERE  order_stock_other_id= ".$this->mOrderStockOtherId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_stock_other_id=".$this->mOrderStockOtherId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mQty == "") $asrErrReturn["qty"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Stock List

		Last update :		22 Mar 02

		Description:		Order Stock List

*********************************************************/

class OrderStockOtherList extends DataList {
	var $TABLE = "t_order_stock_other";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_stock_other_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderStockOther($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderStockOtherId()."\"");
			if (($defaultId != null) && ($objItem->getOrderStockOtherId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getQty()."</option>");
		}
		echo("</select>");
	}

}
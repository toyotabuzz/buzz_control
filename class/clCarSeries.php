<?
/*********************************************************
		Class :					Car Series

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_serie table

*********************************************************/
 
class CarSeries extends DB{

	var $TABLE="t_car_series";

	var $mCarSeriesId;
	function getCarSeriesId() { return $this->mCarSeriesId; }
	function setCarSeriesId($data) { $this->mCarSeriesId = $data; }
	
	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }

	var $mCarModelTitle;
	function getCarModelTitle() { return $this->mCarModelTitle; }
	function setCarModelTitle($data) { $this->mCarModelTitle = $data; }
	
	var $mCarTypeId;
	function getCarTypeId() { return $this->mCarTypeId; }
	function setCarTypeId($data) { $this->mCarTypeId = $data; }
	
	var $mCarTypeTitle;
	function getCarTypeTitle() { return $this->mCarTypeTitle; }
	function setCarTypeTitle($data) { $this->mCarTypeTitle = $data; }	

	var $mCarType;
	function getCarType() { return $this->mCarType; }
	function setCarType($data) { $this->mCarType = $data; }	

	function getCarTypeDetail() { 
	
				switch ($this->mCarType){
					case 1:
						return "ö��";
						break;
					case 2:
						return "ö�к�";
						break;
					case 3:
						return "ö 7 �����";
						break;
					case 4:
						return "ö���";
						break;
				}
 }

	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mModel;
	function getModel() { return htmlspecialchars($this->mModel); }
	function setModel($data) { $this->mModel = $data; }	
	
	var $mRetail;
	function getRetail() { return htmlspecialchars($this->mRetail); }
	function setRetail($data) { $this->mRetail = $data; }		
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }		
	
	var $mCarNumber;
	function getCarNumber() { return htmlspecialchars($this->mCarNumber); }
	function setCarNumber($data) { $this->mCarNumber = $data; }		
	
	var $mEngineNumber;
	function getEngineNumber() { return htmlspecialchars($this->mEngineNumber); }
	function setEngineNumber($data) { $this->mEngineNumber = $data; }				
	
	var $mMargin;
	function getMargin() { return htmlspecialchars($this->mMargin); }
	function setMargin($data) { $this->mMargin = $data; }	
	
	var $mMarginDate;
	function getMarginDate() { return htmlspecialchars($this->mMarginDate); }
	function setMarginDate($data) { $this->mMarginDate = $data; }	
	
	var $mGoa;
	function getGoa() { return htmlspecialchars($this->mGoa); }
	function setGoa($data) { $this->mGoa = $data; }		

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }		
	
	var $mExpire;
	function getExpire() { return htmlspecialchars($this->mExpire); }
	function setExpire($data) { $this->mExpire = $data; }		
	
	var $mStartDate;
	function getStartDate() { return htmlspecialchars($this->mStartDate); }
	function setStartDate($data) { $this->mStartDate = $data; }		
	
	//���¶֧���»�Сѹ����繤��Ѵ��â���������öź��
	var $mInsureAdd;
	function getInsureAdd() { return htmlspecialchars($this->mInsureAdd); }
	function setInsureAdd($data) { $this->mInsureAdd = $data; }
	
	var $mCC;
	function getCC() { return $this->mCC; }
	function setCC($data) { $this->mCC = $data; }
	
	var $mGear;
	function getGear() { return $this->mGear; }
	function setGear($data) { $this->mGear = $data; }	
	
	function CarSeries($objData=NULL) {
        If ($objData->car_series_id !="") {
            $this->setCarSeriesId($objData->car_series_id);
			$this->setCarTypeId($objData->car_type_id);
			$this->setCarTypeTitle($objData->car_type_title);
			$this->setCarType($objData->car_type);
			$this->setCarModelId($objData->car_model_id);
			$this->setCarModelTitle($objData->car_model_title);
			$this->setTitle($objData->title);
			$this->setModel($objData->model);
			$this->setRetail($objData->retail);
			$this->setPrice($objData->price);
			$this->setCarNumber($objData->car_number);
			$this->setEngineNumber($objData->engine_number);
			$this->setMargin($objData->margin);
			$this->setMarginDate($objData->margin_date);
			$this->setGoa($objData->goa);
			$this->setRank($objData->rank);
			$this->setExpire($objData->expire);
			$this->setStartDate($objData->start_date);
			$this->setInsureAdd($objData->insure_add);
			$this->setCC($objData->cc);
			$this->setGear($objData->gear);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCarSeriesId == '') {
			return false;
		}
		$strSql = "SELECT CS.*, CM.title AS car_model_title , CT.title AS car_type_title FROM ".$this->TABLE." CS "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_car_type CT ON CT.car_type_id = CS.car_type_id  "
		."  WHERE car_series_id =".$this->mCarSeriesId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarSeries($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mCarSeriesId == '') {
			return false;
		}
		$strSql = "SELECT CS.*, CM.title AS car_model_title , CT.title AS car_type_title FROM ".$this->TABLE." CS "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_car_type CT ON CT.car_type_id = CS.car_type_id  "
		."  WHERE car_series_id =".$this->mCarSeriesId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarSeries($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT CS.*, CM.title AS car_model_title , CT.title AS car_type_title FROM ".$this->TABLE." CS "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_car_type CT ON CT.car_type_id = CS.car_type_id  "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarSeries($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, car_type_id, car_type,  car_model_id, retail, price, car_number, engine_number, margin, margin_date, goa, rank, start_date, expire, insure_add, cc, model, gear ) "
						." VALUES ( '".$this->mTitle
						."','".$this->mCarTypeId."','"
						.$this->mCarType."','"
						.$this->mCarModelId."','"
						.$this->mRetail."','"
						.$this->mPrice."','"
						.$this->mCarNumber."','"
						.$this->mEngineNumber."','"
						.$this->mMargin."','"
						.$this->mMarginDate."','"
						.$this->mGoa."','"
						.$this->mRank."','"
						.$this->mStartDate."','"
						.$this->mExpire."','"
						.$this->mInsureAdd."','".$this->mCC."','".$this->mModel."' ,'".$this->mGear."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCarSeriesId = mysql_insert_id();
            return $this->mCarSeriesId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , car_type = '".$this->mCarType."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." , car_number = '".$this->mCarNumber."'  "
						." , engine_number = '".$this->mEngineNumber."'  "
						." , cc = '".$this->mCC."'  "
						." , rank = '".$this->mRank."'  "
						." , insure_add = '".$this->mInsureAdd."'  "
						." , model = '".$this->mModel."'  "
						." , gear = '".$this->mGear."'  "
						." WHERE  car_series_id = ".$this->mCarSeriesId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function updateAccount(){
		$strSql = "UPDATE ".$this->TABLE
						." SET retail = '".$this->mRetail."'  "
						." , price = '".$this->mPrice."'  "
						." , expire = '".$this->mExpire."'  "
						." , start_date = '".$this->mStartDate."'  "
						." WHERE  car_series_id = ".$this->mCarSeriesId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateRank(){
		$strSql = "UPDATE ".$this->TABLE
						." SET rank= '".$this->mRank."'  "
						." WHERE  car_series_id = ".$this->mCarSeriesId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateInsure(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , car_type = '".$this->mCarType."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." , car_number = '".$this->mCarNumber."'  "
						." , engine_number = '".$this->mEngineNumber."'  "
						." , rank = '".$this->mRank."'  "
						." , cc = '".$this->mCC."'  "
						." , retail = '".$this->mRetail."'  "
						." , price = '".$this->mPrice."'  "	
						." , insure_add = '".$this->mInsureAdd."'  "
						." , model = '".$this->mModel."'  "
						." WHERE  car_series_id = ".$this->mCarSeriesId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_series_id=".$this->mCarSeriesId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mModel == "") $asrErrReturn["model"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Series List

		Last update :		22 Mar 02

		Description:		Car Series List

*********************************************************/

class CarSeriesList extends DataList {
	var $TABLE = "t_car_series";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_series_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarSeries($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT CS.car_series_id) as rowCount FROM ".$this->TABLE." CS  "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_car_type CT ON CT.car_type_id = CS.car_type_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT CS.*, CM.title AS car_model_title , CT.title AS car_type_title FROM ".$this->TABLE." CS "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_car_type CT ON CT.car_type_id = CS.car_type_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarSeries($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarSeriesId()."\"");
			if (($defaultId != null) && ($objItem->getCarSeriesId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarSeriesId()."\"");
			if (($defaultId != null) && ($objItem->getCarSeriesId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	function printSelectModel($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarSeriesId()."\"");
			if (($defaultId != null) && ($objItem->getCarSeriesId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getModel()."</option>");
		}
		echo("</select>");
	}			
	
	
}
<?
/*********************************************************
		Class :					Customer Duplicate

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_duplicate table

*********************************************************/
 
class CustomerDuplicate extends DB{

	var $TABLE="t_customer_duplicate";

	var $mDuplicateId;
	function getDuplicateId() { return $this->mDuplicateId; }
	function setDuplicateId($data) { $this->mDuplicateId = $data; }
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }
	
	var $mDuplicateDate;
	function getDuplicateDate() { return htmlspecialchars($this->mDuplicateDate); }
	function setDuplicateDate($data) { $this->mDuplicateDate = $data; }
	
	var $mMemberId;
	function getMemberId() { return htmlspecialchars($this->mMemberId); }
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mSaleId;
	function getSaleId() { return htmlspecialchars($this->mSaleId); }
	function setSaleId($data) { $this->mSaleId = $data; }

	var $mEventId;
	function getEventId() { return htmlspecialchars($this->mEventId); }
	function setEventId($data) { $this->mEventId = $data; }

	var $mGroupId;
	function getGroupId() { return htmlspecialchars($this->mGroupId); }
	function setGroupId($data) { $this->mGroupId = $data; }

	var $mInformationDate;
	function getInformationDate() { return htmlspecialchars($this->mInformationDate); }
	function setInformationDate($data) { $this->mInformationDate = $data; }
	
	function CustomerDuplicate($objData=NULL) {
        If ($objData->duplicate_id !="") {
            $this->setDuplicateId($objData->duplicate_id);
			$this->setCustomerId($objData->customer_id);
			$this->setDuplicateDate($objData->duplicate_date);
			$this->setMemberId($objData->member_id);
			$this->setSaleId($objData->sale_id);
			$this->setEventId($objData->event_id);
			$this->setGroupId($objData->group_id);
			$this->setInformationDate($objData->information_date);
        }
    }

	function init(){

	}
		
	function load() {

		if ($this->mDuplicateId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE duplicate_id =".$this->mDuplicateId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerDuplicate($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerDuplicate($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( duplicate_date , customer_id, member_id, sale_id, group_id, event_id, information_date ) "
						." VALUES ( '".date("Y-m-d H:i:s")."' , '".$this->mCustomerId."' , '".$this->mMemberId."' , '".$this->mSaleId."' , '".$this->mGroupId."' , '".$this->mEventId."' , '".$this->mInformationDate."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mDuplicateId = mysql_insert_id();
            return $this->mDuplicateId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET duplicate_date = '".$this->mDuplicateDate."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." , member_id = '".$this->mMemberId."'  "
						." , sale_id = '".$this->mSaleId."'  "
						." , group_id = '".$this->mGroupId."'  "
						." , event_id = '".$this->mEventId."'  "
						." , information_date = '".$this->mInformationDate."'  "
						." WHERE  duplicate_id = ".$this->mDuplicateId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE duplicate_id=".$this->mDuplicateId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Duplicate List

		Last update :		22 Mar 02

		Description:		Customer Duplicate List

*********************************************************/

class CustomerDuplicateList extends DataList {
	var $TABLE = "t_customer_duplicate";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT duplicate_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerDuplicate($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getDuplicateId()."\"");
			if (($defaultId != null) && ($objItem->getDuplicateId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getDuplicateDate()."</option>");
		}
		echo("</select>");
	}			
	
}
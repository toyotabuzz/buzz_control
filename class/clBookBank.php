<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class BookBank extends DB{

	var $TABLE="t_bookbank";

	var $m_bookbank_id;
	function get_bookbank_id() { return $this->m_bookbank_id; }
	function set_bookbank_id($data) { $this->m_bookbank_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }
	
	var $m_bank;
	function get_bank() { return $this->m_bank; }
	function set_bank($data) { $this->m_bank = $data; }
	
	var $m_branch;
	function get_branch() { return $this->m_branch; }
	function set_branch($data) { $this->m_branch = $data; }
	
	var $m_type;
	function get_type() { return $this->m_type; }
	function set_type($data) { $this->m_type = $data; }
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }
	
	function BookBank($objData=NULL) {
        If ($objData->bookbank_id !="") {
			$this->set_bookbank_id($objData->bookbank_id);
			$this->set_title($objData->title);
			$this->set_code($objData->code);
			$this->set_bank($objData->bank);
			$this->set_branch($objData->branch);
			$this->set_type($objData->type);
			$this->set_company_id($objData->company_id);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_bookbank_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE bookbank_id =".$this->m_bookbank_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BookBank($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BookBank($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( title , code , bank , branch , type , company_id ) " ." VALUES ( "
		." '".$this->m_title."' , "
		." '".$this->m_code."' , "
		." '".$this->m_bank."' , "
		." '".$this->m_branch."' , "
		." '".$this->m_type."' , "
		." '".$this->m_company_id."' "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_bookbank_id = mysql_insert_id();
            return $this->m_bookbank_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." title = '".$this->m_title."' "
		." , code = '".$this->m_code."' "
		." , bank = '".$this->m_bank."' "
		." , branch = '".$this->m_branch."' "
		." , type = '".$this->m_type."' "
		." , company_id = '".$this->m_company_id."' "
		." WHERE bookbank_id = ".$this->m_bookbank_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE bookbank_id=".$this->m_bookbank_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class BookBankList extends DataList {
	var $TABLE = "t_bookbank";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT bookbank_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new BookBank($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_bookbank_id()."\"");
			if (($defaultId != null) && ($objItem->get_bookbank_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()." , ".$objItem->get_bank()."</option>");
		}
		echo("</select>");
	}		
	
	function printSelectInsure($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		if($this->m_bookbank_id != ''){
			$strSql = "SELECT * FROM t_bookbank, t_bank WHERE bookbank_id = ".$this->m_bookbank_id ." AND  " . $this->TABLE.".bank = tbank.bank_id AND t_bookbank.isactive = 'Y'";
		} else {
			$strSql = "SELECT * FROM t_bookbank, t_bank WHERE " . $this->TABLE.".bank = t_bank.bank_id AND t_bookbank.isactive = 'Y'";
		}
		$this->getConnection();
		$result = $this->query($strSql);
		while($row = $result->nextRow()) {
			echo("<option value=\"".$row->bookbank_id."\"");
			if (($defaultId != null) && ($row->bookbank_id == $defaultId)) {
				echo(" selected");
			}
			echo(">".$row->title."</option>");
		}
		// foreach ($this->mItemList as $objItem) {
		// 	echo '<pre>';
		// 	print_r($objItem);
		// 	exit;					
		// 	echo("<option value=\"".$objItem->get_bookbank_id()."\"");
		// 	if (($defaultId != null) && ($objItem->get_bookbank_id() == $defaultId)) {
		// 		echo(" selected");
		// 	}
		// 	echo(">".$objItem->get_bank()."</option>");
		// }
		echo("</select>");
	}		
}
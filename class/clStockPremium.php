<?
/*********************************************************
		Class :					Stock Premium

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_premium table

*********************************************************/
 
class StockPremium extends DB{

	var $TABLE="t_stock_premium";

	var $mStockPremiumId;
	function getStockPremiumId() { return $this->mStockPremiumId; }
	function setStockPremiumId($data) { $this->mStockPremiumId = $data; }
	
	var $mCode;
	function getCode() { return $this->mCode; }
	function setCode($data) { $this->mCode = $data; }	
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mStockDate;
	function getStockDate() { return htmlspecialchars($this->mStockDate); }
	function setStockDate($data) { $this->mStockDate = $data; }
	
	var $mTotal;
	function getTotal() { return htmlspecialchars($this->mTotal); }
	function setTotal($data) { $this->mTotal = $data; }

	var $mPack;
	function getPack() { return htmlspecialchars($this->mPack); }
	function setPack($data) { $this->mPack = $data; }
	
	var $mWarning;
	function getWarning() { return $this->mWarning; }
	function setWarning($data) { $this->mWarning = $data; }	
	
	var $mWarningAmount;
	function getWarningAmount() { return $this->mWarningAmount; }
	function setWarningAmount($data) { $this->mWarningAmount = $data; }		
	
	function StockPremium($objData=NULL) {
        If ($objData->stock_premium_id !="") {
            $this->setStockPremiumId($objData->stock_premium_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setTotal($objData->total);
			$this->setPack($objData->pack);
			$this->setWarning($objData->warning);
			$this->setWarningAmount($objData->warning_amount);
			$this->setStockDate($objData->stock_date);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
		$this->setCode(stripslashes($this->mCode));
		$this->setPack(stripslashes($this->mPack));
	}
		
	function load() {

		if ($this->mStockPremiumId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_premium_id =".$this->mStockPremiumId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title,code,stock_date,total, pack , warning, warning_amount  ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mCode."', '".date("Y-m-d")."' ,  '".$this->mTotal."' , '".$this->mPack."', '".$this->mWarning."' , '".$this->mWarningAmount."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockPremiumId = mysql_insert_id();
            return $this->mStockPremiumId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , code = '".$this->mCode."'  "
						." , stock_date = '".date("Y-m-d")."'  "
						." , total = '".$this->mTotal."'  "
						." , pack = '".$this->mPack."'  "
						." , warning = '".$this->mWarning."'  "
						." , warning_amount = '".$this->mWarningAmount."'  "
						." WHERE  stock_premium_id = ".$this->mStockPremiumId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateTotal(){
		$strSql = "UPDATE ".$this->TABLE
						." SET total = '".$this->mTotal."'  "
						." WHERE  stock_premium_id = ".$this->mStockPremiumId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_premium_id=".$this->mStockPremiumId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == "") $asrErrReturn["code"] = "��س��к�";
		if ($this->mTotal == "") $asrErrReturn["total"] = "��س��к�";
		if ($this->mPack == "") $asrErrReturn["pack"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Premium List

		Last update :		22 Mar 02

		Description:		Customer Stock Premium List

*********************************************************/

class StockPremiumList extends DataList {
	var $TABLE = "t_stock_premium";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_premium_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPremium($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_premium_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPremium($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockPremiumId()."\"");
			if (($defaultId != null) && ($objItem->getStockPremiumId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
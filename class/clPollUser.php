<?
/*********************************************************
		Class :				PollUser

		Last update :		20 Nov 02

		Description:		Class manage PollUser table

*********************************************************/
 
class PollUser extends DB{

	var $TABLE="t_poll_user";
	
	var $mPollUserId;
	function getPollUserId() { return $this->mPollUserId; }
	function setPollUserId($data) { $this->mPollUserId = $data; }
	
	var $mPollId;
	function getPollId() { return $this->mPollId; }
	function setPollId($data) { $this->mPollId = $data; }

	var $mName;
	function getName() { return $this->mName; }
	function setName($data) { $this->mName = $data; }

	var $mCountry;
	function getCountry() { return htmlspecialchars($this->mCountry); }
	function setCountry($data) { $this->mCountry = $data; }

	var $mEmail;
	function getEmail() { return htmlspecialchars($this->mEmail); }
	function setEmail($data) { $this->mEmail = $data; }

	var $mPostedDate;
	function getPostedDate() { return htmlspecialchars($this->mPostedDate); }
	function setPostedDate($data) { $this->mPostedDate = $data; }

	function PollUser($objData=NULL) {
        If ($objData->pollUserId!="") {
            $this->setPollUserId($objData->pollUserId);
            $this->setPollId($objData->pollId);
			$this->setName($objData->name);
			$this->setCountry($objData->country);
			$this->setEmail($objData->email);
			$this->setPostedDate($objData->postedDate);
        }
    }

	function init(){	
		$this->setEmail(stripslashes($this->mEmail));
	}
	
	function load() {

		if ($this->mPollUserId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE pollUserId =".$this->mPollUserId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollUser($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadCount($strCondition) {

		$strSql = "SELECT Count(pollId) as rowCount   FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return $row->rowCount;
            }
        }
		return false;
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( email, name, country, pollId, postedDate  )"
						." VALUES ( '".$this->mEmail."' , "
						."  '".$this->mName."' , "
						."  '".$this->mCountry."' , "
						."  '".$this->mPollId."' , "
						."  '".date("Y-m-d")."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollUserId = mysql_insert_id();
            return $this->mPollUserId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET email = '".$this->mEmail."' , "
						." country = '".$this->mCountry."' , "
						." name = '".$this->mName."' , "
						." postedDate = '".$this->mPostedDate."' , "
						." pollId = '".$this->mPollId."'  "
						." WHERE  pollUserId = ".$this->mPollUserId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pollUserId=".$this->mPollUserId." ";
        $this->getConnection();
        $this->query($strSql);
       $strSql = " DELETE FROM t_poll_back "
                . " WHERE pollUserId=".$this->mPollUserId." ";
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_FILE_NEWS.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_FILE_NEWS.$mFileName) And (is_file(PATH_FILE_NEWS.$mFileName) ) )
	    {
		    unLink(PATH_FILE_NEWS.$mFileName);
	    }
	}
}

/*********************************************************
		Class :				PollUserList

		Last update :		20 Nov 02

		Description:		PollUseruser list

*********************************************************/


class PollUserList extends DataList {
	var $TABLE = "t_poll_user";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pollUserId) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "				
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollUser($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
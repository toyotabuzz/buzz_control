<?
/*********************************************************
		Class :					Insure Quotation

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureQuotationSet extends DB{

	var $TABLE="t_insure_quotation_set";

	var $m_quotation_set_id;
	function get_quotation_set_id() { return $this->m_quotation_set_id; }
	function set_quotation_set_id($data) { $this->m_quotation_set_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_text01;
	function get_text01() { return $this->m_text01; }
	function set_text01($data) { $this->m_text01 = $data; }
	
	var $m_text02;
	function get_text02() { return $this->m_text02; }
	function set_text02($data) { $this->m_text02 = $data; }
	
	var $m_text03;
	function get_text03() { return $this->m_text03; }
	function set_text03($data) { $this->m_text03 = $data; }
	
	var $m_text04;
	function get_text04() { return $this->m_text04; }
	function set_text04($data) { $this->m_text04 = $data; }
	
	var $m_text05;
	function get_text05() { return $this->m_text05; }
	function set_text05($data) { $this->m_text05 = $data; }
	
	var $m_text06;
	function get_text06() { return $this->m_text06; }
	function set_text06($data) { $this->m_text06 = $data; }
	
	var $m_text07;
	function get_text07() { return $this->m_text07; }
	function set_text07($data) { $this->m_text07 = $data; }
	
	var $m_text08;
	function get_text08() { return $this->m_text08; }
	function set_text08($data) { $this->m_text08 = $data; }
	
	var $m_text09;
	function get_text09() { return $this->m_text09; }
	function set_text09($data) { $this->m_text09 = $data; }
	
	var $m_text10;
	function get_text10() { return $this->m_text10; }
	function set_text10($data) { $this->m_text10 = $data; }
	
	var $m_text11;
	function get_text11() { return $this->m_text11; }
	function set_text11($data) { $this->m_text11 = $data; }
	
	var $m_text12;
	function get_text12() { return $this->m_text12; }
	function set_text12($data) { $this->m_text12 = $data; }
	
	var $m_text13;
	function get_text13() { return $this->m_text13; }
	function set_text13($data) { $this->m_text13 = $data; }
	
	var $m_text14;
	function get_text14() { return $this->m_text14; }
	function set_text14($data) { $this->m_text14 = $data; }	
	
	var $m_text15;
	function get_text15() { return $this->m_text15; }
	function set_text15($data) { $this->m_text15 = $data; }
	
	var $m_text16;
	function get_text16() { return $this->m_text16; }
	function set_text16($data) { $this->m_text16 = $data; }		
	
	var $m_text17;
	function get_text17() { return $this->m_text17; }
	function set_text17($data) { $this->m_text17 = $data; }		
	
	var $m_text18;
	function get_text18() { return $this->m_text18; }
	function set_text18($data) { $this->m_text18 = $data; }		
	
	var $m_text19;
	function get_text19() { return $this->m_text19; }
	function set_text19($data) { $this->m_text19 = $data; }		
	
	var $m_text20;
	function get_text20() { return $this->m_text20; }
	function set_text20($data) { $this->m_text20 = $data; }		
	
	var $m_text21;
	function get_text21() { return $this->m_text21; }
	function set_text21($data) { $this->m_text21 = $data; }		
	
	var $m_text22;
	function get_text22() { return $this->m_text22; }
	function set_text22($data) { $this->m_text22 = $data; }		
	
	var $m_text23;
	function get_text23() { return $this->m_text23; }
	function set_text23($data) { $this->m_text23 = $data; }							
	
	var $m_insure_title;
	function get_insure_title() { return $this->m_insure_title; }
	function set_insure_title($data) { $this->m_insure_title = $data; }			
	
	var $m_show_web;
	function get_show_web() { return $this->m_show_web; }
	function set_show_web($data) { $this->m_show_web = $data; }

	function InsureQuotationSet($objData=NULL) {

        If ($objData->quotation_set_id !="") {
			$this->set_quotation_set_id($objData->quotation_set_id);
			$this->set_title($objData->title);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_text01($objData->text01);
			$this->set_text02($objData->text02);
			$this->set_text03($objData->text03);
			$this->set_text04($objData->text04);
			$this->set_text05($objData->text05);
			$this->set_text06($objData->text06);
			$this->set_text07($objData->text07);
			$this->set_text08($objData->text08);
			$this->set_text09($objData->text09);
			$this->set_text10($objData->text10);
			$this->set_text11($objData->text11);
			$this->set_text12($objData->text12);
			$this->set_text13($objData->text13);
			$this->set_text14($objData->text14);
			$this->set_text15($objData->text15);
			$this->set_text16($objData->text16);
			$this->set_text17($objData->text17);
			$this->set_text18($objData->text18);
			$this->set_text19($objData->text19);
			$this->set_text20($objData->text20);
			$this->set_text21($objData->text21);
			$this->set_text22($objData->text22);
			$this->set_text23($objData->text23);
			$this->set_show_web($objData->show_web);
			
			$this->set_insure_title($objData->insure_title);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_quotation_set_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE quotation_set_id =".$this->m_quotation_set_id;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureQuotationSet($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureQuotationSet($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( title, insure_company_id, text01, text02, text03, text04, text05, text06, text07, text08, text09, text10, text11, text12, text13, text14, text15, text16, text17, text18, text19, text20, text21, text22, text23, show_web  ) " ." VALUES ( "
		." '".$this->m_title."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_text01."' , "
		." '".$this->m_text02."' , "
		." '".$this->m_text03."' , "
		." '".$this->m_text04."' , "
		." '".$this->m_text05."' , "
		." '".$this->m_text06."' , "
		." '".$this->m_text07."' , "
		." '".$this->m_text08."' , "
		." '".$this->m_text09."' , "
		." '".$this->m_text10."' , "
		." '".$this->m_text11."' , "
		." '".$this->m_text12."' , "
		." '".$this->m_text13."' , "
		." '".$this->m_text14."' ,"
		." '".$this->m_text15."' , "
		." '".$this->m_text16."' , "
		." '".$this->m_text17."' , "
		." '".$this->m_text18."' , "
		." '".$this->m_text19."' , "
		." '".$this->m_text20."' , "
		." '".$this->m_text21."' , "
		." '".$this->m_text22."' , "
		." '".$this->m_text23."' ,  "
		." '".$this->m_show_web."'   "
		." ) "; 
 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_quotation_set_id = mysql_insert_id();
            return $this->m_quotation_set_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  title = '".$this->m_title."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , text01 = '".$this->m_text01."' "
		." , text02 = '".$this->m_text02."' "
		." , text03 = '".$this->m_text03."' "
		." , text04 = '".$this->m_text04."' "
		." , text05 = '".$this->m_text05."' "
		." , text06 = '".$this->m_text06."' "
		." , text07 = '".$this->m_text07."' "
		." , text08 = '".$this->m_text08."' "
		." , text09 = '".$this->m_text09."' "
		." , text10 = '".$this->m_text10."' "
		." , text11 = '".$this->m_text11."' "
		." , text12 = '".$this->m_text12."' "
		." , text13 = '".$this->m_text13."' "
		." , text14 = '".$this->m_text14."' "
		." , text15 = '".$this->m_text15."' "
		." , text16 = '".$this->m_text16."' "
		." , text17 = '".$this->m_text17."' "
		." , text18 = '".$this->m_text18."' "
		." , text19 = '".$this->m_text19."' "
		." , text20 = '".$this->m_text20."' "
		." , text21 = '".$this->m_text21."' "
		." , text22 = '".$this->m_text22."' "
		." , text23 = '".$this->m_text23."' "
		." , show_web = '".$this->m_show_web."' "
		." WHERE quotation_set_id = ".$this->m_quotation_set_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE quotation_set_id=".$this->m_quotation_set_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureQuotationSetList extends DataList {
	var $TABLE = "t_insure_quotation_set";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT quotation_set_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_company C ON C.insure_company_id = P.insure_company_id "
		
		.$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, C.title as insure_title FROM ".$this->TABLE." P "
			." LEFT JOIN t_insure_company C ON C.insure_company_id = P.insure_company_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureQuotationSet($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT quotation_set_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_company C ON C.insure_company_id = P.insure_company_id "
		
		.$this->getFilterSQL();	// WHERE clause

		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, C.title as insure_title FROM ".$this->TABLE." P "
			." LEFT JOIN t_insure_company C ON C.insure_company_id = P.insure_company_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureQuotationSet($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
}
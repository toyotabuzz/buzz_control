<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class StockInsure extends DB{

	var $TABLE="t_stock_insure";

	var $m_stock_insure_id;
	function get_stock_insure_id() { return $this->m_stock_insure_id; }
	function set_stock_insure_id($data) { $this->m_stock_insure_id = $data; }
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_title01;
	function get_title01() { return $this->m_title01; }
	function set_title01($data) { $this->m_title01 = $data; }	
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	//1. ʵ�ͤ�ѭ��  2. ʵ�ͤ�. ��Сѹ���  3.�Ѵʵ�ͤ
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	//�������� / �ú.
	var $m_insure_type;
	function get_insure_type() { return $this->m_insure_type; }
	function set_insure_type($data) { $this->m_insure_type = $data; }
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }

	//ʶҹС�â�� 1.��������  2.���  3. ���ش 4.¡��ԡ 5.����ա�â��
	var $m_status_sale;
	function get_status_sale() { return $this->m_status_sale; }
	function set_status_sale($data) { $this->m_status_sale = $data; }	
	
	var $m_cprice;
	function get_cprice() { return $this->m_cprice; }
	function set_cprice($data) { $this->m_cprice = $data; }	
	
	var $m_stock_remark;
	function get_stock_remark() { return $this->m_stock_remark; }
	function set_stock_remark($data) { $this->m_stock_remark = $data; }		
	
	var $m_cancel_date;
	function get_cancel_date() { return $this->m_cancel_date; }
	function set_cancel_date($data) { $this->m_cancel_date = $data; }			
	
	var $m_account_remark;
	function get_account_remark() { return $this->m_account_remark; }
	function set_account_remark($data) { $this->m_account_remark = $data; }		
	
	var $m_insure_officer_id;
	function get_insure_officer_id() { return $this->m_insure_officer_id; }
	function set_insure_officer_id($data) { $this->m_insure_officer_id = $data; }			
	
	var $m_insure_cancel_id;
	function get_insure_cancel_id() { return $this->m_insure_cancel_id; }
	function set_insure_cancel_id($data) { $this->m_insure_cancel_id = $data; }				
	
	var $m_doc_send;
	function get_doc_send() { return $this->m_doc_send; }
	function set_doc_send($data) { $this->m_doc_send = $data; }		
	
	function StockInsure($objData=NULL) {
        If ($objData->stock_insure_id !=""  ) {
			$this->set_stock_insure_id($objData->stock_insure_id);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_title($objData->title);
			$this->set_title01($objData->title01);
			$this->set_price($objData->price);
			$this->set_insure_id($objData->insure_id);
			$this->set_status($objData->status);
			$this->set_insure_type($objData->insure_type);
			$this->set_company_id($objData->company_id);
			$this->set_status_sale($objData->status_sale);
			$this->set_cprice($objData->cprice);
			$this->set_stock_remark($objData->stock_remark);
			$this->set_cancel_date($objData->cancel_date);
			$this->set_account_remark($objData->account_remark);
			$this->set_insure_officer_id($objData->insure_officer_id);
			$this->set_insure_cancel_id($objData->insure_cancel_id);
			$this->set_doc_send($objData->doc_send);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_stock_insure_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_insure_id =".$this->m_stock_insure_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockInsure($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockInsure($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(stock_insure_id) AS RSUM  FROM ".$this->TABLE."  "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( broker_id , insure_company_id , title , title01, price , insure_id , status , insure_type , company_id, stock_remark, status_sale, account_remark, created_at, created_by ) " ." VALUES ( "
		." '".$this->m_broker_id."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_title."' , "
		." '".$this->m_title01."' , "
		." '".$this->m_price."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_status."' , "
		." '".$this->m_insure_type."' , "
		." '".$this->m_company_id."' , "
		." '".$this->m_stock_remark."' , "
		." '".$this->m_status_sale."' , "
		." '".$this->m_account_remark."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_stock_insure_id = mysql_insert_id();
            return $this->m_stock_insure_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   broker_id = '".$this->m_broker_id."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , title = '".$this->m_title."' "
		." , title01 = '".$this->m_title01."' "
		." , price = '".$this->m_price."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   company_id = '".$this->m_company_id."' "
		." , status = '".$this->m_status."' "
		." , status_sale = '".$this->m_status_sale."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
		//echo $strSql;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatusInsure(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   company_id = '".$this->m_company_id."' "
		." , status = '".$this->m_status."' "
		." , status_sale = '".$this->m_status_sale."' "
		." , cprice = '".$this->m_cprice."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateDoc(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   doc_send = '".$this->m_doc_send."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateStatusPrbDamage(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  status_sale = '".$this->m_status_sale."' "
		." , cprice = '".$this->m_cprice."' "
		." , cancel_date = '".$this->m_cancel_date."' "
		." , stock_remark = '".$this->m_stock_remark."' "
		." , insure_officer_id = '".$this->m_insure_officer_id."' "
		." , insure_cancel_id = '".$this->m_insure_cancel_id."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatusPrbReturn(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  status_sale = '".$this->m_status_sale."' "
		." , insure_id = '".$this->m_insure_id."' "
		." , cprice = '".$this->m_cprice."' "
		." , cancel_date = '".$this->m_cancel_date."' "
		." , stock_remark = '".$this->m_stock_remark."' "
		." , insure_officer_id = '".$this->m_insure_officer_id."' "
		." , insure_cancel_id = '".$this->m_insure_cancel_id."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateStatusSale(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  insure_id = '".$this->m_insure_id."' "
		." , price = '".$this->m_price."' "
		." , cprice = '".$this->m_cprice."' "
		." , status_sale = '".$this->m_status_sale."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
		
		//echo $strSql;
		
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function update_status_step05(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  insure_id = '".$this->m_insure_id."' "
		." , price = '".$this->m_price."' "
		." , status_sale = '".$this->m_status_sale."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStatusCancel(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  insure_id = '".$this->m_insure_id."' "
		." , cprice = '".$this->m_cprice."' "
		." , cancel_date = '".$this->m_cancel_date."' "
		." , stock_remark = '".$this->m_stock_remark."' "
		." , status_sale = '".$this->m_status_sale."' "
		." WHERE stock_insure_id = ".$this->m_stock_insure_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_insure_id=".$this->m_stock_insure_id." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_stock_insure_detail  WHERE stock_insure_id=".$this->m_stock_insure_id." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	   
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class StockInsureList extends DataList {
	var $TABLE = "t_stock_insure";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_insure_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockInsure($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_stock_insure_id()."\"");
			if (($defaultId != null) && ($objItem->get_stock_insure_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}
}
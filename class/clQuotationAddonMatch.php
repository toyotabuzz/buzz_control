<?
/*********************************************************
		Class :			  QuotationAddonMatch

		Last update :	  11 Mar 21

		Description:	  Class manage tmquotation_addon_match table

*********************************************************/
 
class QuotationAddonMatch extends DB{

	var $TABLE="tmquotation_addon_match";

	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_quotation_addon_id;
	function get_quotation_addon_id() { return $this->m_quotation_addon_id; }
	function set_quotation_addon_id($data) { $this->m_quotation_addon_id = $data; }

	var $m_payment_subject_id;
	function get_payment_subject_id() { return $this->m_payment_subject_id; }
	function set_payment_subject_id($data) { $this->m_payment_subject_id = $data; }
	
	var $m_isactive;
	function get_isactive() { return $this->m_isactive; }
	function set_isactive($data) { $this->m_isactive = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }
    
    function QuotationAddonMatch($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_quotation_addon_id($objData->quotation_addon_id);
			$this->set_payment_subject_id($objData->payment_subject_id);
			$this->set_isactive($objData->isactive);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id." ";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->QuotationAddonMatch($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->QuotationAddonMatch($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_addon_id , payment_subject_id , isactive  , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_quotation_addon_id."' , "
		." '".$this->m_payment_subject_id."' , "
		." '".$this->m_isactive."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." quotation_addon_id = '".$this->m_quotation_addon_id."' "
		." , payment_subject_id = '".$this->m_payment_subject_id."' "
		." , isactive = '".$this->m_isactive."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :			  Insure Company List

		Last update :	  11 Mar 21

		Description:	  Insure Company List

*********************************************************/


class QuotationAddonMatchList extends DataList {
	var $TABLE = "tmquotation_addon_match";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT id) as rowCount FROM ".$this->TABLE." OP  "
		.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.* FROM ".$this->TABLE." OP  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new QuotationAddonMatch($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}

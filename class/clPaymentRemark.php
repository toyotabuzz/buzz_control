<?
/*********************************************************
		Class :					Payment Type

		Last update :	  10 Jan 02

		Description:	  Class manage t_payment_remark table

*********************************************************/
 
class PaymentRemark extends DB{

	var $TABLE="t_payment_remark";

	var $mPaymentRemarkId;
	function getPaymentRemarkId() { return $this->mPaymentRemarkId; }
	function setPaymentRemarkId($data) { $this->mPaymentRemarkId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }

	var $mDel;
	function getDel() { return htmlspecialchars($this->mDel); }
	function setDel($data) { $this->mDel = $data; }
	
	
	function PaymentRemark($objData=NULL) {
        If ($objData->payment_remark_id !="") {
            $this->setPaymentRemarkId($objData->payment_remark_id);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
			$this->setDel($objData->del);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mPaymentRemarkId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE payment_remark_id =".$this->mPaymentRemarkId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PaymentRemark($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PaymentRemark($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, rank, del ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mRank."' ,'".$this->mDel."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mPaymentRemarkId = mysql_insert_id();
            return $this->mPaymentRemarkId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , rank = '".$this->mRank."' , del =  '".$this->mDel."'  "
						." WHERE  payment_remark_id = ".$this->mPaymentRemarkId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE payment_remark_id=".$this->mPaymentRemarkId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Payment Type List

		Last update :		22 Mar 02

		Description:		Payment Type List

*********************************************************/

class PaymentRemarkList extends DataList {
	var $TABLE = "t_payment_remark";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT payment_remark_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PaymentRemark($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPaymentRemarkId()."\"");
			if (($defaultId != null) && ($objItem->getPaymentRemarkId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPaymentRemarkId()."\"");
			if (($defaultId != null) && ($objItem->getPaymentRemarkId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
<?
/*********************************************************
		Class :				Province

		Last update :		16 NOV 02

		ProvinceCode:		Class manage province table

*********************************************************/

class Province extends DB{

	var $TABLE="t_area_province";
	
	var $mProvinceId;
	function getProvinceId() { return $this->mProvinceId; }
	function setProvinceId($data) { $this->mProvinceId = $data; }
	
	var $mProvinceName;
	function getProvinceName() { return htmlspecialchars($this->mProvinceName); }
	function setProvinceName($data) { $this->mProvinceName = $data; }

	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }	

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	

	var $mPcode;
	function getPcode() { return htmlspecialchars($this->mPcode); }
	function setPcode($data) { $this->mPcode = $data; }	
	
	function Province($objData=NULL) {
        If ($objData->province_id!="") {
            $this->setProvinceId($objData->province_id);
            $this->setProvinceName($objData->province_name);
			$this->setProvinceCode($objData->province_code);
			$this->setRank($objData->rank);
			$this->setPcode($objData->pcode);
        }
    }
	
	function init(){
		$this->setProvinceName(stripslashes($this->mProvinceName));
	}
	
	function load() {

		if ($this->mProvinceId == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE C.province_id =".$this->mProvinceId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Province($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Province($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( province_name, province_code, rank ) "
						." VALUES ( '".$this->mProvinceName."' ,"
						." '".$this->mProvinceCode."' , "
						." '".$this->mRank."' ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mProvinceId = mysql_insert_id();
            return $this->mProvinceId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET province_name = '".$this->mProvinceName."' , "
						." province_code = '".$this->mProvinceCode."' ,  "
						." rank = '".$this->mRank."'   "
						." WHERE  province_id = ".$this->mProvinceId."  ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updatePcode(){
		$strSql = "UPDATE ".$this->TABLE
						." SET pcode = '".$this->mPcode."'  "
						." WHERE  province_id = ".$this->mProvinceId."  ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE province_id=".$this->mProvinceId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				if ($this->mProvinceName == "") $asrErrReturn["province_name"] = "Please enter province name";
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }

}

/*********************************************************
		Class :				ProvinceList

		Last update :		22 Mar 02

		ProvinceCode:		Province user list

*********************************************************/


class ProvinceList extends DataList {
	var $TABLE = "t_area_province";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT province_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.* FROM ".$this->TABLE." C "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Province($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT province_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.* FROM ".$this->TABLE." C "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Province($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getProvinceCode()."\"");
			if (($defaultId != null) && ($objItem->getProvinceCode() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getProvinceName()."</option>");
		}
		echo("</select>");
	}

	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getProvinceCode()."\"");
			if (($defaultId != null) && ($objItem->getProvinceCode() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getProvinceName()."</option>");
		}
		echo("</select>");
	}

}
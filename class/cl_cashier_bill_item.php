<?php
/*********************************************************
		Class :				CashierBillItem

		Last update :		16 NOV 02

		Description:		Class manage category table

*********************************************************/
class CashierBillItem extends DB{

	var $TABLE="t_cashier_bill_item";
	
	var $m_cashier_bill_item_id;
	function get_cashier_bill_item_id() { return $this->m_cashier_bill_item_id; }
	function set_cashier_bill_item_id($data) { $this->m_cashier_bill_item_id = $data; }
	
	var $m_cashier_bill_id;
	function get_cashier_bill_id() { return $this->m_cashier_bill_id; }
	function set_cashier_bill_id($data) { $this->m_cashier_bill_id = $data; }
	
	var $m_rank;
	function get_rank() { return $this->m_rank; }
	function set_rank($data) { $this->m_rank = $data; }
	
	var $m_x1;
	function get_x1() { return $this->m_x1; }
	function set_x1($data) { $this->m_x1 = $data; }
	
	var $m_y1;
	function get_y1() { return $this->m_y1; }
	function set_y1($data) { $this->m_y1 = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	function CashierBillItem($objData=NULL) {
        If (isset($objData)) {
			$this->set_cashier_bill_item_id($objData->cashier_bill_item_id);
			$this->set_cashier_bill_id($objData->cashier_bill_id);
			$this->set_rank($objData->rank);
			$this->set_x1($objData->x1);
			$this->set_y1($objData->y1);
			$this->set_title($objData->title);
        }
    }
	
	function init(){
		$this->set_title(stripslashes($this->m_title));
		$this->set_content(stripslashes($this->m_content));
	}
	
	function load() {

		if ($this->m_cashier_bill_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE cashier_bill_item_id =".$this->m_cashier_bill_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierBillItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strSql) {

		if ($strSql == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE ".$strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierBillItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}		

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( cashier_bill_id , rank , x1 , y1 , title ) " ." VALUES ( "
		." '".$this->m_cashier_bill_id."' , "
		." '".$this->m_rank."' , "
		." '".$this->m_x1."' , "
		." '".$this->m_y1."' , "
		." '".$this->m_title."'  "
		." ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->m_cashier_bill_item_id = mysql_insert_id();
            return $this->m_cashier_bill_item_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." cashier_bill_id = '".$this->m_cashier_bill_id."' "
		." , rank = '".$this->m_rank."' "
		." , x1 = '".$this->m_x1."' "
		." , y1 = '".$this->m_y1."' "
		." , title = '".$this->m_title."' "

		." WHERE cashier_bill_item_id = ".$this->m_cashier_bill_item_id." ";
        $this->getConnection();
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE cashier_bill_item_id=".$this->m_cashier_bill_item_id." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {

    }
	
	Function checkDelete()
    {
        return true;
    }



}

/*********************************************************
		Class :				CashierBillItemList

		Last update :		22 Mar 02

		Description:		CashierBillItem user list

*********************************************************/


class CashierBillItemList extends DataList {
	var $TABLE = "t_cashier_bill_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT cashier_bill_item_id) as rowCount FROM ".$this->TABLE
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * "
			."FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CashierBillItem($row);
			}
			return true;
		} else {
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		echo ("<option value=\"0\">$header</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_cashier_bill_item_id()."\"");
			if (($defaultId != null) && ($objItem->get_cashier_bill_item_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}

}
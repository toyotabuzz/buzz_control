<?
/*********************************************************
		Class :					ProductType

		Last update :	  10 Jan 02

		Description:	  Class manage t_product_type table

*********************************************************/
 
class StockProductType extends DB{

	var $TABLE="t_stock_product_type";

	var $mStockProductTypeId;
	function getStockProductTypeId() { return $this->mStockProductTypeId; }
	function setStockProductTypeId($data) { $this->mStockProductTypeId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	function StockProductType($objData=NULL) {
        If ($objData->stock_product_type_id !="") {
            $this->setStockProductTypeId($objData->stock_product_type_id);
			$this->setTitle($objData->title);
			$this->setCode($objData->code);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mStockProductTypeId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_product_type_id =".$this->mStockProductTypeId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockProductType($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockProductType($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, code ) "
						." VALUES ( '".$this->mTitle."', '".$this->mCode."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockProductTypeId = mysql_insert_id();
            return $this->mStockProductTypeId;
			$this->unsetConnection();
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." code = '".$this->mCode."'  "
						." WHERE  stock_product_type_id = ".$this->mStockProductTypeId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_product_type_id=".$this->mStockProductTypeId." ";
        $this->getConnection();
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == "") {$asrErrReturn["code"] = "��س��к�";
		}else{
			if(!$this->checkUnique($Mode,"code","stock_product_type_id","t_stock_product_type",$this->mStockProductTypeId,$this->mCode)){
				$asrErrReturn["code"] = '���ʫ�ӫ�͹';
			}
		}
		
        Return $asrErrReturn;
    }
	
   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class StockProductTypeList extends DataList {
	var $TABLE = "t_stock_product_type";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_product_type_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProductType($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockProductTypeId()."\"");
			if (($defaultId != null) && ($objItem->getStockProductTypeId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockProductTypeId()."\"");
			if (($defaultId != null) && ($objItem->getStockProductTypeId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
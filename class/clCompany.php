<?
/*********************************************************
		Class :				Company

		Last update :		20 Aug 05

		Description:		Class manage company table

*********************************************************/
 
class Company extends User{

	var $TABLE="t_company";

	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }	

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mBibTitle;
	function getBibTitle() { return htmlspecialchars($this->mBibTitle); }
	function setBibTitle($data) { $this->mBibTitle = $data; }

	var $mShortTitle;
	function getShortTitle() { return htmlspecialchars($this->mShortTitle); }
	function setShortTitle($data) { $this->mShortTitle = $data; }	
	
	var $mShortTitleEn;
	function getShortTitleEn() { return htmlspecialchars($this->mShortTitleEn); }
	function setShortTitleEn($data) { $this->mShortTitleEn = $data; }
	
	var $mAddress1;
	function getAddress1() { return htmlspecialchars($this->mAddress1); }
	function setAddress1($data) { $this->mAddress1 = $data; }

	var $mAddress2;
	function getAddress2() { return htmlspecialchars($this->mAddress2); }
	function setAddress2($data) { $this->mAddress2 = $data; }
	
	var $mProvince;
	function getProvince() { return htmlspecialchars($this->mProvince); }
	function setProvince($data) { $this->mProvince = $data; }

	var $mZip;
	function getZip() { return htmlspecialchars($this->mZip); }
	function setZip($data) { $this->mZip = $data; }

	var $mPhone;
	function getPhone() { return htmlspecialchars($this->mPhone); }
	function setPhone($data) { $this->mPhone = $data; }

	var $mFax;
	function getFax() { return htmlspecialchars($this->mFax); }
	function setFax($data) { $this->mFax = $data; }

	var $mTaxId;
	function getTaxId() { return htmlspecialchars($this->mTaxId); }
	function setTaxId($data) { $this->mTaxId = $data; }

	var $mEmail;
	function getEmail() { return htmlspecialchars($this->mEmail); }
	function setEmail($data) { $this->mEmail = $data; }

	var $mWebsite;
	function getWebsite() { return htmlspecialchars($this->mWebsite); }
	function setWebsite($data) { $this->mWebsite = $data; }
	
	var $mContactName;
	function getContactName() { return htmlspecialchars($this->mContactName); }
	function setContactName($data) { $this->mContactName = $data; }		
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }	

	var $mLogo;
	function getLogo() { return htmlspecialchars($this->mLogo); }
	function setLogo($data) { $this->mLogo = $data; }
	
	var $mViriyaCode;
	function getViriyaCode() { return htmlspecialchars($this->mViriyaCode); }
	function setViriyaCode($data) { $this->mViriyaCode = $data; }
	
	var $mCusCode;
	function getCusCode() { return htmlspecialchars($this->mCusCode); }
	function setCusCode($data) { $this->mCusCode = $data; }
	
	var $mInven;
	function getInven() { return htmlspecialchars($this->mInven); }
	function setInven($data) { $this->mInven = $data; }
	
	var $mLocation;
	function getLocation() { return htmlspecialchars($this->mLocation); }
	function setLocation($data) { $this->mLocation = $data; }
	
	var $mOrderNumberDefault;
	function getOrderNumberDefault() { return htmlspecialchars($this->mOrderNumberDefault); }
	function setOrderNumberDefault($data) { $this->mOrderNumberDefault = $data; }
	
	var $mOrderNumberRecent;
	function getOrderNumberRecent() { return htmlspecialchars($this->mOrderNumberRecent); }
	function setOrderNumberRecent($data) { $this->mOrderNumberRecent = $data; }	
	
	function getOrderNumber(){
		global $sCompanyId;	
		$orderNum = $this->mOrderNumberRecent+1;
		if($orderNum <10) $strBooking = "0000".$orderNum;
		if($orderNum <100 AND $orderNum >=10) $strBooking = "000".$orderNum;
		if($orderNum <1000  AND $orderNum >=100) $strBooking = "00".$orderNum;
		if($orderNum <10000  AND $orderNum >=1000) $strBooking = "0".$orderNum;
		if($orderNum >=10000  ) $strBooking = $orderNum;
		return $this->mOrderNumberDefault."A1".$sCompanyId.date("ymd")."/".$strBooking;
	}
	
	var $mOrderNumberNoheadDefault;
	function getOrderNumberNoheadDefault() { return htmlspecialchars($this->mOrderNumberNoheadDefault); }
	function setOrderNumberNoheadDefault($data) { $this->mOrderNumberNoheadDefault = $data; }
	
	var $mOrderNumberNoheadRecent;
	function getOrderNumberNoheadRecent() { return htmlspecialchars($this->mOrderNumberNoheadRecent); }
	function setOrderNumberNoheadRecent($data) { $this->mOrderNumberNoheadRecent = $data; }		
	
	var $mOrderNumber01;
	function getOrderNumber01() { return htmlspecialchars($this->mOrderNumber01); }
	function setOrderNumber01($data) { $this->mOrderNumber01 = $data; }
	
	var $mOrderNumber02;
	function getOrderNumber02() { return htmlspecialchars($this->mOrderNumber02); }
	function setOrderNumber02($data) { $this->mOrderNumber02 = $data; }		
	
	function getOrderNumberNohead(){
		global $sCompanyId;
		$sendingNum = $this->mOrderNumberNoheadRecent+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return $this->mOrderNumberNoheadDefault."B1".$sCompanyId.date("ymd")."/".$strSending;
	}	
	
	function genOrderNumber01(){
		global $sCompanyId;	
		$sendingNum = $this->mOrderNumber01+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return "C1".$sCompanyId.date("ymd")."/".$strSending;
	}	
	
	function genOrderNumber02(){
		global $sCompanyId;	
		$sendingNum = $this->mOrderNumber02+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return "D1".$sCompanyId.date("ymd")."/".$strSending;
	}			
	
	function Company($objData=NULL) {
        If ($objData->company_id !="") {
            $this->setCompanyId($objData->company_id);
			$this->setTitle($objData->title);
			$this->setBibTitle($objData->bib_title);
			$this->setShortTitle($objData->short_title);
			$this->setShortTitleEn($objData->short_title_en);
			$this->setAddress1($objData->address1);
			$this->setAddress2($objData->address2);
			$this->setProvince($objData->province);
			$this->setZip($objData->zip);
			$this->setPhone($objData->phone);
			$this->setFax($objData->fax);
			$this->setTaxId($objData->tax_id);
			$this->setRemark($objData->remark);
			$this->setContactName($objData->contact_name);
			$this->setEmail($objData->email);
			$this->setWebsite($objData->website);
			$this->setLogo($objData->logo);
			$this->setOrderNumberDefault($objData->order_number_default);
			$this->setOrderNumberRecent($objData->order_number_recent);
			$this->setOrderNumberNoheadDefault($objData->order_number_nohead_default);
			$this->setOrderNumberNoheadRecent($objData->order_number_nohead_recent);
			$this->setOrderNumber01($objData->order_number_01);
			$this->setOrderNumber02($objData->order_number_02);	
			$this->setViriyaCode($objData->viriya_code);	
			$this->setCusCode($objData->cus_code);	
			$this->setInven($objData->inven);	
			$this->setLocation($objData->location);	
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
		$this->setBibTitle(stripslashes($this->mBibTitle));
		$this->setAddress1(stripslashes($this->mAddress1));
		$this->setAddress2(stripslashes($this->mAddress2));
		$this->setProvince(stripslashes($this->mProvince));
		$this->setZip(stripslashes($this->mZip));
		$this->setPhone(stripslashes($this->mPhone));
		$this->setFax(stripslashes($this->mFax));
		$this->setTaxId(stripslashes($this->mTaxId));
		$this->setRemark(stripslashes($this->mRemark));
		$this->setContactName(stripslashes($this->mContactName));
		$this->setEmail(stripslashes($this->mEmail));
		$this->setWebsite(stripslashes($this->mWebsite));
	}
		
	function load() {

		if ($this->mCompanyId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE company_id =".$this->mCompanyId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				// echo print_r($row); exit();
				$this->Company($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mCompanyId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE company_id =".$this->mCompanyId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Company($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, short_title, short_title_en, address1, address2, province, zip, phone, fax, tax_id ,remark, contact_name, email , logo, website )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".$this->mShortTitle."' , "
						."  '".$this->mShortTitleEn."' , "
						."  '".$this->mAddress1."' , "
						."  '".$this->mAddress2."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mPhone."' , "
						."  '".$this->mFax."' , "
						."  '".$this->mTaxId."' ,"
						."  '".$this->mRemark."' ,"
						."  '".$this->mContactName."' , "
						."  '".$this->mEmail."' , "
						."  '".$this->mLogo."' , "
						."  '".$this->mWebsite."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mCompanyId = mysql_insert_id();
            return $this->mCompanyId;
        } else {
			return false;
	    }
	}
	
	
	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." short_title = '".$this->mShortTitle."' , "
						." short_title_en = '".$this->mShortTitleEn."' , "
						." address1 = '".$this->mAddress1."' , "
						." address2 = '".$this->mAddress2."' , "
						." province = '".$this->mProvince."' , "
						." zip = '".$this->mZip."' , "
						." tax_id = '".$this->mTaxId."' , "
						." phone = '".$this->mPhone."' , "
						." fax = '".$this->mFax."' , "
						." remark = '".$this->mRemark."' , "
						." contact_name = '".$this->mContactName."' , "
						." email = '".$this->mEmail."' , "
						." logo = '".$this->mLogo."' , "
						." website = '".$this->mWebsite."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOrderNumber(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_default = '".$this->mOrderNumberDefault."'  "
						." , order_number_recent = '".$this->mOrderNumberRecent."'  "
						." , order_number_nohead_default = '".$this->mOrderNumberNoheadDefault."'  "
						." , order_number_nohead_recent = '".$this->mOrderNumberNoheadRecent."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateInven(){
		$strSql = "UPDATE ".$this->TABLE
						." SET inven = '".$this->mInven."'  "
						." , location = '".$this->mLocation."'  "
						." , cus_code = '".$this->mCusCode."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	
	
	function updateOrderNumberRecent(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_recent = '".($this->mOrderNumberRecent+1)."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOrderNumberNoheadRecent(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_nohead_recent = '".($this->mOrderNumberNoheadRecent+1)."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOrderNumber01(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_01 = '".($this->mOrderNumber01+1)."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateOrderNumber02(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_02 = '".($this->mOrderNumber02+1)."'  "
						." WHERE  company_id = ".$this->mCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE company_id=".$this->mCompanyId." ";
        $this->getConnection();
        $this->query($strSql);
		$this->deletePicture($this->mLogo);
	}

	 Function check()
    {
		if ($this->mTitle == "") $asrErrReturn["title"] = '��س��кت��ͺ���ѷ';
		if ($this->mAddress1 == "") $asrErrReturn["address1"] = '��س��кط������';
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}	
	
}


/*********************************************************
		Class :				CompanyList

		Last update :		20 Nov 02

		Description:		Company list

*********************************************************/


class CompanyList extends DataList {
	var $TABLE = "t_company";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT company_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Company($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		echo ("<option value=\"0\">- ���͡�Ң� -</option>");		
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getShortTitle()."</option>");
		}
		echo("</select>");
	}	
	
	function printSelect1($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getShortTitle()."</option>");
		}
		echo("</select>");
	}		
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		if(substr($script,0,8) == "onchange"){
			echo ("<select class=\"field\" name=\"".$name."\" ".$script." >\n");
		}else{	
			echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		}
		echo ("<option value=\"0\">- $header -</option>");		
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getShortTitle()."</option>");
		}
		echo("</select>");
	}		
	
}
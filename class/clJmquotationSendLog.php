<?
/*********************************************************
		Class :					JmQuotationSendLog

		Last update :	  2021/03/31

		Description:	  Class manage jmquotation_send_log table

*********************************************************/
 
class JmQuotationSendLog extends DB {
    var $TABLE="jmquotation_send_log";

    var $mId;
	function getId() { return $this->mId; }
	function setId($data) { $this->mId = $data; }

    var $mInsureQuotationId;
	function getInsureQuotationId() { return $this->mInsureQuotationId; }
	function setInsureQuotationId($data) { $this->mInsureQuotationId = $data; }

    var $mQuotationSendType;
	function getQuotationSendType() { return $this->mQuotationSendType; }
	function setQuotationSendType($data) { $this->mQuotationSendType = $data; }

    var $mQuotationSendText;
	function getQuotationSendText() { return $this->mQuotationSendText; }
	function setQuotationSendText($data) { $this->mQuotationSendText = $data; }

    var $mSeqNo;
	function getSeqNo() { return $this->mSeqNo; }
	function setSeqNo($data) { $this->mSeqNo = $data; }

    function JmQuotationSendLog($objData=NULL) {
        $this->setId($objData->id);
        $this->setInsureQuotationId($objData->t_insure_quotation_id);
        $this->setQuotationSendType($objData->quotaion_send_type);
        $this->setQuotationSendText($objData->quotaion_send_text);
        $this->setSeqNo($objData->seq_no);
    }
    
    function init () {
		
	}

    function load () {
		if ($this->mId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE id =".$this->mId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->JmQuotationSendLog($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
}

class JmQuotationSendLogList extends DataList {
	var $TABLE = "jmquotation_send_log";

    var $mInsureQuotationId;
	function getInsureQuotationId() { return $this->mInsureQuotationId; }
	function setInsureQuotationId($data) { $this->mInsureQuotationId = $data; }
    
    function init () {
		
	}

	function load() {
		if ($this->mInsureQuotationId=='') {
			return false;
		}

        $strSql = "SELECT * FROM ".$this->TABLE." WHERE t_insure_quotation_id =".$this->mInsureQuotationId;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new JmQuotationSendLog($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class StockCarTbrImport extends DB{

	var $TABLE="t_stock_car_tbr_import";

	var $m_stock_car_tbr_import_id;
	function get_stock_car_tbr_import_id() { return $this->m_stock_car_tbr_import_id; }
	function set_stock_car_tbr_import_id($data) { $this->m_stock_car_tbr_import_id = $data; }
	
	var $m_po_number;
	function get_po_number() { return $this->m_po_number; }
	function set_po_number($data) { $this->m_po_number = $data; }
	
	var $m_import_date;
	function get_import_date() { return $this->m_import_date; }
	function set_import_date($data) { $this->m_import_date = $data; }
	
	var $m_invoice_date;
	function get_invoice_date() { return $this->m_invoice_date; }
	function set_invoice_date($data) { $this->m_invoice_date = $data; }
	
	var $m_reported_date;
	function get_reported_date() { return $this->m_reported_date; }
	function set_reported_date($data) { $this->m_reported_date = $data; }
	
	var $m_file_name;
	function get_file_name() { return $this->m_file_name; }
	function set_file_name($data) { $this->m_file_name = $data; }
	
	var $m_importer;
	function get_importer() { return $this->m_importer; }
	function set_importer($data) { $this->m_importer = $data; }

	function StockCarTbrImport($objData=NULL) {
        If ($objData->stock_car_tbr_import_id !="") {
			$this->set_stock_car_tbr_import_id($objData->stock_car_tbr_import_id);
			$this->set_po_number($objData->po_number);
			$this->set_import_date($objData->import_date);
			$this->set_invoice_date($objData->invoice_date);
			$this->set_reported_date($objData->reported_date);
			$this->set_file_name($objData->file_name);
			$this->set_importer($objData->importer);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_stock_car_tbr_import_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_car_tbr_import_id =".$this->m_stock_car_tbr_import_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarTbrImport($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarTbrImport($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( po_number , import_date , invoice_date , reported_date , file_name , importer ) " ." VALUES ( "
		." '".$this->m_po_number."' , "
		." '".$this->m_import_date."' , "
		." '".$this->m_invoice_date."' , "
		." '".$this->m_reported_date."' , "
		." '".$this->m_file_name."' , "
		." '".$this->m_importer."' "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_stock_car_tbr_import_id = mysql_insert_id();
            return $this->m_stock_car_tbr_import_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." po_number = '".$this->m_po_number."' "
		." , import_date = '".$this->m_import_date."' "
		." , invoice_date = '".$this->m_invoice_date."' "
		." , reported_date = '".$this->m_reported_date."' "
		." , file_name = '".$this->m_file_name."' "
		." , importer = '".$this->m_importer."' "
		." WHERE stock_car_tbr_import_id = ".$this->m_stock_car_tbr_import_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_car_tbr_import_id=".$this->m_stock_car_tbr_import_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class StockCarTbrImportList extends DataList {
	var $TABLE = "t_stock_car_tbr_import";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_car_tbr_import_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCarTbrImport($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
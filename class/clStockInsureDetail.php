<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_insure_detail table

*********************************************************/
 
class StockInsureDetail extends DB{

	var $TABLE="t_stock_insure_detail";

	var $m_stock_insure_detail_id;
	function get_stock_insure_detail_id() { return $this->m_stock_insure_detail_id; }
	function set_stock_insure_detail_id($data) { $this->m_stock_insure_detail_id = $data; }
	
	var $m_stock_insure_id;
	function get_stock_insure_id() { return $this->m_stock_insure_id; }
	function set_stock_insure_id($data) { $this->m_stock_insure_id = $data; }
	
	var $m_date;
	function get_date() { return $this->m_date; }
	function set_date($data) { $this->m_date = $data; }
	
	var $m_num;
	function get_num() { return $this->m_num; }
	function set_num($data) { $this->m_num = $data; }
	
	// 1.in   2.out
	var $m_type;
	function get_type() { return $this->m_type; }
	function set_type($data) { $this->m_type = $data; }
	
	// 1.acc  2.ins
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_user_by;
	function get_user_by() { return $this->m_user_by; }
	function set_user_by($data) { $this->m_user_by = $data; }
	
	var $m_user_request;
	function get_user_request() { return $this->m_user_request; }
	function set_user_request($data) { $this->m_user_request = $data; }
	
	var $m_insure_type;
	function get_insure_type() { return $this->m_insure_type; }
	function set_insure_type($data) { $this->m_insure_type = $data; }	
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }	
	
	// 1.acc  2.ins
	var $m_status_from;
	function get_status_from() { return $this->m_status_from; }
	function set_status_from($data) { $this->m_status_from = $data; }
	
	var $m_company_id_from;
	function get_company_id_from() { return $this->m_company_id_from; }
	function set_company_id_from($data) { $this->m_company_id_from = $data; }
	
	var $m_transfer_type;
	function get_transfer_type() { return $this->m_transfer_type; }
	function set_transfer_type($data) { $this->m_transfer_type = $data; }
	
	function StockInsureDetail($objData=NULL) {
        If ($objData->stock_insure_detail_id !="" or $objData->num !="" ) {
			$this->set_stock_insure_detail_id($objData->stock_insure_detail_id);
			$this->set_stock_insure_id($objData->stock_insure_id);
			$this->set_date($objData->date);
			$this->set_num($objData->num);
			$this->set_type($objData->type);
			$this->set_status($objData->status);
			$this->set_company_id($objData->company_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_remark($objData->remark);
			$this->set_user_by($objData->user_by);
			$this->set_user_request($objData->user_request);
			$this->set_insure_type($objData->insure_type);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_status_from($objData->status_from);
			$this->set_company_id_from($objData->company_id_from);
			$this->set_transfer_type($objData->transfer_type);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_stock_insure_detail_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_insure_detail_id =".$this->m_stock_insure_detail_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockInsureDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT O.*, S.broker_id, S.insure_company_id FROM ".$this->TABLE." O "
						." LEFT JOIN t_stock_insure S ON S.stock_insure_id =  O.stock_insure_id  "
						."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockInsureDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(stock_insure_detail_id) AS RSUM  FROM ".$this->TABLE."  "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}	

	function genNum($hNumType) {
		$strSql = "SELECT MAX(num)  acc_num_out FROM ".$this->TABLE."  WHERE num LIKE '".$hNumType.date("dmy")."/%' " ;

		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				
				$gen = substr($row->acc_num_out,strlen($row->acc_num_out)-4,4);
				$gen = $gen+1;
				if($gen < 10) $gen= "000".$gen;
				if($gen >= 10 and $gen < 100 ) $gen= "00".$gen;
				if($gen >= 100 and $gen < 1000 ) $gen= "0".$gen;
				
				
				$num_gen = $hNumType.date("dmy")."/".$gen;
                $result->freeResult();
				return $num_gen;
            }
        }else{
			return $hNumType.date("ymd")."/0001";
		}
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( stock_insure_id , date , num , type , status , company_id , insure_id , remark , user_by , user_request, insure_type, status_from, company_id_from, transfer_type ) " ." VALUES ( "
		." '".$this->m_stock_insure_id."' , "
		." '".date("Y-m-d H:i:s")."' , "
		." '".$this->m_num."' , "
		." '".$this->m_type."' , "
		." '".$this->m_status."' , "
		." '".$this->m_company_id."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_user_by."' , "
		." '".$this->m_user_request."' , "
		." '".$this->m_insure_type."' , "
		." '".$this->m_status_from."' , "
		." '".$this->m_company_id_from."' , "
		." '".$this->m_transfer_type."' "
		." ) ";


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_stock_insure_detail_id = mysql_insert_id();
            return $this->m_stock_insure_detail_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." stock_insure_detail_id = '".$this->m_stock_insure_detail_id."' "

		." , num = '".$this->m_num."' "
		." , type = '".$this->m_type."' "
		." , status = '".$this->m_status."' "
		." , company_id = '".$this->m_company_id."' "
		." , insure_id = '".$this->m_insure_id."' "
		." , remark = '".$this->m_remark."' "
		." , user_by = '".$this->m_user_by."' "
		." , user_request = '".$this->m_user_request."' "
		." WHERE stock_insure_detail_id = ".$this->m_stock_insure_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_insure_detail_id=".$this->m_stock_insure_detail_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class StockInsureDetailList extends DataList {
	var $TABLE = "t_stock_insure_detail";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_insure_detail_id) as rowCount FROM ".$this->TABLE
			." O  "
			." LEFT JOIN t_stock_insure S ON S.stock_insure_id =  O.stock_insure_id  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT O.* FROM ".$this->TABLE." O "
			." LEFT JOIN t_stock_insure S ON S.stock_insure_id =  O.stock_insure_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockInsureDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSum() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT num) as rowCount FROM ".$this->TABLE
			." O  LEFT JOIN t_stock_insure SI ON SI.stock_insure_id = O.stock_insure_id  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT O.num, O.date, O.company_id , O.remark, SI.broker_id, SI.insure_company_id , O.transfer_type FROM ".$this->TABLE." O "
			." LEFT JOIN t_stock_insure SI ON SI.stock_insure_id = O.stock_insure_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockInsureDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function loadNum() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT num) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT( num ) as num FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockInsureDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_stock_insure_detail_id()."\"");
			if (($defaultId != null) && ($objItem->get_stock_insure_detail_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}
}
<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_form table

*********************************************************/
 
class InsureForm extends DB{

var $TABLE="t_insure_form";

var $m_insure_form_id;
function get_insure_form_id() { return $this->m_insure_form_id; }
function set_insure_form_id($data) { $this->m_insure_form_id = $data; }

var $m_form_date;
function get_form_date() { return $this->m_form_date; }
function set_form_date($data) { $this->m_form_date = $data; }

var $m_form_time;
function get_form_time() { return $this->m_form_time; }
function set_form_time($data) { $this->m_form_time = $data; }

var $m_insure_id;
function get_insure_id() { return $this->m_insure_id; }
function set_insure_id($data) { $this->m_insure_id = $data; }

var $m_sale_id;
function get_sale_id() { return $this->m_sale_id; }
function set_sale_id($data) { $this->m_sale_id = $data; }

var $m_team_id;
function get_team_id() { return $this->m_team_id; }
function set_team_id($data) { $this->m_team_id = $data; }

var $m_company_id;
function get_company_id() { return $this->m_company_id; }
function set_company_id($data) { $this->m_company_id = $data; }

var $m_frm_type;
function get_frm_type() { return $this->m_frm_type; }
function set_frm_type($data) { $this->m_frm_type = $data; }

var $m_code;
function get_code() { return $this->m_code; }
function set_code($data) { $this->m_code = $data; }

var $m_title;
function get_title() { return stripslashes($this->m_title); }
function set_title($data) { $this->m_title = $data; }

var $m_remark;
function get_remark() { return stripslashes($this->m_remark); }
function set_remark($data) { $this->m_remark = $data; }

var $m_t01;
function get_t01() { return stripslashes($this->m_t01); }
function set_t01($data) { $this->m_t01 = $data; }

var $m_t02;
function get_t02() { return stripslashes($this->m_t02); }
function set_t02($data) { $this->m_t02 = $data; }

var $m_t03;
function get_t03() { return stripslashes($this->m_t03); }
function set_t03($data) { $this->m_t03 = $data; }

var $m_t04;
function get_t04() { return stripslashes($this->m_t04); }
function set_t04($data) { $this->m_t04 = $data; }

var $m_t05;
function get_t05() { return stripslashes($this->m_t05); }
function set_t05($data) { $this->m_t05 = $data; }

var $m_t06;
function get_t06() { return stripslashes($this->m_t06); }
function set_t06($data) { $this->m_t06 = $data; }

var $m_t07;
function get_t07() { return stripslashes($this->m_t07); }
function set_t07($data) { $this->m_t07 = $data; }

var $m_t08;
function get_t08() { return stripslashes($this->m_t08); }
function set_t08($data) { $this->m_t08 = $data; }

var $m_t09;
function get_t09() { return stripslashes($this->m_t09); }
function set_t09($data) { $this->m_t09 = $data; }

var $m_t10;
function get_t10() { return stripslashes($this->m_t10); }
function set_t10($data) { $this->m_t10 = $data; }

var $m_t11;
function get_t11() { return stripslashes($this->m_t11); }
function set_t11($data) { $this->m_t11 = $data; }

var $m_t12;
function get_t12() { return stripslashes($this->m_t12); }
function set_t12($data) { $this->m_t12 = $data; }

var $m_t13;
function get_t13() { return stripslashes($this->m_t13); }
function set_t13($data) { $this->m_t13 = $data; }

var $m_t14;
function get_t14() { return stripslashes($this->m_t14); }
function set_t14($data) { $this->m_t14 = $data; }

var $m_t15;
function get_t15() { return stripslashes($this->m_t15); }
function set_t15($data) { $this->m_t15 = $data; }

var $m_t16;
function get_t16() { return stripslashes($this->m_t16); }
function set_t16($data) { $this->m_t16 = $data; }

var $m_t17;
function get_t17() { return stripslashes($this->m_t17); }
function set_t17($data) { $this->m_t17 = $data; }

var $m_t18;
function get_t18() { return stripslashes($this->m_t18); }
function set_t18($data) { $this->m_t18 = $data; }

var $m_t19;
function get_t19() { return stripslashes($this->m_t19); }
function set_t19($data) { $this->m_t19 = $data; }

var $m_t20;
function get_t20() { return stripslashes($this->m_t20); }
function set_t20($data) { $this->m_t20 = $data; }

var $m_t21;
function get_t21() { return stripslashes($this->m_t21); }
function set_t21($data) { $this->m_t21 = $data; }

var $m_t22;
function get_t22() { return stripslashes($this->m_t22); }
function set_t22($data) { $this->m_t22 = $data; }

var $m_t23;
function get_t23() { return stripslashes($this->m_t23); }
function set_t23($data) { $this->m_t23 = $data; }

var $m_t24;
function get_t24() { return stripslashes($this->m_t24); }
function set_t24($data) { $this->m_t24 = $data; }

var $m_t25;
function get_t25() { return stripslashes($this->m_t25); }
function set_t25($data) { $this->m_t25 = $data; }

var $m_t26;
function get_t26() { return stripslashes($this->m_t26); }
function set_t26($data) { $this->m_t26 = $data; }

var $m_t27;
function get_t27() { return stripslashes($this->m_t27); }
function set_t27($data) { $this->m_t27 = $data; }

var $m_t28;
function get_t28() { return stripslashes($this->m_t28); }
function set_t28($data) { $this->m_t28 = $data; }

var $m_t29;
function get_t29() { return stripslashes($this->m_t29); }
function set_t29($data) { $this->m_t29 = $data; }

var $m_t30;
function get_t30() { return stripslashes($this->m_t30); }
function set_t30($data) { $this->m_t30 = $data; }

var $m_t31;
function get_t31() { return stripslashes($this->m_t31); }
function set_t31($data) { $this->m_t31 = $data; }

var $m_t32;
function get_t32() { return stripslashes($this->m_t32); }
function set_t32($data) { $this->m_t32 = $data; }

var $m_t33;
function get_t33() { return stripslashes($this->m_t33); }
function set_t33($data) { $this->m_t33 = $data; }

var $m_t34;
function get_t34() { return stripslashes($this->m_t34); }
function set_t34($data) { $this->m_t34 = $data; }

var $m_t35;
function get_t35() { return stripslashes($this->m_t35); }
function set_t35($data) { $this->m_t35 = $data; }

var $m_t36;
function get_t36() { return stripslashes($this->m_t36); }
function set_t36($data) { $this->m_t36 = $data; }

var $m_t37;
function get_t37() { return stripslashes($this->m_t37); }
function set_t37($data) { $this->m_t37 = $data; }

var $m_t38;
function get_t38() { return stripslashes($this->m_t38); }
function set_t38($data) { $this->m_t38 = $data; }

var $m_t39;
function get_t39() { return stripslashes($this->m_t39); }
function set_t39($data) { $this->m_t39 = $data; }

var $m_t40;
function get_t40() { return stripslashes($this->m_t40); }
function set_t40($data) { $this->m_t40 = $data; }

var $m_t41;
function get_t41() { return stripslashes($this->m_t41); }
function set_t41($data) { $this->m_t41 = $data; }

var $m_t42;
function get_t42() { return stripslashes($this->m_t42); }
function set_t42($data) { $this->m_t42 = $data; }

var $m_t43;
function get_t43() { return stripslashes($this->m_t43); }
function set_t43($data) { $this->m_t43 = $data; }

var $m_t44;
function get_t44() { return stripslashes($this->m_t44); }
function set_t44($data) { $this->m_t44 = $data; }

var $m_t45;
function get_t45() { return stripslashes($this->m_t45); }
function set_t45($data) { $this->m_t45 = $data; }

var $m_t46;
function get_t46() { return stripslashes($this->m_t46); }
function set_t46($data) { $this->m_t46 = $data; }

var $m_t47;
function get_t47() { return stripslashes($this->m_t47); }
function set_t47($data) { $this->m_t47 = $data; }

var $m_t48;
function get_t48() { return stripslashes($this->m_t48); }
function set_t48($data) { $this->m_t48 = $data; }

var $m_t49;
function get_t49() { return stripslashes($this->m_t49); }
function set_t49($data) { $this->m_t49 = $data; }

var $m_t50;
function get_t50() { return stripslashes($this->m_t50); }
function set_t50($data) { $this->m_t50 = $data; }

var $m_t51;
function get_t51() { return stripslashes($this->m_t51); }
function set_t51($data) { $this->m_t51 = $data; }

var $m_t52;
function get_t52() { return stripslashes($this->m_t52); }
function set_t52($data) { $this->m_t52 = $data; }

var $m_t53;
function get_t53() { return stripslashes($this->m_t53); }
function set_t53($data) { $this->m_t53 = $data; }

var $m_t54;
function get_t54() { return stripslashes($this->m_t54); }
function set_t54($data) { $this->m_t54 = $data; }

var $m_t55;
function get_t55() { return stripslashes($this->m_t55); }
function set_t55($data) { $this->m_t55 = $data; }

var $m_t56;
function get_t56() { return stripslashes($this->m_t56); }
function set_t56($data) { $this->m_t56 = $data; }

var $m_t57;
function get_t57() { return stripslashes($this->m_t57); }
function set_t57($data) { $this->m_t57 = $data; }

var $m_t58;
function get_t58() { return stripslashes($this->m_t58); }
function set_t58($data) { $this->m_t58 = $data; }

var $m_t59;
function get_t59() { return stripslashes($this->m_t59); }
function set_t59($data) { $this->m_t59 = $data; }

var $m_t60;
function get_t60() { return stripslashes($this->m_t60); }
function set_t60($data) { $this->m_t60 = $data; }

var $m_t61;
function get_t61() { return stripslashes($this->m_t61); }
function set_t61($data) { $this->m_t61 = $data; }

var $m_t62;
function get_t62() { return stripslashes($this->m_t62); }
function set_t62($data) { $this->m_t62 = $data; }

var $m_t63;
function get_t63() { return stripslashes($this->m_t63); }
function set_t63($data) { $this->m_t63 = $data; }

var $m_t64;
function get_t64() { return stripslashes($this->m_t64); }
function set_t64($data) { $this->m_t64 = $data; }

var $m_t65;
function get_t65() { return stripslashes($this->m_t65); }
function set_t65($data) { $this->m_t65 = $data; }

var $m_t66;
function get_t66() { return stripslashes($this->m_t66); }
function set_t66($data) { $this->m_t66 = $data; }

var $m_t67;
function get_t67() { return stripslashes($this->m_t67); }
function set_t67($data) { $this->m_t67 = $data; }

var $m_t68;
function get_t68() { return stripslashes($this->m_t68); }
function set_t68($data) { $this->m_t68 = $data; }

var $m_t69;
function get_t69() { return stripslashes($this->m_t69); }
function set_t69($data) { $this->m_t69 = $data; }

var $m_t70;
function get_t70() { return stripslashes($this->m_t70); }
function set_t70($data) { $this->m_t70 = $data; }

var $m_t71;
function get_t71() { return stripslashes($this->m_t71); }
function set_t71($data) { $this->m_t71 = $data; }

var $m_t72;
function get_t72() { return stripslashes($this->m_t72); }
function set_t72($data) { $this->m_t72 = $data; }

var $m_t73;
function get_t73() { return stripslashes($this->m_t73); }
function set_t73($data) { $this->m_t73 = $data; }

var $m_t74;
function get_t74() { return stripslashes($this->m_t74); }
function set_t74($data) { $this->m_t74 = $data; }

var $m_t75;
function get_t75() { return stripslashes($this->m_t75); }
function set_t75($data) { $this->m_t75 = $data; }

var $m_t76;
function get_t76() { return stripslashes($this->m_t76); }
function set_t76($data) { $this->m_t76 = $data; }

var $m_t77;
function get_t77() { return stripslashes($this->m_t77); }
function set_t77($data) { $this->m_t77 = $data; }

var $m_t78;
function get_t78() { return stripslashes($this->m_t78); }
function set_t78($data) { $this->m_t78 = $data; }

var $m_t79;
function get_t79() { return stripslashes($this->m_t79); }
function set_t79($data) { $this->m_t79 = $data; }

var $m_t80;
function get_t80() { return stripslashes($this->m_t80); }
function set_t80($data) { $this->m_t80 = $data; }

var $m_t81;
function get_t81() { return stripslashes($this->m_t81); }
function set_t81($data) { $this->m_t81 = $data; }

var $m_t82;
function get_t82() { return stripslashes($this->m_t82); }
function set_t82($data) { $this->m_t82 = $data; }

var $m_t83;
function get_t83() { return stripslashes($this->m_t83); }
function set_t83($data) { $this->m_t83 = $data; }

var $m_t84;
function get_t84() { return stripslashes($this->m_t84); }
function set_t84($data) { $this->m_t84 = $data; }

var $m_t85;
function get_t85() { return stripslashes($this->m_t85); }
function set_t85($data) { $this->m_t85 = $data; }

var $m_t86;
function get_t86() { return stripslashes($this->m_t86); }
function set_t86($data) { $this->m_t86 = $data; }

var $m_t87;
function get_t87() { return stripslashes($this->m_t87); }
function set_t87($data) { $this->m_t87 = $data; }

var $m_t88;
function get_t88() { return stripslashes($this->m_t88); }
function set_t88($data) { $this->m_t88 = $data; }

var $m_t89;
function get_t89() { return stripslashes($this->m_t89); }
function set_t89($data) { $this->m_t89 = $data; }

var $m_t90;
function get_t90() { return stripslashes($this->m_t90); }
function set_t90($data) { $this->m_t90 = $data; }

var $m_t91;
function get_t91() { return stripslashes($this->m_t91); }
function set_t91($data) { $this->m_t91 = $data; }

var $m_t92;
function get_t92() { return stripslashes($this->m_t92); }
function set_t92($data) { $this->m_t92 = $data; }

var $m_t93;
function get_t93() { return stripslashes($this->m_t93); }
function set_t93($data) { $this->m_t93 = $data; }

var $m_t94;
function get_t94() { return stripslashes($this->m_t94); }
function set_t94($data) { $this->m_t94 = $data; }

var $m_t95;
function get_t95() { return stripslashes($this->m_t95); }
function set_t95($data) { $this->m_t95 = $data; }

var $m_t96;
function get_t96() { return stripslashes($this->m_t96); }
function set_t96($data) { $this->m_t96 = $data; }

var $m_t97;
function get_t97() { return stripslashes($this->m_t97); }
function set_t97($data) { $this->m_t97 = $data; }

var $m_t98;
function get_t98() { return stripslashes($this->m_t98); }
function set_t98($data) { $this->m_t98 = $data; }

var $m_t99;
function get_t99() { return stripslashes($this->m_t99); }
function set_t99($data) { $this->m_t99 = $data; }

var $m_t00;
function get_t00() { return stripslashes($this->m_t00); }
function set_t00($data) { $this->m_t00 = $data; }

var $m_sup_approve;
function get_sup_approve() { return $this->m_sup_approve; }
function set_sup_approve($data) { $this->m_sup_approve = $data; }

var $m_sup_date;
function get_sup_date() { return $this->m_sup_date; }
function set_sup_date($data) { $this->m_sup_date = $data; }

var $m_sup_time;
function get_sup_time() { return $this->m_sup_time; }
function set_sup_time($data) { $this->m_sup_time = $data; }

var $m_sup_id;
function get_sup_id() { return $this->m_sup_id; }
function set_sup_id($data) { $this->m_sup_id = $data; }

var $m_sup_remark;
function get_sup_remark() { return $this->m_sup_remark; }
function set_sup_remark($data) { $this->m_sup_remark = $data; }

var $m_admin_approve;
function get_admin_approve() { return $this->m_admin_approve; }
function set_admin_approve($data) { $this->m_admin_approve = $data; }

var $m_admin_date;
function get_admin_date() { return $this->m_admin_date; }
function set_admin_date($data) { $this->m_admin_date = $data; }

var $m_admin_time;
function get_admin_time() { return $this->m_admin_time; }
function set_admin_time($data) { $this->m_admin_time = $data; }

var $m_admin_id;
function get_admin_id() { return $this->m_admin_id; }
function set_admin_id($data) { $this->m_admin_id = $data; }

var $m_admin_remark;
function get_admin_remark() { return $this->m_admin_remark; }
function set_admin_remark($data) { $this->m_admin_remark = $data; }

var $m_admin_code;
function get_admin_code() { return $this->m_admin_code; }
function set_admin_code($data) { $this->m_admin_code = $data; }

var $m_admin_code_date;
function get_admin_code_date() { return $this->m_admin_code_date; }
function set_admin_code_date($data) { $this->m_admin_code_date = $data; }
	
	function InsureForm($objData=NULL) {
	
		
        If ($objData->insure_form_id !="") {
				$this->set_insure_form_id($objData->insure_form_id);
				$this->set_form_date($objData->form_date);
				$this->set_form_time($objData->form_time);
				$this->set_insure_id($objData->insure_id);
				$this->set_company_id($objData->company_id);
				$this->set_frm_type($objData->frm_type);
				$this->set_code($objData->code);
				$this->set_title($objData->title);
				$this->set_remark($objData->remark);

				$this->set_t01($objData->t01);
				$this->set_t02($objData->t02);
				$this->set_t03($objData->t03);
				$this->set_t04($objData->t04);
				$this->set_t05($objData->t05);
				$this->set_t06($objData->t06);
				$this->set_t07($objData->t07);
				$this->set_t08($objData->t08);
				$this->set_t09($objData->t09);
				$this->set_t10($objData->t10);
				$this->set_t11($objData->t11);
				$this->set_t12($objData->t12);
				$this->set_t13($objData->t13);
				$this->set_t14($objData->t14);
				$this->set_t15($objData->t15);
				$this->set_t16($objData->t16);
				$this->set_t17($objData->t17);
				$this->set_t18($objData->t18);
				$this->set_t19($objData->t19);
				$this->set_t20($objData->t20);
				$this->set_t21($objData->t21);
				$this->set_t22($objData->t22);
				$this->set_t23($objData->t23);
				$this->set_t24($objData->t24);
				$this->set_t25($objData->t25);
				$this->set_t26($objData->t26);
				$this->set_t27($objData->t27);
				$this->set_t28($objData->t28);
				$this->set_t29($objData->t29);
				$this->set_t30($objData->t30);
				$this->set_t31($objData->t31);
				$this->set_t32($objData->t32);
				$this->set_t33($objData->t33);
				$this->set_t34($objData->t34);
				$this->set_t35($objData->t35);
				$this->set_t36($objData->t36);
				$this->set_t37($objData->t37);
				$this->set_t38($objData->t38);
				$this->set_t39($objData->t39);
				$this->set_t40($objData->t40);
				$this->set_t41($objData->t41);
				$this->set_t42($objData->t42);
				$this->set_t43($objData->t43);
				$this->set_t44($objData->t44);
				$this->set_t45($objData->t45);
				$this->set_t46($objData->t46);
				$this->set_t47($objData->t47);
				$this->set_t48($objData->t48);
				$this->set_t49($objData->t49);
				$this->set_t50($objData->t50);
				$this->set_t51($objData->t51);
				$this->set_t52($objData->t52);
				$this->set_t53($objData->t53);
				$this->set_t54($objData->t54);
				$this->set_t55($objData->t55);
				$this->set_t56($objData->t56);
				$this->set_t57($objData->t57);
				$this->set_t58($objData->t58);
				$this->set_t59($objData->t59);
				$this->set_t60($objData->t60);
				$this->set_t61($objData->t61);
				$this->set_t62($objData->t62);
				$this->set_t63($objData->t63);
				$this->set_t64($objData->t64);
				$this->set_t65($objData->t65);
				$this->set_t66($objData->t66);
				$this->set_t67($objData->t67);
				$this->set_t68($objData->t68);
				$this->set_t69($objData->t69);
				$this->set_t70($objData->t70);
				$this->set_t71($objData->t71);
				$this->set_t72($objData->t72);
				$this->set_t73($objData->t73);
				$this->set_t74($objData->t74);
				$this->set_t75($objData->t75);
				$this->set_t76($objData->t76);
				$this->set_t77($objData->t77);
				$this->set_t78($objData->t78);
				$this->set_t79($objData->t79);
				$this->set_t80($objData->t80);
				$this->set_t81($objData->t81);
				$this->set_t82($objData->t82);
				$this->set_t83($objData->t83);
				$this->set_t84($objData->t84);
				$this->set_t85($objData->t85);
				$this->set_t86($objData->t86);
				$this->set_t87($objData->t87);
				$this->set_t88($objData->t88);
				$this->set_t89($objData->t89);
				$this->set_t90($objData->t90);
				$this->set_t91($objData->t91);
				$this->set_t92($objData->t92);
				$this->set_t93($objData->t93);
				$this->set_t94($objData->t94);
				$this->set_t95($objData->t95);
				$this->set_t96($objData->t96);
				$this->set_t97($objData->t97);
				$this->set_t98($objData->t98);
				$this->set_t99($objData->t99);
				$this->set_t00($objData->t00);
				$this->set_sup_approve($objData->sup_approve);
				$this->set_sup_date($objData->sup_date);
				$this->set_sup_time($objData->sup_time);
				$this->set_sup_id($objData->sup_id);
				$this->set_sup_remark($objData->sup_remark);
				$this->set_admin_approve($objData->admin_approve);
				$this->set_admin_date($objData->admin_date);
				$this->set_admin_time($objData->admin_time);
				$this->set_admin_id($objData->admin_id);
				$this->set_admin_remark($objData->admin_remark);
				$this->set_admin_code($objData->admin_code);
				$this->set_admin_code_date($objData->admin_code_date);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_insure_form_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_form_id =".$this->m_insure_form_id;
		
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureForm($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->m_insure_form_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_form_id =".$this->m_insure_form_id;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureForm($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureForm($row);
      		    $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT Count(DISTINCT I.insure_form_id) as rowCount FROM ".$this->TABLE." I  "
		." LEFT JOIN t_insure  P ON I.insure_id =  P.insure_id "
		." LEFT JOIN t_insure_car IC ON P.car_id =  IC.car_id  WHERE ".$strCondition;
		
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->rowCount;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	

	function add() {
		$objMM = new Member();
		$objMM->setMemberId($_SESSION['sMemberId']);
		$objMM->load();

		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id, sale_id, team_id, manager_id, company_id , form_date, form_time, frm_type , code , title , remark , t01 , t02 , t03 , t04 , t05 , t06 , t07 , t08 , t09 , t10 , t11 , t12 , t13 , t14 , t15 , t16 , t17 , t18 , t19 , t20 , t21 , t22 , t23 , t24 , t25 , t26 , t27 , t28 , t29 , t30 , t31 , t32 , t33 , t34 , t35 , t36 , t37 , t38 , t39 , t40 , t41 , t42 , t43 , t44 , t45 , t46 , t47 , t48 , t49 , t50 , t51 , t52 , t53 , t54 , t55 , t56 , t57 , t58 , t59 , t60 , t61 , t62 , t63 , t64 , t65 , t66 , t67 , t68 , t69 , t70 , t71 , t72 , t73 , t74 , t75 , t76 , t77 , t78 , t79 , t80 , t81 , t82 , t83 , t84 , t85 , t86 , t87 , t88 , t89 , t90 , t91 , t92 , t93 , t94 , t95 , t96 , t97 , t98 , t99 , t00, created_at, created_by) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$_SESSION['sMemberId']."' , "
		." '".$objMM->getTeam()."' , "
		." 0 , "
		." '".$objMM->getCompanyId()."' , "
		." '".date("Y-m-d")."' , "
		." '".date("H:i:s")."' , "
		." '".$this->m_frm_type."' , "
		." '".$this->m_code."' , "
		." '".addslashes($this->m_title)."' , "
		." '".addslashes($this->m_remark)."' , "
		." '".addslashes($this->m_t01)."' , "
		." '".addslashes($this->m_t02)."' , "
		." '".addslashes($this->m_t03)."' , "
		." '".addslashes($this->m_t04)."' , "
		." '".addslashes($this->m_t05)."' , "
		." '".addslashes($this->m_t06)."' , "
		." '".addslashes($this->m_t07)."' , "
		." '".addslashes($this->m_t08)."' , "
		." '".addslashes($this->m_t09)."' , "
		." '".addslashes($this->m_t10)."' , "
		." '".addslashes($this->m_t11)."' , "
		." '".addslashes($this->m_t12)."' , "
		." '".addslashes($this->m_t13)."' , "
		." '".addslashes($this->m_t14)."' , "
		." '".addslashes($this->m_t15)."' , "
		." '".addslashes($this->m_t16)."' , "
		." '".addslashes($this->m_t17)."' , "
		." '".addslashes($this->m_t18)."' , "
		." '".addslashes($this->m_t19)."' , "
		." '".addslashes($this->m_t20)."' , "
		." '".addslashes($this->m_t21)."' , "
		." '".addslashes($this->m_t22)."' , "
		." '".addslashes($this->m_t23)."' , "
		." '".addslashes($this->m_t24)."' , "
		." '".addslashes($this->m_t25)."' , "
		." '".addslashes($this->m_t26)."' , "
		." '".addslashes($this->m_t27)."' , "
		." '".addslashes($this->m_t28)."' , "
		." '".addslashes($this->m_t29)."' , "
		." '".addslashes($this->m_t30)."' , "
		." '".addslashes($this->m_t31)."' , "
		." '".addslashes($this->m_t32)."' , "
		." '".addslashes($this->m_t33)."' , "
		." '".addslashes($this->m_t34)."' , "
		." '".addslashes($this->m_t35)."' , "
		." '".addslashes($this->m_t36)."' , "
		." '".addslashes($this->m_t37)."' , "
		." '".addslashes($this->m_t38)."' , "
		." '".addslashes($this->m_t39)."' , "
		." '".addslashes($this->m_t40)."' , "
		." '".addslashes($this->m_t41)."' , "
		." '".addslashes($this->m_t42)."' , "
		." '".addslashes($this->m_t43)."' , "
		." '".addslashes($this->m_t44)."' , "
		." '".addslashes($this->m_t45)."' , "
		." '".addslashes($this->m_t46)."' , "
		." '".addslashes($this->m_t47)."' , "
		." '".addslashes($this->m_t48)."' , "
		." '".addslashes($this->m_t49)."' , "
		." '".addslashes($this->m_t50)."' , "
		." '".addslashes($this->m_t51)."' , "
		." '".addslashes($this->m_t52)."' , "
		." '".addslashes($this->m_t53)."' , "
		." '".addslashes($this->m_t54)."' , "
		." '".addslashes($this->m_t55)."' , "
		." '".addslashes($this->m_t56)."' , "
		." '".addslashes($this->m_t57)."' , "
		." '".addslashes($this->m_t58)."' , "
		." '".addslashes($this->m_t59)."' , "
		." '".addslashes($this->m_t60)."' , "
		." '".addslashes($this->m_t61)."' , "
		." '".addslashes($this->m_t62)."' , "
		." '".addslashes($this->m_t63)."' , "
		." '".addslashes($this->m_t64)."' , "
		." '".addslashes($this->m_t65)."' , "
		." '".addslashes($this->m_t66)."' , "
		." '".addslashes($this->m_t67)."' , "
		." '".addslashes($this->m_t68)."' , "
		." '".addslashes($this->m_t69)."' , "
		." '".addslashes($this->m_t70)."' , "
		." '".addslashes($this->m_t71)."' , "
		." '".addslashes($this->m_t72)."' , "
		." '".addslashes($this->m_t73)."' , "
		." '".addslashes($this->m_t74)."' , "
		." '".addslashes($this->m_t75)."' , "
		." '".addslashes($this->m_t76)."' , "
		." '".addslashes($this->m_t77)."' , "
		." '".addslashes($this->m_t78)."' , "
		." '".addslashes($this->m_t79)."' , "
		." '".addslashes($this->m_t80)."' , "
		." '".addslashes($this->m_t81)."' , "
		." '".addslashes($this->m_t82)."' , "
		." '".addslashes($this->m_t83)."' , "
		." '".addslashes($this->m_t84)."' , "
		." '".addslashes($this->m_t85)."' , "
		." '".addslashes($this->m_t86)."' , "
		." '".addslashes($this->m_t87)."' , "
		." '".addslashes($this->m_t88)."' , "
		." '".addslashes($this->m_t89)."' , "
		." '".addslashes($this->m_t90)."' , "
		." '".addslashes($this->m_t91)."' , "
		." '".addslashes($this->m_t92)."' , "
		." '".addslashes($this->m_t93)."' , "
		." '".addslashes($this->m_t94)."' , "
		." '".addslashes($this->m_t95)."' , "
		." '".addslashes($this->m_t96)."' , "
		." '".addslashes($this->m_t97)."' , "
		." '".addslashes($this->m_t98)."' , "
		." '".addslashes($this->m_t99)."' , "
		." '".addslashes($this->m_t00)."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_form_id = mysql_insert_id();
            return $this->m_insure_form_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , sale_id = '".$this->m_sale_id."' "
		." , team_id = '".$this->m_team_id."' "
		." , company_id = '".$this->m_company_id."' "
		." , frm_type = '".$this->m_frm_type."' "
		." , code = '".$this->m_code."' "
		." , title = '".addslashes($this->m_title)."' "
		." , remark = '".addslashes($this->m_remark)."' "
		." , t01 = '".addslashes($this->m_t01)."' "
		." , t02 = '".addslashes($this->m_t02)."' "
		." , t03 = '".addslashes($this->m_t03)."' "
		." , t04 = '".addslashes($this->m_t04)."' "
		." , t05 = '".addslashes($this->m_t05)."' "
		." , t06 = '".addslashes($this->m_t06)."' "
		." , t07 = '".addslashes($this->m_t07)."' "
		." , t08 = '".addslashes($this->m_t08)."' "
		." , t09 = '".addslashes($this->m_t09)."' "
		." , t10 = '".addslashes($this->m_t10)."' "
		." , t11 = '".addslashes($this->m_t11)."' "
		." , t12 = '".addslashes($this->m_t12)."' "
		." , t13 = '".addslashes($this->m_t13)."' "
		." , t14 = '".addslashes($this->m_t14)."' "
		." , t15 = '".addslashes($this->m_t15)."' "
		." , t16 = '".addslashes($this->m_t16)."' "
		." , t17 = '".addslashes($this->m_t17)."' "
		." , t18 = '".addslashes($this->m_t18)."' "
		." , t19 = '".addslashes($this->m_t19)."' "
		." , t20 = '".addslashes($this->m_t20)."' "
		." , t21 = '".addslashes($this->m_t21)."' "
		." , t22 = '".addslashes($this->m_t22)."' "
		." , t23 = '".addslashes($this->m_t23)."' "
		." , t24 = '".addslashes($this->m_t24)."' "
		." , t25 = '".addslashes($this->m_t25)."' "
		." , t26 = '".addslashes($this->m_t26)."' "
		." , t27 = '".addslashes($this->m_t27)."' "
		." , t28 = '".addslashes($this->m_t28)."' "
		." , t29 = '".addslashes($this->m_t29)."' "
		." , t30 = '".addslashes($this->m_t30)."' "
		." , t31 = '".addslashes($this->m_t31)."' "
		." , t32 = '".addslashes($this->m_t32)."' "
		." , t33 = '".addslashes($this->m_t33)."' "
		." , t34 = '".addslashes($this->m_t34)."' "
		." , t35 = '".addslashes($this->m_t35)."' "
		." , t36 = '".addslashes($this->m_t36)."' "
		." , t37 = '".addslashes($this->m_t37)."' "
		." , t38 = '".addslashes($this->m_t38)."' "
		." , t39 = '".addslashes($this->m_t39)."' "
		." , t40 = '".addslashes($this->m_t40)."' "
		." , t41 = '".addslashes($this->m_t41)."' "
		." , t42 = '".addslashes($this->m_t42)."' "
		." , t43 = '".addslashes($this->m_t43)."' "
		." , t44 = '".addslashes($this->m_t44)."' "
		." , t45 = '".addslashes($this->m_t45)."' "
		." , t46 = '".addslashes($this->m_t46)."' "
		." , t47 = '".addslashes($this->m_t47)."' "
		." , t48 = '".addslashes($this->m_t48)."' "
		." , t49 = '".addslashes($this->m_t49)."' "
		." , t50 = '".addslashes($this->m_t50)."' "
		." , t51 = '".addslashes($this->m_t51)."' "
		." , t52 = '".addslashes($this->m_t52)."' "
		." , t53 = '".addslashes($this->m_t53)."' "
		." , t54 = '".addslashes($this->m_t54)."' "
		." , t55 = '".addslashes($this->m_t55)."' "
		." , t56 = '".addslashes($this->m_t56)."' "
		." , t57 = '".addslashes($this->m_t57)."' "
		." , t58 = '".addslashes($this->m_t58)."' "
		." , t59 = '".addslashes($this->m_t59)."' "
		." , t60 = '".addslashes($this->m_t60)."' "
		." , t61 = '".addslashes($this->m_t61)."' "
		." , t62 = '".addslashes($this->m_t62)."' "
		." , t63 = '".addslashes($this->m_t63)."' "
		." , t64 = '".addslashes($this->m_t64)."' "
		." , t65 = '".addslashes($this->m_t65)."' "
		." , t66 = '".addslashes($this->m_t66)."' "
		." , t67 = '".addslashes($this->m_t67)."' "
		." , t68 = '".addslashes($this->m_t68)."' "
		." , t69 = '".addslashes($this->m_t69)."' "
		." , t70 = '".addslashes($this->m_t70)."' "
		." , t71 = '".addslashes($this->m_t71)."' "
		." , t72 = '".addslashes($this->m_t72)."' "
		." , t73 = '".addslashes($this->m_t73)."' "
		." , t74 = '".addslashes($this->m_t74)."' "
		." , t75 = '".addslashes($this->m_t75)."' "
		." , t76 = '".addslashes($this->m_t76)."' "
		." , t77 = '".addslashes($this->m_t77)."' "
		." , t78 = '".addslashes($this->m_t78)."' "
		." , t79 = '".addslashes($this->m_t79)."' "
		." , t80 = '".addslashes($this->m_t80)."' "
		." , t81 = '".addslashes($this->m_t81)."' "
		." , t82 = '".addslashes($this->m_t82)."' "
		." , t83 = '".addslashes($this->m_t83)."' "
		." , t84 = '".addslashes($this->m_t84)."' "
		." , t85 = '".addslashes($this->m_t85)."' "
		." , t86 = '".addslashes($this->m_t86)."' "
		." , t87 = '".addslashes($this->m_t87)."' "
		." , t88 = '".addslashes($this->m_t88)."' "
		." , t89 = '".addslashes($this->m_t89)."' "
		." , t90 = '".addslashes($this->m_t90)."' "
		." , t91 = '".addslashes($this->m_t91)."' "
		." , t92 = '".addslashes($this->m_t92)."' "
		." , t93 = '".addslashes($this->m_t93)."' "
		." , t94 = '".addslashes($this->m_t94)."' "
		." , t95 = '".addslashes($this->m_t95)."' "
		." , t96 = '".addslashes($this->m_t96)."' "
		." , t97 = '".addslashes($this->m_t97)."' "
		." , t98 = '".addslashes($this->m_t98)."' "
		." , t99 = '".addslashes($this->m_t99)."' "
		." , t00 = '".addslashes($this->m_t00)."' "
		." WHERE insure_form_id = ".$this->m_insure_form_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function update_sup(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   sup_approve = '".$this->m_sup_approve."' "
		." ,  admin_approve = '' "
		." , sup_date = '".$this->m_sup_date."' "
		." , sup_time = '".date('H:i:s')."' "
		." , sup_id = '".$this->m_sup_id."' "
		." , sup_remark = '".$this->m_sup_remark."' "
		." WHERE insure_form_id = ".$this->m_insure_form_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function update_admin(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   admin_approve = '".$this->m_admin_approve."' "
		." , admin_date = '".$this->m_admin_date."' "
		." , admin_time = '".date('H:i:s')."' "
		." , admin_id = '".$this->m_admin_id."' "
		." , admin_remark = '".$this->m_admin_remark."' "
		." WHERE insure_form_id = ".$this->m_insure_form_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	

	function update_admin_code(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   admin_code = '".$this->m_admin_code."' "
		."  , admin_code_date = '".$this->m_admin_code_date."' "
		." , admin_remark = '".$this->m_admin_remark."' "
		." WHERE insure_form_id = ".$this->m_insure_form_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function update_back(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   admin_date = '0000-00-00' , "
		."   admin_time = NULL , "
		."   admin_approve = '' , "
		."   sup_date = '0000-00-00' , "
		."   sup_time = NULL , "
		."   sup_approve = ''  "
		." WHERE insure_form_id = ".$this->m_insure_form_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_form_id=".$this->m_insure_form_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strSql) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strSql;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class InsureFormList extends DataList {
	var $TABLE = "t_insure_form";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_form_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureForm($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
	function loadList() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_form_id) as rowCount FROM ".$this->TABLE." I  "
		." LEFT JOIN t_insure P ON I.insure_id =  P.insure_id "
		." LEFT JOIN t_insure_car IC ON P.car_id =  IC.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
		.$this->getFilterSQL();	// WHERE clause
		//echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
        $strSql = "SELECT DISTINCT I.* FROM ".$this->TABLE." I  "
		." LEFT JOIN t_insure  P ON I.insure_id =  P.insure_id "
		." LEFT JOIN t_insure_car IC ON P.car_id =  IC.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureForm($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_form_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureForm($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
}
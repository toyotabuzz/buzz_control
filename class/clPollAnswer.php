<?
/*********************************************************
		Class :				PollAnswer

		Last update :		20 Nov 02

		Description:		Class manage PollAnswert table

*********************************************************/
 
class PollAnswer extends DB{

	var $TABLE="t_poll_answer";
	
	var $mPollAnswerId;
	function getPollAnswerId() { return $this->mPollAnswerId; }
	function setPollAnswerId($data) { $this->mPollAnswerId = $data; }
	
	var $mPollId;
	function getPollId() { return $this->mPollId; }
	function setPollId($data) { $this->mPollId = $data; }

	var $mPollQuestionId;
	function getPollQuestionId() { return $this->mPollQuestionId; }
	function setPollQuestionId($data) { $this->mPollQuestionId = $data; }

	var $mTypeId;
	function getTypeId() { return htmlspecialchars($this->mTypeId); }
	function setTypeId($data) { $this->mTypeId = $data; }

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mMoreDetail;
	function getMoreDetail() { return htmlspecialchars($this->mMoreDetail); }
	function setMoreDetail($data) { $this->mMoreDetail = $data; }

	var $mRank;
	function getRank() { return $this->mRank; }
	function setRank($data) { $this->mRank = $data; }

	function PollAnswer($objData=NULL) {
        If ($objData->pollAnswerId!="") {
            $this->setPollAnswerId($objData->pollAnswerId);
            $this->setPollId($objData->pollId);
            $this->setPollQuestionId($objData->pollQuestionId);
			$this->setTypeId($objData->typeId);
			$this->setTitle($objData->title);
			$this->setMoreDetail($objData->moreDetail);
			$this->setRank($objData->rank);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
	}
	
	function load() {

		if ($this->mPollAnswerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE pollAnswerId =".$this->mPollAnswerId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollAnswer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, typeId, pollId, pollQuestionId, moreDetail, rank  )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".$this->mTypeId."' , "
						."  '".$this->mPollId."' , "
						."  '".$this->mPollQuestionId."' , "
						."  '".$this->mMoreDetail."' , "
						."  '".$this->mRank."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollAnswerId = mysql_insert_id();
            return $this->mPollAnswerId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." typeId = '".$this->mTypeId."' , "
						." moreDetail = '".$this->mMoreDetail."' , "
						." pollQuestionId = '".$this->mPollQuestionId."' ,  "
						." pollId = '".$this->mPollId."' , "
						." rank = '".$this->mRank."'  "
						." WHERE  pollAnswerId = ".$this->mPollAnswerId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pollAnswerId=".$this->mPollAnswerId." ";
        $this->getConnection();
        $this->query($strSql);

	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_FILE_NEWS.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_FILE_NEWS.$mFileName) And (is_file(PATH_FILE_NEWS.$mFileName) ) )
	    {
		    unLink(PATH_FILE_NEWS.$mFileName);
	    }
	}
}

/*********************************************************
		Class :				PollAnswerList

		Last update :		20 Nov 02

		Description:		PollAnsweruser list

*********************************************************/


class PollAnswerList extends DataList {
	var $TABLE = "t_poll_answer";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pollAnswerId) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "				
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollAnswer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
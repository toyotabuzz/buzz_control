<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureTrack extends DB{

	var $TABLE="t_insure_track";

	var $m_insure_track_id;
	function get_insure_track_id() { return $this->m_insure_track_id; }
	function set_insure_track_id($data) { $this->m_insure_track_id = $data; }
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_tracking_type;
	function get_tracking_type() { return $this->m_tracking_type; }
	function set_tracking_type($data) { $this->m_tracking_type = $data; }
	
	var $m_feedback;
	function get_feedback() { return $this->m_feedback; }
	function set_feedback($data) { $this->m_feedback = $data; }
	
	var $m_feedback_remark;
	function get_feedback_remark() { return $this->m_feedback_remark; }
	function set_feedback_remark($data) { $this->m_feedback_remark = $data; }

	var $m_feedback_date;
	function get_feedback_date() { return $this->m_feedback_date; }
	function set_feedback_date($data) { $this->m_feedback_date = $data; }

	var $m_feedback_time;
	function get_feedback_time() { return $this->m_feedback_time; }
	function set_feedback_time($data) { $this->m_feedback_time = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }
	
	//�յ������
	var $m_year_extend;
	function get_year_extend() { return $this->m_year_extend; }
	function set_year_extend($data) { $this->m_year_extend = $data; }

	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	var $m_timeline;
	function get_timeline() { return $this->m_timeline; }
	function set_timeline($data) { $this->m_timeline = $data; }	

	var $m_rate_lead;
	function get_rate_lead() { return $this->m_rate_lead; }
	function set_rate_lead($data) { $this->m_rate_lead = $data; }	
	
	function InsureTrack($objData=NULL) {
        If ($objData->insure_track_id !="") {
			$this->set_insure_track_id($objData->insure_track_id);
			$this->set_car_id($objData->car_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_tracking_type($objData->tracking_type);
			$this->set_feedback($objData->feedback);
			$this->set_feedback_time($objData->feedback_time);
			$this->set_feedback_remark($objData->feedback_remark);
			$this->set_feedback_date($objData->feedback_date);
			$this->set_remark($objData->remark);
			$this->set_date_add($objData->date_add);
			$this->set_year_extend($objData->year_extend);
			$this->set_status($objData->status);
			$this->set_timeline($objData->timeline);
			$this->set_rate_lead($objData->rate_lead);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_track_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_track_id =".$this->m_insure_track_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadLastTrack() {
		if ($this->m_insure_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_id =".$this->m_insure_id." ORDER BY insure_track_id DESC LIMIT 1";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	
	function loadMaxCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MAX(insure_track_id) as insure_track_id FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( car_id, insure_id, tracking_type, feedback, feedback_time, feedback_remark, feedback_date, remark, date_add , year_extend, timeline, status, rate_lead, created_at, created_by ) " ." VALUES ( "
		
		." '".$this->m_car_id."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_tracking_type."' , "
		." '".$this->m_feedback."' , "
		." '".$this->m_feedback_time."' , "
		." '".$this->m_feedback_remark."' , "
		." '".((empty($this->m_feedback_date))?'000-00-00':$this->m_feedback_date)."' , "
		." '".$this->m_remark."' , "
		." '".date("Y-m-d H:i:s")."' , "
		." '".$this->m_year_extend."' , "
		." '".$this->m_timeline."' , "
		." '".$this->m_status."', "
		." '".$this->m_rate_lead."', "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 

		// echo $strSql;exit();
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_track_id = mysql_insert_id();
            return $this->m_insure_track_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  car_id = '".$this->m_car_id."' "
		." , insure_id = '".$this->m_insure_id."' "
		." , tracking_type = '".$this->m_tracking_type."' "
		." , feedback = '".$this->m_feedback."' "
		." , feedback_remark = '".$this->m_feedback_remark."' "
		." , remark = '".$this->m_remark."' "
		." , timeline = '".$this->m_timeline."' "
		." , rate_lead = '".$this->m_rate_lead."' "
		." , date_add = '".$this->m_date_add."' "
		." WHERE insure_track_id = ".$this->m_insure_track_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  status = '".$this->m_status."' "
		." WHERE insure_track_id = ".$this->m_insure_track_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_track_id=".$this->m_insure_track_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureTrackList extends DataList {
	var $TABLE = "t_insure_track";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			// echo $strSql;
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureTrack($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadSale() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_id) as rowCount FROM ".$this->TABLE." P  "
		." left join t_insure I ON I.insure_id = P.insure_id  "
		.$this->getFilterSQL();	// WHERE clause
	//    echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." left join t_insure I ON I.insure_id = P.insure_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureTrack($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
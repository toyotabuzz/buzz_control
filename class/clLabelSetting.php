<?
/*********************************************************
		Class :					LabelSetting

		Last update :	  10 Jan 02

		Description:	  Class manage t_label_setting table

*********************************************************/
 
class LabelSetting extends DB{

	var $TABLE="t_label_setting";

	var $mLabelId;
	function getLabelId() { return $this->mLabelId; }
	function setLabelId($data) { $this->mLabelId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mHeight;
	function getHeight() { return htmlspecialchars($this->mHeight); }
	function setHeight($data) { $this->mHeight = $data; }
	
	var $mHeight01;
	function getHeight01() { return htmlspecialchars($this->mHeight01); }
	function setHeight01($data) { $this->mHeight01 = $data; }
	
	var $mHeight02;
	function getHeight02() { return htmlspecialchars($this->mHeight02); }
	function setHeight02($data) { $this->mHeight02 = $data; }
	
	var $mHeight03;
	function getHeight03() { return htmlspecialchars($this->mHeight03); }
	function setHeight03($data) { $this->mHeight03 = $data; }
	
	var $mWidth;
	function getWidth() { return htmlspecialchars($this->mWidth); }
	function setWidth($data) { $this->mWidth = $data; }
	
	var $mWidth01;
	function getWidth01() { return htmlspecialchars($this->mWidth01); }
	function setWidth01($data) { $this->mWidth01 = $data; }
	
	var $mWidth02;
	function getWidth02() { return htmlspecialchars($this->mWidth02); }
	function setWidth02($data) { $this->mWidth02 = $data; }
	
	var $mWidth03;
	function getWidth03() { return htmlspecialchars($this->mWidth03); }
	function setWidth03($data) { $this->mWidth03 = $data; }
	
	var $mNum01;
	function getNum01() { return htmlspecialchars($this->mNum01); }
	function setNum01($data) { $this->mNum01 = $data; }
	
	var $mNum02;
	function getNum02() { return htmlspecialchars($this->mNum02); }
	function setNum02($data) { $this->mNum02 = $data; }
	
	var $mNum03;
	function getNum03() { return htmlspecialchars($this->mNum03); }
	function setNum03($data) { $this->mNum03 = $data; }
	
	var $mNum04;
	function getNum04() { return htmlspecialchars($this->mNum04); }
	function setNum04($data) { $this->mNum04 = $data; }
	
	var $mFontSize;
	function getFontSize() { return htmlspecialchars($this->mFontSize); }
	function setFontSize($data) { $this->mFontSize = $data; }
	
	var $mPaper;
	function getPaper() { return htmlspecialchars($this->mPaper); }
	function setPaper($data) { $this->mPaper = $data; }
	
	var $mDateAdd;
	function getDateAdd() { return htmlspecialchars($this->mDateAdd); }
	function setDateAdd($data) { $this->mDateAdd = $data; }
	
	function LabelSetting($objData=NULL) {
        If ($objData->label_id !="") {
            $this->setLabelId($objData->label_id);
			$this->setTitle($objData->title);
			$this->setHeight($objData->height);
			$this->setHeight01($objData->height01);
			$this->setHeight02($objData->height02);
			$this->setHeight03($objData->height03);
			$this->setWidth($objData->width);
			$this->setWidth01($objData->width01);
			$this->setWidth02($objData->width02);
			$this->setWidth03($objData->width03);
			$this->setNum01($objData->num01);
			$this->setNum02($objData->num02);
			$this->setNum03($objData->num03);
			$this->setNum04($objData->num04);
			$this->setFontSize($objData->font_size);
			$this->setPaper($objData->paper);
			$this->setDateAdd($objData->date_add);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mLabelId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE label_id =".$this->mLabelId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->LabelSetting($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->LabelSetting($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		global $sMemberId;		
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, height,height01, height02, height03, width,  width01, width02, width03, num01, num02, num03, num04, font_size , paper, date_add ) " 
						." VALUES ( '".$this->mTitle."' ,'".$this->mHeight."' ,'".$this->mHeight01."' ,'".$this->mHeight02."' ,'".$this->mHeight03."' ,'".$this->mWidth."' ,'".$this->mWidth01."' ,'".$this->mWidth02."' ,'".$this->mWidth03."' ,'".$this->mNum01."' ,'".$this->mNum02."' ,'".$this->mNum03."' ,'".$this->mNum04."', '".$this->mFontSize."', '".$this->mPaper."'  , '".$this->mDateAdd."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mLabelId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mLabelId,"add");			
			
            return $this->mLabelId;
        } else {
			return false;
	    }
	}

	function update(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , height = '".$this->mHeight."'  "
						." , height01 = '".$this->mHeight01."'  "
						." , height02 = '".$this->mHeight02."'  "
						." , height03 = '".$this->mHeight03."'  "
						." , width = '".$this->mWidth."'  "
						." , width01 = '".$this->mWidth01."'  "
						." , width02 = '".$this->mWidth02."'  "
						." , width03 = '".$this->mWidth03."'  "
						." , num01 = '".$this->mNum01."'  "
						." , num02 = '".$this->mNum02."'  "	
						." , num03 = '".$this->mNum03."'  "
						." , num04 = '".$this->mNum04."'  "	
						." , font_size = '".$this->mFontSize."'  "	
						." , paper = '".$this->mPaper."'  "	
						." WHERE  label_id = ".$this->mLabelId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mLabelId,"update");
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
		global $sMemberId;		
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE label_id=".$this->mLabelId." ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mLabelId,"delete");
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;		
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кػ����� label ";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer LabelSettingTopic List

		Last update :		22 Mar 02

		Description:		Customer LabelSettingTopic List

*********************************************************/

class LabelSettingList extends DataList {
	var $TABLE = "t_label_setting";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT label_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new LabelSetting($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getLabelId()."\"");
			if (($defaultId != null) && ($objItem->getLabelId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
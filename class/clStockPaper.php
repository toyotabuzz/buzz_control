<?
/*********************************************************
		Class :					Payment Subject

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_paper table

*********************************************************/
 
class StockPaper extends DB{

	var $TABLE="t_stock_paper";

	var $mStockPaperId;
	function getStockPaperId() { return $this->mStockPaperId; }
	function setStockPaperId($data) { $this->mStockPaperId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mTypeId;
	function getTypeId() { return htmlspecialchars($this->mTypeId); }
	function setTypeId($data) { $this->mTypeId = $data; }	
	
	function getTypeIdDetail(){
		if($this->mTypeId == 0) return "IC-�Ѻ�Թ���";
		if($this->mTypeId == 1) return "IC-�����Թ���";
	}	
	
	function StockPaper($objData=NULL) {
        If ($objData->stock_paper_id !="") {
            $this->setStockPaperId($objData->stock_paper_id);
			$this->setTitle($objData->title);
			$this->setTypeId($objData->type_id);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mStockPaperId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_paper_id =".$this->mStockPaperId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPaper($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPaper($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ,type_id) "
						." VALUES ( '".$this->mTitle."' ,  '".$this->mTypeId."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockPaperId = mysql_insert_id();
            return $this->mStockPaperId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , type_id = '".$this->mTypeId."'  "
						." WHERE  stock_paper_id = ".$this->mStockPaperId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_paper_id=".$this->mStockPaperId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == ""){
			$asrErrReturn["title"] = "��س��к�";
		}else{
			if(!$this->checkUnique($Mode,"title","stock_paper_id","t_stock_paper",$this->mStockPaperId,$this->mTitle)){
				$asrErrReturn["title"] = '�������͡��ë�ӫ�͹';
			}
		}
        Return $asrErrReturn;
    }
	
   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class StockPaperList extends DataList {
	var $TABLE = "t_stock_paper";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_paper_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPaper($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockPaperId()."\"");
			if (($defaultId != null) && ($objItem->getStockPaperId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockPaperId()."\"");
			if (($defaultId != null) && ($objItem->getStockPaperId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
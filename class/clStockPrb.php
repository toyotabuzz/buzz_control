<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class StockPrb extends DB{

	var $TABLE="t_stock_prb";

	var $m_stock_prb_id;
	function get_stock_prb_id() { return $this->m_stock_prb_id; }
	function set_stock_prb_id($data) { $this->m_stock_prb_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_title01;
	function get_title01() { return $this->m_title01; }
	function set_title01($data) { $this->m_title01 = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }
	
	var $m_date_out;
	function get_date_out() { return $this->m_date_out; }
	function set_date_out($data) { $this->m_date_out = $data; }
	
	// 0 = �������
	// 1 = �١��
	// 2 = �觤׹
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function get_status_detail() {
		if($this->m_status == 0 ) return "����";
		if($this->m_status == 1 ) return "�١��";
		if($this->m_status == 2 ) return "�觤׹";
	 }	
	function set_status($data) { $this->m_status = $data; }

	var $m_use_status_01;
	function get_use_status_01() { return $this->m_use_status_01; }
	function set_use_status_01($data) { $this->m_use_status_01 = $data; }	

	var $m_use_status_02;
	function get_use_status_02() { return $this->m_use_status_02; }
	function set_use_status_02($data) { $this->m_use_status_02 = $data; }	
	
	function StockPrb($objData=NULL) {
        If ($objData->stock_prb_id !="") {
			$this->set_stock_prb_id($objData->stock_prb_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_title($objData->title);
			$this->set_title01($objData->title01);
			$this->set_date_add($objData->date_add);
			$this->set_date_out($objData->date_out);
			$this->set_status($objData->status);
			$this->set_use_status_01($objData->use_status_01);
			$this->set_use_status_02($objData->use_status_02);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_stock_prb_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_prb_id =".$this->m_stock_prb_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPrb($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPrb($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_company_id , title , title01, date_add, date_out ) " ." VALUES ( "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_title."' , "
		." '".$this->m_title01."' , "
		." '".$this->m_date_add."' , "
		." '".$this->m_date_out."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_stock_prb_id = mysql_insert_id();
            return $this->m_stock_prb_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  insure_company_id = '".$this->m_insure_company_id."' "
		." , title = '".$this->m_title."' "
		." , title01 = '".$this->m_title01."' "
		." , date_add = '".$this->m_date_add."' "
		." , date_out = '".$this->m_date_out."' "
		." WHERE stock_prb_id = ".$this->m_stock_prb_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  status = '".$this->m_status."' "
		." WHERE stock_prb_id = ".$this->m_stock_prb_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus01(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  use_status_01 = '".$this->m_use_status_01."' "
		." WHERE stock_prb_id = ".$this->m_stock_prb_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStatus02(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  use_status_02 = '".$this->m_use_status_02."' "
		." WHERE stock_prb_id = ".$this->m_stock_prb_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_prb_id=".$this->m_stock_prb_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class StockPrbList extends DataList {
	var $TABLE = "t_stock_prb";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_prb_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPrb($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_stock_prb_id()."\"");
			if (($defaultId != null) && ($objItem->get_stock_prb_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	function printSelectTitle01($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_stock_prb_id()."\"");
			if (($defaultId != null) && ($objItem->get_stock_prb_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title01()."</option>");
		}
		echo("</select>");
	}			
	
	
}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureBeaItem extends DB{

	var $TABLE="t_insure_bea_item";

var $m_insure_bea_item_id;
function get_insure_bea_item_id() { return $this->m_insure_bea_item_id; }
function set_insure_bea_item_id($data) { $this->m_insure_bea_item_id = $data; }

var $m_insure_bea_id;
function get_insure_bea_id() { return $this->m_insure_bea_id; }
function set_insure_bea_id($data) { $this->m_insure_bea_id = $data; }

var $m_start_date;
function get_start_date() { return $this->m_start_date; }
function set_start_date($data) { $this->m_start_date = $data; }

var $m_end_date;
function get_end_date() { return $this->m_end_date; }
function set_end_date($data) { $this->m_end_date = $data; }

var $m_plan0101;
function get_plan0101() { return $this->m_plan0101; }
function set_plan0101($data) { $this->m_plan0101 = $data; }

var $m_plan0102;
function get_plan0102() { return $this->m_plan0102; }
function set_plan0102($data) { $this->m_plan0102 = $data; }

var $m_plan0103;
function get_plan0103() { return $this->m_plan0103; }
function set_plan0103($data) { $this->m_plan0103 = $data; }

var $m_plan0104;
function get_plan0104() { return $this->m_plan0104; }
function set_plan0104($data) { $this->m_plan0104 = $data; }

var $m_plan0105;
function get_plan0105() { return $this->m_plan0105; }
function set_plan0105($data) { $this->m_plan0105 = $data; }

var $m_plan0106;
function get_plan0106() { return $this->m_plan0106; }
function set_plan0106($data) { $this->m_plan0106 = $data; }

var $m_plan0107;
function get_plan0107() { return $this->m_plan0107; }
function set_plan0107($data) { $this->m_plan0107 = $data; }

var $m_plan0108;
function get_plan0108() { return $this->m_plan0108; }
function set_plan0108($data) { $this->m_plan0108 = $data; }

var $m_plan0109;
function get_plan0109() { return $this->m_plan0109; }
function set_plan0109($data) { $this->m_plan0109 = $data; }

var $m_plan0110;
function get_plan0110() { return $this->m_plan0110; }
function set_plan0110($data) { $this->m_plan0110 = $data; }

var $m_plan0111;
function get_plan0111() { return $this->m_plan0111; }
function set_plan0111($data) { $this->m_plan0111 = $data; }

var $m_plan0112;
function get_plan0112() { return $this->m_plan0112; }
function set_plan0112($data) { $this->m_plan0112 = $data; }

	function InsureBeaItem($objData=NULL) {
        If ($objData->insure_bea_item_id !="") {
			$this->set_insure_bea_item_id($objData->insure_bea_item_id);
			$this->set_insure_bea_id($objData->insure_bea_id);
			$this->set_start_date($objData->start_date);
			$this->set_end_date($objData->end_date);
			
			$this->set_plan0101($objData->plan0101);
			$this->set_plan0102($objData->plan0102);
			$this->set_plan0103($objData->plan0103);
			$this->set_plan0104($objData->plan0104);
			$this->set_plan0105($objData->plan0105);
			$this->set_plan0106($objData->plan0106);
			$this->set_plan0107($objData->plan0107);
			$this->set_plan0108($objData->plan0108);
			$this->set_plan0109($objData->plan0109);
			$this->set_plan0110($objData->plan0110);
			$this->set_plan0111($objData->plan0111);
			$this->set_plan0112($objData->plan0112);

        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_bea_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_bea_item_id =".$this->m_insure_bea_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_bea_id , start_date , end_date , plan0101 , plan0102 , plan0103 , plan0104 , plan0105 , plan0106 , plan0107 , plan0108 , plan0109 , plan0110 , plan0111  , plan0112) " ." VALUES ( "
		." '".$this->m_insure_bea_id."' , "
		." '".$this->m_start_date."' , "
		." '".$this->m_end_date."' , "
		." '".$this->m_plan0101."' , "
		." '".$this->m_plan0102."' , "
		." '".$this->m_plan0103."' , "
		." '".$this->m_plan0104."' , "
		." '".$this->m_plan0105."' , "
		." '".$this->m_plan0106."' , "
		." '".$this->m_plan0107."' , "
		." '".$this->m_plan0108."' , "
		." '".$this->m_plan0109."' , "
		." '".$this->m_plan0110."' , "
		." '".$this->m_plan0111."' , "
		." '".$this->m_plan0112."'  "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_bea_item_id = mysql_insert_id();
            return $this->m_insure_bea_item_id;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE." SET "

		."  start_date = '".$this->m_start_date."' "
		." , end_date = '".$this->m_end_date."' "

		." , plan0101 = '".$this->m_plan0101."' "
		." , plan0102 = '".$this->m_plan0102."' "
		." , plan0103 = '".$this->m_plan0103."' "
		." , plan0104 = '".$this->m_plan0104."' "
		." , plan0105 = '".$this->m_plan0105."' "
		." , plan0106 = '".$this->m_plan0106."' "
		." , plan0107 = '".$this->m_plan0107."' "
		." , plan0108 = '".$this->m_plan0108."' "
		." , plan0109 = '".$this->m_plan0109."' "
		." , plan0110 = '".$this->m_plan0110."' "
		." , plan0111 = '".$this->m_plan0111."' "
		." , plan0112 = '".$this->m_plan0112."' "
		." WHERE insure_bea_item_id = ".$this->m_insure_bea_item_id." "; 	
	
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_bea_item_id=".$this->m_insure_bea_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
       $strSql = " DELETE FROM t_insure_bea_item_detail "
                . " WHERE insure_bea_item_id=".$this->m_insure_bea_item_id." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureBeaItemList extends DataList {
	var $TABLE = "t_insure_bea_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {
			echo("<option value=\"".$objItem->get_insure_bea_item_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_bea_item_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
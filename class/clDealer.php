<?
/*********************************************************
		Class :				Dealer

		Last update :		20 Aug 05

		Description:		Class manage dealer table

*********************************************************/
 
class Dealer extends User{

	var $TABLE="t_dealer";

	var $mDealerId;
	function getDealerId() { return $this->mDealerId; }
	function setDealerId($data) { $this->mDealerId = $data; }	

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mAddress1;
	function getAddress1() { return htmlspecialchars($this->mAddress1); }
	function setAddress1($data) { $this->mAddress1 = $data; }

	var $mAddress2;
	function getAddress2() { return htmlspecialchars($this->mAddress2); }
	function setAddress2($data) { $this->mAddress2 = $data; }
	
	var $mProvince;
	function getProvince() { return htmlspecialchars($this->mProvince); }
	function setProvince($data) { $this->mProvince = $data; }

	var $mZip;
	function getZip() { return htmlspecialchars($this->mZip); }
	function setZip($data) { $this->mZip = $data; }

	var $mPhone;
	function getPhone() { return htmlspecialchars($this->mPhone); }
	function setPhone($data) { $this->mPhone = $data; }

	var $mFax;
	function getFax() { return htmlspecialchars($this->mFax); }
	function setFax($data) { $this->mFax = $data; }

	var $mTaxId;
	function getTaxId() { return htmlspecialchars($this->mTaxId); }
	function setTaxId($data) { $this->mTaxId = $data; }

	var $mEmail;
	function getEmail() { return htmlspecialchars($this->mEmail); }
	function setEmail($data) { $this->mEmail = $data; }

	var $mWebsite;
	function getWebsite() { return htmlspecialchars($this->mWebsite); }
	function setWebsite($data) { $this->mWebsite = $data; }
	
	var $mContactName;
	function getContactName() { return htmlspecialchars($this->mContactName); }
	function setContactName($data) { $this->mContactName = $data; }		
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }	

	var $mLogo;
	function getLogo() { return htmlspecialchars($this->mLogo); }
	function setLogo($data) { $this->mLogo = $data; }
	
	function Dealer($objData=NULL) {
        If ($objData->dealer_id !="") {
            $this->setDealerId($objData->dealer_id);
			$this->setTitle($objData->title);
			$this->setAddress1($objData->address1);
			$this->setAddress2($objData->address2);
			$this->setProvince($objData->province);
			$this->setZip($objData->zip);
			$this->setPhone($objData->phone);
			$this->setFax($objData->fax);
			$this->setTaxId($objData->tax_id);
			$this->setRemark($objData->remark);
			$this->setContactName($objData->contact_name);
			$this->setEmail($objData->email);
			$this->setWebsite($objData->website);
			$this->setLogo($objData->logo);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
		$this->setAddress1(stripslashes($this->mAddress1));
		$this->setAddress2(stripslashes($this->mAddress2));
		$this->setProvince(stripslashes($this->mProvince));
		$this->setZip(stripslashes($this->mZip));
		$this->setPhone(stripslashes($this->mPhone));
		$this->setFax(stripslashes($this->mFax));
		$this->setTaxId(stripslashes($this->mTaxId));
		$this->setRemark(stripslashes($this->mRemark));
		$this->setContactName(stripslashes($this->mContactName));
		$this->setEmail(stripslashes($this->mEmail));
		$this->setWebsite(stripslashes($this->mWebsite));
	}
		
	function load() {

		if ($this->mDealerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE dealer_id =".$this->mDealerId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Dealer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mDealerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE dealer_id =".$this->mDealerId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Dealer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	

	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Dealer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, address1, address2, province, zip, phone, fax, tax_id ,remark, contact_name, email , logo, website )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".$this->mAddress1."' , "
						."  '".$this->mAddress2."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mPhone."' , "
						."  '".$this->mFax."' , "
						."  '".$this->mTaxId."' ,"
						."  '".$this->mRemark."' ,"
						."  '".$this->mContactName."' , "
						."  '".$this->mEmail."' , "
						."  '".$this->mLogo."' , "
						."  '".$this->mWebsite."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mDealerId = mysql_insert_id();
            return $this->mDealerId;
        } else {
			return false;
	    }
	}
	
	
	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." address1 = '".$this->mAddress1."' , "
						." address2 = '".$this->mAddress2."' , "
						." province = '".$this->mProvince."' , "
						." zip = '".$this->mZip."' , "
						." tax_id = '".$this->mTaxId."' , "
						." phone = '".$this->mPhone."' , "
						." fax = '".$this->mFax."' , "
						." remark = '".$this->mRemark."' , "
						." contact_name = '".$this->mContactName."' , "
						." email = '".$this->mEmail."' , "
						." logo = '".$this->mLogo."' , "
						." website = '".$this->mWebsite."'  "
						." WHERE  dealer_id = ".$this->mDealerId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE dealer_id=".$this->mDealerId." ";
        $this->getConnection();
        $this->query($strSql);
		$this->deletePicture($this->mLogo);
		
       $strSql = " DELETE FROM t_stock_dealer  "
                . " WHERE dealer_id=".$this->mDealerId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
		
	}

	 Function check($strMode)
    {
		if ($this->mTitle == "") {
			$asrErrReturn["title"] = '��س��кت��ͺ���ѷ';
		}else{
			if(!$this->checkUniqueTitle($strMode)){
				$asrErrReturn["title"] = '���ͺ���ѷ��ӫ�͹';
			}		
		}
        Return $asrErrReturn;
    }
	
    function checkUniqueTitle($strMode)
    {
        $strSql  = "SELECT title FROM t_dealer ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE title = " .$this->convStr4SQL($this->mTitle);
		}else{
			$strSql .= " WHERE title = " .$this->convStr4SQL($this->mTitle)." and dealer_id != ".$this->mDealerId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}	
	
}


/*********************************************************
		Class :				DealerList

		Last update :		20 Nov 02

		Description:		Dealer list

*********************************************************/


class DealerList extends DataList {
	var $TABLE = "t_dealer";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT dealer_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Dealer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT dealer_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Dealer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"0\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getDealerId()."\"");
			if (($defaultId != null) && ($objItem->getDealerId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
}
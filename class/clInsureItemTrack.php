<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureItemTrack extends DB{

	var $TABLE="t_insure_item_track";

	var $m_insure_item_track_id;
	function get_insure_item_track_id() { return $this->m_insure_item_track_id; }
	function set_insure_item_track_id($data) { $this->m_insure_item_track_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_insure_item_id;
	function get_insure_item_id() { return $this->m_insure_item_id; }
	function set_insure_item_id($data) { $this->m_insure_item_id = $data; }
	
	var $m_track_status;
	function get_track_status() { return $this->m_track_status; }
	function set_track_status($data) { $this->m_track_status = $data; }
	
	var $m_track_date;
	function get_track_date() { return $this->m_track_date; }
	function set_track_date($data) { $this->m_track_date = $data; }
	
	var $m_track_new_date;
	function get_track_new_date() { return $this->m_track_new_date; }
	function set_track_new_date($data) { $this->m_track_new_date = $data; }
	
	var $m_track_remark;
	function get_track_remark() { return $this->m_track_remark; }
	function set_track_remark($data) { $this->m_track_remark = $data; }
	

	
	function InsureItemTrack($objData=NULL) {
        If ($objData->insure_item_track_id !="") {
			$this->set_insure_item_track_id($objData->insure_item_track_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_insure_item_id($objData->insure_item_id);
			$this->set_track_status($objData->track_status);
			$this->set_track_date($objData->track_date);
			$this->set_track_new_date($objData->track_new_date);
			$this->set_track_remark($objData->track_remark);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_item_track_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_item_track_id =".$this->m_insure_item_track_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureItemTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureItemTrack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_item_id , track_status , track_date , track_new_date,  track_remark ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_insure_item_id."' , "
		." '".$this->m_track_status."' , "
		." '".$this->m_track_date."' , "
		." '".$this->m_track_new_date."' , "
		." '".$this->m_track_remark."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_item_track_id = mysql_insert_id();
            return $this->m_insure_item_track_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , insure_item_id = '".$this->m_insure_item_id."' "
		." , track_status = '".$this->m_track_status."' "
		." , track_date = '".$this->m_track_date."' "
		." , track_new_date = '".$this->m_track_new_date."' "
		." , track_remark = '".$this->m_track_remark."' "


		." WHERE insure_item_track_id = ".$this->m_insure_item_track_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_item_track_id=".$this->m_insure_item_track_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureItemTrackList extends DataList {
	var $TABLE = "t_insure_item_track";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_track_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_customer C ON IC.customer_id = C.customer_id "
		
		.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_customer C ON IC.customer_id = C.customer_id "		
		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureItemTrack($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }


}
<?
/*********************************************************
		Class :					SaleTarget

		Last update :	  10 Jan 02

		Description:	  Class manage t_sale_target table

*********************************************************/
 
class SaleTarget extends DB{

	var $TABLE="t_sale_target";

	var $mTargetId;
	function getTargetId() { return $this->mTargetId; }
	function setTargetId($data) { $this->mTargetId = $data; }
	
	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mTeamId;
	function getTeamId() { return $this->mTeamId; }
	function setTeamId($data) { $this->mTeamId = $data; }
	
	var $mTarget;
	function getTarget() { return $this->mTarget; }
	function setTarget($data) { $this->mTarget = $data; }
	
	var $mMonth;
	function getMonth() { return $this->mMonth; }
	function setMonth($data) { $this->mMonth = $data; }
	
	var $mYear;
	function getYear() { return $this->mYear; }
	function setYear($data) { $this->mYear = $data; }
	
	function SaleTarget($objData=NULL) {
        If ($objData->year !="") {
			$this->setTargetId($objData->target_id);
			$this->setCarModelId($objData->car_model_id);
			$this->setTeamId($objData->team_id);
			$this->setTarget($objData->target);
			$this->setMonth($objData->month);
			$this->setYear($objData->year);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mSaleTargetId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE target_id =".$this->mSaleTargetId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->SaleTarget($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->SaleTarget($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( car_model_id,team_id,target,month,year ) " ." VALUES ( "
		." '".$this->mCarModelId."' , "
		." '".$this->mTeamId."' , "
		." '".$this->mTarget."' , "
		." '".$this->mMonth."' , "
		." '".$this->mYear."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mSaleTargetId = mysql_insert_id();
            return $this->mSaleTargetId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE ." SET "
		."  car_model_id = '".$this->mCarModelId."' "
		." , team_id = '".$this->mTeamId."' "
		." , target = '".$this->mTarget."' "
		." , month = '".$this->mMonth."' "
		." , year = '".$this->mYear."' "
		." WHERE target_id = ".$this->mTargetId." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE target_id=".$this->mSaleTargetId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTarget == "") $asrErrReturn["target"] = "��س��к�";
		if ($this->mMonth == "") $asrErrReturn["month"] = "��س��к�";
		if ($this->mYear == "") $asrErrReturn["year"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class SaleTargetList extends DataList {
	var $TABLE = "t_sale_target";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT target_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new SaleTarget($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadYear() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT target_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT(year) AS year FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new SaleTarget($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getSaleTargetId()."\"");
			if (($defaultId != null) && ($objItem->getSaleTargetId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getSaleTargetId()."\"");
			if (($defaultId != null) && ($objItem->getSaleTargetId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
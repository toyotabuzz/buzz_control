<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureBeaCar extends DB{

	var $TABLE="t_insure_bea_car";

	var $m_insure_bea_car_id;
	function get_insure_bea_car_id() { return $this->m_insure_bea_car_id; }
	function set_insure_bea_car_id($data) { $this->m_insure_bea_car_id = $data; }
	
	var $m_insure_bea_id;
	function get_insure_bea_id() { return $this->m_insure_bea_id; }
	function set_insure_bea_id($data) { $this->m_insure_bea_id = $data; }
	
	var $m_car_type;
	function get_car_type() { return $this->m_car_type; }
	function set_car_type($data) { $this->m_car_type = $data; }
	
	var $m_car_model_id;
	function get_car_model_id() { return $this->m_car_model_id; }
	function set_car_model_id($data) { $this->m_car_model_id = $data; }
	
	var $m_car_serie_id;
	function get_car_serie_id() { return $this->m_car_serie_id; }
	function set_car_serie_id($data) { $this->m_car_serie_id = $data; }

	function InsureBeaCar($objData=NULL) {
        If ($objData->insure_bea_car_id !="") {
			$this->set_insure_bea_car_id($objData->insure_bea_car_id);
			$this->set_insure_bea_id($objData->insure_bea_id);
			$this->set_car_type($objData->car_type);
			$this->set_car_model_id($objData->car_model_id);
			$this->set_car_serie_id($objData->car_serie_id);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_bea_car_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_bea_car_id =".$this->m_insure_bea_car_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_bea_id , car_type , car_model_id , car_serie_id ) " ." VALUES ( "
		." '".$this->m_insure_bea_id."' , "
		." '".$this->m_car_type."' , "
		." '".$this->m_car_model_id."' , "
		." '".$this->m_car_serie_id."' "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_bea_car_id = mysql_insert_id();
            return $this->m_insure_bea_car_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_bea_id = '".$this->m_insure_bea_id."' "
		." , car_type = '".$this->m_car_type."' "
		." , car_model_id = '".$this->m_car_model_id."' "
		." , car_serie_id = '".$this->m_car_serie_id."' "
		." WHERE insure_bea_car_id = ".$this->m_insure_bea_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_bea_car_id=".$this->m_insure_bea_car_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureBeaCarList extends DataList {
	var $TABLE = "t_insure_bea_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {
			echo("<option value=\"".$objItem->get_insure_bea_car_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_bea_car_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
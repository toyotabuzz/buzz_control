<?
/*********************************************************
		Class :					ModuleMenu

		Last update :	  10 Jan 02

		Description:	  Class manage t_module table

*********************************************************/
 
class ModuleMenu extends DB{

	var $TABLE="t_module_menu";

	var $mModuleMenuId;
	function getModuleMenuId() { return $this->mModuleMenuId; }
	function setModuleMenuId($data) { $this->mModuleMenuId = $data; }
	
	var $mModuleId;
	function getModuleId() { return $this->mModuleId; }
	function setModuleId($data) { $this->mModuleId = $data; }	
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mAction;
	function getAction() { return htmlspecialchars($this->mAction); }
	function setAction($data) { $this->mAction = $data; }	
	
	function ModuleMenu($objData=NULL) {
        If ($objData->module_menu_id !="") {
            $this->setModuleMenuId($objData->module_menu_id);
			$this->setModuleId($objData->module_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setAction($objData->action);
        }
    }

	function init(){	
		$this->setCode(stripslashes($this->mCode));
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mModuleMenuId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE module_menu_id =".$this->mModuleMenuId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->ModuleMenu($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->ModuleMenu($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( module_id, code,  action, title ) "
						." VALUES ( '".$this->mModuleId."' , "
						."  '".$this->mCode."' , "
						."  '".$this->mAction."' , "
						."  '".$this->mTitle."' ) ";
        $this->getConnection();		
		//echo $strSql;
        If ($Result = $this->query($strSql)) { 
            $this->mModuleMenuId = mysql_insert_id();
            return $this->mModuleMenuId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET code = '".$this->mCode."' , "
						." module_id = '".$this->mModuleId."' , "
						." action = '".$this->mAction."' , "
						." title= '".$this->mTitle."' "
						." WHERE  module_menu_id = ".$this->mModuleMenuId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE module_menu_id=".$this->mModuleMenuId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_member_module "
                . " WHERE module_code='".$this->mCode."' ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCode == "") $asrErrReturn["code"] = "��س��к� Code";
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��������";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				ModuleMenuList

		Last update :		22 Mar 02

		Description:		ModuleMenu user list

*********************************************************/


class ModuleMenuList extends DataList {
	var $TABLE = "t_module_menu";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT module_menu_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new ModuleMenu($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCode()."\"");
			if (($defaultId != null) && ($objItem->getModuleMenuId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			

}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureBea extends DB{

	var $TABLE="t_insure_bea";

var $m_insure_bea_id;
function get_insure_bea_id() { return $this->m_insure_bea_id; }
function set_insure_bea_id($data) { $this->m_insure_bea_id = $data; }

var $m_start_date;
function get_start_date() { return $this->m_start_date; }
function set_start_date($data) { $this->m_start_date = $data; }

var $m_end_date;
function get_end_date() { return $this->m_end_date; }
function set_end_date($data) { $this->m_end_date = $data; }

var $m_broker_id;
function get_broker_id() { return $this->m_broker_id; }
function set_broker_id($data) { $this->m_broker_id = $data; }

var $m_insure_company_id;
function get_insure_company_id() { return $this->m_insure_company_id; }
function set_insure_company_id($data) { $this->m_insure_company_id = $data; }

var $m_title;
function get_title() { return $this->m_title; }
function set_title($data) { $this->m_title = $data; }

var $m_personal_type;
function get_personal_type() { return $this->m_personal_type; }
function set_personal_type($data) { $this->m_personal_type = $data; }

var $m_fix_type;
function get_fix_type() { return $this->m_fix_type; }
function set_fix_type($data) { $this->m_fix_type = $data; }

var $m_fee_code;
function get_fee_code() { return $this->m_fee_code; }
function set_fee_code($data) { $this->m_fee_code = $data; }

var $m_fee_code1;
function get_fee_code1() { return $this->m_fee_code1; }
function set_fee_code1($data) { $this->m_fee_code1 = $data; }

var $m_include_prb;
function get_include_prb() { return $this->m_include_prb; }
function set_include_prb($data) { $this->m_include_prb = $data; }

var $m_prb_price;
function get_prb_price() { return $this->m_prb_price; }
function set_prb_price($data) { $this->m_prb_price = $data; }

var $m_group_type;
function get_group_type() { return $this->m_group_type; }
function set_group_type($data) { $this->m_group_type = $data; }

var $m_remark;
function get_remark() { return $this->m_remark; }
function set_remark($data) { $this->m_remark = $data; }

var $m_insure_type_id;
function get_insure_type_id() { return $this->m_insure_type_id; }
function set_insure_type_id($data) { $this->m_insure_type_id = $data; }

var $m_percent;
function get_percent() { return $this->m_percent; }
function set_percent($data) { $this->m_percent = $data; }

var $m_car_year;
function get_car_year() { return $this->m_car_year; }
function set_car_year($data) { $this->m_car_year = $data; }

var $m_start_year;
function get_start_year() { return $this->m_start_year; }
function set_start_year($data) { $this->m_start_year = $data; }

var $m_end_year;
function get_end_year() { return $this->m_end_year; }
function set_end_year($data) { $this->m_end_year = $data; }

var $m_product_vib;
function get_product_vib() { return $this->m_product_vib; }
function set_product_vib($data) { $this->m_product_vib = $data; }

	
	function InsureBea($objData=NULL) {
        If ($objData->insure_bea_id !="") {
			$this->set_insure_bea_id($objData->insure_bea_id);
			$this->set_start_date($objData->start_date);
			$this->set_end_date($objData->end_date);
			$this->set_title($objData->title);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_personal_type($objData->personal_type);
			$this->set_fix_type($objData->fix_type);
			$this->set_fee_code($objData->fee_code);
			$this->set_fee_code1($objData->fee_code1);
			$this->set_include_prb($objData->include_prb);
			$this->set_prb_price($objData->prb_price);
			$this->set_group_type($objData->group_type);
			$this->set_remark($objData->remark);
			$this->set_insure_type_id($objData->insure_type_id);
			$this->set_percent($objData->percent);
			$this->set_car_year($objData->car_year);
			$this->set_start_year($objData->start_year);
			$this->set_end_year($objData->end_year);
			$this->set_product_vib($objData->product_vib);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_bea_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_bea_id =".$this->m_insure_bea_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBea($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBea($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( start_date , end_date , start_year, end_year, title, broker_id, insure_company_id , personal_type , fix_type , fee_code , fee_code1, include_prb , prb_price , group_type , insure_type_id, car_year, percent, product_vib, remark ) " ." VALUES ( "
		." '".$this->m_start_date."' , "
		." '".$this->m_end_date."' , "
		." '".$this->m_start_year."' , "
		." '".$this->m_end_year."' , "
		." '".$this->m_title."' , "
		." '".$this->m_broker_id."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_personal_type."' , "
		." '".$this->m_fix_type."' , "
		." '".$this->m_fee_code."' , "
		." '".$this->m_fee_code1."' , "
		." '".$this->m_include_prb."' , "
		." '".$this->m_prb_price."' , "
		." '".$this->m_group_type."' , "
		." '".$this->m_insure_type_id."' , "
		." '".$this->m_car_year."' , "
		." '".$this->m_percent."' , "
		." '".$this->m_product_vib."' , "
		." '".$this->m_remark."' "
		." ) ";


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_bea_id = mysql_insert_id();
            return $this->m_insure_bea_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." start_date = '".$this->m_start_date."' "
		." , end_date = '".$this->m_end_date."' "
		." , start_year = '".$this->m_start_year."' "
		." , end_year = '".$this->m_end_year."' "
		." , title = '".$this->m_title."' "
		." , broker_id = '".$this->m_broker_id."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , personal_type = '".$this->m_personal_type."' "
		." , fix_type = '".$this->m_fix_type."' "
		." , fee_code = '".$this->m_fee_code."' "
		." , fee_code1 = '".$this->m_fee_code1."' "
		." , include_prb = '".$this->m_include_prb."' "
		." , prb_price = '".$this->m_prb_price."' "
		." , group_type = '".$this->m_group_type."' "
		." , insure_type_id = '".$this->m_insure_type_id."' "
		." , car_year = '".$this->m_car_year."' "
		." , percent = '".$this->m_percent."' "
		." , product_vib = '".$this->m_product_vib."' "
		." , remark = '".$this->m_remark."' "
		." WHERE insure_bea_id = ".$this->m_insure_bea_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_bea_id=".$this->m_insure_bea_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureBeaList extends DataList {
	var $TABLE = "t_insure_bea";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBea($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBea($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {
			echo("<option value=\"".$objItem->get_insure_bea_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_bea_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
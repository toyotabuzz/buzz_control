<?
/*********************************************************
		Class :					Car Model

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_model table

*********************************************************/
 
class CarModel extends DB{

	var $TABLE="t_car_model";

	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mCarTypeId;
	function getCarTypeId() { return $this->mCarTypeId; }
	function setCarTypeId($data) { $this->mCarTypeId = $data; }
	
	var $mSaleModel;
	function getSaleModel() { return htmlspecialchars($this->mSaleModel); }
	function setSaleModel($data) { $this->mSaleModel = $data; }
		
	var $mSaleModel01;
	function getSaleModel01() { return htmlspecialchars($this->mSaleModel01); }
	function setSaleModel01($data) { $this->mSaleModel01 = $data; }
		
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	
	
	var $mExpire;
	function getExpire() { return htmlspecialchars($this->mExpire); }
	function setExpire($data) { $this->mExpire = $data; }	
	
	var $mACC03;
	function getACC03() { return htmlspecialchars($this->mACC03); }
	function setACC03($data) { $this->mACC03 = $data; }			
	
	var $mInsureName;
	function getInsureName() { return htmlspecialchars($this->mInsureName); }
	function setInsureName($data) { $this->mInsureName = $data; }				
	
	function CarModel($objData=NULL) {
        If ($objData->car_model_id !="") {
            $this->setCarModelId($objData->car_model_id);
			$this->setCarTypeId($objData->car_type_id);
			$this->setSaleModel($objData->sale_model);
			$this->setSaleModel01($objData->sale_model_01);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
			$this->setExpire($objData->expire);
			$this->setACC03($objData->acc03);
			$this->setInsureName($objData->insure_name);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCarModelId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_model_id =".$this->mCarModelId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarModel($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarModel($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title , car_type_id, sale_model, sale_model_01, expire, acc03, insure_name,  rank ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mCarTypeId."' , '".$this->mSaleModel."' , '".$this->mSaleModel01."' , '".$this->mExpire."' , '".$this->mACC03."' , '".$this->mInsureName."' , '".$this->mRank."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCarModelId = mysql_insert_id();
            return $this->mCarModelId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , sale_model = '".$this->mSaleModel."'  "
						." , sale_model_01 = '".$this->mSaleModel01."'  "
						." , expire = '".$this->mExpire."'  "
						." , acc03 = '".$this->mACC03."'  "
						." WHERE  car_model_id = ".$this->mCarModelId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateInsure(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , sale_model = '".$this->mSaleModel."'  "
						." , sale_model_01 = '".$this->mSaleModel01."'  "
						." , insure_name = '".$this->mInsureName."'  "
						." WHERE  car_model_id = ".$this->mCarModelId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function updateRank(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  rank = '".$this->mRank."'  "
						." WHERE  car_model_id = ".$this->mCarModelId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_model_id=".$this->mCarModelId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�Ẻö";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Model List

		Last update :		22 Mar 02

		Description:		Car Model List

*********************************************************/

class CarModelList extends DataList {
	var $TABLE = "t_car_model";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_model_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarModel($row);
			}			
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarModelId()."\"");
			if (($defaultId != null) && ($objItem->getCarModelId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarModelId()."\"");
			if (($defaultId != null) && ($objItem->getCarModelId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
}
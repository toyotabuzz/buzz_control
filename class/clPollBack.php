<?
/*********************************************************
		Class :				PollBack

		Last update :		20 Nov 02

		Description:		Class manage PollBack table

*********************************************************/
 
class PollBack extends DB{

	var $TABLE="t_poll_back";
	
	var $mPollBackId;
	function getPollBackId() { return $this->mPollBackId; }
	function setPollBackId($data) { $this->mPollBackId = $data; }
	
	var $mPollId;
	function getPollId() { return $this->mPollId; }
	function setPollId($data) { $this->mPollId = $data; }

	var $mPollAnswerId;
	function getPollAnswerId() { return $this->mPollAnswerId; }
	function setPollAnswerId($data) { $this->mPollAnswerId = $data; }

	//customer id
	var $mPollUserId;
	function getPollUserId() { return $this->mPollUserId; }
	function setPollUserId($data) { $this->mPollUserId = $data; }
	
	//customer duplicate id
	var $mPollUserId01;
	function getPollUserId01() { return $this->mPollUserId01; }
	function setPollUserId01($data) { $this->mPollUserId01 = $data; }
	
	var $mVal;
	function getVal() { return htmlspecialchars($this->mVal); }
	function setVal($data) { $this->mVal = $data; }

	var $mContent;
	function getContent() { return htmlspecialchars($this->mContent); }
	function setContent($data) { $this->mContent = $data; }

	var $mMoreDetail;
	function getMoreDetail() { return htmlspecialchars($this->mMoreDetail); }
	function setMoreDetail($data) { $this->mMoreDetail = $data; }
	
	//������Ẻ�ͺ���
	//event
	//phone
	var $mType;
	function getType() { return htmlspecialchars($this->mType); }
	function setType($data) { $this->mType = $data; }	

	function PollBack($objData=NULL) {
        If ($objData->pollBackId!="") {
            $this->setPollBackId($objData->pollBackId);
            $this->setPollId($objData->pollId);
            $this->setPollAnswerId($objData->pollAnswerId);
			$this->setPollUserId($objData->pollUserId);
			$this->setPollUserId01($objData->pollUserId01);
			$this->setVal($objData->val);
			$this->setContent($objData->content);
			$this->setMoreDetail($objData->moreDetail);
			$this->setType($objData->type);
        }
    }

	function init(){	
		$this->setContent(stripslashes($this->mContent));
	}
	
	function load() {

		if ($this->mPollBackId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE pollBackId =".$this->mPollBackId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollBack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadByCondition($strCondition) {

		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollBack($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(C.pollBackId) AS RSUM  FROM ".$this->TABLE." C "
					." WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}	

	function loadCount($strCondition) {

		$strSql = "SELECT Count(DISTINCT pollId) as rowCount   FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return $row->rowCount;
            }
        }
		return false;
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( content, pollUserId, pollUserId01, val, pollId, pollAnswerId, moreDetail, type  )"
						." VALUES ( '".$this->mContent."' , "
						."  '".$this->mPollUserId."' , "
						."  '".$this->mPollUserId01."' , "
						."  '".$this->mVal."' , "
						."  '".$this->mPollId."' , "
						."  '".$this->mPollAnswerId."' , "
						."  '".$this->mMoreDetail."' , "
						."  '".$this->mType."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollBackId = mysql_insert_id();
            return $this->mPollBackId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET content = '".$this->mContent."' , "
						." val = '".$this->mVal."' , "
						." pollUserId = '".$this->mPollUserId."' , "
						." pollUserId01 = '".$this->mPollUserId01."' , "
						." moreDetail = '".$this->mMoreDetail."' , "
						." pollAnswerId = '".$this->mPollAnswerId."' ,  "
						." pollId = '".$this->mPollId."'  "
						." WHERE  pollBackId = ".$this->mPollBackId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pollBackId=".$this->mPollBackId." ";
        $this->getConnection();
        $this->query($strSql);

	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
}

/*********************************************************
		Class :				PollBackList

		Last update :		20 Nov 02

		Description:		PollBackuser list

*********************************************************/


class PollBackList extends DataList {
	var $TABLE = "t_poll_back";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pollBackId) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "				
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollBack($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
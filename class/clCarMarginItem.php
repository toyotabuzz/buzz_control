<?
/*********************************************************
		Class :					Car Margin Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_margin_item_item table

*********************************************************/
 
class CarMarginItem extends DB{

	var $TABLE="t_car_margin_item";

	var $mCarMarginItemId;
	function getCarMarginItemId() { return $this->mCarMarginItemId; }
	function setCarMarginItemId($data) { $this->mCarMarginItemId = $data; }
	
	var $mCarSeriesId;
	function getCarSeriesId() { return $this->mCarSeriesId; }
	function setCarSeriesId($data) { $this->mCarSeriesId = $data; }	
	
	var $mCarMarginId;
	function getCarMarginId() { return htmlspecialchars($this->mCarMarginId); }
	function setCarMarginId($data) { $this->mCarMarginId = $data; }
	
	function CarMarginItem($objData=NULL) {
        If ($objData->car_margin_item_id !="") {
            $this->setCarMarginItemId($objData->car_margin_item_id);
			$this->setCarSeriesId($objData->car_series_id);
			$this->setCarMarginId($objData->car_margin_id);
        }
    }

	function init(){
		$this->setCarMarginId(stripslashes($this->mCarMarginId));
		$this->setCarSeriesId(stripslashes($this->mCarSeriesId));
	}
		
	function load() {

		if ($this->mCarMarginItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_margin_item_id =".$this->mCarMarginItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarMarginItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarMarginItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( car_margin_id,car_series_id ) "
						." VALUES ( '".$this->mCarMarginId."' , '".$this->mCarSeriesId."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCarMarginItemId = mysql_insert_id();
            return $this->mCarMarginItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET car_margin_id = '".$this->mCarMarginId."'  "
						." , car_series_id = '".$this->mCarSeriesId."'  "
						." WHERE  car_margin_item_id = ".$this->mCarMarginItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_margin_item_id=".$this->mCarMarginItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCarMarginId == "") $asrErrReturn["car_margin_id"] = "��س��к�";
        Return $asrErrReturn;
    }
	
	
}

/*********************************************************
		Class :				Customer Car Margin List

		Last update :		22 Mar 02

		Description:		Customer Car MarginList

*********************************************************/

class CarMarginItemList extends DataList {
	var $TABLE = "t_car_margin_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.car_margin_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_car_margin C ON C.car_margin_id = P.car_margin_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P "
		." LEFT JOIN t_car_margin C ON C.car_margin_id = P.car_margin_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarMarginItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarMarginItemId()."\"");
			if (($defaultId != null) && ($objItem->getCarMarginItemId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCarMarginId()."</option>");
		}
		echo("</select>");
	}			
	
}
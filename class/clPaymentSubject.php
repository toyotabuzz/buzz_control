<?
/*********************************************************
		Class :					Payment Subject

		Last update :	  10 Jan 02

		Description:	  Class manage t_payment_subject table

*********************************************************/
 
class PaymentSubject extends DB{

	var $TABLE="t_payment_subject";

	var $mPaymentSubjectId;
	function getPaymentSubjectId() { return $this->mPaymentSubjectId; }
	function setPaymentSubjectId($data) { $this->mPaymentSubjectId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mTypeId;
	function getTypeId() { return htmlspecialchars($this->mTypeId); }
	function setTypeId($data) { $this->mTypeId = $data; }	
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	
	
	var $mDiscount;
	function getDiscount() { return htmlspecialchars($this->mDiscount); }
	function setDiscount($data) { $this->mDiscount = $data; }		
	
	function getTypeIdDetail(){
		if($this->mTypeId == 0) return "���ͺö";
		if($this->mTypeId == 1) return "�ͧö";
		if($this->mTypeId == 3) return "����";
		if($this->mTypeId == 4) return "��Сѹ���";
	}	
	
	var $mNohead;
	function getNohead() { return htmlspecialchars($this->mNohead); }
	function setNohead($data) { $this->mNohead = $data; }	
	
	function getNoheadDetail(){
		if($this->mNohead == 0) return "������Ѻ�Թ";
		if($this->mNohead == 1) return "㺡ӡѺ����";
		if($this->mNohead == 2) return "�ء�����";
	}		
	
	function PaymentSubject($objData=NULL) {
        If ($objData->payment_subject_id !="") {
            $this->setPaymentSubjectId($objData->payment_subject_id);
			$this->setTitle($objData->title);
			$this->setTypeId($objData->type_id);
			$this->setNohead($objData->nohead);
			$this->setRank($objData->rank);
			$this->setDiscount($objData->discount);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mPaymentSubjectId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE payment_subject_id =".$this->mPaymentSubjectId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PaymentSubject($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PaymentSubject($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ,type_id , rank, discount, nohead, created_at, created_by) "
						." VALUES ( '".$this->mTitle."' ,  '".$this->mTypeId."' ,  '".$this->mRank."' ,  '".$this->mDiscount."',  '".$this->mNohead."', NOW(), '".$_SESSION['sMemberId']."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mPaymentSubjectId = mysql_insert_id();
            return $this->mPaymentSubjectId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , type_id = '".$this->mTypeId."'  "
						." , rank = '".$this->mRank."'  "
						." , discount = '".$this->mDiscount."'  "
						." , nohead = '".$this->mNohead."'  "
						." WHERE  payment_subject_id = ".$this->mPaymentSubjectId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE payment_subject_id=".$this->mPaymentSubjectId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mTitle == "") $asrErrReturn["type_id"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class PaymentSubjectList extends DataList {
	var $TABLE = "t_payment_subject";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT payment_subject_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PaymentSubject($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPaymentSubjectId()."\"");
			if (($defaultId != null) && ($objItem->getPaymentSubjectId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" id=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPaymentSubjectId()."\"");
			if (($defaultId != null) && ($objItem->getPaymentSubjectId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
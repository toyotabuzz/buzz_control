<?
/*********************************************************
		Class :					Order Price

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_price table

*********************************************************/
 
class OrderPrice extends DB{

	var $TABLE="t_order_price";

	var $mOrderPriceId;
	function getOrderPriceId() { return $this->mOrderPriceId; }
	function setOrderPriceId($data) { $this->mOrderPriceId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }

	var $mOrderExtraId;
	function getOrderExtraId() { return $this->mOrderExtraId; }
	function setOrderExtraId($data) { $this->mOrderExtraId = $data; }
	
	var $mOrderPaymentId;
	function getOrderPaymentId() { return $this->mOrderPaymentId; }
	function setOrderPaymentId($data) { $this->mOrderPaymentId = $data; }
	
	var $mPaymentSubjectId;
	function getPaymentSubjectId() { return $this->mPaymentSubjectId; }
	function setPaymentSubjectId($data) { $this->mPaymentSubjectId = $data; }
	
	var $mPaymentSubjectTitle;
	function getPaymentSubjectTitle() { return $this->mPaymentSubjectTitle; }
	function setPaymentSubjectTitle($data) { $this->mPaymentSubjectTitle = $data; }	

	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mQty;
	function getQty() { return htmlspecialchars($this->mQty); }
	function setQty($data) { $this->mQty = $data; }

	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }	
	
	var $mVat;
	function getVat() { return htmlspecialchars($this->mVat); }
	function setVat($data) { $this->mVat = $data; }		
	
	//0 = �ͧ��������
	//1 = ���ͺ�����	
	//2 = ���ͺ��������
	//3 = ������������
	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }		
	
	var $mPayin;
	// �ͧ������ �Ѻ��� �����͡ �Դź����ͧ
	function getPayin() { return htmlspecialchars($this->mPayin); }
	function setPayin($data) { $this->mPayin = $data; }		
	
	
	
	function OrderPrice($objData=NULL) {
        If ($objData->order_price_id !="") {
            $this->setOrderPriceId($objData->order_price_id);
			$this->setOrderId($objData->order_id);
			$this->setOrderExtraId($objData->order_extra_id);
			$this->setOrderPaymentId($objData->order_payment_id);
			$this->setPaymentSubjectId($objData->payment_subject_id);
			$this->setPaymentSubjectTitle($objData->payment_subject_title);
			$this->setPrice($objData->price);
			$this->setQty($objData->qty);
			$this->setRemark($objData->remark);
			$this->setVat($objData->vat);
			$this->setStatus($objData->status);
			$this->setPayin($objData->payin);
        }
    }

	function init(){
		$this->setPrice(stripslashes($this->mPrice));
	}
		
	function load() {

		if ($this->mOrderPriceId == '') {
			return false;
		}
		$strSql = "SELECT OP.*, PS.title AS payment_subject_title FROM ".$this->TABLE." OP "
		." LEFT JOIN t_payment_subject PS ON PS.payment_subject_id = OP.payment_subject_id "
		."  WHERE order_price_id =".$this->mOrderPriceId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPrice($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPrice($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSum($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SUM(price) AS price FROM ".$this->TABLE." OP "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "
		."  WHERE ".$strCondition;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$nVal = $row->price;
                $result->freeResult();
				return $nVal;
            }
        }
		return false;
	}	
	
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_id, order_extra_id, order_payment_id, payment_subject_id, price, qty, remark, vat, status,payin ) "
						." VALUES ( '".$this->mOrderId."','".$this->mOrderExtraId."','".$this->mOrderPaymentId."','".$this->mPaymentSubjectId."','".$this->mPrice."','".$this->mQty."','".$this->mRemark."','".$this->mVat."','".$this->mStatus."','".$this->mPayin."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderPriceId = mysql_insert_id();
            return $this->mOrderPriceId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_id = '".$this->mOrderId."'  "
						." , order_extra_id = '".$this->mOrderExtraId."'  "
						." , order_payment_id = '".$this->mOrderPaymentId."'  "
						." , payment_subject_id = '".$this->mPaymentSubjectId."'  "
						." , price = '".$this->mPrice."'  "
						." , qty = '".$this->mQty."'  "
						." , remark = '".$this->mRemark."'  "
						." , payin = '".$this->mPayin."'  "
						." , vat = '".$this->mVat."'  "
						." , status = '".$this->mStatus."'  "
						." WHERE  order_price_id = ".$this->mOrderPriceId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_price_id=".$this->mOrderPriceId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_order_price_payment  "
                . " WHERE order_price_id=".$this->mOrderPriceId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
		
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Price List

		Last update :		22 Mar 02

		Description:		Order Price List

*********************************************************/

class OrderPriceList extends DataList {
	var $TABLE = "t_order_price";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_price_id) as rowCount FROM ".$this->TABLE." OP "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "		
		." LEFT JOIN t_payment_subject PS ON PS.payment_subject_id = OP.payment_subject_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.*, PS.title AS payment_subject_title FROM ".$this->TABLE." OP "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "
		." LEFT JOIN t_payment_subject PS ON PS.payment_subject_id = OP.payment_subject_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderPrice($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderPriceId()."\"");
			if (($defaultId != null) && ($objItem->getOrderPriceId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCustomerId()."</option>");
		}
		echo("</select>");
	}

}
<?
/*********************************************************
		Class :					Reply

		Last update :	  10 Jan 02

		Description:	  Class manage t_reply table

*********************************************************/
 
class Reply extends DB{

	var $TABLE="t_msg_reply";

	var $m_reply_id;
	function get_reply_id() { return $this->m_reply_id; }
	function set_reply_id($data) { $this->m_reply_id = $data; }
	
	var $m_msg_id;
	function get_msg_id() { return $this->m_msg_id; }
	function set_msg_id($data) { $this->m_msg_id = $data; }
	
	var $m_reply_date;
	function get_reply_date() { return $this->m_reply_date; }
	function set_reply_date($data) { $this->m_reply_date = $data; }
	
	var $m_content;
	function get_content() { return stripslashes($this->m_content); }
	function set_content($data) { $this->m_content = $data; }
	
	var $m_answer;
	function get_answer() { return stripslashes($this->m_answer); }
	function set_answer($data) { $this->m_answer = $data; }
	
	var $m_reply_by;
	function get_reply_by() { return $this->m_reply_by; }
	function set_reply_by($data) { $this->m_reply_by = $data; }
	
	var $m_fixed;
	function get_fixed() { return $this->m_fixed; }
	function set_fixed($data) { $this->m_fixed = $data; }
	
	var $m_url;
	function get_url() { return $this->m_url; }
	function set_url($data) { $this->m_url = $data; }
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }

	
	function Reply($objData=NULL) {
        If ($objData->reply_id !="") {
			$this->set_reply_id($objData->reply_id);
			$this->set_msg_id($objData->msg_id);			
			$this->set_reply_date($objData->reply_date);
			$this->set_content($objData->content);
			$this->set_answer($objData->answer);			
			$this->set_reply_by($objData->reply_by);
			$this->set_fixed($objData->fixed);
			$this->set_url($objData->url);
			$this->set_status($objData->status);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_reply_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE reply_id =".$this->m_reply_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Reply($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Reply($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( reply_date, msg_id, content, reply_by, fixed, url , status ) " ." VALUES ( "
		
		." '".date("Y-m-d H:i:s")."' , "
		." '".$this->m_msg_id."' , "
		." '".addslashes($this->m_content)."' , "
		." '".$this->m_reply_by."' , "
		." '".$this->m_fixed."' , "
		." '".$this->m_url."' , "
		." '".$this->m_status."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_reply_id = mysql_insert_id();
            return $this->m_reply_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  content = '".addslashes($this->m_content)."' "
		." , url = '".$this->m_url."' "
		." , fixed = '".$this->m_fixed."' "
		." , status = '".$this->m_status."' "
		." WHERE reply_id = ".$this->m_reply_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function updateFixed(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		." fixed = '".$this->m_fixed."' , "
		." answer = '".addslashes($this->m_answer)."' "
		." , status = '".$this->m_status."' "
		." WHERE reply_id = ".$this->m_reply_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE reply_id=".$this->m_reply_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserReplyor;
        $Mode = StrToLower($Mode);
        Return $asrReplyReturn;
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class ReplyList extends DataList {
	var $TABLE = "t_msg_reply";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT reply_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_member M ON M.member_id = P.reply_by "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_member M ON M.member_id = P.reply_by "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Reply($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
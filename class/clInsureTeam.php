<?
/*********************************************************
		Class :					InsureTeam

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_team table

*********************************************************/
 
class InsureTeam extends DB{

	var $TABLE="t_insure_team";

	var $mInsureTeamId;
	function getInsureTeamId() { return $this->mInsureTeamId; }
	function setInsureTeamId($data) { $this->mInsureTeamId = $data; }
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mCompanyId;
	function getCompanyId() { return htmlspecialchars($this->mCompanyId); }
	function setCompanyId($data) { $this->mCompanyId = $data; }	
	
	var $mAccCompanyId;
	function getAccCompanyId() { return htmlspecialchars($this->mAccCompanyId); }
	function setAccCompanyId($data) { $this->mAccCompanyId = $data; }		
	
	var $mCompanyDetail;
	function getCompanyDetail() { return htmlspecialchars($this->mCompanyDetail); }
	function setCompanyDetail($data) { $this->mCompanyDetail = $data; }		
	
	function InsureTeam($objData=NULL) {
        If ($objData->team_id !="") {
            $this->setInsureTeamId($objData->team_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setCompanyId($objData->company_id);
			$this->setAccCompanyId($objData->acc_company_id);
			$this->setCompanyDetail($objData->company_detail);
        }
    }

	function init(){	
		$this->setCode(stripslashes($this->mCode));
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mInsureTeamId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE team_id =".$this->mInsureTeamId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTeam($row);
                $result->freeResult();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureTeam($row);
                $result->freeResult();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( code, title, company_id ) "
						." VALUES ( '".$this->mCode."' , "
						."  '".trim($this->mTitle)."' , '".$this->mCompanyId."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mInsureTeamId = mysql_insert_id();
			$this->unsetConnection();
            return $this->mInsureTeamId;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET code = '".$this->mCode."' , "
						." title = '".trim($this->mTitle)."' , "
						." company_id = '".$this->mCompanyId."'  "
						." WHERE  team_id = ".$this->mInsureTeamId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateAcc(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  acc_company_id = '".$this->mAccCompanyId."'  "
						." WHERE  team_id = ".$this->mInsureTeamId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE team_id=".$this->mInsureTeamId." ";
        $this->getConnection();
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

	
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��ͷ��";
		
        Return $asrErrReturn;
    }
	
   function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT code  FROM t_insure_team ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE company_id=".$this->mCompanyId." and  code = ".trim(strtoupper($this->convStr4SQL($this->mCode)));
		}else{
			$strSql .= " WHERE company_id=".$this->mCompanyId." and  code = ".trim(strtoupper($this->convStr4SQL($this->mCode)))." and team_id != ".$this->mInsureTeamId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
}

/*********************************************************
		Class :				InsureTeamList

		Last update :		22 Mar 02

		Description:		InsureTeam user list

*********************************************************/


class InsureTeamList extends DataList {
	var $TABLE = "t_insure_team";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT team_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_company C ON C.company_id= P.company_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, C.title AS company_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_company C ON C.company_id= P.company_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureTeam($row);
			}
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT team_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_company C ON C.company_id= P.company_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, C.title AS company_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_company C ON C.company_id= P.company_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureTeam($row);
			}
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInsureTeamId()."\"");
			if (($defaultId != null) && ($objItem->getInsureTeamId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}
	
	
}
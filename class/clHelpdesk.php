<?
/*********************************************************
		Class :					Helpdesk

		Last update :	  10 Jan 02

		Description:	  Class manage t_helpdesk table

*********************************************************/
 
class Helpdesk extends DB{

	var $TABLE="t_helpdesk";

	var $mHelpdeskId;
	function getHelpdeskId() { return $this->mHelpdeskId; }
	function setHelpdeskId($data) { $this->mHelpdeskId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }
	
	var $mShow;
	function getShow() { return htmlspecialchars($this->mShow); }
	function setShow($data) { $this->mShow = $data; }
	
	function Helpdesk($objData=NULL) {
        If ($objData->helpdesk_id !="") {
            $this->setHelpdeskId($objData->helpdesk_id);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
			$this->setShow($objData->show);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mHelpdeskId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE helpdesk_id =".$this->mHelpdeskId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Helpdesk($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Helpdesk($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, rank, show ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mRank."' , '".$this->mShow."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mHelpdeskId = mysql_insert_id();
            return $this->mHelpdeskId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , rank= '".$this->mRank."'  "
						." , show = '".$this->mShow."'  "
						." WHERE  helpdesk_id = ".$this->mHelpdeskId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE helpdesk_id=".$this->mHelpdeskId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���Ǣ��";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer HelpdeskTopic List

		Last update :		22 Mar 02

		Description:		Customer HelpdeskTopic List

*********************************************************/

class HelpdeskList extends DataList {
	var $TABLE = "t_helpdesk";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT helpdesk_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Helpdesk($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getHelpdeskId()."\"");
			if (($defaultId != null) && ($objItem->getHelpdeskId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
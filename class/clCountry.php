<?
/*********************************************************
		Class :				Country

		Last update :		17 Nov 02

		Description:		Class manage icon table

*********************************************************/
 
class Country extends DB{

	var $TABLE="t_country";
	
	var $mCountryCode;
	function getCountryCode() { return $this->mCountryCode; }
	function setCountryCode($data) { $this->mCountryCode = $data; }

	var $mCountryName;
	function getCountryName() { return htmlspecialchars($this->mCountryName); }
	function setCountryName($data) { $this->mCountryName = $data; }

	function Country($objData=NULL) {
        If ($objData->countryCode!="") {
            $this->setCountryCode($objData->countryCode);
            $this->setCountryName($objData->countryName);

        }
    }
	
	function load() {

		if ($this->mCountryCode == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE countryCode = '".$this->mCountryCode."'";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Country($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

}

/*********************************************************
		Class :				CountryList

		Last update :		22 Mar 02

		Description:		Country user list

*********************************************************/


class CountryList extends DataList {
	var $TABLE = "t_country";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT countryCode) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT *  FROM t_country  ";
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Country($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($countryName, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$countryName."\">\n");
		if ($header == "search") {echo ("<option value=\"0\">- any country -</option>");};
		if ($header == "Add" || $header == "Update" || $header == "") {echo ("<option value=\"0\">- select country -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCountryCode()."\"");
			if (($defaultId != null) && ($objItem->getCountryCode() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCountryName()."</option>");
		}
		echo("</select>");
	}
}
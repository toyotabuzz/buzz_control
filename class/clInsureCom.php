<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureCom extends DB{

	var $TABLE="t_insure_com";

	var $m_insure_com_id;
	function get_insure_com_id() { return $this->m_insure_com_id; }
	function set_insure_com_id($data) { $this->m_insure_com_id = $data; }
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_insure_type_id;
	function get_insure_type_id() { return $this->m_insure_type_id; }
	function set_insure_type_id($data) { $this->m_insure_type_id = $data; }
	
	var $m_com_type;
	function get_com_type() { return $this->m_com_type; }
	function set_com_type($data) { $this->m_com_type = $data; }	
	
	var $m_comp;
	function get_comp() { return $this->m_comp; }
	function set_comp($data) { $this->m_comp = $data; }
	
	var $m_comk;
	function get_comk() { return $this->m_comk; }
	function set_comk($data) { $this->m_comk = $data; }

	var $m_comp1;
	function get_comp1() { return $this->m_comp1; }
	function set_comp1($data) { $this->m_comp1 = $data; }
	
	var $m_comk1;
	function get_comk1() { return $this->m_comk1; }
	function set_comk1($data) { $this->m_comk1 = $data; }	
	
	var $m_comp2;
	function get_comp2() { return $this->m_comp2; }
	function set_comp2($data) { $this->m_comp2 = $data; }
	
	var $m_comk2;
	function get_comk2() { return $this->m_comk2; }
	function set_comk2($data) { $this->m_comk2 = $data; }	
	
	var $m_start_date;
	function get_start_date() { return $this->m_start_date; }
	function set_start_date($data) { $this->m_start_date = $data; }
	
	var $m_end_date;
	function get_end_date() { return $this->m_end_date; }
	function set_end_date($data) { $this->m_end_date = $data; }		
	

	function InsureCom($objData=NULL) {
        If ($objData->insure_com_id !="") {
			$this->set_insure_com_id($objData->insure_com_id);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_insure_type_id($objData->insure_type_id);
			$this->set_com_type($objData->com_type);
			$this->set_comp($objData->comp);
			$this->set_comk($objData->comk);
			$this->set_comp1($objData->comp1);
			$this->set_comk1($objData->comk1);
			$this->set_comp2($objData->comp2);
			$this->set_comk2($objData->comk2);
			$this->set_start_date($objData->start_date);
			$this->set_end_date($objData->end_date);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_com_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_com_id =".$this->m_insure_com_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCom($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCom($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( broker_id , insure_company_id , insure_type_id , com_type, start_date, end_date, comp , comk, comp1, comk1, comp2, comk2 ) " ." VALUES ( "
		." '".$this->m_broker_id."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_insure_type_id."' , "
		." '".$this->m_com_type."' , "
		." '".$this->m_start_date."' , "
		." '".$this->m_end_date."' , "
		." '".$this->m_comp."' , "
		." '".$this->m_comk."' , "
		." '".$this->m_comp1."' , "
		." '".$this->m_comk1."' , "
		." '".$this->m_comp2."' , "
		." '".$this->m_comk2."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_com_id = mysql_insert_id();
            return $this->m_insure_com_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." broker_id = '".$this->m_broker_id."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , insure_type_id = '".$this->m_insure_type_id."' "
		." , com_type = '".$this->m_com_type."' "
		." , start_date = '".$this->m_start_date."' "
		." , end_date = '".$this->m_end_date."' "
		." , comp = '".$this->m_comp."' "
		." , comk = '".$this->m_comk."' "
		." , comp1 = '".$this->m_comp1."' "
		." , comk1 = '".$this->m_comk1."' "
		." , comp2 = '".$this->m_comp2."' "
		." , comk2 = '".$this->m_comk2."' "
		." WHERE insure_com_id = ".$this->m_insure_com_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_com_id=".$this->m_insure_com_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureComList extends DataList {
	var $TABLE = "t_insure_com";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_com_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCom($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
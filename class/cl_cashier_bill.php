<?php
/*********************************************************
		Class :				CashierBill

		Last update :		16 NOV 02

		Description:		Class manage category table

*********************************************************/
class CashierBill extends DB{

	var $TABLE="t_cashier_bill";
	
	var $m_cashier_bill_id;
	function get_cashier_bill_id() { return $this->m_cashier_bill_id; }
	function set_cashier_bill_id($data) { $this->m_cashier_bill_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }
	
	function CashierBill($objData=NULL) {
        If (isset($objData)) {
			$this->set_cashier_bill_id($objData->cashier_bill_id);
			$this->set_title($objData->title);
			$this->set_company_id($objData->company_id);
        }
    }
	
	function init(){
		$this->set_title(stripslashes($this->m_title));
	}
	
	function load() {

		if ($this->m_cashier_bill_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE cashier_bill_id =".$this->m_cashier_bill_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierBill($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strSql) {

		if ($strSql == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE ".$strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierBill($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}		

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( title , company_id  ) " ." VALUES ( "
		." '".$this->m_title."' , "
		." '".$this->m_company_id."'  "
		." ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->m_cashier_bill_id = mysql_insert_id();
            return $this->m_cashier_bill_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." title = '".$this->m_title."' "
		." , company_id = '".$this->m_company_id."' "
		." WHERE cashier_bill_id = ".$this->m_cashier_bill_id." ";
        $this->getConnection();
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE cashier_bill_id=".$this->m_cashier_bill_id." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {

    }
	
	Function checkDelete()
    {
        return true;
    }

	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}


}

/*********************************************************
		Class :				CashierBillList

		Last update :		22 Mar 02

		Description:		CashierBill user list

*********************************************************/


class CashierBillList extends DataList {
	var $TABLE = "t_cashier_bill";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT cashier_bill_id) as rowCount FROM ".$this->TABLE
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * "
			."FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CashierBill($row);
			}
			return true;
		} else {
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		echo ("<option value=\"0\">$header</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_cashier_bill_id()."\"");
			if (($defaultId != null) && ($objItem->get_cashier_bill_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}

}
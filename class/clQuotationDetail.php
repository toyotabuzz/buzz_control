<?
/*********************************************************
		Class :			  QuotationDetail

		Last update :	  11 Mar 21

		Description:	  Class manage jmquotation_detail table

*********************************************************/
 
class QuotationDetail extends DB{

	var $TABLE="jmquotation_detail";

	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_quotation_id;
	function get_quotation_id() { return $this->m_quotation_id; }
	function set_quotation_id($data) { $this->m_quotation_id = $data; }

	var $m_code_id;
	function get_code_id() { return $this->m_code_id; }
	function set_code_id($data) { $this->m_code_id = $data; }
	
	var $m_value;
	function get_value() { return $this->m_value; }
	function set_value($data) { $this->m_value = $data; }
	
	var $m_description;
	function get_description() { return $this->m_description; }
	function set_description($data) { $this->m_description = $data; }

	var $m_isactive;
	function get_isactive() { return $this->m_isactive; }
	function set_isactive($data) { $this->m_isactive = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }
    
    function QuotationDetail($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_quotation_id($objData->quotation_id);
			$this->set_code_id($objData->code_id);
			$this->set_value($objData->value);
			$this->set_description($objData->description);
			$this->set_isactive($objData->isactive);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id." AND isactive = 'Y'";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->QuotationDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->QuotationDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_id , code_id , value , description , isactive , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_quotation_id."' , "
		." '".$this->m_code_id."' , "
		." '".$this->m_value."' , "
		." '".$this->m_description."' , "
		." '".$this->m_isactive."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." quotation_id = '".$this->m_quotation_id."' "
		." , code_id = '".$this->m_code_id."' "
		." , value = '".$this->m_value."' "
		." , description = '".$this->m_description."' "
		." , isactive = '".$this->m_isactive."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :			  Insure Company List

		Last update :	  11 Mar 21

		Description:	  Insure Company List

*********************************************************/
<?
/*********************************************************
		Class :				Customer
		Last update :		9 Jan 07
		Description:		Class manage t_customer  table
*********************************************************/
 
class CustomerCCard extends DB{

	var $TABLE="t_customer_ccard";
	
	var $mCCardId;
	function getCCardId() { return $this->mCCardId; }
	function setCCardId($data) { $this->mCCardId = $data; }
	
	var $mT01;
	function getT01() { return $this->mT01; }
	function setT01($data) { $this->mT01 = $data; }
	
	var $mT02;
	function getT02() { return $this->mT02; }
	function setT02($data) { $this->mT02 = $data; }
	
	var $mT03;
	function getT03() { return $this->mT03; }
	function setT03($data) { $this->mT03 = $data; }
	
	var $mT04;
	function getT04() { return $this->mT04; }
	function setT04($data) { $this->mT04 = $data; }
	
	var $mT05;
	function getT05() { return $this->mT05; }
	function setT05($data) { $this->mT05 = $data; }
	
	var $mT06;
	function getT06() { return $this->mT06; }
	function setT06($data) { $this->mT06 = $data; }
	
	var $mT07;
	function getT07() { return $this->mT07; }
	function setT07($data) { $this->mT07 = $data; }
	
	var $mT08;
	function getT08() { return $this->mT08; }
	function setT08($data) { $this->mT08 = $data; }
	
	var $mT09;
	function getT09() { return $this->mT09; }
	function setT09($data) { $this->mT09 = $data; }
	
	var $mT10;
	function getT10() { return $this->mT10; }
	function setT10($data) { $this->mT10 = $data; }
	
	var $mT11;
	function getT11() { return $this->mT11; }
	function setT11($data) { $this->mT11 = $data; }
	
	var $mT12;
	function getT12() { return $this->mT12; }
	function setT12($data) { $this->mT12 = $data; }
	
	var $mT13;
	function getT13() { return $this->mT13; }
	function setT13($data) { $this->mT13 = $data; }
	
	var $mT14;
	function getT14() { return $this->mT14; }
	function setT14($data) { $this->mT14 = $data; }
	
	var $mT15;
	function getT15() { return $this->mT15; }
	function setT15($data) { $this->mT15 = $data; }
	
	var $mT16;
	function getT16() { return $this->mT16; }
	function setT16($data) { $this->mT16 = $data; }
	
	var $mT17;
	function getT17() { return $this->mT17; }
	function setT17($data) { $this->mT17 = $data; }
	
	var $mT18;
	function getT18() { return $this->mT18; }
	function setT18($data) { $this->mT18 = $data; }
	
	var $mT19;
	function getT19() { return $this->mT19; }
	function setT19($data) { $this->mT19 = $data; }
	
	var $mT20;
	function getT20() { return $this->mT20; }
	function setT20($data) { $this->mT20 = $data; }
	
	var $mT21;
	function getT21() { return $this->mT21; }
	function setT21($data) { $this->mT21 = $data; }
	
	var $mT22;
	function getT22() { return $this->mT22; }
	function setT22($data) { $this->mT22 = $data; }
	
	var $mT23;
	function getT23() { return $this->mT23; }
	function setT23($data) { $this->mT23 = $data; }
	
	var $mT24;
	function getT24() { return $this->mT24; }
	function setT24($data) { $this->mT24 = $data; }
	
	var $mT25;
	function getT25() { return $this->mT25; }
	function setT25($data) { $this->mT25 = $data; }
	
	var $mT26;
	function getT26() { return $this->mT26; }
	function setT26($data) { $this->mT26 = $data; }
	
	var $mT27;
	function getT27() { return $this->mT27; }
	function setT27($data) { $this->mT27 = $data; }
	
	var $mT28;
	function getT28() { return $this->mT28; }
	function setT28($data) { $this->mT28 = $data; }
	
	var $mT29;
	function getT29() { return $this->mT29; }
	function setT29($data) { $this->mT29 = $data; }
	
	var $mT30;
	function getT30() { return $this->mT30; }
	function setT30($data) { $this->mT30 = $data; }
	
	var $mT31;
	function getT31() { return $this->mT31; }
	function setT31($data) { $this->mT31 = $data; }
	
	var $mT32;
	function getT32() { return $this->mT32; }
	function setT32($data) { $this->mT32 = $data; }
	
	var $mT33;
	function getT33() { return $this->mT33; }
	function setT33($data) { $this->mT33 = $data; }
	
	var $mT34;
	function getT34() { return $this->mT34; }
	function setT34($data) { $this->mT34 = $data; }
	
	var $mT35;
	function getT35() { return $this->mT35; }
	function setT35($data) { $this->mT35 = $data; }
	
	var $mT36;
	function getT36() { return $this->mT36; }
	function setT36($data) { $this->mT36 = $data; }
	
	var $mT37;
	function getT37() { return $this->mT37; }
	function setT37($data) { $this->mT37 = $data; }
	
	var $mT38;
	function getT38() { return $this->mT38; }
	function setT38($data) { $this->mT38 = $data; }
	
	var $mT39;
	function getT39() { return $this->mT39; }
	function setT39($data) { $this->mT39 = $data; }
	
	var $mT40;
	function getT40() { return $this->mT40; }
	function setT40($data) { $this->mT40 = $data; }
	
	var $mT41;
	function getT41() { return $this->mT41; }
	function setT41($data) { $this->mT41 = $data; }
	
	var $mT42;
	function getT42() { return $this->mT42; }
	function setT42($data) { $this->mT42 = $data; }
	
	var $mT43;
	function getT43() { return $this->mT43; }
	function setT43($data) { $this->mT43 = $data; }
	
	var $mT44;
	function getT44() { return $this->mT44; }
	function setT44($data) { $this->mT44 = $data; }
	
	var $mT45;
	function getT45() { return $this->mT45; }
	function setT45($data) { $this->mT45 = $data; }
	
	var $mT46;
	function getT46() { return $this->mT46; }
	function setT46($data) { $this->mT46 = $data; }
	
	var $mT47;
	function getT47() { return $this->mT47; }
	function setT47($data) { $this->mT47 = $data; }
	
	var $mT48;
	function getT48() { return $this->mT48; }
	function setT48($data) { $this->mT48 = $data; }
	
	var $mT49;
	function getT49() { return $this->mT49; }
	function setT49($data) { $this->mT49 = $data; }
	
	var $mT50;
	function getT50() { return $this->mT50; }
	function setT50($data) { $this->mT50 = $data; }
	
	var $mT51;
	function getT51() { return $this->mT51; }
	function setT51($data) { $this->mT51 = $data; }
	
	var $mT52;
	function getT52() { return $this->mT52; }
	function setT52($data) { $this->mT52 = $data; }
	
	var $mT53;
	function getT53() { return $this->mT53; }
	function setT53($data) { $this->mT53 = $data; }
	
	var $mT54;
	function getT54() { return $this->mT54; }
	function setT54($data) { $this->mT54 = $data; }
	
	var $mT55;
	function getT55() { return $this->mT55; }
	function setT55($data) { $this->mT55 = $data; }
	
	var $mT56;
	function getT56() { return $this->mT56; }
	function setT56($data) { $this->mT56 = $data; }
	
	var $mT57;
	function getT57() { return $this->mT57; }
	function setT57($data) { $this->mT57 = $data; }
	
	var $mT58;
	function getT58() { return $this->mT58; }
	function setT58($data) { $this->mT58 = $data; }
	
	var $mT59;
	function getT59() { return $this->mT59; }
	function setT59($data) { $this->mT59 = $data; }
	
	var $mT60;
	function getT60() { return $this->mT60; }
	function setT60($data) { $this->mT60 = $data; }
	
	var $mT61;
	function getT61() { return $this->mT61; }
	function setT61($data) { $this->mT61 = $data; }
	
	var $mT62;
	function getT62() { return $this->mT62; }
	function setT62($data) { $this->mT62 = $data; }
	
	var $mT63;
	function getT63() { return $this->mT63; }
	function setT63($data) { $this->mT63 = $data; }
	
	var $mT64;
	function getT64() { return $this->mT64; }
	function setT64($data) { $this->mT64 = $data; }
	
	var $mT65;
	function getT65() { return $this->mT65; }
	function setT65($data) { $this->mT65 = $data; }
	
	var $mT66;
	function getT66() { return $this->mT66; }
	function setT66($data) { $this->mT66 = $data; }
	
	var $mT67;
	function getT67() { return $this->mT67; }
	function setT67($data) { $this->mT67 = $data; }
	
	var $mT68;
	function getT68() { return $this->mT68; }
	function setT68($data) { $this->mT68 = $data; }
	
	var $mT69;
	function getT69() { return $this->mT69; }
	function setT69($data) { $this->mT69 = $data; }
	
	var $mT70;
	function getT70() { return $this->mT70; }
	function setT70($data) { $this->mT70 = $data; }
	
	var $mT71;
	function getT71() { return $this->mT71; }
	function setT71($data) { $this->mT71 = $data; }
	
	var $mT72;
	function getT72() { return $this->mT72; }
	function setT72($data) { $this->mT72 = $data; }
	
	var $mT73;
	function getT73() { return $this->mT73; }
	function setT73($data) { $this->mT73 = $data; }
	
	var $mT74;
	function getT74() { return $this->mT74; }
	function setT74($data) { $this->mT74 = $data; }
	
	var $mT75;
	function getT75() { return $this->mT75; }
	function setT75($data) { $this->mT75 = $data; }
	
	var $mT76;
	function getT76() { return $this->mT76; }
	function setT76($data) { $this->mT76 = $data; }
	
	var $mT77;
	function getT77() { return $this->mT77; }
	function setT77($data) { $this->mT77 = $data; }
	
	var $mT78;
	function getT78() { return $this->mT78; }
	function setT78($data) { $this->mT78 = $data; }
	
	var $mT79;
	function getT79() { return $this->mT79; }
	function setT79($data) { $this->mT79 = $data; }
	
	var $mT80;
	function getT80() { return $this->mT80; }
	function setT80($data) { $this->mT80 = $data; }
	
	var $mT81;
	function getT81() { return $this->mT81; }
	function setT81($data) { $this->mT81 = $data; }
	
	var $mT82;
	function getT82() { return $this->mT82; }
	function setT82($data) { $this->mT82 = $data; }
	
	var $mT83;
	function getT83() { return $this->mT83; }
	function setT83($data) { $this->mT83 = $data; }
	
	var $mT84;
	function getT84() { return $this->mT84; }
	function setT84($data) { $this->mT84 = $data; }
	
	var $mT85;
	function getT85() { return $this->mT85; }
	function setT85($data) { $this->mT85 = $data; }
	
	var $mT86;
	function getT86() { return $this->mT86; }
	function setT86($data) { $this->mT86 = $data; }
	
	var $mT87;
	function getT87() { return $this->mT87; }
	function setT87($data) { $this->mT87 = $data; }
	
	var $mCheckStatus;
	function getCheckStatus() { return $this->mCheckStatus; }
	function setCheckStatus($data) { $this->mCheckStatus = $data; }	
	
	function CustomerCCard($objData=NULL) {
        If ($objData->ccard_id !="") {
		
			$this->setCCardId($objData->ccard_id);
			$this->setT01($objData->T01);
			$this->setT02($objData->T02);
			$this->setT03($objData->T03);
			$this->setT04($objData->T04);
			$this->setT05($objData->T05);
			$this->setT06($objData->T06);
			$this->setT07($objData->T07);
			$this->setT08($objData->T08);
			$this->setT09($objData->T09);
			$this->setT10($objData->T10);
			$this->setT11($objData->T11);
			$this->setT12($objData->T12);
			$this->setT13($objData->T13);
			$this->setT14($objData->T14);
			$this->setT15($objData->T15);
			$this->setT16($objData->T16);
			$this->setT17($objData->T17);
			$this->setT18($objData->T18);
			$this->setT19($objData->T19);
			$this->setT20($objData->T20);
			$this->setT21($objData->T21);
			$this->setT22($objData->T22);
			$this->setT23($objData->T23);
			$this->setT24($objData->T24);
			$this->setT25($objData->T25);
			$this->setT26($objData->T26);
			$this->setT27($objData->T27);
			$this->setT28($objData->T28);
			$this->setT29($objData->T29);
			$this->setT30($objData->T30);
			$this->setT31($objData->T31);
			$this->setT32($objData->T32);
			$this->setT33($objData->T33);
			$this->setT34($objData->T34);
			$this->setT35($objData->T35);
			$this->setT36($objData->T36);
			$this->setT37($objData->T37);
			$this->setT38($objData->T38);
			$this->setT39($objData->T39);
			$this->setT40($objData->T40);
			$this->setT41($objData->T41);
			$this->setT42($objData->T42);
			$this->setT43($objData->T43);
			$this->setT44($objData->T44);
			$this->setT45($objData->T45);
			$this->setT46($objData->T46);
			$this->setT47($objData->T47);
			$this->setT48($objData->T48);
			$this->setT49($objData->T49);
			$this->setT50($objData->T50);
			$this->setT51($objData->T51);
			$this->setT52($objData->T52);
			$this->setT53($objData->T53);
			$this->setT54($objData->T54);
			$this->setT55($objData->T55);
			$this->setT56($objData->T56);
			$this->setT57($objData->T57);
			$this->setT58($objData->T58);
			$this->setT59($objData->T59);
			$this->setT60($objData->T60);
			$this->setT61($objData->T61);
			$this->setT62($objData->T62);
			$this->setT63($objData->T63);
			$this->setT64($objData->T64);
			$this->setT65($objData->T65);
			$this->setT66($objData->T66);
			$this->setT67($objData->T67);
			$this->setT68($objData->T68);
			$this->setT69($objData->T69);
			$this->setT70($objData->T70);
			$this->setT71($objData->T71);
			$this->setT72($objData->T72);
			$this->setT73($objData->T73);
			$this->setT74($objData->T74);
			$this->setT75($objData->T75);
			$this->setT76($objData->T76);
			$this->setT77($objData->T77);
			$this->setT78($objData->T78);
			$this->setT79($objData->T79);
			$this->setT80($objData->T80);
			$this->setT81($objData->T81);
			$this->setT82($objData->T82);
			$this->setT83($objData->T83);
			$this->setT84($objData->T84);
			$this->setT85($objData->T85);
			$this->setT86($objData->T86);
			$this->setT87($objData->T87);
			$this->setCheckStatus($objData->check_status);
        }
    }

	function init(){
		$this->setCCardId(stripslashes($this->mCCardId));
	}
	
	function load() {

		if ($this->mCCardId == '') {
			return false;
		}
		$strSql = "SELECT C.* FROM ".$this->TABLE." C "
					." WHERE ccard_id = ".$this->mCCardId;

		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->CustomerCCard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( CCardId,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,T21,T22,T23,T24,T25,T26,T27,T28,T29,T30,T31,T32,T33,T34,T35,T36,T37,T38,T39,T40,T41,T42,T43,T44,T45,T46,T47,T48,T49,T50,T51,T52,T53,T54,T55,T56,T57,T58,T59,T60,T61,T62,T63,T64,T65,T66,T67,T68,T69,T70,T71,T72,T73,T74,T75,T76,T77,T78,T79,T80,T81,T82,T83,T84,T85,T86,T87 ) " 
		." VALUES ( '".$this->mCCardId."' , "
		." '".$this->mT01."' , "
		." '".$this->mT02."' , "
		." '".$this->mT03."' , "
		." '".$this->mT04."' , "
		." '".$this->mT05."' , "
		." '".$this->mT06."' , "
		." '".$this->mT07."' , "
		." '".$this->mT08."' , "
		." '".$this->mT09."' , "
		." '".$this->mT10."' , "
		." '".$this->mT11."' , "
		." '".$this->mT12."' , "
		." '".$this->mT13."' , "
		." '".$this->mT14."' , "
		." '".$this->mT15."' , "
		." '".$this->mT16."' , "
		." '".$this->mT17."' , "
		." '".$this->mT18."' , "
		." '".$this->mT19."' , "
		." '".$this->mT20."' , "
		." '".$this->mT21."' , "
		." '".$this->mT22."' , "
		." '".$this->mT23."' , "
		." '".$this->mT24."' , "
		." '".$this->mT25."' , "
		." '".$this->mT26."' , "
		." '".$this->mT27."' , "
		." '".$this->mT28."' , "
		." '".$this->mT29."' , "
		." '".$this->mT30."' , "
		." '".$this->mT31."' , "
		." '".$this->mT32."' , "
		." '".$this->mT33."' , "
		." '".$this->mT34."' , "
		." '".$this->mT35."' , "
		." '".$this->mT36."' , "
		." '".$this->mT37."' , "
		." '".$this->mT38."' , "
		." '".$this->mT39."' , "
		." '".$this->mT40."' , "
		." '".$this->mT41."' , "
		." '".$this->mT42."' , "
		." '".$this->mT43."' , "
		." '".$this->mT44."' , "
		." '".$this->mT45."' , "
		." '".$this->mT46."' , "
		." '".$this->mT47."' , "
		." '".$this->mT48."' , "
		." '".$this->mT49."' , "
		." '".$this->mT50."' , "
		." '".$this->mT51."' , "
		." '".$this->mT52."' , "
		." '".$this->mT53."' , "
		." '".$this->mT54."' , "
		." '".$this->mT55."' , "
		." '".$this->mT56."' , "
		." '".$this->mT57."' , "
		." '".$this->mT58."' , "
		." '".$this->mT59."' , "
		." '".$this->mT60."' , "
		." '".$this->mT61."' , "
		." '".$this->mT62."' , "
		." '".$this->mT63."' , "
		." '".$this->mT64."' , "
		." '".$this->mT65."' , "
		." '".$this->mT66."' , "
		." '".$this->mT67."' , "
		." '".$this->mT68."' , "
		." '".$this->mT69."' , "
		." '".$this->mT70."' , "
		." '".$this->mT71."' , "
		." '".$this->mT72."' , "
		." '".$this->mT73."' , "
		." '".$this->mT74."' , "
		." '".$this->mT75."' , "
		." '".$this->mT76."' , "
		." '".$this->mT77."' , "
		." '".$this->mT78."' , "
		." '".$this->mT79."' , "
		." '".$this->mT80."' , "
		." '".$this->mT81."' , "
		." '".$this->mT82."' , "
		." '".$this->mT83."' , "
		." '".$this->mT84."' , "
		." '".$this->mT85."' , "
		." '".$this->mT86."' , "
		." '".$this->mT87."' ) ";

						
		//echo $strSql;
        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCCardId = mysql_insert_id();
            return $this->mCCardId;
        } else {
			return false;
	    }
	}


	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET  T01 = '".$this->mT01."' "
		." , T02 = '".$this->mT02."' "
		." , T03 = '".$this->mT03."' "
		." , T04 = '".$this->mT04."' "
		." , T05 = '".$this->mT05."' "
		." , T06 = '".$this->mT06."' "
		." , T07 = '".$this->mT07."' "
		." , T08 = '".$this->mT08."' "
		." , T09 = '".$this->mT09."' "
		." , T10 = '".$this->mT10."' "
		." , T11 = '".$this->mT11."' "
		." , T12 = '".$this->mT12."' "
		." , T13 = '".$this->mT13."' "
		." , T14 = '".$this->mT14."' "
		." , T15 = '".$this->mT15."' "
		." , T16 = '".$this->mT16."' "
		." , T17 = '".$this->mT17."' "
		." , T18 = '".$this->mT18."' "
		." , T19 = '".$this->mT19."' "
		." , T20 = '".$this->mT20."' "
		." , T21 = '".$this->mT21."' "
		." , T22 = '".$this->mT22."' "
		." , T23 = '".$this->mT23."' "
		." , T24 = '".$this->mT24."' "
		." , T25 = '".$this->mT25."' "
		." , T26 = '".$this->mT26."' "
		." , T27 = '".$this->mT27."' "
		." , T28 = '".$this->mT28."' "
		." , T29 = '".$this->mT29."' "
		." , T30 = '".$this->mT30."' "
		." , T31 = '".$this->mT31."' "
		." , T32 = '".$this->mT32."' "
		." , T33 = '".$this->mT33."' "
		." , T34 = '".$this->mT34."' "
		." , T35 = '".$this->mT35."' "
		." , T36 = '".$this->mT36."' "
		." , T37 = '".$this->mT37."' "
		." , T38 = '".$this->mT38."' "
		." , T39 = '".$this->mT39."' "
		." , T40 = '".$this->mT40."' "
		." , T41 = '".$this->mT41."' "
		." , T42 = '".$this->mT42."' "
		." , T43 = '".$this->mT43."' "
		." , T44 = '".$this->mT44."' "
		." , T45 = '".$this->mT45."' "
		." , T46 = '".$this->mT46."' "
		." , T47 = '".$this->mT47."' "
		." , T48 = '".$this->mT48."' "
		." , T49 = '".$this->mT49."' "
		." , T50 = '".$this->mT50."' "
		." , T51 = '".$this->mT51."' "
		." , T52 = '".$this->mT52."' "
		." , T53 = '".$this->mT53."' "
		." , T54 = '".$this->mT54."' "
		." , T55 = '".$this->mT55."' "
		." , T56 = '".$this->mT56."' "
		." , T57 = '".$this->mT57."' "
		." , T58 = '".$this->mT58."' "
		." , T59 = '".$this->mT59."' "
		." , T60 = '".$this->mT60."' "
		." , T61 = '".$this->mT61."' "
		." , T62 = '".$this->mT62."' "
		." , T63 = '".$this->mT63."' "
		." , T64 = '".$this->mT64."' "
		." , T65 = '".$this->mT65."' "
		." , T66 = '".$this->mT66."' "
		." , T67 = '".$this->mT67."' "
		." , T68 = '".$this->mT68."' "
		." , T69 = '".$this->mT69."' "
		." , T70 = '".$this->mT70."' "
		." , T71 = '".$this->mT71."' "
		." , T72 = '".$this->mT72."' "
		." , T73 = '".$this->mT73."' "
		." , T74 = '".$this->mT74."' "
		." , T75 = '".$this->mT75."' "
		." , T76 = '".$this->mT76."' "
		." , T77 = '".$this->mT77."' "
		." , T78 = '".$this->mT78."' "
		." , T79 = '".$this->mT79."' "
		." , T80 = '".$this->mT80."' "
		." , T81 = '".$this->mT81."' "
		." , T82 = '".$this->mT82."' "
		." , T83 = '".$this->mT83."' "
		." , T84 = '".$this->mT84."' "
		." , T85 = '".$this->mT85."' "
		." , T86 = '".$this->mT86."' "
		." , T87 = '".$this->mT87."' "
		." , check_status = '".$this->mCheckStatus."' "
		." WHERE ccard_id = ".$this->mCCardId." "; 
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ccard_id=".$this->mCCardId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		//if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кؤӹ�˹�ҹ��";
		//if ($this->mFirstname == "") $asrErrReturn["firstname"] = "��س��кت���";
		//if ($this->mLastname == "") $asrErrReturn["lastName"] = "��س��кع��ʡ��";

		//if ($this->mAddress == "") $asrErrReturn["address"] = "��س��кط������";
		//if ($this->mZip == "") $asrErrReturn["zip"] = "��س��к�������ɳ���";
		//if ($this->mProvince == "") $asrErrReturn["province"] = "��س��к�������ɳ���";
		/*
        Switch ( strtolower($Mode) )
        {
            Case "add":
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = "Duplicate email please type again";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}
				break;	
            Case "update":
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = " Duplicate email please type again";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}
				break;	
            Case "delete":
                Break;
        }
		*/
        Return $asrErrReturn;
    }

}

/*********************************************************
		Class :				CustomerList
		Last update :		25  Nov 06
		Description:		Class manage customer list
*********************************************************/

class CustomerCCardList extends DataList {
	var $TABLE = "t_customer_ccard";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.ccard_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  * FROM ".$this->TABLE."  O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerCCard($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }


}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureOwner extends DB{

	var $TABLE="t_insure_owner";

	var $m_insure_ower_id;
	function get_insure_owner_id() { return $this->m_insure_ower_id; }
	function set_insure_owner_id($data) { $this->m_insure_ower_id = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_check_no;
	function get_check_no() { return $this->m_check_no; }
	function set_check_no($data) { $this->m_check_no = $data; }

	var $m_check_date;
	function get_check_date() { return $this->m_check_date; }
	function set_check_date($data) { $this->m_check_date = $data; }
	
	var $m_check_price;
	function get_check_price() { return $this->m_check_price; }
	function set_check_price($data) { $this->m_check_price = $data; }
	
	var $m_acc_type;
	function get_acc_type() { return $this->m_acc_type; }
	function set_acc_type($data) { $this->m_acc_type = $data; }
	
	function InsureOwner($objData=NULL) {
        If ($objData->insure_ower_id !="") {
			$this->set_insure_owner_id($objData->insure_ower_id);
			$this->set_date_add($objData->date_add);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_check_no($objData->check_no);
			$this->set_check_date($objData->check_date);
			$this->set_check_price($objData->check_price);
			$this->set_acc_type($objData->acc_type);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_ower_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_ower_id =".$this->m_insure_ower_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureOwner($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureOwner($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( date_add, broker_id , insure_company_id , check_no, check_date, check_price, acc_type ) " ." VALUES ( "
		." '".$this->m_date_add."' , "
		." '".$this->m_broker_id."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_check_no."' , "
		." '".$this->m_check_date."' , "
		." '".$this->m_check_price."' , "
		." '".$this->m_acc_type."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_ower_id = mysql_insert_id();
            return $this->m_insure_ower_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   check_no = '".$this->m_check_no."' "
		." , check_date = '".$this->m_check_date."' "
		." , check_price = '".$this->m_check_price."' "
		." WHERE insure_ower_id = ".$this->m_insure_ower_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_ower_id=".$this->m_insure_ower_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureOwnerList extends DataList {
	var $TABLE = "t_insure_owner";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_ower_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_company  T ON T.insure_company_id = P.insure_company_id ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P  LEFT JOIN t_insure_company  T ON T.insure_company_id = P.insure_company_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureOwner($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
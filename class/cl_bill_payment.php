<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class BillPayment extends DB{

	var $TABLE="t_bill_payment";

	var $m_bill_payment_id;
	function get_bill_payment_id() { return $this->m_bill_payment_id; }
	function set_bill_payment_id($data) { $this->m_bill_payment_id = $data; }

	var $m_import_date;
	function get_import_date() { return $this->m_import_date; }
	function set_import_date($data) { $this->m_import_date = $data; }

	var $m_import_by;
	function get_import_by() { return $this->m_import_by; }
	function set_import_by($data) { $this->m_import_by = $data; }

	var $m_bank;
	function get_bank() { return $this->m_bank; }
	function set_bank($data) { $this->m_bank = $data; }

	var $m_file;
	function get_file() { return $this->m_file; }
	function set_file($data) { $this->m_file = $data; }

	var $m_record_type;
	function get_record_type() { return $this->m_record_type; }
	function set_record_type($data) { $this->m_record_type = $data; }

	var $m_sequence_no;
	function get_sequence_no() { return $this->m_sequence_no; }
	function set_sequence_no($data) { $this->m_sequence_no = $data; }

	var $m_bank_code;
	function get_bank_code() { return $this->m_bank_code; }
	function set_bank_code($data) { $this->m_bank_code = $data; }

	var $m_company_account;
	function get_company_account() { return $this->m_company_account; }
	function set_company_account($data) { $this->m_company_account = $data; }

	var $m_company_name;
	function get_company_name() { return $this->m_company_name; }
	function set_company_name($data) { $this->m_company_name = $data; }

	var $m_effective_date;
	function get_effective_date() { return $this->m_effective_date; }
	function set_effective_date($data) { $this->m_effective_date = $data; }

	var $m_service_code;
	function get_service_code() { return $this->m_service_code; }
	function set_service_code($data) { $this->m_service_code = $data; }

	var $m_filler;
	function get_filler() { return $this->m_filler; }
	function set_filler($data) { $this->m_filler = $data; }

	var $m_total_debit_amount;
	function get_total_debit_amount() { return $this->m_total_debit_amount; }
	function set_total_debit_amount($data) { $this->m_total_debit_amount = $data; }

	var $m_total_debit_transection;
	function get_total_debit_transection() { return $this->m_total_debit_transection; }
	function set_total_debit_transection($data) { $this->m_total_debit_transection = $data; }

	var $m_total_credit_amount;
	function get_total_credit_amount() { return $this->m_total_credit_amount; }
	function set_total_credit_amount($data) { $this->m_total_credit_amount = $data; }

	var $m_total_credit_transection;
	function get_total_credit_transection() { return $this->m_total_credit_transection; }
	function set_total_credit_transection($data) { $this->m_total_credit_transection = $data; }

	var $m_spare;
	function get_spare() { return $this->m_spare; }
	function set_spare($data) { $this->m_spare = $data; }

	
	function BillPayment($objData=NULL) {
        If ($objData->bill_payment_id !="") {
			$this->set_bill_payment_id($objData->bill_payment_id);
			$this->set_import_date($objData->import_date);
			$this->set_import_by($objData->import_by);
			$this->set_bank($objData->bank);
			$this->set_file($objData->file);
			$this->set_record_type($objData->record_type);
			$this->set_sequence_no($objData->sequence_no);
			$this->set_bank_code($objData->bank_code);
			$this->set_company_account($objData->company_account);
			$this->set_company_name($objData->company_name);
			$this->set_effective_date($objData->effective_date);
			$this->set_service_code($objData->service_code);
			$this->set_filler($objData->filler);
			$this->set_total_debit_amount($objData->total_debit_amount);
			$this->set_total_debit_transection($objData->total_debit_transection);
			$this->set_total_credit_amount($objData->total_credit_amount);
			$this->set_total_credit_transection($objData->total_credit_transection);
			$this->set_spare($objData->spare);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_bill_payment_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE bill_payment_id =".$this->m_bill_payment_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BillPayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BillPayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( import_date , import_by , bank , file , record_type , sequence_no , bank_code , company_account , company_name , effective_date , service_code , filler , total_debit_amount , total_debit_transection , total_credit_amount , total_credit_transection , spare ) " ." VALUES ( "
		." '".$this->m_import_date."' , "
		." '".$this->m_import_by."' , "
		." '".$this->m_bank."' , "
		." '".$this->m_file."' , "
		." '".$this->m_record_type."' , "
		." '".$this->m_sequence_no."' , "
		." '".$this->m_bank_code."' , "
		." '".$this->m_company_account."' , "
		." '".$this->m_company_name."' , "
		." '".$this->m_effective_date."' , "
		." '".$this->m_service_code."' , "
		." '".$this->m_filler."' , "
		." '".$this->m_total_debit_amount."' , "
		." '".$this->m_total_debit_transection."' , "
		." '".$this->m_total_credit_amount."' , "
		." '".$this->m_total_credit_transection."' , "
		." '".$this->m_spare."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_bill_payment_id = mysql_insert_id();
            return $this->m_bill_payment_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "		
		."   bank = '".$this->m_bank."' "
		." , file = '".$this->m_file."' "
		." , record_type = '".$this->m_record_type."' "
		." , sequence_no = '".$this->m_sequence_no."' "
		." , bank_code = '".$this->m_bank_code."' "
		." , company_account = '".$this->m_company_account."' "
		." , company_name = '".$this->m_company_name."' "
		." , effective_date = '".$this->m_effective_date."' "
		." , service_code = '".$this->m_service_code."' "
		." , filler = '".$this->m_filler."' "
		." , total_debit_amount = '".$this->m_total_debit_amount."' "
		." , total_debit_transection = '".$this->m_total_debit_transection."' "
		." , total_credit_amount = '".$this->m_total_credit_amount."' "
		." , total_credit_transection = '".$this->m_total_credit_transection."' "
		." , spare = '".$this->m_spare."' "
		." WHERE bill_payment_id = ".$this->m_bill_payment_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE bill_payment_id=".$this->m_bill_payment_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class BillPaymentList extends DataList {
	var $TABLE = "t_bill_payment";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT bill_payment_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new BillPayment($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT bill_payment_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_adler IA ON IA.ref1 = P.ref1 "
		." LEFT JOIN t_insure I ON I.insure_id = IA.insure_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT P.* FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_adler IA ON IA.ref1 = P.ref1 "
		." LEFT JOIN t_insure I ON I.insure_id = IA.insure_id "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new BillPayment($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT bill_payment_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new BillPayment($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_bill_payment_id()."\"");
			if (($defaultId != null) && ($objItem->get_bill_payment_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
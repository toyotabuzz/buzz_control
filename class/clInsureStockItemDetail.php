<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_stock_item_detail table

*********************************************************/
 
class InsureStockItemDetail extends DB{

	var $TABLE="t_insure_stock_item_detail";

	var $m_insure_stock_item_detail_id;
	function get_insure_stock_item_detail_id() { return $this->m_insure_stock_item_detail_id; }
	function set_insure_stock_item_detail_id($data) { $this->m_insure_stock_item_detail_id = $data; }
	
	var $m_insure_stock_item_id;
	function get_insure_stock_item_id() { return $this->m_insure_stock_item_id; }
	function set_insure_stock_item_id($data) { $this->m_insure_stock_item_id = $data; }
	
	var $m_insure_stock_id;
	function get_insure_stock_id() { return $this->m_insure_stock_id; }
	function set_insure_stock_id($data) { $this->m_insure_stock_id = $data; }

	var $m_stock_insure_id;
	function get_stock_insure_id() { return $this->m_stock_insure_id; }
	function set_stock_insure_id($data) { $this->m_stock_insure_id = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_checked;
	function get_checked() { return $this->m_checked; }
	function set_checked($data) { $this->m_checked = $data; }
	
	function InsureStockItemDetail($objData=NULL) {
        If ($objData->insure_stock_item_detail_id !=""  ) {
			$this->set_insure_stock_item_detail_id($objData->insure_stock_item_detail_id);
			$this->set_insure_stock_item_id($objData->insure_stock_item_id);
			$this->set_insure_stock_id($objData->insure_stock_id);
			$this->set_remark($objData->remark);
			$this->set_checked($objData->checked);
        }
    }

	function init(){
		
	}
		
		
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(insure_stock_item_detail_id) AS RSUM  FROM ".$this->TABLE
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
		
		
	function load() {

		if ($this->m_insure_stock_item_detail_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_stock_item_detail_id =".$this->m_insure_stock_item_detail_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureStockItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_stock_item_id , insure_stock_id, remark ) " ." VALUES ( "
		." '".$this->m_insure_stock_item_id."' , "
		." '".$this->m_insure_stock_id."' , "
		." '".$this->m_remark."'  "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_stock_item_detail_id = mysql_insert_id();
            return $this->m_insure_stock_item_detail_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  remark = '".$this->m_remark."' , "
		."  checked = '".$this->m_checked."' "
		." WHERE insure_stock_item_detail_id = ".$this->m_insure_stock_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_stock_item_detail_id=".$this->m_insure_stock_item_detail_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureStockItemDetailList extends DataList {
	var $TABLE = "t_insure_stock_item_detail";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_stock_item_detail_id) as rowCount FROM ".$this->TABLE." O  "
			." LEFT JOIN t_insure_stock_item S ON S.insure_stock_item_id =  O.insure_stock_item_id  "
			." LEFT JOIN t_insure_stock SS ON SS.insure_stock_id =  O.insure_stock_id  "
			." LEFT JOIN t_insure I ON I.insure_id =  SS.insure_id  "
			." LEFT JOIN t_insure_car IC ON IC.car_id =  I.car_id  "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT O.* FROM ".$this->TABLE." O "
			." LEFT JOIN t_insure_stock_item S ON S.insure_stock_item_id =  O.insure_stock_item_id  "
			." LEFT JOIN t_insure_stock SS ON SS.insure_stock_id =  O.insure_stock_id  "
			." LEFT JOIN t_insure I ON I.insure_id =  SS.insure_id  "
			." LEFT JOIN t_insure_car IC ON IC.car_id =  I.car_id  "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureStockItemDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_insure_stock_item_detail_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_stock_item_detail_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}
}
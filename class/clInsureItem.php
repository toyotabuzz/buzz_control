<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureItem extends DB{

	var $TABLE="t_insure_item";

	var $m_insure_item_id;
	function get_insure_item_id() { return $this->m_insure_item_id; }
	function set_insure_item_id($data) { $this->m_insure_item_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_payment_type;
	function get_payment_type() { return $this->m_payment_type; }
	function set_payment_type($data) { $this->m_payment_type = $data; }

	var $m_payment_style_id;
	function get_payment_style_id() { return $this->m_payment_style_id; }
	function set_payment_style_id($data) { $this->m_payment_style_id = $data; }

	var $m_payment_style_title;
	function get_payment_style_title() { return $this->m_payment_style_title; }
	function set_payment_style_title($data) { $this->m_payment_style_title = $data; }
	
	var $m_payment_date;
	function get_payment_date() { return $this->m_payment_date; }
	function set_payment_date($data) { $this->m_payment_date = $data; }

	var $m_payment_time;
	function get_payment_time() { return $this->m_payment_time; }
	function set_payment_time($data) { $this->m_payment_time = $data; }
	
	var $m_payment_price;
	function get_payment_price() { return $this->m_payment_price; }
	function set_payment_price($data) { $this->m_payment_price = $data; }
	
	//�ѧ��������, ��ҧ����, ��������
	var $m_payment_status;
	function get_payment_status() { return $this->m_payment_status; }
	function set_payment_status($data) { $this->m_payment_status = $data; }
	
	var $m_payment_number;
	function get_payment_number() { return $this->m_payment_number; }
	function set_payment_number($data) { $this->m_payment_number = $data; }
	
	var $m_insure_date;
	function get_insure_date() { return $this->m_insure_date; }
	function set_insure_date($data) { $this->m_insure_date = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_payment_order;
	function get_payment_order() { return $this->m_payment_order; }
	function set_payment_order($data) { $this->m_payment_order = $data; }
	
	var $m_payment_sub_order;
	function get_payment_sub_order() { return $this->m_payment_sub_order; }
	function set_payment_sub_order($data) { $this->m_payment_sub_order = $data; }	
	
	var $m_payment_no;
	function get_payment_no() { return $this->m_payment_no; }
	function set_payment_no($data) { $this->m_payment_no = $data; }
	
	var $m_transfer;
	function get_transfer() { return $this->m_transfer; }
	function set_transfer($data) { $this->m_transfer = $data; }
	
	var $m_acc_approve;
	function get_acc_approve() { return $this->m_acc_approve; }
	function set_acc_approve($data) { $this->m_acc_approve = $data; }
	
	var $m_acc_date;
	function get_acc_date() { return $this->m_acc_date; }
	function set_acc_date($data) { $this->m_acc_date = $data; }
	
	var $m_acc_by;
	function get_acc_by() { return $this->m_acc_by; }
	function set_acc_by($data) { $this->m_acc_by = $data; }
	
	var $m_acc_company_id;
	function get_acc_company_id() { return $this->m_acc_company_id; }
	function set_acc_company_id($data) { $this->m_acc_company_id = $data; }
	
	var $m_acc_remark;
	function get_acc_remark() { return $this->m_acc_remark; }
	function set_acc_remark($data) { $this->m_acc_remark = $data; }
	
	var $m_old_number_nohead;
	function get_old_number_nohead() { return $this->m_old_number_nohead; }
	function set_old_number_nohead($data) { $this->m_old_number_nohead = $data; }
	
	var $m_old_number_head;
	function get_old_number_head() { return $this->m_old_number_head; }
	function set_old_number_head($data) { $this->m_old_number_head = $data; }
	
	var $m_payment_number_nohead;
	function get_payment_number_nohead() { return $this->m_payment_number_nohead; }
	function set_payment_number_nohead($data) { $this->m_payment_number_nohead = $data; }
	
	var $m_payment_number_head;
	function get_payment_number_head() { return $this->m_payment_number_head; }
	function set_payment_number_head($data) { $this->m_payment_number_head = $data; }
	
	//����Ѻ�͡������ payent more 
	var $m_insure_car_id;
	function get_insure_car_id() { return $this->m_insure_car_id; }
	function set_insure_car_id($data) { $this->m_insure_car_id = $data; }
	
	
	//***********************************���ҧ����红����� � �ѹ�������Թ�ó��ա������¹�ŧ�����������ѧ�ҡ��
	var $m_cus_name;
	function get_cus_name() { return $this->m_cus_name; }
	function set_cus_name($data) { $this->m_cus_name = $data; }
	
	var $m_cus_address;
	function get_cus_address() { return $this->m_cus_address; }
	function set_cus_address($data) { $this->m_cus_address = $data; }
	
	var $m_car_code;
	function get_car_code() { return $this->m_car_code; }
	function set_car_code($data) { $this->m_car_code = $data; }
	
	var $m_car_name;
	function get_car_name() { return $this->m_car_name; }
	function set_car_name($data) { $this->m_car_name = $data; }
	
	var $m_sale_name;
	function get_sale_name() { return $this->m_sale_name; }
	function set_sale_name($data) { $this->m_sale_name = $data; }
	
	var $m_insure_customer;
	function get_insure_customer() { return $this->m_insure_customer; }
	function set_insure_customer($data) { $this->m_insure_customer = $data; }
	
	var $m_insure_sale;
	function get_insure_sale() { return $this->m_insure_sale; }
	function set_insure_sale($data) { $this->m_insure_sale = $data; }		
	
	var $m_insure_car;
	function get_insure_car() { return $this->m_insure_car; }
	function set_insure_car($data) { $this->m_insure_car = $data; }	
	//*************************************���ҧ����红����� � �ѹ�������Թ�ó��ա������¹�ŧ�����������ѧ�ҡ��
	
	/* �Ţ�ѹ������Ѻ�Թ㺡ӡѺ���� �Ѿഷ 2014*/
	var $m_cashier_number;
	function get_cashier_number() { return $this->m_cashier_number; }
	function set_cashier_number($data) { $this->m_cashier_number = $data; }	
	
	
	function InsureItem($objData=NULL) {
		if ($objData->insure_item_id !="" OR $objData->payment_number_head != "" OR $objData->payment_number_nohead != "" OR $objData->cashier_number != ""  ) {
			$this->set_insure_item_id($objData->insure_item_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_payment_type($objData->payment_type);
			$this->set_payment_style_id($objData->payment_style_id);
			$this->set_payment_style_title($objData->payment_style_title);
			$this->set_payment_date($objData->payment_date);
			$this->set_payment_time($objData->payment_time);
			$this->set_payment_price($objData->payment_price);
			$this->set_payment_status($objData->payment_status);
			$this->set_payment_number($objData->payment_number);
			$this->set_insure_date($objData->insure_date);
			$this->set_remark($objData->remark);
			$this->set_payment_order($objData->payment_order);
			$this->set_payment_sub_order($objData->payment_sub_order);
			$this->set_payment_no($objData->payment_no);
			$this->set_acc_approve($objData->acc_approve);
			$this->set_acc_date($objData->acc_date);
			$this->set_acc_by($objData->acc_by);
			$this->set_acc_company_id($objData->acc_company_id);
			$this->set_acc_remark($objData->acc_remark);
			$this->set_old_number_nohead($objData->old_number_nohead);
			$this->set_old_number_head($objData->old_number_head);
			$this->set_payment_number_nohead($objData->payment_number_nohead);
			$this->set_payment_number_head($objData->payment_number_head);
			$this->set_insure_car_id($objData->insure_car_id);
			$this->set_insure_customer($objData->insure_customer);
			$this->set_insure_sale($objData->insure_sale);
			$this->set_insure_car($objData->insure_car);
			$this->set_cus_name($objData->cus_name);
			$this->set_car_code($objData->car_code);
			$this->set_cus_address($objData->cus_address);
			$this->set_car_name($objData->car_name);
			$this->set_sale_name($objData->sale_name);
			$this->set_cashier_number($objData->cashier_number);
		}
    }

	function init(){
		
	}
		
	function load() {
		
		if ($this->m_insure_item_id == '') {
			return false;
		}
		$strSql = "SELECT P.*, turef.ref_desc AS payment_style_title FROM ".$this->TABLE." P "
		." LEFT JOIN turef ON turef.ref_type='PAYMENT' AND turef.ref_code='STYLE' AND turef.ref_value = P.payment_style_id "
		." WHERE P.insure_item_id =".$this->m_insure_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadMaxHead($strCondition) {
		$strSql = "SELECT MAX(payment_number_head) as payment_number_head  FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	
	function loadMaxNoHead($strCondition) {
		$strSql = "SELECT MAX(payment_number_nohead) as payment_number_nohead  FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadMaxCashierNumber($strCondition) {
		$strSql = "SELECT MAX(cashier_number) as cashier_number  FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumPrice($strCondition) {
		$strSql = "SELECT SUM(payment_price) as payment_price FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$hPaymentPrice = $row->payment_price;
                $result->freeResult();
				return $hPaymentPrice;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT P.*, turef.ref_desc AS payment_style_title FROM ".$this->TABLE." P "
		." LEFT JOIN turef ON turef.ref_type='PAYMENT' AND turef.ref_code='STYLE' AND turef.ref_value = P.payment_style_id "
		." WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_car_id, payment_type , payment_style_id , payment_date , payment_time , payment_price , payment_status , payment_number , insure_date , remark , payment_sub_order , payment_order , payment_no , acc_approve , acc_date , acc_by , acc_remark , created_at, created_by ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_insure_car_id."' , "
		." '".$this->m_payment_type."' , "
		." '".$this->m_payment_style_id."' , "
		." '".$this->m_payment_date."' , "
		." '".$this->m_payment_time."' , "
		." '".$this->m_payment_price."' , "
		." '".$this->m_payment_status."' , "
		." '".$this->m_payment_number."' , "
		." '".$this->m_insure_date."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_payment_sub_order."' , "
		." '".$this->m_payment_order."' , "
		." '".$this->m_payment_no."' , "

		." '".$this->m_acc_approve."' , "
		." '".$this->m_acc_date."' , "
		." '".$this->m_acc_by."' , "
		." '".$this->m_acc_remark."' ,  "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_item_id = mysql_insert_id();
            return $this->m_insure_item_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , payment_type = '".$this->m_payment_type."' "
		." , payment_style_id = '".$this->m_payment_style_id."' "
		." , payment_date = '".$this->m_payment_date."' "
		." , payment_time = '".$this->m_payment_time."' "
		." , payment_price = '".$this->m_payment_price."' "
		." , payment_status = '".$this->m_payment_status."' "
		." , payment_number = '".$this->m_payment_number."' "
		." , insure_date = '".$this->m_insure_date."' "
		." , remark = '".$this->m_remark."' "
		." , payment_sub_order = '".$this->m_payment_sub_order."' "
		." , payment_order = '".$this->m_payment_order."' "
		." , payment_no = '".$this->m_payment_no."' "
		." , acc_approve = '".$this->m_acc_approve."' "
		." , acc_date = '".$this->m_acc_date."' "
		." , acc_by = '".$this->m_acc_by."' "
		." , acc_remark = '".$this->m_acc_remark."' "

		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  payment_status = '".$this->m_payment_status."' "
		." , insure_date = '".$this->m_insure_date."' "
		." , payment_no = '".$this->m_payment_no."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateLog(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  insure_customer = '".$this->m_insure_customer."' "
		." , insure_car = '".$this->m_insure_car."' "
		." , insure_sale = '".$this->m_insure_sale."' "
		." , cus_name = '".$this->m_cus_name."' "
		." , cus_address = '".$this->m_cus_address."' "
		." , car_code = '".$this->m_car_code."' "
		." , car_name = '".$this->m_car_name."' "
		." , sale_name = '".$this->m_sale_name."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	
	
	function update_remark(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  remark = '".$this->m_remark."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();

        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function update_more_price(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  payment_price = '".$this->m_payment_price."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();

        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	function updateAcc(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  acc_approve = '".$this->m_acc_approve."' "
		." , acc_date = '".$this->m_acc_date."' "
		." , acc_by = '".$this->m_acc_by."' "
		." , acc_company_id = '".$this->m_acc_company_id."' "
		." , acc_remark = '".$this->m_acc_remark."' "
		." , payment_number_head = '".$this->m_payment_number_head."' "
		." , payment_number_nohead = '".$this->m_payment_number_nohead."' "
		." , cashier_number = '".$this->m_cashier_number."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
		//exit;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateOldNumberNohead(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  old_number_nohead = '".$this->m_old_number_nohead."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateOldNumberHead(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  old_number_head = '".$this->m_old_number_head."' "
		." WHERE insure_item_id = ".$this->m_insure_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_item_id=".$this->m_insure_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureItemList extends DataList {
	var $TABLE = "t_insure_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		." LEFT JOIN t_insure_form F ON F.insure_id = P.insure_id AND frm_type = 0901 "
		." LEFT JOIN t_insure_payment OP ON P.insure_item_id = OP.order_id "
		
		.$this->getFilterSQL();	// WHERE clause
	    echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, turef.ref_desc AS payment_style_title FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		." LEFT JOIN t_insure_form F ON F.insure_id = P.insure_id AND frm_type = 0901 "
		." LEFT JOIN turef ON turef.ref_type='PAYMENT' AND turef.ref_code='STYLE' AND turef.ref_value = P.payment_style_id "
		." LEFT JOIN t_insure_payment OP ON P.insure_item_id = OP.order_id "
		
			.$this->getFilterSQL()	// WHERE clause
			.' GROUP BY P.insure_item_id '
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadOrderPayment() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		." LEFT JOIN t_insure_payment OP ON P.insure_item_id = OP.order_id "
		." LEFT JOIN t_insure_form F ON F.insure_id = P.insure_id AND frm_type = 0901 "
		
		.$this->getFilterSQL();	// WHERE clause
		// echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, turef.ref_desc AS payment_style_title FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure I ON I.insure_id = P.insure_id "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		." LEFT JOIN t_insure_payment OP ON P.insure_item_id = OP.order_id "
		." LEFT JOIN t_insure_form F ON F.insure_id = P.insure_id AND frm_type = 0901 "
		." LEFT JOIN turef ON turef.ref_type='PAYMENT' AND turef.ref_code='STYLE' AND turef.ref_value = P.payment_style_id "
		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }


	function loadPayment() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE
			." P  "
			." LEFT JOIN t_insure ON t_insure.insure_id = P.insure_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_insure ON t_insure.insure_id = P.insure_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadMore() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_car IC ON IC.car_id = P.insure_car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		
		.$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, turef.ref_desc AS payment_type_title FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure_car IC ON IC.car_id = P.insure_car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		." LEFT JOIN turef ON turef.ref_type='PAYMENT' AND turef.ref_code='STYLE' AND turef.ref_value = P.payment_style_id "
		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
		
	
}
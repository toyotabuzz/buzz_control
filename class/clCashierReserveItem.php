<?
/*********************************************************
		Class :					Cashier Reserve Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_cashier_reserve_item table

*********************************************************/
 
class CashierReserveItem extends DB{

	var $TABLE="t_cashier_reserve_item";

	var $mReserveItemId;
	function getReserveItemId() { return $this->mReserveItemId; }
	function setReserveItemId($data) { $this->mReserveItemId = $data; }	
	
	var $mReserveId;
	function getReserveId() { return $this->mReserveId; }
	function setReserveId($data) { $this->mReserveId = $data; }
	
	var $mReserveTitle;
	function getReserveTitle() { return $this->mReserveTitle; }
	function setReserveTitle($data) { $this->mReserveTitle = $data; }	
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mTotal;
	function getTotal() { return htmlspecialchars($this->mTotal); }
	function setTotal($data) { $this->mTotal = $data; }
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	

	function CashierReserveItem($objData=NULL) {
        If ($objData->reserve_item_id !="") {
            $this->setReserveItemId($objData->reserve_item_id);
			$this->setReserveId($objData->reserve_id);
			$this->setReserveTitle($objData->reserve_title);
			$this->setRemark($objData->remark);
			$this->setTotal($objData->total);
			$this->setRemark($objData->remark);
			$this->setRank($objData->rank);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
		$this->setReserveTitle(stripslashes($this->mReserveTitle));
	}
		
	function load() {

		if ($this->mReserveItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE reserve_item_id =".$this->mReserveItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierReserveItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierReserveItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( reserve_title,total, remark , reserve_id, rank ) "
						." VALUES (  '".$this->mReserveTitle."',  '".$this->mTotal."' , '".$this->mRemark."', '".$this->mReserveId."', '".$this->mRank."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mReserveItemId = mysql_insert_id();
            return $this->mReserveItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET remark = '".$this->mRemark."'  "
						." , reserve_title = '".$this->mReserveTitle."'  "
						." , total = '".$this->mTotal."'  "
						." , reserve_id = '".$this->mReserveId."'  "
						." , rank = '".$this->mRank."'  "
						." WHERE  reserve_item_id = ".$this->mReserveItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE reserve_item_id=".$this->mReserveItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mReserveId == "") $asrErrReturn["reserve_id"] = "��س��к�";
		//if ($this->mTotal == "") $asrErrReturn["total"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Cashier Reserve Item List

		Last update :		22 Mar 02

		Description:		Customer Cashier Reserve Item List

*********************************************************/

class CashierReserveItemList extends DataList {
	var $TABLE = "t_cashier_reserve_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT reserve_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CashierReserveItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
}
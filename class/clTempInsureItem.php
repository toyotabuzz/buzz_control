<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class TempInsureItem extends DB{

	var $TABLE="t_temp_insure_item";

	var $m_link_id;
	function get_link_id() { return $this->m_link_id; }
	function set_link_id($data) { $this->m_link_id = $data; }		
	
	var $m_insure_item_id;
	function get_insure_item_id() { return $this->m_insure_item_id; }
	function set_insure_item_id($data) { $this->m_insure_item_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_payment_type;
	function get_payment_type() { return $this->m_payment_type; }
	function set_payment_type($data) { $this->m_payment_type = $data; }
	
	var $m_payment_date;
	function get_payment_date() { return $this->m_payment_date; }
	function set_payment_date($data) { $this->m_payment_date = $data; }
	
	var $m_payment_price;
	function get_payment_price() { return $this->m_payment_price; }
	function set_payment_price($data) { $this->m_payment_price = $data; }
	
	//�ѧ��������, ��ҧ����, ��������
	var $m_payment_status;
	function get_payment_status() { return $this->m_payment_status; }
	function set_payment_status($data) { $this->m_payment_status = $data; }
	
	var $m_payment_number;
	function get_payment_number() { return $this->m_payment_number; }
	function set_payment_number($data) { $this->m_payment_number = $data; }
	
	var $m_insure_date;
	function get_insure_date() { return $this->m_insure_date; }
	function set_insure_date($data) { $this->m_insure_date = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_payment_order;
	function get_payment_order() { return $this->m_payment_order; }
	function set_payment_order($data) { $this->m_payment_order = $data; }
	
	var $m_payment_sub_order;
	function get_payment_sub_order() { return $this->m_payment_sub_order; }
	function set_payment_sub_order($data) { $this->m_payment_sub_order = $data; }	
	
	var $m_payment_no;
	function get_payment_no() { return $this->m_payment_no; }
	function set_payment_no($data) { $this->m_payment_no = $data; }
	
	var $m_transfer;
	function get_transfer() { return $this->m_transfer; }
	function set_transfer($data) { $this->m_transfer = $data; }
	
	var $m_acc_approve;
	function get_acc_approve() { return $this->m_acc_approve; }
	function set_acc_approve($data) { $this->m_acc_approve = $data; }
	
	var $m_acc_date;
	function get_acc_date() { return $this->m_acc_date; }
	function set_acc_date($data) { $this->m_acc_date = $data; }
	
	var $m_acc_by;
	function get_acc_by() { return $this->m_acc_by; }
	function set_acc_by($data) { $this->m_acc_by = $data; }
	
	var $m_acc_remark;
	function get_acc_remark() { return $this->m_acc_remark; }
	function set_acc_remark($data) { $this->m_acc_remark = $data; }
	
	
	function TempInsureItem($objData=NULL) {
        If ($objData->insure_item_id !="") {
			$this->set_link_id($objData->link_id);
			$this->set_insure_item_id($objData->insure_item_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_payment_type($objData->payment_type);
			$this->set_payment_date($objData->payment_date);
			$this->set_payment_price($objData->payment_price);
			$this->set_payment_status($objData->payment_status);
			$this->set_payment_number($objData->payment_number);
			$this->set_insure_date($objData->insure_date);
			$this->set_remark($objData->remark);
			$this->set_payment_order($objData->payment_order);
			$this->set_payment_sub_order($objData->payment_sub_order);
			$this->set_payment_no($objData->payment_no);
			$this->set_acc_approve($objData->acc_approve);
			$this->set_acc_date($objData->acc_date);
			$this->set_acc_by($objData->acc_by);
			$this->set_acc_remark($objData->acc_remark);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_item_id =".$this->m_insure_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( link_id,  insure_item_id, insure_id , payment_type , payment_date , payment_price
		 , payment_status , payment_number , insure_date , remark , payment_sub_order , payment_order , payment_no , acc_approve , acc_date , acc_by , acc_remark  ) " ." VALUES ( "
		." '".$this->m_link_id."' , "
		." '".$this->m_insure_item_id."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_payment_type."' , "
		." '".$this->m_payment_date."' , "
		." '".$this->m_payment_price."' , "
		." '".$this->m_payment_status."' , "
		." '".$this->m_payment_number."' , "
		." '".$this->m_insure_date."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_payment_sub_order."' , "
		." '".$this->m_payment_order."' , "
		." '".$this->m_payment_no."' , "
		." '".$this->m_acc_approve."' , "
		." '".$this->m_acc_date."' , "
		." '".$this->m_acc_by."' , "
		." '".$this->m_acc_remark."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_item_id = mysql_insert_id();
            return $this->m_insure_item_id;
        } else {
			return false;
	    }
	}


	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_item_id=".$this->m_insure_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class TempInsureItemList extends DataList {
	var $TABLE = "t_temp_insure_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_temp_insure I ON I.insure_id = P.insure_id and I.link_id = P.link_id  "
		." LEFT JOIN t_temp_insure_car IC ON IC.car_id = I.car_id  "
		." LEFT JOIN t_temp_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		
		.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_temp_insure I ON I.insure_id = P.insure_id and I.link_id = P.link_id  "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id "
		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new TempInsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadPayment() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_item_id) as rowCount FROM ".$this->TABLE." P  "
			." LEFT JOIN t_temp_insure I ON I.insure_id = P.insure_id and P.link_id = I.link_id  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_temp_insure I ON I.insure_id = P.insure_id and P.link_id = I.link_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new TempInsureItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
}
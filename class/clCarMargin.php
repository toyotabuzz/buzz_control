<?
/*********************************************************
		Class :					Car Margin

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_margin table

*********************************************************/
 
class CarMargin extends DB{

	var $TABLE="t_car_margin";

	var $mCarMarginId;
	function getCarMarginId() { return $this->mCarMarginId; }
	function setCarMarginId($data) { $this->mCarMarginId = $data; }
	
	var $mCode;
	function getCode() { return $this->mCode; }
	function setCode($data) { $this->mCode = $data; }	
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mContent;
	function getContent() { return htmlspecialchars($this->mContent); }
	function setContent($data) { $this->mContent = $data; }
	
	var $mMargin;
	function getMargin() { return $this->mMargin; }
	function setMargin($data) { $this->mMargin = $data; }	
	
	var $mStartDate;
	function getStartDate() { return $this->mStartDate; }
	function setStartDate($data) { $this->mStartDate = $data; }	
	
	var $mEndDate;
	function getEndDate() { return $this->mEndDate; }
	function setEndDate($data) { $this->mEndDate = $data; }	
	
	var $mAllTime;
	function getAllTime() { return $this->mAllTime; }
	function setAllTime($data) { $this->mAllTime = $data; }	
	
	var $mGOA;
	function getGOA() { return $this->mGOA; }
	function setGOA($data) { $this->mGOA = $data; }		
	
	function CarMargin($objData=NULL) {
        If ($objData->car_margin_id !="") {
            $this->setCarMarginId($objData->car_margin_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setContent($objData->content);			
			$this->setMargin($objData->margin);
			$this->setStartDate($objData->start_date);
			$this->setEndDate($objData->end_date);
			$this->setAllTime($objData->alltime);
			$this->setGOA($objData->goa);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
		$this->setCode(stripslashes($this->mCode));
	}
		
	function load() {

		if ($this->mCarMarginId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_margin_id =".$this->mCarMarginId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarMargin($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarMargin($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title,code, content, start_date, end_date, alltime, goa,  margin ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mCode."' , '".$this->mContent."' , '".$this->mStartDate."', '".$this->mEndDate."', '".$this->mAllTime."', '".$this->mGOA."', '".$this->mMargin."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCarMarginId = mysql_insert_id();
            return $this->mCarMarginId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , code = '".$this->mCode."'  "
						." , content = '".$this->mContent."'  "
						." , start_date = '".$this->mStartDate."'  "
						." , end_date = '".$this->mEndDate."'  "
						." , alltime = '".$this->mAllTime."'  "
						." , goa = '".$this->mGOA."'  "
						." , margin = '".$this->mMargin."'  "
						." WHERE  car_margin_id = ".$this->mCarMarginId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_margin_id=".$this->mCarMarginId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == "") {
			$asrErrReturn["code"] = '��س��к�����';
		}else{
			if(!$this->checkUniqueCode(strtolower($Mode) )){
				$asrErrReturn["code"] = '���ʫ�ӫ�͹';
			}		
		}			
        Return $asrErrReturn;
    }
	
    function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT code  FROM t_car_margin ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)));
		}else{
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)))." and car_margin_id != ".$this->mCarMarginId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				Customer Car Margin List

		Last update :		22 Mar 02

		Description:		Customer Car MarginList

*********************************************************/

class CarMarginList extends DataList {
	var $TABLE = "t_car_margin";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.car_margin_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_car_margin_item C ON C.car_margin_id = P.car_margin_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_car_margin_item C ON C.car_margin_id = P.car_margin_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarMargin($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarMarginId()."\"");
			if (($defaultId != null) && ($objItem->getCarMarginId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
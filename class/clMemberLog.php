<?
/*********************************************************
		Class :					MemberLog

		Last update :	  10 Jan 02

		Description:	  Class manage t_member_log table

*********************************************************/
 
class MemberLog extends DB{

	var $TABLE="t_member_log";

	var $mMemberLogId;
	function getMemberLogId() { return $this->mMemberLogId; }
	function setMemberLogId($data) { $this->mMemberLogId = $data; }
	
	var $mMemberId;
	function getMemberId() { return htmlspecialchars($this->mMemberId); }
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mDateLog;
	function getDateLog() { return htmlspecialchars($this->mDateLog); }
	function setDateLog($data) { $this->mDateLog = $data; }

	var $mAction;
	function getAction() { return htmlspecialchars($this->mAction); }
	function setAction($data) { $this->mAction = $data; }

	var $mModuleCode;
	function getModuleCode() { return htmlspecialchars($this->mModuleCode); }
	function setModuleCode($data) { $this->mModuleCode = $data; }

	var $mUrl;
	function getUrl() { return htmlspecialchars($this->mUrl); }
	function setUrl($data) { $this->mUrl = $data; }
	
	var $mIP;
	function getIP() { return htmlspecialchars($this->mIP); }
	function setIP($data) { $this->mIP = $data; }	
	
	function MemberLog($objData=NULL) {
        If ($objData->member_log_id !="") {
            $this->setMemberLogId($objData->member_log_id);
			$this->setMemberId($objData->member_id);
			$this->setDateLog($objData->date_log);
			$this->setModuleCode($objData->module_code);
			$this->setAction($objData->action);
			$this->setUrl($objData->url);
			$this->setIP($objData->ip);
        }
    }

	function init(){
	}


	function load() {

		if ($this->mMemberLogId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE member_log_id =".$this->mMemberLogId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberLog($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( memberId, date_log, module_code,url, ip, action ) "
						." VALUES ( '".$this->mMemberId."' , "
						."  '".$this->mDateLog."' , "
						."  '".$this->mModuleCode."' , "
						."  '".$this->mUrl."' , "
						."  '".$this->mIP."' , "
						."  '".$this->mAction."' ) ";
        $this->getConnection();
        If ($Result = $this->query($strSql)) { 
            $this->mMemberLogId = mysql_insert_id();
            return $this->mMemberLogId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET member_id = '".$this->mMemberId."' , "
						." date_log = '".$this->mDateLog."' , "
						." url = '".$this->mUrl."' , "
						." module_code = '".$this->mModuleCode."' , "
						." ip = '".$this->mIP."' , "
						." action = '".$this->mAction."'  "
						." WHERE  member_log_id = ".$this->mMemberLogId."  ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE member_log_id=".$this->mMemberLogId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mMemberId == "") $asrErrReturn["memberId"] = "��س��к���Ҫԡ";
		if ($this->mUrl == "") $asrErrReturn["url"] = "��س��к� URL";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				MemberLogList

		Last update :		10 Jan 07

		Description:		MemberLog List

*********************************************************/


class MemberLogList extends DataList {
	var $TABLE = "t_member_log";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_log_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new MemberLog($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
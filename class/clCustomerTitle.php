<?
/*********************************************************
		Class :					Customer Title

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_title table

*********************************************************/
 
class CustomerTitle extends DB{

	var $TABLE="t_customer_title";

	var $mCustomerTitleId;
	function getCustomerTitleId() { return $this->mCustomerTitleId; }
	function setCustomerTitleId($data) { $this->mCustomerTitleId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mLabel;
	function getLabel() { return htmlspecialchars($this->mLabel); }
	function setLabel($data) { $this->mLabel = $data; }	
	
	function CustomerTitle($objData=NULL) {
        If ($objData->customer_title_id !="") {
            $this->setCustomerTitleId($objData->customer_title_id);
			$this->setTitle($objData->title);
			$this->setLabel($objData->label);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCustomerTitleId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE customer_title_id =".$this->mCustomerTitleId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerTitle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerTitle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, label ) "
						." VALUES ( '".$this->mTitle."','".$this->mLabel."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCustomerTitleId = mysql_insert_id();
            return $this->mCustomerTitleId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , label = '".$this->mLabel."'  "
						." WHERE  customer_title_id = ".$this->mCustomerTitleId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE customer_title_id=".$this->mCustomerTitleId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Title List

		Last update :		22 Mar 02

		Description:		Customer Title List

*********************************************************/

class CustomerTitleList extends DataList {
	var $TABLE = "t_customer_title";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT customer_title_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerTitle($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCustomerTitleId()."\"");
			if (($defaultId != null) && ($objItem->getCustomerTitleId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCustomerTitleId()."\"");
			if (($defaultId != null) && ($objItem->getCustomerTitleId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
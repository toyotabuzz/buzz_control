<?
/*********************************************************
		Class :			  QuotationItemAddon

		Last update :	  11 Mar 21

		Description:	  Class manage jmquotation_item_addon table

*********************************************************/
 
class QuotationItemAddon extends DB{

	var $TABLE="jmquotation_item_addon";

	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_quotation_item_id;
	function get_quotation_item_id() { return $this->m_quotation_item_id; }
	function set_quotation_item_id($data) { $this->m_quotation_item_id = $data; }

	var $m_tmquotation_coverage_id;
	function get_tmquotation_coverage_id() { return $this->m_tmquotation_coverage_id; }
	function set_tmquotation_coverage_id($data) { $this->m_tmquotation_coverage_id = $data; }
	
	var $m_tmquotation_addon_id;
	function get_tmquotation_addon_id() { return $this->m_tmquotation_addon_id; }
	function set_tmquotation_addon_id($data) { $this->m_tmquotation_addon_id = $data; }
	
	var $m_description;
	function get_description() { return $this->m_description; }
	function set_description($data) { $this->m_description = $data; }

    var $m_value;
	function get_value() { return $this->m_value; }
	function set_value($data) { $this->m_value = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }
    
    function QuotationItemAddon($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_quotation_item_id($objData->quotation_item_id);
			$this->set_tmquotation_coverage_id($objData->tmquotation_coverage_id);
			$this->set_tmquotation_addon_id($objData->tmquotation_addon_id);
			$this->set_description($objData->description);
			$this->set_value($objData->value);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id." ";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->QuotationItemAddon($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->QuotationItemAddon($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_item_id , tmquotation_coverage_id , tmquotation_addon_id , description , value , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_quotation_item_id."' , "
		." '".$this->m_tmquotation_coverage_id."' , "
		." '".$this->m_tmquotation_addon_id."' , "
		." '".$this->m_description."' , "
		." '".$this->m_value."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." quotation_item_id = '".$this->m_quotation_item_id."' "
		." , tmquotation_coverage_id = '".$this->m_tmquotation_coverage_id."' "
		." , tmquotation_addon_id = '".$this->m_tmquotation_addon_id."' "
		." , description = '".$this->m_description."' "
		." , value = '".$this->m_value."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :			  Insure Company List

		Last update :	  11 Mar 21

		Description:	  Insure Company List

*********************************************************/

class QuotationItemAddonList extends DataList {
	var $TABLE = "jmquotation_item_addon";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT Q.id) as rowCount FROM ".$this->TABLE
			." Q  ".$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new QuotationItemAddon($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
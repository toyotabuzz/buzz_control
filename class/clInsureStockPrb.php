<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureStockPrb extends DB{

	var $TABLE="t_insure_stock_prb";

	var $m_insure_stock_id;
	function get_insure_stock_id() { return $this->m_insure_stock_id; }
	function set_insure_stock_id($data) { $this->m_insure_stock_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_stock_number;
	function get_stock_number() { return $this->m_stock_number; }
	function set_stock_number($data) { $this->m_stock_number = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }	
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	var $m_status_date;
	function get_status_date() { return $this->m_status_date; }
	function set_status_date($data) { $this->m_status_date = $data; }
	
	var $m_status_company_id;
	function get_status_company_id() { return $this->m_status_company_id; }
	function set_status_company_id($data) { $this->m_status_company_id = $data; }	
	
	var $m_status_department_id;
	function get_status_department_id() { return $this->m_status_department_id; }
	function set_status_department_id($data) { $this->m_status_department_id = $data; }	
	
	var $m_status_officer_id;
	function get_status_officer_id() { return $this->m_status_officer_id; }
	function set_status_officer_id($data) { $this->m_status_officer_id = $data; }	
	
	var $m_changes;
	function get_changes() { return $this->m_changes; }
	function set_changes($data) { $this->m_changes = $data; }		
	
	var $m_change_date;
	function get_change_date() { return $this->m_change_date; }
	function set_change_date($data) { $this->m_change_date = $data; }			
	
	var $m_change_form_id;
	function get_change_form_id() { return $this->m_change_form_id; }
	function set_change_form_id($data) { $this->m_change_form_id = $data; }		
	
	var $m_cancel;
	function get_cancel() { return $this->m_cancel; }
	function set_cancel($data) { $this->m_cancel = $data; }			
	
	var $m_cancel_date;
	function get_cancel_date() { return $this->m_cancel_date; }
	function set_cancel_date($data) { $this->m_cancel_date = $data; }				
	
	var $m_cancel_form_id;
	function get_cancel_form_id() { return $this->m_cancel_form_id; }
	function set_cancel_form_id($data) { $this->m_cancel_form_id = $data; }				
	
	var $m_stock_date;
	function get_stock_date() { return $this->m_stock_date; }
	function set_stock_date($data) { $this->m_stock_date = $data; }				
	
	var $m_stock_company_id;
	function get_stock_company_id() { return $this->m_stock_company_id; }
	function set_stock_company_id($data) { $this->m_stock_company_id = $data; }				
	
	var $m_stock_department_id;
	function get_stock_department_id() { return $this->m_stock_department_id; }
	function set_stock_department_id($data) { $this->m_stock_department_id = $data; }				
	
	var $m_stock_officer_id;
	function get_stock_officer_id() { return $this->m_stock_officer_id; }
	function set_stock_officer_id($data) { $this->m_stock_officer_id = $data; }				
	
	var $m_send_code;
	function get_send_code() { return $this->m_send_code; }
	function set_send_code($data) { $this->m_send_code = $data; }
	
	var $m_doc_send;
	function get_doc_send() { return $this->m_doc_send; }
	function set_doc_send($data) { $this->m_doc_send = $data; }	
	
	var $m_run_stock;
	function get_run_stock() { return $this->m_run_stock; }
	function set_run_stock($data) { $this->m_run_stock = $data; }		
	
	var $m_stock_code;
	function get_stock_code() { return $this->m_stock_code; }
	function set_stock_code($data) { $this->m_stock_code = $data; }

	var $m_p_stock_id;
	function get_p_stock_id() { return $this->m_p_stock_id; }
	function set_p_stock_id($data) { $this->m_p_stock_id = $data; }
	
	function InsureStockPrb($objData=NULL) {
        If ($objData->insure_stock_id !=""  ) {
			$this->set_insure_stock_id($objData->insure_stock_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_stock_number($objData->stock_number);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_remark($objData->remark);
			$this->set_status($objData->status);
			$this->set_status_date($objData->status_date);
			$this->set_status_company_id($objData->status_company_id);
			$this->set_status_department_id($objData->status_department_id);
			$this->set_status_officer_id($objData->status_officer_id);
			$this->set_changes($objData->changes);
			$this->set_change_date($objData->change_date);
			$this->set_change_form_id($objData->change_form_id);
			$this->set_cancel($objData->cancel);
			$this->set_cancel_date($objData->cancel_date);
			$this->set_cancel_form_id($objData->cancel_form_id);
			$this->set_stock_date($objData->stock_date);
			$this->set_stock_company_id($objData->stock_company_id);
			$this->set_stock_department_id($objData->stock_department_id);
			$this->set_stock_officer_id($objData->stock_officer_id);
			$this->set_send_code($objData->send_code);
			$this->set_doc_send($objData->doc_send);
			$this->set_run_stock($objData->run_stock);
			$this->set_stock_code($objData->stock_code);
			$this->set_p_stock_id($objData->p_stock_id);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_stock_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_stock_id =".$this->m_insure_stock_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureStockPrb($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureStockPrb($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(insure_stock_id) AS RSUM  FROM ".$this->TABLE."  "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , stock_number , insure_company_id , remark,
		 status , status_date , status_company_id, status_department_id, status_officer_id,  stock_date, stock_company_id, stock_department_id, stock_officer_id , run_stock, stock_code, p_stock_id  ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_stock_number."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_status."' , "
		." '".$this->m_status_date."' , "
		." '".$this->m_status_company_id."' , "
		." '".$this->m_status_department_id."' , "
		." '".$this->m_status_officer_id."' , "
		." '".$this->m_stock_date."' , "
		." '".$this->m_stock_company_id."' , "
		." '".$this->m_stock_department_id."' , "
		." '".$this->m_stock_officer_id."' ,  "		
		." '".$this->m_run_stock."' , "
		." '".$this->m_stock_code."' , "
		." '".$this->m_p_stock_id."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_stock_id = mysql_insert_id();
            return $this->m_insure_stock_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   insure_id = '".$this->m_insure_id."' "
		." , stock_number = '".$this->m_stock_number."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , remark = '".$this->m_remark."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateView(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  stock_number = '".$this->m_stock_number."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStockCode(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  stock_code = '".$this->m_stock_code."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $this->query($strSql);
		$this->unsetConnection();
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  status = '".$this->m_status."' "
		." , status_date = '".$this->m_status_date."' "
		." , status_company_id = '".$this->m_status_company_id."' "
		." , status_department_id = '".$this->m_status_department_id."' "	
		." , status_officer_id = '".$this->m_status_officer_id."' "	
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
		//echo $strSql;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateChange(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   changes = '".$this->m_changes."' "
		." , change_date = '".$this->m_change_date."' "
		." , change_form_id = '".$this->m_change_form_id."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateCancel(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  cancel = '".$this->m_cancel."' "
		." , cancel_date = '".$this->m_cancel_date."' "
		." , cancel_form_id = '".$this->m_cancel_form_id."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateSendCode(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  send_code = '".$this->m_send_code."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateDoc(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  doc_send = '".$this->m_doc_send."' "
		." WHERE insure_stock_id = ".$this->m_insure_stock_id." "; 
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_stock_id=".$this->m_insure_stock_id." ";
        $this->getConnection();
        $this->query($strSql);
		
	    $this->unsetConnection();
		return $result;
	   
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureStockPrbList extends DataList {
	var $TABLE = "t_insure_stock_prb";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_stock_id) as rowCount FROM ".$this->TABLE." O  "
		." LEFT JOIN t_insure I ON I.insure_id = O.insure_id  "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = IC.insure_customer_id "
		.$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT O.* FROM ".$this->TABLE." O "
		." LEFT JOIN t_insure I ON I.insure_id = O.insure_id  "
		." LEFT JOIN t_insure_car IC ON IC.car_id = I.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = IC.insure_customer_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureStockPrb($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_insure_stock_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_stock_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_insure_company_id()."</option>");
		}
		echo("</select>");
	}
}
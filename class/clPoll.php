<?
/*********************************************************
		Class :				Poll

		Last update :		20 Nov 02

		Description:		Class manage Pollt table

*********************************************************/
 
class Poll extends DB{

	var $TABLE="t_poll";
	
	var $mPollId;
	function getPollId() { return $this->mPollId; }
	function setPollId($data) { $this->mPollId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mShowOnWeb;
	function getShowOnWeb() { return htmlspecialchars($this->mShowOnWeb); }
	function setShowOnWeb($data) { $this->mShowOnWeb = $data; }

	var $mPostedDate;
	function getPostedDate() { return htmlspecialchars($this->mPostedDate); }
	function setPostedDate($data) { $this->mPostedDate = $data; }

	function Poll($objData=NULL) {
        If ($objData->pollId!="") {
            $this->setPollId($objData->pollId);
			$this->setTitle($objData->title);
			$this->setShowOnWeb($objData->showOnWeb);
			$this->setPostedDate($objData->postedDate);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));

	}
	
	function load() {

		if ($this->mPollId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE pollId =".$this->mPollId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Poll($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadByCondition($strCondition) {

		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Poll($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, postedDate, showOnWeb  )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".date("Y-m-d")."' , "
						."  '".$this->mShowOnWeb."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollId = mysql_insert_id();
            return $this->mPollId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." showOnWeb = '".$this->mShowOnWeb."'  "
						." WHERE  pollId = ".$this->mPollId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pollId=".$this->mPollId." ";
        $this->getConnection();
        $this->query($strSql);
       $strSql = " DELETE FROM t_poll_question "
                . " WHERE pollId=".$this->mPollId." ";
        $this->query($strSql);
       $strSql = " DELETE FROM t_poll_answer "
                . " WHERE pollId=".$this->mPollId." ";
        $this->query($strSql);

	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				if ($this->mTitle == "") $asrErrReturn["title"] = "Please enter Title";
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_FILE_NEWS.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_FILE_NEWS.$mFileName) And (is_file(PATH_FILE_NEWS.$mFileName) ) )
	    {
		    unLink(PATH_FILE_NEWS.$mFileName);
	    }
	}
}

/*********************************************************
		Class :				PollList

		Last update :		20 Nov 02

		Description:		Polluser list

*********************************************************/


class PollList extends DataList {
	var $TABLE = "t_poll";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pollId) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "				
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Poll($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
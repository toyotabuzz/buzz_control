<?
/*********************************************************
		Class :				BillPaymentItem

		Last update :		16 NOV 02

		Description:		Class manage category table

*********************************************************/
class BillPaymentItem extends DB{

	var $TABLE="t_bill_payment_item";
	
	var $m_bill_payment_item_id;
	function get_bill_payment_item_id() { return $this->m_bill_payment_item_id; }
	function set_bill_payment_item_id($data) { $this->m_bill_payment_item_id = $data; }

	var $m_bill_payment_id;
	function get_bill_payment_id() { return $this->m_bill_payment_id; }
	function set_bill_payment_id($data) { $this->m_bill_payment_id = $data; }

	var $m_ref1;
	function get_ref1() { return $this->m_ref1; }
	function set_ref1($data) { $this->m_ref1 = $data; }

	var $m_ref2;
	function get_ref2() { return $this->m_ref2; }
	function set_ref2($data) { $this->m_ref2 = $data; }

	var $m_amount;
	function get_amount() { return $this->m_amount; }
	function set_amount($data) { $this->m_amount = $data; }

	var $m_bank;
	function get_bank() { return $this->m_bank; }
	function set_bank($data) { $this->m_bank = $data; }

	var $m_pay_date;
	function get_pay_date() { return $this->m_pay_date; }
	function set_pay_date($data) { $this->m_pay_date = $data; }

	var $m_pay_data;
	function get_pay_data() { return $this->m_pay_data; }
	function set_pay_data($data) { $this->m_pay_data = $data; }

	var $m_branch_no;
	function get_branch_no() { return $this->m_branch_no; }
	function set_branch_no($data) { $this->m_branch_no = $data; }

	var $m_teller_no;
	function get_teller_no() { return $this->m_teller_no; }
	function set_teller_no($data) { $this->m_teller_no = $data; }
	
	var $m_import_date;
	function get_import_date() { return $this->m_import_date; }	
	function set_import_date($data) { $this->m_import_date = $data; }
	

	
	function BillPaymentItem($objData=NULL) {
        If (isset($objData)) {
			$this->set_bill_payment_item_id($objData->bill_payment_item_id);
			$this->set_bill_payment_id($objData->bill_payment_id);
			$this->set_ref1($objData->ref1);
			$this->set_ref2($objData->ref2);
			$this->set_amount($objData->amount);
			$this->set_bank($objData->bank);
			$this->set_pay_date($objData->pay_date);
			$this->set_pay_data($objData->pay_data);
			$this->set_branch_no($objData->branch_no);
			$this->set_teller_no($objData->teller_no);
			$this->set_import_date($objData->import_date);
        }
    }
	
	function init(){
		$this->set_title(stripslashes($this->m_title));
		$this->set_content(stripslashes($this->m_content));
	}
	
	function load() {

		if ($this->m_bill_payment_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE bill_payment_item_id =".$this->m_bill_payment_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BillPaymentItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strSql) {

		if ($strSql == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE ".$strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->BillPaymentItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}		

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( bill_payment_id , ref1 , ref2 , amount , bank , pay_date , pay_data , branch_no , teller_no ) " ." VALUES ( "
		." '".$this->m_bill_payment_id."' , "
		." '".$this->m_ref1."' , "
		." '".$this->m_ref2."' , "
		." '".$this->m_amount."' , "
		." '".$this->m_bank."' , "
		." '".$this->m_pay_date."' , "
		." '".$this->m_pay_data."' , "
		." '".$this->m_branch_no."' , "
		." '".$this->m_teller_no."' "
		." ) "; 
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->m_bill_payment_item_id = mysql_insert_id();
            return $this->m_bill_payment_item_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." bill_payment_id = '".$this->m_bill_payment_id."' "
		." , ref1 = '".$this->m_ref1."' "
		." , ref2 = '".$this->m_ref2."' "
		." , amount = '".$this->m_amount."' "
		." , bank = '".$this->m_bank."' "
		." , pay_date = '".$this->m_pay_date."' "
		." , pay_data = '".$this->m_pay_data."' "
		." , branch_no = '".$this->m_branch_no."' "
		." , teller_no = '".$this->m_teller_no."' "
		." WHERE bill_payment_item_id = ".$this->m_bill_payment_item_id." "; 
        $this->getConnection();
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE bill_payment_item_id=".$this->m_bill_payment_item_id." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {

    }
	
	Function checkDelete()
    {
        return true;
    }



}

/*********************************************************
		Class :				BillPaymentItemList

		Last update :		22 Mar 02

		Description:		BillPaymentItem user list

*********************************************************/


class BillPaymentItemList extends DataList {
	var $TABLE = "t_bill_payment_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT bill_payment_item_id) as rowCount FROM ".$this->TABLE." BI "
			." LEFT JOIN t_bill_payment B ON B.bill_payment_id = BI.bill_payment_id "
			." ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT BI.* ,B.import_date AS import_date  "
			."FROM ".$this->TABLE." BI "
			." LEFT JOIN t_bill_payment B ON B.bill_payment_id = BI.bill_payment_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new BillPaymentItem($row);
			}
			return true;
		} else {
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		echo ("<option value=\"0\">$header</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_bill_payment_item_id()."\"");
			if (($defaultId != null) && ($objItem->get_bill_payment_item_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}

}
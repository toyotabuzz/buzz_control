<?
/*********************************************************
		Class :				Headline

		Last update :		20 Nov 02

		Description:		Class manage Headline table

*********************************************************/

class Headline extends DB{

var $TABLE="t_headline";

var $m_headline_id;
function get_headline_id() { return $this->m_headline_id; }
function set_headline_id($data) { $this->m_headline_id = $data; }

var $m_main_category;
function get_main_category() { return $this->m_main_category; }
function set_main_category($data) { $this->m_main_category = $data; }

var $m_sub_category;
function get_sub_category() { return $this->m_sub_category; }
function set_sub_category($data) { $this->m_sub_category = $data; }

var $m_title;
function get_title() { return stripslashes($this->m_title); }
function set_title($data) { $this->m_title = $data; }

var $m_title_01;
function get_title_01() { return stripslashes($this->m_title_01); }
function set_title_01($data) { $this->m_title_01 = $data; }

var $m_content;
function get_content() { return stripslashes($this->m_content); }
function set_content($data) { $this->m_content = $data; }

var $m_content_01;
function get_content_01() { return stripslashes($this->m_content_01); }
function set_content_01($data) { $this->m_content_01 = $data; }

var $m_picture_01;
function get_picture_01() { return $this->m_picture_01; }
function set_picture_01($data) { $this->m_picture_01 = $data; }

var $m_picture_02;
function get_picture_02() { return $this->m_picture_02; }
function set_picture_02($data) { $this->m_picture_02 = $data; }

var $m_picture_03;
function get_picture_03() { return $this->m_picture_03; }
function set_picture_03($data) { $this->m_picture_03 = $data; }

var $m_date_add;
function get_date_add() { return $this->m_date_add; }
function set_date_add($data) { $this->m_date_add = $data; }

var $m_show_web;
function get_show_web() { return $this->m_show_web; }
function set_show_web($data) { $this->m_show_web = $data; }

var $m_show_index;
function get_show_index() { return $this->m_show_index; }
function set_show_index($data) { $this->m_show_index = $data; }

var $m_start_date;
function get_start_date() { return $this->m_start_date; }
function set_start_date($data) { $this->m_start_date = $data; }

var $m_end_date;
function get_end_date() { return $this->m_end_date; }
function set_end_date($data) { $this->m_end_date = $data; }

var $m_rank;
function get_rank() { return $this->m_rank; }
function set_rank($data) { $this->m_rank = $data; }

var $m_locks;
function get_locks() { return $this->m_locks; }
function set_locks($data) { $this->m_locks = $data; }

	function Headline($objData=NULL) {
        If ($objData->headline_id !="") {
			$this->set_headline_id($objData->headline_id);
			$this->set_main_category($objData->main_category);
			$this->set_sub_category($objData->sub_category);
			$this->set_title($objData->title);
			$this->set_content($objData->content);
			$this->set_title_01($objData->title_01);
			$this->set_content_01($objData->content_01);
			$this->set_picture_01($objData->picture_01);
			$this->set_picture_02($objData->picture_02);
			$this->set_picture_03($objData->picture_03);
			$this->set_date_add($objData->date_add);
			$this->set_show_web($objData->show_web);
			$this->set_show_index($objData->show_index);
			$this->set_start_date($objData->start_date);
			$this->set_end_date($objData->end_date);
			$this->set_rank($objData->rank);
			$this->set_locks($objData->locks);
        }
    }

	function init(){	
		$this->set_title(stripslashes($this->m_title));
	}
	
	function load() {

		if ($this->m_headline_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE headline_id = '".$this->m_headline_id."'";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Headline($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}

	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Headline($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." (  main_category, sub_category, title, content,  title_01, content_01, picture_01 ,picture_02,picture_03, date_add, show_web, show_index,  start_date, end_date, rank , locks ) " ." VALUES ( "

		." '".$this->m_main_category."' , "
		." '".$this->m_sub_category."' , "
		." '".addslashes($this->m_title)."' , "
		." '".addslashes($this->m_content)."' , "
		." '".addslashes($this->m_title_01)."' , "
		." '".addslashes($this->m_content_01)."' , "
		." '".$this->m_picture_01."' , "
		." '".$this->m_picture_02."' , "
		." '".$this->m_picture_03."' , "
		." '".date("Y-m-d")."' , "
		." '".$this->m_show_web."' , "
		." '".$this->m_show_index."' , "
		." '".$this->m_start_date."' , "
		." '".$this->m_end_date."' , "
		." '".$this->m_rank."' , "
		." '".$this->m_locks."'  "
		." ) "; 


        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->m_headline_id = mysql_insert_id();
			$this->unsetConnection();
            return $this->m_headline_id;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE 
		." SET " 
		."  main_category = '".$this->m_main_category."' "
		." , sub_category = '".$this->m_sub_category."' "
		." , title = '".addslashes($this->m_title)."' "
		." , content = '".addslashes($this->m_content)."' "
		." , title_01 = '".addslashes($this->m_title_01)."' "
		." , content_01 = '".addslashes($this->m_content_01)."' "
		." , picture_01 = '".$this->m_picture_01."' "
		." , picture_02 = '".$this->m_picture_02."' "
		." , picture_03 = '".$this->m_picture_03."' "
		." , show_web = '".$this->m_show_web."' "
		." , show_index = '".$this->m_show_index."' "
		." , start_date = '".$this->m_start_date."' "
		." , end_date = '".$this->m_end_date."' "
		." , rank = '".$this->m_rank."' "
		." , locks = '".$this->m_locks."' "
		." WHERE headline_id = ".$this->m_headline_id." "; 
        $this->getConnection();

		//echo $strSql;
        $this->query($strSql);
		$this->unsetConnection();

	}

	function updateRank(){

		$strSql = "UPDATE ".$this->TABLE 
		." SET " 
		."  rank = '".$this->m_rank."' "
		." WHERE headline_id = ".$this->m_headline_id." "; 
        $this->getConnection();

		//echo $strSql;
        $this->query($strSql);
		$this->unsetConnection();
	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE headline_id=".$this->m_headline_id." ";
        $this->getConnection();
        $this->query($strSql);
		$this->deleteFile($this->m_picture_01);
		$this->deleteFile($this->m_picture_02);
		$this->unsetConnection();
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				HeadlineList

		Last update :		20 Nov 02

		Description:		Headlineuser list

*********************************************************/


class HeadlineList extends DataList {
	var $TABLE = "t_headline";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT headline_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				$this->unsetConnection();
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
	
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Headline($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
		
}

}
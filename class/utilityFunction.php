<?php
function track_member_log($memberId, $module, $action, $ip = null, $url = null){
		$strSql = "INSERT INTO t_member_log "
						." ( member_id, date_log, module_code,url, ip, action ) "
						." VALUES ( '".$memberId."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$module."' , "
						."  '".$url."' , "
						."  '".$ip."' , "
						."  '".$action."' ) ";
	    $objDb = New Db;
	    $objDb->getConnection();
		$result = $objDb->query($strSql);
		//mysql_query($strSql) or die("Can't insert member log $strSql");
}

// �ѧ��ѹ����Ѻ�纻���ѵԡ���� SMS
function saveLogSendSMS ($moduleName=null, $to=null, $text=null, $result=null)
{
	if (!$moduleName&&!$to&&!$text) {
		return false;
	}

	$strSql = "INSERT INTO jusms_log "
		." ( module_name, sms_to, sms_text, api_respond, created_at, created_by, updated_at, updated_by ) "
		." VALUES ( '".$moduleName."' , "
		."  '".$to."' , "
		."  '".$text."' , "
		."  '".$result."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."', "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
	$objDb = New Db;
	$objDb->getConnection();
	$result = $objDb->query($strSql);
	return $result;
}

// �ŧ�ٻẺ�������Ѿ���������ö�� SMS ��
function convertMobileToSMS ($number)
{
	$number = preg_replace('/^0/', '66', str_replace('-', '', $number));
    return $number;
}

// �ѧ��ѹ����Ѻ�� SMS ��ҹ��� Service �ͧ https://ants.co.th
function sendSMSViaANTS ($to=null, $text=null)
{
	if (!$to&&!$text) {
		return false;
	}

	$sms = array(
		'to'   => convertMobileToSMS($to),
		'text' => iconv("tis-620", "utf-8", $text)
	);

	// set url
	$url = URL_SEND_SMS . '?'.http_build_query($sms);

	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $output contains the output string 
	$output = curl_exec($ch); 
	// print_r($output);exit;
	// close curl resource to free up system resources 
	curl_close($ch);
	// print_r($output);exit;
	return $output;
}

// �ѧ��ѹ����Ѻ�� SMS
// $to : 0123456789 (�ó���������)
// $to : 0123456789,0123456789 (�ó���������)
function sendSMS ($moduleName=null, $to=null, $text=null)
{
	// ��������Т�ͤ���������ҧ
	if (!$to&&!$text) {
		return false;
	}
	// ��Ǩ�ͺ�����١��ͧ�ͧ�������Ѿ��
	$_arrMobile = explode(',', $to);
	$arrMobile  = array();
	if ($_arrMobile) {
		foreach ($_arrMobile as $val) {
			preg_match_all('!\d+!', trim($val), $matches);
			if ($matches[0]!='') {
				$mobileNo = implode($matches[0]);
				if ($mobileNo==''||strlen($mobileNo)!=10) {
					return false;
				} else {
					$arrMobile[] = $mobileNo;
				}
			}
		}
	}
	// ǹ�觢�ͤ���
	foreach ($arrMobile as $mobileNo) {
		$result = sendSMSViaANTS($mobileNo, $text);
		saveLogSendSMS($moduleName, $mobileNo, $text, $result);
		// print_r($result);exit;
	}
	return true;
}

// �ѧ��ѹ����Ѻ�� Email
function sendEmailViaBuzzSure ($emailTo=null,$subject=null,$html=null)
{
	if (!$emailTo||!$subject) {
		return false;
	}

	$data = array(
		'emailTo' => $emailTo,
		'subject' => iconv("tis-620", "utf-8", $subject),
		'html' => iconv("tis-620", "utf-8", $html)
	);
	$url = getBuzzSureLink().'/backend/web/email/send?'.http_build_query($data);
	// create curl resource
	$ch = curl_init();
	// set url
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $output contains the output string 
	$output = curl_exec($ch); 
	// print_r($output);exit;
	// close curl resource to free up system resources 
	curl_close($ch);

	return $output;
}

// ��Ǩ�ͺ�����١��ͧ�ͧ email
function verifyEmail ($email) {
	return (preg_match("/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/", $email) || !preg_match("/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $email)) ? false : true;
}

// ���¡��ҹ�ԧ������Թ Payment gateway
function getPaymentLink ($refId='', $amount='', $customerName='', $customerTel='') {
	$arrData = array(
		'ref_id' => $refId,
		'amount' => str_replace(',', '', $amount),
		'customer_name' => iconv("tis-620", "utf-8", $customerName),
		'customer_tel' => $customerTel
	);

	$url = getBuzzSureLink().'/backend/web/payment/getlink?'.http_build_query($arrData);
	// echo $url;exit;
	// create curl resource
	$ch = curl_init();
	// set url
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $output contains the output string 
	$output = curl_exec($ch); 
	// echo '>>>>>>>';print_r($output);exit;
	// close curl resource to free up system resources 
	curl_close($ch);

	return $output;
}

//���¡����� �Ѻ�����Թ Payment Send SMS
function getReceivePaymentLink($InsureId = '', $InsureItemId = '' , $PhoneNumber = '' , $userId = '') {
	$arrData = array(
		't_insure_id' 		=> $InsureId,
		't_insureItem_id'	=> $InsureItemId,
		'phone_number' 		=> $PhoneNumber,
		'user_id'			=> $userId
	);

	$url = getBuzzSureLink().'/backend/web/sms/get-receive-payment-sms?'.http_build_query($arrData);
	// echo "<pre>"; print_r($url); echo "</pre>"; 
	// create curl resource
	$ch = curl_init();
	// set url
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $output contains the output string 
	$output = curl_exec($ch); 
	// echo "<pre>"; print_r($output); echo "</pre>"; exit;
	// close curl resource to free up system resources 
	curl_close($ch);

	return $output;
}

//���¡�� LINK PRINT PDF �Դ��â�� ��Ш��ա���� SMS
function getPrintPdfCloseSale($InsureId = '',$userId = '') {
	$arrData = array(
		't_insure_id' 		=> $InsureId,
		'user_id'			=> $userId
	);

	$url = getBuzzSureLink().'/backend/web/receipt/receipt/get-print-close-sale?'.http_build_query($arrData);
	// echo "<pre>"; print_r($url); echo "</pre>"; 
	// create curl resource
	$ch = curl_init();
	// set url
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $output contains the output string 
	$output = curl_exec($ch); 
	// echo "<pre>"; print_r($output); echo "</pre>"; exit;
	// close curl resource to free up system resources 
	curl_close($ch);

	return $output;
}

// ���¡ Short url ����Ѻ�� �Դ��â��
function getShortUrlCloseSale($insureId = '') {

	if ($insureId!='') {
		$url = getBuzzSureLink().'/backend/web/receipt/receipt/get-short-url-close-sale?'.http_build_query(array('t_insure_id'=>$insureId));
		// echo "<pre>"; print_r($url); echo "</pre>"; 
		// create curl resource
		$ch = curl_init();
		// set url
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// $output contains the output string 
		$output = curl_exec($ch); 
		// echo "<pre>"; print_r($output); echo "</pre>"; exit;
		// close curl resource to free up system resources 
		curl_close($ch);
		// echo "<pre>"; print_r($url); echo "</pre>"; exit;
	
		return $output;
	}
}
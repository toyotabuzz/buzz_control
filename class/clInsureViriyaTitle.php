<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureViriyaTitle extends DB{

	var $TABLE="t_insure_v_title";

	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	function InsureViriyaTitle($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_title($objData->title);
			$this->set_code($objData->code);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureViriyaTitle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureViriyaTitle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." (  title , code ) " ." VALUES ( "
		." '".$this->m_title."' , "
		." '".$this->m_code."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  title = '".$this->m_title."' "
		." , code = '".$this->m_code."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureViriyaTitleList extends DataList {
	var $TABLE = "t_insure_v_title";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureViriyaTitle($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureViriyaTitle($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_id()."\"");
			if (($defaultId != null) && ($objItem->get_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
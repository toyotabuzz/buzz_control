<?
/*********************************************************
		Class :					Customer Expect Car

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_expect_car table

*********************************************************/
 
class ExpectCar extends DB{

	var $TABLE="t_customer_expect_car";

	var $mExpectCarId;
	function getExpectCarId() { return $this->mExpectCarId; }
	function setExpectCarId($data) { $this->mExpectCarId = $data; }
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }

	var $mCarTypeId;
	function getCarTypeId() { return $this->mCarTypeId; }
	function setCarTypeId($data) { $this->mCarTypeId = $data; }

	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mCarColorId;
	function getCarColorId() { return $this->mCarColorId; }
	function setCarColorId($data) { $this->mCarColorId = $data; }
	
	function ExpectCar($objData=NULL) {
        If ($objData->expect_car_id !="" or $objData->customer_id !="" ) {
            $this->setExpectCarId($objData->expect_car_id);
			$this->setCustomerId($objData->customer_id);
			$this->setCarTypeId($objData->car_type_id);
			$this->setCarModelId($objData->car_model_id);
			$this->setCarColorId($objData->car_color_id);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->mExpectCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE expect_car_id =".$this->mExpectCarId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->ExpectCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->ExpectCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( customer_id, car_type_id, car_model_id, car_color_id ) "
						." VALUES ( '".$this->mCustomerId."', '".$this->mCarTypeId."', '".$this->mCarModelId."', '".$this->mCarColorId."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mExpectCarId = mysql_insert_id();
            return $this->mExpectCarId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET customer_id = '".$this->mCustomerId."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." , car_color_id = '".$this->mCarColorId."'  "
						." WHERE  expect_car_id = ".$this->mExpectCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE expect_car_id=".$this->mExpectCarId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		//if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��ͻ�����ö";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Expect Car List

		Last update :		22 Mar 02

		Description:		Expect Car List

*********************************************************/

class ExpectCarList extends DataList {
	var $TABLE = "t_customer_expect_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT expect_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	  // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new ExpectCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadAdvanceSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT E.customer_id) as rowCount FROM ".$this->TABLE." E  "
			." LEFT JOIN t_customer C ON E.customer_id = C.customer_id "			
			.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT E.customer_id  FROM ".$this->TABLE." E "
			." LEFT JOIN t_customer C ON E.customer_id = C.customer_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new ExpectCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadAdvanceSearc�hExcel() {
		// also gets latest delivery date
		$strSql = " SELECT DISTINCT E.customer_id  FROM ".$this->TABLE." E "
			." LEFT JOIN t_customer C ON E.customer_id = C.customer_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimit();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new ExpectCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
		
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT expect_car_id) as rowCount FROM ".$this->TABLE." E "
			." LEFT JOIN t_customer C ON E.customer_id = C.customer_id "			
			.$this->getFilterSQL();	// WHERE clause
	  // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." E "
			." LEFT JOIN t_customer C ON E.customer_id = C.customer_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new ExpectCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	

}
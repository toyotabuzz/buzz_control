<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureBeaItemDetail extends DB{

	var $TABLE="t_insure_bea_item_detail";

var $m_insure_bea_item_detail_id;
function get_insure_bea_item_detail_id() { return $this->m_insure_bea_item_detail_id; }
function set_insure_bea_item_detail_id($data) { $this->m_insure_bea_item_detail_id = $data; }

var $m_insure_bea_item_id;
function get_insure_bea_item_id() { return $this->m_insure_bea_item_id; }
function set_insure_bea_item_id($data) { $this->m_insure_bea_item_id = $data; }

var $m_insure_bea_id;
function get_insure_bea_id() { return $this->m_insure_bea_id; }
function set_insure_bea_id($data) { $this->m_insure_bea_id = $data; }

var $m_total;
function get_total() { return $this->m_total; }
function set_total($data) { $this->m_total = $data; }

var $m_year;
function get_year() { return $this->m_year; }
function set_year($data) { $this->m_year = $data; }

var $m_plan;
function get_plan() { return $this->m_plan; }
function set_plan($data) { $this->m_plan = $data; }

var $m_price1;
function get_price1() { return $this->m_price1; }
function set_price1($data) { $this->m_price1 = $data; }

var $m_price2;
function get_price2() { return $this->m_price2; }
function set_price2($data) { $this->m_price2 = $data; }

var $m_price3;
function get_price3() { return $this->m_price3; }
function set_price3($data) { $this->m_price3 = $data; }

var $m_price4;
function get_price4() { return $this->m_price4; }
function set_price4($data) { $this->m_price4 = $data; }

var $m_driver_age;
function get_driver_age() { return $this->m_driver_age; }
function set_driver_age($data) { $this->m_driver_age = $data; }



	function InsureBeaItemDetail($objData=NULL) {
        If ($objData->insure_bea_item_detail_id !="") {
			$this->set_insure_bea_item_detail_id($objData->insure_bea_item_detail_id);
			$this->set_insure_bea_item_id($objData->insure_bea_item_id);
			$this->set_insure_bea_id($objData->insure_bea_id);
			$this->set_total($objData->total);
			$this->set_year($objData->year);
			$this->set_plan($objData->plan);
			$this->set_price1($objData->price1);
			$this->set_price2($objData->price2);
			$this->set_price3($objData->price3);
			$this->set_price4($objData->price4);
			$this->set_driver_age($objData->driver_age);

			
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_bea_item_detail_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_bea_item_detail_id =".$this->m_insure_bea_item_detail_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBeaItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_bea_item_id , insure_bea_id , total , year , plan , 
		price1 , price2 , price3 , price4 , driver_age  ) " ." VALUES ( "
		." '".$this->m_insure_bea_item_id."' , "
		." '".$this->m_insure_bea_id."' , "
		." '".$this->m_total."' , "
		." '".$this->m_year."' , "
		." '".$this->m_plan."' , "
		." '".$this->m_price1."' , "
		." '".$this->m_price2."' , "
		." '".$this->m_price3."' , "
		." '".$this->m_price4."' , "
		." '".$this->m_driver_age."'  "

		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_bea_item_detail_id = mysql_insert_id();
            return $this->m_insure_bea_item_detail_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  total = '".$this->m_total."' "
		." , year = '".$this->m_year."' "
		." , plan = '".$this->m_plan."' "
		." , price1 = '".$this->m_price1."' "
		." , price2 = '".$this->m_price2."' "
		." , price3 = '".$this->m_price3."' "
		." , price4 = '".$this->m_price4."' "
		." , driver_age = '".$this->m_driver_age."' "

		
		." WHERE insure_bea_item_detail_id = ".$this->m_insure_bea_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_bea_item_detail_id=".$this->m_insure_bea_item_detail_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureBeaItemDetailList extends DataList {
	var $TABLE = "t_insure_bea_item_detail";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_item_detail_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaItemDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_item_detail_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_bea B ON B.insure_bea_id = P.insure_bea_id "
		." LEFT JOIN t_insure_bea_item C ON C.insure_bea_item_id = P.insure_bea_item_id "
		." LEFT JOIN t_insure_bea_car CC ON CC.insure_bea_id = B.insure_bea_id "

		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure_bea B ON B.insure_bea_id = P.insure_bea_id "
		." LEFT JOIN t_insure_bea_item C ON C.insure_bea_item_id = P.insure_bea_item_id "
		." LEFT JOIN t_insure_bea_car CC ON CC.insure_bea_id = B.insure_bea_id "

		.' '.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaItemDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bea_item_detail_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBeaItemDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {
			echo("<option value=\"".$objItem->get_insure_bea_item_detail_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_bea_item_detail_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
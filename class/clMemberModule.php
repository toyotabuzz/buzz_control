<?
/*********************************************************
		Class :					MemberModule

		Last update :	  10 Jan 02

		Description:	  Class manage t_member_module table

*********************************************************/
 
class MemberModule extends DB{

	var $TABLE="t_member_module";

	var $mMemberModuleId;
	function getMemberModuleId() { return $this->mMemberModuleId; }
	function setMemberModuleId($data) { $this->mMemberModuleId = $data; }
	
	var $mMemberId;
	function getMemberId() { return htmlspecialchars($this->mMemberId); }
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mModuleId;
	function getModuleId() { return htmlspecialchars($this->mModuleId); }
	function setModuleId($data) { $this->mModuleId = $data; }

	var $mModuleTitle;
	function getModuleTitle() { return htmlspecialchars($this->mModuleTitle); }
	function setModuleTitle($data) { $this->mModuleTitle = $data; }
	
	var $mModuleCode;
	function getModuleCode() { return htmlspecialchars($this->mModuleCode); }
	function setModuleCode($data) { $this->mModuleCode = $data; }	
	
	var $mModuleType;
	function getModuleType() { return htmlspecialchars($this->mModuleType); }
	function setModuleType($data) { $this->mModuleType = $data; }	
	
	var $mAccess;
	function getAccess() { return htmlspecialchars($this->mAccess); }
	function setAccess($data) { $this->mAccess = $data; }
	
	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }
	
	function MemberModule($objData=NULL) {
        If ($objData->member_module_id !="") {
            $this->setMemberModuleId($objData->member_module_id);
			$this->setMemberId($objData->member_id);
			$this->setModuleId($objData->module_id);
			$this->setModuleTitle($objData->module_title);
			$this->setModuleCode($objData->module_code);
			$this->setModuleType($objData->module_type);
			$this->setAccess($objData->access);
			$this->setStatus($objData->status);
			
        }
    }

	function init(){	
	}

	function load() {

		if ($this->mMemberModuleId == '') {
			return false;
		}
		$strSql = " SELECT MM.* , MO.title AS module_title, MO.code AS module_code FROM ".$this->TABLE." MM "
		." LEFT JOIN t_module MO ON MO.module_id = MM.module_id "
		."  WHERE member_module_id =".$this->mMemberModuleId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberModule($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = " SELECT MM.* , MO.title AS module_title, MO.code AS module_code FROM ".$this->TABLE." MM "
		." LEFT JOIN t_module MO ON MO.module_id = MM.module_id "
		."  WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberModule($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByConditionMenu($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = " SELECT MM.*  FROM ".$this->TABLE." MM "
		."  WHERE ".$strCondition;
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberModule($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( member_id, module_id, module_code, module_type, access, status ) "
						." VALUES ( '".$this->mMemberId."' , "
						."  '".$this->mModuleId."' , "
						."  '".$this->mModuleCode."' , "
						."  '".$this->mModuleType."' , "
						."  '".$this->mAccess."' , "
						."  '".$this->mStatus."' ) ";
        $this->getConnection();
        If ($Result = $this->query($strSql)) { 
            $this->mMemberModuleId = mysql_insert_id();
            return $this->mMemberModuleId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET member_id = '".$this->mMemberId."' , "
						." module_id = '".$this->mModuleId."' , "
						." module_type = '".$this->mModuleType."' , "
						." module_code = '".$this->mModuleCode."' , "
						." status = '".$this->mStatus."' , "
						." access = '".$this->mAccess."'  "
						." WHERE  member_module_id = ".$this->mMemberModuleId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE member_module_id=".$this->mMemberModuleId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mMemberId == "") $asrErrReturn["memberId"] = "��س��к���Ҫԡ";
		if ($this->mModuleId == "") $asrErrReturn["moduleId"] = "��س��кت��������";
		if ($this->mAccess == "") $asrErrReturn["access"] = "��س��кء����ҹ";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				MemberModuleList

		Last update :		10 Jan 07

		Description:		MemberModule List

*********************************************************/


class MemberModuleList extends DataList {
	var $TABLE = "t_member_module";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_module_id) as rowCount FROM ".$this->TABLE." MM  "
		." LEFT JOIN t_module MO ON MO.module_id = MM.module_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT MM.* , MO.title AS module_title, MO.code AS member_module_code FROM ".$this->TABLE." MM "
		." LEFT JOIN t_module MO ON MO.module_id = MM.module_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new MemberModule($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
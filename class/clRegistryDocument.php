<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_registry_document table

*********************************************************/
 
class RegistryDocument extends DB{

	var $TABLE="t_registry_document";

	var $mRegistryDocumentId;
	function getRegistryDocumentId() { return $this->mRegistryDocumentId; }
	function setRegistryDocumentId($data) { $this->mRegistryDocumentId = $data; }
	
	var $mOrderId;
	function getOrderId() { return htmlspecialchars($this->mOrderId); }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mRegistryConditionId;
	function getRegistryConditionId() { return htmlspecialchars($this->mRegistryConditionId); }
	function setRegistryConditionId($data) { $this->mRegistryConditionId = $data; }
	
	function RegistryDocument($objData=NULL) {
        If ($objData->registry_document_id !="") {
            $this->setRegistryDocumentId($objData->registry_document_id);
			$this->setOrderId($objData->order_id);
			 $this->setRegistryConditionId($objData->registry_condition_id);
        }
    }

	function init(){
		$this->setOrderId(stripslashes($this->mOrderId));
	}
		
	function load() {

		if ($this->mRegistryDocumentId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE registry_document_id =".$this->mRegistryDocumentId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryDocument($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mRegistryDocumentId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE registry_document_id =".$this->mRegistryDocumentId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryDocument($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strDocument) {

		if ($strDocument == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strDocument;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryDocument($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_id, registry_condition_id ) "
						." VALUES ( '".$this->mOrderId."',  '".$this->mRegistryConditionId."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mRegistryDocumentId = mysql_insert_id();
            return $this->mRegistryDocumentId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_id = '".$this->mOrderId."' , "
						." registry_condition_id = '".$this->mRegistryConditionId."'  "
						." WHERE  registry_document_id = ".$this->mRegistryDocumentId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE registry_document_id=".$this->mRegistryDocumentId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class RegistryDocumentList extends DataList {
	var $TABLE = "t_registry_document";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT registry_document_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new RegistryDocument($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getRegistryDocumentId()."\"");
			if (($defaultId != null) && ($objItem->getRegistryDocumentId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getOrderId()."</option>");
		}
		echo("</select>");
	}			
	
}
<?

/* VARIABLE FUNCTIONS */

	/**
	* Returns the value of a variable from the globals collection.
	 *
	 * Returns the value of a variable from PHP global variables.
	 * Accesses the value in a portable manner by testing for the presence
	 * of the $_REQUEST object.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return string The value of the variable
	 */
	function getRequestVariable($strParam) 
	{
		$value="";
		if (!empty($_REQUEST)) {
			if (!empty($_REQUEST[$strParam])) {$value=$_REQUEST[$strParam];}
		} else {
			eval("global \$".$strParam."; \$value=\$".$strParam.";");
		}
		return $value;
	}
	
	/**
	 * Returns the value of a server variable from the globals collection.
	 *
	 * Returns the value of a server variable from PHP global variables.
	 * Accesses the value in a portable manner by testing for the presence
	 * of the $_SERVER object.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return string The value of the variable
	 */
	function getServerVariable($strParam) 
	{
		$value="";
		if (!empty($_SERVER)) {
			if (!empty($_SERVER[$strParam])) {$value=$_SERVER[$strParam];}
		} else {
			eval("global \$".$strParam."; \$value=\$".$strParam.";");
		}
		return $value;
	}	
	
	
	/**
	 * Returns the value of a session variable from the globals collection.
	 *
	 * Returns the value of a session variable from PHP global variables.
	 * Accesses the value in a portable manner by testing for the presence
	 * of the $_SESSION object.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return string The value of the variable
	 */
	function getSessionVariable($strParam) {
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			if (isset($_SESSION[$strParam])) {
				return $_SESSION[$strParam];
			} else {return false;}
		} else if (isset($HTTP_SESSION_VARS)) {
			if (isset($HTTP_SESSION_VARS[$strParam])) {
				return $HTTP_SESSION_VARS[$strParam];
			} else {return false;}
		}
	}
	
	
	/**
	 * Set the value of a session variable from the globals collection.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@param string $value The value
	 *@return void 
	 */
	function setSessionVariable($strParam,$value) {
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			$_SESSION[$strParam]=$value;
		} else if (isset($HTTP_SESSION_VARS)) {
			eval("global \$".$strParam.";");
			eval("\$".$strParam."=\"\";");
			eval("\$".$strParam."= \"".$value."\";");
			eval("session_register('".$strParam."');"); 
		}	
	
	}
	
	
	/**
	 * Set the value of a session variable from the globals collection to some object.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@param string $obj The object
	 *@return void 
	 */
	function setObjectInSession($strParam,$obj) {
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			$_SESSION[$strParam]=$obj;
		} else if (isset($HTTP_SESSION_VARS)) {
			$thisObj = $obj;
			$stringObj = serialize( $thisObj );
			eval("global \$".$strParam.";");
			eval("\$".$strParam."=\"\";");
			eval("\$".$strParam."= \"\$stringObj\";");
			eval("session_register('".$strParam."');"); 
		}	
	
	}
	
	
	/**
	 * Get the value of a object in a session variable from the globals collection.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return object An object stored in session
	 */
	function getObjectFromSession($strParam) {	
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			if (isset($_SESSION[$strParam])) {
				return $_SESSION[$strParam];
			} else {return false;}
		} else if (isset($HTTP_SESSION_VARS)) {
			if (isset($HTTP_SESSION_VARS[$strParam])) {
				return unserialize($HTTP_SESSION_VARS[$strParam]);
			} else {return false;}
		}
	}
	
	
	/**
	 * Unset the value of a session variable from the globals collection.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return void 
	 */
	function unsetSessionVariable($strParam) {
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			if (isset($_SESSION[$strParam])) {
				unset($_SESSION[$strParam]);
			} else {return false;}
		} else if (isset($HTTP_SESSION_VARS)) {
			if (isset($HTTP_SESSION_VARS[$strParam])) {
				unset($HTTP_SESSION_VARS[$strParam]);
			} else {return false;}
		}
	}
	
	/**
	 * Test for the existence of a session variable from the globals collection.
	 *
	 *@access public
	 *@param string $strParam The name of the variable
	 *@return boolean Whether the session variable exists 
	 */
	function issetSessionVariable($strParam) {
		global $HTTP_SESSION_VARS;
		if (isset($_SESSION)) {
			if (isset($_SESSION[$strParam])) {
				return true;
			} else {return false;}
		} else if (isset($HTTP_SESSION_VARS)) {
			if (isset($HTTP_SESSION_VARS[$strParam])) {
				return true;
			} else {return false;}
		}
	}
	
	
	
	/**
	 * Print all variables contained in the globals collection.	 
	 */
	function PrintAllServerVariables() {
		foreach($GLOBALS as $key=>$value) {
			print $key . " = " . $value . "<br />";
		}	
	}
	
?>
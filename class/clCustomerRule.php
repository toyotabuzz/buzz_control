<?
/*********************************************************
		Class :					Customer Rule

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_rule table

*********************************************************/
 
class CustomerRule extends DB{

	var $TABLE="t_customer_rule";

	var $mRuleId;
	function getRuleId() { return $this->mRuleId; }
	function setRuleId($data) { $this->mRuleId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mRuleType;
	function getRuleType() { return htmlspecialchars($this->mRuleType); }
	function setRuleType($data) { $this->mRuleType = $data; }
	
	var $mRuleName;
	function getRuleName() { return htmlspecialchars($this->mRuleName); }
	function setRuleName($data) { $this->mRuleName = $data; }
	
	var $mRuleCondition;
	function getRuleCondition() { return htmlspecialchars($this->mRuleCondition); }
	function setRuleCondition($data) { $this->mRuleCondition = $data; }
	
	var $mRuleField;
	function getRuleField() { return htmlspecialchars($this->mRuleField); }
	function setRuleField($data) { $this->mRuleField = $data; }	
	
	function CustomerRule($objData=NULL) {
        If ($objData->rule_id !="") {
            $this->setRuleId($objData->rule_id);
			$this->setTitle($objData->title);
			$this->setRuleType($objData->rule_type);
			$this->setRuleName($objData->rule_name);
			$this->setRuleCondition($objData->rule_condition);
			$this->setRuleField($objData->rule_field);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mRuleId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE rule_id =".$this->mRuleId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerRule($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerRule($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, rule_type, rule_name, rule_condition, rule_field ) "
						." VALUES ( '".$this->mTitle."' ,  '".$this->mRuleType."' ,  '".$this->mRuleName."' ,  '".$this->mRuleCondition."'  ,  '".$this->mRuleField."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mRuleId = mysql_insert_id();
            return $this->mRuleId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , rule_type = '".$this->mRuleType."'  "
						." , rule_name = '".$this->mRuleName."'  "
						." , rule_condition = '".$this->mRuleCondition."'  "
						." , rule_field = '".$this->mRuleField."'  "
						." WHERE  rule_id = ".$this->mRuleId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE rule_id=".$this->mRuleId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت����ô�١���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Rule List

		Last update :		22 Mar 02

		Description:		Customer Rule List

*********************************************************/

class CustomerRuleList extends DataList {
	var $TABLE = "t_customer_rule";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT rule_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerRule($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($countryName, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$countryName."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getRuleId()."\"");
			if (($defaultId != null) && ($objItem->getRuleId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}		
	
	
}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureAcc extends DB{

	var $TABLE="t_insure_acc";

	var $m_insure_acc_id;
	function get_insure_acc_id() { return $this->m_insure_acc_id; }
	function set_insure_acc_id($data) { $this->m_insure_acc_id = $data; }
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }
	
	var $m_acc;
	function get_acc() { return $this->m_acc; }
	function set_acc($data) { $this->m_acc = $data; }


	function InsureAcc($objData=NULL) {
        If ($objData->insure_acc_id !="") {
			$this->set_insure_acc_id($objData->insure_acc_id);
			$this->set_broker_id($objData->broker_id);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_acc($objData->acc);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_acc_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_acc_id =".$this->m_insure_acc_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAcc($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAcc($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( broker_id , insure_company_id , acc ) " ." VALUES ( "
		." '".$this->m_broker_id."' , "
		." '".$this->m_insure_company_id."' , "
		." '".$this->m_acc."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_acc_id = mysql_insert_id();
            return $this->m_insure_acc_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." broker_id = '".$this->m_broker_id."' "
		." , insure_company_id = '".$this->m_insure_company_id."' "
		." , acc = '".$this->m_acc."' "
		." WHERE insure_acc_id = ".$this->m_insure_acc_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_acc_id=".$this->m_insure_acc_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureAccList extends DataList {
	var $TABLE = "t_insure_acc";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_acc_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_company  T ON T.insure_company_id = P.insure_company_id ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." P  LEFT JOIN t_insure_company  T ON T.insure_company_id = P.insure_company_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureAcc($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
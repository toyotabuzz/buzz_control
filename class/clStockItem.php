<?
/*********************************************************
		Class :					Stock Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_item table

*********************************************************/
 
class StockItem extends DB{

	var $TABLE="t_stock_item";

	var $mStockItemId;
	function getStockItemId() { return $this->mStockItemId; }
	function setStockItemId($data) { $this->mStockItemId = $data; }
	
	var $mStockId;
	function getStockId() { return $this->mStockId; }
	function setStockId($data) { $this->mStockId = $data; }
	
	var $mStockProductId;
	function getStockProductId() { return $this->mStockProductId; }
	function setStockProductId($data) { $this->mStockProductId = $data; }	
	
	var $mStockProductTitle;
	function getStockProductTitle() { return $this->mStockProductTitle; }
	function setStockProductTitle($data) { $this->mStockProductTitle = $data; }		
	
	//999 = �ԡ���µ����ػ�ó�ҡ������
	var $mLot;
	function getLot() { return $this->mLot; }
	function setLot($data) { $this->mLot = $data; }	
	
	var $mStockPlaceId;
	function getStockPlaceId() { return $this->mStockPlaceId; }
	function setStockPlaceId($data) { $this->mStockPlaceId = $data; }	
	
	var $mStockPlaceTitle;
	function getStockPlaceTitle() { return $this->mStockPlaceTitle; }
	function setStockPlaceTitle($data) { $this->mStockPlaceTitle = $data; }		

	var $mStockCargoTitle;
	function getStockCargoTitle() { return $this->mStockCargoTitle; }
	function setStockCargoTitle($data) { $this->mStockCargoTitle = $data; }		
	
	var $mQty;
	function getQty() { return $this->mQty; }
	function setQty($data) { $this->mQty = $data; }	
	
	var $mPrice;
	function getPrice() { return $this->mPrice; }
	function setPrice($data) { $this->mPrice = $data; }	
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }

	var $mPrItemId;
	function getPrItemId() { return $this->mPrItemId; }
	function setPrItemId($data) { $this->mPrItemId = $data; }
	
	var $mStockOrderId;
	function getStockOrderId() { return $this->mStockOrderId; }
	function setStockOrderId($data) { $this->mStockOrderId = $data; }
	
	var $mStockItemDate;
	function getStockItemDate() { return $this->mStockItemDate; }
	function setStockItemDate($data) { $this->mStockItemDate = $data; }
	
	var $mStockMemberId;
	function getStockMemberId() { return $this->mStockMemberId; }
	function setStockMemberId($data) { $this->mStockMemberId = $data; }
	
	function StockItem($objData=NULL) {
        If ($objData->stock_item_id !="" OR $objData->stock_product_id != "") {
            $this->setStockItemId($objData->stock_item_id);
			$this->setStockId($objData->stock_id);
			$this->setStockProductId($objData->stock_product_id);
			$this->setStockProductTitle($objData->stock_product_title);
			$this->setLot($objData->lot);
			$this->setStockPlaceId($objData->stock_place_id);
			$this->setStockPlaceTitle($objData->stock_place_title);
			$this->setStockCargoTitle($objData->stock_cargo_title);
			$this->setQty($objData->qty);
			$this->setPrice($objData->price);
			$this->setStatus($objData->status);
			$this->setPrItemId($objData->pr_item_id);
			$this->setStockOrderId($objData->stock_order_id);
			$this->setStockItemDate($objData->stock_item_date);
			$this->setStockMemberId($objData->stock_member_id);
        }
    }

	function init(){
		$this->setLot(stripslashes($this->mLot));
	}
	
	function load() {

		if ($this->mStockItemId == '') {
			return false;
		}
		$strSql = "SELECT S.*,  SP.title AS stock_product_title, SL.title AS stock_place_title, SC.title AS stock_cargo_title FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		."  WHERE stock_item_id =".$this->mStockItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT S.*,  SP.title AS stock_product_title, SL.title AS stock_place_title, SC.title AS stock_cargo_title FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function getStockIn($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SUM(qty) AS val FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SL.stock_cargo_id  "
		."  WHERE status=0 AND ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockItem($row);
                $result->freeResult();
				return $row->val ;
            }
        }
		return false;
	}	
	
	function getStockOut($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SUM(qty) AS val FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SL.stock_cargo_id  "		
		."  WHERE status=1 AND ".$strCondition;

		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockItem($row);
                $result->freeResult();
				return $row->val;
            }
        }
		return false;
	}		
	
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_id,stock_product_id, stock_place_id, lot,  price, status, pr_item_id, stock_order_id , stock_item_date, qty , stock_member_id ) "
						." VALUES ( '".$this->mStockId
						."' , '".$this->mStockProductId
						."' , '".$this->mStockPlaceId
						."', '".$this->mLot
						."' ,  '".$this->mPrice
						."' ,  '".$this->mStatus
						."' ,  '".$this->mPrItemId
						."' , '".$this->mStockOrderId
						."' , '".date("Y-m-d H:i:s")
						."' , '".$this->mQty
						."' , '".$this->mStockMemberId."' ) ";
						
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockItemId = mysql_insert_id();
            return $this->mStockItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_id = '".$this->mStockId."'  "
						." , stock_product_id = '".$this->mStockProductId."'  "
						." , lot = '".$this->mLot."'  "
						." , qty = '".$this->mQty."'  "
						." , pr_item_id = '".$this->mPrItemId."'  "
						." , stock_order_id = '".$this->mStockOrderId."'  "
						." , price = '".$this->mPrice."'  "
						." , status = '".$this->mStatus."'  "
						." , stock_place_id = '".$this->mStockPlaceId."'  "
						." WHERE  stock_item_id = ".$this->mStockItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateCondition($strSql){
        $this->getConnection();
        return $this->query($strSql);
	}	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_item_id=".$this->mStockItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strSql) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strSql." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockId == "") $asrErrReturn["stock_id"] = "��س��к�";		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Item List

		Last update :		22 Mar 02

		Description:		Customer Stock Item List

*********************************************************/

class StockItemList extends DataList {
	var $TABLE = "t_stock_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_item_id) as rowCount FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		." LEFT JOIN t_stock  SS ON SS.stock_id = S.stock_id  "
		." LEFT JOIN t_pr_item PR ON PR.pr_item_id = S.pr_item_id  "
		." LEFT JOIN t_dealer D ON PR.dealer_id = D.dealer_id  "
		." LEFT JOIN t_po PO ON PR.po_id = PO.po_id  "		
		." LEFT JOIN t_stock_car SCC ON SCC.stock_car_id = SS.stock_car_id  "		
		.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = "SELECT S.*,  SP.title AS stock_product_title, SL.title AS stock_place_title, SC.title AS stock_cargo_title FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		." LEFT JOIN t_stock  SS ON SS.stock_id = S.stock_id  "
		." LEFT JOIN t_pr_item PR ON PR.pr_item_id = S.pr_item_id  "
		." LEFT JOIN t_dealer D ON PR.dealer_id = D.dealer_id  "
		." LEFT JOIN t_po PO ON PR.po_id = PO.po_id  "
		." LEFT JOIN t_stock_car SCC ON SCC.stock_car_id = SS.stock_car_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSum() {

		$strSql = "SELECT DISTINCT S.stock_product_id, S.stock_place_id FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
		
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_item_id) as rowCount FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = "SELECT S.*,  SP.title AS stock_product_title, SL.title AS stock_place_title, SC.title AS stock_cargo_title FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = S.stock_product_id  "
		." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id  "
		." LEFT JOIN t_stock_place SL ON SL.stock_place_id = S.stock_place_id  "
		." LEFT JOIN t_stock_cargo  SC ON SL.stock_cargo_id = SC.stock_cargo_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
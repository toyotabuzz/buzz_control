<?
/*********************************************************
		Class :					Sale

		Last update :	  10 Jan 02

		Description:	  Class manage t_sale table

*********************************************************/
 
class Sale extends DB{

	var $TABLE="t_sale";

	var $mSaleId;
	function getSaleId() { return $this->mSaleId; }
	function setSaleId($data) { $this->mSaleId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	function Sale($objData=NULL) {
        If ($objData->sale_id !="") {
            $this->setSaleId($objData->sale_id);
			$this->setTitle($objData->title);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mSaleId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE sale_id =".$this->mSaleId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Sale($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Sale($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ) "
						." VALUES ( '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mSaleId = mysql_insert_id();
            return $this->mSaleId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." WHERE  sale_id = ".$this->mSaleId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE sale_id=".$this->mSaleId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��;�ѡ�ҹ���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Group List

		Last update :		22 Mar 02

		Description:		Customer Group List

*********************************************************/

class SaleList extends DataList {
	var $TABLE = "t_sale";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT sale_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Sale($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getSaleId()."\"");
			if (($defaultId != null) && ($objItem->getSaleId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}				
	
	
	function printSelectCombo($name, $defaultId = null ,$header = null) {
		echo ("<cr:combobox id=\"".$name."\" autosize=\"true\" name=\"".$name."\" type=\"0\"   selectedIndex=\"1\"  autosuggest=\"true\">\n");
		if ($header != "") {echo ("<option value=\"0\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getSaleId()."\"");
			if (($defaultId != null) && ($objItem->getSaleId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</cr:combobox>");
	}					
	
}
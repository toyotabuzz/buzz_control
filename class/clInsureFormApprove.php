<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureFormApprove extends DB{

	var $TABLE="t_insure_form_approve";

	var $m_insure_form_approve_id;
	function get_insure_form_approve_id() { return $this->m_insure_form_approve_id; }
	function set_insure_form_approve_id($data) { $this->m_insure_form_approve_id = $data; }
	
	var $m_insure_form_id;
	function get_insure_form_id() { return $this->m_insure_form_id; }
	function set_insure_form_id($data) { $this->m_insure_form_id = $data; }
	
	var $m_approve_type;
	function get_approve_type() { return $this->m_approve_type; }
	function set_approve_type($data) { $this->m_approve_type = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }

	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	function InsureFormApprove($objData=NULL) {
        If ($objData->insure_form_approve_id !="") {
			$this->set_insure_form_approve_id($objData->insure_form_approve_id);
			$this->set_insure_form_id($objData->insure_form_id);
			$this->set_approve_type($objData->approve_type);
			$this->set_remark($objData->remark);
			$this->set_date_add($objData->date_add);
			$this->set_status($objData->status);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_form_approve_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_form_approve_id =".$this->m_insure_form_approve_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFormApprove($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFormApprove($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_form_id,  approve_type,  remark, date_add , status ) " ." VALUES ( "
		
		." '".$this->m_insure_form_id."' , "
		." '".$this->m_approve_type."' , "
		." '".$this->m_remark."' , "
		." '".date("Y-m-d H:i:s")."' , "
		." '".$this->m_status."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_form_approve_id = mysql_insert_id();
            return $this->m_insure_form_approve_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "

		."  approve_type = '".$this->m_approve_type."' "
		." , remark = '".$this->m_remark."' "

		." WHERE insure_form_approve_id = ".$this->m_insure_form_approve_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  status = '".$this->m_status."' "
		." WHERE insure_form_approve_id = ".$this->m_insure_form_approve_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_form_approve_id=".$this->m_insure_form_approve_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureFormApproveList extends DataList {
	var $TABLE = "t_insure_form_approve";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT approve_date) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureFormApprove($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }


	
}
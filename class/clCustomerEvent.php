<?
/*********************************************************
		Class :					Customer Event

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_event table

*********************************************************/
 
class CustomerEvent extends DB{

	var $TABLE="t_customer_event";

	var $mEventId;
	function getEventId() { return $this->mEventId; }
	function setEventId($data) { $this->mEventId = $data; }
	
	var $mEventCode;
	function getEventCode() { return htmlspecialchars($this->mEventCode); }
	function setEventCode($data) { $this->mEventCode = $data; }	
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mStartDate;
	function getStartDate() { return htmlspecialchars($this->mStartDate); }
	function setStartDate($data) { $this->mStartDate = $data; }
	
	var $mEndDate;
	function getEndDate() { return htmlspecialchars($this->mEndDate); }
	function setEndDate($data) { $this->mEndDate = $data; }
	
	var $mBudget;
	function getBudget() { return htmlspecialchars($this->mBudget); }
	function setBudget($data) { $this->mBudget = $data; }
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mBoothPlace;
	function getBoothPlace() { return htmlspecialchars($this->mBoothPlace); }
	function setBoothPlace($data) { $this->mBoothPlace = $data; }	
	
	var $mBoothArea;
	function getBoothArea() { return htmlspecialchars($this->mBoothArea); }
	function setBoothArea($data) { $this->mBoothArea = $data; }					
	
	var $mActivity;
	function getActivity() { return htmlspecialchars($this->mActivity); }
	function setActivity($data) { $this->mActivity = $data; }	
	
	var $mProblem;
	function getProblem() { return htmlspecialchars($this->mProblem); }
	function setProblem($data) { $this->mProblem = $data; }	
	
	function CustomerEvent($objData=NULL) {
        If ($objData->event_id !="") {
            $this->setEventId($objData->event_id);
			$this->setEventCode($objData->event_code);
			$this->setTitle($objData->title);
			$this->setStartDate($objData->start_date);
			$this->setEndDate($objData->end_date);
			$this->setBudget($objData->budget);
			$this->setRemark($objData->remark);
			$this->setBoothPlace($objData->booth_place);
			$this->setBoothArea($objData->booth_area);
			$this->setActivity($objData->activity);
			$this->setProblem($objData->problem);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mEventId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE event_id =".$this->mEventId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerEvent($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerEvent($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title,event_code, start_date, end_date, budget, remark, booth_place, booth_area, activity, problem  ) "
						." VALUES ( '".$this->mTitle."' ,'".$this->mEventCode."' ,'".$this->mStartDate."' ,'".$this->mEndDate."' ,'".$this->mBudget."' ,'".$this->mRemark."' ,'".$this->mBoothPlace."' ,'".$this->mBoothArea."' ,'".$this->mActivity."' ,'".$this->mProblem."'   ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mEventId = mysql_insert_id();
            return $this->mEventId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , event_code = '".$this->mEventCode."'  "
						." , start_date = '".$this->mStartDate."'  "
						." , end_date = '".$this->mEndDate."'  "
						." , budget = '".$this->mBudget."'  "
						." , remark = '".$this->mRemark."'  "
						." , booth_place = '".$this->mBoothPlace."'  "
						." , booth_area = '".$this->mBoothArea."'  "
						." , activity = '".$this->mActivity."'  "
						." , problem = '".$this->mProblem."'  "
						." WHERE  event_id = ".$this->mEventId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE event_id=".$this->mEventId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت����ô�١���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Event List

		Last update :		22 Mar 02

		Description:		Customer Event List

*********************************************************/

class CustomerEventList extends DataList {
	var $TABLE = "t_customer_event";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT event_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerEvent($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT event_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerEvent($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"0\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getEventId()."\"");
			if (($defaultId != null) && ($objItem->getEventId() == $defaultId)) {
				echo(" selected");
			}
			echo(">[".$objItem->getEventCode()."] ".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			

}
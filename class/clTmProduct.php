<?

/*********************************************************
		Class :			  TmProduct

		Last update :	  24 april 2021

		Description:	  Class manage tmproduct table

 *********************************************************/

class TmProduct extends DB
{

	var $TABLE = "tmproduct";

	var $id;
	function get_id()
	{
		return $this->id;
	}
	function set_id($data)
	{
		$this->id = $data;
	}

	var $insure_id;
	function get_insure_id()
	{
		return $this->insure_id;
	}
	function set_insure_id($data)
	{
		$this->insure_id = $data;
	}

	var $repair_type_id;
	function get_repair_typr_id()
	{
		return $this->repair_type_id;
	}
	function set_repair_type_id($data)
	{
		$this->repair_type_id = $data;
	}

	var $repair_type;
	function get_repair_typr()
	{
		return $this->repair_type;
	}
	function set_repair_type($data)
	{
		$this->repair_type = $data;
	}

	var $policy_type_id;
	function get_policy_type_id()
	{
		return $this->policy_type_id;
	}
	function set_policy_type_id($data)
	{
		$this->policy_type_id = $data;
	}

	var $remark;
	function get_remark()
	{
		return $this->remark;
	}
	function set_remark($data)
	{
		$this->remark = $data;
	}

	var $created_at;
	function get_created_at()
	{
		return $this->created_at;
	}
	function set_created_at($data)
	{
		$this->created_at = $data;
	}

	var $created_by;
	function get_created_by()
	{
		return $this->created_by;
	}
	function set_created_by($data)
	{
		$this->created_by = $data;
	}

	var $updated_at;
	function get_updated_at()
	{
		return $this->updated_at;
	}
	function set_updated_at($data)
	{
		$this->updated_at = $data;
	}

	var $updated_by;
	function get_updated_by()
	{
		return $this->updated_by;
	}
	function set_updated_by($data)
	{
		$this->updated_by = $data;
	}

	function TmProduct($objData = NULL)
	{
		if ($objData->id != "") {
			$this->set_id($objData->id);
			$this->set_insure_id($objData->insure_id);
			$this->set_repair_type_id($objData->repair_type_id);
			$this->set_repair_type($objData->repair_type);
			$this->set_policy_type_id($objData->policy_type_id);
			$this->set_remark($objData->remark);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
		}
	}

	function loadRepairType($strCondition)
	{
		$strSql = "SELECT tmproduct.*, ref_desc AS repair_type FROM " . $this->TABLE
			. " INNER JOIN turef ON turef.ref_value = tmproduct.repair_type "
			. "  WHERE turef.ref_type = 'QUOTATION' AND ref_code = 'REPAIR_TYPE' AND " . $strCondition;
		$this->getConnection();
		// echo $strSql;
		if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->TmProduct($row);
				$result->freeResult();
				return true;
            }
        }
		return false;
	}

	function load()
	{
		if ($this->id == '') {
			return false;
		}
		$strSql = "SELECT * FROM " . $this->TABLE . "  WHERE id =" . $this->id;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			if ($row = $result->nextRow()) {
				$this->TmProduct($row);
				$result->freeResult();
				return true;
			}
		}
		return false;
	}

	function loadByCondition($strCondition)
	{
		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM " . $this->TABLE . "  WHERE " . $strCondition;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			if ($row = $result->nextRow()) {
				$this->TmProduct($row);
				$result->freeResult();
				return true;
			}
		}
		return false;
	}
}

/*********************************************************
		Class :				Tm Product

		Last update :		24 april 2021

		Description:		Tm Product

 *********************************************************/

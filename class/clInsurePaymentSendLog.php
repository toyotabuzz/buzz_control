<?
/*********************************************************
		Class :					InsurePaymentSendLog

		Last update :	  2021/03/31

		Description:	  Class manage t_insure_payment_send_log table

*********************************************************/
 
class InsurePaymentSendLog extends DB {
    var $TABLE="juinsure_payment_send_log";

    var $mId;
	function getId() { return $this->mId; }
	function setId($data) { $this->mId = $data; }

    var $mInsureId;
	function getInsureId() { return $this->mInsureId; }
	function setInsureId($data) { $this->mInsureId = $data; }

    var $mInsureItemId;
	function getInsureItemId() { return $this->mInsureItemId; }
	function setInsureItemId($data) { $this->mInsureItemId = $data; }

    var $mSendType;
	function getSendType() { return $this->mSendType; }
	function setSendType($data) { $this->mSendType = $data; }

    var $mSendTo;
	function getSendTo() { return $this->mSendTo; }
	function setSendTo($data) { $this->mSendTo = $data; }

    var $mSendText;
	function getSendText() { return $this->mSendText; }
	function setSendText($data) { $this->mSendText = $data; }

    var $mSendNo;
	function getSendNo() { return $this->mSendNo; }
	function setSendNo($data) { $this->mSendNo = $data; }

    function InsurePaymentSendLog($objData=NULL) {
        $this->setId($objData->id);
        $this->setInsureId($objData->insure_id);
        $this->setInsureItemId($objData->insure_item_id);
        $this->setSendType($objData->send_type);
        $this->setSendTo($objData->send_to);
        $this->setSendText($objData->send_text);
        $this->setSendNo($objData->send_no);
    }
    
    function init () {
		
	}

    // ���¡�����Ŵ��� ID
    function load () {
		if ($this->mId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE id =".$this->mId;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->JmQuotationSendLog($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

    // �ӹǹ����觢����Ť�������ش
    function loadMaxSendNo() {
		$strSql = "SELECT MAX(send_no) as send_no  FROM ".$this->TABLE."  WHERE insure_id =".$this->mInsureId." and insure_item_id =".$this->mInsureItemId;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

    function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_item_id, send_type, send_to, send_text, send_no, created_at, created_by, updated_at, updated_by ) " ." VALUES ( "
		." '".$this->mInsureId."' , "
		." '".$this->mInsureItemId."' , "
		." '".$this->mSendType."' , "
		." '".$this->mSendTo."' , "
		." '".$this->mSendText."' , "
		." '".$this->mSendNo."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."', "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) {
            $this->mId = mysql_insert_id();
            return $this->mId;
        } else {
			return false;
	    }
	}
}
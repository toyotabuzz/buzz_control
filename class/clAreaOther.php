<?
/*********************************************************
		Class :				AreaOther

		Last update :		16 NOV 02

		AreaType:		Class manage amphur table

*********************************************************/

class AreaOther extends DB{

	var $TABLE="t_area_other";
	
	var $mAreaId;
	function getAreaId() { return $this->mAreaId; }
	function setAreaId($data) { $this->mAreaId = $data; }
	
	var $mAreaType;
	function getAreaType() { return htmlspecialchars($this->mAreaType); }
	function setAreaType($data) { $this->mAreaType = $data; }

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }	
	
	function AreaOther($objData=NULL) {
        If ($objData->area_id!="") {
            $this->setAreaId($objData->area_id);
			$this->setAreaType($objData->area_type);
			$this->setTitle($objData->title);
        }
    }
	
	function init(){

	}
	
	function load() {

		if ($this->mAreaId == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE C.area_id =".$this->mAreaId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->AreaOther($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->AreaOther($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( area_type, title ) "
						." VALUES ( '".$this->mAreaType."' ,"
						." '".$this->mTitle."' ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mAreaId = mysql_insert_id();
            return $this->mAreaId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET area_type = '".$this->mAreaType."' , "
						." title = '".$this->mTitle."'   "
						." WHERE  area_id = ".$this->mAreaId."  ";
        $this->getConnection();
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE area_id=".$this->mAreaId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }

   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
	
}

/*********************************************************
		Class :				AreaOtherList

		Last update :		22 Mar 02

		AreaType:		AreaOther user list

*********************************************************/


class AreaOtherList extends DataList {
	var $TABLE = "t_area_other";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT area_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new AreaOther($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT area_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new AreaOther($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getAreaType()."\"");
			if (($defaultId != null) && ($objItem->getAreaType() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getAreaOtherName()."</option>");
		}
		echo("</select>");
	}

}
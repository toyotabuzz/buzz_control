<?
/*********************************************************
		Class :					Module

		Last update :	  10 Jan 02

		Description:	  Class manage t_module table

*********************************************************/
 
class Module extends DB{

	var $TABLE="t_module";

	var $mModuleId;
	function getModuleId() { return $this->mModuleId; }
	function setModuleId($data) { $this->mModuleId = $data; }
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mLogo;
	function getLogo() { return htmlspecialchars($this->mLogo); }
	function setLogo($data) { $this->mLogo = $data; }		
	
	var $mLogo2;
	function getLogo2() { return htmlspecialchars($this->mLogo2); }
	function setLogo2($data) { $this->mLogo2 = $data; }			
	
	function Module($objData=NULL) {
        If ($objData->module_id !="") {
            $this->setModuleId($objData->module_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setLogo($objData->logo);
			$this->setLogo2($objData->logo2);
        }
    }

	function init(){	
		$this->setCode(stripslashes($this->mCode));
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mModuleId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE module_id =".$this->mModuleId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Module($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Module($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( code, logo, logo2, title ) "
						." VALUES ( '".$this->mCode."' , "
						."  '".$this->mLogo."' , "
						."  '".$this->mLogo2."' , "
						."  '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mModuleId = mysql_insert_id();
            return $this->mModuleId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET code = '".$this->mCode."' , "
						." title = '".$this->mTitle."' , "
						." logo2 = '".$this->mLogo2."' , "
						." logo = '".$this->mLogo."'  "
						." WHERE  module_id = ".$this->mModuleId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE module_id=".$this->mModuleId." ";
        $this->getConnection();
        $this->query($strSql);
		$this->deletePicture($this->mLogo);
		$this->deletePicture($this->mLogo2);
		
       $strSql = " DELETE FROM t_member_module "
                . " WHERE module_id=".$this->mModuleId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCode == "") $asrErrReturn["code"] = "��س��к� Code";
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��������";
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}		
}

/*********************************************************
		Class :				ModuleList

		Last update :		22 Mar 02

		Description:		Module user list

*********************************************************/


class ModuleList extends DataList {
	var $TABLE = "t_module";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT module_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Module($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCode()."\"");
			if (($defaultId != null) && ($objItem->getCode() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
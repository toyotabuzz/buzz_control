<?
/*********************************************************
		Class :					Stock Dealer

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_dealer table

*********************************************************/
 
class StockDealer extends DB{

	var $TABLE="t_stock_dealer";

	var $mStockDealerId;
	function getStockDealerId() { return $this->mStockDealerId; }
	function setStockDealerId($data) { $this->mStockDealerId = $data; }
	
	var $mDealerId;
	function getDealerId() { return htmlspecialchars($this->mDealerId); }
	function setDealerId($data) { $this->mDealerId = $data; }	
	
	var $mDealerDetail;
	function getDealerDetail() { return htmlspecialchars($this->mDealerDetail); }
	function setDealerDetail($data) { $this->mDealerDetail = $data; }	
	
	var $mStockProductId;
	function getStockProductId() { return htmlspecialchars($this->mStockProductId); }
	function setStockProductId($data) { $this->mStockProductId = $data; }	
	
	var $mStockProductDetail;
	function getStockProductDetail() { return htmlspecialchars($this->mStockProductDetail); }
	function setStockProductDetail($data) { $this->mStockProductDetail = $data; }	
 
	var $mPrice;
	function getPrice() { return $this->mPrice; }
	function setPrice($data) { $this->mPrice = $data; }	 
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }	 	

	function StockDealer($objData=NULL) {
        If ($objData->stock_dealer_id !="") {
            $this->setStockDealerId($objData->stock_dealer_id);
			$this->setDealerId($objData->dealer_id);
			$this->setDealerDetail($objData->dealer_detail);
			$this->setStockProductId($objData->stock_product_id);
			$this->setStockProductDetail($objData->stock_product_detail);
			$this->setPrice($objData->price);
			$this->setRemark($objData->remark);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
	}
		
	function load() {

		if ($this->mStockDealerId == '') {
			return false;
		}
		$strSql = "SELECT SD.*, SP.title AS stock_product_detail , D.title AS dealer_detail  FROM ".$this->TABLE." SD "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
		."  WHERE stock_dealer_id =".$this->mStockDealerId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockDealer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SD.*, SP.title AS stock_product_detail , D.title AS dealer_detail  FROM ".$this->TABLE." SD "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockDealer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( dealer_id, stock_product_id, price, remark ) "
						." VALUES ( '".$this->mDealerId
						."' , '".$this->mStockProductId
						."', '".$this->mPrice
						."', '".$this->mRemark."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockDealerId = mysql_insert_id();
            return $this->mStockDealerId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET dealer_id = '".$this->mDealerId."'  "
						." , stock_product_id = '".$this->mStockProductId."'  "
						." , remark = '".$this->mRemark."'  "
						." , price = '".$this->mPrice."'  "
						." WHERE  stock_dealer_id = ".$this->mStockDealerId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_dealer_id=".$this->mStockDealerId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function checkDelete(){
        if ($this->mOrderId == 0)
       {
			return true;
        }
		return false;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mDealerId == "") $asrErrReturn["dealer_id"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Dealer List

		Last update :		22 Mar 02

		Description:		Customer Stock Dealer List

*********************************************************/

class StockDealerList extends DataList {
	var $TABLE = "t_stock_dealer";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_dealer_id) as rowCount FROM ".$this->TABLE." SD  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SD.*, SP.title AS stock_product_detail , D.title AS dealer_detail  FROM ".$this->TABLE." SD "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockDealer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_dealer_id) as rowCount FROM ".$this->TABLE." SD  "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SD.*, SP.title AS stock_product_detail , D.title AS dealer_detail  FROM ".$this->TABLE." SD "
		." LEFT JOIN t_stock_product SP ON SP.stock_product_id = SD.stock_product_id "
		." LEFT JOIN t_dealer D ON D.dealer_id = SD.dealer_id "	
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockDealer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockDealerId()."\"");
			if (($defaultId != null) && ($objItem->getStockDealerId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getStockName()."</option>");
		}
		echo("</select>");
	}			
	
}
<?
/*********************************************************
		Class :					Insure Quotation

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsurexQuotation extends DB{

	var $TABLE="t_insurex_quotation";

	var $m_quotation_id;
	function get_quotation_id() { return $this->m_quotation_id; }
	function set_quotation_id($data) { $this->m_quotation_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }	
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_quotation_number;
	function get_quotation_number() { return stripslashes($this->m_quotation_number); }
	function set_quotation_number($data) { $this->m_quotation_number = addslashes($data); }
	
	var $m_date_add;
	function get_date_add() { return stripslashes($this->m_date_add); }
	function set_date_add($data) { $this->m_date_add = addslashes($data); }
	
	var $m_xtra01;
	function get_xtra01() { return stripslashes($this->m_xtra01); }
	function set_xtra01($data) { $this->m_xtra01 = addslashes($data); }
	
	var $m_xtra02;
	function get_xtra02() { return stripslashes($this->m_xtra02); }
	function set_xtra02($data) { $this->m_xtra02 = addslashes($data); }

	var $m_text01;
	function get_text01() { return stripslashes($this->m_text01); }
	function set_text01($data) { $this->m_text01 = addslashes($data); }
	
	var $m_text02;
	function get_text02() { return stripslashes($this->m_text02); }
	function set_text02($data) { $this->m_text02 = addslashes($data); }
	
	var $m_text03;
	function get_text03() { return stripslashes($this->m_text03); }
	function set_text03($data) { $this->m_text03 = addslashes($data); }
	
	var $m_text04;
	function get_text04() { return stripslashes($this->m_text04); }
	function set_text04($data) { $this->m_text04 = addslashes($data); }
	
	var $m_text05;
	function get_text05() { return stripslashes($this->m_text05); }
	function set_text05($data) { $this->m_text05 = addslashes($data); }
	
	var $m_text06;
	function get_text06() { return stripslashes($this->m_text06); }
	function set_text06($data) { $this->m_text06 = addslashes($data); }
	
	var $m_text07;
	function get_text07() { return stripslashes($this->m_text07); }
	function set_text07($data) { $this->m_text07 = addslashes($data); }
	
	var $m_text08;
	function get_text08() { return stripslashes($this->m_text08); }
	function set_text08($data) { $this->m_text08 = addslashes($data); }
	
	var $m_text09;
	function get_text09() { return stripslashes($this->m_text09); }
	function set_text09($data) { $this->m_text09 = addslashes($data); }
	
	var $m_text10;
	function get_text10() { return stripslashes($this->m_text10); }
	function set_text10($data) { $this->m_text10 = addslashes($data); }
	
	var $m_text11;
	function get_text11() { return stripslashes($this->m_text11); }
	function set_text11($data) { $this->m_text11 = addslashes($data); }
	
	var $m_text12;
	function get_text12() { return stripslashes($this->m_text12); }
	function set_text12($data) { $this->m_text12 = addslashes($data); }
	
	var $m_text13;
	function get_text13() { return stripslashes($this->m_text13); }
	function set_text13($data) { $this->m_text13 = addslashes($data); }
	
	var $m_text14;
	function get_text14() { return stripslashes($this->m_text14); }
	function set_text14($data) { $this->m_text14 = addslashes($data); }
	
	var $m_text15;
	function get_text15() { return stripslashes($this->m_text15); }
	function set_text15($data) { $this->m_text15 = addslashes($data); }
	
	var $m_text16;
	function get_text16() { return stripslashes($this->m_text16); }
	function set_text16($data) { $this->m_text16 = addslashes($data); }
	
	var $m_text17;
	function get_text17() { return stripslashes($this->m_text17); }
	function set_text17($data) { $this->m_text17 = addslashes($data); }
	
	var $m_text18;
	function get_text18() { return stripslashes($this->m_text18); }
	function set_text18($data) { $this->m_text18 = addslashes($data); }
	
	var $m_text19;
	function get_text19() { return stripslashes($this->m_text19); }
	function set_text19($data) { $this->m_text19 = addslashes($data); }
	
	var $m_text20;
	function get_text20() { return stripslashes($this->m_text20); }
	function set_text20($data) { $this->m_text20 = addslashes($data); }	
	
	var $m_text21;
	function get_text21() { return stripslashes($this->m_text21); }
	function set_text21($data) { $this->m_text21 = addslashes($data); }	
	
	var $m_text22;
	function get_text22() { return stripslashes($this->m_text22); }
	function set_text22($data) { $this->m_text22 = addslashes($data); }	
	
	var $m_text23;
	function get_text23() { return stripslashes($this->m_text23); }
	function set_text23($data) { $this->m_text23 = addslashes($data); }			

	var $m_text24;
	function get_text24() { return stripslashes($this->m_text24); }
	function set_text24($data) { $this->m_text24 = addslashes($data); }
	
	var $m_text25;
	function get_text25() { return stripslashes($this->m_text25); }
	function set_text25($data) { $this->m_text25 = addslashes($data); }			

	var $m_text26;
	function get_text26() { return stripslashes($this->m_text26); }
	function set_text26($data) { $this->m_text26 = addslashes($data); }
	
	var $m_remark;
	function get_remark() { return stripslashes($this->m_remark); }
	function set_remark($data) { $this->m_remark = addslashes($data); }
	
	//�յ������
	var $m_year_extend;
	function get_year_extend() { return stripslashes($this->m_year_extend); }
	function set_year_extend($data) { $this->m_year_extend = addslashes($data); }
	
	//��ʹ��Ҥҹ��١���͡��
	var $m_selected;
	function get_selected() { return stripslashes($this->m_selected); }
	function set_selected($data) { $this->m_selected = addslashes($data); }	
	
	var $m_permit_all1;
	function get_permit_all1() { return stripslashes($this->m_permit_all1); }
	function set_permit_all1($data) { $this->m_permit_all1 = addslashes($data); }
	
	var $m_permit_all2;
	function get_permit_all2() { return stripslashes($this->m_permit_all2); }
	function set_permit_all2($data) { $this->m_permit_all2 = addslashes($data); }
	
	var $m_include_vat;
	function get_include_vat() { return stripslashes($this->m_include_vat); }
	function set_include_vat($data) { $this->m_include_vat = addslashes($data); }	
	
	var $m_mails;
	function get_mails() { return stripslashes($this->m_mails); }
	function set_mails($data) { $this->m_mails = addslashes($data); }		
	
	var $m_export_date;
	function get_export_date() { return $this->m_export_date; }
	function set_export_date($data) { $this->m_export_date = $data; }			
	
	function InsurexQuotation($objData=NULL) {
        If ($objData->quotation_id !="") {
			$this->set_quotation_id($objData->quotation_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_car_id($objData->car_id);
			$this->set_quotation_number($objData->quotation_number);
			$this->set_date_add($objData->date_add);
			$this->set_xtra01($objData->xtra01);
			$this->set_xtra02($objData->xtra02);
			$this->set_text01($objData->text01);
			$this->set_text02($objData->text02);
			$this->set_text03($objData->text03);
			$this->set_text04($objData->text04);
			$this->set_text05($objData->text05);
			$this->set_text06($objData->text06);
			$this->set_text07($objData->text07);
			$this->set_text08($objData->text08);
			$this->set_text09($objData->text09);
			$this->set_text10($objData->text10);
			$this->set_text11($objData->text11);
			$this->set_text12($objData->text12);
			$this->set_text13($objData->text13);
			$this->set_text14($objData->text14);
			$this->set_text15($objData->text15);
			$this->set_text16($objData->text16);
			$this->set_text17($objData->text17);
			$this->set_text18($objData->text18);
			$this->set_text19($objData->text19);
			$this->set_text20($objData->text20);
			$this->set_text21($objData->text21);
			$this->set_text22($objData->text22);
			$this->set_text23($objData->text23);
			$this->set_text24($objData->text24);
			$this->set_text25($objData->text25);
			$this->set_text26($objData->text26);
			$this->set_remark($objData->remark);
			$this->set_year_extend($objData->year_extend);
			$this->set_selected($objData->selected);
			$this->set_permit_all1($objData->permit_all1);
			$this->set_permit_all2($objData->permit_all2);
			$this->set_include_vat($objData->include_vat);
			$this->set_mails($objData->mails);
			$this->set_export_date($objData->export_date);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_quotation_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE quotation_id =".$this->m_quotation_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsurexQuotation($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsurexQuotation($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_id, car_id, insure_id, quotation_number, date_add, text01, text02, text03, text04, text05, text06, text07, text08, text09, text10,
		 text11, text12, text13, text14, text15, text16, text17, text18, text19, text20, text21, text22, text23, text24, text25, text26, xtra01, xtra02, remark, permit_all1, permit_all2, include_vat, year_extend , mails, export_date ) " ." VALUES ( "
		." '".$this->m_quotation_id."' , "
		." '".$this->m_car_id."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_quotation_number."' , "
		." '".date("Y-m-d")."' , "
		." '".$this->m_text01."' , "
		." '".$this->m_text02."' , "
		." '".$this->m_text03."' , "
		." '".$this->m_text04."' , "
		." '".$this->m_text05."' , "
		." '".$this->m_text06."' , "
		." '".$this->m_text07."' , "
		." '".$this->m_text08."' , "
		." '".$this->m_text09."' , "
		." '".$this->m_text10."' , "
		." '".$this->m_text11."' , "
		." '".$this->m_text12."' , "
		." '".$this->m_text13."' , "
		." '".$this->m_text14."' , "
		." '".$this->m_text15."' , "
		." '".$this->m_text16."' , "
		." '".$this->m_text17."' , "
		." '".$this->m_text18."' , "
		." '".$this->m_text19."' , "
		." '".$this->m_text20."' , "
		." '".$this->m_text21."' , "
		." '".$this->m_text22."' , "
		." '".$this->m_text23."' , "
		." '".$this->m_text24."' , "
		." '".$this->m_text25."' , "
		." '".$this->m_text26."' , "
		." '".$this->m_xtra01."' , "
		." '".$this->m_xtra02."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_permit_all1."' , "
		." '".$this->m_permit_all2."' , "
		." '".$this->m_include_vat."' , "
		." '".$this->m_year_extend."' , "
		." '".$this->m_mails."' , "
		." '".$this->m_export_date."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_quotation_id = mysql_insert_id();
            return $this->m_quotation_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "

		."  text01 = '".$this->m_text01."' "
		." , text02 = '".$this->m_text02."' "
		." , text03 = '".$this->m_text03."' "
		." , text04 = '".$this->m_text04."' "
		." , text05 = '".$this->m_text05."' "
		." , text06 = '".$this->m_text06."' "
		." , text07 = '".$this->m_text07."' "
		." , text08 = '".$this->m_text08."' "
		." , text09 = '".$this->m_text09."' "
		." , text10 = '".$this->m_text10."' "
		." , text11 = '".$this->m_text11."' "
		." , text12 = '".$this->m_text12."' "
		." , text13 = '".$this->m_text13."' "
		." , text14 = '".$this->m_text14."' "
		." , text15 = '".$this->m_text15."' "
		." , text16 = '".$this->m_text16."' "
		." , text17 = '".$this->m_text17."' "
		." , text18 = '".$this->m_text18."' "
		." , text19 = '".$this->m_text19."' "
		." , text20=  '".$this->m_text20."' "
		." , text21=  '".$this->m_text21."' "
		." , text22=  '".$this->m_text22."' "
		." , text23=  '".$this->m_text23."' "
		." , text24=  '".$this->m_text24."' "
		." , text25=  '".$this->m_text25."' "
		." , text26=  '".$this->m_text26."' "
		." , include_vat=  '".$this->m_include_vat."' "
		." , permit_all1=  '".$this->m_permit_all1."' "
		." , permit_all2=  '".$this->m_permit_all2."' "
		." , remark = '".$this->m_remark."' "
		." , xtra01 = '".$this->m_xtra01."' "
		." , xtra02 = '".$this->m_xtra02."' "
		." , mails = '".$this->m_mails."' "
		." WHERE quotation_id = ".$this->m_quotation_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateSelected(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET selected= '' "
		." WHERE insure_id = ".$this->m_insure_id; 
		
		$strSql1 = "UPDATE ".$this->TABLE 
		." SET selected= '".$this->m_selected."' "
		." WHERE quotation_id = ".$this->m_quotation_id." "; 		
		
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->query($strSql1);
		$this->unsetConnection();
		return $result;
        
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE quotation_id=".$this->m_quotation_id." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_insurex_quotation_item "
                . " WHERE quotation_id=".$this->m_quotation_id." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
		
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsurexQuotationList extends DataList {
	var $TABLE = "t_insurex_quotation";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT quotation_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsurexQuotation($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
<?
/*********************************************************
		Class :					Cashier Reserve

		Last update :	  10 Jan 02

		Description:	  Class manage t_cashier_reserve table

*********************************************************/
 
class CashierReserve extends DB{

	var $TABLE="t_cashier_reserve";

	var $mReserveId;
	function getReserveId() { return $this->mReserveId; }
	function setReserveId($data) { $this->mReserveId = $data; }
	
	var $mReserveDate;
	function getReserveDate() { return $this->mReserveDate; }
	function setReserveDate($data) { $this->mReserveDate = $data; }	
	
	var $mReserveBy;
	function getReserveBy() { return htmlspecialchars($this->mReserveBy); }
	function setReserveBy($data) { $this->mReserveBy = $data; }
	
	var $mCustomerName;
	function getCustomerName() { return htmlspecialchars($this->mCustomerName); }
	function setCustomerName($data) { $this->mCustomerName = $data; }
	
	var $mReserveNumber;
	function getReserveNumber() { return htmlspecialchars($this->mReserveNumber); }
	function setReserveNumber($data) { $this->mReserveNumber = $data; }

	var $mSaleId;
	function getSaleId() { return htmlspecialchars($this->mSaleId); }
	function setSaleId($data) { $this->mSaleId = $data; }
	
	var $mCarSeriesId;
	function getCarSeriesId() { return $this->mCarSeriesId; }
	function setCarSeriesId($data) { $this->mCarSeriesId = $data; }	
	
	var $mTotal;
	function getTotal() { return $this->mTotal; }
	function setTotal($data) { $this->mTotal = $data; }		
	
	var $mPayBy;
	function getPayBy() { return $this->mPayBy; }
	function setPayBy($data) { $this->mPayBy = $data; }		
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }

	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }	
	
	function CashierReserve($objData=NULL) {
        If ($objData->reserve_id !="") {
            $this->setReserveId($objData->reserve_id);
			$this->setReserveDate($objData->reserve_date);
			$this->setReserveBy($objData->reserve_by);
			$this->setReserveNumber($objData->reserve_number);
			$this->setSaleId($objData->sale_id);
			$this->setCarSeriesId($objData->car_series_id);
			$this->setTotal($objData->total);
			$this->setRemark($objData->remark);
			$this->setPayBy($objData->pay_by);			
			$this->setCustomerName($objData->customer_name);
			$this->setStatus($objData->status);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
        }
    }

	function init(){
		$this->setPayBy(stripslashes($this->mPayBy));
		$this->setRemark(stripslashes($this->mRemark));
		$this->setCustomerName(stripslashes($this->mCustomerName));
	}
		
	function load() {

		if ($this->mReserveId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE reserve_id =".$this->mReserveId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierReserve($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CashierReserve($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( reserve_by,reserve_date,customer_name,reserve_number, "
						." sale_id , car_series_id, total, remark, pay_by  ) "
						." VALUES ( '".$this->mReserveBy
						."' ,'".$this->mReserveDate
						."', '".$this->mCustomerName
						."' ,'".$this->mReserveNumber
						."' ,'".$this->mSaleId
						."', '".$this->mCarSeriesId
						."' ,'".$this->mTotal
						."' ,'".$this->mRemark
						."' ,'".$this->mPayBy."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mReserveId = mysql_insert_id();
            return $this->mReserveId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET reserve_by = '".$this->mReserveBy."'  "
						." , reserve_date = '".$this->mReserveDate."'  "
						." , customer_name = '".$this->mCustomerName."'  "
						." , reserve_number = '".$this->mReserveNumber."'  "
						." , sale_id = '".$this->mSaleId."'  "
						." , car_series_id = '".$this->mCarSeriesId."'  "
						." , total = '".$this->mTotal."'  "
						." , remark = '".$this->mRemark."'  "
						." , pay_by = '".$this->mPayBy."'  "
						." WHERE  reserve_id = ".$this->mReserveId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		// 0 : new record
		// 1 : set on booking record 
		// 2 : set on c-card record
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." WHERE  reserve_id = ".$this->mReserveId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateDelete(){
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  reserve_id = ".$this->mReserveId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE reserve_id=".$this->mReserveId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mReserveBy == "") $asrErrReturn["reserve_by"] = "��س��к�";
		if ($this->mReserveDate == "") $asrErrReturn["reserve_date"] = "��س��к�";
		if ($this->mReserveNumber == "") $asrErrReturn["reserve_number"] = "��س��к�";
		if ($this->mSaleId == "0") $asrErrReturn["sale_id"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Cashier Reserve List

		Last update :		22 Mar 02

		Description:		Customer Cashier Reserve List

*********************************************************/

class CashierReserveList extends DataList {
	var $TABLE = "t_cashier_reserve";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT reserve_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CashierReserve($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
<?
/*********************************************************
		Class :					Payment Style

		Last update :	  2021/04/08

		Description:	  Class manage turef table ref_type = PAYMENT, ref_code = STYLE

*********************************************************/
 
class PaymentStyle extends DB {

	var $TABLE = "turef";

	var $mId;
	function getId() { return $this->mId; }
	function setId($data) { $this->mId = $data; }
	
	var $mRefType;
	function getRefType() { return htmlspecialchars($this->mRefType); }
	function setRefType($data) { $this->mRefType = $data; }

	var $mRefCode;
	function getRefCode() { return htmlspecialchars($this->mRefCode); }
	function setRefCode($data) { $this->mRefCode = $data; }

    var $mRefSeq;
	function getRefSeq() { return htmlspecialchars($this->mRefSeq); }
	function setRefSeq($data) { $this->mRefSeq = $data; }

    var $mRefDesc;
	function getRefDesc() { return htmlspecialchars($this->mRefDesc); }
	function setRefDesc($data) { $this->mRefDesc = $data; }

    var $mRefValue;
	function getRefValue() { return htmlspecialchars($this->mRefValue); }
	function setRefValue($data) { $this->mRefValue = $data; }

    var $mRefRemark;
	function getRefRemark() { return htmlspecialchars($this->mRefRemark); }
	function setRefRemark($data) { $this->mRefRemark = $data; }

    var $mRefIsactive;
	function getRefIsactive() { return htmlspecialchars($this->mRefIsactive); }
	function setRefIsactive($data) { $this->mRefIsactive = $data; }
	
	function PaymentStyle($objData=NULL) {
        if ($objData->id !="") {
            $this->setId($objData->id);
            $this->setRefType($objData->ref_type);
			$this->setRefCode($objData->ref_code);
            $this->setRefSeq($objData->ref_seq);
            $this->setRefDesc($objData->ref_desc);
            $this->setRefValue($objData->ref_value);
            $this->setRefRemark($objData->ref_remark);
        }
    }

	function init(){
        //
	}
		
	function load() {
		if ($this->mRefType == ''||$this->mRefCode == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ref_type = '".$this->$this->mRefType."' AND ref_code = '".$this->mRefCode."'";
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->PaymentStyle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->PaymentStyle($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
}

/*********************************************************
		Class :				Payment Style List

		Last update :		2021/04/08

		Description:		Payment Style List

*********************************************************/

class PaymentStyleList extends DataList {
	var $TABLE = "turef";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PaymentStyle($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getRefValue()."\"");
			if (($defaultId != null) && ($objItem->getRefValue() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getRefDesc()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getRefValue()."\"");
			if (($defaultId != null) && ($objItem->getRefValue() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getRefDesc()."</option>");
		}
		echo("</select>");
    }	
}
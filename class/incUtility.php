<?

Function DropdownAge($SelectName, $Selected,$SelectDefault=Null)
{
	$arrValue= Array(
                "below 18" => "below 18",
                "18-24" => "18-24",
                "25-30" => "25-30",
                "31-40" => "31-40",
                "41-50" => "41-50",
                "51 plus" => "51 plus"
                );
    $strHtml = "\n<select name=\"".$SelectName."\">\n";
    If ($SelectDefault) $strHtml.= "\t<option value=\"\">".$SelectDefault."</option>\n";
    
    ForEach ($arrValue As $strKey => $strValue)
    {
        $strHtml.= "\t<option value=\"".$strValue."\"";
        If ( $Selected == $strKey )
            { $strHtml .= " selected"; }
        $strHtml .= ">".$strValue."</option>\n";
    }
    $strHtml.= "</select>\n";
    Return $strHtml;
}

Function DropdownTitle($SelectName, $Selected,$SelectDefault=Null)
{

	$arrValue= Array(
                "���" => "���",
                "�ҧ���" => "�ҧ���",
                "�ҧ" => "�ҧ",
                "Mr" => "Mr",
                "Miss" => "Miss",
                "Mrs" => "Mrs",
				"���." => "���.",
				"˨�." => "˨�.",
				"���." => "���.",
				"����к�" => "����к�"
                );
    $strHtml = "\n<select name=\"".$SelectName."\">\n";
    If ($SelectDefault) $strHtml.= "\t<option value=\"\">".$SelectDefault."</option>\n";
    
    ForEach ($arrValue As $strKey => $strValue)
    {
        $strHtml.= "\t<option value=\"".$strValue."\"";
        If ( $Selected == $strKey )
            { $strHtml .= " selected"; }
        $strHtml .= ">".$strValue."</option>\n";
    }
    $strHtml.= "</select>\n";
    Return $strHtml;
}


Function DropDownCollection($SelectName,$Selected,$SelectDefault) 
{
    $strHtml = "\n<select name=\"".$SelectName."\">\n";
    If ($SelectDefault) $strHtml.= "\t<option value=\"\">".$SelectDefault."</option>\n";
    $strSql = "SELECT * FROM Collection";
    $strSql .= " ORDER BY collectionNameEn";

    $objDb = New Db;
    $objDb->getConnection();
    If ($result = $objDb->query($strSql))
    {
        While ( $row = $result->nextRow() )
        {
        If ( ($Selected == $row->collectionId) Or ($Selected == $row->collectionNameEN) )  $strHtml.= "\t<option value=\"".$row->collectionId."\" selected>".$row->collectionNameEN."</option>\n";
        Else $strHtml.= "\t<option value=\"".$row->collectionId."\">".$row->collectionNameEN."</option>\n";
        }
    }
    $result->freeResult;
    UnSet($objDb);

    $strHtml.= "</select>";
    Return $strHtml;
}

Function RadioEnalbed($RadioName, $Selected)  //used by distributor
{
    $arrEnable = Array(
                0 =>"Enabled"
                ,1 =>"Disabled"
                );
    $strHtml= "";

    If (!$Selected) { $Selected="Enabled";}
    Else {$Selected="Disabled";}

    ForEach ($arrEnable As $Key => $strValue)
    {
        $strHtml.= "\t<input type=\"radio\" name=\"".$RadioName."\" value=\"".$Key ."\"";
        If ( ($Selected == $strValue) ) { $strHtml .= " checked"; }
        $strHtml .= ">".$strValue."\n";
    }
    UnSet($arrEnabled);
    Return $strHtml;
}

Function RadioYes($RadioName, $Selected)  //used by distributor
{
    $arrEnable = Array(
                1 =>"Yes"
                ,0 =>"No"
                );
    $strHtml= "";

    If (!$Selected) { $Selected="No";}
    Else {$Selected="Yes";}

    ForEach ($arrEnable As $Key => $strValue)
    {
        $strHtml.= "\t<input type=\"radio\" name=\"".$RadioName."\" value=\"".$Key ."\"";
        If ( ($Selected == $strValue) ) { $strHtml .= " checked"; }
        $strHtml .= ">".$strValue."\n";
    }
    UnSet($arrEnabled);
    Return $strHtml;
}

Function Radio($arrValue, $RadioName, $Selected=Null)    //For generate Radion Button
{
    ForEach ($arrValue As $strValue)
    {
        Echo "\t<input type=\"radio\" name=\"".$RadioName."\" value=\"".$strValue ."\"";
        If ( ($Selected == $strValue) ) { Echo " checked"; }
        Echo ">".$strValue."\n";
    }
}

Function RadioTitle($RadioName, $Selected)
{
    $arrValue = Array("Mr","Mrs","Ms");
    $strHtml= "";

    For ($i=0; $i<Count($arrValue); $i++)
        If ($Selected == $arrValue[$i])  $strHtml.= "\t<input type=\"radio\" name=\"".$RadioName."\" value=\"".$arrValue[$i] ."\" checked>".$arrValue[$i]."\n";
        Else $strHtml.= "\t<input type=\"radio\" name=\"".$RadioName."\" value=\"".$arrValue[$i] ."\">".$arrValue[$i]."\n";
    UnSet($arrValue);

    Return $strHtml;
}

Function Checkbox($CheckboxName,$CheckboxDescription,$Selected){
	if ($Selected == 1){ 
		$strHtml= $CheckboxDescription."<input type='checkbox' name='".$CheckboxName."' Checked>";	
	}else{
		$strHtml= $CheckboxDescription."<input type='checkbox' name='".$CheckboxName."' >";	
	}
	Return $strHtml;
}

Function CheckboxValue($Selected,$Language){
	Switch($Language){
		case "EN":
				if ($Selected == 1){ 
					$strHtml= "Yes";
				}else{
					$strHtml= "No";
				}
				break;
		case "FR":
				if ($Selected == 1){ 
					$strHtml= "Yes";
				}else{
					$strHtml= "No";
				}
				break;
		}
	Return $strHtml;
}

Function FormatCode($code,$len){
	//add zero to $code
	for($i=0;$i < ($len-strlen($code));$i++){
		$format = $format."0";
	}
	$code= $format.$code;	
	return $code;
}

Function DropdownInsure($SelectName, $Selected,$SelectDefault=Null)
{
	$arrValue= Array(
                "GOA" => "GOA",
                "�1" => "�1",
                "�2" => "�2",
                "�2+" => "�2+",
                "�3" => "�3+",
                "����" => "����"
                );
    $strHtml = "<select name=\"".$SelectName."\">";
    If ($SelectDefault) $strHtml.= "<option value=\"\">".$SelectDefault."</option>";
    
    ForEach ($arrValue As $strKey => $strValue)
    {
        $strHtml.= "<option value=\"".$strValue."\"";
        If ( $Selected == $strKey )
            { $strHtml .= " selected"; }
        $strHtml .= ">".$strValue."</option>";
    }
    $strHtml.= "</select>";
    return $strHtml;
}

Function DropdownHour($SelectName, $Selected, $SelectDefault = Null)
{
	$arrValue = array();
    for ($i=0;$i<=23;$i++) {
        $val = substr('00'.$i, -2);
        $arrValue[$val] = $val;
    }

    $strHtml = "<select name=\"".$SelectName."\">";
    if ($SelectDefault) $strHtml.= "<option value=\"\">".$SelectDefault."</option>";
    
    foreach ($arrValue As $strKey => $strValue) {
        $strHtml.= "<option value=\"".$strValue."\"";
        if ( $Selected == $strKey )
            { $strHtml .= " selected"; }
        $strHtml .= ">".$strValue."</option>";
    }
    $strHtml.= "</select>";
    return $strHtml;
}

Function DropdownMinute($SelectName, $Selected, $SelectDefault = Null)
{
	$arrValue = array();
    for ($i=0;$i<=59;$i++) {
        $val = substr('00'.$i, -2);
        $arrValue[$val] = $val;
    }

    $strHtml = "<select name=\"".$SelectName."\">";
    if ($SelectDefault) $strHtml.= "<option value=\"\">".$SelectDefault."</option>";
    
    foreach ($arrValue As $strKey => $strValue) {
        $strHtml.= "<option value=\"".$strValue."\"";
        if ( $Selected == $strKey )
            { $strHtml .= " selected"; }
        $strHtml .= ">".$strValue."</option>";
    }
    $strHtml.= "</select>";
    return $strHtml;
}
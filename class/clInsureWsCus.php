<?
/*********************************************************
		Class :					InsureWSCus

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_ws_cus table

*********************************************************/
 
class InsureWSCus extends DB{

	var $TABLE="t_insure_ws_cus";

	var $m_ws_cus_id;
	function get_ws_cus_id() { return $this->m_ws_cus_id; }
	function set_ws_cus_id($data) { $this->m_ws_cus_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_broker_id;
	function get_broker_id() { return $this->m_broker_id; }
	function set_broker_id($data) { $this->m_broker_id = $data; }
	
	var $m_prb_id;
	function get_prb_id() { return $this->m_prb_id; }
	function set_prb_id($data) { $this->m_prb_id = $data; }
	
	var $m_ws_cus_no;
	function get_ws_cus_no() { return stripslashes($this->m_ws_cus_no); }
	function set_ws_cus_no($data) { $this->m_ws_cus_no = $data; }
	
	var $m_ws_cus_name;
	function get_ws_cus_name() { return stripslashes($this->m_ws_cus_name); }
	function set_ws_cus_name($data) { $this->m_ws_cus_name = $data; }
	
	var $m_ws_cus_date;
	function get_ws_cus_date() { return $this->m_ws_cus_date; }
	function set_ws_cus_date($data) { $this->m_ws_cus_date = $data; }
	
	var $m_ws_cus_price;
	function get_ws_cus_price() { return $this->m_ws_cus_price; }
	function set_ws_cus_price($data) { $this->m_ws_cus_price = $data; }
	
	var $m_ws_cus_type;
	function get_ws_cus_type() { return $this->m_ws_cus_type; }
	function set_ws_cus_type($data) { $this->m_ws_cus_type = $data; }
	
	var $m_ws_cus_status;
	function get_ws_cus_status() { return $this->m_ws_cus_status; }
	function set_ws_cus_status($data) { $this->m_ws_cus_status = $data; }	
	
	//�ѹ����ͻ�Ҩҡ�ѹ����駧ҹ ������֧���������ç�Ѻ��¡�÷���ͧ
	var $m_ws_cus_date1;
	function get_ws_cus_date1() { return $this->m_ws_cus_date1; }
	function set_ws_cus_date1($data) { $this->m_ws_cus_date1 = $data; }	

	function InsureWSCus($objData=NULL) {
        If ($objData->ws_cus_id !="") {
			$this->set_ws_cus_id($objData->ws_cus_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_broker_id($objData->broker_id);
			$this->set_prb_id($objData->prb_id);
			$this->set_ws_cus_no($objData->ws_cus_no);
			$this->set_ws_cus_name($objData->ws_cus_name);
			$this->set_ws_cus_date($objData->ws_cus_date);
			$this->set_ws_cus_price($objData->ws_cus_price);
			$this->set_ws_cus_type($objData->ws_cus_type);
			$this->set_ws_cus_status($objData->ws_cus_status);
			$this->set_ws_cus_date1($objData->ws_cus_date1);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_ws_cus_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ws_cus_id =".$this->m_ws_cus_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureWSCus($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureWSCus($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , broker_id , prb_id , ws_cus_no , ws_cus_name , ws_cus_date , ws_cus_date1 , ws_cus_price , ws_cus_type ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_broker_id."' , "
		." '".$this->m_prb_id."' , "
		." '".addslashes($this->m_ws_cus_no)."' , "
		." '".addslashes($this->m_ws_cus_name)."' , "
		." '".$this->m_ws_cus_date."' , "
		." '".$this->m_ws_cus_date1."' , "
		." '".$this->m_ws_cus_price."' , "
		." '".$this->m_ws_cus_type."' "
		." ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_ws_cus_id = mysql_insert_id();
            return $this->m_ws_cus_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  ws_cus_no = '".addslashes($this->m_ws_cus_no)."' "
		." , ws_cus_date = '".$this->m_ws_cus_date."' "
		." , ws_cus_status = '".$this->m_ws_cus_status."' "
		." WHERE ws_cus_id = ".$this->m_ws_cus_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ws_cus_id=".$this->m_ws_cus_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class InsureWSCusList extends DataList {
	var $TABLE = "t_insure_ws_cus";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT ws_cus_id) as rowCount FROM ".$this->TABLE." I  "
		." LEFT JOIN t_insure P ON I.insure_id = P.insure_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT I.* FROM ".$this->TABLE." I "
		." LEFT JOIN t_insure P ON I.insure_id = P.insure_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureWSCus($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
<?
/*********************************************************
		Class :					Car Premium

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_premium table

*********************************************************/
 
class CarPremium extends DB{

	var $TABLE="t_car_premium";

	var $mPremiumId;
	function getPremiumId() { return $this->mPremiumId; }
	function setPremiumId($data) { $this->mPremiumId = $data; }
	
	var $mCode;
	function getCode() { return $this->mCode; }
	function setCode($data) { $this->mCode = $data; }	
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }	
	
	function CarPremium($objData=NULL) {
        If ($objData->premium_id !="") {
            $this->setPremiumId($objData->premium_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
			$this->setStatus($objData->status);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
		$this->setCode(stripslashes($this->mCode));
	}
		
	function load() {

		if ($this->mPremiumId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE premium_id =".$this->mPremiumId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title,code,status ) "
						." VALUES ( '".$this->mTitle."' , '".$this->mCode."', '".$this->mStatus."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mPremiumId = mysql_insert_id();
            return $this->mPremiumId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , code = '".$this->mCode."'  "
						." , status = '".$this->mStatus."'  "
						." WHERE  premium_id = ".$this->mPremiumId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE premium_id=".$this->mPremiumId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���Ǣ�͡�õԴ���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Car Premium List

		Last update :		22 Mar 02

		Description:		Customer Car PremiumList

*********************************************************/

class CarPremiumList extends DataList {
	var $TABLE = "t_car_premium";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT premium_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarPremium($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPremiumId()."\"");
			if (($defaultId != null) && ($objItem->getPremiumId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
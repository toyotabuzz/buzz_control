<?php
/*@*******************************************\
  @package  User
  @date     22/04/2001
  @author   Gan
  @description  Smallest Class of User

  @example

\*********************************************/

/*******************************************************************\
	Class : SimpleUser 
\*******************************************************************/

Class SimpleUser extends DB {

	/*@*******************************************\
	  @class SimpleUser extends DB
	  @date   22/04/2001
	  @desciption
		ceci est la description
		sur plusieurs ligne
	  @example
		ceci est un exemple
		sur plusieurs lignes (<pre><code>)    
	\*********************************************/

    //::: Properties :::

	var $TABLE_USER;
	var $mUserId;
		//@property userId		user ID

    //::: Constructor :::

	function SimpleUser($objData = null) {
		//@param objData	represents DB row to fill object properties
		if ($objData != null) {
			$this->mUserId = $objData->userId;
		}
	}

	//::: Interface Functions :::
	
	function getId() { return $this->getUserId(); } // alias
	function setId($data) { $this->setUserId($data); } // alias
	function getUserId() { return $this->mUserId; }
	function setUserId($data) { $this->mUserId = $data; }	

	//::: Functions :::

	function load() {
		//@description	load object from DB
		//@description	user Id need to be set before calling load()
		if ($this->mId == '') {
			return false;
		}
		$strSql = "SELECT *"
			." FROM ".$this->TABLE_USER;
        $strSql.=" WHERE userId = ".$id;
        $this->getConnection();
        if ($result = $this->query($strSql_User))
        {
            if ($row = $result->nextRow())
            {
                $this->SimpleUser($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

}

/*******************************************************************\
	Class : ContactUser 
\*******************************************************************/

class ContactUser extends SimpleUser {

	/*@*******************************************\
	  @class ContactUser extends SimpleUser
	  @date   22/04/2001
	  @desciption
		ceci est la description
		sur plusieurs ligne
	  @example
		ceci est un exemple
		sur plusieurs lignes (<pre><code>)    
	\*********************************************/

    var $mUserId;
    function getId() { return $this->mUserId; }
    function setId($val) { $this->mUserId=$val; }
	
	var $mId;
	//function getId() { return $this->mUserId; }
	//function setId($data) { $this->mUserId=$data; }
	
	function ContactUser($objData = null) {
		$this->SimpleUser($objData);
		if ($objData != null) {
			$this->mUserId = $objData->userId;
			$this->mUsername = $objData->userName;
			$this->mPassword = $objData->password;
		}
	}

}

/*********************************************************
		Class :				User

		Last update :	22 Mar 02

		Description:		User - creation, update, delete, login
								and authentication.

*********************************************************/

class User Extends ContactUser {

	var $mUserName;
	function getUsername() { return $this->mUsername; }
	function setUsername($data) { $this->mUsername = $data; }

    var $mPassword;      // password : String
	function getPassword() { return $this->mPassword; }
    function setPassword($data) { $this->mPassword = $data; }

	var $mPassword1;	
    Function getPassword1() { Return $this->mPassword1; }
    Function setPassword1($Password) { $this->mPassword1 = $Password; }

	var $mPassword2;
    Function getPassword2() { Return $this->mPassword2; }
    Function setPassword2($Password2) { $this->mPassword2 = $Password2; }

	var $mPasswordHash;	// encrypted password
	function getPasswordHash() { return $this->mPasswordHash; }
	function setPasswordHash($data) { $this->mPasswordHash = $data; }

    var $mSalt;          // Salt value is random string for password security : String
	function getSalt() { return $this->mSalt; }

    function setSalt($data) { $this->mSalt = $data; }
    var $mDisabled;      // Account Enabled/Disabled             : Boolean
	function getDisabled() 
    {
        if ( $this->mDisabled == "1" ) { return true; }
        else {  return false; }
    }
	function setDisabled($data) 
    {
        if ( $data )  { $this->mDisabled = 1; }
        else { $this->mDisabled = 0; }
    }

//+++++++++++++++++++++ Constructor ++++++++++++++++++++
    function User($objData = null)
    {
		$this->ContactUser($objData);
		if ($objData != null) {
			$this->mUserName = $objData->userName;
			// $this->mPassword = $objData->password;
			$this->mPasswordHash = $objData->passwordHash;
			$this->mSalt = $objData->salt;
			$this->mDisabled = $objData->disabled;
			$this->mAlert = $objData->alert;
			
		}
    }

//++++++++++++++++++++++ Load ++++++++++++++++++++++++

	function loadUser() {
		if ($this->mUserId) {
			return $this->loadById($this->mUserId);
		} else {
			return false;
		}
	}
	
    function loadById($userId) { 
		return $this->privLoad("userId",$userId);
    }

    function loadByUserName($Username) {
		return $this->privLoad("username",$Username); 
	}

    function privLoad($by, $value) {
		$strSql_User = "SELECT u.* "
				." FROM ".$this->TABLE_USER." u";
        switch (strToLower($by))
        {
            case "username":
                $strSql_User .= " WHERE userName = " .$this->convStr4SQL($value);
                break;
            case "userid":
                $strSql_User .= " WHERE userId = ".$value;
                break;
			case "code":
				$strSql_User .= " WHERE code = ".$value;
				break;
        }
		//echo($strSql_User);
        $this->getConnection();
        if ($result = $this->query($strSql_User))
        {
            if ($row = $result->nextRow())
            {
                $this->User($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
    }

	Function privIsLoad()
    {
        If ( $this->mUserId == 0 Or $this->mUserId =="" ) { Return False; }
        Else { Return True; }
    }

//+++++++++++++++++++ Update +++++++++++++++++++++++++

    function update($flag = true) {
        if ($this->mPassword != "") {
			$this->mSalt = $this->createSalt(8);
			$this->mPasswordHash = $this->encrypt($this->mPassword , $this->mSalt);
		}
        if ( !$this->privIsLoad() )
        { 
            echo "Please specify which user you want to update";
            exit;
        }

        $strSql  = "UPDATE " .$this->TABLE_USER ." SET "
			." userName = ".$this->convStr4SQL($this->mUsername);

		if ($this->mPassword != "") {
        	$strSql .= ", password = ".$this->convStr4SQL($this->mPasswordHash);
        	$strSql .= ", salt = ".$this->convStr4SQL($this->mSalt);
		}
        $strSql .= " WHERE userId=" .$this->mUserId;
        $this->getConnection();
        return $this->query($strSql);
    }

	function updatePassword()
    {
		$newPass = $this->mPassword;
        if ((empty($newPass)) || (!$this->mUserId)) {
			return false;
		}
        $strSalt = $this->createSalt(8);
        $strNewPasswordHash = $this->encrypt($newPass ,$strSalt);
        
        $strSql  = "UPDATE " .$this->TABLE_USER;
		$strSql.=" SET password=" .$this->convStr4SQL($strNewPasswordHash)
			." , salt=" .$this->convStr4SQL($strSalt);
        $strSql .= " WHERE userId=" .$userId;
        $this->getConnection();
        return $this->query($strSql);
    }

	function updateDisabled() {
		if (!$this->mUserId) {
			return false;
		}
		$strSql  = "UPDATE " .$this->TABLE_USER;
		$strSql.=" SET disabled=" .$this->mDisabled;
        $strSql .= " WHERE userId=" .$userId;
        $this->getConnection();
        return $this->query($strSql);
	}

	function init($mode){
	Global $hId;
	Global $hUsername;
	Global $hPassword, $hPassword1, $hPassword2;
	
		switch (strtolower($mode)) {
		case "add":
		case "update":
			$this->setId($hId);
			$this->setUsername(stripslashes(htmlspecialchars($hUsername)));
			$this->setPassword(stripslashes(htmlspecialchars($hPassword)));
			$this->setPassword1(stripslashes(htmlspecialchars($hPassword1)));
			$this->setPassword2(stripslashes(htmlspecialchars($hPassword2)));
			break;
		}
		
	}
	
	
//+++++++++++++++++++++ Log in/out +++++++++++++++++++++++

    function auth($username, $password, &$error)
    /*Authentications user.
    *   1. returns boolean.
    *   2. sets $error with error message.*/
    {
		global $langUserError;
        $error = '';
        if ($username == "")        //User not enter Username
        {
            $error = $langUserError[authUsernameEmpty];
            return false;
        }
        
        $strSql  = "SELECT password, salt, userId FROM " .$this->TABLE_USER;
        $strSql .= " WHERE username =" .$this->convStr4SQL($username);		
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
              $this->setUserId($row->userId);
              $strHashValue = $row->password;
              $strSalt = $row->salt;
              //Calculate Hash of specified password + Salt from DB
			  
              $strHashValue2  = $this->encrypt($password , $strSalt);
              if ( !isSet($strHashValue) || ($strHashValue == "") ) {
                $strHashValue = "";				
                $strHashValue = $this->encrypt($strHashValue ,$strSalt);
              }
			  /*
			  echo "username=".$username."<br>";
			  echo "password=".$password."<br>";
			  echo "salt=".$strSalt."<br>";
			  echo "hashDB=".$strHashValue."<br>";
			  echo "hashUS=".$strHashValue2."<br>";
			        */       
              if ( $strHashValue == $strHashValue2 ) //Password is correct.
              {
                $this->loadById($row->userId);
                if ( $this->mDisabled )    //Account Disabled
                {
                    $error = $langUserError[authAccountDisabled];
                } else {
                    return true;
                }
              }
              else  //Invalid password.
              {
				$error = $langUserError[authInvalidPassword];
              }
            }
            else // Username not found.
            {
              $error = $langUserError[authUsernameNotExist]; //user does not exist
            }
        }
		if (!empty($error)) {
			return false;
		} else {
			return true;
		}
    }

	Function encrypt($CryptText, $SaltValue = Null)
    {
        Return crypt($CryptText,$SaltValue);
    }
	
	
	
    function login()
    {
        global $sUserId;
        if ($this->mUserId != "")
        {
			// register user Id
            $this->setSessionVar("sUserId",$this->mUserId);
			$this->setSessionVar("sUserType",$this->mUserType);
        }
    }

    function logout()
    {
        global $sUserId;
        session_destroy();
    }

    function isLogged()
    {
        $lUserId = $this->getSessionVar('sUserId');
		$lUserType= $this->getSessionVar('sUserType');
        if ($lUserId == 0 || empty($lUserId) || $lUserType != $this->mUserType)
        {
            return false;
        }
        else    //Session or Cookies is not empty
        {
            $this->mUserId = $lUserId;
			// $this->mLevel = $this->getSessionVar('sUserLevel');
            return true;
        }
    }

//++++++++++++++++++ Validation functions +++++++++++++++++++++

	function checkCommon() {
		global $langUserError;
		return $asrErrReturn;
    }

	function setNewUserName($userName, &$error) {
		global $langUserError;

		if (!$this->mDisabled) {
			$intLength = strlen($userName);
			if (($intLength < USERNAME_MIN) || ($intLength > USERNAME_MAX)) {
				$error = $langUserError[strUsernameLength]; 
			} else if ($this->checkUnique($name, $type)) {
				$error =  $langUserError[strUsernameExisting];
			}
		} 
		if (!empty($error)) {
			return false;
		} else {
			return true;
		}
	}

	function setNewPassword($pwd1, $pwd2, &$error) {
		global $langUserError;
		if ((strlen($pwd1) < PASSWORD_MIN || strlen($pwd1) > PASSWORD_MAX) and (strlen($pwd1) > 0)) {
			$error = $langUserError[strPWDLength];
			return false;
		}
		if (strval($pwd1) != strval($pwd2)) {
			$error = $langUserError[strPWDVerify];
			return false;
		} else {
			$this->mPassword = $pwd1;
			return true;
		}
	}


    function checkUnique($name, $type)
    {
		if ($name == ""){
			return false;
		}
        $strSql  = "SELECT userId, ".$type." FROM " .$this->TABLE_USER;
        $strSql .= " WHERE ".$type." = " .$this->convStr4SQL($name);
		if ($this->mUserId) {
			$strSql .= " AND userId <> ".$this->mUserId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				return $row->userId;
			}
        }
        return false;
    }

	function validateEmailFormat($Email)     {
        
        global $langUserError;
        
        If ( trim($Email) == "" )
        { Return $langUserError[strEmailEmpty]; }
        
        If ( !eRegi("^[_\.\'0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$", $Email) )
        { Return $langUserError[strEmailInvalidFormat]; }
    }

	function valid_name($name)
	{
	   
	   // return FALSE if it contains characters which 
	   // which ARNT on the specified list
	   if(ereg('[^[:space:]a-zA-Z0-9_.-]{1,}', $name))
	   {
	     return false;
	   } 
	   else 
	   {
	     return true;
	   }
	
	}

//++++++++++++++++++ Misc functions +++++++++++++++++++++

	function setSessionVar($name, $value) {
		global $$name;
		$$name = $value;
		session_register($name);
	}

	function getSessionVar($name) {
		global $$name;
		return $$name;
	}

	function unsetSessionVar($name) {
		global $$name;
		if (session_is_registered($name)) {
			session_unregister($name);
		}
	}

    function createSalt($length = 8) // Generate random salt
    {
        mt_srand ((Double) microTime() * 1000000);
        $strSalt = "";
        $strChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $intLenChars = strlen($strChars);
        For ( $i = 0; $i < $length; $i++ ) 
        {
            $n = mt_rand(1, $intLenChars);
            $strSalt .= substr($strChars, $n, 1);
        }
        return $strSalt;
    }
	
}


/*********************************************************
		Class :				UserList

		Last update :	03 Jan 02

		Description:		Simple list of users (simpleUser or full User)

*********************************************************/

class UserList extends DataList {

	var $TABLE_LIST;
	
	function UserList() {
		$this->TABLE_LIST = "User";
		$this->DataList();
	}

    function load () {

        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT userId) as rowCount FROM ".$this->TABLE_LIST
			." ".$this->getFilterSQL();	// WHERE clause
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * "
			."FROM ".$this->TABLE_LIST." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new User($row);
			}
			return true;
		} else {
			return false;
		}
    }

	function printSelect($name, $opt = false, $defaultId = null) {
		echo ("\n<select name=\"".$name."\">\n");
		for ($i =0; $i < $this->mCount; $i++) {
			$objItem = $this->itemList[$i];
			echo("\t<option value=\"".$objItem->getUserId()."\"");
			if (($defaultId != null) && ($objItem->getUserId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getFullName(22)."</option>\n");
		}
		echo("</select>\n");
	}

}

?>
<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_event_price_item table

*********************************************************/
 
class EventPriceItem extends DB{

	var $TABLE="t_event_price_item";

	var $m_event_price_item_id;
	function get_event_price_item_id() { return $this->m_event_price_item_id; }
	function set_event_price_item_id($data) { $this->m_event_price_item_id = $data; }
	
	var $m_event_price_id;
	function get_event_price_id() { return $this->m_event_price_id; }
	function set_event_price_id($data) { $this->m_event_price_id = $data; }
	
	var $m_event_id;
	function get_event_id() { return $this->m_event_id; }
	function set_event_id($data) { $this->m_event_id = $data; }
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }

	
	function EventPriceItem($objData=NULL) {
        If ($objData->event_price_item_id !="") {
			$this->set_event_price_item_id($objData->event_price_item_id);
			$this->set_event_price_id($objData->event_price_id);
			$this->set_event_id($objData->event_id);
			$this->set_price($objData->price);
			$this->set_remark($objData->remark);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mEventPriceItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE event_price_item_id =".$this->mEventPriceItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventPriceItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mEventPriceItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE event_price_item_id =".$this->mEventPriceItemId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventPriceItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventPriceItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( event_price_id , event_id , price , remark ) " ." VALUES ( "
		." '".$this->m_event_price_id."' , "
		." '".$this->m_event_id."' , "
		." '".$this->m_price."' , "
		." '".$this->m_remark."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mEventPriceItemId = mysql_insert_id();
            return $this->mEventPriceItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." event_price_id = '".$this->m_event_price_id."' "
		." , event_id = '".$this->m_event_id."' "
		." , price = '".$this->m_price."' "
		." , remark = '".$this->m_remark."' "
		." WHERE event_price_item_id = ".$this->m_event_price_item_id." ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE event_price_item_id=".$this->mEventPriceItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class EventPriceItemList extends DataList {
	var $TABLE = "t_event_price_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT event_price_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new EventPriceItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getEventPriceItemId()."\"");
			if (($defaultId != null) && ($objItem->getEventPriceItemId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
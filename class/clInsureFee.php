<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureFee extends DB{

	var $TABLE="t_insure_fee";

	var $m_insure_fee_id;
	function get_insure_fee_id() { return $this->m_insure_fee_id; }
	function set_insure_fee_id($data) { $this->m_insure_fee_id = $data; }
	
	var $m_use_drive;
	function get_use_drive() { return $this->m_use_drive; }
	function set_use_drive($data) { $this->m_use_drive = $data; }
	
	var $m_use_type;
	function get_use_type() { return $this->m_use_type; }
	function set_use_type($data) { $this->m_use_type = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }
	
	var $m_insure_fee;
	function get_insure_fee() { return $this->m_insure_fee; }
	function set_insure_fee($data) { $this->m_insure_fee = $data; }
	
	var $m_insure_vat;
	function get_insure_vat() { return $this->m_insure_vat; }
	function set_insure_vat($data) { $this->m_insure_vat = $data; }
	
	var $m_argon;
	function get_argon() { return $this->m_argon; }
	function set_argon($data) { $this->m_argon = $data; }

	var $m_total;
	function get_total() { return $this->m_total; }
	function set_total($data) { $this->m_total = $data; }
	
	function InsureFee($objData=NULL) {
        If ($objData->insure_fee_id !="") {
			$this->set_insure_fee_id($objData->insure_fee_id);
			$this->set_use_type($objData->use_type);
			$this->set_use_drive($objData->use_drive);
			$this->set_title($objData->title);
			$this->set_code($objData->code);
			$this->set_insure_fee($objData->insure_fee);
			$this->set_insure_vat($objData->insure_vat);
			$this->set_argon($objData->argon);
			$this->set_total($objData->total);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_fee_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_fee_id =".$this->m_insure_fee_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFee($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFee($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." (  title , use_type, use_drive, code, insure_fee, insure_vat ,argon,total ) " ." VALUES ( "

		." '".$this->m_title."' , "
		." '".$this->m_use_type."' , "
		." '".$this->m_use_drive."' , "
		." '".$this->m_code."' , "
		." '".$this->m_insure_fee."' , "
		." '".$this->m_insure_vat."' , "
		." '".$this->m_argon."' , "
		." '".$this->m_total."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_fee_id = mysql_insert_id();
            return $this->m_insure_fee_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  title = '".$this->m_title."' "
		." , use_type = '".$this->m_use_type."' "
		." , use_drive = '".$this->m_use_drive."' "
		." , code = '".$this->m_code."' "
		." , insure_fee = '".$this->m_insure_fee."' "
		." , insure_vat = '".$this->m_insure_vat."' "
		." , argon = '".$this->m_argon."' "
		." , total = '".$this->m_total."' "
		." WHERE insure_fee_id = ".$this->m_insure_fee_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_fee_id=".$this->m_insure_fee_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureFeeList extends DataList {
	var $TABLE = "t_insure_fee";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_fee_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureFee($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_fee_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureFee($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_insure_fee_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_fee_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
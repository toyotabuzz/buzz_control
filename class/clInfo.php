<?
/*********************************************************
		Class :					Customer Title

		Last update :	  10 Jan 02

		Description:	  Class manage t_info table

*********************************************************/
 
class Info extends DB{

	var $TABLE="t_info";

	var $mInfoId;
	function getInfoId() { return $this->mInfoId; }
	function setInfoId($data) { $this->mInfoId = $data; }
	
	var $mOrderNumberDefault;
	function getOrderNumberDefault() { return htmlspecialchars($this->mOrderNumberDefault); }
	function setOrderNumberDefault($data) { $this->mOrderNumberDefault = $data; }
	
	var $mOrderNumberRecent;
	function getOrderNumberRecent() { return htmlspecialchars($this->mOrderNumberRecent); }
	function setOrderNumberRecent($data) { $this->mOrderNumberRecent = $data; }	
	
	function getOrderNumber(){
		$orderNum = $this->mOrderNumberRecent+1;
		if($orderNum <10) $strBooking = "0000".$orderNum;
		if($orderNum <100 AND $orderNum >=10) $strBooking = "000".$orderNum;
		if($orderNum <1000  AND $orderNum >=100) $strBooking = "00".$orderNum;
		if($orderNum <10000  AND $orderNum >=1000) $strBooking = "0".$orderNum;
		if($orderNum >=10000  ) $strBooking = $orderNum;
		return $this->mOrderNumberDefault."A1".date("ymd")."/".$strBooking;
	}
	
	var $mOrderNumberNoheadDefault;
	function getOrderNumberNoheadDefault() { return htmlspecialchars($this->mOrderNumberNoheadDefault); }
	function setOrderNumberNoheadDefault($data) { $this->mOrderNumberNoheadDefault = $data; }
	
	var $mOrderNumberNoheadRecent;
	function getOrderNumberNoheadRecent() { return htmlspecialchars($this->mOrderNumberNoheadRecent); }
	function setOrderNumberNoheadRecent($data) { $this->mOrderNumberNoheadRecent = $data; }		
	
	var $mOrderNumber01;
	function getOrderNumber01() { return htmlspecialchars($this->mOrderNumber01); }
	function setOrderNumber01($data) { $this->mOrderNumber01 = $data; }
	
	var $mOrderNumber02;
	function getOrderNumber02() { return htmlspecialchars($this->mOrderNumber02); }
	function setOrderNumber02($data) { $this->mOrderNumber02 = $data; }		
	
	function getOrderNumberNohead(){
		$sendingNum = $this->mOrderNumberNoheadRecent+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return $this->mOrderNumberNoheadDefault."B1".date("ymd")."/".$strSending;
	}	
	
	function genOrderNumber01(){
		$sendingNum = $this->mOrderNumber01+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return "C1".date("ymd")."/".$strSending;
	}	
	
	function genOrderNumber02(){
		$sendingNum = $this->mOrderNumber02+1;
		if($sendingNum <10) $strSending = "0000".$sendingNum;
		if($sendingNum <100  AND $sendingNum >=10) $strSending = "000".$sendingNum;
		if($sendingNum <1000  AND $sendingNum >=100) $strSending = "00".$sendingNum;
		if($sendingNum <10000  AND $sendingNum >=1000) $strSending = "0".$sendingNum;
		if($sendingNum >=10000  ) $strSending = $sendingNum;
		return "D1".date("ymd")."/".$strSending;
	}			
	
	var $mHalfCom;
	function getHalfCom() { return htmlspecialchars($this->mHalfCom); }
	function setHalfCom($data) { $this->mHalfCom = $data; }
	
	var $mNoCom;
	function getNoCom() { return htmlspecialchars($this->mNoCom); }
	function setNoCom($data) { $this->mNoCom = $data; }
	
	var $mFullCom;
	function getFullCom() { return htmlspecialchars($this->mFullCom); }
	function setFullCom($data) { $this->mFullCom = $data; }
	
	var $mNetCom;
	function getNetCom() { return htmlspecialchars($this->mNetCom); }
	function setNetCom($data) { $this->mNetCom = $data; }

	
	function Info($objData=NULL) {
        If ($objData->info_id !="") {
            $this->setInfoId($objData->info_id);
			$this->setOrderNumberDefault($objData->order_number_default);
			$this->setOrderNumberRecent($objData->order_number_recent);
			$this->setOrderNumberNoheadDefault($objData->order_number_nohead_default);
			$this->setOrderNumberNoheadRecent($objData->order_number_nohead_recent);
			$this->setOrderNumber01($objData->order_number_01);
			$this->setOrderNumber02($objData->order_number_02);
			$this->setHalfCom($objData->half_com);
			$this->setNoCom($objData->no_com);
			$this->setFullCom($objData->full_com);
			$this->setNetCom($objData->net_com);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mInfoId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE info_id =".$this->mInfoId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Info($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Info($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_number_default, order_number_recent, order_number_nohead_default, order_number_nohead_recent) "
						." VALUES ( '".$this->mOrderNumberDefault."','".$this->mOrderNumberRecent."','".$this->mOrderNumberNoheadDefault."','".$this->mOrderNumberNoheadRecent."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mInfoId = mysql_insert_id();
            return $this->mInfoId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_default = '".$this->mOrderNumberDefault."'  "
						." , order_number_recent = '".$this->mOrderNumberRecent."'  "
						." , order_number_nohead_default = '".$this->mOrderNumberNoheadDefault."'  "
						." , order_number_nohead_recent = '".$this->mOrderNumberNoheadRecent."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateOrderNumberRecent(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_recent = '".($this->mOrderNumberRecent+1)."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateCom(){
		$strSql = "UPDATE ".$this->TABLE
						." SET half_com = '".($this->mHalfCom)."'  "
						." , no_com = '".($this->mNoCom)."'  "
						." , full_com = '".($this->mFullCom)."'  "
						." , net_com = '".($this->mNetCom)."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	function updateOrderNumberNoheadRecent(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_nohead_recent = '".($this->mOrderNumberNoheadRecent+1)."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOrderNumber01(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_01 = '".($this->mOrderNumber01+1)."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateOrderNumber02(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_number_02 = '".($this->mOrderNumber02+1)."'  "
						." WHERE  info_id = ".$this->mInfoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE info_id=".$this->mInfoId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Title List

		Last update :		22 Mar 02

		Description:		Customer Title List

*********************************************************/

class InfoList extends DataList {
	var $TABLE = "t_info";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT info_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Info($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInfoId()."\"");
			if (($defaultId != null) && ($objItem->getInfoId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInfoId()."\"");
			if (($defaultId != null) && ($objItem->getInfoId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
<?
/*********************************************************
		Class :					Stock Car Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_car_item table

*********************************************************/
 
class StockCarItem extends DB{

	var $TABLE="t_stock_car_item";

	var $mStockCarItemId;
	function getStockCarItemId() { return $this->mStockCarItemId; }
	function setStockCarItemId($data) { $this->mStockCarItemId = $data; }
	
	var $mStockCarId;
	function getStockCarId() { return $this->mStockCarId; }
	function setStockCarId($data) { $this->mStockCarId = $data; }
	
	var $mStockDate;
	function getStockDate() { return $this->mStockDate; }
	function setStockDate($data) { $this->mStockDate = $data; }	
	
	var $mStockTime;
	function getStockTime() { return $this->mStockTime; }
	function setStockTime($data) { $this->mStockTime = $data; }	
	
	var $mStockBy;
	function getStockBy() { return $this->mStockBy; }
	function setStockBy($data) { $this->mStockBy = $data; }	
	
	var $mSaleId;
	function getSaleId() { return $this->mSaleId; }
	function setSaleId($data) { $this->mSaleId = $data; }	
	
	var $mStockNumber;
	function getStockNumber() { return $this->mStockNumber; }
	function setStockNumber($data) { $this->mStockNumber = $data; }	
	
	var $mStockCargoId;
	function getStockCargoId() { return $this->mStockCargoId; }
	function setStockCargoId($data) { $this->mStockCargoId = $data; }	
	
	var $mParking;
	function getParking() { return $this->mParking; }
	function setParking($data) { $this->mParking = $data; }
	
	var $mStockReasonId;
	function getStockReasonId() { return $this->mStockReasonId; }
	function setStockReasonId($data) { $this->mStockReasonId = $data; }	
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mStockDateReturn;
	function getStockDateReturn() { return $this->mStockDateReturn; }
	function setStockDateReturn($data) { $this->mStockDateReturn = $data; }	
	
	var $mStockTimeReturn;
	function getStockTimeReturn() { return $this->mStockTimeReturn; }
	function setStockTimeReturn($data) { $this->mStockTimeReturn = $data; }	
	
	var $mStockByReturn;
	function getStockByReturn() { return $this->mStockByReturn; }
	function setStockByReturn($data) { $this->mStockByReturn = $data; }	
	
	var $mSaleIdReturn;
	function getSaleIdReturn() { return $this->mSaleIdReturn; }
	function setSaleIdReturn($data) { $this->mSaleIdReturn = $data; }	
	
	var $mStockNumberReturn;
	function getStockNumberReturn() { return $this->mStockNumberReturn; }
	function setStockNumberReturn($data) { $this->mStockNumberReturn = $data; }	
	
	var $mStockCargoIdReturn;
	function getStockCargoIdReturn() { return $this->mStockCargoIdReturn; }
	function setStockCargoIdReturn($data) { $this->mStockCargoIdReturn = $data; }	
	
	var $mParkingReturn;
	function getParkingReturn() { return $this->mParkingReturn; }
	function setParkingReturn($data) { $this->mParkingReturn = $data; }
	
	var $mRemarkReturn;
	function getRemarkReturn() { return $this->mRemarkReturn; }
	function setRemarkReturn($data) { $this->mRemarkReturn = $data; }	
	
	var $mComplete;
	function getComplete() { return $this->mComplete; }
	function setComplete($data) { $this->mComplete = $data; }		
	
	function StockCarItem($objData=NULL) {
        If ($objData->stock_car_item_id !="") {
            $this->setStockCarItemId($objData->stock_car_item_id);
			$this->setStockCarId($objData->stock_car_id);
			$this->setStockDate($objData->stock_date);
			$this->setStockTime($objData->stock_time);
			$this->setStockBy($objData->stock_by);
			$this->setSaleId($objData->sale_id);
			$this->setStockNumber($objData->stock_number);
			$this->setStockCargoId($objData->stock_cargo_id);
			$this->setParking($objData->parking);
			$this->setStockReasonId($objData->stock_reason_id);
			$this->setRemark($objData->remark);
			$this->setStockDateReturn($objData->stock_date_return);
			$this->setStockTimeReturn($objData->stock_time_return);
			$this->setStockByReturn($objData->stock_by_return);
			$this->setSaleIdReturn($objData->sale_id_return);
			$this->setStockNumberReturn($objData->stock_number_return);
			$this->setStockCargoIdReturn($objData->stock_cargo_id_return);
			$this->setParkingReturn($objData->parking_return);
			$this->setRemarkReturn($objData->remark_return);
			$this->setComplete($objData->complete);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
		$this->setParking(stripslashes($this->mParking));
	}
	
	function load() {

		if ($this->mStockCarItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_car_item_id =".$this->mStockCarItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_car_id,stock_date, stock_time, stock_reason_id, stock_number, stock_by, sale_id, stock_cargo_id, parking, remark ) "
						." VALUES ( '".$this->mStockCarId
						."' , '".$this->mStockDate
						."', '".$this->mStockTime
						."' ,  '".$this->mStockReasonId
						."' ,  '".$this->mStockNumber
						."' ,  '".$this->mStockBy
						."' , '".$this->mSaleId
						."' , '".$this->mStockCargoId
						."' , '".$this->mParking
						."' , '".$this->mRemark."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockCarItemId = mysql_insert_id();
            return $this->mStockCarItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_car_id = '".$this->mStockCarId."'  "
						." , stock_date_return = '".$this->mStockDateReturn."'  "
						." , stock_time_return = '".$this->mStockTimeReturn."'  "
						." , stock_by_return = '".$this->mStockByReturn."'  "
						." , sale_id_return = '".$this->mSaleIdReturn."'  "
						." , stock_cargo_id_return = '".$this->mStockCargoIdReturn."'  "
						." , parking_return = '".$this->mParkingReturn."'  "
						." , remark_return = '".$this->mRemarkReturn."'  "
						." , complete = '".$this->mComplete."'  "
						." WHERE  stock_car_item_id = ".$this->mStockCarItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_car_item_id=".$this->mStockCarItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockCarId == "") $asrErrReturn["stock_car_id"] = "��س��к�";		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Car Item List

		Last update :		22 Mar 02

		Description:		Customer Stock Car Item List

*********************************************************/

class StockCarItemList extends DataList {
	var $TABLE = "t_stock_car_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_car_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCarItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
}
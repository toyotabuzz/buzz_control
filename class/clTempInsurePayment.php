<?
/*********************************************************
		Class :					Order Payment

		Last update :	  10 Jan 02

		Description:	  Class manage t_temp_insure_payment table

*********************************************************/
 
class TempInsurePayment extends DB{

	var $TABLE="t_temp_insure_payment";

	var $m_link_id;
	function get_link_id() { return $this->m_link_id; }
	function set_link_id($data) { $this->m_link_id = $data; }			
	
	var $mTempInsurePaymentId;
	function getTempInsurePaymentId() { return $this->mTempInsurePaymentId; }
	function setTempInsurePaymentId($data) { $this->mTempInsurePaymentId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mOrderExtraId;
	function getOrderExtraId() { return $this->mOrderExtraId; }
	function setOrderExtraId($data) { $this->mOrderExtraId = $data; }

	var $mPaymentTypeId;
	function getPaymentTypeId() { return $this->mPaymentTypeId; }
	function setPaymentTypeId($data) { $this->mPaymentTypeId = $data; }

	var $mPaymentTypeTitle;
	function getPaymentTypeTitle() { return $this->mPaymentTypeTitle; }
	function setPaymentTypeTitle($data) { $this->mPaymentTypeTitle = $data; }
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mBankId;
	function getBankId() { return htmlspecialchars($this->mBankId); }
	function setBankId($data) { $this->mBankId = $data; }

	var $mBankTitle;
	function getBankTitle() { return htmlspecialchars($this->mBankTitle); }
	function setBankTitle($data) { $this->mBankTitle = $data; }
	
	var $mBranch;
	function getBranch() { return htmlspecialchars($this->mBranch); }
	function setBranch($data) { $this->mBranch = $data; }

	var $mCheckNo;
	function getCheckNo() { return htmlspecialchars($this->mCheckNo); }
	function setCheckNo($data) { $this->mCheckNo = $data; }
	
	var $mCheckDate;
	function getCheckDate() { return htmlspecialchars($this->mCheckDate); }
	function setCheckDate($data) { $this->mCheckDate = $data; }
		
	//0 = �ͧ��������
	//1 = ���ͺ�����	
	//2 = ���ͺ��������
	//3 = ������������
	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }		
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }		
	
	var $mNoHead;
	function getNoHead() { return htmlspecialchars($this->mNoHead); }
	function setNoHead($data) { $this->mNoHead = $data; }		
	
	var $mBookBankId;
	function getBookBankId() { return htmlspecialchars($this->mBookBankId); }
	function setBookBankId($data) { $this->mBookBankId = $data; }		
	
	var $mAccCheck;
	function getAccCheck() { return htmlspecialchars($this->mAccCheck); }
	function setAccCheck($data) { $this->mAccCheck = $data; }			
	
	var $mAccCheckDate;
	function getAccCheckDate() { return htmlspecialchars($this->mAccCheckDate); }
	function setAccCheckDate($data) { $this->mAccCheckDate = $data; }			
	
	var $mAccCheckBy;
	function getAccCheckBy() { return htmlspecialchars($this->mAccCheckBy); }
	function setAccCheckBy($data) { $this->mAccCheckBy = $data; }		
	
	var $mAccCheckRemark;
	function getAccCheckRemark() { return htmlspecialchars($this->mAccCheckRemark); }
	function setAccCheckRemark($data) { $this->mAccCheckRemark = $data; }				
	
	var $mPaymentNo;
	function getPaymentNo() { return htmlspecialchars($this->mPaymentNo); }
	function setPaymentNo($data) { $this->mPaymentNo = $data; }			
	
	function TempInsurePayment($objData=NULL) {
        If ($objData->insure_payment_id !="") {
			$this->set_link_id($objData->link_id);
            $this->setTempInsurePaymentId($objData->insure_payment_id);
			$this->setOrderId($objData->order_id);
			$this->setOrderExtraId($objData->order_extra_id);
			$this->setPaymentTypeId($objData->payment_type_id);
			$this->setPaymentTypeTitle($objData->payment_type_title);
			$this->setPrice($objData->price);
			$this->setBankId($objData->bank_id);
			$this->setBankTitle($objData->bank_title);
			$this->setBranch($objData->branch);
			$this->setCheckNo($objData->check_no);
			$this->setCheckDate($objData->check_date);
			$this->setStatus($objData->status);
			$this->setRemark($objData->remark);
			$this->setNoHead($objData->no_head);
			$this->setBookBankId($objData->bookbank_id);
			$this->setAccCheck($objData->acc_check);
			$this->setAccCheckDate($objData->acc_check_date);
			$this->setAccCheckBy($objData->acc_check_by);
			$this->setAccCheckRemark($objData->acc_check_remark);
			$this->setPaymentNo($objData->payment_no);
			
        }
    }

	function init(){
		$this->setBranch(stripslashes($this->mBranch));
		$this->setCheckNo(stripslashes($this->mCheckNo));
		$this->setPrice(stripslashes($this->mPrice));
	}
		
	function load() {

		if ($this->mTempInsurePaymentId == '') {
			return false;
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE insure_payment_id =".$this->mTempInsurePaymentId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsurePayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE ".$strCondition;
		
		//echo $strSql."<br>";
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsurePayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSum($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SUM(OP.price) AS sum_price FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "		
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return $row->sum_price;
            }
        }
		return false;
	}	
	
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( insure_payment_id, link_id,  order_id, order_extra_id, payment_type_id, price, bank_id, branch, check_no, check_date, remark, no_head, status, bookbank_id,  payment_no ) "
						." VALUES (  '".$this->mTempInsurePaymentId."',   '".$this->m_link_id."',  '".$this->mOrderId."','".$this->mOrderExtraId."','".$this->mPaymentTypeId."','".$this->mPrice."','".$this->mBankId."','".$this->mBranch."','".$this->mCheckNo."','".$this->mCheckDate."','".$this->mRemark."','".$this->mNoHead."' ,'".$this->mStatus."' ,'".$this->mBookBankId."' ,'".$this->mPaymentNo."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mTempInsurePaymentId = mysql_insert_id();
            return $this->mTempInsurePaymentId;
        } else {
			return false;
	    }
	}


	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_payment_id=".$this->mTempInsurePaymentId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_order_price_payment  "
                . " WHERE insure_payment_id=".$this->mTempInsurePaymentId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Payment List

		Last update :		22 Mar 02

		Description:		Order Payment List

*********************************************************/

class TempInsurePaymentList extends DataList {
	var $TABLE = "t_temp_insure_payment";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_payment_id) as rowCount FROM ".$this->TABLE
			." OP  ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new TempInsurePayment($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getTempInsurePaymentId()."\"");
			if (($defaultId != null) && ($objItem->getTempInsurePaymentId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getPaymentTypeTitle().",".$objItem->getPrice()."</option>");
		}
		echo("</select>");
	}

}
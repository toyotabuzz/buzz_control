<?
/*********************************************************
		Class :				PO

		Last update :		21 May 05

		Description:		Class manage PO_Order table

*********************************************************/
 
class PO extends DB{

	var $TABLE="t_po";
	
	var $mPoId;
	function getPoId() { return $this->mPoId; }
	function setPoId($data) { $this->mPoId = $data; }

	var $mMemberId;
	function getMemberId() { return $this->mMemberId;}
	function setMemberId($data) { $this->mMemberId = $data; }

	var $mPoNumber;
	function getPoNumber() { return $this->mPoNumber;}
	function setPoNumber($data) { $this->mPoNumber = $data; }
	
	var $mPoDate;
	function getPoDate() { return htmlspecialchars($this->mPoDate); }
	function setPoDate($data) { $this->mPoDate = $data; }

	var $mRemark;
	function getRemark() { return $this->mRemark;}
	function setRemark($data) { $this->mRemark = $data; }	
	
	var $mDealerId;
	function getDealerId() { return $this->mDealerId;}
	function setDealerId($data) { $this->mDealerId = $data; }		
	
	var $mPoDay;
	function getPoDay() { return $this->mPoDay;}
	function setPoDay($data) { $this->mPoDay = $data; }			
	
	var $mVat;
	function getVat() { return $this->mVat;}
	function setVat($data) { $this->mVat = $data; }				
	
	var $mDiscountPercent;
	function getDiscountPercent() { return $this->mDiscountPercent;}
	function setDiscountPercent($data) { $this->mDiscountPercent = $data; }				
	
	var $mDiscountCash;
	function getDiscountCash() { return $this->mDiscountCash;}
	function setDiscountCash($data) { $this->mDiscountCash = $data; }				
	
	var $mStatus;
	function getStatus() { return $this->mStatus;}
	function setStatus($data) { $this->mStatus = $data; }
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId;}
	function setCompanyId($data) { $this->mCompanyId = $data; }	
	
	function getStatusDetail(){
		if($this->mStatus == "0") return "���Ѻ�Թ���";
		if($this->mStatus == "1") return "�Ѻ�Թ�����¡�����ú";
		if($this->mStatus == "2") return "�Ѻ�Թ��Ҥú����ӹǹ";
	}	
	
	
	function PO($objData=NULL) {
        If ($objData->po_id!="") {
            $this->setPoId($objData->po_id);
            $this->setMemberId($objData->member_id);
			$this->setPoNumber($objData->po_number);
            $this->setPoDate($objData->po_date);
			$this->setRemark($objData->remark);
			$this->setDealerId($objData->dealer_id);
			$this->setPoDay($objData->po_day);
			$this->setVat($objData->vat);
			$this->setDiscountPercent($objData->discount_percent);
			$this->setDiscountCash($objData->discount_cash);
			$this->setStatus($objData->status);
			$this->setCompanyId($objData->company_id);
        }
    }

	function init(){	
		$this->setRemark(stripslashes($this->mRemark));
	}
	
	function load() {

		if ($this->mPoId == '') {
			return false;
		}
		$strSql = "SELECT O.*  FROM ".$this->TABLE." O "	
					." WHERE po_id = ".$this->mPoId;
		$this->getConnection();

        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PO($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadMaxPoNumber() {

		$strSql = "SELECT MAX(po_number) AS po_number FROM ".$this->TABLE;
		
		//echo $strSql;
		
		$this->getConnection();

        if ($result = $this->query($strSql))
        {	
            if ($row = $result->nextRow())
            {
	            $result->freeResult();
				return $row->po_number;
            }
		
		}
		return false;
	}	
	
	
	function add() {
		global $sCompanyId;
		$strSql = "INSERT INTO ".$this->TABLE
						." ( member_id, po_number, po_date, remark, po_day, vat, discount_percent, discount_cash, company_id, dealer_Id ) "
						." VALUES ( '".$this->mMemberId."' , "
						."  '".$this->mPoNumber."' , "
						."  '".$this->mPoDate."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mPoDay."' , "
						."  '".$this->mVat."' , "
						."  '".$this->mDiscountPercent."' , "
						."  '".$this->mDiscountCash."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mDealerId."' ) ";
						
        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mPoId = mysql_insert_id();
            return $this->mPoId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET member_id = '".$this->mMemberId."'  "
						." ,  po_number = '".$this->mPoNumber."'  "
						." ,  po_date = '".$this->mPoDate."'  "
						." ,  dealer_id = '".$this->mDealerId."'  "
						." ,  po_day = '".$this->mPoDay."'  "
						." ,  vat = '".$this->mVat."'  "
						." ,  discount_percent = '".$this->mDiscountPercent."'  "
						." ,  discount_cash = '".$this->mDiscountCash."'  "
						." ,  remark = '".$this->mRemark."'  "
						." WHERE  po_id = ".$this->mPoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}


	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status= '".$this->mStatus."'  "
						." WHERE  po_id = ".$this->mPoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE po_id=".$this->mPoId." ";
				
        $this->getConnection();
        $this->query($strSql);
		$this->deleteOrderItem();
		//echo $strSql;

		$this->unsetConnection();

	}

	 Function check($strMode)
    {
		global $langUserError;
		$asrErrReturn=array();
        Switch ( strtolower($strMode) )
        {
            Case "update":
            Case "add":
				
				if ($this->mMemberId == "") $asrErrReturn["member_id"] = "��س��кؼ����觫���";
				
				if ($this->mPoNumber == "") {
					$asrErrReturn["po_number"] = '��س��к��Ţ���㺢ͫ���';
				}else{
					if(!$this->checkUniquePoNumber(strtolower($strMode) )){
						$asrErrReturn["po_number"] = '�Ţ���㺢ͫ��ͫ�ӫ�͹';
					}		
				}				
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function deleteOrderItem(){
       $strSql = " UPDATE t_pr_item  SET po_id = 0  WHERE po_id =".$this->mPoId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
    function checkUniquePoNumber($strMode)
    {
        $strSql  = "SELECT po_number FROM t_po ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE po_number = ".trim(strtoupper($this->convStr4SQL($this->mPoNumber)));
		}else{
			$strSql .= " WHERE po_number = ".trim(strtoupper($this->convStr4SQL($this->mPoNumber)))." and po_id != ".$this->mPoId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
	
}

/*********************************************************
		Class :				POList

		Last update :		21 May 05

		Description:	Class manage pr  list

*********************************************************/


class POList extends DataList {
	var $TABLE = "t_po";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT po_id) as rowCount FROM ".$this->TABLE." O  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT O.*  FROM ".$this->TABLE." O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PO($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadPurchase() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.po_id) as rowCount FROM ".$this->TABLE." O  "
		." left join t_pr_item  PR ON PR.po_id = O.po_id "
		." left join t_pr  PRS ON PRS.pr_id = PR.pr_id "
		." left join t_stock_car  SC ON SC.stock_car_id = PRS.stock_car_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT Distinct O.*  FROM ".$this->TABLE." O "
			." left join t_pr_item  PR ON PR.po_id = O.po_id "
			." left join t_pr  PRS ON PRS.pr_id = PR.pr_id "
			." left join t_stock_car  SC ON SC.stock_car_id = PRS.stock_car_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PO($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT po_id) as rowCount FROM ".$this->TABLE." O  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT O.*  FROM ".$this->TABLE." O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PO($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
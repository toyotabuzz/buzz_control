<?

/*! @class DBResult -- database result set
	@abstract
		object = new DBResult(query, [result])
		Create object representing result set from query
	@discussion
		return false on error (print out error messages as well)

		EXAMPLE:
			$query = "SELECT * FROM myTable";
			$objDBResult = new DBResult($query,mysql_query($query));
			while ($row = $objDBResult->nextRow()) {
				$column1 = $row->column1
				$column2 = $row->column2
			}
		
		Notes:
			Instance is created by object DB
			Only methods should be used to retreive result rows
*/

Class DBResult {
    Var $db_result;
    Var $num_rows;
    Var $n;

    Function DBResult($qry,$result = null) {
        If ($result) {
            $this->db_result = $result;
            $this->num_rows = mysql_num_rows($result);
            $this->n = 0;
        } Else { //-- Modified by Pim : To be able to notice db error
            Echo "<i>".$qry."</i><br>";
            Echo "MySql #".mysql_errno().": ".mysql_error()."<br><br>";
            Return False;
        } //-- End Modified
    }

/*!
	@function getNumberRows
	@abstract
		integer = DBResult->getNumberRows()
		Get number of rows in result set
	@result
		integer		- number of rows
	@discussion
		EXAMPLE:
			$nbRows = $objDBResult->getNumberRows();
*/

    Function getNumberRows() {
        Return $this->num_rows;
    }

/*!
	@function hasMoreRow
	@abstract
		boolean = DBResult->hasMoreRow()
		When iterating over resultset, this function can be used to check if
		can iterate once more
	@result
		boolean		- has more rows or not
	@discussion
		EXAMPLE:
			while ($objResult->hasMoreRow()) {
				$row = $objResult->nextRow();
			}
		NOTE:
			This is actualy useless as nextRow returns false if no more row to get
*/

    Function hasMoreRow() {
        If ($this->n < $this->num_rows) {
            Return true;
        } Else {
            Return false;
        }
    }

/*!
	@function nextRow
	@abstract
		object = DBResult->nextRow()
		When iterating over resultset, this function returns an object that
		represents a row in databse
	@result
		object		- each property represents a field in table
	@discussion
		EXAMPLE:
			while ($row = $objDBResult->nextRow()) {
				echo($row->column1);
				echo($row->column2);
			}
		NOTE:
			Automatically free memory when last row is reached used freeResult()
*/

    Function nextRow() {
        $row = mysql_fetch_object($this->db_result);
        If (!$row) {
              $this->freeResult();
              Return false;
          }
        $this->n++;
          Return $row;
    }

/*!
	@function freeResult
	@abstract
		DBResult->freeResult()
		Free memory from result set and reset properties
	@discussion
		EXAMPLE
			if ($row = $objDBResult->nextRow()) {
				echo($row->column1);
				echo($row->column2);
				$objDBResult->freeResult();
			}
		NOTE
			Need to be called if result set if we know we don't call
			nextRow() when end of result set is reached (otherwise
			result set has been freed already)
*/
   
    Function freeResult() {
          If ($this->db_result) {
             //mysql_free_result($this->db_result);
          }
        $this->n = 0;
        $this->num_rows = 0;
      }
}

/*!
	@class DBConnection
	@abstract
		object = new DBConnection()
		Create object representing connection to database
	@discussion
		EXAMPLE<pre>
			$objDBConnection = new DBConnection();
			$objDBResult = $objDBConnection->querySelect("SELECT * FROM myTable");
			while ($row = $objDBResult->nextRow()) {
				$column1 = $row->column1
				$column2 = $row->column2
			}</pre>
		NOTES
			Not meant to be used directly by programmer
*/

Class DBConnection {
      Var $db_link;

      Function DBConnection() {
          $this->connect();
      }

      Function connect() {
		  // echo("<P>db://".DB_USER.":".DB_PASS."@".DB_HOST."/".DB_NAME."<p>");
          $this->db_link = mysql_connect(DB_HOST,DB_USER,DB_PASS);
          mysql_select_db(DB_NAME,$this->db_link);
		  mysql_query("SET NAMES tis620"); 
      }
	  
      Function isConnected() {
          If ($this->db_link) {
              Return true;
          } Else {
              Return false;
          }
      }
	  
      Function disconnect() {
          If ($this->db_link) {
              mysql_close($this->db_link);
			  //echo "close database";
			  Return true;
          } Else {
 	  		  //echo "no conection found can't close db";
              Return false;
          }
      }

    Function query_select($qry) {
          // echo("query=$qry");
        Return new DBResult($qry,mysql_query($qry));
      }

      Function query_affect($qry) {
          // echo("query=$qry");
          If ($this->isConnected()) {
              mysql_query($qry);
              //-- Modified by Pim : To be able to notice db error
              If (($affected_row = mysql_affected_rows($this->db_link)) == -1) {
                Echo "<i>".$qry."</i><br>";
                Echo "MySql #".mysql_errno().": ".mysql_error()."<br><br>";
              }
              return $affected_row;
              // -- End Modified
          } else {
              echo("<!-- not connected to database! -->");
            return false;
            // be careful!
            // false means not connected
            // 0 means not found or not affected
          }
      }      
}


Class DBConnectionUTF8 {
      Var $db_link;

      Function DBConnectionUTF8() {
          $this->connect();
      }

      Function connect() {
		  // echo("<P>db://".DB_USER.":".DB_PASS."@".DB_HOST."/".DB_NAME."<p>");
          $this->db_link = mysql_connect(DB_HOST,DB_USER,DB_PASS);
          mysql_select_db(DB_NAME,$this->db_link);		  
		  //mysql_query("SET NAMES tis620");
		  mysql_query("SET NAMES utf8"); 
      }	  

      Function isConnected() {
          If ($this->db_link) {
              Return true;
          } Else {
              Return false;
          }
      }
	  
      Function disconnect() {
          If ($this->db_link) {
              mysql_close($this->db_link);
			  //echo "close database";
			  Return true;
          } Else {
 	  		  //echo "no conection found can't close db";
              Return false;
          }
      }

    Function query_select($qry) {
          // echo("query=$qry");
        Return new DBResult($qry,mysql_query($qry));
      }

      Function query_affect($qry) {
          // echo("query=$qry");
          If ($this->isConnected()) {
              mysql_query($qry);
              //-- Modified by Pim : To be able to notice db error
              If (($affected_row = mysql_affected_rows($this->db_link)) == -1) {
                Echo "<i>".$qry."</i><br>";
                Echo "MySql #".mysql_errno().": ".mysql_error()."<br><br>";
              }
              return $affected_row;
              // -- End Modified
          } else {
              echo("<!-- not connected to database! -->");
            return false;
            // be careful!
            // false means not connected
            // 0 means not found or not affected
          }
      }      
}

/*!
	@class DB
	@abstract
		object = new DB()
		Super class of any object which need to connect to database
	@discussion
		EXAMPLE
			$objDB = new DB();
			$objDBResult = $objDB->query("SELECT * FROM myTable");
			while ($row = $objDBResult->nextRow()) {
				$column1 = $row->column1
				$column2 = $row->column2
			}
		NOTES
			Meant to be used within a class
		SEE ALSO
			DBResult
*/

Class DB {

    Var $db_connection;

    Function DB() {
    }

    Function getConnection() {
        If (!$this->db_connection) {
            $this->db_connection = new DBConnection();
        }
		if (!$this->db_connection->isConnected()) {
			echo "<BR>not connected<BR>";
		}
		
	   Return $this->db_connection;
    }
    
    Function getConnectionUTF8() {
        If (!$this->db_connection) {
            $this->db_connection = new DBConnectionUTF8();
        }
		if (!$this->db_connection->isConnected()) {
			echo "<BR>not connected<BR>";
		}
		
        Return $this->db_connection;
    }	
	
	Function unsetConnection(){
		/*
		If ($this->db_connection) {
			$this->db_connection->disconnect();
            unset($this->db_connection);
			//mysql_close();
        }
		*/
	}
	
	
    Function query($qry) {
		// echo "<p>".$qry;
        $pos = strpos(ltrim($qry),"SELECT");
          If (($pos !== false) && ($pos == 0)) {
              Return $this->db_connection->query_select($qry);
        } Else {
            Return $this->db_connection->query_affect($qry);
        }
    }
    
	// To avoid string problem with Single Quote
    Function privInsertAp($String)
	{
		If ( $String != "" )
		{
			If (StrStr($String, "'"))
			{ Return Str_Replace("'", "''", $String ); } //Str_Replace("'", "\'", $String ); }
			Else
			{ Return $String; }
		}
    }

    // Functions for convert Data Type for SQL command
    Function convStr4SQL($String)
    {
        $String = Trim($String);
        If ( $String == "" )    // Empty string
        { Return "''"; } //Return "''"; //
        Else                // Not empty, so have to insert prefix and suffix for String data type
        { 
			//Return "'" . stripslashes($this->privInsertAp($String))."'";
			Return "'" . addSlashes(HtmlSpecialChars(StripSlashes(Trim($String))))."'";
			//Return "'" . addslashes($String)."'";
		 }
    }

    Function convStr4SQL2($String)
    {
        $String = Trim($String);
        If ( $String == "" )    // Empty string
        { Return "''"; } //
        Else                // Not empty, so have to insert prefix and suffix for String data type
        { 
			Return "'" . $this->privInsertAp($String)."'";
			//Return "'" . addslashes($String)."'";
		 }
    }

	function convDateOrNow4SQL($date) { 
		// argument is already SQL friendly: YYYY-MM-DD [HH[:MM[:SS]]
		if (eregi("now",$date)) {
			return "NOW()";
		} else {
			return "'".$date."'";
		}
    }

    Function convNum4SQL($Number)
    {
        If ( Trim($Number) == "" )    // Empty
        { Return 0; }
        Else
        { Return $Number; }
    }
    
    Function convDateTime4SQL($DateTime)    //Argument must be format "H:i:s m-d-Y"
    {
        If ( $DateTime == "" )
        { Return "Null"; }
        Else
        { Return "'" .Date("Y-m-d H:i:s", MkTime($DateTime) ) ."'"; }
    }

    Function convDate4SQL($DateTime)    //Argument must be format "H:i:s m-d-Y"
    {
        if ( $DateTime == "" )
        { return "Null"; }
        else
        { Return "'" .Date("Y-m-d", MkTime($DateTime) ) ."'"; }
    }

    Function convTime4SQL($DateTime)    //Argument must be format "H:i:s m-d-Y"
    {
        If ( $DateTime == "" )
        { Return "Null"; }
        Else
        { Return "'" .Date("H:i:s", MkTime($DateTime) ) ."'"; }
    }

    Function convDb2DateTime($DateTime)
    {
        If ( $DateTime == "" )
        { Return Null; }
        Else
        {
            List($strDate, $strTime) = Split(" ", $DateTime);
            $arrDate = Split("[/.-]", $strDate);
            $arrTime = Split("[/:]", $strTime);
            $strTimeStamp  = $arrTime[0] ."," .$arrTime[1] ."," .$arrTime[2] .",";
            $strTimeStamp .= $arrDate[1] ."," .$arrDate[2] ."," .$arrDate[0];
            Return MkTime($strTimeStamp); // Format "H:i:s m-d-Y"
										  // For example : 
										  // strDateTimeShown = StrFTime("%A , %B %d %Y %H:%M:%S", $this->lastUpdateProfile)
        }
    }
	
    Function SplitDateTime($DateTime)
    {
        If ( $DateTime == "" )
        { Return Null; }
        Else
        {
            List($strDate, $strTime) = Split(" ", $DateTime);
            $arrDate = Split("[/.-]", $strDate);
            $arrTime = Split("[/:]", $strTime);
			$arrDateTime["hour"] = $arrTime[0];
			$arrDateTime["minute"] = $arrTime[1];
			$arrDateTime["second"] = $arrTime[2];
			$arrDateTime["month"] = $arrDate[1];
			$arrDateTime["day"] = $arrDate[2];
			$arrDateTime["year"] = $arrDate[0];
            Return $arrDateTime; // 
        }
    }
	
	function concatSQL($begin, $end, $separator = 'AND', $instruction = '') {
		$separator = ' '.trim($separator).' ';
		if (!empty($instruction)) {
			$instruction .= ' ';
		}
		if (empty($begin)) {
			if (empty($end)) {
				return false;
			} else {
				return $instruction.$end;
			}
		} else if (empty($end)) {
			return $instruction.$begin;
		} else {
			return $begin.$separator.$end;
		}
	}

	function stripHTML(&$str) {
		$to_removed_array = array (
			"'<html>'si",
			"'</html>'si",
			"'<body[^>]*>'si",
			"'</body>'si",
			"'<head[^>]*>.*?</head>'si",
			"'<style[^>]*>.*?</style>'si",
			"'<script[^>]*>.*?</script>'si",
			"'<object[^>]*>.*?</object>'si",
			"'<embed[^>]*>.*?</embed>'si",
			"'<applet[^>]*>.*?</applet>'si",
			"'<mocha[^>]*>.*?</mocha>'si");
		$str = preg_replace($to_removed_array, '', $str);
		$str = ereg_replace("</?[^>]*>",'',$str);
	}
}
?>
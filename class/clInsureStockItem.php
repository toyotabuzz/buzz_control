<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_stock_item table

*********************************************************/
 
class InsureStockItem extends DB{

	var $TABLE="t_insure_stock_item";

	var $m_insure_stock_item_id;
	function get_insure_stock_item_id() { return $this->m_insure_stock_item_id; }
	function set_insure_stock_item_id($data) { $this->m_insure_stock_item_id = $data; }
	
	var $m_stock_type;
	function get_stock_type() { return $this->m_stock_type; }
	function set_stock_type($data) { $this->m_stock_type = $data; }
	
	var $m_from_company;
	function get_from_company() { return $this->m_from_company; }
	function set_from_company($data) { $this->m_from_company = $data; }
	
	var $m_from_department;
	function get_from_department() { return $this->m_from_department; }
	function set_from_department($data) { $this->m_from_department = $data; }
	
	var $m_from_officer;
	function get_from_officer() { return $this->m_from_officer; }
	function set_from_officer($data) { $this->m_from_officer = $data; }
	
	var $m_from_date;
	function get_from_date() { return $this->m_from_date; }
	function set_from_date($data) { $this->m_from_date = $data; }
	
	var $m_from_remark;
	function get_from_remark() { return $this->m_from_remark; }
	function set_from_remark($data) { $this->m_from_remark = $data; }
	
	var $m_to_company;
	function get_to_company() { return $this->m_to_company; }
	function set_to_company($data) { $this->m_to_company = $data; }
	
	var $m_to_department;
	function get_to_department() { return $this->m_to_department; }
	function set_to_department($data) { $this->m_to_department = $data; }
	
	var $m_to_officer;
	function get_to_officer() { return $this->m_to_officer; }
	function set_to_officer($data) { $this->m_to_officer = $data; }
	
	var $m_to_check;
	function get_to_check() { return $this->m_to_check; }
	function set_to_check($data) { $this->m_to_check = $data; }
	
	var $m_to_date;
	function get_to_date() { return $this->m_to_date; }
	function set_to_date($data) { $this->m_to_date = $data; }	
	
	var $m_to_remark;
	function get_to_remark() { return $this->m_to_remark; }
	function set_to_remark($data) { $this->m_to_remark = $data; }

	function InsureStockItem($objData=NULL) {
        If ($objData->insure_stock_item_id !=""  ) {
			$this->set_insure_stock_item_id($objData->insure_stock_item_id);
			$this->set_stock_type($objData->stock_type);
			$this->set_from_company($objData->from_company);
			$this->set_from_department($objData->from_department);
			$this->set_from_officer($objData->from_officer);
			$this->set_from_date($objData->from_date);
			$this->set_from_remark($objData->from_remark);
			$this->set_to_company($objData->to_company);
			$this->set_to_department($objData->to_department);
			$this->set_to_officer($objData->to_officer);
			$this->set_to_check($objData->to_check);
			$this->set_to_date($objData->to_date);
			$this->set_to_remark($objData->to_remark);

        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_stock_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_stock_item_id =".$this->m_insure_stock_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureStockItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT O.*, S.broker_id, S.insure_company_id FROM ".$this->TABLE." O "
						." LEFT JOIN t_stock_insure S ON S.stock_insure_id =  O.stock_insure_id  "
						."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureStockItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(insure_stock_item_id) AS RSUM  FROM ".$this->TABLE."  "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}	

	function genNum($hNumType) {
		$strSql = "SELECT MAX(num)  acc_num_out FROM ".$this->TABLE."  WHERE num LIKE '".$hNumType.date("dmy")."/%' " ;

		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				
				$gen = substr($row->acc_num_out,strlen($row->acc_num_out)-4,4);
				$gen = $gen+1;
				if($gen < 10) $gen= "000".$gen;
				if($gen >= 10 and $gen < 100 ) $gen= "00".$gen;
				if($gen >= 100 and $gen < 1000 ) $gen= "0".$gen;
				
				
				$num_gen = $hNumType.date("dmy")."/".$gen;
                $result->freeResult();
				return $num_gen;
            }
        }else{
			return $hNumType.date("ymd")."/0001";
		}
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( stock_type , from_company , from_department , from_officer, from_date, from_remark, to_company, to_department  ) " ." VALUES ( "
		." '".$this->m_stock_type."' , "
		." '".$this->m_from_company."' , "
		." '".$this->m_from_department."' , "
		." '".$this->m_from_officer."' , "
		." '".$this->m_from_date."' , "
		." '".$this->m_from_remark."' , "
		." '".$this->m_to_company."' , "
		." '".$this->m_to_department."'  "
		." ) ";


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_stock_item_id = mysql_insert_id();
            return $this->m_insure_stock_item_id;
        } else {
			return false;
	    }
	}

	function updateRecieve(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." to_officer = '".$this->m_to_officer."' "
		." , to_check = '".$this->m_to_check."' "
		." , to_date = '".$this->m_to_date."' "
		." , to_remark = '".$this->m_to_remark."' "
		." WHERE insure_stock_item_id = ".$this->m_insure_stock_item_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_stock_item_id=".$this->m_insure_stock_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		
       $strSql = " DELETE FROM t_insure_stock_item_detail "
                . " WHERE insure_stock_item_id=".$this->m_insure_stock_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureStockItemList extends DataList {
	var $TABLE = "t_insure_stock_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_stock_item_id) as rowCount FROM ".$this->TABLE
			." O  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT O.* FROM ".$this->TABLE." O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureStockItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_insure_stock_item_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_stock_item_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}
}
<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_registry_price table

*********************************************************/
 
class RegistryPrice extends DB{

	var $TABLE="t_registry_price";

	var $mRegistryPriceId;
	function getRegistryPriceId() { return $this->mRegistryPriceId; }
	function setRegistryPriceId($data) { $this->mRegistryPriceId = $data; }
	
	var $mCarModelId;
	function getCarModelId() { return htmlspecialchars($this->mCarModelId); }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mCC;
	function getCC() { return htmlspecialchars($this->mCC); }
	function setCC($data) { $this->mCC = $data; }	
	
	var $mPrice1;
	function getPrice1() { return htmlspecialchars($this->mPrice1); }
	function setPrice1($data) { $this->mPrice1 = $data; }
	
	var $mPrice2;
	function getPrice2() { return htmlspecialchars($this->mPrice2); }
	function setPrice2($data) { $this->mPrice2 = $data; }
	
	var $mPrice3;
	function getPrice3() { return htmlspecialchars($this->mPrice3); }
	function setPrice3($data) { $this->mPrice3 = $data; }
	
	var $mPrice4;
	function getPrice4() { return htmlspecialchars($this->mPrice4); }
	function setPrice4($data) { $this->mPrice4 = $data; }
	
	function RegistryPrice($objData=NULL) {
        If ($objData->registry_price_id !="") {
            $this->setRegistryPriceId($objData->registry_price_id);
			$this->setCarModelId($objData->car_model_id);
			$this->setCC($objData->cc);
			$this->setPrice1($objData->price1);
			$this->setPrice2($objData->price2);
			$this->setPrice3($objData->price3);
			$this->setPrice4($objData->price4);
        }
    }

	function init(){
		$this->setCarModelId(stripslashes($this->mCarModelId));
	}
		
	function load() {

		if ($this->mRegistryPriceId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE registry_price_id =".$this->mRegistryPriceId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryPrice($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mRegistryPriceId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE registry_price_id =".$this->mRegistryPriceId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryPrice($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->RegistryPrice($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( car_model_id,cc, price1,price2,price3,price4 ) "
						." VALUES ( '".$this->mCarModelId."' , '".$this->mCC."' , '".$this->mPrice1."' , '".$this->mPrice2."' , '".$this->mPrice3."' , '".$this->mPrice4."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mRegistryPriceId = mysql_insert_id();
            return $this->mRegistryPriceId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET car_model_id = '".$this->mCarModelId."' , cc = '".$this->mCC."' , price1 = '".$this->mPrice1."'  , price2='".$this->mPrice2."'  , price3 = '".$this->mPrice3."'  , price4='".$this->mPrice4."' "
						." WHERE  registry_price_id = ".$this->mRegistryPriceId." ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE registry_price_id=".$this->mRegistryPriceId."   ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCarModelId == "") $asrErrReturn["car_model_id"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class RegistryPriceList extends DataList {
	var $TABLE = "t_registry_price";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT registry_price_id) as rowCount FROM ".$this->TABLE
			."  LEFT JOIN t_car_model ON t_registry_price.car_model_id = t_car_model.car_model_id "
			."   ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_car_model ON P.car_model_id = t_car_model.car_model_id "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new RegistryPrice($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getRegistryPriceId()."\"");
			if (($defaultId != null) && ($objItem->getRegistryPriceId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCarModelId()."</option>");
		}
		echo("</select>");
	}			
	
}
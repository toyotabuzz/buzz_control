<?
/*********************************************************
		Class :				Customer
		Last update :		9 Jan 07
		Description:		Class manage t_customer  table
*********************************************************/
 
class Customer extends DB{

	var $TABLE="t_customer";
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }

	var $mTypeId;
	function getTypeId() { return htmlspecialchars($this->mTypeId); }
	function setTypeId($data) { $this->mTypeId = $data; }

	function getTypeIdDetail(){
		if($this->mTypeId == "A") return "A-CARD";
		if($this->mTypeId == "B") return "Booking";
		if($this->mTypeId == "C") return "C-CARD";
	}	
	
	var $mACard;
	function getACard() { return htmlspecialchars($this->mACard); }
	function setACard($data) { $this->mACard = $data; }
	
	var $mBCard;
	function getBCard() { return htmlspecialchars($this->mBCard); }
	function setBCard($data) { $this->mBCard = $data; }	
	
	var $mCCard;
	function getCCard() { return htmlspecialchars($this->mCCard); }
	function setCCard($data) { $this->mCCard = $data; }	
	
	var $mICard;
	function getICard() { return htmlspecialchars($this->mICard); }
	function setICard($data) { $this->mICard = $data; }		
	
	var $mBondCard;
	function getBondCard() { return htmlspecialchars($this->mBondCard); }
	function setBondCard($data) { $this->mBondCard = $data; }
	
	var $mGradeId;
	function getGradeId() { return htmlspecialchars($this->mGradeId); }
	function setGradeId($data) { $this->mGradeId = $data; }
	
	var $mEventId;
	function getEventId() { return htmlspecialchars($this->mEventId); }
	function setEventId($data) { $this->mEventId = $data; }	

	var $mGroupId;
	function getGroupId() { return htmlspecialchars($this->mGroupId); }
	function setGroupId($data) { $this->mGroupId = $data; }

	var $mFollowUp;
	function getFollowUp() { return htmlspecialchars($this->mFollowUp); }
	function setFollowUp($data) { $this->mFollowUp = $data; }
		
	var $mInformationDate;
	function getInformationDate() { return htmlspecialchars($this->mInformationDate); }
	function setInformationDate($data) { $this->mInformationDate = $data; }

	var $mSaleId;
	function getSaleId() { return htmlspecialchars($this->mSaleId); }
	function setSaleId($data) { $this->mSaleId = $data; }

	var $mBirthday;
	function getBirthday() { return htmlspecialchars($this->mBirthday); }
	function setBirthday($data) { $this->mBirthday = $data; }

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	function getTitleDetail(){
		if($this->mTitle == "" ){
		 	return "�س";
		 }else{ 
		 	return $this->mTitle;  
		 }
	}

	var $mFirstname;
	function getFirstname() { return htmlspecialchars($this->mFirstname); }
	function setFirstname($data) { $this->mFirstname = $data; }
	
	var $mLastname;
	function getlastname() { return htmlspecialchars($this->mLastname); }
	function setlastname($data) { $this->mLastname = $data; }
	
	var $mCustomerTitleId;
	function getCustomerTitleId() { return htmlspecialchars($this->mCustomerTitleId); }
	function setCustomerTitleId($data) { $this->mCustomerTitleId = $data; }	
	
	var $mCustomerTitleDetail;
	function getCustomerTitleDetail() { return htmlspecialchars($this->mCustomerTitleDetail); }
	function setCustomerTitleDetail($data) { $this->mCustomerTitleDetail = $data; }	

	//�׹��� �س�ء�ó� ������
	function getCustomerTitleIdDetail(){
		if($this->mCustomerTitleId == "0" ){
		 	return "�س";
		 }else{ 
		 	return htmlspecialchars($this->mCustomerTitleDetail);  
		 }
	}		
	
	//return �ҧ �ҧ��� ��� ������
	function getCustomerTitleIdDetail01(){
		if($this->mCustomerTitleId == "0" ){
			if($this->mTitle == "" ){
			 	return "�س";
			 }else{ 
			 	if($this->mTitle == "����к�"){
			 		return "";  
				}else{
					return $this->mTitle;  
				}
			 }
		 }else{ 
		 	return htmlspecialchars($this->mCustomerTitleDetail);  
		 }
	}		
	
	//return �ҧ �ҧ��� ��� ������
	function getTitleLabel(){
		if($this->mCustomerTitleId == "0" ){
			switch ($this->mTitle){

				case "����к�":
					return "";
					break;
				case "Mr":
					return "Mr";
					break;
				case "Miss":
					return "Miss";
					break;
				case "Mrs":
					return "Mrs";
					break;
				case "���.":
					return "���.";
					break;
				case "˨�.":
					return "˨�.";
					break;
				case "���.":
					return "���.";
					break;					
				case "���":
					return "���.";
					break;
				case "˨�":
					return "˨�.";
					break;
				case "���":
					return "���.";
					break;
				default:
					return "�س";

			}
		 }else{ 
		 	return htmlspecialchars($this->mCustomerTitleDetail);  
		 }
	}			
	
	var $mEmail;
	function getEmail() { return htmlspecialchars($this->mEmail); }
	function setEmail($data) { $this->mEmail = $data; }
	
	var $mIDCard;
	function getIDCard() { return htmlspecialchars($this->mIDCard); }
	function setIDCard($data) { $this->mIDCard = $data; }
	
	function getAddressNew(){
		if($this->mHomeNo != "" or $this->mCompanyName != "" or $this->mBuilding != "" or $this->mMooNo != "" or $this->mMooban != "" or $this->mSoi != "" or $this->mRoad != ""){
		$hAddress = trim($this->getHomeNoDetail()." ".$this->getCompanyName());
		$hAddress = trim($hAddress." ".$this->getBuilding());
		$hAddress = trim($hAddress." ".$this->getMooNoDetail());
		$hAddress = trim($hAddress." ".$this->getMoobanDetail());
		$hAddress = trim($hAddress." ".$this->getSoiDetail());
		$hAddress = trim($hAddress." ".$this->getRoadDetail());
		
		}else{
		$hAddress = trim($this->getAddress()."  ".$this->getAddress1());
		}
		return $hAddress;
	}	
	
	
	function getAddressAll(){
		if($this->mHomeNo != "" or $this->mCompanyName != "" or $this->mBuilding != "" or $this->mMooNo != "" or $this->mMooban != "" or $this->mSoi != "" or $this->mRoad != ""){
		$hAddress = trim($this->getHomeNoDetail()." ".$this->getCompanyName());
		$hAddress = trim($hAddress." ".$this->getBuilding());
		$hAddress = trim($hAddress." ".$this->getMooNoDetail());
		$hAddress = trim($hAddress." ".$this->getMoobanDetail());
		$hAddress = trim($hAddress." ".$this->getSoiDetail());
		$hAddress = trim($hAddress." ".$this->getRoadDetail());
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon." ࢵ ".$this->mAmphur." ".$this->mProvince." ".$this->mZip;
		}else{
		$hAddress=  $hAddress." �.".$this->mTumbon." �.".$this->mAmphur." ".$this->mProvince." ".$this->mZip;
		}
		
		}else{
		$hAddress = trim($this->getAddress()."  ".$this->getAddress1());
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon." ࢵ ".$this->mAmphur." ".$this->mProvince." ".$this->mZip;
		}else{
		$hAddress=  $hAddress." �.".$this->mTumbon." �.".$this->mAmphur." ".$this->mProvince." ".$this->mZip;
		}
		}
		return $hAddress;
	}
	
	
	function getAddressAll01(){
		if($this->mHomeNo01 != "" or $this->mCompanyName01 != "" or $this->mBuilding01 != "" or $this->mMooNo01 != "" or $this->mMooban01 != "" or $this->mSoi01 != "" or $this->mRoad01 != ""){
		if($this->mHomeNo01 != "") $hAddress = "�Ţ��� ".$this->mHomeNo01;
		if($this->mCompanyName01 != "") $hAddress .= " ".$this->mCompanyName01;
		if($this->mBuilding01 != "") $hAddress .= " ".$this->mBuilding01;
		if($this->mMooNo01 != "") $hAddress .= " ������ ".$this->mMooNo01;
		if($this->mMooban01 != "") $hAddress .= " �.".$this->mMooban01;
		if($this->mSoi01 != "") $hAddress .= " �.".$this->mSoi01;
		if($this->mRoad01 != "") $hAddress .= " �.".$this->mRoad01;
		
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon01." ࢵ ".$this->mAmphur01." ".$this->mProvince01." ".$this->mZip01;
		}else{
		$hAddress=  $hAddress." �. ".$this->mTumbon01." �.".$this->mAmphur01." ".$this->mProvince01." ".$this->mZip;
		}
		
		}else{
		$hAddress = trim($this->getAddress01()."  ".$this->getAddress011());
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon01." ࢵ ".$this->mAmphur01." ".$this->mProvince01." ".$this->mZip01;
		}else{
		$hAddress=  $hAddress." �. ".$this->mTumbon01." �.".$this->mAmphur01." ".$this->mProvince01." ".$this->mZip01;
		}
		}
		return $hAddress;
	}
	
	function getAddressAll02(){
		if($this->mHomeNo02 != "" or $this->mCompanyName02 != "" or $this->mBuilding02 != "" or $this->mMooNo02 != "" or $this->mMooban02 != "" or $this->mSoi02 != "" or $this->mRoad02 != ""){
		if($this->mHomeNo02 != "") $hAddress = "�Ţ��� ".$this->mHomeNo02;
		if($this->mCompanyName02 != "") $hAddress .= " ".$this->mCompanyName02;
		if($this->mBuilding02 != "") $hAddress .= " ".$this->mBuilding02;
		if($this->mMooNo02 != "") $hAddress .= " ������ ".$this->mMooNo02;
		if($this->mMooban02 != "") $hAddress .= " �.".$this->mMooban02;
		if($this->mSoi02 != "") $hAddress .= " �.".$this->mSoi02;
		if($this->mRoad02 != "") $hAddress .= " �.".$this->mRoad02;
		
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon02." ࢵ ".$this->mAmphur02." ".$this->mProvince02." ".$this->mZip02;
		}else{
		$hAddress=  $hAddress." �.".$this->mTumbon02." �.".$this->mAmphur02." ".$this->mProvince02." ".$this->mZip02;
		}
		
		}else{
		$hAddress = trim($this->getAddress02()."  ".$this->getAddress021());
		if($this->mProvince == "��ا෾��ҹ��"){
		$hAddress=  $hAddress." �ǧ ".$this->mTumbon02." ࢵ ".$this->mAmphur02." ".$this->mProvince02." ".$this->mZip02;
		}else{
		$hAddress=  $hAddress." �.".$this->mTumbon02." �.".$this->mAmphur02." ".$this->mProvince02." ".$this->mZip02;
		}
		}
		return $hAddress;
	}
	
	function getAddressAll03(){
		if($this->mHomeNo03 != "" or $this->mCompanyName03 != "" or $this->mBuilding03 != "" or $this->mMooNo03 != "" or $this->mMooban03 != "" or $this->mSoi03 != "" or $this->mRoad03 != ""){
			if($this->mHomeNo03 != "") $hAddress = "�Ţ��� ".$this->mHomeNo03;
			if($this->mCompanyName03 != "") $hAddress .= " ".$this->mCompanyName03;
			if($this->mBuilding03 != "") $hAddress .= " ".$this->mBuilding03;
			if($this->mMooNo03 != "") $hAddress .= " ������ ".$this->mMooNo03;
			if($this->mMooban03 != "") $hAddress .= " �.".$this->mMooban03;
			if($this->mSoi03 != "") $hAddress .= " �.".$this->mSoi03;
			if($this->mRoad03 != "") $hAddress .= " �.".$this->mRoad03;
			
			if($this->mProvince == "��ا෾��ҹ��"){
			$hAddress=  $hAddress." �ǧ ".$this->mTumbon03." ࢵ ".$this->mAmphur03." ".$this->mProvince03." ".$this->mZip03;
			}else{
			$hAddress=  $hAddress." �Ӻ� ".$this->mTumbon03." ����� ".$this->mAmphur03." ".$this->mProvince03." ".$this->mZip03;
			}
		
		}else{
			$hAddress = trim($this->getAddress03()."  ".$this->getAddress031());
			if($this->mProvince == "��ا෾��ҹ��"){
			$hAddress=  $hAddress." �ǧ ".$this->mTumbon03." ࢵ ".$this->mAmphur03." ".$this->mProvince03." ".$this->mZip03;
			}else{
			$hAddress=  $hAddress." �Ӻ� ".$this->mTumbon03." ����� ".$this->mAmphur03." ".$this->mProvince03." ".$this->mZip03;
			}
		}
		return $hAddress;
	}
	
	function getAddressLabel(){
		if($this->mHomeNo != "" or $this->mCompanyName != "" or $this->mBuilding != "" or $this->mMooNo != "" or $this->mMooban != "" or $this->mSoi != "" or $this->mRoad != ""){
			if(trim($this->mHomeNo) != "") $hAddress = "�Ţ��� ".$this->mHomeNo;
			if(trim($this->mCompanyName) != "") $hAddress .= " ".$this->mCompanyName;
			if(trim($this->mBuilding) != "") $hAddress .= " ".$this->mBuilding;
			if(trim($this->mMooNo) != "") $hAddress .= " ������ ".$this->mMooNo;
			if(trim($this->mMooban) != "") $hAddress .= " �.".$this->mMooban;
			if(trim($this->mSoi) != "") $hAddress .= " �.".$this->mSoi;
			if(trim($this->mRoad) != "") $hAddress .= " �.".$this->mRoad;
			
		
		}else{
			$hAddress = trim($this->getAddress()."  ".$this->getAddress1());
		}
		return $hAddress;
	}		
	
	function getAddressLabel01(){
		if($this->mHomeNo01 != "" or $this->mCompanyName01 != "" or $this->mBuilding01 != "" or $this->mMooNo01 != "" or $this->mMooban01 != "" or $this->mSoi01 != "" or $this->mRoad01 != ""){
			if(trim($this->mHomeNo01) != "") $hAddress = "�Ţ��� ".$this->mHomeNo01;
			if(trim($this->mCompanyName01) != "") $hAddress .= " ".$this->mCompanyName01;
			if(trim($this->mBuilding01) != "") $hAddress .= " ".$this->mBuilding01;
			if(trim($this->mMooNo01) != "") $hAddress .= " ������ ".$this->mMooNo01;
			if(trim($this->mMooban01) != "") $hAddress .= " �.".$this->mMooban01;
			if(trim($this->mSoi01) != "") $hAddress .= " �.".$this->mSoi01;
			if(trim($this->mRoad01) != "") $hAddress .= " �.".$this->mRoad01;
			
		
		}else{
			$hAddress = trim($this->getAddress01()."  ".$this->getAddress011());
		}
		return $hAddress;
	}	
	
	function getAddressLabel02(){
		if($this->mHomeNo02 != "" or $this->mCompanyName02 != "" or $this->mBuilding02 != "" or $this->mMooNo02 != "" or $this->mMooban02 != "" or $this->mSoi02 != "" or $this->mRoad02 != ""){

			if(trim($this->mHomeNo02) != "") $hAddress = "�Ţ��� ".$this->mHomeNo02;
			if(trim($this->mCompanyName02) != "") $hAddress .= " ".$this->mCompanyName02;
			if(trim($this->mBuilding02) != "") $hAddress .= " ".$this->mBuilding02;
			if(trim($this->mMooNo02) != "") $hAddress .= " ������ ".$this->mMooNo02;
			if(trim($this->mMooban02) != "") $hAddress .= " �.".$this->mMooban02;
			if(trim($this->mSoi02) != "") $hAddress .= " �.".$this->mSoi02;
			if(trim($this->mRoad02) != "") $hAddress .= " �.".$this->mRoad02;
		
		}else{
			$hAddress = trim($this->getAddress02()."  ".$this->getAddress021());
		}
		return $hAddress;
	}			
	
	
	function getAddressLabel03(){
		if($this->mHomeNo03 != "" or $this->mCompanyName03 != "" or $this->mBuilding03 != "" or $this->mMooNo03 != "" or $this->mMooban03 != "" or $this->mSoi03 != "" or $this->mRoad03 != ""){
			if(trim($this->mHomeNo03) != "") $hAddress = "�Ţ��� ".$this->mHomeNo03;
			if(trim($this->mCompanyName03) != "") $hAddress .= " ".$this->mCompanyName03;
			if(trim($this->mBuilding03) != "") $hAddress .= " ".$this->mBuilding03;
			if(trim($this->mMooNo03) != "") $hAddress .= " ������ ".$this->mMooNo03;
			if(trim($this->mMooban03) != "") $hAddress .= " �.".$this->mMooban03;
			if(trim($this->mSoi03) != "") $hAddress .= " �.".$this->mSoi03;
			if(trim($this->mRoad03) != "") $hAddress .= " �.".$this->mRoad03;
		}else{
			$hAddress = trim($this->getAddress03()."  ".$this->getAddress031());
		}
		return $hAddress;
	}	
	
	var $mAddress;
	function getAddress() { return htmlspecialchars($this->mAddress); }
	function setAddress($data) { $this->mAddress = $data; }

	var $mAddress1;
	function getAddress1() { return htmlspecialchars($this->mAddress1); }
	function setAddress1($data) { $this->mAddress1 = $data; }
	
	var $mHomeNo;
	function getHomeNo() { return htmlspecialchars($this->mHomeNo); }
	function getHomeNoDetail() { 
		if($this->mHomeNo != ""){
			return "�Ţ��� ".htmlspecialchars($this->mHomeNo); 
		}else{
			return "";
		}
	}	
	function setHomeNo($data) { $this->mHomeNo = $data; }

	var $mCompanyName;
	function getCompanyName() { return htmlspecialchars($this->mCompanyName); }
	function setCompanyName($data) { $this->mCompanyName = $data; }
	
	var $mBuilding;
	function getBuilding() { return htmlspecialchars($this->mBuilding); }
	function setBuilding($data) { $this->mBuilding = $data; }

	var $mMooNo;
	function getMooNo() { return htmlspecialchars($this->mMooNo); }
	function getMooNoDetail() { 
		if($this->mMooNo != ""){
			return "������ ".htmlspecialchars($this->mMooNo); 
		}else{
			return "";
		}
	}
	function setMooNo($data) { $this->mMooNo = $data; }

	var $mMooban;
	function getMooban() { return htmlspecialchars($this->mMooban); }
	function getMoobanDetail() { 
		if($this->mMooban != ""){
			return "�.".htmlspecialchars($this->mMooban); 
		}else{
			return "";
		}
	}
	function setMooban($data) { $this->mMooban = $data; }

	var $mSoi;
	function getSoi() { return htmlspecialchars($this->mSoi); }
	function getSoiDetail() { 
		if($this->mSoi != ""){
			return "�.".htmlspecialchars($this->mSoi); 
		}else{
			return "";
		}
	}	
	function setSoi($data) { $this->mSoi = $data; }

	var $mRoad;
	function getRoad() { return htmlspecialchars($this->mRoad); }
	function getRoadDetail() { 
		if($this->mRoad != ""){
			return "�.".htmlspecialchars($this->mRoad); 
		}else{
			return "";
		}
	}	
	function setRoad($data) { $this->mRoad = $data; }
	
	var $mTumbon;
	function getTumbon() { return htmlspecialchars($this->mTumbon); }
	function setTumbon($data) { $this->mTumbon = $data; }

	var $mAmphur;
	function getAmphur() { return htmlspecialchars($this->mAmphur); }
	function setAmphur($data) { $this->mAmphur = $data; }

	var $mProvince;
	function getProvince() { return htmlspecialchars($this->mProvince); }
	function setProvince($data) { $this->mProvince = $data; }

	var $mTumbonCode;
	function getTumbonCode() { return htmlspecialchars($this->mTumbonCode); }
	function setTumbonCode($data) { $this->mTumbonCode = $data; }

	var $mAmphurCode;
	function getAmphurCode() { return htmlspecialchars($this->mAmphurCode); }
	function setAmphurCode($data) { $this->mAmphurCode = $data; }

	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }	
	
	var $mZipCode;
	function getZipCode() { return htmlspecialchars($this->mZipCode); }
	function setZipCode($data) { $this->mZipCode = $data; }	
	
	var $mZip;
	function getZip() { return htmlspecialchars($this->mZip); }
	function setZip($data) { $this->mZip = $data; }

	var $mHomeTel;
	function getHomeTel() { return htmlspecialchars($this->mHomeTel); }
	function setHomeTel($data) { $this->mHomeTel = $data; }

	var $mMobile;
	function getMobile() { return $this->mMobile; }
	function setMobile($data) { $this->mMobile = $data; }

	var $mOfficeTel;
	function getOfficeTel() { return htmlspecialchars($this->mOfficeTel); }
	function setOfficeTel($data) { $this->mOfficeTel = $data; }

	var $mFax;
	function getFax() { return htmlspecialchars($this->mFax); }
	function setFax($data) { $this->mFax = $data; }
	
	var $mContactName;
	function getContactName() { return htmlspecialchars($this->mContactName); }
	function setContactName($data) { $this->mContactName = $data; }

	var $mAddress01;
	function getAddress01() { return htmlspecialchars($this->mAddress01); }
	function setAddress01($data) { $this->mAddress01 = $data; }

	var $mAddress011;
	function getAddress011() { return htmlspecialchars($this->mAddress011); }
	function setAddress011($data) { $this->mAddress011 = $data; }
	
	var $mHomeNo01;
	function getHomeNo01() { return htmlspecialchars($this->mHomeNo01); }
	function setHomeNo01($data) { $this->mHomeNo01 = $data; }

	var $mCompanyName01;
	function getCompanyName01() { return htmlspecialchars($this->mCompanyName01); }
	function setCompanyName01($data) { $this->mCompanyName01 = $data; }
	
	var $mBuilding01;
	function getBuilding01() { return htmlspecialchars($this->mBuilding01); }
	function setBuilding01($data) { $this->mBuilding01 = $data; }

	var $mMooNo01;
	function getMooNo01() { return htmlspecialchars($this->mMooNo01); }
	function setMooNo01($data) { $this->mMooNo01 = $data; }

	var $mMooban01;
	function getMooban01() { return htmlspecialchars($this->mMooban01); }
	function setMooban01($data) { $this->mMooban01 = $data; }

	var $mSoi01;
	function getSoi01() { return htmlspecialchars($this->mSoi01); }
	function setSoi01($data) { $this->mSoi01 = $data; }

	var $mRoad01;
	function getRoad01() { return htmlspecialchars($this->mRoad01); }
	function setRoad01($data) { $this->mRoad01 = $data; }
	
	var $mTumbon01;
	function getTumbon01() { return htmlspecialchars($this->mTumbon01); }
	function setTumbon01($data) { $this->mTumbon01 = $data; }

	var $mAmphur01;
	function getAmphur01() { return htmlspecialchars($this->mAmphur01); }
	function setAmphur01($data) { $this->mAmphur01 = $data; }

	var $mProvince01;
	function getProvince01() { return htmlspecialchars($this->mProvince01); }
	function setProvince01($data) { $this->mProvince01 = $data; }

	var $mTumbonCode01;
	function getTumbonCode01() { return htmlspecialchars($this->mTumbonCode01); }
	function setTumbonCode01($data) { $this->mTumbonCode01 = $data; }

	var $mAmphurCode01;
	function getAmphurCode01() { return htmlspecialchars($this->mAmphurCode01); }
	function setAmphurCode01($data) { $this->mAmphurCode01 = $data; }

	var $mProvinceCode01;
	function getProvinceCode01() { return htmlspecialchars($this->mProvinceCode01); }
	function setProvinceCode01($data) { $this->mProvinceCode01 = $data; }	
	
	var $mZipCode01;
	function getZipCode01() { return htmlspecialchars($this->mZipCode01); }
	function setZipCode01($data) { $this->mZipCode01 = $data; }	
	
	var $mZip01;
	function getZip01() { return htmlspecialchars($this->mZip01); }
	function setZip01($data) { $this->mZip01 = $data; }	
	
	var $mTel01;
	function getTel01() { return htmlspecialchars($this->mTel01); }
	function setTel01($data) { $this->mTel01 = $data; }	
	
	var $mFax01;
	function getFax01() { return htmlspecialchars($this->mFax01); }
	function setFax01($data) { $this->mFax01 = $data; }	
	
	var $mCompany;
	function getCompany() { return htmlspecialchars($this->mCompany); }
	function setCompany($data) { $this->mCompany = $data; }	
	
	var $mCompanyType;
	function getCompanyType() { return htmlspecialchars($this->mCompanyType); }
	function setCompanyType($data) { $this->mCompanyType = $data; }	
	
	var $mCompanyPosition;
	function getCompanyPosition() { return htmlspecialchars($this->mCompanyPosition); }
	function setCompanyPosition($data) { $this->mCompanyPosition = $data; }	
	
	var $mCompanySection;
	function getCompanySection() { return htmlspecialchars($this->mCompanySection); }
	function setCompanySection($data) { $this->mCompanySection = $data; }	
	
	var $mCompanyDepartment;
	function getCompanyDepartment() { return htmlspecialchars($this->mCompanyDepartment); }
	function setCompanyDepartment($data) { $this->mCompanyDepartment = $data; }	
	
	var $mCompanyAge;
	function getCompanyAge() { return htmlspecialchars($this->mCompanyAge); }
	function setCompanyAge($data) { $this->mCompanyAge = $data; }	

	var $mCompanyAgeMonth;
	function getCompanyAgeMonth() { return htmlspecialchars($this->mCompanyAgeMonth); }
	function setCompanyAgeMonth($data) { $this->mCompanyAgeMonth = $data; }	
	
	var $mCompanySalary;
	function getCompanySalary() { return htmlspecialchars($this->mCompanySalary); }
	function setCompanySalary($data) { $this->mCompanySalary = $data; }	
	
	var $mCompanyOther;
	function getCompanyOther() { return htmlspecialchars($this->mCompanyOther); }
	function setCompanyOther($data) { $this->mCompanyOther = $data; }	
	
	var $mCompanyOtherRemark;
	function getCompanyOtherRemark() { return htmlspecialchars($this->mCompanyOtherRemark); }
	function setCompanyOtherRemark($data) { $this->mCompanyOtherRemark = $data; }	
	
	var $mMariageName;
	function getMariageName() { return htmlspecialchars($this->mMariageName); }
	function setMariageName($data) { $this->mMariageName = $data; }	
	
	var $mMariageCompany;
	function getMariageCompany() { return htmlspecialchars($this->mMariageCompany); }
	function setMariageCompany($data) { $this->mMariageCompany = $data; }	
	
	var $mMariagePosition;
	function getMariagePosition() { return htmlspecialchars($this->mMariagePosition); }
	function setMariagePosition($data) { $this->mMariagePosition = $data; }	
	
	var $mMariagePhone;
	function getMariagePhone() { return htmlspecialchars($this->mMariagePhone); }
	function setMariagePhone($data) { $this->mMariagePhone = $data; }	
	
	var $mMariagePhoneExtend;
	function getMariagePhoneExtend() { return htmlspecialchars($this->mMariagePhoneExtend); }
	function setMariagePhoneExtend($data) { $this->mMariagePhoneExtend = $data; }	
	
	var $mMariageSalary;
	function getMariageSalary() { return htmlspecialchars($this->mMariageSalary); }
	function setMariageSalary($data) { $this->mMariageSalary = $data; }	

	var $mRentalType;
	function getRentalType() { return htmlspecialchars($this->mRentalType); }
	function setRentalType($data) { $this->mRentalType = $data; }	

	var $mRentalYear;
	function getRentalYear() { return htmlspecialchars($this->mRentalYear); }
	function setRentalYear($data) { $this->mRentalYear = $data; }	

	var $mRentalWith;
	function getRentalWith() { return htmlspecialchars($this->mRentalWith); }
	function setRentalWith($data) { $this->mRentalWith = $data; }	
	
	var $mAddress02;
	function getAddress02() { return htmlspecialchars($this->mAddress02); }
	function setAddress02($data) { $this->mAddress02 = $data; }

	var $mAddress021;
	function getAddress021() { return htmlspecialchars($this->mAddress021); }
	function setAddress021($data) { $this->mAddress021 = $data; }
	
	var $mHomeNo02;
	function getHomeNo02() { return htmlspecialchars($this->mHomeNo02); }
	function setHomeNo02($data) { $this->mHomeNo02 = $data; }
	
	var $mCompanyName02;
	function getCompanyName02() { return htmlspecialchars($this->mCompanyName02); }
	function setCompanyName02($data) { $this->mCompanyName02 = $data; }

	var $mBuilding02;
	function getBuilding02() { return htmlspecialchars($this->mBuilding02); }
	function setBuilding02($data) { $this->mBuilding02 = $data; }

	var $mMooNo02;
	function getMooNo02() { return htmlspecialchars($this->mMooNo02); }
	function setMooNo02($data) { $this->mMooNo02 = $data; }

	var $mMooban02;
	function getMooban02() { return htmlspecialchars($this->mMooban02); }
	function setMooban02($data) { $this->mMooban02 = $data; }

	var $mSoi02;
	function getSoi02() { return htmlspecialchars($this->mSoi02); }
	function setSoi02($data) { $this->mSoi02 = $data; }

	var $mRoad02;
	function getRoad02() { return htmlspecialchars($this->mRoad02); }
	function setRoad02($data) { $this->mRoad02 = $data; }	
	
	var $mTumbon02;
	function getTumbon02() { return htmlspecialchars($this->mTumbon02); }
	function setTumbon02($data) { $this->mTumbon02 = $data; }

	var $mAmphur02;
	function getAmphur02() { return htmlspecialchars($this->mAmphur02); }
	function setAmphur02($data) { $this->mAmphur02 = $data; }

	var $mProvince02;
	function getProvince02() { return htmlspecialchars($this->mProvince02); }
	function setProvince02($data) { $this->mProvince02 = $data; }
	
	var $mTumbonCode02;
	function getTumbonCode02() { return htmlspecialchars($this->mTumbonCode02); }
	function setTumbonCode02($data) { $this->mTumbonCode02 = $data; }

	var $mAmphurCode02;
	function getAmphurCode02() { return htmlspecialchars($this->mAmphurCode02); }
	function setAmphurCode02($data) { $this->mAmphurCode02 = $data; }

	var $mProvinceCode02;
	function getProvinceCode02() { return htmlspecialchars($this->mProvinceCode02); }
	function setProvinceCode02($data) { $this->mProvinceCode02 = $data; }	

	var $mZipCode02;
	function getZipCode02() { return htmlspecialchars($this->mZipCode02); }
	function setZipCode02($data) { $this->mZipCode02 = $data; }	
	
	var $mZip02;
	function getZip02() { return htmlspecialchars($this->mZip02); }
	function setZip02($data) { $this->mZip02 = $data; }	
	
	var $mTel02;
	function getTel02() { return htmlspecialchars($this->mTel02); }
	function setTel02($data) { $this->mTel02 = $data; }	
	
	var $mFax02;
	function getFax02() { return htmlspecialchars($this->mFax02); }
	function setFax02($data) { $this->mFax02 = $data; }	
	
	var $mName03;
	function getName03() { return htmlspecialchars($this->mName03); }
	function setName03($data) { $this->mName03 = $data; }		
	
	var $mAddress03;
	function getAddress03() { return htmlspecialchars($this->mAddress03); }
	function setAddress03($data) { $this->mAddress03 = $data; }

	var $mAddress031;
	function getAddress031() { return htmlspecialchars($this->mAddress031); }
	function setAddress031($data) { $this->mAddress031 = $data; }
	
	var $mHomeNo03;
	function getHomeNo03() { return htmlspecialchars($this->mHomeNo03); }
	function setHomeNo03($data) { $this->mHomeNo03 = $data; }

	var $mCompanyName03;
	function getCompanyName03() { return htmlspecialchars($this->mCompanyName03); }
	function setCompanyName03($data) { $this->mCompanyName03 = $data; }
	
	var $mBuilding03;
	function getBuilding03() { return htmlspecialchars($this->mBuilding03); }
	function setBuilding03($data) { $this->mBuilding03 = $data; }

	var $mMooNo03;
	function getMooNo03() { return htmlspecialchars($this->mMooNo03); }
	function setMooNo03($data) { $this->mMooNo03 = $data; }

	var $mMooban03;
	function getMooban03() { return htmlspecialchars($this->mMooban03); }
	function setMooban03($data) { $this->mMooban03 = $data; }

	var $mSoi03;
	function getSoi03() { return htmlspecialchars($this->mSoi03); }
	function setSoi03($data) { $this->mSoi03 = $data; }

	var $mRoad03;
	function getRoad03() { return htmlspecialchars($this->mRoad03); }
	function setRoad03($data) { $this->mRoad03 = $data; }	
	
	var $mTumbon03;
	function getTumbon03() { return htmlspecialchars($this->mTumbon03); }
	function setTumbon03($data) { $this->mTumbon03 = $data; }

	var $mAmphur03;
	function getAmphur03() { return htmlspecialchars($this->mAmphur03); }
	function setAmphur03($data) { $this->mAmphur03 = $data; }

	var $mProvince03;
	function getProvince03() { return htmlspecialchars($this->mProvince03); }
	function setProvince03($data) { $this->mProvince03 = $data; }
	
	var $mTumbonCode03;
	function getTumbonCode03() { return htmlspecialchars($this->mTumbonCode03); }
	function setTumbonCode03($data) { $this->mTumbonCode03 = $data; }

	var $mAmphurCode03;
	function getAmphurCode03() { return htmlspecialchars($this->mAmphurCode03); }
	function setAmphurCode03($data) { $this->mAmphurCode03 = $data; }

	var $mProvinceCode03;
	function getProvinceCode03() { return htmlspecialchars($this->mProvinceCode03); }
	function setProvinceCode03($data) { $this->mProvinceCode03 = $data; }	

	var $mZipCode03;
	function getZipCode03() { return htmlspecialchars($this->mZipCode03); }
	function setZipCode03($data) { $this->mZipCode03 = $data; }	
	
	var $mZip03;
	function getZip03() { return htmlspecialchars($this->mZip03); }
	function setZip03($data) { $this->mZip03 = $data; }	
	
	var $mTel03;
	function getTel03() { return htmlspecialchars($this->mTel03); }
	function setTel03($data) { $this->mTel03 = $data; }	
	
	var $mFax03;
	function getFax03() { return htmlspecialchars($this->mFax03); }
	function setFax03($data) { $this->mFax03 = $data; }	
	
	var $mName04;
	function getName04() { return htmlspecialchars($this->mName04); }
	function setName04($data) { $this->mName04 = $data; }			
	
	var $mAddress04;
	function getAddress04() { return htmlspecialchars($this->mAddress04); }
	function setAddress04($data) { $this->mAddress04 = $data; }

	var $mAddress041;
	function getAddress041() { return htmlspecialchars($this->mAddress041); }
	function setAddress041($data) { $this->mAddress041 = $data; }
	
	var $mHomeNo04;
	function getHomeNo04() { return htmlspecialchars($this->mHomeNo04); }
	function setHomeNo04($data) { $this->mHomeNo04 = $data; }

	var $mCompanyName04;
	function getCompanyName04() { return htmlspecialchars($this->mCompanyName04); }
	function setCompanyName04($data) { $this->mCompanyName04 = $data; }
	
	var $mBuilding04;
	function getBuilding04() { return htmlspecialchars($this->mBuilding04); }
	function setBuilding04($data) { $this->mBuilding04 = $data; }

	var $mMooNo04;
	function getMooNo04() { return htmlspecialchars($this->mMooNo04); }
	function setMooNo04($data) { $this->mMooNo04 = $data; }

	var $mMooban04;
	function getMooban04() { return htmlspecialchars($this->mMooban04); }
	function setMooban04($data) { $this->mMooban04 = $data; }

	var $mSoi04;
	function getSoi04() { return htmlspecialchars($this->mSoi04); }
	function setSoi04($data) { $this->mSoi04 = $data; }

	var $mRoad04;
	function getRoad04() { return htmlspecialchars($this->mRoad04); }
	function setRoad04($data) { $this->mRoad04 = $data; }	
	
	var $mTumbon04;
	function getTumbon04() { return htmlspecialchars($this->mTumbon04); }
	function setTumbon04($data) { $this->mTumbon04 = $data; }

	var $mAmphur04;
	function getAmphur04() { return htmlspecialchars($this->mAmphur04); }
	function setAmphur04($data) { $this->mAmphur04 = $data; }

	var $mProvince04;
	function getProvince04() { return htmlspecialchars($this->mProvince04); }
	function setProvince04($data) { $this->mProvince04 = $data; }
	
	var $mTumbonCode04;
	function getTumbonCode04() { return htmlspecialchars($this->mTumbonCode04); }
	function setTumbonCode04($data) { $this->mTumbonCode04 = $data; }

	var $mAmphurCode04;
	function getAmphurCode04() { return htmlspecialchars($this->mAmphurCode04); }
	function setAmphurCode04($data) { $this->mAmphurCode04 = $data; }

	var $mProvinceCode04;
	function getProvinceCode04() { return htmlspecialchars($this->mProvinceCode04); }
	function setProvinceCode04($data) { $this->mProvinceCode04 = $data; }	

	var $mZipCode04;
	function getZipCode04() { return htmlspecialchars($this->mZipCode04); }
	function setZipCode04($data) { $this->mZipCode04 = $data; }	
	
	var $mZip04;
	function getZip04() { return htmlspecialchars($this->mZip04); }
	function setZip04($data) { $this->mZip04 = $data; }	
	
	var $mTel04;
	function getTel04() { return htmlspecialchars($this->mTel04); }
	function setTel04($data) { $this->mTel04 = $data; }	
	
	var $mFax04;
	function getFax04() { return htmlspecialchars($this->mFax04); }
	function setFax04($data) { $this->mFax04 = $data; }		
	
	
	var $mIncomplete;
	function getIncomplete() { return htmlspecialchars($this->mIncomplete); }
	function setIncomplete($data) { $this->mIncomplete = $data; }	
	
	var $mMailback;
	function getMailback() { return htmlspecialchars($this->mMailback); }
	function setMailback($data) { $this->mMailback = $data; }	
	
	var $mVerifyAddress;
	function getVerifyAddress() { return htmlspecialchars($this->mVerifyAddress); }
	function setVerifyAddress($data) { $this->mVerifyAddress = $data; }	

	var $mVerifyPhone;
	function getVerifyPhone() { return htmlspecialchars($this->mVerifyPhone); }
	function setVerifyPhone($data) { $this->mVerifyPhone = $data; }		
	
	var $mAddBy;
	function getAddBy() { return htmlspecialchars($this->mAddBy); }
	function setAddBy($data) { $this->mAddBy = $data; }

	var $mAddDate;
	function getAddDate() { return htmlspecialchars($this->mAddDate); }
	function setAddDate($data) { $this->mAddDate = $data; }
	
	var $mEditBy;
	function getEditBy() { return htmlspecialchars($this->mEditBy); }
	function setEditBy($data) { $this->mEditBy = $data; }
	
	var $mEditDate;
	function getEditDate() { return htmlspecialchars($this->mEditDate); }
	function setEditDate($data) { $this->mEditDate = $data; }
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }
	
	var $mAcardOld;
	function getAcardOld() { return $this->mAcardOld; }
	function setAcardOld($data) { $this->mAcardOld = $data; }
	
	var $mMailbackRemark;
	function getMailbackRemark() { return $this->mMailbackRemark; }
	function setMailbackRemark($data) { $this->mMailbackRemark = $data; }	

	var $mMailbackDate;
	function getMailbackDate() { return $this->mMailbackDate; }
	function setMailbackDate($data) { $this->mMailbackDate = $data; }		

	var $mMailbackReason;
	function getMailbackReason() { return $this->mMailbackReason; }
	function setMailbackReason($data) { $this->mMailbackReason = $data; }		
	
	var $mDuplicates;
	function getDuplicates() { return $this->mDuplicates; }
	function setDuplicates($data) { $this->mDuplicates = $data; }			
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }				
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }
	
	var $mInsureMember;
	function getInsureMember() { return $this->mInsureMember; }
	function setInsureMember($data) { $this->mInsureMember = $data; }
	
	var $mNoCall;
	function getNoCall() { return $this->mNoCall; }
	function setNoCall($data) { $this->mNoCall = $data; }
	
	var $mNoCallReason;
	function getNoCallReason() { return $this->mNoCallReason; }
	function setNoCallReason($data) { $this->mNoCallReason = $data; }
	
	var $mNoSms;
	function getNoSms() { return $this->mNoSms; }
	function setNoSms($data) { $this->mNoSms = $data; }
	
	var $mNoSmsReason;
	function getNoSmsReason() { return $this->mNoSmsReason; }
	function setNoSmsReason($data) { $this->mNoSmsReason = $data; }

	var $mNoMail;
	function getNoMail() { return $this->mNoMail; }
	function setNoMail($data) { $this->mNoMail = $data; }
	
	var $mNoMailReason;
	function getNoMailReason() { return $this->mNoMailReason; }
	function setNoMailReason($data) { $this->mNoMailReason = $data; }			
	
	function Customer($objData=NULL) {
        If ($objData->customer_id!="") {
            $this->setCustomerId($objData->customer_id);
            $this->setTypeId($objData->type_id);
			$this->setACard($objData->acard);
			$this->setBCard($objData->bcard);
			$this->setCCard($objData->ccard);
			$this->setICard($objData->icard);
			$this->setBondCard($objData->bondcard);
            $this->setGradeId($objData->grade_id);
            $this->setGroupId($objData->group_id);
			$this->setEventId($objData->event_id);
			$this->setFollowUp($objData->follow_up);
            $this->setInformationDate($objData->information_date);
            $this->setSaleId($objData->sale_id);
            $this->setBirthday($objData->birthday);
            $this->setTitle($objData->title);			
			$this->setFirstname($objData->firstname);
			$this->setlastname($objData->lastname);
			$this->setCustomerTitleId($objData->customer_title_id);
			$this->setCustomerTitleDetail($objData->customer_title_detail);
			$this->setEmail($objData->email);
			$this->setIDCard($objData->id_card);
			$this->setAddress($objData->address);
			$this->setAddress1($objData->address1);
			
			$this->setHomeNo($objData->homeno);
			$this->setCompanyName($objData->company_name);
			$this->setBuilding($objData->building);
			$this->setMooNo($objData->moono);
			$this->setMooban($objData->mooban);
			$this->setSoi($objData->soi);
			$this->setRoad($objData->road);
			
			$this->setTumbon($objData->tumbon);
			$this->setAmphur($objData->amphur);
			$this->setProvince($objData->province);
			$this->setTumbonCode($objData->tumbon_code);
			$this->setAmphurCode($objData->amphur_code);
			$this->setProvinceCode($objData->province_code);
			$this->setZipCode($objData->zip_code);
			$this->setZip($objData->zip);
			$this->setHomeTel($objData->home_tel);
			$this->setMobile($objData->mobile);
			$this->setOfficeTel($objData->office_tel);
			$this->setFax($objData->fax);
			$this->setContactName($objData->contact_name);
			$this->setAddress01($objData->address01);
			$this->setAddress011($objData->address011);
			
			$this->setHomeNo01($objData->homeno01);
			$this->setCompanyName01($objData->company_name01);
			$this->setBuilding01($objData->building01);
			$this->setMooNo01($objData->moono01);
			$this->setMooban01($objData->mooban01);
			$this->setSoi01($objData->soi01);
			$this->setRoad01($objData->road01);
			
			$this->setTumbon01($objData->tumbon01);
			$this->setAmphur01($objData->amphur01);
			$this->setProvince01($objData->province01);
			$this->setTumbonCode01($objData->tumbon_code01);
			$this->setAmphurCode01($objData->amphur_code01);
			$this->setProvinceCode01($objData->province_code01);	
			$this->setZipCode01($objData->zip_code01);	
			$this->setZip01($objData->zip01);
			$this->setTel01($objData->tel01);
			$this->setFax01($objData->fax01);
			
			
			$this->setCompany($objData->company);
			$this->setCompanyType($objData->company_type);
			$this->setCompanyPosition($objData->company_position);
			$this->setCompanySection($objData->company_section);
			$this->setCompanyDepartment($objData->company_department);
			$this->setCompanyAge($objData->company_age);
			$this->setCompanyAgeMonth($objData->company_age_month);
			$this->setCompanySalary($objData->company_salary);
			$this->setCompanyOther($objData->company_other);
			$this->setCompanyOtherRemark($objData->company_other_remark);
			$this->setMariageName($objData->mariage_name);
			$this->setMariageCompany($objData->mariage_company);
			$this->setMariagePosition($objData->mariage_position);
			$this->setMariagePhone($objData->mariage_phone);
			$this->setMariagePhoneExtend($objData->mariage_phone_extend);
			$this->setMariageSalary($objData->mariage_salary);
			
			$this->setRentalType($objData->rental_type);
			$this->setRentalYear($objData->rental_year);
			$this->setRentalWith($objData->rental_with);
			
			$this->setAddress02($objData->address02);
			$this->setAddress021($objData->address021);
			
			$this->setHomeNo02($objData->homeno02);
			$this->setCompanyName02($objData->company_name02);
			$this->setBuilding02($objData->building02);
			$this->setMooNo02($objData->moono02);
			$this->setMooban02($objData->mooban02);
			$this->setSoi02($objData->soi02);
			$this->setRoad02($objData->road02);			
			
			$this->setTumbon02($objData->tumbon02);
			$this->setAmphur02($objData->amphur02);
			$this->setProvince02($objData->province02);
			$this->setTumbonCode02($objData->tumbon_code02);
			$this->setAmphurCode02($objData->amphur_code02);
			$this->setProvinceCode02($objData->province_code02);
			$this->setZipCode02($objData->zip_code02);	
			$this->setZip02($objData->zip02);
			$this->setTel02($objData->tel02);
			$this->setFax02($objData->fax02);
			
			$this->setName03($objData->name03);
			$this->setAddress03($objData->address03);
			$this->setAddress031($objData->address031);
			
			$this->setHomeNo03($objData->homeno03);
			$this->setCompanyName03($objData->company_name03);
			$this->setBuilding03($objData->building03);
			$this->setMooNo03($objData->moono03);
			$this->setMooban03($objData->mooban03);
			$this->setSoi03($objData->soi03);
			$this->setRoad03($objData->road03);			
			
			$this->setTumbon03($objData->tumbon03);
			$this->setAmphur03($objData->amphur03);
			$this->setProvince03($objData->province03);
			$this->setTumbonCode03($objData->tumbon_code03);
			$this->setAmphurCode03($objData->amphur_code03);
			$this->setProvinceCode03($objData->province_code03);
			$this->setZipCode03($objData->zip_code03);	
			$this->setZip03($objData->zip03);
			$this->setTel03($objData->tel03);
			$this->setFax03($objData->fax03);

			$this->setName04($objData->name04);
			$this->setAddress04($objData->address04);
			$this->setAddress041($objData->address041);
			
			$this->setHomeNo04($objData->homeno04);
			$this->setCompanyName04($objData->company_name04);
			$this->setBuilding04($objData->building04);
			$this->setMooNo04($objData->moono04);
			$this->setMooban04($objData->mooban04);
			$this->setSoi04($objData->soi04);
			$this->setRoad04($objData->road04);			
			
			$this->setTumbon04($objData->tumbon04);
			$this->setAmphur04($objData->amphur04);
			$this->setProvince04($objData->province04);
			$this->setTumbonCode04($objData->tumbon_code04);
			$this->setAmphurCode04($objData->amphur_code04);
			$this->setProvinceCode04($objData->province_code04);
			$this->setZipCode04($objData->zip_code04);	
			$this->setZip04($objData->zip04);
			$this->setTel04($objData->tel04);			
			$this->setFax04($objData->fax04);			
			
			$this->setIncomplete($objData->incomplete);
			$this->setMailback($objData->mailback);
			$this->setVerifyAddress($objData->verify_address);
			$this->setVerifyPhone($objData->verify_phone);
			$this->setFax03($objData->fax03);			
			$this->setAddBy($objData->add_by);
			$this->setAddDate($objData->add_date);
			$this->setEditBy($objData->edit_by);
			$this->setEditDate($objData->edit_date);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
			$this->setAcardOld($objData->acard_old);
			$this->setMailbackRemark($objData->mailback_remark);
			$this->setMailbackReason($objData->mailback_reason);
			$this->setMailbackDate($objData->mailback_date);
			$this->setDuplicates($objData->duplicates);
			$this->setRemark($objData->remark);
			$this->setCompanyId($objData->company_id);
			$this->setInsureMember($objData->insure_member);
			$this->setNoCall($objData->no_call);
			$this->setNoCallReason($objData->no_call_reason);
			$this->setNoSms($objData->no_sms);
			$this->setNoSmsReason($objData->no_sms_reason);
			$this->setNoMail($objData->no_mail);
			$this->setNoMailReason($objData->no_mail_reason);

        }
    }

	function init(){	
		$this->setIDCard(stripslashes($this->mIDCard));
		$this->setTitle(stripslashes($this->mTitle));
		$this->setFirstname(stripslashes($this->mFirstname));
		$this->setLastname(stripslashes($this->mLastname));		
		$this->setEmail(stripslashes($this->mEmail));
		$this->setAddress(stripslashes($this->mAddress));
		$this->setAddress1(stripslashes($this->mAddress1));
		$this->setTumbon(stripslashes($this->mTumbon));
		$this->setAmphur(stripslashes($this->mAmphur));
		$this->setProvince(stripslashes($this->mProvince));
		$this->setZip(stripslashes($this->mZip));
		$this->setHomeTel(stripslashes($this->mHomeTel));
		$this->setOfficeTel(stripslashes($this->mOfficeTel));
		$this->setMobile(stripslashes($this->mMobile));
		$this->setFax(stripslashes($this->mFax));
		
		$this->setAddress01(stripslashes($this->mAddress01));
		$this->setAddress011(stripslashes($this->mAddress011));
		$this->setTumbon01(stripslashes($this->mTumbon01));
		$this->setAmphur01(stripslashes($this->mAmphur01));
		$this->setProvince01(stripslashes($this->mProvince01));
		$this->setZip01(stripslashes($this->mZip01));		
		$this->setTel01(stripslashes($this->mTel01));		
		$this->setFax01(stripslashes($this->mFax01));		
		
		$this->setCompany(stripslashes($this->mCompany));
		$this->setAddress02(stripslashes($this->mAddress02));
		$this->setAddress021(stripslashes($this->mAddress021));
		$this->setTumbon02(stripslashes($this->mTumbon02));
		$this->setAmphur02(stripslashes($this->mAmphur02));
		$this->setProvince02(stripslashes($this->mProvince02));
		$this->setZip02(stripslashes($this->mZip02));		
		$this->setTel02(stripslashes($this->mTel02));		
		$this->setFax02(stripslashes($this->mFax02));		
		
		$this->setName03(stripslashes($this->mName03));
		$this->setAddress03(stripslashes($this->mAddress03));
		$this->setAddress031(stripslashes($this->mAddress031));
		$this->setTumbon03(stripslashes($this->mTumbon03));
		$this->setAmphur03(stripslashes($this->mAmphur03));
		$this->setProvince03(stripslashes($this->mProvince03));
		$this->setZip03(stripslashes($this->mZip03));		
		$this->setTel03(stripslashes($this->mTel03));		
		$this->setFax03(stripslashes($this->mFax03));
		
		$this->setName04(stripslashes($this->mName04));
		$this->setAddress04(stripslashes($this->mAddress04));
		$this->setAddress041(stripslashes($this->mAddress041));
		$this->setTumbon04(stripslashes($this->mTumbon04));
		$this->setAmphur04(stripslashes($this->mAmphur04));
		$this->setProvince04(stripslashes($this->mProvince04));
		$this->setZip04(stripslashes($this->mZip04));		
		$this->setTel04(stripslashes($this->mTel04));		
		$this->setFax04(stripslashes($this->mFax04));
		
	}
	
	function load() {

		if ($this->mCustomerId == '') {
			return false;
		}
		$strSql = "SELECT C.*,  CT.title AS customer_title_detail FROM ".$this->TABLE." C "
					." LEFT JOIN t_customer_title CT ON CT.customer_title_id = C.customer_title_id "
					." WHERE customer_id = ".$this->mCustomerId;

		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Customer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadUTF8() {

		if ($this->mCustomerId == '') {
			return false;
		}
		$strSql = "SELECT C.*,  CT.title AS customer_title_detail FROM ".$this->TABLE." C "
					." LEFT JOIN t_customer_title CT ON CT.customer_title_id = C.customer_title_id "
					." WHERE customer_id = ".$this->mCustomerId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Customer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	//function load for sale report : rp01
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(C.customer_id) AS RSUM  FROM ".$this->TABLE." C "
					." WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}
	
	
	function add() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( company_id, type_id, acard, bcard, ccard, icard, grade_id, event_id, group_id, follow_up, sale_id, information_date, birthday, title, firstname, lastname, customer_title_id, email, "
						."   id_card, contact_name, address, address1, homeno, company_name,  building, moono, mooban, soi, road,  tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip_code, zip, home_tel, mobile, office_tel, fax, add_date, add_by ,
						address01, address011, homeno01, company_name01, building01, moono01, mooban01, soi01, road01,  tumbon01, amphur01,province01,  tumbon_code01, amphur_code01, province_code01, zip_code01, zip01, tel01, fax01,
						company, company_type, company_position, company_section, company_department, company_age, company_age_month, company_salary, company_other, company_other_remark, mariage_name, mariage_company, mariage_position, mariage_phone, mariage_phone_extend, mariage_salary, 
						address02, address021, homeno02, company_name02, building02, moono02, mooban02, soi02, road02, tumbon02, amphur02,province02, tumbon_code02, amphur_code02, province_code02, zip_code02, zip02, tel02, fax02,
						name03, address03, address031, homeno03, company_name03, building03, moono03, mooban03, soi03, road03, tumbon03, amphur03,province03,  tumbon_code03, amphur_code03, province_code03,  zip_code03, zip03, tel03, fax03,
						name04, address04, address041, homeno04, company_name04, building04, moono04, mooban04, soi04, road04, tumbon04, amphur04,province04,  tumbon_code04, amphur_code04, province_code04,  zip_code04, zip04, tel04, fax04,
						 incomplete ,duplicates,  remark, mailback_remark, mailback_reason, insure_member ) "
						." VALUES ( '".$sCompanyId."' , "
						."  '".$this->mTypeId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mICard."' , "
						."  '".$this->mGradeId."' , "
						."  '".$this->mEventId."' , "
						."  '".$this->mGroupId."' , "
						."  '".$this->mFollowUp."' , "
						."  '".$this->mSaleId."' , "
						."  '".$this->mInformationDate."' , "
						."  '".$this->mBirthday."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mEmail."' , "

						."  '".$this->mIDCard."' , "
						."  '".$this->mContactName."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mAddress1."' , "
						."  '".$this->mHomeNo."' , "
						."  '".$this->mCompanyName."' , "
						."  '".$this->mBuilding."' , "
						."  '".$this->mMooNo."' , "
						."  '".$this->mMooban."' , "
						."  '".$this->mSoi."' , "
						."  '".$this->mRoad."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZipCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mHomeTel."' , "
						."  '".$this->mMobile."' , "
						."  '".$this->mOfficeTel."' , "
						."  '".$this->mFax."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' ,"


						."  '".$this->mAddress01."' , "
						."  '".$this->mAddress011."' , "
						."  '".$this->mHomeNo01."' , "
						."  '".$this->mCompanyName01."' , "
						."  '".$this->mBuilding01."' , "
						."  '".$this->mMooNo01."' , "
						."  '".$this->mMooban01."' , "
						."  '".$this->mSoi01."' , "
						."  '".$this->mRoad01."' , "
						."  '".$this->mTumbon01."' , "
						."  '".$this->mAmphur01."' , "
						."  '".$this->mProvince01."' , "
						."  '".$this->mTumbonCode01."' , "
						."  '".$this->mAmphurCode01."' , "
						."  '".$this->mProvinceCode01."' , "
						."  '".$this->mZipCode01."' , "
						."  '".$this->mZip01."' , "
						."  '".$this->mTel01."' , "
						."  '".$this->mFax01."' , "
						
						
						."  '".$this->mCompany."' , "
						."  '".$this->mCompanyType."' , "
						."  '".$this->mCompanyPosition."' , "
						."  '".$this->mCompanySection."' , "
						."  '".$this->mCompanyDepartment."' , "
						."  '".$this->mCompanyAge."' , "
						."  '".$this->mCompanyAgeMonth."' , "
						."  '".$this->mCompanySalary."' , "
						."  '".$this->mCompanyOther."' , "
						."  '".$this->mCompanyOtherRemark."' , "
						."  '".$this->mMariageName."' , "
						."  '".$this->mMariageCompany."' , "
						."  '".$this->mMariagePosition."' , "
						."  '".$this->mMariagePhone."' , "
						."  '".$this->mMariagePhoneExtend."' , "
						."  '".$this->mMariageSalary."' , "
						
						."  '".$this->mAddress02."' , "
						."  '".$this->mAddress021."' , "
						
						."  '".$this->mHomeNo02."' , "
						."  '".$this->mCompanyName02."' , "
						."  '".$this->mBuilding02."' , "
						."  '".$this->mMooNo02."' , "
						."  '".$this->mMooban02."' , "
						."  '".$this->mSoi02."' , "
						."  '".$this->mRoad02."' , "
						
						."  '".$this->mTumbon02."' , "
						."  '".$this->mAmphur02."' , "
						."  '".$this->mProvince02."' , "
						."  '".$this->mTumbonCode02."' , "
						."  '".$this->mAmphurCode02."' , "
						."  '".$this->mProvinceCode02."' , "
						."  '".$this->mZipCode02."' , "
						."  '".$this->mZip02."' , "
						."  '".$this->mTel02."' , "
						."  '".$this->mFax02."' , "
						
						."  '".$this->mName03."' , "
						."  '".$this->mAddress03."' , "
						."  '".$this->mAddress031."' , "
						
						."  '".$this->mHomeNo03."' , "
						."  '".$this->mCompanyName03."' , "
						."  '".$this->mBuilding03."' , "
						."  '".$this->mMooNo03."' , "
						."  '".$this->mMooban03."' , "
						."  '".$this->mSoi03."' , "
						."  '".$this->mRoad03."' , "
						
						."  '".$this->mTumbon03."' , "
						."  '".$this->mAmphur03."' , "
						."  '".$this->mProvince03."' , "
						."  '".$this->mTumbonCode03."' , "
						."  '".$this->mAmphurCode03."' , "
						."  '".$this->mProvinceCode03."' , "
						."  '".$this->mZipCode03."' , "
						."  '".$this->mZip03."' , "
						."  '".$this->mTel03."' , "
						."  '".$this->mFax03."' , "
						
						."  '".$this->mName04."' , "
						."  '".$this->mAddress04."' , "
						."  '".$this->mAddress041."' , "
						
						."  '".$this->mHomeNo04."' , "
						."  '".$this->mCompanyName04."' , "
						."  '".$this->mBuilding04."' , "
						."  '".$this->mMooNo04."' , "
						."  '".$this->mMooban04."' , "
						."  '".$this->mSoi04."' , "
						."  '".$this->mRoad04."' , "
						
						."  '".$this->mTumbon04."' , "
						."  '".$this->mAmphur04."' , "
						."  '".$this->mProvince04."' , "
						."  '".$this->mTumbonCode04."' , "
						."  '".$this->mAmphurCode04."' , "
						."  '".$this->mProvinceCode04."' , "
						."  '".$this->mZipCode04."' , "
						."  '".$this->mZip04."' , "
						."  '".$this->mTel04."' , "
						."  '".$this->mFax04."' , "
						
						."  '".$this->mIncomplete."' , "
						."  '".$this->mDuplicates."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mMailbackRemark."' , "
						."  '".$this->mMailbackReason."' , "
						."  '".$this->mInsureMember."' ) ";
						
		//echo $strSql;
        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_acard");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}
	
	
	function addImport() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( company_id, type_id, acard, bcard, ccard, icard, grade_id, event_id, group_id, follow_up, sale_id, information_date, birthday, title, firstname, lastname, customer_title_id, email, "
						."   id_card, contact_name, address, address1, homeno, company_name,  building, moono, mooban, soi, road,  tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip_code, zip, home_tel, mobile, office_tel, fax, add_date, add_by ,
						address01, address011, homeno01, company_name01, building01, moono01, mooban01, soi01, road01,  tumbon01, amphur01,province01,  tumbon_code01, amphur_code01, province_code01, zip_code01, zip01, tel01, fax01,
						company, company_type, company_position, company_section, company_department, company_age, company_age_month, company_salary, company_other, company_other_remark, mariage_name, mariage_company, mariage_position, mariage_phone, mariage_phone_extend, mariage_salary, 
						address02, address021, homeno02, company_name02, building02, moono02, mooban02, soi02, road02, tumbon02, amphur02,province02, tumbon_code02, amphur_code02, province_code02, zip_code02, zip02, tel02, fax02,
						name03, address03, address031, homeno03, company_name03, building03, moono03, mooban03, soi03, road03, tumbon03, amphur03,province03,  tumbon_code03, amphur_code03, province_code03,  zip_code03, zip03, tel03, fax03,
						name04, address04, address041, homeno04, company_name04, building04, moono04, mooban04, soi04, road04, tumbon04, amphur04,province04,  tumbon_code04, amphur_code04, province_code04,  zip_code04, zip04, tel04, fax04,
						 incomplete ,duplicates,  remark, mailback_remark, mailback_reason, insure_member ) "
						." VALUES ( '".$sCompanyId."' , "
						."  '".$this->mTypeId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mICard."' , "
						."  '".$this->mGradeId."' , "
						."  '".$this->mEventId."' , "
						."  '".$this->mGroupId."' , "
						."  '".$this->mFollowUp."' , "
						."  '".$this->mSaleId."' , "
						."  '".$this->mInformationDate."' , "
						."  '".$this->mBirthday."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mEmail."' , "

						."  '".$this->mIDCard."' , "
						."  '".$this->mContactName."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mAddress1."' , "
						."  '".$this->mHomeNo."' , "
						."  '".$this->mCompanyName."' , "
						."  '".$this->mBuilding."' , "
						."  '".$this->mMooNo."' , "
						."  '".$this->mMooban."' , "
						."  '".$this->mSoi."' , "
						."  '".$this->mRoad."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZipCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mHomeTel."' , "
						."  '".$this->mMobile."' , "
						."  '".$this->mOfficeTel."' , "
						."  '".$this->mFax."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' ,"


						."  '".$this->mAddress01."' , "
						."  '".$this->mAddress011."' , "
						."  '".$this->mHomeNo01."' , "
						."  '".$this->mCompanyName01."' , "
						."  '".$this->mBuilding01."' , "
						."  '".$this->mMooNo01."' , "
						."  '".$this->mMooban01."' , "
						."  '".$this->mSoi01."' , "
						."  '".$this->mRoad01."' , "
						."  '".$this->mTumbon01."' , "
						."  '".$this->mAmphur01."' , "
						."  '".$this->mProvince01."' , "
						."  '".$this->mTumbonCode01."' , "
						."  '".$this->mAmphurCode01."' , "
						."  '".$this->mProvinceCode01."' , "
						."  '".$this->mZipCode01."' , "
						."  '".$this->mZip01."' , "
						."  '".$this->mTel01."' , "
						."  '".$this->mFax01."' , "
						
						
						."  '".$this->mCompany."' , "
						."  '".$this->mCompanyType."' , "
						."  '".$this->mCompanyPosition."' , "
						."  '".$this->mCompanySection."' , "
						."  '".$this->mCompanyDepartment."' , "
						."  '".$this->mCompanyAge."' , "
						."  '".$this->mCompanyAgeMonth."' , "
						."  '".$this->mCompanySalary."' , "
						."  '".$this->mCompanyOther."' , "
						."  '".$this->mCompanyOtherRemark."' , "
						."  '".$this->mMariageName."' , "
						."  '".$this->mMariageCompany."' , "
						."  '".$this->mMariagePosition."' , "
						."  '".$this->mMariagePhone."' , "
						."  '".$this->mMariagePhoneExtend."' , "
						."  '".$this->mMariageSalary."' , "
						
						."  '".$this->mAddress02."' , "
						."  '".$this->mAddress021."' , "
						
						."  '".$this->mHomeNo02."' , "
						."  '".$this->mCompanyName02."' , "
						."  '".$this->mBuilding02."' , "
						."  '".$this->mMooNo02."' , "
						."  '".$this->mMooban02."' , "
						."  '".$this->mSoi02."' , "
						."  '".$this->mRoad02."' , "
						
						."  '".$this->mTumbon02."' , "
						."  '".$this->mAmphur02."' , "
						."  '".$this->mProvince02."' , "
						."  '".$this->mTumbonCode02."' , "
						."  '".$this->mAmphurCode02."' , "
						."  '".$this->mProvinceCode02."' , "
						."  '".$this->mZipCode02."' , "
						."  '".$this->mZip02."' , "
						."  '".$this->mTel02."' , "
						."  '".$this->mFax02."' , "
						
						."  '".$this->mName03."' , "
						."  '".$this->mAddress03."' , "
						."  '".$this->mAddress031."' , "
						
						."  '".$this->mHomeNo03."' , "
						."  '".$this->mCompanyName03."' , "
						."  '".$this->mBuilding03."' , "
						."  '".$this->mMooNo03."' , "
						."  '".$this->mMooban03."' , "
						."  '".$this->mSoi03."' , "
						."  '".$this->mRoad03."' , "
						
						."  '".$this->mTumbon03."' , "
						."  '".$this->mAmphur03."' , "
						."  '".$this->mProvince03."' , "
						."  '".$this->mTumbonCode03."' , "
						."  '".$this->mAmphurCode03."' , "
						."  '".$this->mProvinceCode03."' , "
						."  '".$this->mZipCode03."' , "
						."  '".$this->mZip03."' , "
						."  '".$this->mTel03."' , "
						."  '".$this->mFax03."' , "
						
						."  '".$this->mName04."' , "
						."  '".$this->mAddress04."' , "
						."  '".$this->mAddress041."' , "
						
						."  '".$this->mHomeNo04."' , "
						."  '".$this->mCompanyName04."' , "
						."  '".$this->mBuilding04."' , "
						."  '".$this->mMooNo04."' , "
						."  '".$this->mMooban04."' , "
						."  '".$this->mSoi04."' , "
						."  '".$this->mRoad04."' , "
						
						."  '".$this->mTumbon04."' , "
						."  '".$this->mAmphur04."' , "
						."  '".$this->mProvince04."' , "
						."  '".$this->mTumbonCode04."' , "
						."  '".$this->mAmphurCode04."' , "
						."  '".$this->mProvinceCode04."' , "
						."  '".$this->mZipCode04."' , "
						."  '".$this->mZip04."' , "
						."  '".$this->mTel04."' , "
						."  '".$this->mFax04."' , "
						
						."  '".$this->mIncomplete."' , "
						."  '".$this->mDuplicates."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mMailbackRemark."' , "
						."  '".$this->mMailbackReason."' , "
						."  '".$this->mInsureMember."' ) ";
						
		echo $strSql."<br>";
        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_acard");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }

	}
	
	

	function addCashierBooking() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( type_id, company_id, acard, bcard,ccard, title, firstname, lastname, customer_title_id, "
						."  address, address1, homeno, company_name,  building, moono, mooban, soi, road,  tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip, home_tel, mobile, incomplete, add_date, add_by ) "
						." VALUES ( '".$this->mTypeId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mAddress1."' , "
						."  '".$this->mHomeNo."' , "
						."  '".$this->mCompanyName."' , "
						."  '".$this->mBuilding."' , "
						."  '".$this->mMooNo."' , "
						."  '".$this->mMooban."' , "
						."  '".$this->mSoi."' , "
						."  '".$this->mRoad."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mHomeTel."' , "
						."  '".$this->mMobile."' , "
						."  '".$this->mIncomplete."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' )";

        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_cashier_booking");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}	
	
	function addTemp() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO t_customer "
						." ( type_id, company_id, acard, bcard,ccard, grade_id, event_id, group_id, follow_up, sale_id, information_date, birthday, title, firstname, lastname, customer_title_id, "
						."  address, tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip, incomplete, add_date, add_by , acard_old, mailback ) "
						." VALUES ( '".$this->mTypeId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mGradeId."' , "
						."  '".$this->mEventId."' , "
						."  '".$this->mGroupId."' , "
						."  '".$this->mFollowUp."' , "
						."  '".$this->mSaleId."' , "
						."  '".$this->mInformationDate."' , "
						."  '".$this->mBirthday."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mIncomplete."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' , "
						."  '".$this->mAcardOld."' , "
						."  '".$this->mMailback."' )";
		echo $strSql;
        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
            return $this->mCustomerId;
        } else {
			return false;
	    }

	}		
	
	
	function addAccountDealer() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( type_id, company_id, acard, bcard,ccard, title, firstname, lastname, customer_title_id, "
						."  address, tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip, dealer, incomplete, add_date, add_by ) "
						." VALUES ( '".$this->mTypeId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZip."' , "
						."  '1' , "
						."  '".$this->mIncomplete."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' )";

        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_account_dealer");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}	
	
	function addInsurance() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( type_id, company_id, acard, bcard, ccard, icard, grade_id, event_id, group_id, follow_up, sale_id, information_date, birthday, title, firstname, lastname, customer_title_id, email, "
						."   id_card, address, tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip_code, zip, home_tel, mobile, office_tel, fax, 
							  name04, address04, address041, tumbon04, amphur04,province04,  tumbon_code04, amphur_code04, province_code04,  zip_code04, zip04, tel04, fax04,	add_date, add_by  ) "
						." VALUES ( '".$this->mTypeId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mACard."' , "
						."  '".$this->mBCard."' , "
						."  '".$this->mCCard."' , "
						."  '".$this->mICard."' , "
						."  '".$this->mGradeId."' , "
						."  '".$this->mEventId."' , "
						."  '".$this->mGroupId."' , "
						."  '".$this->mFollowUp."' , "
						."  '".$this->mSaleId."' , "
						."  '".$this->mInformationDate."' , "
						."  '".$this->mBirthday."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mEmail."' , "
						."  '".$this->mIDCard."' , "
						
						."  '".$this->mAddress."' , "
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZipCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mHomeTel."' , "
						."  '".$this->mMobile."' , "
						."  '".$this->mOfficeTel."' , "
						."  '".$this->mFax."' , "
						
						."  '".$this->mName04."' , "
						."  '".$this->mAddress04."' , "
						."  '".$this->mAddress041."' , "
						."  '".$this->mTumbon04."' , "
						."  '".$this->mAmphur04."' , "
						."  '".$this->mProvince04."' , "
						."  '".$this->mTumbonCode04."' , "
						."  '".$this->mAmphurCode04."' , "
						."  '".$this->mProvinceCode04."' , "
						."  '".$this->mZipCode04."' , "
						."  '".$this->mZip04."' , "
						."  '".$this->mTel04."' , "
						."  '".$this->mFax04."' , "
						
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' )";

        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_insurance");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}		
	
	function addBond() {
	global $sMemberId;	
	global $sCompanyId;	
	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( type_id, company_id, bondcard, information_date, birthday, title, firstname, lastname, customer_title_id, email, "
						."   id_card, homeno, company_name,  building, moono, mooban, soi, road, tumbon, amphur, province, tumbon_code, amphur_code, province_code, zip_code, zip, home_tel, mobile, office_tel, fax, 
							  homeno02, company_name02,  building02, moono02, mooban02, soi02, road02,  tumbon02, amphur02,province02,  tumbon_code02, amphur_code02, province_code02,  zip_code02, zip02, tel02, fax02,	
							  company, company_type, company_position, company_section, company_department, company_age, company_age_month, company_salary, company_other, company_other_remark, rental_type, rental_with, rental_year, 
							  
							  add_date, add_by  ) "
						." VALUES ( '".$this->mTypeId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mBondCard."' , "
						."  '".date("Y-m-d")."' , "
						."  '".$this->mBirthday."' , "
						."  '".$this->mTitle."' , "
						."  '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mCustomerTitleId."' , "
						."  '".$this->mEmail."' , "
						."  '".$this->mIDCard."' , "

						."  '".$this->mHomeNo."' , "
						."  '".$this->mCompanyName."' , "
						."  '".$this->mBuilding."' , "
						."  '".$this->mMooNo."' , "
						."  '".$this->mMooban."' , "
						."  '".$this->mSoi."' , "
						."  '".$this->mRoad."' , "
						
						."  '".$this->mTumbon."' , "
						."  '".$this->mAmphur."' , "
						."  '".$this->mProvince."' , "
						."  '".$this->mTumbonCode."' , "
						."  '".$this->mAmphurCode."' , "
						."  '".$this->mProvinceCode."' , "
						."  '".$this->mZipCode."' , "
						."  '".$this->mZip."' , "
						."  '".$this->mHomeTel."' , "
						."  '".$this->mMobile."' , "
						."  '".$this->mOfficeTel."' , "
						."  '".$this->mFax."' , "
						

						."  '".$this->mHomeNo02."' , "
						."  '".$this->mCompanyName02."' , "
						."  '".$this->mBuilding02."' , "
						."  '".$this->mMooNo02."' , "
						."  '".$this->mMooban02."' , "
						."  '".$this->mSoi02."' , "
						."  '".$this->mRoad02."' , "
						
						."  '".$this->mTumbon02."' , "
						."  '".$this->mAmphur02."' , "
						."  '".$this->mProvince02."' , "
						."  '".$this->mTumbonCode02."' , "
						."  '".$this->mAmphurCode02."' , "
						."  '".$this->mProvinceCode02."' , "
						."  '".$this->mZipCode02."' , "
						."  '".$this->mZip02."' , "
						."  '".$this->mTel02."' , "
						."  '".$this->mFax02."' , "
						
						."  '".$this->mCompany."' , "
						."  '".$this->mCompanyType."' , "
						."  '".$this->mCompanyPosition."' , "
						."  '".$this->mCompanySection."' , "
						."  '".$this->mCompanyDepartment."' , "
						."  '".$this->mCompanyAge."' , "
						."  '".$this->mCompanyAgeMonth."' , "
						."  '".$this->mCompanySalary."' , "
						."  '".$this->mCompanyOther."' , "
						."  '".$this->mCompanyOtherRemark."' , "
						."  '".$this->mRentalType."' , "
						."  '".$this->mRentalWith."' , "
						."  '".$this->mRentalYear."' , "
						
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mAddBy."' )";

        $this->getConnection();

        If ($result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"add_insurance");
			
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}		
		
	
	
	function update(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  acard = '".$this->mACard."'  "	
						." ,  bcard = '".$this->mBCard."'  "	
						." ,  ccard = '".$this->mCCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  grade_id = '".$this->mGradeId."'  "
						." ,  event_id = '".$this->mEventId."'  "
						." ,  group_id = '".$this->mGroupId."'  "	
						." ,  follow_up = '".$this->mFollowUp."'  "	
						." ,  sale_id = '".$this->mSaleId."'  "	
						." ,  information_date = '".$this->mInformationDate."'  "	
						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , id_card  = '".$this->mIDCard."'  "
						." , contact_name  = '".$this->mContactName."'  "
						." , address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , address01  = '".$this->mAddress01."'  "
						." , address011  = '".$this->mAddress011."'  "
						
						." , homeno01  = '".$this->mHomeNo01."'  "
						." , company_name01  = '".$this->mCompanyName01."'  "
						." , building01  = '".$this->mBuilding01."'  "
						." , moono01 = '".$this->mMooNo01."'  "
						." , mooban01  = '".$this->mMooban01."'  "
						." , soi01  = '".$this->mSoi01."'  "
						." , road01  = '".$this->mRoad01."'  "
						
						." , tumbon01  = '".$this->mTumbon01."'  "
						." , amphur01  = '".$this->mAmphur01."'  "
						." , province01  = '".$this->mProvince01."'  "
						." , tumbon_code01  = '".$this->mTumbonCode01."'  "
						." , amphur_code01  = '".$this->mAmphurCode01."'  "
						." , province_code01  = '".$this->mProvinceCode01."'  "
						." , zip_code01  = '".$this->mZipCode01."'  "
						." , zip01  = '".$this->mZip01."'  "
						." , tel01  = '".$this->mTel01."'  "
						." , fax01  = '".$this->mFax01."'  "
						." , company  = '".$this->mCompany."'  "
						." , address02  = '".$this->mAddress02."'  "
						." , address021  = '".$this->mAddress021."'  "
						
						." , homeno02  = '".$this->mHomeNo02."'  "
						." , company_name02  = '".$this->mCompanyName02."'  "
						." , building02  = '".$this->mBuilding02."'  "
						." , moono02 = '".$this->mMooNo02."'  "
						." , mooban02  = '".$this->mMooban02."'  "
						." , soi02  = '".$this->mSoi02."'  "
						." , road02  = '".$this->mRoad02."'  "
						
						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "
						." , name03  = '".$this->mName03."'  "
						." , address03  = '".$this->mAddress03."'  "
						." , address031  = '".$this->mAddress031."'  "
						
						." , homeno03  = '".$this->mHomeNo03."'  "
						." , company_name03  = '".$this->mCompanyName03."'  "
						." , building03  = '".$this->mBuilding03."'  "
						." , moono03 = '".$this->mMooNo03."'  "
						." , mooban03  = '".$this->mMooban03."'  "
						." , soi03  = '".$this->mSoi03."'  "
						." , road03  = '".$this->mRoad03."'  "
						
						." , tumbon03  = '".$this->mTumbon03."'  "
						." , amphur03  = '".$this->mAmphur03."'  "
						." , province03  = '".$this->mProvince03."'  "
						." , tumbon_code03  = '".$this->mTumbonCode03."'  "
						." , amphur_code03  = '".$this->mAmphurCode03."'  "
						." , province_code03  = '".$this->mProvinceCode03."'  "
						." , zip_code03  = '".$this->mZipCode03."'  "
						." , zip03  = '".$this->mZip03."'  "
						." , tel03  = '".$this->mTel03."'  "
						." , fax03  = '".$this->mFax03."'  "
						." , incomplete  = '".$this->mIncomplete."'  "
						." , mailback  = '".$this->mMailback."'  "
						." , mailback_remark  = '".$this->mMailbackRemark."'  "	
						." , mailback_reason  = '".$this->mMailbackReason."'  "	
						." , mailback_date  = '".$this->mMailbackDate."'  "	
						." , verify_address  = '".$this->mVerifyAddress."'  "
						." , verify_phone  = '".$this->mVerifyPhone."'  "
						
						." , no_call  = '".$this->mNoCall."'  "
						." , no_call_reason  = '".$this->mNoCallReason."'  "
						." , no_sms  = '".$this->mNoSms."'  "
						." , no_sms_reason  = '".$this->mNoSmsReason."'  "
						." , no_mail  = '".$this->mNoMail."'  "
						." , no_mail_reason  = '".$this->mNoMailReason."'  "
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_acard");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateAcard(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  acard = '".$this->mACard."'  "	
						." ,  bcard = '".$this->mBCard."'  "	
						." ,  ccard = '".$this->mCCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  grade_id = '".$this->mGradeId."'  "
						." ,  event_id = '".$this->mEventId."'  "
						." ,  group_id = '".$this->mGroupId."'  "	
						." ,  follow_up = '".$this->mFollowUp."'  "	
						." ,  sale_id = '".$this->mSaleId."'  "	
						." ,  information_date = '".$this->mInformationDate."'  "	
						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , contact_name  = '".$this->mContactName."'  "
						." , id_card  = '".$this->mIDCard."'  "
						." , address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , duplicates  = '".$this->mDuplicates."'  "
						." , remark  = '".$this->mRemark."'  "

						." , incomplete  = '".$this->mIncomplete."'  "
						." , mailback  = '".$this->mMailback."'  "
						." , mailback_remark  = '".$this->mMailbackRemark."'  "	
						." , mailback_reason  = '".$this->mMailbackReason."'  "	
						." , mailback_date  = '".$this->mMailbackDate."'  "	
						." , verify_address  = '".$this->mVerifyAddress."'  "
						." , verify_phone  = '".$this->mVerifyPhone."'  "
						
						." , no_call  = '".$this->mNoCall."'  "
						." , no_call_reason  = '".$this->mNoCallReason."'  "
						." , no_sms  = '".$this->mNoSms."'  "
						." , no_sms_reason  = '".$this->mNoSmsReason."'  "
						." , no_mail  = '".$this->mNoMail."'  "
						." , no_mail_reason  = '".$this->mNoMailReason."'  "
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_acard");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateCashierBooking(){
	global $sMemberId;	
	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  acard = '".$this->mACard."'  "	
						." ,  bcard = '".$this->mBCard."'  "	
						." ,  ccard = '".$this->mCCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , incomplete  = '".$this->mIncomplete."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_cashier_booking");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateBond(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "

						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , id_card  = '".$this->mIDCard."'  "

						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "

						." , homeno02  = '".$this->mHomeNo02."'  "
						." , company_name02  = '".$this->mCompanyName02."'  "
						." , building02  = '".$this->mBuilding02."'  "
						." , moono02 = '".$this->mMooNo02."'  "
						." , mooban02  = '".$this->mMooban02."'  "
						." , soi02  = '".$this->mSoi02."'  "
						." , road02  = '".$this->mRoad02."'  "
						
						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "

						." , company  = '".$this->mCompany."'  "
						." , company_type  = '".$this->mCompanyType."'  "
						." , company_position  = '".$this->mCompanyPosition."'  "
						." , company_section  = '".$this->mCompanySection."'  "
						." , company_department  = '".$this->mCompanyDepartment."'  "
						." , company_age  = '".$this->mCompanyAge."'  "
						." , company_age_month  = '".$this->mCompanyAgeMonth."'  "
						." , company_salary  = '".$this->mCompanySalary."'  "
						." , company_other  = '".$this->mCompanyOther."'  "
						." , company_other_remark  = '".$this->mCompanyOtherRemark."'  "

						." , rental_type  = '".$this->mRentalType."'  "
						." , rental_with = '".$this->mRentalWith."'  "
						." , rental_year  = '".$this->mRentalYear."'  "
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_crl_booking_bond");
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateCrlBooking(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  bcard = '".$this->mBCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  grade_id = '".$this->mGradeId."'  "
						." ,  event_id = '".$this->mEventId."'  "
						." ,  group_id = '".$this->mGroupId."'  "	
						." ,  follow_up = '".$this->mFollowUp."'  "	
						." ,  sale_id = '".$this->mSaleId."'  "	
						." ,  information_date = '".$this->mInformationDate."'  "	
						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , id_card  = '".$this->mIDCard."'  "
						." , contact_name  = '".$this->mContactName."'  "
						
						." , address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , address01  = '".$this->mAddress01."'  "
						." , address011  = '".$this->mAddress011."'  "
						
						." , homeno01  = '".$this->mHomeNo01."'  "
						." , company_name01  = '".$this->mCompanyName01."'  "
						." , building01  = '".$this->mBuilding01."'  "
						." , moono01 = '".$this->mMooNo01."'  "
						." , mooban01  = '".$this->mMooban01."'  "
						." , soi01  = '".$this->mSoi01."'  "
						." , road01  = '".$this->mRoad01."'  "
						
						." , tumbon01  = '".$this->mTumbon01."'  "
						." , amphur01  = '".$this->mAmphur01."'  "
						." , province01  = '".$this->mProvince01."'  "
						." , tumbon_code01  = '".$this->mTumbonCode01."'  "
						." , amphur_code01  = '".$this->mAmphurCode01."'  "
						." , province_code01  = '".$this->mProvinceCode01."'  "
						." , zip_code01  = '".$this->mZipCode01."'  "
						." , zip01  = '".$this->mZip01."'  "
						." , tel01  = '".$this->mTel01."'  "
						." , fax01  = '".$this->mFax01."'  "
						." , company  = '".$this->mCompany."'  "
						." , address02  = '".$this->mAddress02."'  "
						." , address021  = '".$this->mAddress021."'  "
						
						." , homeno02  = '".$this->mHomeNo02."'  "
						." , company_name02  = '".$this->mCompanyName02."'  "
						." , building02  = '".$this->mBuilding02."'  "
						." , moono02 = '".$this->mMooNo02."'  "
						." , mooban02  = '".$this->mMooban02."'  "
						." , soi02  = '".$this->mSoi02."'  "
						." , road02  = '".$this->mRoad02."'  "
						
						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "
						." , name03  = '".$this->mName03."'  "
						." , address03  = '".$this->mAddress03."'  "
						." , address031  = '".$this->mAddress031."'  "
						
						." , homeno03  = '".$this->mHomeNo03."'  "
						." , company_name03  = '".$this->mCompanyName03."'  "
						." , building03  = '".$this->mBuilding03."'  "
						." , moono03 = '".$this->mMooNo03."'  "
						." , mooban03  = '".$this->mMooban03."'  "
						." , soi03  = '".$this->mSoi03."'  "
						." , road03  = '".$this->mRoad03."'  "
						
						." , tumbon03  = '".$this->mTumbon03."'  "
						." , amphur03  = '".$this->mAmphur03."'  "
						." , province03  = '".$this->mProvince03."'  "
						." , tumbon_code03  = '".$this->mTumbonCode03."'  "
						." , amphur_code03  = '".$this->mAmphurCode03."'  "
						." , province_code03  = '".$this->mProvinceCode03."'  "
						." , zip_code03  = '".$this->mZipCode03."'  "
						." , zip03  = '".$this->mZip03."'  "
						." , tel03  = '".$this->mTel03."'  "
						." , fax03  = '".$this->mFax03."'  "
						
						." , company  = '".$this->mCompany."'  "
						." , company_type  = '".$this->mCompanyType."'  "
						." , company_position  = '".$this->mCompanyPosition."'  "
						." , company_section  = '".$this->mCompanySection."'  "
						." , company_department  = '".$this->mCompanyDepartment."'  "
						." , company_age  = '".$this->mCompanyAge."'  "
						." , company_age_month  = '".$this->mCompanyAgeMonth."'  "
						." , company_salary  = '".$this->mCompanySalary."'  "
						." , company_other  = '".$this->mCompanyOther."'  "
						." , company_other_remark  = '".$this->mCompanyOtherRemark."'  "

						." , mariage_name  = '".$this->mMariageName."'  "
						." , mariage_company  = '".$this->mMariageCompany."'  "
						." , mariage_position = '".$this->mMariagePosition."'  "
						." , mariage_phone  = '".$this->mMariagePhone."'  "
						." , mariage_phone_extend  = '".$this->mMariagePhoneExtend."'  "
						." , mariage_salary  = '".$this->mMariageSalary."'  "

						." , rental_type  = '".$this->mRentalType."'  "
						." , rental_with = '".$this->mRentalWith."'  "
						." , rental_year  = '".$this->mRentalYear."'  "


						
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_crl_booking");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
		
	
	function updateAccountDealer(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  acard = '".$this->mACard."'  "	
						." ,  bcard = '".$this->mBCard."'  "	
						." ,  ccard = '".$this->mCCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , address  = '".$this->mAddress."'  "
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , incomplete  = '".$this->mIncomplete."'  "
						." , zip  = '".$this->mZip."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_account_dealer");		
		
        $result=$this->query($strSql);

		return $result;
	}	
	


	function updateCrlSending(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "


						." ,  ccard = '".$this->mCCard."'  "	
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  grade_id = '".$this->mGradeId."'  "
						." ,  event_id = '".$this->mEventId."'  "
						." ,  group_id = '".$this->mGroupId."'  "	
						." ,  follow_up = '".$this->mFollowUp."'  "	
						." ,  sale_id = '".$this->mSaleId."'  "	
						." ,  information_date = '".$this->mInformationDate."'  "	
						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , id_card  = '".$this->mIDCard."'  "
						." , contact_name = '".$this->mContactName."'  "

						." , address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , address01  = '".$this->mAddress01."'  "
						." , address011  = '".$this->mAddress011."'  "
						
						." , homeno01  = '".$this->mHomeNo01."'  "
						." , company_name01  = '".$this->mCompanyName01."'  "
						." , building01  = '".$this->mBuilding01."'  "
						." , moono01 = '".$this->mMooNo01."'  "
						." , mooban01  = '".$this->mMooban01."'  "
						." , soi01  = '".$this->mSoi01."'  "
						." , road01  = '".$this->mRoad01."'  "
						
						." , tumbon01  = '".$this->mTumbon01."'  "
						." , amphur01  = '".$this->mAmphur01."'  "
						." , province01  = '".$this->mProvince01."'  "
						." , tumbon_code01  = '".$this->mTumbonCode01."'  "
						." , amphur_code01  = '".$this->mAmphurCode01."'  "
						." , province_code01  = '".$this->mProvinceCode01."'  "
						." , zip_code01  = '".$this->mZipCode01."'  "
						." , zip01  = '".$this->mZip01."'  "
						." , tel01  = '".$this->mTel01."'  "
						." , fax01  = '".$this->mFax01."'  "
						." , company  = '".$this->mCompany."'  "
						." , address02  = '".$this->mAddress02."'  "
						." , address021  = '".$this->mAddress021."'  "
						
						." , homeno02  = '".$this->mHomeNo02."'  "
						." , company_name02  = '".$this->mCompanyName02."'  "
						." , building02  = '".$this->mBuilding02."'  "
						." , moono02 = '".$this->mMooNo02."'  "
						." , mooban02  = '".$this->mMooban02."'  "
						." , soi02  = '".$this->mSoi02."'  "
						." , road02  = '".$this->mRoad02."'  "
						
						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "
						." , name03  = '".$this->mName03."'  "
						." , address03  = '".$this->mAddress03."'  "
						." , address031  = '".$this->mAddress031."'  "
						
						." , homeno03  = '".$this->mHomeNo03."'  "
						." , company_name03  = '".$this->mCompanyName03."'  "
						." , building03  = '".$this->mBuilding03."'  "
						." , moono03 = '".$this->mMooNo03."'  "
						." , mooban03  = '".$this->mMooban03."'  "
						." , soi03  = '".$this->mSoi03."'  "
						." , road03  = '".$this->mRoad03."'  "
						
						." , tumbon03  = '".$this->mTumbon03."'  "
						." , amphur03  = '".$this->mAmphur03."'  "
						." , province03  = '".$this->mProvince03."'  "
						." , tumbon_code03  = '".$this->mTumbonCode03."'  "
						." , amphur_code03  = '".$this->mAmphurCode03."'  "
						." , province_code03  = '".$this->mProvinceCode03."'  "
						." , zip_code03  = '".$this->mZipCode03."'  "
						." , zip03  = '".$this->mZip03."'  "
						." , tel03  = '".$this->mTel03."'  "
						." , fax03  = '".$this->mFax03."'  "
						
						." , company  = '".$this->mCompany."'  "
						." , company_type  = '".$this->mCompanyType."'  "
						." , company_position  = '".$this->mCompanyPosition."'  "
						." , company_section  = '".$this->mCompanySection."'  "
						." , company_department  = '".$this->mCompanyDepartment."'  "
						." , company_age  = '".$this->mCompanyAge."'  "
						." , company_age_month  = '".$this->mCompanyAgeMonth."'  "
						." , company_salary  = '".$this->mCompanySalary."'  "
						." , company_other  = '".$this->mCompanyOther."'  "
						." , company_other_remark  = '".$this->mCompanyOtherRemark."'  "

						." , mariage_name  = '".$this->mMariageName."'  "
						." , mariage_company  = '".$this->mMariageCompany."'  "
						." , mariage_position = '".$this->mMariagePosition."'  "
						." , mariage_phone  = '".$this->mMariagePhone."'  "
						." , mariage_phone_extend  = '".$this->mMariagePhoneExtend."'  "
						." , mariage_salary  = '".$this->mMariageSalary."'  "

						." , rental_type  = '".$this->mRentalType."'  "
						." , rental_with = '".$this->mRentalWith."'  "
						." , rental_year  = '".$this->mRentalYear."'  "
						
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		
		//echo $strSql;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_crl_sending");
				
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	
	
	
	function updateCrlSending01(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET company  = '".$this->mCompany."'  "
						." , company_type  = '".$this->mCompanyType."'  "
						." , company_position  = '".$this->mCompanyPosition."'  "
						." , company_section  = '".$this->mCompanySection."'  "
						." , company_department  = '".$this->mCompanyDepartment."'  "
						." , company_age  = '".$this->mCompanyAge."'  "
						." , company_age_month  = '".$this->mCompanyAgeMonth."'  "
						." , company_salary  = '".$this->mCompanySalary."'  "
						." , company_other  = '".$this->mCompanyOther."'  "
						." , company_other_remark  = '".$this->mCompanyOtherRemark."'  "

						." , mariage_name  = '".$this->mMariageName."'  "
						." , mariage_company  = '".$this->mMariageCompany."'  "
						." , mariage_position = '".$this->mMariagePosition."'  "
						." , mariage_phone  = '".$this->mMariagePhone."'  "
						." , mariage_phone_extend  = '".$this->mMariagePhoneExtend."'  "
						." , mariage_salary  = '".$this->mMariageSalary."'  "

						." , rental_type  = '".$this->mRentalType."'  "
						." , rental_with = '".$this->mRentalWith."'  "
						." , rental_year  = '".$this->mRentalYear."'  "
						
						
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		
		//echo $strSql;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_crl_sending");
				
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	
	
	
	
	
	function updateCrlAddress(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET 
							 address  = '".$this->mAddress."'  "
						." , address1  = '".$this->mAddress1."'  "
						
						." , homeno  = '".$this->mHomeNo."'  "
						." , company_name  = '".$this->mCompanyName."'  "
						." , building  = '".$this->mBuilding."'  "
						." , moono  = '".$this->mMooNo."'  "
						." , mooban  = '".$this->mMooban."'  "
						." , soi  = '".$this->mSoi."'  "
						." , road  = '".$this->mRoad."'  "
						
						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , email  = '".$this->mEmail."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , address01  = '".$this->mAddress01."'  "
						." , address011  = '".$this->mAddress011."'  "
						
						." , homeno01  = '".$this->mHomeNo01."'  "
						." , company_name01  = '".$this->mCompanyName01."'  "
						." , building01  = '".$this->mBuilding01."'  "
						." , moono01 = '".$this->mMooNo01."'  "
						." , mooban01  = '".$this->mMooban01."'  "
						." , soi01  = '".$this->mSoi01."'  "
						." , road01  = '".$this->mRoad01."'  "
						
						." , tumbon01  = '".$this->mTumbon01."'  "
						." , amphur01  = '".$this->mAmphur01."'  "
						." , province01  = '".$this->mProvince01."'  "
						." , tumbon_code01  = '".$this->mTumbonCode01."'  "
						." , amphur_code01  = '".$this->mAmphurCode01."'  "
						." , province_code01  = '".$this->mProvinceCode01."'  "
						." , zip_code01  = '".$this->mZipCode01."'  "
						." , zip01  = '".$this->mZip01."'  "
						." , tel01  = '".$this->mTel01."'  "
						." , fax01  = '".$this->mFax01."'  "
						." , company  = '".$this->mCompany."'  "
						." , address02  = '".$this->mAddress02."'  "
						." , address021  = '".$this->mAddress021."'  "
						
						." , homeno02  = '".$this->mHomeNo02."'  "
						." , company_name02  = '".$this->mCompanyName02."'  "
						." , building02  = '".$this->mBuilding02."'  "
						." , moono02 = '".$this->mMooNo02."'  "
						." , mooban02  = '".$this->mMooban02."'  "
						." , soi02  = '".$this->mSoi02."'  "
						." , road02  = '".$this->mRoad02."'  "
						
						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "
						." , name03  = '".$this->mName03."'  "
						." , address03  = '".$this->mAddress03."'  "
						." , address031  = '".$this->mAddress031."'  "
						
						." , homeno03  = '".$this->mHomeNo03."'  "
						." , company_name03  = '".$this->mCompanyName03."'  "
						." , building03  = '".$this->mBuilding03."'  "
						." , moono03 = '".$this->mMooNo03."'  "
						." , mooban03  = '".$this->mMooban03."'  "
						." , soi03  = '".$this->mSoi03."'  "
						." , road03  = '".$this->mRoad03."'  "
						
						." , tumbon03  = '".$this->mTumbon03."'  "
						." , amphur03  = '".$this->mAmphur03."'  "
						." , province03  = '".$this->mProvince03."'  "
						." , tumbon_code03  = '".$this->mTumbonCode03."'  "
						." , amphur_code03  = '".$this->mAmphurCode03."'  "
						." , province_code03  = '".$this->mProvinceCode03."'  "
						." , zip_code03  = '".$this->mZipCode03."'  "
						." , zip03  = '".$this->mZip03."'  "
						." , tel03  = '".$this->mTel03."'  "
						." , fax03  = '".$this->mFax03."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		
		//echo $strSql;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_crl_address");
				
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateInsurance(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  title = '".$this->mTitle."'  "
						." ,  acard = '".$this->mACard."'  "
						." ,  bcard = '".$this->mBCard."'  "
						." ,  ccard = '".$this->mCCard."'  "
						." ,  icard = '".$this->mICard."'  "
						." ,  type_id = '".$this->mTypeId."'  "	
						." ,  grade_id = '".$this->mGradeId."'  "
						." ,  event_id = '".$this->mEventId."'  "
						." ,  group_id = '".$this->mGroupId."'  "	
						." ,  follow_up = '".$this->mFollowUp."'  "	
						." ,  insure_member = '".$this->mInsureMember."'  "	
						." ,  information_date = '".$this->mInformationDate."'  "	
						." ,  birthday = '".$this->mBirthday."'  "	
						." ,  customer_title_id = '".$this->mCustomerTitleId."'  "	
						." ,  firstname = '".$this->mFirstname."'  "
						." , lastname  = '".$this->mLastname."'  "
						." , email  = '".$this->mEmail."'  "
						." , id_card  = '".$this->mIDCard."'  "
						." , address  = '".$this->mAddress."'  "

						." , tumbon  = '".$this->mTumbon."'  "
						." , amphur  = '".$this->mAmphur."'  "
						." , province  = '".$this->mProvince."'  "
						." , tumbon_code  = '".$this->mTumbonCode."'  "
						." , amphur_code  = '".$this->mAmphurCode."'  "
						." , province_code  = '".$this->mProvinceCode."'  "
						." , zip_code  = '".$this->mZipCode."'  "
						." , zip  = '".$this->mZip."'  "
						." , home_tel  = '".$this->mHomeTel."'  "
						." , mobile  = '".$this->mMobile."'  "
						." , office_tel  = '".$this->mOfficeTel."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." , fax  = '".$this->mFax."'  "
						." , address01  = '".$this->mAddress01."'  "

						." , tumbon01  = '".$this->mTumbon01."'  "
						." , amphur01  = '".$this->mAmphur01."'  "
						." , province01  = '".$this->mProvince01."'  "
						." , tumbon_code01  = '".$this->mTumbonCode01."'  "
						." , amphur_code01  = '".$this->mAmphurCode01."'  "
						." , province_code01  = '".$this->mProvinceCode01."'  "
						." , zip_code01  = '".$this->mZipCode01."'  "
						." , zip01  = '".$this->mZip01."'  "
						." , tel01  = '".$this->mTel01."'  "
						." , fax01  = '".$this->mFax01."'  "
						." , company  = '".$this->mCompany."'  "
						." , address02  = '".$this->mAddress02."'  "

						." , tumbon02  = '".$this->mTumbon02."'  "
						." , amphur02  = '".$this->mAmphur02."'  "
						." , province02  = '".$this->mProvince02."'  "
						." , tumbon_code02  = '".$this->mTumbonCode02."'  "
						." , amphur_code02  = '".$this->mAmphurCode02."'  "
						." , province_code02  = '".$this->mProvinceCode02."'  "
						." , zip_code02  = '".$this->mZipCode02."'  "
						." , zip02  = '".$this->mZip02."'  "
						." , tel02  = '".$this->mTel02."'  "
						." , fax02  = '".$this->mFax02."'  "
						." , name03  = '".$this->mName03."'  "
						." , address03  = '".$this->mAddress03."'  "

						." , tumbon03  = '".$this->mTumbon03."'  "
						." , amphur03  = '".$this->mAmphur03."'  "
						." , province03  = '".$this->mProvince03."'  "
						." , tumbon_code03  = '".$this->mTumbonCode03."'  "
						." , amphur_code03  = '".$this->mAmphurCode03."'  "
						." , province_code03  = '".$this->mProvinceCode03."'  "
						." , zip_code03  = '".$this->mZipCode03."'  "
						." , zip03  = '".$this->mZip03."'  "
						." , tel03  = '".$this->mTel03."'  "
						." , fax03  = '".$this->mFax03."'  "
						
						." , name04  = '".$this->mName04."'  "
						." , address04  = '".$this->mAddress04."'  "
						." , address041  = '".$this->mAddress041."'  "
						." , tumbon04  = '".$this->mTumbon04."'  "
						." , amphur04  = '".$this->mAmphur04."'  "
						." , province04  = '".$this->mProvince04."'  "
						." , tumbon_code04  = '".$this->mTumbonCode04."'  "
						." , amphur_code04  = '".$this->mAmphurCode04."'  "
						." , province_code04  = '".$this->mProvinceCode04."'  "
						." , zip_code04  = '".$this->mZipCode04."'  "
						." , zip04  = '".$this->mZip04."'  "
						." , tel04  = '".$this->mTel04."'  "
						." , fax04  = '".$this->mFax04."'  "

						." , incomplete  = '".$this->mIncomplete."'  "
						." , mailback  = '".$this->mMailback."'  "
						." , insure_member  = '".$this->mInsureMember."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		
		//echo $strSql;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_insurance");
				
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateInsuranceFix(){
	global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET tumbon04  = '".$this->mTumbon04."'  "
						
						." , name04  = '".$this->mName04."'  "
						." , address04  = '".$this->mAddress04."'  "
						." , address041  = '".$this->mAddress041."'  "
						." , amphur04  = '".$this->mAmphur04."'  "
						." , province04  = '".$this->mProvince04."'  "
						." , tumbon_code04  = '".$this->mTumbonCode04."'  "
						." , amphur_code04  = '".$this->mAmphurCode04."'  "
						." , province_code04  = '".$this->mProvinceCode04."'  "
						." , zip_code04  = '".$this->mZipCode04."'  "
						." , zip04  = '".$this->mZip04."'  "
						." , tel04  = '".$this->mTel04."'  "
						." , fax04  = '".$this->mFax04."'  "

						." , insure_member  = '".$this->mInsureMember."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		
		//echo $strSql;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_insurance");
				
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function delete() {
	global $sMemberId;	
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE customer_id=".$this->mCustomerId." ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"delete");
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateDelete(){
	global $sMemberId;	
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_delete_status");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function updateVerifyAddress(){
	global $sMemberId;	
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET verify_address = '".$this->mVerifyAddress."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_verify_address");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		

	function updateVerifyPhone(){
	global $sMemberId;	
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET verify_phone = '".$this->mVerifyAddress."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mCustomerId,"update_verify_phone");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function updateInComplete(){
		$strSql = "UPDATE ".$this->TABLE
						." SET incomplete = '".$this->mIncomplete."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		//if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кؤӹ�˹�ҹ��";
		//if ($this->mFirstname == "") $asrErrReturn["firstname"] = "��س��кت���";
		//if ($this->mLastname == "") $asrErrReturn["lastName"] = "��س��кع��ʡ��";

		//if ($this->mAddress == "") $asrErrReturn["address"] = "��س��кط������";
		//if ($this->mZip == "") $asrErrReturn["zip"] = "��س��к�������ɳ���";
		//if ($this->mProvince == "") $asrErrReturn["province"] = "��س��к�������ɳ���";
		/*
        Switch ( strtolower($Mode) )
        {
            Case "add":
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = "Duplicate email please type again";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}
				break;	
            Case "update":
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = " Duplicate email please type again";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}
				break;	
            Case "delete":
                Break;
        }
		*/
        Return $asrErrReturn;
    }

	 Function checkCashierBooking($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }	
	
	 Function checkCashierSending($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }		
	
	 Function checkCrlBooking($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }	
	
	 Function checkCrlSending($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }			
	
    function checkUniqueEmail($strMode)
    {
        $strSql  = "SELECT customer_id FROM t_customer ";
		if ($strMode == "add"){
			$strSql .= " WHERE email = " .$this->convStr4SQL($this->mEmail);
		}else{
			$strSql .= " WHERE email = " .$this->convStr4SQL($this->mEmail)." and customer_id != ".$this->mCustomerId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Email found.
            {
				return $row->customer_id;
			}
        }
        return false;
    }

}

/*********************************************************
		Class :				CustomerList
		Last update :		25  Nov 06
		Description:		Class manage customer list
*********************************************************/

class CustomerList extends DataList {
	var $TABLE = "t_customer";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.customer_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause

	   
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  * FROM t_customer  O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Customer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadAdvanceSearch() {
		// also gets latest delivery date
        //Get Number of Users list
		$strSql = "SELECT Count(DISTINCT C.customer_id) as rowCount FROM ".$this->TABLE." C "
		//." LEFT JOIN t_customer_expect_car E ON E.customer_id = C.customer_id "		
		.$this->getFilterSQL();	// WHERE clause


		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
			$strSql = " SELECT  C.* FROM t_customer  C "
			//." LEFT JOIN t_customer_expect_car E ON E.customer_id = C.customer_id "
			.' '.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING		
		//echo $strSql;
		//exit;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Customer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadAdvanceSearchExcel() {
		//��� acard search print excel
		// also gets latest delivery date
        //Get Number of Users list
		/*
		$strSql = "SELECT Count(DISTINCT C.customer_id) as rowCount FROM ".$this->TABLE." C "
		//." LEFT JOIN t_customer_expect_car E ON E.customer_id = C.customer_id "		
		.$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		*/
			$strSql = " SELECT  C.* FROM t_customer  C "
			//." LEFT JOIN t_customer_expect_car E ON E.customer_id = C.customer_id "
			.' '.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimit();	// PAGING		
		//echo $strSql;
		//exit;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Customer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.customer_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  *  FROM t_customer  O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Customer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadUTF8AutoSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.customer_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT customer_id, firstname, lastname, address  FROM t_customer  O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Customer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		
	
}
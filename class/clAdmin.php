<?
/*********************************************************
		Class :				Admin

		Last update :		22 Mar 02

		Description:		Admin user

*********************************************************/

class Admin extends User {

	function Admin($objData = null) {
		$this->User($objData);
		$this->TABLE_USER="t_admin";
		$this->mUserType="admin";
	}

	function load() {
		$this->loadUser();
	}

	function add() {
		$this->addUser();
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				$this->setNewUserName($this->mUsername,$errUsername);
				if ($errUsername != "") $asrErrReturn[username] = $errUsername;
				$this->setNewPassword($this->mPassword1, $this->mPassword2,$errPassword);
				if ($errPassword != "") $asrErrReturn[password] = $errPassword;
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function usernameExists($username)
    {
		return $this->checkUnique($username, 'userName');
    }
    
    function emailExists($email)
    {
       return $this->checkUnique($email, 'email');
    }

}

/*********************************************************
		Class :				AdminList

		Last update :		22 Mar 02

		Description:		Admin user list

*********************************************************/


class AdminList extends UserList {
	
	function AdminList($objData=null) {
		$this->UserList($objData);
		$this->TABLE_LIST = "t_admin";
	}

	function loadMore() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT userId) as rowCount FROM ".$this->TABLE_LIST
			." ".$this->getFilterSQL();	// WHERE clause
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * "
			."FROM ".$this->TABLE_LIST." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Admin($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
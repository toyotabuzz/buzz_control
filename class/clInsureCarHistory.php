<?
/*********************************************************
		Class :					InsureCarHistory

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureCarHistory extends DB{

	var $TABLE="t_insure_car_history";

	var $m_car_history_id;
	function get_car_history_id() { return $this->m_car_history_id; }
	function set_car_history_id($data) { $this->m_car_history_id = $data; }
	
	var $m_insure_history_id;
	function get_insure_history_id() { return $this->m_insure_history_id; }
	function set_insure_history_id($data) { $this->m_insure_history_id = $data; }		
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_sale_id;
	function get_sale_id() { return $this->m_sale_id; }
	function set_sale_id($data) { $this->m_sale_id = $data; }
	
	var $m_team;
	function get_team() { return $this->m_team; }
	function set_team($data) { $this->m_team = $data; }	
	
	var $m_team_id;
	function get_team_id() { return $this->m_team_id; }
	function set_team_id($data) { $this->m_team_id = $data; }	
	
	//acard, booking, ccard
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }

	var $m_source;
	function get_source() { return $this->m_source; }
	function set_source($data) { $this->m_source = $data; }
	
	var $m_insure_customer_id;
	function get_insure_customer_id() { return $this->m_insure_customer_id; }
	function set_insure_customer_id($data) { $this->m_insure_customer_id = $data; }	
	
	var $m_customer_id;
	function get_customer_id() { return $this->m_customer_id; }
	function set_customer_id($data) { $this->m_customer_id = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }

	var $m_car_type;
	function get_car_type() { return $this->m_car_type; }
	function set_car_type($data) { $this->m_car_type = $data; }
	
	var $m_car_model_id;
	function get_car_model_id() { return $this->m_car_model_id; }
	function set_car_model_id($data) { $this->m_car_model_id = $data; }
	
	var $m_car_series_id;
	function get_car_series_id() { return $this->m_car_series_id; }
	function set_car_series_id($data) { $this->m_car_series_id = $data; }
	
	var $m_color;
	function get_color() { return $this->m_color; }
	function set_color($data) { $this->m_color = $data; }
	
	var $m_car_number;
	function get_car_number() { return $this->m_car_number; }
	function set_car_number($data) { $this->m_car_number = $data; }
	
	var $m_engine_number;
	function get_engine_number() { return $this->m_engine_number; }
	function set_engine_number($data) { $this->m_engine_number = $data; }
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }	
	
	var $m_register_year;
	function get_register_year() { return $this->m_register_year; }
	function set_register_year($data) { $this->m_register_year = $data; }
	
	var $m_date_verify;
	function get_date_verify() { return $this->m_date_verify; }
	function set_date_verify($data) { $this->m_date_verify = $data; }	
	
	var $m_suggest_from;
	function get_suggest_from() { return $this->m_suggest_from; }
	function set_suggest_from($data) { $this->m_suggest_from = $data; }
	
	var $m_call_date;
	function get_call_date() { return $this->m_call_date; }
	function set_call_date($data) { $this->m_call_date = $data; }
	
	var $m_call_remark;
	function get_call_remark() { return $this->m_call_remark; }
	function set_call_remark($data) { $this->m_call_remark = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }

	var $m_order_id;
	function get_order_id() { return $this->m_order_id; }
	function set_order_id($data) { $this->m_order_id = $data; }
	
	//�����ŷ�����١��
	var $m_junk;
	function get_junk() { return $this->m_junk; }
	function set_junk($data) { $this->m_junk = $data; }	
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }	
	
	var $m_mail_date;
	function get_mail_date() { return $this->m_mail_date; }
	function set_mail_date($data) { $this->m_mail_date = $data; }	
	
	//ʶҹС�÷ӧҹ�ͧ acard ��� �մѧ���  1. ready 2. mail  3.call1  4. quotation 5. call2  6. booking 7. ccard  8.reject 
	var $m_operate;
	function get_operate() { return $this->m_operate; }
	function set_operate($data) { $this->m_operate = $data; }		
	
	var $m_operate_prb;
	function get_operate_prb() { return $this->m_operate_prb; }
	function set_operate_prb($data) { $this->m_operate_prb = $data; }		
	
	var $m_buy_type;
	function get_buy_type() { return $this->m_buy_type; }
	function set_buy_type($data) { $this->m_buy_type = $data; }		
	
	var $m_buy_company;
	function get_buy_company() { return $this->m_buy_company; }
	function set_buy_company($data) { $this->m_buy_company = $data; }		

	var $m_registry_date;
	function get_registry_date() { return $this->m_registry_date; }
	function set_registry_date($data) { $this->m_registry_date = $data; }		
	
	var $m_registry_tax_year;
	function get_registry_tax_year() { return $this->m_registry_tax_year; }
	function set_registry_tax_year($data) { $this->m_registry_tax_year = $data; }		
	
	var $m_registry_tax_year_number;
	function get_registry_tax_year_number() { return $this->m_registry_tax_year_number; }
	function set_registry_tax_year_number($data) { $this->m_registry_tax_year_number = $data; }		
	
	var $m_get_car_date;
	function get_get_car_date() { return $this->m_get_car_date; }
	function set_get_car_date($data) { $this->m_get_car_date = $data; }		
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }			
	
	//�ԵԺؤ��= 1, �ؤ�Ÿ����� 0, �ԵԺؤ�� ����ѡ���� � ������ = 3
	var $m_niti;
	function get_niti() { return $this->m_niti; }
	function set_niti($data) { $this->m_niti = $data; }			

	//����������ó� 0,    �������������ó� 1
	var $m_info_type;
	function get_info_type() { return $this->m_info_type; }
	function set_info_type($data) { $this->m_info_type = $data; }			

	var $m_checkc;
	function get_checkc() { return $this->m_checkc; }
	function set_checkc($data) { $this->m_checkc = $data; }			
	
	var $m_provinde_code;
	function get_province_code() { return $this->m_province_code; }
	function set_province_code($data) { $this->m_province_code = $data; }		

	var $m_insure_acard_id;
	function get_insure_acard_id() { return $this->m_insure_acard_id; }
	function set_insure_acard_id($data) { $this->m_insure_acard_id = $data; }			

	var $m_recent_insure_id;
	function get_recent_insure_id() { return $this->m_recent_insure_id; }
	function set_recent_insure_id($data) { $this->m_recent_insure_id = $data; }				
	
	var $m_import_date;
	function get_import_date() { return $this->m_import_date; }
	function set_import_date($data) { $this->m_import_date = $data; }				
	
	var $m_lock_sale;
	function get_lock_sale() { return $this->m_lock_sale; }
	function set_lock_sale($data) { $this->m_lock_sale = $data; }		
	
	var $m_admin_share;
	function get_admin_share() { return $this->m_admin_share; }
	function set_admin_share($data) { $this->m_admin_share = $data; }			
	
	var $m_sup_share;
	function get_sup_share() { return $this->m_sup_share; }
	function set_sup_share($data) { $this->m_sup_share = $data; }			
	
	var $m_fix_sale;
	function get_fix_sale() { return $this->m_fix_sale; }
	function set_fix_sale($data) { $this->m_fix_sale = $data; }			
	
	var $m_date_verify_prb;
	function get_date_verify_prb() { return $this->m_date_verify_prb; }
	function set_date_verify_prb($data) { $this->m_date_verify_prb = $data; }			
	
	var $m_emotion;
	function get_emotion() { return $this->m_emotion; }
	function set_emotion($data) { $this->m_emotion = $data; }			
	
	var $m_duplicated;
	function get_duplicated() { return $this->m_duplicated; }
	function set_duplicated($data) { $this->m_duplicated = $data; }			
	
	var $m_first_company_id;
	function get_first_company_id() { return $this->m_first_company_id; }
	function set_first_company_id($data) { $this->m_first_company_id = $data; }			
	
	var $m_first_team_id;
	function get_first_team_id() { return $this->m_first_team_id; }
	function set_first_team_id($data) { $this->m_first_team_id = $data; }			
	
	var $m_first_sale_id;
	function get_first_sale_id() { return $this->m_first_sale_id; }
	function set_first_sale_id($data) { $this->m_first_sale_id = $data; }
	
	var $m_temp_sale_id;
	function get_temp_sale_id() { return $this->m_temp_sale_id; }
	function set_temp_sale_id($data) { $this->m_temp_sale_id = $data; }	
	
	var $m_temp_remark;
	function get_temp_remark() { return $this->m_temp_remark; }
	function set_temp_remark($data) { $this->m_temp_remark = $data; }
	
	var $m_approve;
	function get_approve() { return $this->m_approve; }
	function set_approve($data) { $this->m_approve = $data; }
	
	var $m_approve_date;
	function get_approve_date() { return $this->m_approve_date; }
	function set_approve_date($data) { $this->m_approve_date = $data; }
	
	var $m_approve_by;
	function get_approve_by() { return $this->m_approve_by; }
	function set_approve_by($data) { $this->m_approve_by = $data; }
	
	
	function InsureCarHistory($objData=NULL) {
        If ($objData->car_history_id !="") {
		
			$this->set_car_history_id($objData->car_history_id);
			$this->set_insure_history_id($objData->insure_history_id);
			$this->set_car_id($objData->car_id);
			$this->set_sale_id($objData->sale_id);
			$this->set_team($objData->team);
			$this->set_team_id($objData->team_id);
			$this->set_source($objData->source);
			$this->set_car_type($objData->car_type);
			$this->set_insure_customer_id($objData->insure_customer_id);
			$this->set_customer_id($objData->customer_id);
			$this->set_code($objData->code);
			$this->set_car_model_id($objData->car_model_id);
			$this->set_car_series_id($objData->car_series_id);
			$this->set_color($objData->color);
			$this->set_price($objData->price);
			$this->set_car_number($objData->car_number);
			$this->set_engine_number($objData->engine_number);
			$this->set_register_year($objData->register_year);
			$this->set_date_verify($objData->date_verify);
			$this->set_suggest_from($objData->suggest_from);
			$this->set_call_date($objData->call_date);
			$this->set_call_remark($objData->call_remark);
			$this->set_date_add($objData->date_add);
			$this->set_order_id($objData->order_id);
			$this->set_junk($objData->junk);			
			$this->set_company_id($objData->company_id);
			$this->set_mail_date($objData->mail_date);
			$this->set_operate($objData->operate);
			$this->set_buy_type($objData->buy_type);
			$this->set_buy_company($objData->buy_company);
			$this->set_registry_date($objData->registry_date);
			$this->set_registry_tax_year($objData->registry_tax_year);
			$this->set_registry_tax_year_number($objData->registry_tax_year_number);
			$this->set_get_car_date($objData->get_car_date);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_niti($objData->niti);
			$this->set_info_type($objData->info_type);
			$this->set_checkc($objData->checkc);
			$this->set_province_code($objData->province_code);
			$this->set_insure_acard_id($objData->insure_acard_id);
			$this->set_recent_insure_id($objData->recent_insure_id);
			$this->set_import_date($objData->import_date);
			$this->set_lock_sale($objData->lock_sale);
			$this->set_admin_share($objData->admin_share);
			$this->set_sup_share($objData->sup_share);
			$this->set_fix_sale($objData->fix_sale);
			$this->set_date_verify_prb($objData->date_verify_prb);
			$this->set_operate_prb($objData->operate_prb);
			$this->set_emotion($objData->emotion);
			$this->set_duplicated($objData->duplicated);
			$this->set_first_company_id($objData->first_company_id);
			$this->set_first_sale_id($objData->first_sale_id);
			$this->set_first_team_id($objData->first_team_id);
			$this->set_temp_sale_id($objData->temp_sale_id);
			$this->set_temp_remark($objData->temp_remark);
			$this->set_approve($objData->approve);
			$this->set_approve_date($objData->approve_date);
			$this->set_approve_by($objData->approve_by);
			//echo "dd".$this->get_insure_company_id();
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_car_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_id =".$this->m_car_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCarHistory($row);
                $result->freeResult();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}

	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCarHistory($row);
      		    $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_history_id, car_id, sale_id, team, team_id, company_id, source, car_type, insure_customer_id, customer_id, code, car_model_id, car_series_id, color, price, car_number, engine_number, register_year,
		 date_verify,  date_verify_prb, suggest_from, call_date, call_remark, date_add , order_id , buy_type, buy_company, registry_date
		 , registry_tax_year, registry_tax_year_number, insure_company_id, get_car_date, niti , checkc, info_type,province_code, import_date, insure_acard_id, temp_sale_id, temp_remark ) " ." VALUES ( "

		 ." '".$this->m_insure_history_id."' , "
		 ." '".$this->m_car_id."' , "
		." '".$this->m_sale_id."' , "
		." '".$this->m_team."' , "
		." '".$this->m_team_id."' , "
		." '".$this->m_company_id."' , "
		." '".addslashes($this->m_source)."' , "
		." '".$this->m_car_type."' , "
		." '".$this->m_insure_customer_id."' , "
		." '".$this->m_customer_id."' , "
		." '".addslashes($this->m_code)."' , "
		." '".$this->m_car_model_id."' , "
		." '".$this->m_car_series_id."' , "
		." '".$this->m_color."' , "
		." '".$this->m_price."' , "
		." '".addslashes($this->m_car_number)."' , "
		." '".addslashes($this->m_engine_number)."' , "
		." '".$this->m_register_year."' , "
		." '".$this->m_date_verify."' , "	
		." '".$this->m_date_verify_prb."' , "	
		." '".addslashes($this->m_suggest_from)."' , "
		." '".$this->m_call_date."' , "
		." '".addslashes($this->m_call_remark)."' , "
		." '".$this->m_date_add."' , "
		." '".$this->m_order_id."',  "
		." '".$this->m_buy_type."' , "
		." '".$this->m_buy_company."' ,  "
		." '".$this->m_registry_date."' ,  "
		." '".$this->m_registry_tax_year."' , "
		." '".$this->m_registry_tax_year_number."' , "
		." '".$this->m_insure_company_id."' ,  "
		." '".$this->m_get_car_date."' ,  "
		." '".$this->m_niti."' , "
		." '".$this->m_checkc."' , "
		." '".$this->m_info_type."' , "
		." '".$this->m_province_code."' , "
		." '".$this->m_import_date."' , "
		." '".$this->m_insure_acard_id."' , "
		." '".$this->m_temp_sale_id."' , "
		." '".$this->m_temp_remark."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_car_id = mysql_insert_id();
			$this->unsetConnection();
            return $this->m_car_id;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}


	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				InsureCarHistory Company List

		Last update :		22 Mar 02

		Description:		InsureCarHistory Company List

*********************************************************/

class InsureCarHistoryList extends DataList {
	var $TABLE = "t_insure_car_history";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_id) as rowCount FROM ".$this->TABLE
			." IC  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
		$strSql = " SELECT * FROM ".$this->TABLE." IC "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCarHistory($row);
			}
			//$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		.$this->getFilterSQL();	// WHERE clause
		
		//echo $strSql;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCarHistory($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadImport() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL();	// WHERE clause
		
		//echo $strSql;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCarHistory($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
<?
/*********************************************************
		Class :				Postcode

		Last update :		16 NOV 02

		PostCode:		Class manage postcode table

*********************************************************/

class Postcode extends DB{

	var $TABLE="t_area_postcode";
	
	var $mPostId;
	function getPostId() { return $this->mPostId; }
	function setPostId($data) { $this->mPostId = $data; }
	
	var $mPostCode;
	function getPostCode() { return htmlspecialchars($this->mPostCode); }
	function setPostCode($data) { $this->mPostCode = $data; }

	var $mPostName;
	function getPostName() { return htmlspecialchars($this->mPostName); }
	function setPostName($data) { $this->mPostName = $data; }

	var $mAmphurCode;
	function getAmphurCode() { return htmlspecialchars($this->mAmphurCode); }
	function setAmphurCode($data) { $this->mAmphurCode = $data; }

	var $mAmphurName;
	function getAmphurName() { return htmlspecialchars($this->mAmphurName); }
	function setAmphurName($data) { $this->mAmphurName = $data; }	
	
	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }

	var $mProvinceName;
	function getProvinceName() { return htmlspecialchars($this->mProvinceName); }
	function setProvinceName($data) { $this->mProvinceName = $data; }

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	
	
	function Postcode($objData=NULL) {
        If ($objData->post_id!="") {
            $this->setPostId($objData->post_id);
			$this->setPostCode($objData->post_code);
            $this->setPostName($objData->post_name);
			$this->setAmphurCode($objData->amphur_code);
            $this->setAmphurName($objData->amphur_name);			
			$this->setProvinceCode($objData->province_code);
			$this->setProvinceName($objData->province_name);
			$this->setRank($objData->rank);
        }
    }
	
	function init(){
		$this->setPostName(stripslashes($this->mPostName));
	}
	
	function load() {

		if ($this->mPostId == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE C.post_id =".$this->mPostId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Postcode($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( post_name, amphur_code, amphur_name, province_code, post_code, province_name, rank ) "
						." VALUES ( '".$this->mPostName."' ,"
						." '".$this->mAmphurCode."' , "
						." '".$this->mAmphurName."' , "
						." '".$this->mProvinceCode."' , "
						." '".$this->mPostCode."' , "
						." '".$this->mProviceName."' , "
						." '".$this->mRank."' ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPostId = mysql_insert_id();
            return $this->mPostId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET post_name = '".$this->mPostName."' , "
						." amphur_code = '".$this->mAmphurCode."' ,  "
						." amphur_name = '".$this->mAmphurName."' ,  "
						." province_code = '".$this->mProvinceCode."' ,  "
						." post_code = '".$this->mPostCode."' ,  "
						." province_name = '".$this->mProvinceName."' ,  "
						." rank = '".$this->mRank."'   "
						." WHERE  post_id = ".$this->mPostId."  ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE post_id=".$this->mPostId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				//if ($this->mPostName == "") $asrErrReturn["post_name"] = "�Please enter postcode name";
				
				if ($this->mPostCode == ""){
					$asrErrReturn["post_code"] = "��س��к�������ɳ���";
				}else{
					if(!$this->checkUnique($Mode,"post_code","post_id",$TABLE,$this->mPostId,$this->mPostCode)){
						$asrErrReturn["post_code"] = '������ɳ����ӫ�͹';
					}
				}
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	

   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	

}

/*********************************************************
		Class :				PostcodeList

		Last update :		22 Mar 02

		PostCode:		Postcode user list

*********************************************************/


class PostcodeList extends DataList {
	var $TABLE = "t_area_postcode";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT post_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Postcode($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT post_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Postcode($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		if ($header == "search") {echo ("<option value=\"0\">- any postcode -</option>");};
		if ($header == "Add" || $header == "Update" || $header == "") {echo ("<option value=\"0\">- select postcode -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPostId()."\"");
			if (($defaultId != null) && ($objItem->getPostId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getPostName()."</option>");
		}
		echo("</select>");
	}

}
<?
/*********************************************************
		Class :					Insure Company

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_company table

*********************************************************/
 
class InsureCompany extends DB{

	var $TABLE="t_insure_company";

	var $mInsureCompanyId;
	function getInsureCompanyId() { return $this->mInsureCompanyId; }
	function setInsureCompanyId($data) { $this->mInsureCompanyId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }
	
	var $mShortTitle;
	function getShortTitle() { return htmlspecialchars($this->mShortTitle); }
	function setShortTitle($data) { $this->mShortTitle = $data; }
	
	var $mContactName;
	function getContactName() { return htmlspecialchars($this->mContactName); }
	function setContactName($data) { $this->mContactName = $data; }
	
	var $mContactTel;
	function getContactTel() { return htmlspecialchars($this->mContactTel); }
	function setContactTel($data) { $this->mContactTel = $data; }
	
	var $mContactFax;
	function getContactFax() { return htmlspecialchars($this->mContactFax); }
	function setContactFax($data) { $this->mContactFax = $data; }
	
	var $mContactCode;
	function getContactCode() { return htmlspecialchars($this->mContactCode); }
	function setContactCode($data) { $this->mContactCode = $data; }
	
	var $mNum1;
	function getNum1() { return htmlspecialchars($this->mNum1); }
	function setNum1($data) { $this->mNum1 = $data; }
	
	var $mNum2;
	function getNum2() { return htmlspecialchars($this->mNum2); }
	function setNum2($data) { $this->mNum2 = $data; }
	
	var $mNum3;
	function getNum3() { return htmlspecialchars($this->mNum3); }
	function setNum3($data) { $this->mNum3 = $data; }

	var $mNum4;
	function getNum4() { return htmlspecialchars($this->mNum4); }
	function setNum4($data) { $this->mNum4 = $data; }
	
	var $mNum5;
	function getNum5() { return htmlspecialchars($this->mNum5); }
	function setNum5($data) { $this->mNum5 = $data; }
	
	var $mNum6;
	function getNum6() { return htmlspecialchars($this->mNum6); }
	function setNum6($data) { $this->mNum6 = $data; }
	
	var $mNum7;
	function getNum7() { return htmlspecialchars($this->mNum7); }
	function setNum7($data) { $this->mNum7 = $data; }
	
	var $mNum8;
	function getNum8() { return htmlspecialchars($this->mNum8); }
	function setNum8($data) { $this->mNum8 = $data; }
	
	var $mNum9;
	function getNum9() { return htmlspecialchars($this->mNum9); }
	function setNum9($data) { $this->mNum9 = $data; }
	
	var $mNum10;
	function getNum10() { return htmlspecialchars($this->mNum10); }
	function setNum10($data) { $this->mNum10 = $data; }
	
	var $mNum11;
	function getNum11() { return htmlspecialchars($this->mNum11); }
	function setNum11($data) { $this->mNum11 = $data; }
	
	var $mNum12;
	function getNum12() { return htmlspecialchars($this->mNum12); }
	function setNum12($data) { $this->mNum12 = $data; }
	
	var $mNum13;
	function getNum13() { return htmlspecialchars($this->mNum13); }
	function setNum13($data) { $this->mNum13 = $data; }
	
	var $mTitle01;
	function getTitle01() { return htmlspecialchars($this->mTitle01); }
	function setTitle01($data) { $this->mTitle01 = $data; }	
	
	var $mTitle02;
	function getTitle02() { return htmlspecialchars($this->mTitle02); }
	function setTitle02($data) { $this->mTitle02 = $data; }	

	var $mTitle03;
	function getTitle03() { return htmlspecialchars($this->mTitle03); }
	function setTitle03($data) { $this->mTitle03 = $data; }	

	var $mTitle04;
	function getTitle04() { return htmlspecialchars($this->mTitle04); }
	function setTitle04($data) { $this->mTitle04 = $data; }	
	//�������˹�� winspeed
	var $mVendor;
	function getVendor() { return htmlspecialchars($this->mVendor); }
	function setVendor($data) { $this->mVendor = $data; }	
	//part_no �ú winspeeed
	var $mPartNo;
	function getPartNo() { return htmlspecialchars($this->mPartNo); }
	function setPartNo($data) { $this->mPartNo = $data; }	
	//part_no ��Сѹ��� winspeed
	var $mPartNo1;
	function getPartNo1() { return htmlspecialchars($this->mPartNo1); }
	function setPartNo1($data) { $this->mPartNo1 = $data; }	
	
	function InsureCompany($objData=NULL) {
        If ($objData->insure_company_id !="") {
            $this->setInsureCompanyId($objData->insure_company_id);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
			$this->setShortTitle($objData->short_title);
			$this->setContactName($objData->contact_name);
			$this->setContactTel($objData->contact_tel);
			$this->setContactFax($objData->contact_fax);
			$this->setContactCode($objData->contact_code);
			$this->setNum1($objData->num1);
			$this->setNum2($objData->num2);
			$this->setNum3($objData->num3);
			$this->setNum4($objData->num4);
			$this->setNum5($objData->num5);
			$this->setNum6($objData->num6);
			$this->setNum7($objData->num7);
			$this->setNum8($objData->num8);
			$this->setNum9($objData->num9);
			$this->setNum10($objData->num10);
			$this->setNum11($objData->num11);
			$this->setNum12($objData->num12);
			$this->setNum13($objData->num13);
			$this->setTitle01($objData->title01);
			$this->setTitle02($objData->title02);
			$this->setTitle03($objData->title03);
			$this->setTitle04($objData->title04);
			$this->setVendor($objData->vendor);
			$this->setPartNo($objData->part_no);
			$this->setPartNo1($objData->part_no1);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mInsureCompanyId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_company_id =".$this->mInsureCompanyId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCompany($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCompany($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, short_title, rank, contact_name, contact_tel, contact_fax, title01,title02, title03, title04,  num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13, vendor, part_no, part_no1, created_at, created_by ) "
						." VALUES ( '".$this->mTitle."', '"
						.$this->mShortTitle."' , '"
						.$this->mRank."' , '"
						.$this->mContactName."' , '"
						.$this->mContactTel."' , '"
						.$this->mContactFax."' , '"
						
						.$this->mTitle01."' , '"
						.$this->mTitle02."' , '"
						.$this->mTitle03."' , '"
						.$this->mTitle04."' , '"
						
						.$this->mNum1."' , '"
						.$this->mNum2."' , '"
						.$this->mNum3."' , '"
						.$this->mNum4."' , '"
						.$this->mNum5."' , '"
						.$this->mNum6."' , '"
						.$this->mNum7."' , '"
						.$this->mNum8."' , '"
						.$this->mNum9."' , '"
						.$this->mNum10."' , '"
						.$this->mNum11."' , '"
						.$this->mNum12."' , '"
						.$this->mNum13."' , '"
						.$this->mVendor."' , '"
						.$this->mPartNo."' , '"
						.$this->mPartNo1."' , "
						." NOW(), "
						." '".$_SESSION['sMemberId']."' "
						." ) ";
						
						
						

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mInsureCompanyId = mysql_insert_id();
            return $this->mInsureCompanyId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." rank = '".$this->mRank."' , "
						." short_title = '".$this->mShortTitle."'  "
						." WHERE  insure_company_id = ".$this->mInsureCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function update_insure(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." short_title = '".$this->mShortTitle."' , "
						." contact_name = '".$this->mContactName."' , "
						." contact_tel = '".$this->mContactTel."' , "
						." contact_fax = '".$this->mContactFax."' , "
						." contact_code = '".$this->mContactCode."' , "
						
						." title01 = '".$this->mTitle01."' , "
						." title02 = '".$this->mTitle02."' , "
						." title03 = '".$this->mTitle03."' , "
						." title04 = '".$this->mTitle04."' , "
						
						." num1 = '".$this->mNum1."' , "
						." num2 = '".$this->mNum2."' , "
						." num3 = '".$this->mNum3."' , "
						." num4 = '".$this->mNum4."' , "
						." num5 = '".$this->mNum5."' , "
						." num6 = '".$this->mNum6."' , "
						." num7 = '".$this->mNum7."' , "
						." num8 = '".$this->mNum8."' , "
						." num9 = '".$this->mNum9."' , "
						." num10 = '".$this->mNum10."' , "
						." num11= '".$this->mNum11."' , "
						." num12 = '".$this->mNum12."' , "
						." num13 = '".$this->mNum13."'  "
						." WHERE  insure_company_id = ".$this->mInsureCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	
	
	function update_insure_acc(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." short_title = '".$this->mShortTitle."' , "

						." vendor = '".$this->mVendor."' , "
						." part_no = '".$this->mPartNo."' , "
						." part_no1 = '".$this->mPartNo1."'  "
						
						." WHERE  insure_company_id = ".$this->mInsureCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_company_id=".$this->mInsureCompanyId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureCompanyList extends DataList {
	var $TABLE = "t_insure_company";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_company_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCompany($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_company_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		

		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCompany($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInsureCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getInsureCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInsureCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getInsureCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}		
	
	function printSelectId($name, $defaultId = null ,$header = null, $id=null) {
		echo ("<select class=\"field\" Name=\"".$name."\"  id=\"".$id."\"  >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInsureCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getInsureCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
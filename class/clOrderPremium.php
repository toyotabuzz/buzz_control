<?
/*********************************************************
		Class :					Order Premium

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_premium table

*********************************************************/
 
class OrderPremium extends DB{

	var $TABLE="t_order_premium";

	var $mOrderPremiumId;
	function getOrderPremiumId() { return $this->mOrderPremiumId; }
	function setOrderPremiumId($data) { $this->mOrderPremiumId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }

	var $mPremiumId;
	function getPremiumId() { return $this->mPremiumId; }
	function setPremiumId($data) { $this->mPremiumId = $data; }

	var $mCustomerId;
	function getCustomerId() { return htmlspecialchars($this->mCustomerId); }
	function setCustomerId($data) { $this->mCustomerId = $data; }
	
	function OrderPremium($objData=NULL) {
        If ($objData->order_premium_id !="") {
            $this->setOrderPremiumId($objData->order_premium_id);
			$this->setPremiumId($objData->premium_id);
			$this->setOrderId($objData->order_id);
			$this->setCustomerId($objData->customer_id);
        }
    }

	function init(){
		$this->setCustomerId(stripslashes($this->mCustomerId));
	}
		
	function load() {

		if ($this->mOrderPremiumId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE order_premium_id =".$this->mOrderPremiumId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPremium($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( customer_id, premium_id, order_id ) "
						." VALUES ( '".$this->mCustomerId."','".$this->mPremiumId."','".$this->mOrderId."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderPremiumId = mysql_insert_id();
            return $this->mOrderPremiumId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET customer_id = '".$this->mCustomerId."'  "
						." , premium_id = '".$this->mPremiumId."'  "
						." , order_id = '".$this->mOrderId."'  "
						." WHERE  order_premium_id = ".$this->mOrderPremiumId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_premium_id=".$this->mOrderPremiumId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCustomerId == "") $asrErrReturn["customer_id"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Premium List

		Last update :		22 Mar 02

		Description:		Order Premium List

*********************************************************/

class OrderPremiumList extends DataList {
	var $TABLE = "t_order_premium";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_premium_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderPremium($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderPremiumId()."\"");
			if (($defaultId != null) && ($objItem->getOrderPremiumId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCustomerId()."</option>");
		}
		echo("</select>");
	}

}
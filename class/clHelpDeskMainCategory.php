<?
/*********************************************************
		Class :					Car Type

		Last update :	  10 Jan 02

		Description:	  Class manage t_main_category table

*********************************************************/
 
class HelpDeskMainCategory extends DB{

	var $TABLE="t_main_category";

	var $mHelpDeskMainCategoryId;
	function getHelpDeskMainCategoryId() { return $this->mHelpDeskMainCategoryId; }
	function setHelpDeskMainCategoryId($data) { $this->mHelpDeskMainCategoryId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	function HelpDeskMainCategory($objData=NULL) {
        If ($objData->main_category_id !="") {
            $this->setHelpDeskMainCategoryId($objData->main_category_id);
			$this->setTitle($objData->title);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mHelpDeskMainCategoryId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE main_category_id =".$this->mHelpDeskMainCategoryId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->HelpDeskMainCategory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->HelpDeskMainCategory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ) "
						." VALUES ( '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mHelpDeskMainCategoryId = mysql_insert_id();
            return $this->mHelpDeskMainCategoryId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." WHERE  main_category_id = ".$this->mHelpDeskMainCategoryId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE main_category_id=".$this->mHelpDeskMainCategoryId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��ͻ�����ö";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Type List

		Last update :		22 Mar 02

		Description:		Car Type List

*********************************************************/

class HelpDeskMainCategoryList extends DataList {
	var $TABLE = "t_main_category";

	function load() {
		//Get Number of Users list
        $strSql = "SELECT Count(DISTINCT main_category_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
		//echo "ddddd";
	    //echo $strSql;
		//exit;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
		//exit;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new HelpDeskMainCategory($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getHelpDeskMainCategoryId()."\"");
			if (($defaultId != null) && ($objItem->getHelpDeskMainCategoryId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getHelpDeskMainCategoryId()."\"");
			if (($defaultId != null) && ($objItem->getHelpDeskMainCategoryId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
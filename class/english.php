<?
/*********************************************************
	LANGUAGE CONFIGURATION FILE : ENGLISH
*********************************************************/

	$langUserError = Array (
        "strUsernameEmpty"     => "Please enter username",
        "strUsernameLength"    => "Please enter username from ".USERNAME_MIN." to ".USERNAME_MAX." character",
        "strUsernameExisting"  => "Username existing",
        "strTitleEmpty"          => "Please enter title",
		
        "strPWDEmpty"          => "Please enter password",
        "strPWDLength"         => "Please enter password from ".PASSWORD_MIN." to ".PASSWORD_MAX." character",
        "strPWDVerify"         => "Entrez a nouveau le mot de passe",

        "strCompanyEmpty"        => "Entrez le nom de la societe.",
		            
        "strEmailEmpty"        => "Please enter email",
        "strEmailInvalidFormat"=> " Invalid email format",
        "strEmailExisting"  => "Email existing",
            
        "strFirstNameEmpty"    => "Entrez votre prenom.",
        "strLastNameEmpty"     => "Entrez votre nom de famille.",
            
        "strDobInvalidFormat"  => "Entrez votre date de naissance.",

        //  ***** Authentication Error Message *****
        "authUnspecified"       => "Non specifie",
        "authUsernameEmpty"     => "��س��к� Username",
        "authUsernameNotExist"  => "��辺������سҡ�͡����",
        "authInvalidPassword"  => "Password ���١��ͧ ��سҡ�͡����",
        "authAccountDisabled"   => "�����١¡��ԡʶҹЪ��Ǥ��ǡ�سҵԴ��ͼ������к�",
		
		"strCodeEmpty" => "Please enter code."
		
	);

	$langDeliveryError = array (
		"clientSelect" => "Choisissez un client",
		"codeEnter" => "Entrez le code de livraison"
	);

	$langPhoneError = array (
		"number" => "Please enter number"
	);

?>
<?
class DataList extends DB {

	var $mSort;
	function getSort() {
		return $this->mSort;
	}
	function getSortSQL($default = '') {
		return $this->concatSQL($this->mSort,$default,',','ORDER BY');
	}
	function setSort($data) {
		if (!empty($data)) {
			$this->mSort = $data;
		}
	}
	function setSortDefault($data) {
		$this->mSort = $data;
	}
	function concatSort($data) {
		$this->concatSQL($this->mSort, $data,',');
	}
	var $mFilter;
	function getFilter() {
		return $this->mFilter;
	}
	function getFilterSQL($default = '') {
		return $this->concatSQL($this->mFilter,$default,'AND','WHERE');
	}
	function setFilter($data) {
		$this->mFilter = $data;
	}
	function setFilterDefault($data) {
		$this->mSort = $data;
	}
	function concatFilter($data, $separator = 'AND') { // AND by default, can be OR
		$this->mFilter = $this->concatSQL($this->mFilter, $data, $separator);
	}
	var $mPageSize;
	function getPageSize() {
		return $this->mPageSize;
	}
	function setPageSize($data) {
		$this->mPageSize = $data;
	}
	var $mPage;
	function getPage() {
		return $this->mPage;
	}
	function setPage($data) {
		$this->mPage = $data;
	}
	
	var $mLimit;
	function getLimit() {
		return $this->mLimit;
	}
	function setLimit($data) {
		$this->mLimit = $data;
	}	
		
	var $mCount;
	function getCount() {
		return $this->mCount;
	}

	var $mItemList;
	function getItemList() {
		return $this->mItemList;
	}

	function DataList() {
		$this->mCount = 0;
		$this->mPageSize = DEF_PAGING;
		$this->mPage = 0;
		$this->mItemList = array();
	}

	function getLimitSQL() {
		//echo ("pageSize=".$this->mPageSize."<br>");
		if (!$this->mPageSize) {
			return false; // no paging
		}
		if ($this->mPage == 0) {
			$this->mPage = 1;
		}
		while ((($this->mPage - 1) * $this->mPageSize) > $this->mCount) {
			$this->mPage--;
		}
		return "LIMIT ".($this->mPage - 1) * $this->mPageSize.", ".$this->mPageSize;
	}
	
	function printSort($link, $param, $field, $imgAsc = DEF_SORT_ASC, $imgAscActive = DEF_SORT_ASC_ACTIVE, $imgDesc = DEF_SORT_DESC, $imgDescActive = DEF_SORT_DESC_ACTIVE) {

		$begin = "<a href=\"".$link;
		if (ereg('\?',$link)) {
			$begin .= "&";
		} else {
			$begin .= "?";
		}
		$begin.= $param."=";
		$end = "\"";
		if (!empty($class)) {
			$end .= " class=\"".$class."\"";
		}
		$end .= ">";
		
		if ($this->mSort == $field." ASC") {
			echo("<img src=".$imgDescActive.">");
		} else {
			echo($begin.$field."%20ASC".$end."<img src=".$imgDesc." border=\"0\"></a>");
		}
		if ($this->mSort == $field." DESC") {
			echo("<img src=".$imgAscActive.">");
		} else {
			echo($begin.$field."%20DESC".$end."<img src=".$imgAsc." border=\"0\"></a>");
		}
	}

	function printPaging($link, $param, $class = "", $classCurrent = "") {
		// link = page filename + original parameters
		// param = name of variable parameter for paging
		// current = current page (integer)
		// class = style for link
		// classCurrent = style for current
		$max = 0;
		while (($max * $this->mPageSize) < $this->mCount) {
			$max++;
		}
		if ($max == 0) {
			return false;
		} else {
			$begin = "<a href=\"".$link;
			if (ereg('\?',$link)) {
				$begin .= "&";
			} else {
				$begin .= "?";
			}
			$begin.= $param."=";
			$end = "\"";
			if (!empty($class)) {
				$end .= " class=\"".$class."\"";
			}
			$end .= ">";
			$first = true;
			for ($i=1; $i<=$max; $i++) {
				if ($first) {
					$first = false;
				} else {
					echo(" | ");
				}
				if ($this->mPage == $i) {
					if (!empty($classCurrent)) {
						if ($classCurrent == "<b>") {
							echo ("<b>".$i."</b>");
						} else {
							echo("<span class=\"".$classCurrent."\">".$i."</span>");
						}
					} else {
						echo($i);
					}
				} else {
					echo $begin.$i.$end.$i."</a>";
				}
			}
		}
	}
	
	function printPagingCount($link, $param, $class = "", $classCurrent = "", $showCount="10") {
		// link = page filename + original parameters
		// param = name of variable parameter for paging
		// current = current page (integer)
		// class = style for link
		// classCurrent = style for current
		$nCount = $this->mCount;
		$totalPage = ceil($nCount/$this->mPageSize);
		$current = $this->mPage;
		
		$showCount = floor($showCount/2);
		
		//echo $showCount;
		
		if( ($current - $showCount)  > 0 ){
			$min = $current - $showCount;
		}else{
			$min = 1;
		}
		
		if( ($current + $showCount) <= $totalPage ){
			$max = $current + $showCount;
		}else{
			$max = $totalPage;
		}

		
		
		for($i=1;$i<=$totalPage;$i++){
			if($i >= $min and $i<=$max){
				if($current == $i){
					echo $i." | ";
				}else{
					echo "<a href=\"".$link."&$param=$i\">".$i."</a> | ";
				}
			}
		}
		
		
	}	

	
	function printPagingSelect($link, $param, $class = "", $classCurrent = "", $showCount="10") {
		// link = page filename + original parameters
		// param = name of variable parameter for paging
		// current = current page (integer)
		// class = style for link
		// classCurrent = style for current
		$nCount = $this->mCount;
		$totalPage = ceil($nCount/$this->mPageSize);
		$current = $this->mPage;
		
		$showCount = floor($showCount/2);
		
		//echo $showCount;
		
		if( ($current - $showCount)  > 0 ){
			$min = $current - $showCount;
		}else{
			$min = 1;
		}
		
		if( ($current + $showCount) <= $totalPage ){
			$max = $current + $showCount;
		}else{
			$max = $totalPage;
		}

		
		echo "<select name='hPage'>";
		for($i=1;$i<=$totalPage;$i++){
			if($i >= $min and $i<=$max){
				if($current == $i){
					echo "<option value='".$i."' selected>".$i;	
				}else{
					echo "<option value='".$i."' >".$i;
				}
			}
		}
		echo "</select>";
		
		
	}	
	
	function hasPrevious() {
		if (empty($this->mPage) || ($this->mPage <= 1)) {
			return false;
		} else {
			return true;
		}
	}

	function printPrevious($link, $param, $text = "&lt;", $class = "", $classDisabled = "") {
		// link = page filename + original parameters
		// param = name of variable parameter for paging
		// current = current page (integer)
		// class = style for link
		// classDisabled = style if disabled
		if (!$this->hasPrevious()) {
			if (!empty($classDisabled)) {
				echo ("<span class=".$classDisabled.">".$text."</span>");
			} else {
				echo ($text);
			}
			return false;
		} else {
			$begin = "<a href=\"".$link;
			if (ereg('\?',$link)) {
				$begin .= "&";
			} else {
				$begin .= "?";
			}
			$begin.= $param."=";
			$end = "\"";
			if (!empty($class)) {
				$end .= " class=\"".$class."\"";
			}
			$end .= ">";
			echo $begin.($this->mPage-1).$end.$text."</a>";
			return true;
		}
	}

	function hasNext() {
		if (empty($this->mPage)) {
			$this->mPage = 1;
		}
		if (($this->mPage * $this->mPageSize) >= ($this->mCount)) {
			return false;
		} else {
			return true;
		}
	}

	function printNext($link, $param, $text = "&gt;", $class = "", $classDisabled = "") {
		// link = page filename + original parameters
		// param = name of variable parameter for paging
		// current = current page (integer)
		// class = style for link
		// classDisabled = style if disabled
		if (!$this->hasNext()) {
			if (!empty($classDisabled)) {
				echo ("<span class=".$classDisabled.">".$text."</span>");
			} else {
				echo ($text);
			}
			return false;
		} else {
			$begin = "<a href=\"".$link;
			if (ereg('\?',$link)) {
				$begin .= "&";
			} else {
				$begin .= "?";
			}
			$begin.= $param."=";
			$end = "\"";
			if (!empty($class)) {
				$end .= " class=\"".$class."\"";
			}
			$end .= ">";
			echo $begin.($this->mPage+1).$end.$text."</a>";
			return true;
		}
	}
}
?>
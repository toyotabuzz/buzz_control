<?
/*********************************************************
		Class :					MemberHistory

		Last update :	  10 Jan 02

		Description:	  Class manage t_member_history table

*********************************************************/
 
class MemberHistory extends DB{

	var $TABLE="t_member_history";

var $m_member_history_id;
function get_member_history_id() { return $this->m_member_history_id; }
function set_member_history_id($data) { $this->m_member_history_id = $data; }

var $m_member_id;
function get_member_id() { return $this->m_member_id; }
function set_member_id($data) { $this->m_member_id = $data; }

var $m_company_id;
function get_company_id() { return $this->m_company_id; }
function set_company_id($data) { $this->m_company_id = $data; }

var $m_team_id;
function get_team_id() { return $this->m_team_id; }
function set_team_id($data) { $this->m_team_id = $data; }

var $m_company_ids;
function get_company_ids() { return $this->m_company_ids; }
function set_company_ids($data) { $this->m_company_ids = $data; }

var $m_team_ids;
function get_team_ids() { return $this->m_team_ids; }
function set_team_ids($data) { $this->m_team_ids = $data; }


var $m_date_add;
function get_date_add() { return $this->m_date_add; }
function set_date_add($data) { $this->m_date_add = $data; }

var $m_remark;
function get_remark() { return $this->m_remark; }
function set_remark($data) { $this->m_remark = $data; }
	
	function MemberHistory($objData=NULL) {
        If ($objData->member_history_id !="") {
			$this->set_member_history_id($objData->member_history_id);
			$this->set_member_id($objData->member_id);
			$this->set_company_id($objData->company_id);
			$this->set_team_id($objData->team_id);
			$this->set_company_ids($objData->company_ids);
			$this->set_team_ids($objData->team_ids);
			$this->set_date_add($objData->date_add);
			$this->set_remark($objData->remark);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mMemberHistoryId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE member_history_id =".$this->mMemberHistoryId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberHistory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->MemberHistory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( member_id , company_id , team_id,  company_ids , team_ids , date_add , remark ) " ." VALUES ( "
		." '".$this->m_member_id."' , "
		." '".$this->m_company_id."' , "
		." '".$this->m_team_id."' , "
		." '".$this->m_company_ids."' , "
		." '".$this->m_team_ids."' , "
		." '".date("Y-m-d H:i:s")."' , "
		." '".$this->m_remark."' "
		." ) "; 
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mMemberHistoryId = mysql_insert_id();
            return $this->mMemberHistoryId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." member_id = '".$this->m_member_id."' "
		." , company_id = '".$this->m_company_id."' "
		." , team_id = '".$this->m_team_id."' "
		." , company_ids = '".$this->m_company_ids."' "
		." , team_ids = '".$this->m_team_ids."' "
		." , remark = '".$this->m_remark."' "
		." WHERE member_history_id = ".$this->m_member_history_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE member_history_id=".$this->mMemberHistoryId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer MemberHistoryTopic List

		Last update :		22 Mar 02

		Description:		Customer MemberHistoryTopic List

*********************************************************/

class MemberHistoryList extends DataList {
	var $TABLE = "t_member_history";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_history_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new MemberHistory($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getMemberHistoryId()."\"");
			if (($defaultId != null) && ($objItem->getMemberHistoryId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
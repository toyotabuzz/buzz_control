<?
/*********************************************************
		Class :					Stock Car

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_car table

*********************************************************/
 
class StockCar extends DB{

	var $TABLE="t_stock_car";

	var $mStockCarId;
	function getStockCarId() { return $this->mStockCarId; }
	function setStockCarId($data) { $this->mStockCarId = $data; }
	
	var $mStockDate;
	function getStockDate() { return htmlspecialchars($this->mStockDate); }
	function setStockDate($data) { $this->mStockDate = $data; }	
	
	var $mTBR;
	function getTBR() { return htmlspecialchars($this->mTBR); }
	function setTBR($data) { $this->mTBR = $data; }	
	
	var $mStockBy;
	function getStockBy() { return htmlspecialchars($this->mStockBy); }
	function setStockBy($data) { $this->mStockBy = $data; }	
	
	var $mStockName;
	function getStockName() { return $this->mStockName; }
	function setStockName($data) { $this->mStockName = $data; }	
	
	var $mInvoiceDate;
	function getInvoiceDate() { return htmlspecialchars($this->mInvoiceDate); }
	function setInvoiceDate($data) { $this->mInvoiceDate = $data; }	
	
	var $mInvoiceNumber;
	function getInvoiceNumber() { return htmlspecialchars($this->mInvoiceNumber); }
	function setInvoiceNumber($data) { $this->mInvoiceNumber = $data; }		
	
	var $mInvoiceRemark;
	function getInvoiceRemark() { return htmlspecialchars($this->mInvoiceRemark); }
	function setInvoiceRemark($data) { $this->mInvoiceRemark = $data; }		
	
	var $mCarSeriesId;
	function getCarSeriesId() { return htmlspecialchars($this->mCarSeriesId); }
	function setCarSeriesId($data) { $this->mCarSeriesId = $data; }
	
	var $mModel;
	function getModel() { return htmlspecialchars($this->mModel); }
	function setModel($data) { $this->mModel = $data; }	
	
	var $mStockNumber;
	function getStockNumber() { return htmlspecialchars($this->mStockNumber); }
	function setStockNumber($data) { $this->mStockNumber = $data; }

	var $mStockNumberReturn;
	function getStockNumberReturn() { return htmlspecialchars($this->mStockNumberReturn); }
	function setStockNumberReturn($data) { $this->mStockNumberReturn = $data; }
	
	var $mStockSourceId;
	function getStockSourceId() { return $this->mStockSourceId; }
	function setStockSourceId($data) { $this->mStockSourceId = $data; }
	
	var $mCarFrom;
	function getCarFrom() { return $this->mCarFrom; }
	function setCarFrom($data) { $this->mCarFrom = $data; }	
	
	var $mGear;
	function getGear() { return $this->mGear; }
	function setGear($data) { $this->mGear = $data; }
	
	var $mCarColorId;
	function getCarColorId() { return $this->mCarColorId; }
	function setCarColorId($data) { $this->mCarColorId = $data; }
	
	var $mCarNumber;
	function getCarNumber() { return $this->mCarNumber; }
	function setCarNumber($data) { $this->mCarNumber = $data; }
	
	var $mEngineNumber;
	function getEngineNumber() { return $this->mEngineNumber; }
	function setEngineNumber($data) { $this->mEngineNumber = $data; }
	
	var $mKey1;
	function getKey1() { return $this->mKey1; }
	function setKey1($data) { $this->mKey1 = $data; }

	var $mKey2;
	function getKey2() { return $this->mKey2; }
	function setKey2($data) { $this->mKey2 = $data; }
	
	var $mStockCargoId;
	function getStockCargoId() { return $this->mStockCargoId; }
	function setStockCargoId($data) { $this->mStockCargoId = $data; }	
	
	var $mRecentParking;
	function getRecentParking() { return $this->mRecentParking; }
	function setRecentParking($data) { $this->mRecentParking = $data; }
	
	var $mSaleId;
	function getSaleId() { return $this->mSaleId; }
	function setSaleId($data) { $this->mSaleId = $data; }
	
	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	function getStatusDetail() { 
	
				switch ($this->mStatus){
					case 0:
						return "ö����";
						break;
					case 1:
						return "ö����";
						break;
					case 2:
						return "ö�ͧ";
						break;
					case 3:
						return "ö���ͺ";
						break;
				}
 }
	
	//complete
	//ʶҹТͧ��èʹ
	// 0 = ����ҧ��ö
	// 1 = ��ҧ��ö
	var $mComplete;
	function getComplete() { return $this->mComplete; }
	function setComplete($data) { $this->mComplete = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }
	
	var $mPrice;	
	function getPrice() { return $this->mPrice; }
	function setPrice($data) { $this->mPrice = $data; }	
	
	var $mParking;	
	function getParking() { return $this->mParking; }
	function setParking($data) { $this->mParking = $data; }		
	
	var $mParkingDate;	
	function getParkingDate() { return $this->mParkingDate; }
	function setParkingDate($data) { $this->mParkingDate = $data; }		
	
	var $mParkIn;	
	function getParkIn() { return $this->mParkIn; }
	function setParkIn($data) { $this->mParkIn = $data; }		
	
	var $mParkInDate;	
	function getParkInDate() { return $this->mParkInDate; }
	function setParkInDate($data) { $this->mParkInDate = $data; }		
	
	var $mParkInFrom;	
	function getParkInFrom() { return $this->mParkInFrom; }
	function setParkInFrom($data) { $this->mParkInFrom = $data; }		
	
	var $mRecieveStatus;	
	function getRecieveStatus() { return $this->mRecieveStatus; }
	function setRecieveStatus($data) { $this->mRecieveStatus = $data; }		
	function getRecieveStatusDetail() { 	
		switch ($this->mRecieveStatus){
			case 1:
				return " ���ͺ�����Ѻö����";
				break;
			case 2:
				return "���ͺ���ǽҡ�ʹ";
				break;
			case 3:
				return "���ͺ���Ǥ�ҧ��ǹ� �Ѻö����";
				break;
			case 4:
				return "���ͺ���Ǥ�ҧ��ǹ� �ҡ�ʹ";
				break;
			case 5:
				return "�ѧ������ͺ ��ҧ��ǹ�";
				break;
			case 6:
				return "�ѧ������ͺ����";
				break;
				
		}
 	}
	
	
	var $mRecieveNumber;	
	function getRecieveNumber() { return $this->mRecieveNumber; }
	function setRecieveNumber($data) { $this->mRecieveNumber = $data; }		
	
	var $mRecieveDate;	
	function getRecieveDate() { return $this->mRecieveDate; }
	function setRecieveDate($data) { $this->mRecieveDate = $data; }		
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }
	
	var $mTBRSale;	
	function getTBRSale() { return $this->mTBRSale; }
	function setTBRSale($data) { $this->mTBRSale = $data; }		
	
	var $mTBRSaleDealer;	
	function getTBRSaleDealer() { return $this->mTBRSaleDealer; }
	function setTBRSaleDealer($data) { $this->mTBRSaleDealer = $data; }		
	
	var $mTBRSaleRemark;	
	function getTBRSaleRemark() { return $this->mTBRSaleRemark; }
	function setTBRSaleRemark($data) { $this->mTBRSaleRemark = $data; }		
	
	var $mDriver;	
	function getDriver() { return $this->mDriver; }
	function setDriver($data) { $this->mDriver = $data; }		
	
	var $mDriverOut;	
	function getDriverOut() { return $this->mDriverOut; }
	function setDriverOut($data) { $this->mDriverOut = $data; }		
	
	var $mPassOrderId;	
	function getPassOrderId() { return $this->mPassOrderId; }
	function setPassOrderId($data) { $this->mPassOrderId = $data; }		
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }		
	
	var $mScan;	
	function getScan() { return $this->mScan; }
	function setScan($data) { $this->mScan = $data; }		
	
	var $mStockCarTbrImportItemId;	
	function getStockCarTbrImportItemId() { return $this->mStockCarTbrImportItemId; }
	function setStockCarTbrImportItemId($data) { $this->mStockCarTbrImportItemId = $data; }				
	
	
	function StockCar($objData=NULL) {
        If ($objData->stock_car_id !="") {
            $this->setStockCarId($objData->stock_car_id);
			$this->setStockDate($objData->stock_date);
			$this->setTBR($objData->tbr);
			$this->setStockBy($objData->stock_by);
			$this->setStockName($objData->stock_name);
			$this->setInvoiceDate($objData->invoice_date);
			$this->setInvoiceNumber($objData->invoice_number);
			$this->setInvoiceRemark($objData->invoice_remark);
			$this->setCarSeriesId($objData->car_series_id);
			$this->setModel($objData->model);
			$this->setStockNumber($objData->stock_number);
			$this->setStockNumberReturn($objData->stock_number_return);
			$this->setStockSourceId($objData->stock_source_id);
			$this->setCarFrom($objData->car_from);
			$this->setGear($objData->gear);
			$this->setCarColorId($objData->car_color_id);
			$this->setCarNumber($objData->car_number);
			$this->setEngineNumber($objData->engine_number);
			$this->setKey1($objData->key1);
			$this->setKey2($objData->key2);
			$this->setStockCargoId($objData->stock_cargo_id);
			$this->setRecentParking($objData->recent_parking);
			$this->setSaleId($objData->sale_id);
			$this->setCarModelId($objData->car_model_id);
			$this->setStatus($objData->status);
			$this->setComplete($objData->complete);
			$this->setOrderId($objData->order_id);
			$this->setCustomerId($objData->customer_id);
			$this->setPrice($objData->price);
			$this->setParking($objData->parking);
			$this->setParkingDate($objData->parking_date);
			$this->setParkIn($objData->park_in);
			$this->setParkInDate($objData->park_in_date);
			$this->setParkInFrom($objData->park_in_from);
			$this->setRecieveStatus($objData->recieve_status);		
			$this->setRecieveNumber($objData->recieve_number);
			$this->setRecieveDate($objData->recieve_date);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
			$this->setTBRSale($objData->tbr_sale);
			$this->setTBRSaleDealer($objData->tbr_sale_dealer);
			$this->setTBRSaleRemark($objData->tbr_sale_remark);	
			$this->setDriver($objData->driver);
			$this->setDriverOut($objData->driver_out);
			$this->setPassOrderId($objData->pass_order_id);
			$this->setCompanyId($objData->company_id);
			$this->setScan($objData->scan);
			$this->setStockCarTbrImportItemId($objData->stock_car_tbr_import_item_id);
        }
    }

	function init(){
		$this->setStockNumber(stripslashes($this->mStockNumber));
		$this->setStockNumberReturn(stripslashes($this->mStockNumberReturn));
		$this->setCarFrom(stripslashes($this->mCarFrom));
		$this->setCarNumber(stripslashes($this->mCarNumber));
		$this->setModel(stripslashes($this->mModel));
		$this->setEngineNumber(stripslashes($this->mEngineNumber));
		$this->setKey1(stripslashes($this->mKey1));
		$this->setKey2(stripslashes($this->mKey2));
		$this->setRecentParking(stripslashes($this->mRecentParking));
	}
		
	function load() {

		if ($this->mStockCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_car_id =".$this->mStockCarId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mStockCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_car_id =".$this->mStockCarId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	//function load for sale report : rp01
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.stock_car_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = O.car_series_id "
					." LEFT JOIN t_car_model CM ON CS.car_model_id = CM.car_model_id "
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}
	
	function add() {
		global $sCompanyId;
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_date, tbr, stock_by, stock_name, invoice_date, invoice_number, car_series_id, model, stock_number, stock_cargo_id, stock_number_return, stock_source_id, car_from, gear, car_color_id, 
						car_number, engine_number, key1, key2, recent_parking, sale_id, car_model_id, price, park_in, park_in_date, park_in_from, driver, driver_out, status, company_id, scan, stock_car_tbr_import_item_id ) "
						." VALUES ( '".$this->mStockDate
						."' , '".$this->mTBR
						."' , '".$this->mStockBy
						."', '".$this->mStockName
						."', '".$this->mInvoiceDate
						."', '".$this->mInvoiceNumber
						."' ,  '".$this->mCarSeriesId
						."' , '".$this->mModel
						."' , '".$this->mStockNumber
						."' , '".$this->mStockCargoId
						."' , '".$this->mStockNumberReturn
						."' , '".$this->mStockSourceId
						."' , '".$this->mCarFrom
						."' , '".$this->mGear
						."' , '".$this->mCarColorId
						."' , '".$this->mCarNumber
						."' , '".$this->mEngineNumber
						."' , '".$this->mKey1
						."' , '".$this->mKey2
						."' , '".$this->mRecentParking
						."' , '".$this->mSaleId
						."' , '".$this->mCarModelId
						."' , '".$this->mPrice
						."' , '".$this->mParkIn
						."' , '".$this->mParkInDate
						."' , '".$this->mParkInFrom
						."' , '".$this->mDriver
						."' , '".$this->mDriverOut
						."', '".$this->mStatus
						."', '".$sCompanyId
						."', '".$this->mScan
						."', '".$this->mStockCarTbrImportItemId."'  ) ";
						
						//echo $strSql;
						//exit;
						
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockCarId = mysql_insert_id();
            return $this->mStockCarId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_date = '".$this->mStockDate."'  "
						." , tbr = '".$this->mTBR."'  "
						." , stock_by = '".$this->mStockBy."'  "
						." , stock_name = '".$this->mStockName."'  "
						." , invoice_date = '".$this->mInvoiceDate."'  "
						." , invoice_number = '".$this->mInvoiceNumber."'  "
						." , car_series_id = '".$this->mCarSeriesId."'  "
						." , model = '".$this->mModel."'  "
						." , stock_number = '".$this->mStockNumber."'  "
						." , stock_number_return = '".$this->mStockNumberReturn."'  "
						." , stock_source_id = '".$this->mStockSourceId."'  "
						." , car_from = '".$this->mCarFrom."'  "
						." , gear = '".$this->mGear."'  "
						." , car_color_id = '".$this->mCarColorId."'  "
						." , car_number = '".$this->mCarNumber."'  "
						." , engine_number = '".$this->mEngineNumber."'  "
						." , key1 = '".$this->mKey1."'  "
						." , key2 = '".$this->mKey2."'  "
						." , stock_cargo_id = '".$this->mStockCargoId."'  "
						." , recent_parking = '".$this->mRecentParking."'  "
						." , sale_id = '".$this->mSaleId."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." , price = '".$this->mPrice."'  "
						." , parking = '".$this->mParking."'  "
						." , parking_date = '".$this->mParkingDate."'  "
						." , park_in = '".$this->mParkIn."'  "
						." , park_in_date = '".$this->mParkInDate."'  "
						." , park_in_from = '".$this->mParkInFrom."'  "
						." , driver = '".$this->mDriver."'  "
						." , driver_out = '".$this->mDriverOut."'  "
						." , status = '".$this->mStatus."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function update_scan(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  tbr = '".$this->mTBR."'  "
						." , stock_by = '".$this->mStockBy."'  "
						." , stock_name = '".$this->mStockName."'  "
						." , stock_number = '".$this->mStockNumber."'  "
						." , stock_source_id = '".$this->mStockSourceId."'  "
						." , car_from = '".$this->mCarFrom."'  "
						." , key1 = '".$this->mKey1."'  "
						." , key2 = '".$this->mKey2."'  "
						." , stock_cargo_id = '".$this->mStockCargoId."'  "
						." , recent_parking = '".$this->mRecentParking."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateParking(){
		$strSql = "UPDATE ".$this->TABLE
						." SET recent_parking = '".$this->mRecentParking."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." WHERE  car_number = '".$this->mCarNumber."'  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateParkingCar(){
		$strSql = "UPDATE ".$this->TABLE
						." SET parking = '".$this->mParking."'  "
						." , parking_date = '".$this->mParkingDate."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateInvoice(){
		$strSql = "UPDATE ".$this->TABLE
						." SET invoice_number = '".$this->mInvoiceNumber."'  "
						." , invoice_date = '".$this->mInvoiceDate."'  "
						." , invoice_remark = '".$this->mInvoiceRemark."'  "
						." , tbr = '".$this->mTBR."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStatusByStockCarId(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." WHERE  stock_car_id = '".$this->mStockCarId."'  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function updateReset(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateComplete(){
		$strSql = "UPDATE ".$this->TABLE
						." SET complete = '".$this->mComplete."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updatePassOrder(){
		$strSql = "UPDATE ".$this->TABLE
						." SET pass_order_id = '".$this->mPassOrderId."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , status = '".$this->mStatus."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateRecieve(){
		$strSql = "UPDATE ".$this->TABLE
						." SET recieve_status = '".$this->mRecieveStatus."'  "
						." , recieve_number = '".$this->mRecieveNumber."'  "
						." , recieve_date = '".$this->mRecieveDate."'  "
						." , tbr = '".$this->mTBR."'  "
						." , status = '".$this->mStatus."'  "
						." , recent_parking = '".$this->mRecentParking."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." WHERE  stock_car_id = '".$this->mStockCarId."'  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateTBRSale(){
		$strSql = "UPDATE ".$this->TABLE
						." SET recieve_status = '".$this->mRecieveStatus."'  "
						." , recieve_number = '".$this->mRecieveNumber."'  "
						." , recieve_date = '".$this->mRecieveDate."'  "
						." , status = '".$this->mStatus."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , customer_id = '".$this->mCustomerId."'  "
						." , tbr_sale = '".$this->mTBRSale."'  "
						." , tbr_sale_dealer = '".$this->mTBRSaleDealer."'  "
						." , tbr_sale_remark = '".$this->mTBRSaleRemark."'  "
						." , driver_out = '".$this->mDriverOut."'  "
						." WHERE  stock_car_id = '".$this->mStockCarId."'  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_car_id=".$this->mStockCarId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_stock_car_item  "
        . " WHERE stock_car_id =".$this->mStockCarId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateDelete(){
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  stock_car_id = ".$this->mStockCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function checkDelete(){
        if ($this->mOrderId == 0)
       {
			return true;
        }
		return false;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

		if ($this->mCarNumber == "") { 
			$asrErrReturn["car_number"] = "��س��к�";
		}else{
			if(!$this->checkUniqueCode(strtolower($Mode) )){
				$asrErrReturn["car_number"] = '�����Ţ�ѧ��ӫ�͹';
			}
		}
		if ($this->mEngineNumber == ""){
			$asrErrReturn["engine_number"] = "��س��к�";
		}else{
			if(!$this->checkUniqueEngine(strtolower($Mode) )){
				$asrErrReturn["engine_number"] = '�����Ţ����ͧ��ӫ�͹';
			}
		}

        Return $asrErrReturn;
    }
	
    function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT car_number  FROM t_stock_car ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE status < 3 AND car_number = ".trim(strtoupper($this->convStr4SQL($this->mCarNumber)));
		}else{
			$strSql .= " WHERE status < 3 AND  car_number = ".trim(strtoupper($this->convStr4SQL($this->mCarNumber)))." and stock_car_id != ".$this->mStockCarId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
    function checkUniqueEngine($strMode)
    {
        $strSql  = "SELECT engine_number  FROM t_stock_car ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE status < 3 AND  engine_number = ".trim(strtoupper($this->convStr4SQL($this->mEngineNumber)));
		}else{
			$strSql .= " WHERE status < 3 AND  engine_number = ".trim(strtoupper($this->convStr4SQL($this->mEngineNumber)))." and stock_car_id != ".$this->mStockCarId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				Customer Stock Car List

		Last update :		22 Mar 02

		Description:		Customer Stock Car List

*********************************************************/

class StockCarList extends DataList {
	var $TABLE = "t_stock_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.stock_car_id) as rowCount FROM ".$this->TABLE
			." P  "
			." LEFT JOIN t_car_series CS ON CS.car_series_id = P.car_series_id  "
			." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
			." LEFT JOIN t_stock_car_cargo CG ON CG.stock_cargo_id = P.stock_cargo_id  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT  P.* FROM ".$this->TABLE." P  "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = P.car_series_id  "
		." LEFT JOIN t_car_model CM ON CM.car_model_id = CS.car_model_id  "
		." LEFT JOIN t_stock_car_cargo CG ON CG.stock_cargo_id = P.stock_cargo_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadReport() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.stock_car_id) as rowCount FROM ".$this->TABLE
			." P  "
			." LEFT JOIN t_car_series CS ON CS.car_series_id = P.car_series_id  "
			." LEFT JOIN t_order O ON O.order_id = P.order_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P  "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = P.car_series_id  "
		." LEFT JOIN t_order O ON O.order_id = P.order_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockCarId()."\"");
			if (($defaultId != null) && ($objItem->getStockCarId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCarNumber()."</option>");
		}
		echo("</select>");
	}			
	
}
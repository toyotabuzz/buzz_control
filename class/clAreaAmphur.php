<?
/*********************************************************
		Class :				Amphur

		Last update :		16 NOV 02

		AmphurCode:		Class manage amphur table

*********************************************************/

class Amphur extends DB{

	var $TABLE="t_area_amphur";
	
	var $mAmphurId;
	function getAmphurId() { return $this->mAmphurId; }
	function setAmphurId($data) { $this->mAmphurId = $data; }
	
	var $mAmphurCode;
	function getAmphurCode() { return htmlspecialchars($this->mAmphurCode); }
	function setAmphurCode($data) { $this->mAmphurCode = $data; }

	var $mAmphurName;
	function getAmphurName() { return htmlspecialchars($this->mAmphurName); }
	function setAmphurName($data) { $this->mAmphurName = $data; }

	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }

	var $mProvinceName;
	function getProvinceName() { return htmlspecialchars($this->mProvinceName); }
	function setProvinceName($data) { $this->mProvinceName = $data; }

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	
	
	function Amphur($objData=NULL) {
        If ($objData->amphur_id!="") {
            $this->setAmphurId($objData->amphur_id);
			$this->setAmphurCode($objData->amphur_code);
            $this->setAmphurName($objData->amphur_name);
			$this->setProvinceCode($objData->province_code);
			$this->setProvinceName($objData->province_name);
			$this->setRank($objData->rank);
        }
    }
	
	function init(){
		$this->setAmphurName(stripslashes($this->mAmphurName));
	}
	
	function load() {

		if ($this->mAmphurId == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE C.amphur_id =".$this->mAmphurId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Amphur($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Amphur($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( amphur_name, province_code, amphur_code, province_name, rank ) "
						." VALUES ( '".$this->mAmphurName."' ,"
						." '".$this->mProvinceCode."' , "
						." '".$this->mAmphurCode."' , "
						." '".$this->mProviceName."' , "
						." '".$this->mRank."' ) ";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mAmphurId = mysql_insert_id();
            return $this->mAmphurId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET amphur_name = '".$this->mAmphurName."' , "
						." province_code = '".$this->mProvinceCode."' ,  "
						." amphur_code = '".$this->mAmphurCode."' ,  "
						." province_name = '".$this->mProvinceName."' ,  "
						." rank = '".$this->mRank."'   "
						." WHERE  amphur_id = ".$this->mAmphurId."  ";
        $this->getConnection();
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE amphur_id=".$this->mAmphurId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function sql($sql){
		$strSql = $sql;
        $this->getConnection();
        return $this->query($strSql);
	}
	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				if ($this->mAmphurName == "") $asrErrReturn["amphur_name"] = "��س��к������";
				
				if ($this->mAmphurCode == ""){
					$asrErrReturn["amphur_code"] = "��س��к����������";
				}else{
					if(!$this->checkUnique($Mode,"amphur_code","amphur_id",$TABLE,$this->mAmphurId,$this->mAmphurCode)){
						$asrErrReturn["amphur_code"] = '��������ͫ�ӫ�͹';
					}
				}
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }

   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
	
}

/*********************************************************
		Class :				AmphurList

		Last update :		22 Mar 02

		AmphurCode:		Amphur user list

*********************************************************/


class AmphurList extends DataList {
	var $TABLE = "t_area_amphur";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT amphur_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Amphur($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT amphur_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Amphur($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getAmphurCode()."\"");
			if (($defaultId != null) && ($objItem->getAmphurCode() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getAmphurName()."</option>");
		}
		echo("</select>");
	}

}
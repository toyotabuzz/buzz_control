<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_transfer

*********************************************************/
 
class OrderTransfer extends DB{

	var $TABLE="t_order_transfer";

	var $m_order_transfer_id;
	function get_order_transfer_id() { return $this->m_order_transfer_id; }
	function set_order_transfer_id($data) { $this->m_order_transfer_id = $data; }
	
	var $m_order_id;
	function get_order_id() { return $this->m_order_id; }
	function set_order_id($data) { $this->m_order_id = $data; }
	
	var $m_booking_company_id;
	function get_booking_company_id() { return $this->m_booking_company_id; }
	function set_booking_company_id($data) { $this->m_booking_company_id = $data; }
	
	var $m_booking_team_id;
	function get_booking_team_id() { return $this->m_booking_team_id; }
	function set_booking_team_id($data) { $this->m_booking_team_id = $data; }
	
	var $m_booking_sale_id;
	function get_booking_sale_id() { return $this->m_booking_sale_id; }
	function set_booking_sale_id($data) { $this->m_booking_sale_id = $data; }
	
	var $m_sending_company_id;
	function get_sending_company_id() { return $this->m_sending_company_id; }
	function set_sending_company_id($data) { $this->m_sending_company_id = $data; }
	
	var $m_sending_team_id;
	function get_sending_team_id() { return $this->m_sending_team_id; }
	function set_sending_team_id($data) { $this->m_sending_team_id = $data; }
	
	var $m_sending_sale_id;
	function get_sending_sale_id() { return $this->m_sending_sale_id; }
	function set_sending_sale_id($data) { $this->m_sending_sale_id = $data; }
	
	var $m_transfer_date;
	function get_transfer_date() { return $this->m_transfer_date; }
	function set_transfer_date($data) { $this->m_transfer_date = $data; }
	
	var $m_transfer_by;
	function get_transfer_by() { return $this->m_transfer_by; }
	function set_transfer_by($data) { $this->m_transfer_by = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	function OrderTransfer($objData=NULL) {
        If ($objData->order_transfer_id !="") {
			$this->set_order_transfer_id($objData->order_transfer_id);
			$this->set_order_id($objData->order_id);
			$this->set_booking_company_id($objData->booking_company_id);
			$this->set_booking_team_id($objData->booking_team_id);
			$this->set_booking_sale_id($objData->booking_sale_id);
			$this->set_sending_company_id($objData->sending_company_id);
			$this->set_sending_team_id($objData->sending_team_id);
			$this->set_sending_sale_id($objData->sending_sale_id);
			$this->set_transfer_date($objData->transfer_date);
			$this->set_transfer_by($objData->transfer_by);
			$this->set_remark($objData->remark);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_order_transfer_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE order_transfer_id =".$this->m_order_transfer_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderTransfer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->m_order_transfer_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE order_transfer_id =".$this->m_order_transfer_id;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderTransfer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderTransfer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( order_id , booking_company_id , booking_team_id , booking_sale_id , sending_company_id , sending_team_id , sending_sale_id , transfer_date , transfer_by , remark ) " ." VALUES ( "
		." '".$this->m_order_id."' , "
		." '".$this->m_booking_company_id."' , "
		." '".$this->m_booking_team_id."' , "
		." '".$this->m_booking_sale_id."' , "
		." '".$this->m_sending_company_id."' , "
		." '".$this->m_sending_team_id."' , "
		." '".$this->m_sending_sale_id."' , "
		." '".$this->m_transfer_date."' , "
		." '".$this->m_transfer_by."' , "
		." '".$this->m_remark."' "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_order_transfer_id = mysql_insert_id();
            return $this->m_order_transfer_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." order_id = '".$this->m_order_id."' "
		." , booking_company_id = '".$this->m_booking_company_id."' "
		." , booking_team_id = '".$this->m_booking_team_id."' "
		." , booking_sale_id = '".$this->m_booking_sale_id."' "
		." , sending_company_id = '".$this->m_sending_company_id."' "
		." , sending_team_id = '".$this->m_sending_team_id."' "
		." , sending_sale_id = '".$this->m_sending_sale_id."' "
		." , transfer_date = '".$this->m_transfer_date."' "
		." , transfer_by = '".$this->m_transfer_by."' "
		." , remark = '".$this->m_remark."' "
		." WHERE order_transfer_id = ".$this->m_order_transfer_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_transfer_id=".$this->m_order_transfer_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class OrderTransferList extends DataList {
	var $TABLE = "t_order_transfer";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_transfer_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderTransfer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderTransferId()."\"");
			if (($defaultId != null) && ($objItem->getOrderTransferId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
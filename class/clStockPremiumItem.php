<?
/*********************************************************
		Class :					Stock Premium Item

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_premium_item table

*********************************************************/
 
class StockPremiumItem extends DB{

	var $TABLE="t_stock_premium_item";

	var $mStockPremiumItemId;
	function getStockPremiumItemId() { return $this->mStockPremiumItemId; }
	function setStockPremiumItemId($data) { $this->mStockPremiumItemId = $data; }	
	
	var $mStockPremiumId;
	function getStockPremiumId() { return $this->mStockPremiumId; }
	function setStockPremiumId($data) { $this->mStockPremiumId = $data; }
	
	var $mStockType;
	function getStockType() { return $this->mStockType; }
	function setStockType($data) { $this->mStockType = $data; }	
	
	var $mStockTypeDetail;
	function getStockTypeDetail() {
		if($this->mStockType == 1){
			$value = "�ԡ�Թ������";
		}else{
			$value = "�����Թ����͡";
		}
	    return $value;	 
    }
	
	var $mStockBy;
	function getStockBy() { return htmlspecialchars($this->mStockBy); }
	function setStockBy($data) { $this->mStockBy = $data; }
	
	var $mPostedDate;
	function getPostedDate() { return htmlspecialchars($this->mPostedDate); }
	function setPostedDate($data) { $this->mPostedDate = $data; }
	
	var $mTotal;
	function getTotal() { return htmlspecialchars($this->mTotal); }
	function setTotal($data) { $this->mTotal = $data; }

	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }
	
	function StockPremiumItem($objData=NULL) {
        If ($objData->stock_premium_item_id !="") {
            $this->setStockPremiumItemId($objData->stock_premium_item_id);
			$this->setStockPremiumId($objData->stock_premium_id);
			$this->setStockType($objData->stock_type);
			$this->setStockBy($objData->stock_by);
			$this->setTotal($objData->total);
			$this->setRemark($objData->remark);
			$this->setPostedDate($objData->posted_date);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
	}
		
	function load() {

		if ($this->mStockPremiumItemId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_premium_item_id =".$this->mStockPremiumItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPremiumItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPremiumItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_by,stock_type,posted_date,total, remark , stock_premium_id ) "
						." VALUES ( '".$this->mStockBy."' , '".$this->mStockType."', '".date("Y-m-d H:i:s")."' ,  '".$this->mTotal."' , '".$this->mRemark."', '".$this->mStockPremiumId."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockPremiumItemId = mysql_insert_id();
            return $this->mStockPremiumItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_by = '".$this->mStockBy."'  "
						." , stock_type = '".$this->mStockType."'  "
						." , posted_date = '".date("Y-m-d H:i:s")."'  "
						." , total = '".$this->mTotal."'  "
						." , remark = '".$this->mRemark."'  "
						." , stock_premium_id = '".$this->mStockPremiumId."'  "
						." WHERE  stock_premium_item_id = ".$this->mStockPremiumItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_premium_item_id=".$this->mStockPremiumItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockPremiumId == "") $asrErrReturn["stock_premium_id"] = "��س��к�";
		if ($this->mTotal == "") $asrErrReturn["total"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Stock Premium Item List

		Last update :		22 Mar 02

		Description:		Customer Stock Premium Item List

*********************************************************/

class StockPremiumItemList extends DataList {
	var $TABLE = "t_stock_premium_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_premium_item_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPremiumItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class Mail extends DB{

	var $TABLE="t_mail";

var $m_mail_id;
function get_mail_id() { return $this->m_mail_id; }
function set_mail_id($data) { $this->m_mail_id = $data; }

var $m_mail_date;
function get_mail_date() { return $this->m_mail_date; }
function set_mail_date($data) { $this->m_mail_date = $data; }

var $m_subject;
function get_subject() { return $this->m_subject; }
function set_subject($data) { $this->m_subject = $data; }

var $m_message;
function get_message() { return $this->m_message; }
function set_message($data) { $this->m_message = $data; }

var $m_from_id;
function get_from_id() { return $this->m_from_id; }
function set_from_id($data) { $this->m_from_id = $data; }

var $m_to_id;
function get_to_id() { return $this->m_to_id; }
function set_to_id($data) { $this->m_to_id = $data; }

var $m_check;
function get_check() { return $this->m_check; }
function set_check($data) { $this->m_check = $data; }

var $m_file1;
function get_file1() { return $this->m_file1; }
function set_file1($data) { $this->m_file1 = $data; }

var $m_file2;
function get_file2() { return $this->m_file2; }
function set_file2($data) { $this->m_file2 = $data; }

var $m_file3;
function get_file3() { return $this->m_file3; }
function set_file3($data) { $this->m_file3 = $data; }


	function Mail($objData=NULL) {
        If ($objData->mail_id !="") {
			$this->set_mail_id($objData->mail_id);
			$this->set_mail_date($objData->mail_date);
			$this->set_subject($objData->subject);
			$this->set_message($objData->message);
			$this->set_from_id($objData->from_id);
			$this->set_to_id($objData->to_id);
			$this->set_check($objData->check);
			$this->set_file1($objData->file1);
			$this->set_file2($objData->file2);
			$this->set_file3($objData->file3);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_mail_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE mail_id =".$this->m_mail_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Mail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Mail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( mail_date , subject , message , from_id , to_id , check , file1 , file2 , file3 ) " ." VALUES ( "
		." '".$this->m_mail_date."' , "
		." '".$this->m_subject."' , "
		." '".$this->m_message."' , "
		." '".$this->m_from_id."' , "
		." '".$this->m_to_id."' , "
		." '".$this->m_check."' , "
		." '".$this->m_file1."' , "
		." '".$this->m_file2."' , "
		." '".$this->m_file3."' "
		." ) ";


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_mail_id = mysql_insert_id();
            return $this->m_mail_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." mail_date = '".$this->m_mail_date."' "
		." , subject = '".$this->m_subject."' "
		." , message = '".$this->m_message."' "
		." , from_id = '".$this->m_from_id."' "
		." , to_id = '".$this->m_to_id."' "
		." , check = '".$this->m_check."' "
		." , file1 = '".$this->m_file1."' "
		." , file2 = '".$this->m_file2."' "
		." , file3 = '".$this->m_file3."' "
		." WHERE mail_id = ".$this->m_mail_id." "; 	
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE mail_id=".$this->m_mail_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class MailList extends DataList {
	var $TABLE = "t_mail1";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT mail_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Mail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT mail_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Mail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_mail_id()."\"");
			if (($defaultId != null) && ($objItem->get_mail_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}		
	
	
}
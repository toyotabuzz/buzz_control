<?
/*********************************************************
		Class :					Order Stock

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_stock table

*********************************************************/
 
class OrderStock extends DB{

	var $TABLE="t_order_stock";

	var $mOrderStockId;
	function getOrderStockId() { return $this->mOrderStockId; }
	function setOrderStockId($data) { $this->mOrderStockId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }

	var $mStockProductId;
	function getStockProductId() { return $this->mStockProductId; }
	function setStockProductId($data) { $this->mStockProductId = $data; }
	
	var $mStockType;
	function getStockType() { return $this->mStockType; }
	function setStockType($data) { $this->mStockType = $data; }

	var $mQty;
	function getQty() { return htmlspecialchars($this->mQty); }
	function setQty($data) { $this->mQty = $data; }
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mTMT;
	function getTMT() { return htmlspecialchars($this->mTMT); }
	function setTMT($data) { $this->mTMT = $data; }	
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }		
	
	var $mStock;
	function getStock() { return htmlspecialchars($this->mStock); }
	function setStock($data) { $this->mStock = $data; }		
	
	var $mCheckStock;
	function getCheckStock() { return htmlspecialchars($this->mCheckStock); }
	function setCheckStock($data) { $this->mCheckStock = $data; }		

	var $mCheckBy;
	function getCheckBy() { return htmlspecialchars($this->mCheckBy); }
	function setCheckBy($data) { $this->mCheckBy = $data; }		

	var $mCheckDate;
	function getCheckDate() { return htmlspecialchars($this->mCheckDate); }
	function setCheckDate($data) { $this->mCheckDate = $data; }		
	
	var $mCheckRemark;
	function getCheckRemark() { return htmlspecialchars($this->mCheckRemark); }
	function setCheckRemark($data) { $this->mCheckRemark = $data; }		

	var $mPritemId;
	function getPritemId() { return htmlspecialchars($this->mPritemId); }
	function setPritemId($data) { $this->mPritemId = $data; }		
		
	function OrderStock($objData=NULL) {
        If ($objData->order_stock_id !="") {
            $this->setOrderStockId($objData->order_stock_id);
			$this->setStockProductId($objData->stock_product_id);
			$this->setOrderId($objData->order_id);
			$this->setQty($objData->qty);
			$this->setPrice($objData->price);
			$this->setTMT($objData->tmt);
			$this->setRemark($objData->remark);
			$this->setStock($objData->stock);
			$this->setCheckStock($objData->check_stock);
			$this->setCheckBy($objData->check_by);
			$this->setCheckDate($objData->check_date);
			$this->setCheckRemark($objData->check_remark);
			$this->setPritemId($objData->pritem_id);
        }
    }

	function init(){
		$this->setQty(stripslashes($this->mQty));
	}
		
	function load() {

		if ($this->mOrderStockId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE order_stock_id =".$this->mOrderStockId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderStock($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderStock($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		global $sMemberId;	
			
		$strSql = "INSERT INTO ".$this->TABLE
						." ( qty, stock_product_id, stock_type, price, tmt, remark, order_id , stock ) "
						." VALUES ( '".$this->mQty."','".$this->mStockProductId."','".$this->mStockType."','".$this->mPrice."','".$this->mTMT."','".$this->mRemark."','".$this->mOrderId."','".$this->mStock."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderStockId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"add_order_stock");	
			
            return $this->mOrderStockId;
        } else {
			return false;
	    }
	}

	function update(){
		global $sMemberId;	
		$strSql = "UPDATE ".$this->TABLE
						." SET qty = '".$this->mQty."'  "
						." , stock_product_id = '".$this->mStockProductId."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , stock_type = '".$this->mStockType."'  "
						." , tmt = '".$this->mTMT."'  "
						." , remark = '".$this->mRemark."'  "
						." , price = '".$this->mPrice."'  "
						." WHERE  order_stock_id = ".$this->mOrderStockId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"update_order_stock");	
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function updateStock(){
		global $sMemberId;	
		$strSql = "UPDATE ".$this->TABLE
						." SET qty = '".$this->mQty."'  "
						." , stock_product_id = '".$this->mStockProductId."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , stock_type = '".$this->mStockType."'  "
						." , tmt = '".$this->mTMT."'  "
						." , remark = '".$this->mRemark."'  "
						." , stock = '".$this->mStock."'  "
						." , price = '".$this->mPrice."'  "
						." WHERE  order_stock_id = ".$this->mOrderStockId."  ";
        $this->getConnection();
		//echo $strSql;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"update_order_stock_01");	
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateCheckStock(){
		global $sMemberId;	
		$strSql = "UPDATE ".$this->TABLE
						." SET check_stock = '".$this->mCheckStock."'  "
						." , check_by = '".$this->mCheckBy."'  "
						." , check_date = '".$this->mCheckDate."'  "
						." , check_remark = '".$this->mCheckRemark."'  "
						." WHERE  order_stock_id = ".$this->mOrderStockId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"update_order_stock_check");	
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updatePr(){
		global $sMemberId;	
		$strSql = "UPDATE ".$this->TABLE
						." SET pritem_id = '".$this->mPritemId."'  "
						." WHERE order_stock_id = ".$this->mOrderStockId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"update_order_stock_check");	
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function delete() {
		global $sMemberId;	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_stock_id=".$this->mOrderStockId." ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderStockId,"delete_order_stock");	
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mQty == "") $asrErrReturn["qty"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Stock List

		Last update :		22 Mar 02

		Description:		Order Stock List

*********************************************************/

class OrderStockList extends DataList {
	var $TABLE = "t_order_stock";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_stock_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id "
			." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id "
			.$this->getFilterSQL();	// WHERE clause
	    
		
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id "
			." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderStock($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadStock() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_stock_id) as rowCount FROM ".$this->TABLE." P "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id "
			." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id "
			." LEFT JOIN t_order O ON O.order_id = P.order_id "
			.$this->getFilterSQL();	// WHERE clause
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id "
			." LEFT JOIN t_stock_product_type SPT ON SP.stock_product_type_id = SPT.stock_product_type_id "
			." LEFT JOIN t_order O ON O.order_id = P.order_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		

		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderStock($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderStockId()."\"");
			if (($defaultId != null) && ($objItem->getOrderStockId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getQty()."</option>");
		}
		echo("</select>");
	}

}
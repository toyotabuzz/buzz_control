<?
/*********************************************************
		Class :					Order Payment

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_payment table

*********************************************************/
 
class OrderPayment extends DB{

	var $TABLE="t_order_payment";

	var $mOrderPaymentId;
	function getOrderPaymentId() { return $this->mOrderPaymentId; }
	function setOrderPaymentId($data) { $this->mOrderPaymentId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mOrderExtraId;
	function getOrderExtraId() { return $this->mOrderExtraId; }
	function setOrderExtraId($data) { $this->mOrderExtraId = $data; }

	var $mPaymentTypeId;
	function getPaymentTypeId() { return $this->mPaymentTypeId; }
	function setPaymentTypeId($data) { $this->mPaymentTypeId = $data; }

	var $mPaymentRemark;
	function getPaymentRemark() { return $this->mPaymentRemark; }
	function setPaymentRemark($data) { $this->mPaymentRemark = $data; }

	var $mPaymentTypeTitle;
	function getPaymentTypeTitle() { return $this->mPaymentTypeTitle; }
	function setPaymentTypeTitle($data) { $this->mPaymentTypeTitle = $data; }
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mBankId;
	function getBankId() { return htmlspecialchars($this->mBankId); }
	function setBankId($data) { $this->mBankId = $data; }

	var $mBankTitle;
	function getBankTitle() { return htmlspecialchars($this->mBankTitle); }
	function setBankTitle($data) { $this->mBankTitle = $data; }
	
	var $mBranch;
	function getBranch() { return htmlspecialchars($this->mBranch); }
	function setBranch($data) { $this->mBranch = $data; }

	var $mCheckNo;
	function getCheckNo() { return htmlspecialchars($this->mCheckNo); }
	function setCheckNo($data) { $this->mCheckNo = $data; }
	
	var $mCheckDate;
	function getCheckDate() { return htmlspecialchars($this->mCheckDate); }
	function setCheckDate($data) { $this->mCheckDate = $data; }
		
	//0 = �ͧ��������
	//1 = ���ͺ�����	
	//2 = ���ͺ��������
	//3 = ������������
	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }		
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }		
	
	var $mNoHead;
	function getNoHead() { return htmlspecialchars($this->mNoHead); }
	function setNoHead($data) { $this->mNoHead = $data; }		

	var $mPaymentRemarkId;
	function getPaymentRemarkId() { return $this->mPaymentRemarkId; }
	function setPaymentRemarkId($data) { $this->mPaymentRemarkId = $data; }	

	var $mPaymentDiscountId;
	function getPaymentDiscountId() { return $this->mPaymentDiscountId; }
	function setPaymentDiscountId($data) { $this->mPaymentDiscountId = $data; }	
	
	function OrderPayment($objData=NULL) {
        If ($objData->order_payment_id !="") {
            $this->setOrderPaymentId($objData->order_payment_id);
			$this->setOrderId($objData->order_id);
			$this->setOrderExtraId($objData->order_extra_id);
			$this->setPaymentTypeId($objData->payment_type_id);
			$this->setPaymentRemark($objData->payment_remark);
			$this->setPaymentTypeTitle($objData->payment_type_title);
			$this->setPrice($objData->price);
			$this->setBankId($objData->bank_id);
			$this->setBankTitle($objData->bank_title);
			$this->setBranch($objData->branch);
			$this->setCheckNo($objData->check_no);
			$this->setCheckDate($objData->check_date);
			$this->setStatus($objData->status);
			$this->setRemark($objData->remark);
			$this->setNoHead($objData->no_head);
			$this->setPaymentRemarkId($objData->payment_remark_id);
			$this->setPaymentDiscountId($objData->payment_discount_id);
        }
    }

	function init(){
		$this->setBranch(stripslashes($this->mBranch));
		$this->setCheckNo(stripslashes($this->mCheckNo));
		$this->setPrice(stripslashes($this->mPrice));
	}
		
	function load() {

		if ($this->mOrderPaymentId == '') {
			return false;
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE order_payment_id =".$this->mOrderPaymentId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderPayment($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSum($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SUM(OP.price) AS sum_price FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "		
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return $row->sum_price;
            }
        }
		return false;
	}	
	
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_id, order_extra_id, payment_type_id, payment_remark, price, bank_id, branch, check_no, check_date, remark, no_head, payment_remark_id, payment_discount_id, status, created_at, created_by ) "
						." VALUES ( '".$this->mOrderId."','".$this->mOrderExtraId."','".$this->mPaymentTypeId."','".$this->mPaymentRemark."','".$this->mPrice
						."','".$this->mBankId."','".$this->mBranch."','".$this->mCheckNo."','".$this->mCheckDate
						."','".$this->mRemark."','".$this->mNoHead."','".$this->mPaymentRemarkId."','".$this->mPaymentDiscountId."','".$this->mStatus."', NOW(), '".$_SESSION['sMemberId']."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderPaymentId = mysql_insert_id();
            return $this->mOrderPaymentId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET order_id = '".$this->mOrderId."'  "
						." , order_extra_id = '".$this->mOrderExtraId."'  "
						." , payment_type_id = '".$this->mPaymentTypeId."'  "
						." , payment_remark = '".$this->mPaymentRemark."'  "
						." , price = '".$this->mPrice."'  "
						." , bank_id = '".$this->mBankId."'  "
						." , branch = '".$this->mBranch."'  "
						." , check_no = '".$this->mCheckNo."'  "
						." , check_date = '".$this->mCheckDate."'  "
						." , no_head = '".$this->mNoHead."'  "
						." , remark = '".$this->mRemark."'  "
						." , payment_remark_id = '".$this->mPaymentRemarkId."'  "
						." , payment_discount_id = '".$this->mPaymentDiscountId."'  "
						." , status = '".$this->mStatus."'  "
						." WHERE  order_payment_id = ".$this->mOrderPaymentId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_payment_id=".$this->mOrderPaymentId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_order_price_payment  "
                . " WHERE order_payment_id=".$this->mOrderPaymentId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Payment List

		Last update :		22 Mar 02

		Description:		Order Payment List

*********************************************************/

class OrderPaymentList extends DataList {
	var $TABLE = "t_order_payment";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_payment_id) as rowCount FROM ".$this->TABLE
			." OP  ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.*, PT.title AS payment_type_title,  B.title AS bank_title FROM ".$this->TABLE." OP  "
		." LEFT JOIN t_payment_type PT ON PT.payment_type_id = OP.payment_type_id "
		." LEFT JOIN t_bank B ON B.bank_id = OP.bank_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderPayment($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderPaymentId()."\"");
			if (($defaultId != null) && ($objItem->getOrderPaymentId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getPaymentTypeTitle().",".$objItem->getPrice()."</option>");
		}
		echo("</select>");
	}

}
<?
/*********************************************************
		Class :					Customer Use Car

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_use_car table

*********************************************************/
 
class UseCar extends DB{

	var $TABLE="t_customer_use_car";

	var $mUseCarId;
	function getUseCarId() { return $this->mUseCarId; }
	function setUseCarId($data) { $this->mUseCarId = $data; }
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }

	var $mCarTypeId;
	function getCarTypeId() { return $this->mCarTypeId; }
	function setCarTypeId($data) { $this->mCarTypeId = $data; }

	var $mCarModelId;
	function getCarModelId() { return $this->mCarModelId; }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	var $mCarColorId;
	function getCarColorId() { return $this->mCarColorId; }
	function setCarColorId($data) { $this->mCarColorId = $data; }
	
	var $mCode;
	function getCode() { return $this->mCode; }
	function setCode($data) { $this->mCode = $data; }
	
	var $mDealer;
	function getDealer() { return $this->mDealer; }
	function setDealer($data) { $this->mDealer = $data; }
	
	function UseCar($objData=NULL) {
        If ($objData->use_car_id !="") {
            $this->setUseCarId($objData->use_car_id);
			$this->setCustomerId($objData->customer_id);
			$this->setCarTypeId($objData->car_type_id);
			$this->setCarModelId($objData->car_model_id);
			$this->setCarColorId($objData->car_color_id);
			$this->setCode($objData->code);
			$this->setDealer($objData->dealer);
        }
    }

	function init(){
		$this->setCode(stripslashes($this->mCode));
		$this->setDealer(stripslashes($this->mDealer));
	}
		
	function load() {

		if ($this->mUseCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE use_car_id =".$this->mUseCarId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->UseCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->UseCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( customer_id, car_type_id, car_model_id, code, dealer, car_color_id ) "
						." VALUES ( '".$this->mCustomerId."', '".$this->mCarTypeId."', '".$this->mCarModelId."', '".$this->mCode."', '".$this->mDealer."', '".$this->mCarColorId."') ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mUseCarId = mysql_insert_id();
            return $this->mUseCarId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET customer_id = '".$this->mCustomerId."'  "
						." , car_type_id = '".$this->mCarTypeId."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." , car_color_id = '".$this->mCarColorId."'  "
						." , code = '".$this->mCode."'  "
						." , dealer = '".$this->mDealer."'  "
						." WHERE  use_car_id = ".$this->mUseCarId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE use_car_id=".$this->mUseCarId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		//if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت��ͻ�����ö";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Use Car List

		Last update :		22 Mar 02

		Description:		Use Car List

*********************************************************/

class UseCarList extends DataList {
	var $TABLE = "t_customer_use_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT use_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new UseCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadAdvanceSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT use_car_id) as rowCount FROM ".$this->TABLE." U  "
			." LEFT JOIN t_customer C ON U.customer_id = C.customer_id "			
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT U.*  FROM ".$this->TABLE." U "
			." LEFT JOIN t_customer C ON U.customer_id = C.customer_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new UseCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadAdvanceSearchExcel() {
		// also gets latest delivery date
		$strSql = " SELECT DISTINCT U.*  FROM ".$this->TABLE." U "
			." LEFT JOIN t_customer C ON U.customer_id = C.customer_id "			
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimit();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new UseCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
<?
/*********************************************************
		Class :					Stock 

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_ table

*********************************************************/
 
class Stock extends DB{

	var $TABLE="t_stock";

	var $mStockId;
	function getStockId() { return $this->mStockId; }
	function setStockId($data) { $this->mStockId = $data; }
	
	var $mStockDate;
	function getStockDate() { return htmlspecialchars($this->mStockDate); }
	function setStockDate($data) { $this->mStockDate = $data; }	
	
	var $mStockBy;
	function getStockBy() { return htmlspecialchars($this->mStockBy); }
	function setStockBy($data) { $this->mStockBy = $data; }	
	
	var $mStockNumber;
	function getStockNumber() { return $this->mStockNumber; }
	function setStockNumber($data) { $this->mStockNumber = $data; }	

	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }	
	
	var $mStockCarId;
	function getStockCarId() { return $this->mStockCarId; }
	function setStockCarId($data) { $this->mStockCarId = $data; }	
	
	var $mPurchaseId;
	function getPurchaseId() { return $this->mPurchaseId; }
	function setPurchaseId($data) { $this->mPurchaseId = $data; }	
	
	var $mStockPaperId;
	function getStockPaperId() { return htmlspecialchars($this->mStockPaperId); }
	function setStockPaperId($data) { $this->mStockPaperId = $data; }
	
	var $mStockPaperDetail;
	function getStockPaperDetail() { return htmlspecialchars($this->mStockPaperDetail); }
	function setStockPaperDetail($data) { $this->mStockPaperDetail = $data; }	
	
	var $mDealerId;
	function getDealerId() { return htmlspecialchars($this->mDealerId); }
	function setDealerId($data) { $this->mDealerId = $data; }	
	
	var $mDealerDetail;
	function getDealerDetail() { return htmlspecialchars($this->mDealerDetail); }
	function setDealerDetail($data) { $this->mDealerDetail = $data; }		

	var $mStockCargoId;
	function getStockCargoId() { return htmlspecialchars($this->mStockCargoId); }
	function setStockCargoId($data) { $this->mStockCargoId = $data; }
	
	var $mDepartmentId;
	function getDepartmentId() { return $this->mDepartmentId; }
	function setDepartmentId($data) { $this->mDepartmentId = $data; }
	
	var $mDepartmentDetail;
	function getDepartmentDetail() { return $this->mDepartmentDetail; }
	function setDepartmentDetail($data) { $this->mDepartmentDetail = $data; }	
	
	var $mAskBy;
	function getAskBy() { return $this->mAskBy; }
	function setAskBy($data) { $this->mAskBy = $data; }

	var $mApproveBy;
	function getApproveBy() { return $this->mApproveBy; }
	function setApproveBy($data) { $this->mApproveBy = $data; }
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }	
	
	var $mStockType;
	function getStockType() { return $this->mStockType; }
	function setStockType($data) { $this->mStockType = $data; }		
	
	function Stock($objData=NULL) {
        If ($objData->stock_id !="") {
            $this->setStockId($objData->stock_id);
			$this->setStockDate($objData->stock_date);
			$this->setStockBy($objData->stock_by);
			$this->setStockNumber($objData->stock_number);
			$this->setOrderId($objData->order_id);
			$this->setStockCarId($objData->stock_car_id);
			$this->setPurchaseId($objData->purchase_id);
			$this->setStockPaperId($objData->stock_paper_id);
			$this->setStockPaperDetail($objData->stock_paper_detail);
			$this->setDealerId($objData->dealer_id);
			$this->setDealerDetail($objData->dealer_detail);
			$this->setStockCargoId($objData->stock_cargo_id);
			$this->setDepartmentId($objData->department_id);
			$this->setDepartmentDetail($objData->department_detail);
			$this->setAskBy($objData->ask_by);
			$this->setApproveBy($objData->approve_by);
			$this->setRemark($objData->remark);
			$this->setStatus($objData->status);
			$this->setStockType($objData->stock_type);
        }
    }

	function init(){
		$this->setStockNumber(stripslashes($this->mStockNumber));
		$this->setRemark(stripslashes($this->mRemark));
	}

	function load() {

		if ($this->mStockId == '') {
			return false;
		}
		$strSql = "SELECT S.*, SPA.title AS stock_paper_detail, D.title AS dealer_detail, DP.title AS department_detail FROM ".$this->TABLE." S "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
		."  WHERE stock_id =".$this->mStockId;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Stock($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT S.*, SPA.title AS stock_paper_detail, D.title AS dealer_detail, DP.title AS department_detail FROM ".$this->TABLE." S "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Stock($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_date, stock_by, order_id,stock_number, stock_car_id, purchase_id, stock_paper_id, dealer_id,  department_id, ask_by, approve_by, stock_type, status, remark ) "
						." VALUES ( '".$this->mStockDate
						."' , '".$this->mStockBy
						."', '".$this->mOrderId
						."', '".$this->mStockNumber
						."', '".$this->mStockCarId
						."', '".$this->mPurchaseId
						."' ,  '".$this->mStockPaperId
						."' , '".$this->mDealerId
						."' , '".$this->mDepartmentId
						."' , '".$this->mAskBy
						."' , '".$this->mApproveBy
						."' , '".$this->mStockType
						."' , '".$this->mStatus
						."' , '".$this->mRemark."'  ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockId = mysql_insert_id();
            return $this->mStockId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_date = '".$this->mStockDate."'  "
						." , stock_by = '".$this->mStockBy."'  "
						." , order_id = '".$this->mOrderId."'  "
						." , stock_number = '".$this->mStockNumber."'  "
						." , stock_car_id = '".$this->mStockCarId."'  "
						." , purchase_id = '".$this->mPurchaseId."'  "
						." , stock_paper_id = '".$this->mStockPaperId."'  "
						." , dealer_id = '".$this->mDealerId."'  "
						." , department_id = '".$this->mDepartmentId."'  "
						." , ask_by = '".$this->mAskBy."'  "
						." , approve_by = '".$this->mApproveBy."'  "
						." , stock_type = '".$this->mStockType."'  "
						." , status = '".$this->mStatus."'  "
						." , remark = '".$this->mRemark."'  "
						." WHERE  stock_id = ".$this->mStockId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_id=".$this->mStockId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_stock_item  "
        . " WHERE stock_id =".$this->mStockId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function checkDelete(){
        if ($this->mOrderId == 0)
       {
			return true;
        }
		return false;
	}

	 Function check($strMode)
    {
		global $langUserError;
		$asrErrReturn=array();
        Switch ( strtolower($strMode) )
        {
            Case "update":
            Case "add":
				
				if ($this->mStockNumber == "") {
					$asrErrReturn["stock_number"] = '��س��к��Ţ�����Ѻ�Թ���';
				}else{
					if(!$this->checkUniqueStockNumber(strtolower($strMode) )){
						$asrErrReturn["stock_number"] = '�Ţ�����Ѻ�Թ��ҫ�ӫ�͹';
					}		
				}				
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
    function checkUniqueStockNumber($strMode)
    {
        $strSql  = "SELECT stock_number FROM t_stock ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE stock_number = ".trim(strtoupper($this->convStr4SQL($this->mStockNumber)));
		}else{
			$strSql .= " WHERE stock_number = ".trim(strtoupper($this->convStr4SQL($this->mStockNumber)))." and stock_id != ".$this->mStockId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
}

/*********************************************************
		Class :				Customer Stock  List

		Last update :		22 Mar 02

		Description:		Customer Stock  List

*********************************************************/

class StockList extends DataList {
	var $TABLE = "t_stock";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_id) as rowCount FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT S.*, SPA.title AS stock_paper_detail, D.title AS dealer_detail, DP.title AS department_detail FROM ".$this->TABLE." S "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Stock($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_id) as rowCount FROM ".$this->TABLE." S  "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT S.*, SPA.title AS stock_paper_detail, D.title AS dealer_detail, DP.title AS department_detail FROM ".$this->TABLE." S "
		." LEFT JOIN t_stock_paper  SPA ON SPA.stock_paper_id = S.stock_paper_id  "
		." LEFT JOIN t_dealer  D ON D.dealer_id = S.dealer_id  "
		." LEFT JOIN t_department  DP ON DP.department_id = S.department_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Stock($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_mail_item_detail table

*********************************************************/
 
class InsureMailItemDetail extends DB{

	var $TABLE="t_insure_mail_item_detail";

	var $m_insure_mail_item_detail_id;
	function get_insure_mail_item_detail_id() { return $this->m_insure_mail_item_detail_id; }
	function set_insure_mail_item_detail_id($data) { $this->m_insure_mail_item_detail_id = $data; }
	
	var $m_insure_mail_item_id;
	function get_insure_mail_item_id() { return $this->m_insure_mail_item_id; }
	function set_insure_mail_item_id($data) { $this->m_insure_mail_item_id = $data; }
	
	var $m_insure_stock_id;
	function get_insure_stock_id() { return $this->m_insure_stock_id; }
	function set_insure_stock_id($data) { $this->m_insure_stock_id = $data; }

	var $m_stock_insure_id;
	function get_stock_insure_id() { return $this->m_stock_insure_id; }
	function set_stock_insure_id($data) { $this->m_stock_insure_id = $data; }
	
	var $m_bill;
	function get_bill() { return $this->m_bill; }
	function set_bill($data) { $this->m_bill = $data; }	
		
	var $m_pasee;
	function get_pasee() { return $this->m_pasee; }
	function set_pasee($data) { $this->m_pasee = $data; }			
		
	var $m_saluk;
	function get_saluk() { return $this->m_saluk; }
	function set_saluk($data) { $this->m_saluk = $data; }
		
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }	
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }
	
	var $m_recieve;
	function get_recieve() { return $this->m_recieve; }
	function set_recieve($data) { $this->m_recieve = $data; }	
	
	var $m_recieve_date;
	function get_recieve_date() { return $this->m_recieve_date; }
	function set_recieve_date($data) { $this->m_recieve_date = $data; }
	
	var $m_recieve_name;
	function get_recieve_name() { return $this->m_recieve_name; }
	function set_recieve_name($data) { $this->m_recieve_name = $data; }
	
	var $m_recieve_relate;
	function get_recieve_relate() { return $this->m_recieve_relate; }
	function set_recieve_relate($data) { $this->m_recieve_relate = $data; }

	var $m_cancel;
	function get_cancel() { return $this->m_cancel; }
	function set_cancel($data) { $this->m_cancel = $data; }
	
	var $m_cancel_date;
	function get_cancel_date() { return $this->m_cancel_date; }
	function set_cancel_date($data) { $this->m_cancel_date = $data; }
	
	var $m_cancel_remark;
	function get_cancel_remark() { return $this->m_cancel_remark; }
	function set_cancel_remark($data) { $this->m_cancel_remark = $data; }	
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }		
	
	var $m_weight;
	function get_weight() { return $this->m_weight; }
	function set_weight($data) { $this->m_weight = $data; }	
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }			
	
	var $m_postcode;
	function get_postcode() { return $this->m_postcode; }
	function set_postcode($data) { $this->m_postcode = $data; }	
	
	function InsureMailItemDetail($objData=NULL) {
        If ($objData->insure_mail_item_detail_id !=""  ) {
			$this->set_insure_mail_item_detail_id($objData->insure_mail_item_detail_id);
			$this->set_insure_mail_item_id($objData->insure_mail_item_id);
			$this->set_insure_stock_id($objData->insure_stock_id);
			$this->set_stock_insure_id($objData->stock_insure_id);
			$this->set_bill($objData->bill);
			$this->set_pasee($objData->pasee);
			$this->set_saluk($objData->saluk);
			$this->set_insure_id($objData->insure_id);
			$this->set_remark($objData->remark);
			$this->set_code($objData->code);
			$this->set_recieve($objData->recieve);
			$this->set_recieve_date($objData->recieve_date);
			$this->set_recieve_name($objData->recieve_name);
			$this->set_recieve_relate($objData->recieve_relate);
			$this->set_cancel($objData->cancel);
			$this->set_cancel_date($objData->cancel_date);
			$this->set_cancel_remark($objData->cancel_remark);
			$this->set_car_id($objData->car_id);
			$this->set_postcode($objData->postcode);
			$this->set_weight($objData->weight);
			$this->set_price($objData->price);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_mail_item_detail_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_mail_item_detail_id =".$this->m_insure_mail_item_detail_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureMailItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(insure_mail_item_detail_id) AS RSUM  FROM ".$this->TABLE
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
		
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT *  FROM ".$this->TABLE." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureMailItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_mail_item_id , insure_stock_id , stock_insure_id, bill, pasee, saluk, 
		 insure_id, code, remark ) " ." VALUES ( "
		." '".$this->m_insure_mail_item_id."' , "
		." '".$this->m_insure_stock_id."' , "
		." '".$this->m_stock_insure_id."' , "
		." '".$this->m_bill."' , "
		." '".$this->m_pasee."' , "
		." '".$this->m_saluk."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_code."' , "
		." '".$this->m_remark."'  "
		." ) ";


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_mail_item_detail_id = mysql_insert_id();
            return $this->m_insure_mail_item_detail_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  remark = '".$this->m_remark."' "
		." WHERE insure_mail_item_detail_id = ".$this->m_insure_mail_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateCode(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  code = '".$this->m_code."' "
		." WHERE insure_mail_item_detail_id = ".$this->m_insure_mail_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateRecieve(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  recieve = '".$this->m_recieve."' "
		." , recieve_date = '".$this->m_recieve_date."' "
		." , recieve_name = '".$this->m_recieve_name."' "
		." , recieve_relate = '".$this->m_recieve_relate."' "
		." WHERE insure_mail_item_detail_id = ".$this->m_insure_mail_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		

	
	function updateCancel(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  cancel = '".$this->m_cancel."' "
		." , cancel_date = '".$this->m_cancel_date."' "
		." , cancel_remark = '".$this->m_cancel_remark."' "
		." WHERE insure_mail_item_detail_id = ".$this->m_insure_mail_item_detail_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_mail_item_detail_id=".$this->m_insure_mail_item_detail_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureMailItemDetailList extends DataList {
	var $TABLE = "t_insure_mail_item_detail";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_mail_item_detail_id) as rowCount FROM ".$this->TABLE." O  "
			." LEFT JOIN t_insure_mail_item S ON S.insure_mail_item_id =  O.insure_mail_item_id  "
			." LEFT JOIN t_insure_stock SS ON SS.insure_stock_id =  O.insure_stock_id  "
			." LEFT JOIN t_insure I ON I.insure_id =  SS.insure_id  "
			." LEFT JOIN t_insure_car IC ON IC.car_id =  I.car_id  "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT O.* FROM ".$this->TABLE." O "
			." LEFT JOIN t_insure_mail_item S ON S.insure_mail_item_id =  O.insure_mail_item_id  "
			." LEFT JOIN t_insure_stock SS ON SS.insure_stock_id =  O.insure_stock_id  "
			." LEFT JOIN t_insure I ON I.insure_id =  SS.insure_id  "
			." LEFT JOIN t_insure_car IC ON IC.car_id =  I.car_id  "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id =  IC.insure_customer_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureMailItemDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->get_insure_mail_item_detail_id()."\"");
			if (($defaultId != null) && ($objItem->get_insure_mail_item_detail_id() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->get_title()."</option>");
		}
		echo("</select>");
	}
}
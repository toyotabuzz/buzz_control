<?
/*********************************************************
		Class :					Customer Contact 

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_contact table

*********************************************************/
 
class CustomerContact extends DB{

	var $TABLE="t_customer_contact";

	var $mCustomerContactId;
	function getCustomerContactId() { return $this->mCustomerContactId; }
	function setCustomerContactId($data) { $this->mCustomerContactId = $data; }	
	
	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }
	
	var $mContactId;
	function getContactId() { return $this->mContactId; }
	function setContactId($data) { $this->mContactId = $data; }
	
	var $mContactDate;
	function getContactDate() { return $this->mContactDate; }
	function setContactDate($data) { $this->mContactDate = $data; }
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mContactBy;
	function getContactBy() { return $this->mContactBy; }
	function setContactBy($data) { $this->mContactBy = $data; }
	
	function CustomerContact($objData=NULL) {
        If ($objData->customer_contact_id !="") {
            $this->setCustomerContactId($objData->customer_contact_id);
			$this->setCustomerId($objData->customer_id);
			$this->setContactId($objData->contact_id);
			$this->setContactBy($objData->contact_by);
			$this->setContactDate($objData->contact_date);
			$this->setRemark($objData->remark);
        }
    }

	function init(){
		$this->setRemark(stripslashes($this->mRemark));
	}
		
	function load() {

		if ($this->mCustomerContactId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE customer_contact_id =".$this->mCustomerContactId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerContact($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerContact($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( customer_id, contact_id, contact_by, contact_date, remark ) "
						." VALUES ( '".$this->mCustomerId."' , '".$this->mContactId."' , '".$this->mContactBy."' , '".date("Y-m-d H:i:s")."' , '".$this->mRemark."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mContactId = mysql_insert_id();
            return $this->mCustomerContactId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET customer_id = '".$this->mCustomerId."'  "
						." , contact_id = '".$this->mContactId."'  "
						." , contact_by = '".$this->mContactBy."'  "
						." , contact_date = '".$this->mContactDate."'  "
						." , remark = '".$this->mRemark."'  "
						." WHERE  customer_contact_id = ".$this->mCustomerContactId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE customer_contact_id=".$this->mCustomerContactId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		//if ($this->mContactBy == "") $asrErrReturn["title"] = "��س��к���Ǣ�͡�õԴ���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Contact List

		Last update :		22 Mar 02

		Description:		Customer Contact List

*********************************************************/

class CustomerContactList extends DataList {
	var $TABLE = "t_customer_contact";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT customer_contact_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerContact($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
}
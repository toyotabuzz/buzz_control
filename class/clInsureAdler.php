<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureAdler extends DB{

	var $TABLE="t_insure_adler";
	
	var $m_insure_adler_id;
	function get_insure_adler_id() { return $this->m_insure_adler_id; }
	function set_insure_adler_id($data) { $this->m_insure_adler_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_insure_quotation_id;
	function get_insure_quotation_id() { return $this->m_insure_quotation_id; }
	function set_insure_quotation_id($data) { $this->m_insure_quotation_id = $data; }
	
	var $m_add_date;
	function get_add_date() { return $this->m_add_date; }
	function set_add_date($data) { $this->m_add_date = $data; }
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	var $m_ref1;
	function get_ref1() { return $this->m_ref1; }
	function set_ref1($data) { $this->m_ref1 = $data; }
	
	var $m_ref2;
	function get_ref2() { return $this->m_ref2; }
	function set_ref2($data) { $this->m_ref2 = $data; }
	
	var $m_export_date;
	function get_export_date() { return $this->m_export_date; }
	function set_export_date($data) { $this->m_export_date = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_item1;
	function get_item1() { return $this->m_item1; }
	function set_item1($data) { $this->m_item1 = $data; }
	
	var $m_item1_check;
	function get_item1_check() { return $this->m_item1_check; }
	function set_item1_check($data) { $this->m_item1_check = $data; }
	
	var $m_item1_remark;
	function get_item1_remark() { return $this->m_item1_remark; }
	function set_item1_remark($data) { $this->m_item1_remark = $data; }
	
	var $m_item2;
	function get_item2() { return $this->m_item2; }
	function set_item2($data) { $this->m_item2 = $data; }
	
	var $m_item2_check;
	function get_item2_check() { return $this->m_item2_check; }
	function set_item2_check($data) { $this->m_item2_check = $data; }
	
	var $m_item2_remark;
	function get_item2_remark() { return $this->m_item2_remark; }
	function set_item2_remark($data) { $this->m_item2_remark = $data; }			
	
	var $m_item3;
	function get_item3() { return $this->m_item3; }
	function set_item3($data) { $this->m_item3 = $data; }
	
	var $m_item3_check;
	function get_item3_check() { return $this->m_item3_check; }
	function set_item3_check($data) { $this->m_item3_check = $data; }
	
	var $m_item3_remark;
	function get_item3_remark() { return $this->m_item3_remark; }
	function set_item3_remark($data) { $this->m_item3_remark = $data; }					
	
	var $m_show_sale;
	function get_show_sale() { return $this->m_show_sale; }
	function set_show_sale($data) { $this->m_show_sale = $data; }
	
	var $m_cancel_date;
	function get_cancel_date() { return $this->m_cancel_date; }
	function set_cancel_date($data) { $this->m_cancel_date = $data; }	
	
	function InsureAdler($objData=NULL) {
        If ($objData->insure_adler_id !="") {
			$this->set_insure_adler_id($objData->insure_adler_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_insure_quotation_id($objData->insure_quotation_id);
			$this->set_add_date($objData->add_date);
			$this->set_status($objData->status);
			$this->set_ref1($objData->ref1);
			$this->set_ref2($objData->ref2);
			$this->set_export_date($objData->export_date);
			$this->set_remark($objData->remark);
			$this->set_item1($objData->item1);
			$this->set_item1_check($objData->item1_check);
			$this->set_item1_remark($objData->item1_remark);
			$this->set_item2($objData->item2);
			$this->set_item2_check($objData->item2_check);
			$this->set_item2_remark($objData->item2_remark);
			$this->set_item3($objData->item3);
			$this->set_item3_check($objData->item3_check);
			$this->set_item3_remark($objData->item3_remark);
			$this->set_show_sale($objData->show_sale);
			$this->set_cancel_date($objData->cancel_date);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_adler_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			."   LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "
			."  WHERE insure_adler_id =".$this->m_insure_adler_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAdler($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT P.* FROM ".$this->TABLE." P "
			."   LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "		
			."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAdler($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_quotation_id , add_date , status , ref1 , ref2 , export_date , remark , item1, item2, item3 ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_insure_quotation_id."' , "
		." '".$this->m_add_date."' , "
		." '".$this->m_status."' , "
		." '".$this->m_ref1."' , "
		." '".$this->m_ref2."' , "
		." '".$this->m_export_date."' , "
		." '".$this->m_remark."', "
		." '".$this->m_item1."' , "
		." '".$this->m_item2."' , "
		." '".$this->m_item3."' "
		." ) ";
		

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_adler_id = mysql_insert_id();
            return $this->m_insure_adler_id;
        } else {
			return false;
	    }

	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , insure_quotation_id = '".$this->m_insure_quotation_id."' "
		." , add_date = '".$this->m_add_date."' "
		." , status = '".$this->m_status."' "
		." , ref1 = '".$this->m_ref1."' "
		." , ref2 = '".$this->m_ref2."' "
		." , export_date = '".$this->m_export_date."' "
		." , remark = '".$this->m_remark."' "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateItem1($strSql){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  item1_check = '".$this->m_item1_check."' "
		." , item1_remark = '".$this->m_item1_remark."' "
		." WHERE ".$strSql;

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateItem2($strSql){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  item2_check = '".$this->m_item2_check."' "
		." , item2_remark = '".$this->m_item2_remark."' "
		." WHERE ".$strSql;

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateItem3($strSql){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  item3_check = '".$this->m_item3_check."' "
		." , item3_remark = '".$this->m_item3_remark."' "
		." WHERE ".$strSql;

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	
	function update_status_item(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." item1_check = '".$this->m_item1_check."' , "
		." item2_check = '".$this->m_item2_check."' , "
		." item3_check = '".$this->m_item3_check."' , "
		." item1_remark = '".$this->m_item1_remark."' , "
		." item2_remark = '".$this->m_item2_remark."' , "
		." item3_remark = '".$this->m_item3_remark."' "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function update_status(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." status = '".$this->m_status."' "
		." , remark = '".$this->m_remark."' "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function update_status_cancel(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." status = '".$this->m_status."' "
		." , cancel_date = '".date("Y-m-d")."' "
		." , remark = '".$this->m_remark."' "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	

	function update_export(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." status = 'exported' , "
		." export_date = '".$this->m_export_date."' "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	function update_show_sale(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." show_sale = 'No'  "
		." WHERE insure_adler_id = ".$this->m_insure_adler_id." "; 	
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_adler_id=".$this->m_insure_adler_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureAdlerList extends DataList {
	var $TABLE = "t_insure_adler";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_adler_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureAdler($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
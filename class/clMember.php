<?
/*********************************************************
		Class :					Member

		Last update :	  10 Jan 02

		Description:	  Class manage t_member table

*********************************************************/
 
class Member extends User{

	var $TABLE="t_member";

	var $mMemberId;
	function getMemberId() { return $this->mMemberId; }
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mCode;
	function getCode() { return $this->mCode; }
	function setCode($data) { $this->mCode = $data; }
	
	var $mFirstname;
	function getFirstname() { return htmlspecialchars($this->mFirstname); }
	function setFirstname($data) { $this->mFirstname = $data; }
	
	var $mLastname;
	function getLastname() { return htmlspecialchars($this->mLastname); }
	function setLastname($data) { $this->mLastname = $data; }

	var $mNickname;
	function getNickname() { return htmlspecialchars($this->mNickname); }
	function setNickname($data) { $this->mNickname = $data; }
	
	var $mUsername;
	function getUsername() { return htmlspecialchars($this->mUsername); }
	function setUsername($data) { $this->mUsername = $data; }

	var $mPassword;
	function getPassword() { return htmlspecialchars($this->mPassword); }
	function setPassword($data) { $this->mPassword = $data; }

	var $mPassword1;
	function getPassword1() { return htmlspecialchars($this->mPassword1); }
	function setPassword1($data) { $this->mPassword1 = $data; }

	var $mSalt;
	function getSalt() { return htmlspecialchars($this->mSalt); }
	function setSalt($data) { $this->mSalt = $data; }

	var $mDateAdd;
	function getDateAdd() { return $this->mDateAdd; }
	function setDateAdd($data) { $this->mDateAdd = $data; }
	
	var $mEmail;
	function getEmail() { return htmlspecialchars($this->mEmail); }
	function setEmail($data) { $this->mEmail = $data; }
	
	var $mIDCard;
	function getIDCard() { return htmlspecialchars($this->mIDCard); }
	function setIDCard($data) { $this->mIDCard = $data; }
	
	var $mAddress;
	function getAddress() { return htmlspecialchars($this->mAddress); }
	function setAddress($data) { $this->mAddress = $data; }
	
	var $mTel;
	function getTel() { return htmlspecialchars($this->mTel); }
	function setTel($data) { $this->mTel = $data; }

	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	
	var $mDepartment;
	function getDepartment() { return $this->mDepartment; }
	function setDepartment($data) { $this->mDepartment = $data; }	
	
	var $mDepartmentDetail;
	function getDepartmentDetail() { return $this->mDepartmentDetail; }
	function setDepartmentDetail($data) { $this->mDepartmentDetail = $data; }		
	
	var $mPosition;
	function getPosition() { return $this->mPosition; }
	function setPosition($data) { $this->mPosition = $data; }
	function getPositionDetail() {
		if($this->mPosition == 1) return "������ü��Ѵ���"; 
		if($this->mPosition == 2) return "�ͧ������ü��Ѵ���"; 
		if($this->mPosition == 3) return "���Ѵ���"; 
		if($this->mPosition == 4) return "�����¼��Ѵ���"; 
		if($this->mPosition == 5) return "���Ѵ����Ң�"; 
		if($this->mPosition == 6) return "���˹��Ἱ�/���"; 
		if($this->mPosition == 7) return "��ѡ�ҹ���"; 
		if($this->mPosition == 8) return "��ѡ�ҹ��á��"; 

	}	
	
	var $mLevel;
	function getLevel() { return $this->mLevel; }
	function setLevel($data) { $this->mLevel = $data; }
	
	var $mTeam;
	function getTeam() { return $this->mTeam; }
	function setTeam($data) { $this->mTeam = $data; }	
	
	var $mTeamDetail;
	function getTeamDetail() { return $this->mTeamDetail; }
	function setTeamDetail($data) { $this->mTeamDetail = $data; }
	
	var $mPicture;
	function getPicture() { return $this->mPicture; }
	function setPicture($data) { $this->mPicture = $data; }	
	
	var $mStartDate;
	function getStartDate() { return $this->mStartDate; }
	function setStartDate($data) { $this->mStartDate = $data; }	
	
	var $mEndDate;
	function getEndDate() { return $this->mEndDate; }
	function setEndDate($data) { $this->mEndDate = $data; }	
	
	var $mRemark;
	function getRemark() { return $this->mRemark; }
	function setRemark($data) { $this->mRemark = $data; }	
	
	var $mDefaultProgram;
	function getDefaultProgram() { return $this->mDefaultProgram; }
	function setDefaultProgram($data) { $this->mDefaultProgram = $data; }	
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }		
	
	var $mInsureLicense;
	function getInsureLicense() { return $this->mInsureLicense; }
	function setInsureLicense($data) { $this->mInsureLicense = $data; }			
	
	var $mInsureEditor;
	function getInsureEditor() { return $this->mInsureEditor; }
	function setInsureEditor($data) { $this->mInsureEditor = $data; }			
	
	var $mInsureStatus;
	function getInsureStatus() { return $this->mInsureStatus; }
	function setInsureStatus($data) { $this->mInsureStatus = $data; }			
	
	var $mPhoneExt;
	function getPhoneExt() { return $this->mPhoneExt; }
	function setPhoneExt($data) { $this->mPhoneExt = $data; }

	var $mEmpCode;
	function getEmpCode() { return $this->mEmpCode; }
	function setEmpCode($data) { $this->mEmpCode = $data; }			
	
	function Member($objData=NULL) {
        If ($objData->member_id !="") {
            $this->setMemberId($objData->member_id);
			$this->setCode($objData->code);
			$this->setFirstname($objData->firstname);
			$this->setLastname($objData->lastname);
			$this->setNickname($objData->nickname);
			$this->setEmail($objData->email);
			$this->setIDCard($objData->id_card);
			$this->setAddress($objData->address);
			$this->setTel($objData->tel);
			$this->setUsername($objData->username);
			$this->setPassword($objData->password);
			$this->setSalt($objData->salt);
			$this->setDateAdd($objData->date_add);
			$this->setStatus($objData->status);
			$this->setDepartment($objData->department);
			$this->setDepartmentDetail($objData->department_detail);
			$this->setPosition($objData->position);
			$this->setLevel($objData->level);
			$this->setTeam($objData->team);
			$this->setTeamDetail($objData->team_detail);
			$this->setPicture($objData->picture);
			$this->setStartDate($objData->start_date);
			$this->setEndDate($objData->end_date);
			$this->setRemark($objData->remark);
			$this->setDefaultProgram($objData->default_program);
			$this->setCompanyId($objData->company_id);
			$this->setInsureLicense($objData->insure_license);
			$this->setInsureEditor($objData->insure_editor);
			$this->setInsureStatus($objData->insure_status);
			$this->setPhoneExt($objData->phone_ext);
			$this->setEmpCode($objData->emp_code);
        }
    }

	function init(){	
		$this->setFirstname(stripslashes($this->mFirstname));
		$this->setLastname(stripslashes($this->mLastname));
		$this->setEmail(stripslashes($this->mEmail));
		$this->setUsername(stripslashes($this->mUsername));
		$this->setPassword(stripslashes($this->mPassword));
	}
		
	function load() {

		if ($this->mMemberId == '') {
			return false;
		}
		$strSql = "SELECT M.*, D.title AS department_detail , T.title AS team_detail  FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		." LEFT JOIN t_team T ON T.team_id = M.team  "
		."  WHERE member_id =".$this->mMemberId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Member($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadUTF8() {

		if ($this->mMemberId == '') {
			return false;
		}
		$strSql = "SELECT M.*, D.title AS department_detail , T.title AS team_detail  FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		." LEFT JOIN t_team T ON T.team_id = M.team  "
		."  WHERE member_id =".$this->mMemberId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Member($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT M.*, D.title AS department_detail FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Member($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}		
	
	function loadMaxByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MAX(code) AS RSUM FROM ".$this->TABLE." M "
		."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}		
	
	function updateCondition($strSql) {
        $this->getConnection();
		//echo $strSql."<br>";
        return $this->query($strSql);
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( firstname, lastname, nickname, code, email, id_card, address, tel, date_add, username, password , department, position, team, picture, start_date, end_date, remark, default_program, company_id, status, emp_code ) "
						." VALUES ( '".$this->mFirstname."' , "
						."  '".$this->mLastname."' , "
						."  '".$this->mNickname."' , "
						."  '".$this->mCode."' , "
						."  '".$this->mEmail."' , "
						."  '".$this->mIDCard."' , "
						."  '".$this->mAddress."' , "
						."  '".$this->mTel."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mUsername."' , "
						."  '".$this->mPassword."' , "
						."  '".$this->mDepartment."' , "
						."  '".$this->mPosition."' , "
						."  '".$this->mTeam."' , "
						."  '".$this->mPicture."' , "
						."  '".$this->mStartDate."' , "
						."  '".$this->mEndDate."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mDefaultProgram."' , "
						."  '".$this->mCompanyId."' , "
						."  '".$this->mStatus."' , "
						."  '".$this->mEmpCode."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mMemberId = mysql_insert_id();
            return $this->mMemberId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET firstname = '".$this->mFirstname."' , "
						." lastname = '".$this->mLastname."' , "
						." nickname = '".$this->mNickname."' , "
						." code = '".$this->mCode."' , "
						." position = '".$this->mPosition."' , "
						." department = '".$this->mDepartment."' , "
						." status = '".$this->mStatus."' , "
						." email = '".$this->mEmail."' , "
						." id_card = '".$this->mIDCard."' , "
						." address = '".$this->mAddress."' , "
						." tel = '".$this->mTel."' , "
						." team = '".$this->mTeam."' , "
						." picture = '".$this->mPicture."' , "
						." start_date = '".$this->mStartDate."' , "
						." end_date = '".$this->mEndDate."' , "
						." remark = '".$this->mRemark."' , "
						." company_id = '".$this->mCompanyId."' , "
						." default_program = '".$this->mDefaultProgram."' , "
						." password = '".$this->mPassword."' , "
						." username = '".$this->mUsername."' , "
						." emp_code = '".$this->mEmpCode."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		// echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updatePersonal(){
		$strSql = "UPDATE ".$this->TABLE
						." SET firstname = '".$this->mFirstname."' , "
						." lastname = '".$this->mLastname."' , "
						." nickname = '".$this->mNickname."' , "
						." code = '".$this->mCode."' , "
						." position = '".$this->mPosition."' , "
						." department = '".$this->mDepartment."' , "
						." email = '".$this->mEmail."' , "
						." id_card = '".$this->mIDCard."' , "
						." address = '".$this->mAddress."' , "
						." tel = '".$this->mTel."' , "
						." team = '".$this->mTeam."' , "
						." picture = '".$this->mPicture."' , "
						." start_date = '".$this->mStartDate."' , "
						." company_id = '".$this->mCompanyId."' , "
						." remark = '".$this->mRemark."' , "
						." status = '".$this->mStatus."' , "
						." end_date = '".$this->mEndDate."' , "
						." emp_code = '".$this->mEmpCode."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		// echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateInsureEditor(){
		$strSql = "UPDATE ".$this->TABLE
						." SET insure_editor = '".$this->mInsureEditor."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function updateProfile(){
		$strSql = "UPDATE ".$this->TABLE
						." SET firstname = '".$this->mFirstname."' , "
						." lastname = '".$this->mLastname."' , "
						." nickname = '".$this->mNickname."' , "
						." email = '".$this->mEmail."' , "
						." insure_license = '".$this->mInsureLicense."' , "
						." id_card = '".$this->mIDCard."' , "
						." address = '".$this->mAddress."' , "
						." tel = '".$this->mTel."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateInsureProfile(){
		$strSql = "UPDATE ".$this->TABLE
						." SET firstname = '".$this->mFirstname."' , "
						." lastname = '".$this->mLastname."' , "
						." nickname = '".$this->mNickname."' , "
						." email = '".$this->mEmail."' , "
						." insure_license = '".$this->mInsureLicense."' , "
						." id_card = '".$this->mIDCard."' , "
						." tel = '".$this->mTel."' , "
						." phone_ext = '".$this->mPhoneExt."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	//���·��
	function updateTeam(){
		$strSql = "UPDATE ".$this->TABLE
						." SET team = '".$this->mTeam."' ,  "
						."  department = '".$this->mDepartment."' ,  "
						."  company_id = '".$this->mCompanyId."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updatePassword(){
		$strSql = "UPDATE ".$this->TABLE
						." SET password = '".$this->mPassword."' , "
						." username = '".$this->mUsername."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE
						." SET status = '".$this->mStatus."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateInsure(){
		$strSql = "UPDATE ".$this->TABLE
						." SET insure_status = '".$this->mInsureStatus."'  "
						." WHERE  member_id = ".$this->mMemberId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE member_id=".$this->mMemberId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_member_log "
       . " WHERE member_id=".$this->mMemberId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mFirstname == "") $asrErrReturn["firstname"] = "��س��кت���";
		if ($this->mLastname == "") $asrErrReturn["lastname"] = "��س��кع��ʡ��";

		//if ($this->mUsername == "") $asrErrReturn["username"] = "��س��к� username";
		//if ($this->mEmail == "") $asrErrReturn["email"] = "��س��к� email";
		//$arrEmail = $this->validateEmailFormat($this->mEmail);		
		/*
		if ($this->mPassword == "" or $this->mPassword1 == ""){
			$asrErrReturn["password"] = "��س��к� password";
		}else{
			if ($this->mPassword != $this->mPassword1) $asrErrReturn["password"] = " ��س��׹�ѹ���ʼ�ҹ Password ";
		}				
		*/
        Switch ( strtolower($Mode) )
        {
            Case "add":
				/*
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = "�������ӫ�͹��سҡ�͡����";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}				
				if ($this->checkUniqueUsername($Mode)) $asrErrReturn["username"] = "Username ��ӫ�͹��س��к�����";
				break;	
				*/
            Case "update":
				/*
				if ($arrEmail == ""){
					if ($this->checkUniqueEmail($Mode)) $asrErrReturn["email"] = "�������ӫ�͹��سҡ�͡����";
				}else{
					$asrErrReturn["email"] = $arrEmail;
				}	*/			
				if ($this->checkUniqueUsername($Mode)) $asrErrReturn["username"] = "Username ��ӫ�͹��س��к�����";
				
				break;	
            Case "delete":
                Break;
        }
        Return $asrErrReturn;
    }

	 Function checkPersonal($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mFirstname == "") $asrErrReturn["firstname"] = "��س��кت���";
		if ($this->mLastname == "") $asrErrReturn["lastname"] = "��س��кع��ʡ��";

        Switch ( strtolower($Mode) )
        {
            Case "add":

            Case "update":

				break;	
            Case "delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	
    function checkUniqueEmail($strMode)
    {
        $strSql  = "SELECT member_id FROM t_member";
		if ($strMode == "add"){
			$strSql .= " WHERE email = " .$this->convStr4SQL($this->mEmail);
		}else{
			$strSql .= " WHERE email = " .$this->convStr4SQL($this->mEmail)." and member_id != ".$this->mMemberId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Email found.
            {
				return $row->member_id;
			}
        }
        return false;
    }

    function checkUniqueUsername($strMode)
    {
        $strSql  = "SELECT member_id FROM t_member";
		if ($strMode == "add"){
			$strSql .= " WHERE username = " .$this->convStr4SQL($this->mUsername);
		}else{
			$strSql .= " WHERE username = " .$this->convStr4SQL($this->mUsername)." and member_id != ".$this->mMemberId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				return $row->member_id;
			}
        }
        return false;
    }

	function checkLogin($userName, $password){
		$strSql = "SELECT * FROM t_member WHERE username = '".$userName."' and password = '".$password."' ";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Member($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

    function auth($username, $password, &$error)
    /*Authentications user.
    *   1. returns boolean.
    *   2. sets $error with error message.*/
    {
		global $langUserError;
        $error = '';
        if ($username == "")        //User not enter Username
        {
            $error = $langUserError[authUsernameEmpty];
            return false;
        }
        
        $strSql  = "SELECT * FROM t_member ";
        $strSql .= " WHERE username = '" .$username."'  and password =  '" .$password."'   ";
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
              $this->setMemberId($row->member_id);
              $strHashValue = $row->password;
              $strSalt = $row->salt;
              //Calculate Hash of specified password + Salt from DB
			  
              $strHashValue2  = $row->password;

              if ( $strHashValue == $strHashValue2 ) //Password is correct.
              {
                $this->setMemberId($row->member_id);
				$this->load();
                if ($this->mStatus == 0)    //Account Disabled
                {
                    $error = $langUserError[authAccountDisabled];
                } else {
                    return true;
                }
              }
              else  //Invalid password.
              {
				$error = $langUserError[authInvalidPassword];
              }
            }
            else // Username not found.
            {
              $error = $langUserError[authUsernameNotExist]; //user does not exist
            }
        }
		if (!empty($error)) {
			return false;
		} else {
			return true;
		}
    }	
	
	
	
    function login()
    {
        global $sMemberId;
        if ($this->mMemberId != "")
        {
			// register user Id
            $this->setSessionVar("sMemberId",$this->mMemberId);
        }
    }

    function logout()
    {
        global $sMemberId;
        session_destroy();
    }

    function isLogged()
    {
        $lMemberId = $this->getSessionVar('sMemberId');
        if ($lMemberId == 0 || empty($lMemberId))
        {
            return false;
        }
        else    //Session or Cookies is not empty
        {
            $this->mMemberId = $lMemberId;
			// $this->mLevel = $this->getSessionVar('sUserLevel');
            return true;
        }
    }

    function isRight($sess)
    {
		if($sess <=	3){
			return true;
		}else{
			return false;
		}
    }	
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}	
	
	
}

/*********************************************************
		Class :				MemberList

		Last update :		22 Mar 02

		Description:		Member user list

*********************************************************/


class MemberList extends DataList {
	var $TABLE = "t_member";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_id) as rowCount FROM ".$this->TABLE." M  "
			." LEFT JOIN t_department D ON D.department_id = M.department  "
			." LEFT JOIN t_team T ON T.team_id = M.team  "
			.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		//exit;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT M.*, D.title AS department_detail , T.title AS team_detail  FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		." LEFT JOIN t_team T ON T.team_id = M.team  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		//exit;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Member($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Member($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function loadOrderBooking() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_id) as rowCount FROM ".$this->TABLE." M  "
			." LEFT JOIN t_department D ON D.department_id = M.department  "
			." LEFT JOIN t_team T ON T.team_id = M.team  "
			." LEFT JOIN t_order O ON M.member_id = O.booking_sale_id  "
			.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		//exit;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT DISTINCT M.*, D.title AS department_detail , T.title AS team_detail  FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		." LEFT JOIN t_team T ON T.team_id = M.team  "
		." LEFT JOIN t_order O ON M.member_id = O.booking_sale_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		//exit;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Member($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function loadOrderSending() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT member_id) as rowCount FROM ".$this->TABLE." M  "
			." LEFT JOIN t_department D ON D.department_id = M.department  "
			." LEFT JOIN t_team T ON T.team_id = M.team  "
			." LEFT JOIN t_order O ON M.member_id = O.sending_sale_id  "
			.$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		//exit;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT DISTINCT M.*, D.title AS department_detail , T.title AS team_detail  FROM ".$this->TABLE." M "
		." LEFT JOIN t_department D ON D.department_id = M.department  "
		." LEFT JOIN t_team T ON T.team_id = M.team  "
		." LEFT JOIN t_order O ON M.member_id = O.sending_sale_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		//exit;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Member($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getMemberId()."\"");
			if (($defaultId != null) && ($objItem->getMemberId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getDepartmentDetail()." : ".$objItem->getTeamDetail()." : ".$objItem->getFirstname()." ( ".$objItem->getNickname().") </option>");
		}
		echo("</select>");
	}			
	
	function printSelectSale($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getMemberId()."\"");
			if (($defaultId != null) && ($objItem->getMemberId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getNickname()."</option>");
		}
		echo("</select>");
	}			

}
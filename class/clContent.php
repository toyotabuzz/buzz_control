<?
/*********************************************************
		Class :				Content

		Last update :		20 Nov 02

		Description:		Class manage Content table

*********************************************************/

class Content extends DB{

var $TABLE="t_content";

var $m_content_id;
function get_content_id() { return $this->m_content_id; }
function set_content_id($data) { $this->m_content_id = $data; }

var $m_headline_id;
function get_headline_id() { return $this->m_headline_id; }
function set_headline_id($data) { $this->m_headline_id = $data; }

var $m_other_type;
function get_other_type() { return $this->m_other_type; }
function set_other_type($data) { $this->m_other_type = $data; }

var $m_rank;
function get_rank() { return $this->m_rank; }
function set_rank($data) { $this->m_rank = $data; }

var $m_title_01;
function get_title_01() { return stripslashes($this->m_title_01); }
function set_title_01($data) { $this->m_title_01 = $data; }

var $m_title_02;
function get_title_02() { return stripslashes($this->m_title_02); }
function set_title_02($data) { $this->m_title_02 = $data; }

var $m_title_03;
function get_title_03() { return stripslashes($this->m_title_03); }
function set_title_03($data) { $this->m_title_03 = $data; }

var $m_title_04;
function get_title_04() { return stripslashes($this->m_title_04); }
function set_title_04($data) { $this->m_title_04 = $data; }

var $m_title_05;
function get_title_05() { return stripslashes($this->m_title_05); }
function set_title_05($data) { $this->m_title_05 = $data; }

var $m_title_06;
function get_title_06() { return stripslashes($this->m_title_06); }
function set_title_06($data) { $this->m_title_06 = $data; }

var $m_title_07;
function get_title_07() { return stripslashes($this->m_title_07); }
function set_title_07($data) { $this->m_title_07 = $data; }

var $m_title_08;
function get_title_08() { return stripslashes($this->m_title_08); }
function set_title_08($data) { $this->m_title_08 = $data; }

var $m_title_09;
function get_title_09() { return stripslashes($this->m_title_09); }
function set_title_09($data) { $this->m_title_09 = $data; }

var $m_title_10;
function get_title_10() { return stripslashes($this->m_title_10); }
function set_title_10($data) { $this->m_title_10 = $data; }

var $m_title_11;
function get_title_11() { return stripslashes($this->m_title_11); }
function set_title_11($data) { $this->m_title_11 = $data; }

var $m_title_12;
function get_title_12() { return stripslashes($this->m_title_12); }
function set_title_12($data) { $this->m_title_12 = $data; }

var $m_title_13;
function get_title_13() { return stripslashes($this->m_title_13); }
function set_title_13($data) { $this->m_title_13 = $data; }

var $m_title_14;
function get_title_14() { return stripslashes($this->m_title_14); }
function set_title_14($data) { $this->m_title_14 = $data; }

var $m_title_15;
function get_title_15() { return stripslashes($this->m_title_15); }
function set_title_15($data) { $this->m_title_15 = $data; }

var $m_title_16;
function get_title_16() { return stripslashes($this->m_title_16); }
function set_title_16($data) { $this->m_title_16 = $data; }

var $m_title_17;
function get_title_17() { return stripslashes($this->m_title_17); }
function set_title_17($data) { $this->m_title_17 = $data; }

var $m_title_18;
function get_title_18() { return stripslashes($this->m_title_18); }
function set_title_18($data) { $this->m_title_18 = $data; }

var $m_title_19;
function get_title_19() { return stripslashes($this->m_title_19); }
function set_title_19($data) { $this->m_title_19 = $data; }

var $m_title_20;
function get_title_20() { return stripslashes($this->m_title_20); }
function set_title_20($data) { $this->m_title_20 = $data; }


var $m_content_01;
function get_content_01() { return stripslashes($this->m_content_01); }
function set_content_01($data) { $this->m_content_01 = $data; }

var $m_content_02;
function get_content_02() { return stripslashes($this->m_content_02); }
function set_content_02($data) { $this->m_content_02 = $data; }

var $m_content_03;
function get_content_03() { return stripslashes($this->m_content_03); }
function set_content_03($data) { $this->m_content_03 = $data; }

var $m_content_04;
function get_content_04() { return stripslashes($this->m_content_04); }
function set_content_04($data) { $this->m_content_04 = $data; }

var $m_content_05;
function get_content_05() { return stripslashes($this->m_content_05); }
function set_content_05($data) { $this->m_content_05 = $data; }

var $m_content_06;
function get_content_06() { return stripslashes($this->m_content_06); }
function set_content_06($data) { $this->m_content_06 = $data; }

var $m_content_07;
function get_content_07() { return stripslashes($this->m_content_07); }
function set_content_07($data) { $this->m_content_07 = $data; }

var $m_content_08;
function get_content_08() { return stripslashes($this->m_content_08); }
function set_content_08($data) { $this->m_content_08 = $data; }

var $m_content_09;
function get_content_09() { return stripslashes($this->m_content_09); }
function set_content_09($data) { $this->m_content_09 = $data; }

var $m_content_10;
function get_content_10() { return stripslashes($this->m_content_10); }
function set_content_10($data) { $this->m_content_10 = $data; }

var $m_content_11;
function get_content_11() { return stripslashes($this->m_content_11); }
function set_content_11($data) { $this->m_content_11 = $data; }

var $m_content_12;
function get_content_12() { return stripslashes($this->m_content_12); }
function set_content_12($data) { $this->m_content_12 = $data; }

var $m_content_13;
function get_content_13() { return stripslashes($this->m_content_13); }
function set_content_13($data) { $this->m_content_13 = $data; }

var $m_content_14;
function get_content_14() { return stripslashes($this->m_content_14); }
function set_content_14($data) { $this->m_content_14 = $data; }

var $m_content_15;
function get_content_15() { return stripslashes($this->m_content_15); }
function set_content_15($data) { $this->m_content_15 = $data; }

var $m_content_16;
function get_content_16() { return stripslashes($this->m_content_16); }
function set_content_16($data) { $this->m_content_16 = $data; }

var $m_content_17;
function get_content_17() { return stripslashes($this->m_content_17); }
function set_content_17($data) { $this->m_content_17 = $data; }

var $m_content_18;
function get_content_18() { return stripslashes($this->m_content_18); }
function set_content_18($data) { $this->m_content_18 = $data; }

var $m_content_19;
function get_content_19() { return stripslashes($this->m_content_19); }
function set_content_19($data) { $this->m_content_19 = $data; }

var $m_content_20;
function get_content_20() { return stripslashes($this->m_content_20); }
function set_content_20($data) { $this->m_content_20 = $data; }


var $m_picture_01;
function get_picture_01() { return $this->m_picture_01; }
function set_picture_01($data) { $this->m_picture_01 = $data; }

var $m_picture_02;
function get_picture_02() { return $this->m_picture_02; }
function set_picture_02($data) { $this->m_picture_02 = $data; }

var $m_picture_03;
function get_picture_03() { return $this->m_picture_03; }
function set_picture_03($data) { $this->m_picture_03 = $data; }

var $m_picture_04;
function get_picture_04() { return $this->m_picture_04; }
function set_picture_04($data) { $this->m_picture_04 = $data; }

var $m_picture_05;
function get_picture_05() { return $this->m_picture_05; }
function set_picture_05($data) { $this->m_picture_05 = $data; }

var $m_picture_06;
function get_picture_06() { return $this->m_picture_06; }
function set_picture_06($data) { $this->m_picture_06 = $data; }

var $m_picture_07;
function get_picture_07() { return $this->m_picture_07; }
function set_picture_07($data) { $this->m_picture_07 = $data; }

var $m_picture_08;
function get_picture_08() { return $this->m_picture_08; }
function set_picture_08($data) { $this->m_picture_08 = $data; }

var $m_picture_09;
function get_picture_09() { return $this->m_picture_09; }
function set_picture_09($data) { $this->m_picture_09 = $data; }

var $m_picture_10;
function get_picture_10() { return $this->m_picture_10; }
function set_picture_10($data) { $this->m_picture_10 = $data; }

var $m_picture_11;
function get_picture_11() { return $this->m_picture_11; }
function set_picture_11($data) { $this->m_picture_11 = $data; }

var $m_picture_12;
function get_picture_12() { return $this->m_picture_12; }
function set_picture_12($data) { $this->m_picture_12 = $data; }

var $m_picture_13;
function get_picture_13() { return $this->m_picture_13; }
function set_picture_13($data) { $this->m_picture_13 = $data; }

var $m_picture_14;
function get_picture_14() { return $this->m_picture_14; }
function set_picture_14($data) { $this->m_picture_14 = $data; }

var $m_picture_15;
function get_picture_15() { return $this->m_picture_15; }
function set_picture_15($data) { $this->m_picture_15 = $data; }

var $m_picture_16;
function get_picture_16() { return $this->m_picture_16; }
function set_picture_16($data) { $this->m_picture_16 = $data; }

var $m_picture_17;
function get_picture_17() { return $this->m_picture_17; }
function set_picture_17($data) { $this->m_picture_17 = $data; }

var $m_picture_18;
function get_picture_18() { return $this->m_picture_18; }
function set_picture_18($data) { $this->m_picture_18 = $data; }

var $m_picture_19;
function get_picture_19() { return $this->m_picture_19; }
function set_picture_19($data) { $this->m_picture_19 = $data; }

var $m_picture_20;
function get_picture_20() { return $this->m_picture_20; }
function set_picture_20($data) { $this->m_picture_20 = $data; }

var $m_picture_title_01;
function get_picture_title_01() { return stripslashes($this->m_picture_title_01); }
function set_picture_title_01($data) { $this->m_picture_title_01 = $data; }

var $m_picture_title_02;
function get_picture_title_02() { return stripslashes($this->m_picture_title_02); }
function set_picture_title_02($data) { $this->m_picture_title_02 = $data; }

var $m_picture_title_03;
function get_picture_title_03() { return stripslashes($this->m_picture_title_03); }
function set_picture_title_03($data) { $this->m_picture_title_03 = $data; }

var $m_picture_title_04;
function get_picture_title_04() { return stripslashes($this->m_picture_title_04); }
function set_picture_title_04($data) { $this->m_picture_title_04 = $data; }

var $m_picture_title_05;
function get_picture_title_05() { return stripslashes($this->m_picture_title_05); }
function set_picture_title_05($data) { $this->m_picture_title_05 = $data; }

var $m_picture_title_06;
function get_picture_title_06() { return stripslashes($this->m_picture_title_06); }
function set_picture_title_06($data) { $this->m_picture_title_06 = $data; }

var $m_picture_title_07;
function get_picture_title_07() { return stripslashes($this->m_picture_title_07); }
function set_picture_title_07($data) { $this->m_picture_title_07 = $data; }

var $m_picture_title_08;
function get_picture_title_08() { return stripslashes($this->m_picture_title_08); }
function set_picture_title_08($data) { $this->m_picture_title_08 = $data; }

var $m_picture_title_09;
function get_picture_title_09() { return stripslashes($this->m_picture_title_09); }
function set_picture_title_09($data) { $this->m_picture_title_09 = $data; }

var $m_picture_title_10;
function get_picture_title_10() { return stripslashes($this->m_picture_title_10); }
function set_picture_title_10($data) { $this->m_picture_title_10 = $data; }

var $m_picture_title_11;
function get_picture_title_11() { return stripslashes($this->m_picture_title_11); }
function set_picture_title_11($data) { $this->m_picture_title_11 = $data; }

var $m_picture_title_12;
function get_picture_title_12() { return stripslashes($this->m_picture_title_12); }
function set_picture_title_12($data) { $this->m_picture_title_12 = $data; }

var $m_picture_title_13;
function get_picture_title_13() { return stripslashes($this->m_picture_title_13); }
function set_picture_title_13($data) { $this->m_picture_title_13 = $data; }

var $m_picture_title_14;
function get_picture_title_14() { return stripslashes($this->m_picture_title_14); }
function set_picture_title_14($data) { $this->m_picture_title_14 = $data; }

var $m_picture_title_15;
function get_picture_title_15() { return stripslashes($this->m_picture_title_15); }
function set_picture_title_15($data) { $this->m_picture_title_15 = $data; }

var $m_picture_title_16;
function get_picture_title_16() { return stripslashes($this->m_picture_title_16); }
function set_picture_title_16($data) { $this->m_picture_title_16 = $data; }

var $m_picture_title_17;
function get_picture_title_17() { return stripslashes($this->m_picture_title_17); }
function set_picture_title_17($data) { $this->m_picture_title_17 = $data; }

var $m_picture_title_18;
function get_picture_title_18() { return stripslashes($this->m_picture_title_18); }
function set_picture_title_18($data) { $this->m_picture_title_18 = $data; }

var $m_picture_title_19;
function get_picture_title_19() { return stripslashes($this->m_picture_title_19); }
function set_picture_title_19($data) { $this->m_picture_title_19 = $data; }

var $m_picture_title_20;
function get_picture_title_20() { return stripslashes($this->m_picture_title_20); }
function set_picture_title_20($data) { $this->m_picture_title_20 = $data; }

var $m_picture_resize_01;
function get_picture_resize_01() { return $this->m_picture_resize_01; }
function set_picture_resize_01($data) { $this->m_picture_resize_01 = $data; }

var $m_picture_resize_02;
function get_picture_resize_02() { return $this->m_picture_resize_02; }
function set_picture_resize_02($data) { $this->m_picture_resize_02 = $data; }

var $m_picture_resize_03;
function get_picture_resize_03() { return $this->m_picture_resize_03; }
function set_picture_resize_03($data) { $this->m_picture_resize_03 = $data; }

var $m_picture_resize_04;
function get_picture_resize_04() { return $this->m_picture_resize_04; }
function set_picture_resize_04($data) { $this->m_picture_resize_04 = $data; }

var $m_picture_resize_05;
function get_picture_resize_05() { return $this->m_picture_resize_05; }
function set_picture_resize_05($data) { $this->m_picture_resize_05 = $data; }

var $m_picture_resize_06;
function get_picture_resize_06() { return $this->m_picture_resize_06; }
function set_picture_resize_06($data) { $this->m_picture_resize_06 = $data; }

var $m_picture_resize_07;
function get_picture_resize_07() { return $this->m_picture_resize_07; }
function set_picture_resize_07($data) { $this->m_picture_resize_07 = $data; }

var $m_picture_resize_08;
function get_picture_resize_08() { return $this->m_picture_resize_08; }
function set_picture_resize_08($data) { $this->m_picture_resize_08 = $data; }

var $m_picture_resize_09;
function get_picture_resize_09() { return $this->m_picture_resize_09; }
function set_picture_resize_09($data) { $this->m_picture_resize_09 = $data; }

var $m_picture_resize_10;
function get_picture_resize_10() { return $this->m_picture_resize_10; }
function set_picture_resize_10($data) { $this->m_picture_resize_10 = $data; }

var $m_picture_resize_11;
function get_picture_resize_11() { return $this->m_picture_resize_11; }
function set_picture_resize_11($data) { $this->m_picture_resize_11 = $data; }

var $m_picture_resize_12;
function get_picture_resize_12() { return $this->m_picture_resize_12; }
function set_picture_resize_12($data) { $this->m_picture_resize_12 = $data; }

var $m_picture_resize_13;
function get_picture_resize_13() { return $this->m_picture_resize_13; }
function set_picture_resize_13($data) { $this->m_picture_resize_13 = $data; }

var $m_picture_resize_14;
function get_picture_resize_14() { return $this->m_picture_resize_14; }
function set_picture_resize_14($data) { $this->m_picture_resize_14 = $data; }

var $m_picture_resize_15;
function get_picture_resize_15() { return $this->m_picture_resize_15; }
function set_picture_resize_15($data) { $this->m_picture_resize_15 = $data; }

var $m_picture_resize_16;
function get_picture_resize_16() { return $this->m_picture_resize_16; }
function set_picture_resize_16($data) { $this->m_picture_resize_16 = $data; }

var $m_picture_resize_17;
function get_picture_resize_17() { return $this->m_picture_resize_17; }
function set_picture_resize_17($data) { $this->m_picture_resize_17 = $data; }

var $m_picture_resize_18;
function get_picture_resize_18() { return $this->m_picture_resize_18; }
function set_picture_resize_18($data) { $this->m_picture_resize_18 = $data; }

var $m_picture_resize_19;
function get_picture_resize_19() { return $this->m_picture_resize_19; }
function set_picture_resize_19($data) { $this->m_picture_resize_19 = $data; }

var $m_picture_resize_20;
function get_picture_resize_20() { return $this->m_picture_resize_20; }
function set_picture_resize_20($data) { $this->m_picture_resize_20 = $data; }

var $m_content_type;
function get_content_type() { return $this->m_content_type; }
function set_content_type($data) { $this->m_content_type = $data; }


	function Content($objData=NULL) {
        If ($objData->content_id !="") {
		$this->set_content_id($objData->content_id);
		$this->set_headline_id($objData->headline_id);
		$this->set_other_type($objData->other_type);
		$this->set_rank($objData->rank);
		$this->set_title_01($objData->title_01);
		$this->set_title_02($objData->title_02);
		$this->set_title_03($objData->title_03);
		$this->set_title_04($objData->title_04);
		$this->set_title_05($objData->title_05);
		$this->set_title_06($objData->title_06);
		$this->set_title_07($objData->title_07);
		$this->set_title_08($objData->title_08);
		$this->set_title_09($objData->title_09);
		$this->set_title_10($objData->title_10);
		$this->set_content_01($objData->content_01);
		$this->set_content_02($objData->content_02);
		$this->set_content_03($objData->content_03);
		$this->set_content_04($objData->content_04);
		$this->set_content_05($objData->content_05);
		$this->set_content_06($objData->content_06);
		$this->set_content_07($objData->content_07);
		$this->set_content_08($objData->content_08);
		$this->set_content_09($objData->content_09);
		$this->set_content_10($objData->content_10);
		$this->set_picture_01($objData->picture_01);
		$this->set_picture_02($objData->picture_02);
		$this->set_picture_03($objData->picture_03);
		$this->set_picture_04($objData->picture_04);
		$this->set_picture_05($objData->picture_05);
		$this->set_picture_06($objData->picture_06);
		$this->set_picture_07($objData->picture_07);
		$this->set_picture_08($objData->picture_08);
		$this->set_picture_09($objData->picture_09);
		$this->set_picture_10($objData->picture_10);
		$this->set_picture_title_01($objData->picture_title_01);
		$this->set_picture_title_02($objData->picture_title_02);
		$this->set_picture_title_03($objData->picture_title_03);
		$this->set_picture_title_04($objData->picture_title_04);
		$this->set_picture_title_05($objData->picture_title_05);
		$this->set_picture_title_06($objData->picture_title_06);
		$this->set_picture_title_07($objData->picture_title_07);
		$this->set_picture_title_08($objData->picture_title_08);
		$this->set_picture_title_09($objData->picture_title_09);
		$this->set_picture_title_10($objData->picture_title_10);
		$this->set_picture_resize_01($objData->picture_resize_01);
		$this->set_picture_resize_02($objData->picture_resize_02);
		$this->set_picture_resize_03($objData->picture_resize_03);
		$this->set_picture_resize_04($objData->picture_resize_04);
		$this->set_picture_resize_05($objData->picture_resize_05);
		$this->set_picture_resize_06($objData->picture_resize_06);
		$this->set_picture_resize_07($objData->picture_resize_07);
		$this->set_picture_resize_08($objData->picture_resize_08);
		$this->set_picture_resize_09($objData->picture_resize_09);
		$this->set_picture_resize_10($objData->picture_resize_10);
		
		$this->set_title_11($objData->title_11);
		$this->set_title_12($objData->title_12);
		$this->set_title_13($objData->title_13);
		$this->set_title_14($objData->title_14);
		$this->set_title_15($objData->title_15);
		$this->set_title_16($objData->title_16);
		$this->set_title_17($objData->title_17);
		$this->set_title_18($objData->title_18);
		$this->set_title_19($objData->title_19);
		$this->set_title_20($objData->title_20);
		$this->set_content_11($objData->content_11);
		$this->set_content_12($objData->content_12);
		$this->set_content_13($objData->content_13);
		$this->set_content_14($objData->content_14);
		$this->set_content_15($objData->content_15);
		$this->set_content_16($objData->content_16);
		$this->set_content_17($objData->content_17);
		$this->set_content_18($objData->content_18);
		$this->set_content_19($objData->content_19);
		$this->set_content_20($objData->content_20);
		$this->set_picture_11($objData->picture_11);
		$this->set_picture_12($objData->picture_12);
		$this->set_picture_13($objData->picture_13);
		$this->set_picture_14($objData->picture_14);
		$this->set_picture_15($objData->picture_15);
		$this->set_picture_16($objData->picture_16);
		$this->set_picture_17($objData->picture_17);
		$this->set_picture_18($objData->picture_18);
		$this->set_picture_19($objData->picture_19);
		$this->set_picture_20($objData->picture_20);
		$this->set_picture_title_11($objData->picture_title_11);
		$this->set_picture_title_12($objData->picture_title_12);
		$this->set_picture_title_13($objData->picture_title_13);
		$this->set_picture_title_14($objData->picture_title_14);
		$this->set_picture_title_15($objData->picture_title_15);
		$this->set_picture_title_16($objData->picture_title_16);
		$this->set_picture_title_17($objData->picture_title_17);
		$this->set_picture_title_18($objData->picture_title_18);
		$this->set_picture_title_19($objData->picture_title_19);
		$this->set_picture_title_20($objData->picture_title_20);
		$this->set_picture_resize_11($objData->picture_resize_11);
		$this->set_picture_resize_12($objData->picture_resize_12);
		$this->set_picture_resize_13($objData->picture_resize_13);
		$this->set_picture_resize_14($objData->picture_resize_14);
		$this->set_picture_resize_15($objData->picture_resize_15);
		$this->set_picture_resize_16($objData->picture_resize_16);
		$this->set_picture_resize_17($objData->picture_resize_17);
		$this->set_picture_resize_18($objData->picture_resize_18);
		$this->set_picture_resize_19($objData->picture_resize_19);
		$this->set_picture_resize_20($objData->picture_resize_20);
		
		$this->set_content_type($objData->content_type);
        }
    }

	function init(){	
		$this->set_title(stripslashes($this->m_title));
	}
	
	function load() {

		if ($this->m_content_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE content_id = '".$this->m_content_id."'";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Content($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}

	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Content($row);
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." (  headline_id , rank ,other_type
		, title_01 , title_02 , title_03 , title_04 , title_05 , title_06 , title_07 , title_08 , title_09 , title_10 
		, content_01 , content_02 , content_03 , content_04 , content_05 , content_06 , content_07 , content_08 , content_09 , content_10 
		, picture_01 , picture_02 , picture_03 , picture_04 , picture_05 , picture_06 , picture_07 , picture_08 , picture_09 , picture_10 
		, picture_title_01 , picture_title_02 , picture_title_03 , picture_title_04 , picture_title_05 , picture_title_06 , picture_title_07 , picture_title_08 , picture_title_09 , picture_title_10 
		, picture_resize_01 , picture_resize_02 , picture_resize_03 , picture_resize_04 , picture_resize_05 , picture_resize_06 , picture_resize_07 , picture_resize_08 , picture_resize_09, picture_resize_10 
		, title_11 , title_12 , title_13 , title_14 , title_15 , title_16 , title_17 , title_18 , title_19 , title_20 
		, content_11 , content_12 , content_13 , content_14 , content_15 , content_16 , content_17 , content_18 , content_19 , content_20 
		, picture_11 , picture_12 , picture_13 , picture_14 , picture_15 , picture_16 , picture_17 , picture_18 , picture_19 , picture_20 
		, picture_title_11 , picture_title_12 , picture_title_13 , picture_title_14 , picture_title_15 , picture_title_16 , picture_title_17 , picture_title_18 , picture_title_19 , picture_title_20 
		, picture_resize_11 , picture_resize_12 , picture_resize_13 , picture_resize_14 , picture_resize_15 , picture_resize_16 , picture_resize_17 , picture_resize_18 , picture_resize_19, picture_resize_20 , content_type ) "


		." VALUES ( "
		." '".$this->m_headline_id."' , "
		." '".$this->m_rank."' , "
		." '".$this->m_other_type."' , "
		." '".addslashes($this->m_title_01)."' , "
		." '".addslashes($this->m_title_02)."' , "
		." '".addslashes($this->m_title_03)."' , "
		." '".addslashes($this->m_title_04)."' , "
		." '".addslashes($this->m_title_05)."' , "
		." '".addslashes($this->m_title_06)."' , "
		." '".addslashes($this->m_title_07)."' , "
		." '".addslashes($this->m_title_08)."' , "
		." '".addslashes($this->m_title_09)."' , "
		." '".addslashes($this->m_title_10)."' , "
		." '".addslashes($this->m_content_01)."' , "
		." '".addslashes($this->m_content_02)."' , "
		." '".addslashes($this->m_content_03)."' , "
		." '".addslashes($this->m_content_04)."' , "
		." '".addslashes($this->m_content_05)."' , "
		." '".addslashes($this->m_content_06)."' , "
		." '".addslashes($this->m_content_07)."' , "
		." '".addslashes($this->m_content_08)."' , "
		." '".addslashes($this->m_content_09)."' , "
		." '".addslashes($this->m_content_10)."' , "
		." '".$this->m_picture_01."' , "
		." '".$this->m_picture_02."' , "
		." '".$this->m_picture_03."' , "
		." '".$this->m_picture_04."' , "
		." '".$this->m_picture_05."' , "
		." '".$this->m_picture_06."' , "
		." '".$this->m_picture_07."' , "
		." '".$this->m_picture_08."' , "
		." '".$this->m_picture_09."' , "
		." '".$this->m_picture_10."' , "
		." '".addslashes($this->m_picture_title_01)."' , "
		." '".addslashes($this->m_picture_title_02)."' , "
		." '".addslashes($this->m_picture_title_03)."' , "
		." '".addslashes($this->m_picture_title_04)."' , "
		." '".addslashes($this->m_picture_title_05)."' , "
		." '".addslashes($this->m_picture_title_06)."' , "
		." '".addslashes($this->m_picture_title_07)."' , "
		." '".addslashes($this->m_picture_title_08)."' , "
		." '".addslashes($this->m_picture_title_09)."' , "
		." '".addslashes($this->m_picture_title_10)."' , "
		." '".$this->m_picture_resize_01."' , "
		." '".$this->m_picture_resize_02."' , "
		." '".$this->m_picture_resize_03."' , "
		." '".$this->m_picture_resize_04."' , "
		." '".$this->m_picture_resize_05."' , "
		." '".$this->m_picture_resize_06."' , "
		." '".$this->m_picture_resize_07."' , "
		." '".$this->m_picture_resize_08."' , "
		." '".$this->m_picture_resize_09."' , "
		." '".$this->m_picture_resize_10."' ,  "
		." '".addslashes($this->m_title_11)."' , "
		." '".addslashes($this->m_title_12)."' , "
		." '".addslashes($this->m_title_13)."' , "
		." '".addslashes($this->m_title_14)."' , "
		." '".addslashes($this->m_title_15)."' , "
		." '".addslashes($this->m_title_16)."' , "
		." '".addslashes($this->m_title_17)."' , "
		." '".addslashes($this->m_title_18)."' , "
		." '".addslashes($this->m_title_19)."' , "
		." '".addslashes($this->m_title_20)."' , "
		." '".addslashes($this->m_content_11)."' , "
		." '".addslashes($this->m_content_12)."' , "
		." '".addslashes($this->m_content_13)."' , "
		." '".addslashes($this->m_content_14)."' , "
		." '".addslashes($this->m_content_15)."' , "
		." '".addslashes($this->m_content_16)."' , "
		." '".addslashes($this->m_content_17)."' , "
		." '".addslashes($this->m_content_18)."' , "
		." '".addslashes($this->m_content_19)."' , "
		." '".addslashes($this->m_content_20)."' , "
		." '".$this->m_picture_11."' , "
		." '".$this->m_picture_12."' , "
		." '".$this->m_picture_13."' , "
		." '".$this->m_picture_14."' , "
		." '".$this->m_picture_15."' , "
		." '".$this->m_picture_16."' , "
		." '".$this->m_picture_17."' , "
		." '".$this->m_picture_18."' , "
		." '".$this->m_picture_19."' , "
		." '".$this->m_picture_20."' , "
		." '".addslashes($this->m_picture_title_11)."' , "
		." '".addslashes($this->m_picture_title_12)."' , "
		." '".addslashes($this->m_picture_title_13)."' , "
		." '".addslashes($this->m_picture_title_14)."' , "
		." '".addslashes($this->m_picture_title_15)."' , "
		." '".addslashes($this->m_picture_title_16)."' , "
		." '".addslashes($this->m_picture_title_17)."' , "
		." '".addslashes($this->m_picture_title_18)."' , "
		." '".addslashes($this->m_picture_title_19)."' , "
		." '".addslashes($this->m_picture_title_20)."' , "
		." '".$this->m_picture_resize_11."' , "
		." '".$this->m_picture_resize_12."' , "
		." '".$this->m_picture_resize_13."' , "
		." '".$this->m_picture_resize_14."' , "
		." '".$this->m_picture_resize_15."' , "
		." '".$this->m_picture_resize_16."' , "
		." '".$this->m_picture_resize_17."' , "
		." '".$this->m_picture_resize_18."' , "
		." '".$this->m_picture_resize_19."' , "
		." '".$this->m_picture_resize_20."' ,  "
		." '".$this->m_content_type."'  "
		." ) "; 

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->m_content_id = mysql_insert_id();
			$this->unsetConnection();
            return $this->m_content_id;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  headline_id = '".$this->m_headline_id."' "
		." , rank = '".$this->m_rank."' "
		." , other_type = '".$this->m_other_type."' "
		." , title_01 = '".addslashes($this->m_title_01)."' "
		." , title_02 = '".addslashes($this->m_title_02)."' "
		." , title_03 = '".addslashes($this->m_title_03)."' "
		." , title_04 = '".addslashes($this->m_title_04)."' "
		." , title_05 = '".addslashes($this->m_title_05)."' "
		." , title_06 = '".addslashes($this->m_title_06)."' "
		." , title_07 = '".addslashes($this->m_title_07)."' "
		." , title_08 = '".addslashes($this->m_title_08)."' "
		." , title_09 = '".addslashes($this->m_title_09)."' "
		." , title_10 = '".addslashes($this->m_title_10)."' "
		." , content_01 = '".addslashes($this->m_content_01)."' "
		." , content_02 = '".addslashes($this->m_content_02)."' "
		." , content_03 = '".addslashes($this->m_content_03)."' "
		." , content_04 = '".addslashes($this->m_content_04)."' "
		." , content_05 = '".addslashes($this->m_content_05)."' "
		." , content_06 = '".addslashes($this->m_content_06)."' "
		." , content_07 = '".addslashes($this->m_content_07)."' "
		." , content_08 = '".addslashes($this->m_content_08)."' "
		." , content_09 = '".addslashes($this->m_content_09)."' "
		." , content_10 = '".addslashes($this->m_content_10)."' "
		." , picture_01 = '".$this->m_picture_01."' "
		." , picture_02 = '".$this->m_picture_02."' "
		." , picture_03 = '".$this->m_picture_03."' "
		." , picture_04 = '".$this->m_picture_04."' "
		." , picture_05 = '".$this->m_picture_05."' "
		." , picture_06 = '".$this->m_picture_06."' "
		." , picture_07 = '".$this->m_picture_07."' "
		." , picture_08 = '".$this->m_picture_08."' "
		." , picture_09 = '".$this->m_picture_09."' "
		." , picture_10 = '".$this->m_picture_10."' "
		." , picture_title_01 = '".addslashes($this->m_picture_title_01)."' "
		." , picture_title_02 = '".addslashes($this->m_picture_title_02)."' "
		." , picture_title_03 = '".addslashes($this->m_picture_title_03)."' "
		." , picture_title_04 = '".addslashes($this->m_picture_title_04)."' "
		." , picture_title_05 = '".addslashes($this->m_picture_title_05)."' "
		." , picture_title_06 = '".addslashes($this->m_picture_title_06)."' "
		." , picture_title_07 = '".addslashes($this->m_picture_title_07)."' "
		." , picture_title_08 = '".addslashes($this->m_picture_title_08)."' "
		." , picture_title_09 = '".addslashes($this->m_picture_title_09)."' "
		." , picture_title_10 = '".addslashes($this->m_picture_title_10)."' "
		." , picture_resize_01 = '".$this->m_picture_resize_01."' "
		." , picture_resize_02 = '".$this->m_picture_resize_02."' "
		." , picture_resize_03 = '".$this->m_picture_resize_03."' "
		." , picture_resize_04 = '".$this->m_picture_resize_04."' "
		." , picture_resize_05 = '".$this->m_picture_resize_05."' "
		." , picture_resize_06 = '".$this->m_picture_resize_06."' "
		." , picture_resize_07 = '".$this->m_picture_resize_07."' "
		." , picture_resize_08 = '".$this->m_picture_resize_08."' "
		." , picture_resize_09 = '".$this->m_picture_resize_09."' "
		." , picture_resize_10 = '".$this->m_picture_resize_10."' "
		
		." , title_11 = '".addslashes($this->m_title_11)."' "
		." , title_12 = '".addslashes($this->m_title_12)."' "
		." , title_13 = '".addslashes($this->m_title_13)."' "
		." , title_14 = '".addslashes($this->m_title_14)."' "
		." , title_15 = '".addslashes($this->m_title_15)."' "
		." , title_16 = '".addslashes($this->m_title_16)."' "
		." , title_17 = '".addslashes($this->m_title_17)."' "
		." , title_18 = '".addslashes($this->m_title_18)."' "
		." , title_19 = '".addslashes($this->m_title_19)."' "
		." , title_20 = '".addslashes($this->m_title_20)."' "
		." , content_11 = '".addslashes($this->m_content_11)."' "
		." , content_12 = '".addslashes($this->m_content_12)."' "
		." , content_13 = '".addslashes($this->m_content_13)."' "
		." , content_14 = '".addslashes($this->m_content_14)."' "
		." , content_15 = '".addslashes($this->m_content_15)."' "
		." , content_16 = '".addslashes($this->m_content_16)."' "
		." , content_17 = '".addslashes($this->m_content_17)."' "
		." , content_18 = '".addslashes($this->m_content_18)."' "
		." , content_19 = '".addslashes($this->m_content_19)."' "
		." , content_20 = '".addslashes($this->m_content_20)."' "
		." , picture_11 = '".$this->m_picture_11."' "
		." , picture_12 = '".$this->m_picture_12."' "
		." , picture_13 = '".$this->m_picture_13."' "
		." , picture_14 = '".$this->m_picture_14."' "
		." , picture_15 = '".$this->m_picture_15."' "
		." , picture_16 = '".$this->m_picture_16."' "
		." , picture_17 = '".$this->m_picture_17."' "
		." , picture_18 = '".$this->m_picture_18."' "
		." , picture_19 = '".$this->m_picture_19."' "
		." , picture_20 = '".$this->m_picture_20."' "
		." , picture_title_11 = '".addslashes($this->m_picture_title_11)."' "
		." , picture_title_12 = '".addslashes($this->m_picture_title_12)."' "
		." , picture_title_13 = '".addslashes($this->m_picture_title_13)."' "
		." , picture_title_14 = '".addslashes($this->m_picture_title_14)."' "
		." , picture_title_15 = '".addslashes($this->m_picture_title_15)."' "
		." , picture_title_16 = '".addslashes($this->m_picture_title_16)."' "
		." , picture_title_17 = '".addslashes($this->m_picture_title_17)."' "
		." , picture_title_18 = '".addslashes($this->m_picture_title_18)."' "
		." , picture_title_19 = '".addslashes($this->m_picture_title_19)."' "
		." , picture_title_20 = '".addslashes($this->m_picture_title_20)."' "
		." , picture_resize_11 = '".$this->m_picture_resize_11."' "
		." , picture_resize_12 = '".$this->m_picture_resize_12."' "
		." , picture_resize_13 = '".$this->m_picture_resize_13."' "
		." , picture_resize_14 = '".$this->m_picture_resize_14."' "
		." , picture_resize_15 = '".$this->m_picture_resize_15."' "
		." , picture_resize_16 = '".$this->m_picture_resize_16."' "
		." , picture_resize_17 = '".$this->m_picture_resize_17."' "
		." , picture_resize_18 = '".$this->m_picture_resize_18."' "
		." , picture_resize_19 = '".$this->m_picture_resize_19."' "
		." , picture_resize_20 = '".$this->m_picture_resize_20."' "
		
		." , content_type = '".$this->m_content_type."' "
		." WHERE content_id = ".$this->m_content_id." "; 
        $this->getConnection();
		//echo $strSql;
        $this->query($strSql);
		$this->unsetConnection();

	}

	function updateRank(){

		$strSql = "UPDATE ".$this->TABLE 
		." SET " 
		."  rank = '".$this->m_rank."' "
		." WHERE content_id = ".$this->m_content_id." "; 
        $this->getConnection();

		//echo $strSql;
        $this->query($strSql);
		$this->unsetConnection();
	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE content_id=".$this->m_content_id." ";
        $this->getConnection();
		$this->deleteFile($this->m_picture_01);
		$this->deleteFile($this->m_picture_02);
		$this->deleteFile($this->m_picture_03);
		$this->deleteFile($this->m_picture_04);
		$this->deleteFile($this->m_picture_05);
		$this->deleteFile($this->m_picture_06);
		$this->deleteFile($this->m_picture_07);
		$this->deleteFile($this->m_picture_08);
		$this->deleteFile($this->m_picture_09);
		$this->deleteFile($this->m_picture_10);	
		$this->deleteFile($this->m_picture_11);
		$this->deleteFile($this->m_picture_12);
		$this->deleteFile($this->m_picture_13);
		$this->deleteFile($this->m_picture_14);
		$this->deleteFile($this->m_picture_15);
		$this->deleteFile($this->m_picture_16);
		$this->deleteFile($this->m_picture_17);
		$this->deleteFile($this->m_picture_18);
		$this->deleteFile($this->m_picture_19);
		$this->deleteFile($this->m_picture_20);
        $this->query($strSql);
		$this->unsetConnection();
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				ContentList

		Last update :		20 Nov 02

		Description:		Contentuser list

*********************************************************/


class ContentList extends DataList {
	var $TABLE = "t_content";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT content_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				$this->unsetConnection();
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
	
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Content($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
		
}

}
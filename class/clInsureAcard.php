<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_color table

*********************************************************/
 
class InsureAcard extends DB{

	var $TABLE="t_insure_acard";

		var $m_insure_acard_id;
		function get_insure_acard_id() { return $this->m_insure_acard_id; }
		function set_insure_acard_id($data) { $this->m_insure_acard_id = $data; }
		
		var $m_t1;
		function get_t1() { return $this->m_t1; }
		function set_t1($data) { $this->m_t1 = $data; }
		
		var $m_t2;
		function get_t2() { return $this->m_t2; }
		function set_t2($data) { $this->m_t2 = $data; }
		
		var $m_t3;
		function get_t3() { return $this->m_t3; }
		function set_t3($data) { $this->m_t3 = $data; }
		
		var $m_t4;
		function get_t4() { return $this->m_t4; }
		function set_t4($data) { $this->m_t4 = $data; }
		
		var $m_t5;
		function get_t5() { return $this->m_t5; }
		function set_t5($data) { $this->m_t5 = $data; }
		
		var $m_t6;
		function get_t6() { return $this->m_t6; }
		function set_t6($data) { $this->m_t6 = $data; }
		
		var $m_t7;
		function get_t7() { return $this->m_t7; }
		function set_t7($data) { $this->m_t7 = $data; }
		
		var $m_t8;
		function get_t8() { return $this->m_t8; }
		function set_t8($data) { $this->m_t8 = $data; }
		
		var $m_t9;
		function get_t9() { return $this->m_t9; }
		function set_t9($data) { $this->m_t9 = $data; }
		
		var $m_t10;
		function get_t10() { return $this->m_t10; }
		function set_t10($data) { $this->m_t10 = $data; }
		
		var $m_t11;
		function get_t11() { return $this->m_t11; }
		function set_t11($data) { $this->m_t11 = $data; }
		
		var $m_t12;
		function get_t12() { return $this->m_t12; }
		function set_t12($data) { $this->m_t12 = $data; }
		
		var $m_t13;
		function get_t13() { return $this->m_t13; }
		function set_t13($data) { $this->m_t13 = $data; }
		
		var $m_t14;
		function get_t14() { return $this->m_t14; }
		function set_t14($data) { $this->m_t14 = $data; }
		
		var $m_t15;
		function get_t15() { return $this->m_t15; }
		function set_t15($data) { $this->m_t15 = $data; }
		
		var $m_t16;
		function get_t16() { return $this->m_t16; }
		function set_t16($data) { $this->m_t16 = $data; }
		
		var $m_t17;
		function get_t17() { return $this->m_t17; }
		function set_t17($data) { $this->m_t17 = $data; }
		
		var $m_t18;
		function get_t18() { return $this->m_t18; }
		function set_t18($data) { $this->m_t18 = $data; }
		
		var $m_car_id;
		function get_car_id() { return $this->m_car_id; }
		function set_car_id($data) { $this->m_car_id = $data; }
		
		var $m_customer_id;
		function get_customer_id() { return $this->m_customer_id; }
		function set_customer_id($data) { $this->m_customer_id = $data; }
		
		var $m_test;
		function get_test() { return $this->m_test; }
		function set_test($data) { $this->m_test = $data; }		
		
		var $m_date_add;
		function get_date_add() { return $this->m_date_add; }
		function set_date_add($data) { $this->m_date_add = $data; }		
		
		var $m_duplicated;
		function get_duplicated() { return $this->m_duplicated; }
		function set_duplicated($data) { $this->m_duplicated = $data; }		

	function InsureAcard($objData=NULL) {
        If ($objData->insure_acard_id !="") {
			$this->set_insure_acard_id($objData->insure_acard_id);
			$this->set_t1($objData->t1);
			$this->set_t2($objData->t2);
			$this->set_t3($objData->t3);
			$this->set_t4($objData->t4);
			$this->set_t5($objData->t5);
			$this->set_t6($objData->t6);
			$this->set_t7($objData->t7);
			$this->set_t8($objData->t8);
			$this->set_t9($objData->t9);
			$this->set_t10($objData->t10);
			$this->set_t11($objData->t11);
			$this->set_t12($objData->t12);
			$this->set_t13($objData->t13);
			$this->set_t14($objData->t14);
			$this->set_t15($objData->t15);
			$this->set_t16($objData->t16);
			$this->set_t17($objData->t17);
			$this->set_t18($objData->t18);
			$this->set_car_id($objData->car_id);
			$this->set_customer_id($objData->customer_id);
			$this->set_test($objData->test);
			$this->set_date_add($objData->date_add);
			$this->set_duplicated($objData->duplicated);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_insure_acard_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_acard_id =".$this->m_insure_acard_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAcard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->m_insure_acard_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_acard_id =".$this->m_insure_acard_id;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAcard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureAcard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( t1 , t2 , t3 , t4 , t5 , t6 , t7 , t8 , t9 , t10 , t11 , t12 , t13 , t14 , t15 , t16 , t17 , t18 , car_id , date_add, customer_id ) " ." VALUES ( "
		." '".$this->m_t1."' , "
		." '".$this->m_t2."' , "
		." '".$this->m_t3."' , "
		." '".$this->m_t4."' , "
		." '".$this->m_t5."' , "
		." '".$this->m_t6."' , "
		." '".$this->m_t7."' , "
		." '".$this->m_t8."' , "
		." '".$this->m_t9."' , "
		." '".$this->m_t10."' , "
		." '".$this->m_t11."' , "
		." '".$this->m_t12."' , "
		." '".$this->m_t13."' , "
		." '".$this->m_t14."' , "
		." '".$this->m_t15."' , "
		." '".$this->m_t16."' , "
		." '".$this->m_t17."' , "
		." '".$this->m_t18."' , "
		." '".$this->m_car_id."' , "
		." '".date("Y-m-d")."' , "
		." '".$this->m_customer_id."' "
		." ) ";
		//echo $strSql."<br>";
		
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_acard_id = mysql_insert_id();
            return $this->m_insure_acard_id;
        } else {
			return false;
	    }

	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." t1 = '".$this->m_t1."' "
		." , t2 = '".$this->m_t2."' "
		." , t3 = '".$this->m_t3."' "
		." , t4 = '".$this->m_t4."' "
		." , t5 = '".$this->m_t5."' "
		." , t6 = '".$this->m_t6."' "
		." , t7 = '".$this->m_t7."' "
		." , t8 = '".$this->m_t8."' "
		." , t9 = '".$this->m_t9."' "
		." , t10 = '".$this->m_t10."' "
		." , t11 = '".$this->m_t11."' "
		." , t12 = '".$this->m_t12."' "
		." , t13 = '".$this->m_t13."' "
		." , t14 = '".$this->m_t14."' "
		." , t15 = '".$this->m_t15."' "
		." , t16 = '".$this->m_t16."' "
		." , t17 = '".$this->m_t17."' "
		." , t18 = '".$this->m_t18."' "
		." , car_id = '".$this->m_car_id."' "
		." , customer_id = '".$this->m_customer_id."' "
		." WHERE insure_acard_id = ".$this->m_insure_acard_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function update_status(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  car_id = '".$this->m_car_id."' "
		." , customer_id = '".$this->m_customer_id."' "
		." , duplicated = '".$this->m_duplicated."' "
		." WHERE insure_acard_id = ".$this->m_insure_acard_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function update_test(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  test= '".$this->m_test."' "
		." WHERE insure_acard_id = ".$this->m_insure_acard_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
		
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_acard_id=".$this->m_insure_acard_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteAll() {
       $strSql = " DELETE FROM ".$this->TABLE;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class InsureAcardList extends DataList {
	var $TABLE = "t_insure_acard";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_acard_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureAcard($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getInsureAcardId()."\"");
			if (($defaultId != null) && ($objItem->getInsureAcardId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
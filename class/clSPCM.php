<?
/*********************************************************
		Class :					SPCM

		Last update :	  10 Jan 02

		Description:	  Class manage t_stock_product_car_model  table

*********************************************************/
 
class SPCM extends DB{

	var $TABLE="t_stock_product_car_model";

	var $mSPCMId;
	function getSPCMId() { return $this->mSPCMId; }
	function setSPCMId($data) { $this->mSPCMId = $data; }
	
	var $mStockProductId;
	function getStockProductId() { return htmlspecialchars($this->mStockProductId); }
	function setStockProductId($data) { $this->mStockProductId = $data; }
	
	var $mCarModelId;
	function getCarModelId() { return htmlspecialchars($this->mCarModelId); }
	function setCarModelId($data) { $this->mCarModelId = $data; }
	
	function SPCM($objData=NULL) {
        If ($objData->spcm_id !="") {
            $this->setSPCMId($objData->spcm_id);
			$this->setStockProductId($objData->stock_product_id);
			$this->setCarModelId($objData->car_model_id);
        }
    }

	function init(){
		
	}
	
	function load() {

		if ($this->mSPCMId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE spcm_id =".$this->mSPCMId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->SPCM($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {
		
		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->SPCM($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( stock_product_id, car_model_id ) "
						." VALUES ( '".$this->mStockProductId."',  '".$this->mCarModelId."' ) ";
        $this->getConnection();	
        If ($Result = $this->query($strSql)) { 
            $this->mSPCMId = mysql_insert_id();
            return $this->mSPCMId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_product_id = '".$this->mStockProductId."'  "
						." , car_model_id = '".$this->mCarModelId."'  "
						." WHERE  spcm_id = ".$this->mSPCMId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE spcm_id=".$this->mSPCMId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strSql) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strSql;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mStockProductId == "") $asrErrReturn["stock_product_id"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class SPCMList extends DataList {
	var $TABLE = "t_stock_product_car_model";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT spcm_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new SPCM($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getSPCMId()."\"");
			if (($defaultId != null) && ($objItem->getSPCMId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getStockProductId()."</option>");
		}
		echo("</select>");
	}			
	
}
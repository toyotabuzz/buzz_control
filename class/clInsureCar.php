<?
/*********************************************************
		Class :					InsureCar

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureCar extends DB{

	var $TABLE="t_insure_car";

	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_sale_id;
	function get_sale_id() { return $this->m_sale_id; }
	function set_sale_id($data) { $this->m_sale_id = $data; }
	
	var $m_team;
	function get_team() { return $this->m_team; }
	function set_team($data) { $this->m_team = $data; }	
	
	var $m_team_id;
	function get_team_id() { return $this->m_team_id; }
	function set_team_id($data) { $this->m_team_id = $data; }	
	
	//acard, booking, ccard
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }

	var $m_source;
	function get_source() { return $this->m_source; }
	function set_source($data) { $this->m_source = $data; }
	
	var $m_insure_customer_id;
	function get_insure_customer_id() { return $this->m_insure_customer_id; }
	function set_insure_customer_id($data) { $this->m_insure_customer_id = $data; }	
	
	var $m_customer_id;
	function get_customer_id() { return $this->m_customer_id; }
	function set_customer_id($data) { $this->m_customer_id = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }

	var $m_car_type;
	function get_car_type() { return $this->m_car_type; }
	function set_car_type($data) { $this->m_car_type = $data; }
	
	var $m_car_model_id;
	function get_car_model_id() { return $this->m_car_model_id; }
	function set_car_model_id($data) { $this->m_car_model_id = $data; }
	
	var $m_car_series_id;
	function get_car_series_id() { return $this->m_car_series_id; }
	function set_car_series_id($data) { $this->m_car_series_id = $data; }
	
	var $m_color;
	function get_color() { return $this->m_color; }
	function set_color($data) { $this->m_color = $data; }
	
	var $m_car_number;
	function get_car_number() { return $this->m_car_number; }
	function set_car_number($data) { $this->m_car_number = $data; }
	
	var $m_engine_number;
	function get_engine_number() { return $this->m_engine_number; }
	function set_engine_number($data) { $this->m_engine_number = $data; }
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }	
	
	var $m_register_year;
	function get_register_year() { return $this->m_register_year; }
	function set_register_year($data) { $this->m_register_year = $data; }
	
	var $m_date_verify;
	function get_date_verify() { return $this->m_date_verify; }
	function set_date_verify($data) { $this->m_date_verify = $data; }	
	
	var $m_suggest_from;
	function get_suggest_from() { return $this->m_suggest_from; }
	function set_suggest_from($data) { $this->m_suggest_from = $data; }
	
	var $m_call_date;
	function get_call_date() { return $this->m_call_date; }
	function set_call_date($data) { $this->m_call_date = $data; }
	
	var $m_call_remark;
	function get_call_remark() { return $this->m_call_remark; }
	function set_call_remark($data) { $this->m_call_remark = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }

	var $m_order_id;
	function get_order_id() { return $this->m_order_id; }
	function set_order_id($data) { $this->m_order_id = $data; }
	
	//�����ŷ�����١��
	var $m_junk;
	function get_junk() { return $this->m_junk; }
	function set_junk($data) { $this->m_junk = $data; }	
	
	var $m_company_id;
	function get_company_id() { return $this->m_company_id; }
	function set_company_id($data) { $this->m_company_id = $data; }	
	
	var $m_mail_date;
	function get_mail_date() { return $this->m_mail_date; }
	function set_mail_date($data) { $this->m_mail_date = $data; }	
	
	//ʶҹС�÷ӧҹ�ͧ acard ��� �մѧ���  1. ready 2. mail  3.call1  4. quotation 5. call2  6. booking 7. ccard  8.reject 
	var $m_operate;
	function get_operate() { return $this->m_operate; }
	function set_operate($data) { $this->m_operate = $data; }		
	
	var $m_operate_prb;
	function get_operate_prb() { return $this->m_operate_prb; }
	function set_operate_prb($data) { $this->m_operate_prb = $data; }		
	
	var $m_buy_type;
	function get_buy_type() { return $this->m_buy_type; }
	function set_buy_type($data) { $this->m_buy_type = $data; }		
	
	var $m_buy_company;
	function get_buy_company() { return $this->m_buy_company; }
	function set_buy_company($data) { $this->m_buy_company = $data; }		

	var $m_registry_date;
	function get_registry_date() { return $this->m_registry_date; }
	function set_registry_date($data) { $this->m_registry_date = $data; }		
	
	var $m_registry_tax_year;
	function get_registry_tax_year() { return $this->m_registry_tax_year; }
	function set_registry_tax_year($data) { $this->m_registry_tax_year = $data; }		
	
	var $m_registry_tax_year_number;
	function get_registry_tax_year_number() { return $this->m_registry_tax_year_number; }
	function set_registry_tax_year_number($data) { $this->m_registry_tax_year_number = $data; }		
	
	var $m_get_car_date;
	function get_get_car_date() { return $this->m_get_car_date; }
	function set_get_car_date($data) { $this->m_get_car_date = $data; }		
	
	var $m_insure_company_id;
	function get_insure_company_id() { return $this->m_insure_company_id; }
	function set_insure_company_id($data) { $this->m_insure_company_id = $data; }			
	
	//�ԵԺؤ��= 1, �ؤ�Ÿ����� 0, �ԵԺؤ�� ����ѡ���� � ������ = 3
	var $m_niti;
	function get_niti() { return $this->m_niti; }
	function set_niti($data) { $this->m_niti = $data; }			

	//����������ó� 0,    �������������ó� 1
	var $m_info_type;
	function get_info_type() { return $this->m_info_type; }
	function set_info_type($data) { $this->m_info_type = $data; }			

	var $m_checkc;
	function get_checkc() { return $this->m_checkc; }
	function set_checkc($data) { $this->m_checkc = $data; }			
	
	var $m_provinde_code;
	function get_province_code() { return $this->m_province_code; }
	function set_province_code($data) { $this->m_province_code = $data; }		

	var $m_insure_acard_id;
	function get_insure_acard_id() { return $this->m_insure_acard_id; }
	function set_insure_acard_id($data) { $this->m_insure_acard_id = $data; }			

	var $m_recent_insure_id;
	function get_recent_insure_id() { return $this->m_recent_insure_id; }
	function set_recent_insure_id($data) { $this->m_recent_insure_id = $data; }				
	
	var $m_import_date;
	function get_import_date() { return $this->m_import_date; }
	function set_import_date($data) { $this->m_import_date = $data; }				
	
	var $m_lock_sale;
	function get_lock_sale() { return $this->m_lock_sale; }
	function set_lock_sale($data) { $this->m_lock_sale = $data; }		
	
	var $m_admin_share;
	function get_admin_share() { return $this->m_admin_share; }
	function set_admin_share($data) { $this->m_admin_share = $data; }			
	
	var $m_sup_share;
	function get_sup_share() { return $this->m_sup_share; }
	function set_sup_share($data) { $this->m_sup_share = $data; }			
	
	var $m_fix_sale;
	function get_fix_sale() { return $this->m_fix_sale; }
	function set_fix_sale($data) { $this->m_fix_sale = $data; }			
	
	var $m_date_verify_prb;
	function get_date_verify_prb() { return $this->m_date_verify_prb; }
	function set_date_verify_prb($data) { $this->m_date_verify_prb = $data; }			
	
	var $m_emotion;
	function get_emotion() { return $this->m_emotion; }
	function set_emotion($data) { $this->m_emotion = $data; }			
	
	var $m_duplicated;
	function get_duplicated() { return $this->m_duplicated; }
	function set_duplicated($data) { $this->m_duplicated = $data; }			
	
	var $m_first_company_id;
	function get_first_company_id() { return $this->m_first_company_id; }
	function set_first_company_id($data) { $this->m_first_company_id = $data; }			
	
	var $m_first_team_id;
	function get_first_team_id() { return $this->m_first_team_id; }
	function set_first_team_id($data) { $this->m_first_team_id = $data; }			
	
	var $m_first_sale_id;
	function get_first_sale_id() { return $this->m_first_sale_id; }
	function set_first_sale_id($data) { $this->m_first_sale_id = $data; }
	
	var $m_temp_sale_id;
	function get_temp_sale_id() { return $this->m_temp_sale_id; }
	function set_temp_sale_id($data) { $this->m_temp_sale_id = $data; }	
	
	var $m_temp_remark;
	function get_temp_remark() { return $this->m_temp_remark; }
	function set_temp_remark($data) { $this->m_temp_remark = $data; }
	
	var $m_approve;
	function get_approve() { return $this->m_approve; }
	function set_approve($data) { $this->m_approve = $data; }
	
	var $m_approve_date;
	function get_approve_date() { return $this->m_approve_date; }
	function set_approve_date($data) { $this->m_approve_date = $data; }
	
	var $m_approve_by;
	function get_approve_by() { return $this->m_approve_by; }
	function set_approve_by($data) { $this->m_approve_by = $data; }
	
	var $m_edits_date;
	function get_edits_date() { return $this->m_edits_date; }
	function set_edits_date($data) { $this->m_edits_date = $data; }
	
	var $m_edits_by;
	function get_edits_by() { return $this->m_edits_by; }
	function set_edits_by($data) { $this->m_edits_by = $data; }	
	
	var $m_cus_code;
	function get_cus_code() { return $this->m_cus_code; }
	function set_cus_code($data) { $this->m_cus_code = $data; }	
	
	var $m_ins_cat;
	function get_ins_cat() { return $this->m_ins_cat; }
	function set_ins_cat($data) { $this->m_ins_cat = $data; }	
	
	var $lead_special;
	function get_lead_special() { return $this->lead_special; }
	function set_lead_special($data) { $this->lead_special = $data; }	

	function InsureCar($objData=NULL) {
        If ($objData->car_id !="") {
			$this->set_car_id($objData->car_id);
			$this->set_sale_id($objData->sale_id);
			$this->set_team($objData->team);
			$this->set_team_id($objData->team_id);
			$this->set_source($objData->source);
			$this->set_car_type($objData->car_type);
			$this->set_insure_customer_id($objData->insure_customer_id);
			$this->set_customer_id($objData->customer_id);
			$this->set_code($objData->code);
			$this->set_car_model_id($objData->car_model_id);
			$this->set_car_series_id($objData->car_series_id);
			$this->set_color($objData->color);
			$this->set_price($objData->price);
			$this->set_car_number($objData->car_number);
			$this->set_engine_number($objData->engine_number);
			$this->set_register_year($objData->register_year);
			$this->set_date_verify($objData->date_verify);
			$this->set_suggest_from($objData->suggest_from);
			$this->set_call_date($objData->call_date);
			$this->set_call_remark($objData->call_remark);
			$this->set_date_add($objData->date_add);
			$this->set_order_id($objData->order_id);
			$this->set_junk($objData->junk);			
			$this->set_company_id($objData->company_id);
			$this->set_mail_date($objData->mail_date);
			$this->set_operate($objData->operate);
			$this->set_buy_type($objData->buy_type);
			$this->set_buy_company($objData->buy_company);
			$this->set_registry_date($objData->registry_date);
			$this->set_registry_tax_year($objData->registry_tax_year);
			$this->set_registry_tax_year_number($objData->registry_tax_year_number);
			$this->set_get_car_date($objData->get_car_date);
			$this->set_insure_company_id($objData->insure_company_id);
			$this->set_niti($objData->niti);
			$this->set_info_type($objData->info_type);
			$this->set_checkc($objData->checkc);
			$this->set_province_code($objData->province_code);
			$this->set_insure_acard_id($objData->insure_acard_id);
			$this->set_recent_insure_id($objData->recent_insure_id);
			$this->set_import_date($objData->import_date);
			$this->set_lock_sale($objData->lock_sale);
			$this->set_admin_share($objData->admin_share);
			$this->set_sup_share($objData->sup_share);
			$this->set_fix_sale($objData->fix_sale);
			$this->set_date_verify_prb($objData->date_verify_prb);
			$this->set_operate_prb($objData->operate_prb);
			$this->set_emotion($objData->emotion);
			$this->set_duplicated($objData->duplicated);
			$this->set_first_company_id($objData->first_company_id);
			$this->set_first_sale_id($objData->first_sale_id);
			$this->set_first_team_id($objData->first_team_id);
			$this->set_temp_sale_id($objData->temp_sale_id);
			$this->set_temp_remark($objData->temp_remark);
			$this->set_approve($objData->approve);
			$this->set_approve_date($objData->approve_date);
			$this->set_approve_by($objData->approve_by);
			$this->set_edits_date($objData->edits_date);
			$this->set_edits_by($objData->edits_by);
			$this->set_cus_code($objData->cus_code);
			$this->set_ins_cat($objData->ins_cat);
			$this->set_lead_special($objData->lead_special);
			
			//echo "dd".$this->get_insure_company_id();
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_car_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_id =".$this->m_car_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCar($row);
                $result->freeResult();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}
	
	function loadUTF8() {

		if ($this->m_car_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_id =".$this->m_car_id;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCar($row);
                $result->freeResult();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureCar($row);
      		    $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		$this->unsetConnection();
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(DISTINCT(car_id)) AS RSUM  FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}	
	
	
	
	function loadSumByConditionImport($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(DISTINCT(car_id)) AS RSUM  FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}	
	
	
	function loadSumByConditionImportNot($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(DISTINCT(car_id)) AS RSUM  FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	
	
	/*
	function loadSumByConditionImportNot($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(DISTINCT(IC.car_id)) AS RSUM  FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		." WHERE ".$strCondition." and IC.car_id NOT IN ( select I.car_id from t_insure  ) ";
		echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}	
	*/
	
	function loadSumImport($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(C.car_id) AS RSUM  FROM ".$this->TABLE." C  "
					." INNER JOIN t_order O ON O.order_id = C.order_id  "
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}		
	
	
	function loadSumByConditionVerifyReject($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(DISTINCT(car_id)) AS RSUM  FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_track C ON IC.car_id = C.car_id and C.tracking_type='call1'  "
		." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}		
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( sale_id, team, team_id, company_id, source, car_type, insure_customer_id, customer_id, code, car_model_id, car_series_id, color, price, car_number, engine_number, register_year,
		 date_verify,  date_verify_prb, suggest_from, call_date, call_remark, date_add , order_id , buy_type, buy_company, registry_date
		 , registry_tax_year, registry_tax_year_number, insure_company_id, get_car_date, niti , checkc, info_type,province_code, import_date, insure_acard_id, temp_sale_id, temp_remark, edits_by, edits_date, created_at, created_by ) " ." VALUES ( "

		." '".$this->m_sale_id."' , "
		." '".$this->m_team."' , "
		." '".$this->m_team_id."' , "
		." '".$this->m_company_id."' , "
		." '".addslashes($this->m_source)."' , "
		." '".$this->m_car_type."' , "
		." '".$this->m_insure_customer_id."' , "
		." '".$this->m_customer_id."' , "
		." '".addslashes($this->m_code)."' , "
		." '".$this->m_car_model_id."' , "
		." '".$this->m_car_series_id."' , "
		." '".$this->m_color."' , "
		." '".$this->m_price."' , "
		." '".addslashes($this->m_car_number)."' , "
		." '".addslashes($this->m_engine_number)."' , "
		." '".$this->m_register_year."' , "
		." '".$this->m_date_verify."' , "	
		." '".$this->m_date_verify_prb."' , "	
		." '".addslashes($this->m_suggest_from)."' , "
		." '".$this->m_call_date."' , "
		." '".addslashes($this->m_call_remark)."' , "
		." '".$this->m_date_add."' , "
		." '".$this->m_order_id."',  "
		." '".$this->m_buy_type."' , "
		." '".$this->m_buy_company."' ,  "
		." '".$this->m_registry_date."' ,  "
		." '".$this->m_registry_tax_year."' , "
		." '".$this->m_registry_tax_year_number."' , "
		." '".$this->m_insure_company_id."' ,  "
		." '".$this->m_get_car_date."' ,  "
		." '".$this->m_niti."' , "
		." '".$this->m_checkc."' , "
		." '".$this->m_info_type."' , "
		." '".$this->m_province_code."' , "
		." '".$this->m_import_date."' , "
		." '".$this->m_insure_acard_id."' , "
		." '".$this->m_temp_sale_id."' , "
		." '".$this->m_temp_remark."' , "
		." '".$this->m_edits_by."' , "
		." '".$this->m_edits_date."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_car_id = mysql_insert_id();
			$this->unsetConnection();
            return $this->m_car_id;
        } else {
			$this->unsetConnection();
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' "
		." , team = '".$this->m_team."' "
		." , team_id = '".$this->m_team_id."' "
		." , car_type = '".$this->m_car_type."' "
		." , insure_customer_id = '".$this->m_insure_customer_id."' "
		." , customer_id = '".$this->m_customer_id."' "
		." , code = '".$this->m_code."' "
		." , car_model_id = '".$this->m_car_model_id."' "
		." , car_series_id = '".$this->m_car_series_id."' "
		." , color = '".$this->m_color."' "
		." , price = '".$this->m_price."' "
		." , car_number = '".$this->m_car_number."' "
		." , engine_number = '".$this->m_engine_number."' "
		." , register_year = '".$this->m_register_year."' "
		." , date_verify = '".$this->m_date_verify."' "
		." , date_verify_prb = '".$this->m_date_verify_prb."' "
		." , suggest_from = '".$this->m_suggest_from."' "
		." , call_date = '".$this->m_call_date."' "
		." , call_remark = '".$this->m_call_remark."' "
		." , buy_type = '".$this->m_buy_type."' "
		." , buy_company = '".$this->m_buy_company."' "
		." , registry_date = '".$this->m_registry_date."' "
		." , registry_tax_year = '".$this->m_registry_tax_year."' "
		." , registry_tax_year_number = '".$this->m_registry_tax_year_number."' "	
		." , insure_company_id = '".$this->m_insure_company_id."' "	
		." , get_car_date = '".$this->m_get_car_date."' "	
		." , niti = '".$this->m_niti."' "	
		." , info_type = '".$this->m_info_type."' "	
		." , checkc = '".$this->m_checkc."' "	
		." , province_code = '".$this->m_province_code."' "	
		." , junk = '".$this->m_junk."' "
		." , edits_by = '".$this->m_edits_by."' "
		." , edits_date = '".$this->m_edits_date."' "
		
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function update_general(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."   customer_id = '".$this->m_customer_id."' "
		."  , insure_customer_id = '".$this->m_insure_customer_id."' "
		." , code = '".$this->m_code."' "
		." , date_verify = '".$this->m_date_verify."' "
		." , date_verify_prb = '".$this->m_date_verify_prb."' "
		." , suggest_from = '".$this->m_suggest_from."' "
		." , car_model_id = '".$this->m_car_model_id."' "
		." , car_series_id = '".$this->m_car_series_id."' "
		." , color = '".$this->m_color."' "
		." , price = '".$this->m_price."' "
		." , car_number = '".$this->m_car_number."' "
		." , engine_number = '".$this->m_engine_number."' "
		." , call_date = '".$this->m_call_date."' "
		." , call_remark = '".$this->m_call_remark."' "
		." , buy_type = '".$this->m_buy_type."' "
		." , buy_company = '".$this->m_buy_company."' "
		." , registry_date = '".$this->m_registry_date."' "
		." , registry_tax_year = '".$this->m_registry_tax_year."' "
		." , registry_tax_year_number = '".$this->m_registry_tax_year_number."' "	
		." , insure_company_id = '".$this->m_insure_company_id."' "	
		." , get_car_date = '".$this->m_get_car_date."' "	
		." , niti = '".$this->m_niti."' "	
		." , info_type = '".$this->m_info_type."' "	
		." , junk = '".$this->m_junk."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;


	}	
	
	function update_cashier(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "	
		." get_car_date = '".$this->m_get_car_date."' "	
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function update_approve(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "	
		." approve = '".$this->m_approve."' , "	
		." approve_date = '".$this->m_approve_date."' , "
		." approve_by = '".$this->m_approve_by."' "	
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
		
	
	
	function updateRegistry(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET  registry_date = '".$this->m_registry_date."' "
		." , registry_tax_year = '".$this->m_registry_tax_year."' "
		." , registry_tax_year_number = '".$this->m_registry_tax_year_number."' "	
		." , register_year= '".$this->m_register_year."' "	
		." , get_car_date = '".$this->m_get_car_date."' "	
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	
	function updateSale(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' , "
		."  team_id = '".$this->m_team_id."'  "
		
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateFirst(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  first_sale_id = '".$this->m_first_sale_id."' , "
		."  first_team_id = '".$this->m_first_team_id."' ,  "
		."  first_company_id = '".$this->m_first_company_id."'  "

		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	
	
	function updateDateVerify(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  date_verify = '".$this->m_date_verify."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateDateVerifyPrb(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  date_verify_prb = '".$this->m_date_verify_prb."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateShareAdmin(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' , "
		."  team_id = '".$this->m_team_id."' , "
		."  company_id = '".$this->m_company_id."'  "
		
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
		//exit;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateCusCode(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  cus_code = '".$this->m_cus_code."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
		//exit;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	
	function updateFixSale(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' , "
		."  team_id = '".$this->m_team_id."' , "
		."  fix_sale = '".$this->m_fix_sale."' , "
		."  lock_sale = '".$this->m_lock_sale."'  "
		
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateFixSale1(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' , "
		."  team_id = '".$this->m_team_id."' , "
		."  fix_sale = '".$this->m_fix_sale."' , "
		."  lock_sale = '".$this->m_lock_sale."' , "
		."  operate = ''  "
		
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateRecall(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sale_id = '".$this->m_sale_id."' , "
		."  date_verify = '".$this->m_date_verify."' ,  "
		."  operate = '".$this->m_operate."' , "
		."  fix_sale = '".$this->m_fix_sale."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function updateCCard(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "		
		."  date_verify = '".$this->m_date_verify."' , "
		."  operate = '".$this->m_operate."' , "
		."  fix_sale = '".$this->m_fix_sale."' , "
		."  insure_company_id = '".$this->m_insure_company_id."' , "
		."  lock_sale = '".$this->m_lock_sale."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateEmotion(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "		
		."  emotion = '".$this->m_emotion."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	
	function updateTeam(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  team_id = '".$this->m_team_id."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateJunk(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  junk= '".$this->m_junk."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}		
	
	

	
	function updateMailDate(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  mail_date = '".$this->m_mail_date."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}			
	
	function updateOperate(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  operate= '".$this->m_operate."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOperatePrb(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  operate_prb= '".$this->m_operate_prb."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateDuplicated(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  duplicated= '".$this->m_duplicated."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateRecent(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  recent_insure_id= '".$this->m_recent_insure_id."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
		//exit;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateCompany(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  company_id = '".$this->m_company_id."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateLock(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  lock_sale = '".$this->m_lock_sale."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function updateAdminShare(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  admin_share= '".$this->m_admin_share."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateSupShare(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  sup_share = '".$this->m_sup_share."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateFix2012(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		// ."  operate= '".$this->m_operate."' , "
		."  date_verify = '".$this->m_date_verify."'  "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql."<br>";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateSql($strSql){
        $this->getConnection();
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	
	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE. " WHERE car_id=".$this->m_car_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				InsureCar Company List

		Last update :		22 Mar 02

		Description:		InsureCar Company List

*********************************************************/

class InsureCarList extends DataList {
	var $TABLE = "t_insure_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT IC.car_id) as rowCount FROM ".$this->TABLE
			." IC  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
		$strSql = " SELECT * FROM ".$this->TABLE." IC "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCar($row);
			}
			//$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT IC.car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		." LEFT JOIN t_insure I ON I.car_id = IC.car_id  "		
		.$this->getFilterSQL();	// WHERE clause
		//echo $strSql;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCar($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadSearchLabel($hLimit) {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT IC.car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		.$this->getFilterSQL();	// WHERE clause
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$hLimit;
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCar($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	

	function loadImport() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT IC.car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL();	// WHERE clause
		
		//echo $strSql;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "		
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCar($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	/*
	function loadImportNot() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT IC.car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL()." and IC.car_id NOT IN ( select I.car_id from t_insure  ) ";	// WHERE clause
		
		echo $strSql;
		
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		
		$strSql = " SELECT DISTINCT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_insure_customer01 C ON IC.insure_customer_id = C.insure_customer_id  "
		." LEFT JOIN t_order O ON O.order_id = IC.order_id  "
		.$this->getFilterSQL()." and IC.car_id NOT IN ( select I.car_id from t_insure  ) "	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureCar($row);
			}
			
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	*/
	
	
}
<?
/*********************************************************
		Class :					InsureVerify

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureVerify extends DB{

	var $TABLE="t_insure_verify";

	var $m_insure_verify_id;
	function get_insure_verify_id() { return $this->m_insure_verify_id; }
	function set_insure_verify_id($data) { $this->m_insure_verify_id = $data; }
	
	var $m_verify_code;
	function get_verify_code() { return $this->m_verify_code; }
	function set_verify_code($data) { $this->m_verify_code = $data; }
	
	var $m_customer_old;
	function get_customer_old() { return $this->m_customer_old; }
	function set_customer_old($data) { $this->m_customer_old = $data; }
	
	var $m_customer_new;
	function get_customer_new() { return $this->m_customer_new; }
	function set_customer_new($data) { $this->m_customer_new = $data; }	

	function InsureVerify($objData=NULL) {
        If ($objData->insure_verify_id !="") {
			$this->set_insure_verify_id($objData->insure_verify_id);
			$this->set_verify_code($objData->verify_code);
			$this->set_customer_old($objData->customer_old);
			$this->set_customer_new($objData->customer_new);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_verify_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_verify_id =".$this->m_insure_verify_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureVerify($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureVerify($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( customer_new, verify_code, customer_old ) " ." VALUES ( "
		." '".$this->m_customer_new."' , "
		." '".$this->m_verify_code."' , "
		." '".$this->m_customer_old."'  "
		." ) "; 


        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_verify_id = mysql_insert_id();
            return $this->m_insure_verify_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET " 
		."   customer_new = '".$this->m_customer_new."' "
		." , verify_code = '".$this->m_verify_code."' "
		." , customer_old = '".$this->m_customer_old."' "

		." WHERE insure_verify_id = ".$this->m_insure_verify_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_verify_id=".$this->m_insure_verify_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				InsureVerify Company List

		Last update :		22 Mar 02

		Description:		InsureVerify Company List

*********************************************************/

class InsureVerifyList extends DataList {
	var $TABLE = "t_insure_verify";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_verify_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureVerify($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadGroup() {
		// also gets latest delivery date
        //Get Number of Users list
		$strSql = " SELECT DISTINCT(verify_code),  FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureVerify($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	
}
<?
/*********************************************************
		Class :				Order
		Last update :		9 Jan 07
		Description:		Class manage t_order  table
*********************************************************/

class Order extends DB{

	var $TABLE="t_order";
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }

	var $mOrderDate;
	function getOrderDate() { return htmlspecialchars($this->mOrderDate); }
	function setOrderDate($data) { $this->mOrderDate = $data; }
	
	var $mOrderStatus;
	function getOrderStatus() { return htmlspecialchars($this->mOrderStatus); }
	function setOrderStatus($data) { $this->mOrderStatus = $data; }	

	function getOrderStatusDetail(){
		if($this->mOrderStatus == 0) return "�ͧö";
		if($this->mOrderStatus == 1) return "�ѹ�֡�ͧ CRL";
		if($this->mOrderStatus == 2) return "��Ǩ�ͧ�ѭ��";
		if($this->mOrderStatus == 10) return "TBR";
		if($this->mOrderStatus == 3) return "�ѹ�֡���ͺ CRL";
		if($this->mOrderStatus == 4) return "��Ǩ���ͺ�Ѵ����";
		if($this->mOrderStatus == 5) return "��Ǩ���ͺ�ѭ��";
		if($this->mOrderStatus == 6) return "���ͺ";
		if($this->mOrderStatus == 7) return "������¹";
		if($this->mOrderStatus == 8) return "�׹�ͧ";
		if($this->mOrderStatus == 9) return "�׹ö";
	}

	var $mOrderNumber;
	function getOrderNumber() { return htmlspecialchars($this->mOrderNumber); }
	function setOrderNumber($data) { $this->mOrderNumber = $data; }
	
	var $mRecieveNumber;
	function getRecieveNumber() { return htmlspecialchars($this->mRecieveNumber); }
	function setRecieveNumber($data) { $this->mRecieveNumber = $data; }
	
	var $mDealer;
	function getDealer() { return htmlspecialchars($this->mDealer); }
	function setDealer($data) { $this->mDealer = $data; }
	
	var $mOrderPrice;
	function getOrderPrice() { return htmlspecialchars($this->mOrderPrice); }
	function setOrderPrice($data) { $this->mOrderPrice = $data; }

	var $mOrderReservePrice;
	function getOrderReservePrice() { return htmlspecialchars($this->mOrderReservePrice); }
	function setOrderReservePrice($data) { $this->mOrderReservePrice = $data; }
	
	var $mOrderReservePrice01;
	function getOrderReservePrice01() { return htmlspecialchars($this->mOrderReservePrice01); }
	function setOrderReservePrice01($data) { $this->mOrderReservePrice01 = $data; }
	
	var $mOrderReservePrice02;
	function getOrderReservePrice02() { return htmlspecialchars($this->mOrderReservePrice02); }
	function setOrderReservePrice02($data) { $this->mOrderReservePrice02 = $data; }		

	var $mOrderOldCar;
	function getOrderOldCar() { return htmlspecialchars($this->mOrderOldCar); }
	function setOrderOldCar($data) { $this->mOrderOldCar = $data; }		
	
	var $mOrderOldCarNumber;
	function getOrderOldCarNumber() { return htmlspecialchars($this->mOrderOldCarNumber); }
	function setOrderOldCarNumber($data) { $this->mOrderOldCarNumber = $data; }		
	
	var $mOrderLocation;
	function getOrderLocation() { return htmlspecialchars($this->mOrderLocation); }
	function setOrderLocation($data) { $this->mOrderLocation = $data; }
	
	var $mBookingNumber;
	function getBookingNumber() { return htmlspecialchars($this->mBookingNumber); }
	function setBookingNumber($data) { $this->mBookingNumber = $data; }

	var $mBookingCustomerId;
	function getBookingCustomerId() { return htmlspecialchars($this->mBookingCustomerId); }
	function setBookingCustomerId($data) { $this->mBookingCustomerId = $data; }
	
	var $mBookingNumberNohead;
	function getBookingNumberNohead() { return htmlspecialchars($this->mBookingNumberNohead); }
	function setBookingNumberNohead($data) { $this->mBookingNumberNohead = $data; }
	
	var $mBookingDate;
	function getBookingDate() { return htmlspecialchars($this->mBookingDate); }
	function setBookingDate($data) { $this->mBookingDate = $data; }

	var $mBookingCashierDate;
	function getBookingCashierDate() { return htmlspecialchars($this->mBookingCashierDate); }
	function setBookingCashierDate($data) { $this->mBookingCashierDate = $data; }
	
	var $mBookingCashierTime;
	function getBookingCashierTime() { return htmlspecialchars($this->mBookingCashierTime); }
	function setBookingCashierTime($data) { $this->mBookingCashierTime = $data; }	

	var $mBookingCashierId;
	function getBookingCashierId() { return htmlspecialchars($this->mBookingCashierId); }
	function setBookingCashierId($data) { $this->mBookingCashierId = $data; }	
	
	var $mBookingCarColor;
	function getBookingCarColor() { return htmlspecialchars($this->mBookingCarColor); }
	function setBookingCarColor($data) { $this->mBookingCarColor = $data; }

	var $mBookingCarSeries;
	function getBookingCarSeries() { return htmlspecialchars($this->mBookingCarSeries); }
	function setBookingCarSeries($data) { $this->mBookingCarSeries = $data; }

	var $mBookingCarType;
	function getBookingCarType() { return htmlspecialchars($this->mBookingCarType); }
	function setBookingCarType($data) { $this->mBookingCarType = $data; }

	var $mBookingCarGear;
	function getBookingCarGear() { return htmlspecialchars($this->mBookingCarGear); }
	function setBookingCarGear($data) { $this->mBookingCarGear = $data; }	
	
	var $mBookingCarPrice;
	function getBookingCarPrice() { return htmlspecialchars($this->mBookingCarPrice); }
	function setBookingCarPrice($data) { $this->mBookingCarPrice = $data; }
	
	var $mBookingCarRemark;
	function getBookingCarRemark() { return htmlspecialchars($this->mBookingCarRemark); }
	function setBookingCarRemark($data) { $this->mBookingCarRemark = $data; }	

	var $mBookingCompanyName;
	function getBookingCompanyName() { return htmlspecialchars($this->mBookingCompanyName); }
	function setBookingCompanyName($data) { $this->mBookingCompanyName = $data; }	
	
	var $mBookingCompanyShortName;
	function getBookingCompanyShortName() { return htmlspecialchars($this->mBookingCompanyShortName); }
	function setBookingCompanyShortName($data) { $this->mBookingCompanyShortName = $data; }	
	
	var $mBookingSaleId;
	function getBookingSaleId() { return htmlspecialchars($this->mBookingSaleId); }
	function setBookingSaleId($data) { $this->mBookingSaleId = $data; }

	var $mBookingSaleName;
	function getBookingSaleName() { return htmlspecialchars($this->mBookingSaleName); }
	function setBookingSaleName($data) { $this->mBookingSaleName = $data; }		
	
	var $mBookingSaleNickname;
	function getBookingSaleNickname() { return htmlspecialchars($this->mBookingSaleNickname); }
	function setBookingSaleNickname($data) { $this->mBookingSaleNickname = $data; }		
	
	var $mBookingTeamId;
	function getBookingTeamId() { return htmlspecialchars($this->mBookingTeamId); }
	function setBookingTeamId($data) { $this->mBookingTeamId = $data; }
	
	var $mBookingTeamName;
	function getBookingTeamName() { return htmlspecialchars($this->mBookingTeamName); }
	function setBookingTeamName($data) { $this->mBookingTeamName = $data; }		
	
	var $mBookingRemark;
	function getBookingRemark() { return htmlspecialchars($this->mBookingRemark); }
	function setBookingRemark($data) { $this->mBookingRemark = $data; }

	var $mBookingCancelDate;
	function getBookingCancelDate() { return htmlspecialchars($this->mBookingCancelDate); }
	function setBookingCancelDate($data) { $this->mBookingCancelDate = $data; }		
	
	var $mBookingCancelNumber;
	function getBookingCancelNumber() { return htmlspecialchars($this->mBookingCancelNumber); }
	function setBookingCancelNumber($data) { $this->mBookingCancelNumber = $data; }		
	
	var $mBookingCancelReason;
	function getBookingCancelReason() { return htmlspecialchars($this->mBookingCancelReason); }
	function setBookingCancelReason($data) { $this->mBookingCancelReason = $data; }
	
	var $mBookingCancelPrice;
	function getBookingCancelPrice() { return htmlspecialchars($this->mBookingCancelPrice); }
	function setBookingCancelPrice($data) { $this->mBookingCancelPrice = $data; }	

	var $mBookingCancelApprove;
	function getBookingCancelApprove() { return htmlspecialchars($this->mBookingCancelApprove); }
	function setBookingCancelApprove($data) { $this->mBookingCancelApprove = $data; }	
	
	var $mBookingMail;
	function getBookingMail() { return htmlspecialchars($this->mBookingMail); }
	function setBookingMail($data) { $this->mBookingMail = $data; }	
	
	var $mBookingMailDate;
	function getBookingMailDate() { return htmlspecialchars($this->mBookingMailDate); }
	function setBookingMailDate($data) { $this->mBookingMailDate = $data; }	

	var $mBookingThankyou;
	function getBookingThankyou() { return htmlspecialchars($this->mBookingThankyou); }
	function setBookingThankyou($data) { $this->mBookingThankyou = $data; }	

	
	var $mSendingNumber;
	function getSendingNumber() { return htmlspecialchars($this->mSendingNumber); }
	function setSendingNumber($data) { $this->mSendingNumber = $data; }
	
	var $mSendingNumberNohead;
	function getSendingNumberNohead() { return htmlspecialchars($this->mSendingNumberNohead); }
	function setSendingNumberNohead($data) { $this->mSendingNumberNohead = $data; }
	
	var $mSendingCustomerId;
	function getSendingCustomerId() { return htmlspecialchars($this->mSendingCustomerId); }
	function setSendingCustomerId($data) { $this->mSendingCustomerId = $data; }
			
	var $mSendingDate;
	function getSendingDate() { return htmlspecialchars($this->mSendingDate); }
	function setSendingDate($data) { $this->mSendingDate = $data; }
	
	var $mSendingCompanyName;
	function getSendingCompanyName() { return htmlspecialchars($this->mSendingCompanyName); }
	function setSendingCompanyName($data) { $this->mSendingCompanyName = $data; }		
	
	var $mSendingCompanyShortName;
	function getSendingCompanyShortName() { return htmlspecialchars($this->mSendingCompanyShortName); }
	function setSendingCompanyShortName($data) { $this->mSendingCompanyShortName = $data; }	
	
	var $mSendingSaleId;
	function getSendingSaleId() { return htmlspecialchars($this->mSendingSaleId); }
	function setSendingSaleId($data) { $this->mSendingSaleId = $data; }
	
	var $mSendingSaleName;
	function getSendingSaleName() { return htmlspecialchars($this->mSendingSaleName); }
	function setSendingSaleName($data) { $this->mSendingSaleName = $data; }		
	
	var $mSendingSaleNickname;
	function getSendingSaleNickname() { return htmlspecialchars($this->mSendingSaleNickname); }
	function setSendingSaleNickname($data) { $this->mSendingSaleNickname = $data; }			

	var $mSendingTeamId;
	function getSendingTeamId() { return htmlspecialchars($this->mSendingTeamId); }
	function setSendingTeamId($data) { $this->mSendingTeamId = $data; }
	
	var $mSendingTeamName;
	function getSendingTeamName() { return htmlspecialchars($this->mSendingTeamName); }
	function setSendingTeamName($data) { $this->mSendingTeamName = $data; }	
	
	var $mSendingRemark;
	function getSendingRemark() { return htmlspecialchars($this->mSendingRemark); }
	function setSendingRemark($data) { $this->mSendingRemark = $data; }		

	var $mSendingCarRemark;
	function getSendingCarRemark() { return htmlspecialchars($this->mSendingCarRemark); }
	function setSendingCarRemark($data) { $this->mSendingCarRemark = $data; }		

	var $mSendingCancelDate;
	function getSendingCancelDate() { return htmlspecialchars($this->mSendingCancelDate); }
	function setSendingCancelDate($data) { $this->mSendingCancelDate = $data; }		
	
	var $mSendingCancelNumber;
	function getSendingCancelNumber() { return htmlspecialchars($this->mSendingCancelNumber); }
	function setSendingCancelNumber($data) { $this->mSendingCancelNumber = $data; }		
	
	var $mSendingCancelReason;
	function getSendingCancelReason() { return htmlspecialchars($this->mSendingCancelReason); }
	function setSendingCancelReason($data) { $this->mSendingCancelReason = $data; }
	
	var $mSendingCancelPrice;
	function getSendingCancelPrice() { return htmlspecialchars($this->mSendingCancelPrice); }
	function setSendingCancelPrice($data) { $this->mSendingCancelPrice = $data; }	

	var $mSendingCancelApprove;
	function getSendingCancelApprove() { return htmlspecialchars($this->mSendingCancelApprove); }
	function setSendingCancelApprove($data) { $this->mSendingCancelApprove = $data; }		
	
	var $mSendingMail;
	function getSendingMail() { return htmlspecialchars($this->mSendingMail); }
	function setSendingMail($data) { $this->mSendingMail = $data; }	
	
	var $mSendingMailDate;
	function getSendingMailDate() { return htmlspecialchars($this->mSendingMailDate); }
	function setSendingMailDate($data) { $this->mSendingMailDate = $data; }	
	
	var $mSendingCashierDate;
	function getSendingCashierDate() { return htmlspecialchars($this->mSendingCashierDate); }
	function setSendingCashierDate($data) { $this->mSendingCashierDate = $data; }
	
	var $mSendingCashierTime;
	function getSendingCashierTime() { return htmlspecialchars($this->mSendingCashierTime); }
	function setSendingCashierTime($data) { $this->mSendingCashierTime = $data; }
	
	var $mSendingCashierId;
	function getSendingCashierId() { return htmlspecialchars($this->mSendingCashierId); }
	function setSendingCashierId($data) { $this->mSendingCashierId = $data; }	
	
	var $mSendingThankyou;
	function getSendingThankyou() { return htmlspecialchars($this->mSendingThankyou); }
	function setSendingThankyou($data) { $this->mSendingThankyou = $data; }	

	var $mSwitchSale;
	function getSwitchSale() { return htmlspecialchars($this->mSwitchSale); }
	function setSwitchSale($data) { $this->mSwitchSale = $data; }
	
	var $mSwitchCar;
	function getSwitchCar() { return htmlspecialchars($this->mSwitchCar); }
	function setSwitchCar($data) { $this->mSwitchCar = $data; }

	var $mSwitchCustomer;
	function getSwitchCustomer() { return htmlspecialchars($this->mSwitchCustomer); }
	function setSwitchCustomer($data) { $this->mSwitchCustomer = $data; }

	var $mStockCarId;
	function getStockCarId() { return htmlspecialchars($this->mStockCarId); }
	function setStockCarId($data) { $this->mStockCarId = $data; }	
	
	var $mStockCarIdTemp;
	function getStockCarIdTemp() { return htmlspecialchars($this->mStockCarIdTemp); }
	function setStockCarIdTemp($data) { $this->mStockCarIdTemp = $data; }	
	
	var $mCancelStockCar;
	function getCancelStockCar() { return htmlspecialchars($this->mCancelStockCar); }
	function setCancelStockCar($data) { $this->mCancelStockCar = $data; }	
	
	var $mCarNumber;
	function getCarNumber() { return htmlspecialchars($this->mCarNumber); }
	function setCarNumber($data) { $this->mCarNumber = $data; }		
	
	var $mStockRedId;
	function getStockRedId() { return htmlspecialchars($this->mStockRedId); }
	function setStockRedId($data) { $this->mStockRedId = $data; }
	
	var $mRedCodePrice;
	function getRedCodePrice() { return htmlspecialchars($this->mRedCodePrice); }
	function setRedCodePrice($data) { $this->mRedCodePrice = $data; }
	
	var $mNoRedCode;
	function getNoRedCode() { return htmlspecialchars($this->mNoRedCode); }
	function setNoRedCode($data) { $this->mNoRedCode = $data; }	
	
	var $mBlackCodeStatus;
	function getBlackCodeStatus() { return htmlspecialchars($this->mBlackCodeStatus); }
	function setBlackCodeStatus($data) { $this->mBlackCodeStatus = $data; }
	
	var $mBlackCodeNumber;
	function getBlackCodeNumber() { return htmlspecialchars($this->mBlackCodeNumber); }
	function setBlackCodeNumber($data) { $this->mBlackCodeNumber = $data; }
	
	var $mBlackCodeDate;
	function getBlackCodeDate() { return htmlspecialchars($this->mBlackCodeDate); }
	function setBlackCodeDate($data) { $this->mBlackCodeDate = $data; }									
	
	var $mRecieveDate;
	function getRecieveDate() { return htmlspecialchars($this->mRecieveDate); }
	function setRecieveDate($data) { $this->mRecieveDate = $data; }					
		
	var $mRegistryStatus;
	//1 = yes
	//2 = no
	function getRegistryStatus() { return htmlspecialchars($this->mRegistryStatus); }
	
	function getRegistryStatusDetail() {
		if($this->mRegistryStatus == 0) return "�ѧ����騴����¹";	
		if($this->mRegistryStatus == 1) return "�͡������ú";
		if($this->mRegistryStatus == 2) return "��ͤ�Ţ";
		if($this->mRegistryStatus == 3) return "�觨�����¹";
		if($this->mRegistryStatus == 4) return "�Ѻ���·���¹�ҡ����";
		if($this->mRegistryStatus == 5) return "�١����Ѻ����";
		if($this->mRegistryStatus == 6) return "����";
	 }	
	
	
	
	function setRegistryStatus($data) { $this->mRegistryStatus = $data; }
		
	var $mRegistryConditionId;
	function getRegistryConditionId() { return htmlspecialchars($this->mRegistryConditionId); }
	function setRegistryConditionId($data) { $this->mRegistryConditionId = $data; }							
		
	var $mRegistryPrice;
	function getRegistryPrice() { return htmlspecialchars($this->mRegistryPrice); }
	function setRegistryPrice($data) { $this->mRegistryPrice = $data; }					
	
	var $mRegistryTaxYear;
	function getRegistryTaxYear() { return htmlspecialchars($this->mRegistryTaxYear); }
	function setRegistryTaxYear($data) { $this->mRegistryTaxYear = $data; }					
	
	var $mRegistryTaxYearNumber;
	function getRegistryTaxYearNumber() { return htmlspecialchars($this->mRegistryTaxYearNumber); }
	function setRegistryTaxYearNumber($data) { $this->mRegistryTaxYearNumber = $data; }					
	
	var $mRegistrySendDate;
	function getRegistrySendDate() { return htmlspecialchars($this->mRegistrySendDate); }
	function setRegistrySendDate($data) { $this->mRegistrySendDate = $data; }					
	
	var $mRegistryMissDocument;
	function getRegistryMissDocument() { return htmlspecialchars($this->mRegistryMissDocument); }
	function setRegistryMissDocument($data) { $this->mRegistryMissDocument = $data; }					
		
	var $mRegistryCondition;
	function getRegistryCondition() { return htmlspecialchars($this->mRegistryCondition); }
	function setRegistryCondition($data) { $this->mRegistryCondition = $data; }					
		
	var $mRegistryTakeCode;
	function getRegistryTakeCode() { return htmlspecialchars($this->mRegistryTakeCode); }
	function setRegistryTakeCode($data) { $this->mRegistryTakeCode = $data; }					
		
	var $mRegistryTakeCodeDate;
	function getRegistryTakeCodeDate() { return htmlspecialchars($this->mRegistryTakeCodeDate); }
	function setRegistryTakeCodeDate($data) { $this->mRegistryTakeCodeDate = $data; }					
		
	var $mRegistryTakeBook;
	function getRegistryTakeBook() { return htmlspecialchars($this->mRegistryTakeBook); }
	function setRegistryTakeBook($data) { $this->mRegistryTakeBook = $data; }					
		
	var $mRegistryTakeBookDate;
	function getRegistryTakeBookDate() { return htmlspecialchars($this->mRegistryTakeBookDate); }
	function setRegistryTakeBookDate($data) { $this->mRegistryTakeBookDate = $data; }					
		
	var $mRegistryRemark;
	function getRegistryRemark() { return htmlspecialchars($this->mRegistryRemark); }
	function setRegistryRemark($data) { $this->mRegistryRemark = $data; }
	
	var $mRegistryEditBy;
	function getRegistryEditBy() { return htmlspecialchars($this->mRegistryEditBy); }
	function setRegistryEditBy($data) { $this->mRegistryEditBy = $data; }
	
	var $mRegistryEditDate;
	function getRegistryEditDate() { return htmlspecialchars($this->mRegistryEditDate); }
	function setRegistryEditDate($data) { $this->mRegistryEditDate = $data; }
		
	var $mRegistryPerson;
	function getRegistryPerson() { return htmlspecialchars($this->mRegistryPerson); }
	function setRegistryPerson($data) { $this->mRegistryPerson = $data; }
	
	var $mRegistryDate01;
	function getRegistryDate01() { return htmlspecialchars($this->mRegistryDate01); }
	function setRegistryDate01($data) { $this->mRegistryDate01 = $data; }
	
	var $mRegistryDate02;
	function getRegistryDate02() { return htmlspecialchars($this->mRegistryDate02); }
	function setRegistryDate02($data) { $this->mRegistryDate02 = $data; }
	
	var $mRegistryDate05;
	function getRegistryDate05() { return htmlspecialchars($this->mRegistryDate05); }
	function setRegistryDate05($data) { $this->mRegistryDate05 = $data; }
	
	var $mRegistryDate07;
	function getRegistryDate07() { return htmlspecialchars($this->mRegistryDate07); }
	function setRegistryDate07($data) { $this->mRegistryDate07 = $data; }
		
	var $mRegistryText03;
	function getRegistryText03() { return htmlspecialchars($this->mRegistryText03); }
	function setRegistryText03($data) { $this->mRegistryText03 = $data; }
		
	var $mRegistryText04;
	function getRegistryText04() { return htmlspecialchars($this->mRegistryText04); }
	function setRegistryText04($data) { $this->mRegistryText04 = $data; }
	
	var $mRegistryText05;
	function getRegistryText05() { return htmlspecialchars($this->mRegistryText05); }
	function setRegistryText05($data) { $this->mRegistryText05 = $data; }
	
	var $mRegistryText06;
	function getRegistryText06() { return htmlspecialchars($this->mRegistryText06); }
	function setRegistryText06($data) { $this->mRegistryText06 = $data; }
	
	var $mRegistryText07;
	function getRegistryText07() { return htmlspecialchars($this->mRegistryText07); }
	function setRegistryText07($data) { $this->mRegistryText07 = $data; }
	
	var $mRegistryRemark01;
	function getRegistryRemark01() { return htmlspecialchars($this->mRegistryRemark01); }
	function setRegistryRemark01($data) { $this->mRegistryRemark01 = $data; }
	
	var $mRegistryRemark02;
	function getRegistryRemark02() { return htmlspecialchars($this->mRegistryRemark02); }
	function setRegistryRemark02($data) { $this->mRegistryRemark02 = $data; }
	
	var $mRegistryRemark03;
	function getRegistryRemark03() { return htmlspecialchars($this->mRegistryRemark03); }
	function setRegistryRemark03($data) { $this->mRegistryRemark03 = $data; }
	
	var $mRegistryRemark04;
	function getRegistryRemark04() { return htmlspecialchars($this->mRegistryRemark04); }
	function setRegistryRemark04($data) { $this->mRegistryRemark04 = $data; }
		
	var $mRegistryRemark05;
	function getRegistryRemark05() { return htmlspecialchars($this->mRegistryRemark05); }
	function setRegistryRemark05($data) { $this->mRegistryRemark05 = $data; }
		
	var $mRegistryRemark06;
	function getRegistryRemark06() { return htmlspecialchars($this->mRegistryRemark06); }
	function setRegistryRemark06($data) { $this->mRegistryRemark06 = $data; }
	
	var $mRegistryRemark07;
	function getRegistryRemark07() { return htmlspecialchars($this->mRegistryRemark07); }
	function setRegistryRemark07($data) { $this->mRegistryRemark07 = $data; }
	
	var $mRegistryRemark08;
	function getRegistryRemark08() { return htmlspecialchars($this->mRegistryRemark08); }
	function setRegistryRemark08($data) { $this->mRegistryRemark08 = $data; }
	
	var $mRegistryRecieveType01;
	function getRegistryRecieveType01() { return htmlspecialchars($this->mRegistryRecieveType01); }
	function setRegistryRecieveType01($data) { $this->mRegistryRecieveType01 = $data; }
	
	var $mRegistryRecieveType02;
	function getRegistryRecieveType02() { return htmlspecialchars($this->mRegistryRecieveType02); }
	function setRegistryRecieveType02($data) { $this->mRegistryRecieveType02 = $data; }
	
	var $mRegistryRecieveName01;
	function getRegistryRecieveName01() { return htmlspecialchars($this->mRegistryRecieveName01); }
	function setRegistryRecieveName01($data) { $this->mRegistryRecieveName01 = $data; }
	
	var $mRegistryRecieveName02;
	function getRegistryRecieveName02() { return htmlspecialchars($this->mRegistryRecieveName02); }
	function setRegistryRecieveName02($data) { $this->mRegistryRecieveName02 = $data; }
	
	var $mRegistrySign;
	function getRegistrySign() { return htmlspecialchars($this->mRegistrySign); }
	function setRegistrySign($data) { $this->mRegistrySign = $data; }	
	
	var $mRegistrySignDate;
	function getRegistrySignDate() { return htmlspecialchars($this->mRegistrySignDate); }
	function setRegistrySignDate($data) { $this->mRegistrySignDate = $data; }
	
	var $mRegistryPrice1;
	function getRegistryPrice1() { return htmlspecialchars($this->mRegistryPrice1); }
	function setRegistryPrice1($data) { $this->mRegistryPrice1 = $data; }
	
	var $mBuyType;
	function getBuyType() { return htmlspecialchars($this->mBuyType); }
	function setBuyType($data) { $this->mBuyType = $data; }
	
	var $mBuyCompany;
	function getBuyCompany() { return htmlspecialchars($this->mBuyCompany); }
	function setBuyCompany($data) { $this->mBuyCompany = $data; }
	
	var $mBuyFee;
	function getBuyFee() { return htmlspecialchars($this->mBuyFee); }
	function setBuyFee($data) { $this->mBuyFee = $data; }
	
	var $mBuyTime;
	function getBuyTime() { return htmlspecialchars($this->mBuyTime); }
	function setBuyTime($data) { $this->mBuyTime = $data; }
	
	var $mBuyPayment;
	function getBuyPayment() { return htmlspecialchars($this->mBuyPayment); }
	function setBuyPayment($data) { $this->mBuyPayment = $data; }
	
	var $mBuyPrice;
	function getBuyPrice() { return htmlspecialchars($this->mBuyPrice); }
	function setBuyPrice($data) { $this->mBuyPrice = $data; }
	
	var $mBuyDown;
	function getBuyDown() { return htmlspecialchars($this->mBuyDown); }
	function setBuyDown($data) { $this->mBuyDown = $data; }
	
	var $mBuyTotal;
	function getBuyTotal() { return htmlspecialchars($this->mBuyTotal); }
	function setBuyTotal($data) { $this->mBuyTotal = $data; }
	
	var $mBuyRemark;
	function getBuyRemark() { return htmlspecialchars($this->mBuyRemark); }
	function setBuyRemark($data) { $this->mBuyRemark = $data; }			
	
	var $mBuyBegin;
	function getBuyBegin() { return htmlspecialchars($this->mBuyBegin); }
	function setBuyBegin($data) { $this->mBuyBegin = $data; }			
	
	var $mBuyProduct;
	function getBuyProduct() { return htmlspecialchars($this->mBuyProduct); }
	function setBuyProduct($data) { $this->mBuyProduct = $data; }			
	
	var $mBuyEngine;
	function getBuyEngine() { return htmlspecialchars($this->mBuyEngine); }
	function setBuyEngine($data) { $this->mBuyEngine = $data; }			
	
	var $mBuyEngineRemark;
	function getBuyEngineRemark() { return htmlspecialchars($this->mBuyEngineRemark); }
	function setBuyEngineRemark($data) { $this->mBuyEngineRemark = $data; }			
	
	var $mPrbCompany;
	function getPrbCompany() { return htmlspecialchars($this->mPrbCompany); }
	function setPrbCompany($data) { $this->mPrbCompany = $data; }

	var $mPrbExpire;
	function getPrbExpire() { return htmlspecialchars($this->mPrbExpire); }
	function setPrbExpire($data) { $this->mPrbExpire = $data; }
	
	var $mPrbPrice;
	function getPrbPrice() { return htmlspecialchars($this->mPrbPrice); }
	function setPrbPrice($data) { $this->mPrbPrice = $data; }
	
	var $mInsureCompany;
	function getInsureCompany() { return htmlspecialchars($this->mInsureCompany); }
	function setInsureCompany($data) { $this->mInsureCompany = $data; }
	
	var $mInsureExpire;
	function getInsureExpire() { return htmlspecialchars($this->mInsureExpire); }
	function setInsureExpire($data) { $this->mInsureExpire = $data; }
	
	var $mInsurePrice;
	function getInsurePrice() { return htmlspecialchars($this->mInsurePrice); }
	function setInsurePrice($data) { $this->mInsurePrice = $data; }
	
	var $mInsureType;
	function getInsureType() { return htmlspecialchars($this->mInsureType); }
	function setInsureType($data) { $this->mInsureType = $data; }
	
	var $mInsureYear;
	function getInsureYear() { return htmlspecialchars($this->mInsureYear); }
	function setInsureYear($data) { $this->mInsureYear = $data; }
	
	var $mInsureBudget;
	function getInsureBudget() { return htmlspecialchars($this->mInsureBudget); }
	function setInsureBudget($data) { $this->mInsureBudget = $data; }

	var $mInsureFrom;
	function getInsureFrom() { return htmlspecialchars($this->mInsureFrom); }
	function setInsureFrom($data) { $this->mInsureFrom = $data; }		

	var $mInsureFromDetail;
	function getInsureFromDetail() { return htmlspecialchars($this->mInsureFromDetail); }
	function setInsureFromDetail($data) { $this->mInsureFromDetail = $data; }		
	
	var $mInsureRemark;
	function getInsureRemark() { return htmlspecialchars($this->mInsureRemark); }
	function setInsureRemark($data) { $this->mInsureRemark = $data; }	
	
	var $mInsureNumber;
	function getInsureNumber() { return htmlspecialchars($this->mInsureNumber); }
	function setInsureNumber($data) { $this->mInsureNumber = $data; }	

	var $mInsureCustomerPay;
	function getInsureCustomerPay() { return htmlspecialchars($this->mInsureCustomerPay); }
	function setInsureCustomerPay($data) { $this->mInsureCustomerPay = $data; }		
	
	var $mInsureImport;
	function getInsureImport() { return htmlspecialchars($this->mInsureImport); }
	function setInsureImport($data) { $this->mInsureImport = $data; }
	
	var $mDiscountPrice;
	function getDiscountPrice() { return htmlspecialchars($this->mDiscountPrice); }
	function setDiscountPrice($data) { $this->mDiscountPrice = $data; }	

	var $mDiscountSubdown;
	function getDiscountSubdown() { return htmlspecialchars($this->mDiscountSubdown); }
	function setDiscountSubdown($data) { $this->mDiscountSubdown = $data; }	
	
	var $mDiscountPremium;
	function getDiscountPremium() { return htmlspecialchars($this->mDiscountPremium); }
	function setDiscountPremium($data) { $this->mDiscountPremium = $data; }	
	
	var $mDiscountProduct;
	function getDiscountProduct() { return htmlspecialchars($this->mDiscountProduct); }
	function setDiscountProduct($data) { $this->mDiscountProduct = $data; }	
	
	var $mDiscountInsurance;
	function getDiscountInsurance() { return htmlspecialchars($this->mDiscountInsurance); }
	function setDiscountInsurance($data) { $this->mDiscountInsurance = $data; }	
	
	var $mDiscountGoa;
	function getDiscountGoa() { return htmlspecialchars($this->mDiscountGoa); }
	function setDiscountGoa($data) { $this->mDiscountGoa = $data; }
	
	var $mDiscount;
	function getDiscount() { return htmlspecialchars($this->mDiscount); }
	function setDiscount($data) { $this->mDiscount = $data; }	

	var $mDiscountSubdownVat;
	function getDiscountSubdownVat() { return htmlspecialchars($this->mDiscountSubdownVat); }
	function setDiscountSubdownVat($data) { $this->mDiscountSubdownVat = $data; }	
	
	var $mDiscountSubdownVatPercent;
	function getDiscountSubdownVatPercent() { return htmlspecialchars($this->mDiscountSubdownVatPercent); }
	function setDiscountSubdownVatPercent($data) { $this->mDiscountSubdownVatPercent = $data; }		
	
	var $mDiscountMargin;
	function getDiscountMargin() { return htmlspecialchars($this->mDiscountMargin); }
	function setDiscountMargin($data) { $this->mDiscountMargin = $data; }	
	
	var $mDiscountMarginDate;
	function getDiscountMarginDate() { return htmlspecialchars($this->mDiscountMarginDate); }
	function setDiscountMarginDate($data) { $this->mDiscountMarginDate = $data; }	

	var $mDiscountOther;
	function getDiscountOther() { return htmlspecialchars($this->mDiscountOther); }
	function setDiscountOther($data) { $this->mDiscountOther = $data; }	

	var $mDiscountOtherPrice;
	function getDiscountOtherPrice() { return htmlspecialchars($this->mDiscountOtherPrice); }
	function setDiscountOtherPrice($data) { $this->mDiscountOtherPrice = $data; }	
	
	var $mDiscountAll;
	function getDiscountAll() { return htmlspecialchars($this->mDiscountAll); }
	function setDiscountAll($data) { $this->mDiscountAll = $data; }	
	
	var $mDiscountOver;
	function getDiscountOver() { return htmlspecialchars($this->mDiscountOver); }
	function setDiscountOver($data) { $this->mDiscountOver = $data; }
	
	var $mDiscountCustomerPay;
	function getDiscountCustomerPay() { return htmlspecialchars($this->mDiscountCustomerPay); }
	function setDiscountCustomerPay($data) { $this->mDiscountCustomerPay = $data; }
	
	var $mDiscountCustomerPayReason;
	function getDiscountCustomerPayReason() { return htmlspecialchars($this->mDiscountCustomerPayReason); }
	function setDiscountCustomerPayReason($data) { $this->mDiscountCustomerPayReason = $data; }
	
	var $mDiscountTMT;
	function getDiscountTMT() { return htmlspecialchars($this->mDiscountTMT); }
	function setDiscountTMT($data) { $this->mDiscountTMT = $data; }
	
	var $mTMBNumber;
	function getTMBNumber() { return htmlspecialchars($this->mTMBNumber); }
	function setTMBNumber($data) { $this->mTMBNumber = $data; }	
	
	var $mTMTFree;
	function getTMTFree() { return htmlspecialchars($this->mTMTFree); }
	function setTMTFree($data) { $this->mTMTFree = $data; }	
	
	var $mTMTFreeRemark;
	function getTMTFreeRemark() { return htmlspecialchars($this->mTMTFreeRemark); }
	function setTMTFreeRemark($data) { $this->mTMTFreeRemark = $data; }	
	
	var $mTMTCoupon;
	function getTMTCoupon() { return htmlspecialchars($this->mTMTCoupon); }
	function setTMTCoupon($data) { $this->mTMTCoupon = $data; }	
	
	var $mTMTCouponRemark;
	function getTMTCouponRemark() { return htmlspecialchars($this->mTMTCouponRemark); }
	function setTMTCouponRemark($data) { $this->mTMTCouponRemark = $data; }	
	
	var $mTMTOther;
	function getTMTOther() { return htmlspecialchars($this->mTMTOther); }
	function setTMTOther($data) { $this->mTMTOther = $data; }	
	
	var $mTMTOtherRemark;
	function getTMTOtherRemark() { return htmlspecialchars($this->mTMTOtherRemark); }
	function setTMTOtherRemark($data) { $this->mTMTOtherRemark = $data; }
	
	var $mFinanceDate;
	function getFinanceDate() { return htmlspecialchars($this->mFinanceDate); }
	function setFinanceDate($data) { $this->mFinanceDate = $data; }
	
	var $mFinanceNumber;
	function getFinanceNumber() { return htmlspecialchars($this->mFinanceNumber); }
	function setFinanceNumber($data) { $this->mFinanceNumber = $data; }
	
	var $mComCar;
	function getComCar() { return htmlspecialchars($this->mComCar); }
	function setComCar($data) { $this->mComCar = $data; }	
	
	var $mComFinance;
	function getComFinance() { return htmlspecialchars($this->mComFinance); }
	function setComFinance($data) { $this->mComFinance = $data; }
	
	var $mComInsure;
	function getComInsure() { return htmlspecialchars($this->mComInsure); }
	function setComInsure($data) { $this->mComInsure = $data; }
	
	var $mComOldCar;
	function getComOldCar() { return htmlspecialchars($this->mComOldCar); }
	function setComOldCar($data) { $this->mComOldCar = $data; }
	
	var $mComEquipment;
	function getComEquipment() { return htmlspecialchars($this->mComEquipment); }
	function setComEquipment($data) { $this->mComEquipment = $data; }
	
	var $mComExtra;
	function getComExtra() { return htmlspecialchars($this->mComExtra); }
	function setComExtra($data) { $this->mComExtra = $data; }
	
	var $mComExtraRemark;
	function getComExtraRemark() { return htmlspecialchars($this->mComExtraRemark); }
	function setComExtraRemark($data) { $this->mComExtraRemark = $data; }
	
	var $mFinanceRemain;
	function getFinanceRemain() { return htmlspecialchars($this->mFinanceRemain); }
	function setFinanceRemain($data) { $this->mFinanceRemain = $data; }
	
	var $mFinanceCom;
	function getFinanceCom() { return htmlspecialchars($this->mFinanceCom); }
	function setFinanceCom($data) { $this->mFinanceCom = $data; }
	
	var $mFinanceComInsure;
	function getFinanceComInsure() { return htmlspecialchars($this->mFinanceComInsure); }
	function setFinanceComInsure($data) { $this->mFinanceComInsure = $data; }	
	
	var $mFinanceFaceCar;
	function getFinanceFaceCar() { return htmlspecialchars($this->mFinanceFaceCar); }
	function setFinanceFaceCar($data) { $this->mFinanceFaceCar = $data; }		
	
	var $mFinanceFaceCom;
	function getFinanceFaceCom() { return htmlspecialchars($this->mFinanceFaceCom); }
	function setFinanceFaceCom($data) { $this->mFinanceFaceCom = $data; }	
	
	var $mScan01;
	function getScan01() { return htmlspecialchars($this->mScan01); }
	function setScan01($data) { $this->mScan01 = $data; }
	
	var $mScan02;
	function getScan02() { return htmlspecialchars($this->mScan02); }
	function setScan02($data) { $this->mScan02 = $data; }
	
	var $mScan03;
	function getScan03() { return htmlspecialchars($this->mScan03); }
	function setScan03($data) { $this->mScan03 = $data; }
	
	var $mScan04;
	function getScan04() { return htmlspecialchars($this->mScan04); }
	function setScan04($data) { $this->mScan04 = $data; }

	var $mScan05;
	function getScan05() { return htmlspecialchars($this->mScan05); }
	function setScan05($data) { $this->mScan05 = $data; }
	
	var $mScan06;
	function getScan06() { return htmlspecialchars($this->mScan06); }
	function setScan06($data) { $this->mScan06 = $data; }	
	
	var $mScan07;
	function getScan07() { return htmlspecialchars($this->mScan07); }
	function setScan07($data) { $this->mScan07 = $data; }
	
	var $mScan08;
	function getScan08() { return htmlspecialchars($this->mScan08); }
	function setScan08($data) { $this->mScan08 = $data; }
	
	var $mScan09;
	function getScan09() { return htmlspecialchars($this->mScan09); }
	function setScan09($data) { $this->mScan09 = $data; }
	
	var $mScan10;
	function getScan10() { return htmlspecialchars($this->mScan10); }
	function setScan10($data) { $this->mScan10 = $data; }
	
	var $mScan11;
	function getScan11() { return htmlspecialchars($this->mScan11); }
	function setScan11($data) { $this->mScan11 = $data; }				

	var $mCarMarginId;
	function getCarMarginId() { return htmlspecialchars($this->mCarMarginId); }
	function setCarMarginId($data) { $this->mCarMarginId = $data; }
	
	var $mAddBy;
	function getAddBy() { return htmlspecialchars($this->mAddBy); }
	function setAddBy($data) { $this->mAddBy = $data; }

	var $mAddDate;
	function getAddDate() { return htmlspecialchars($this->mAddDate); }
	function setAddDate($data) { $this->mAddDate = $data; }
	
	var $mEditBy;
	function getEditBy() { return htmlspecialchars($this->mEditBy); }
	function setEditBy($data) { $this->mEditBy = $data; }
	
	var $mEditDate;
	function getEditDate() { return htmlspecialchars($this->mEditDate); }
	function setEditDate($data) { $this->mEditDate = $data; }	
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }		
	
	var $mCheckPurchase;
	function getCheckPurchase() { return $this->mCheckPurchase; }
	function setCheckPurchase($data) { $this->mCheckPurchase = $data; }		
	
	function getCheckPurchaseDetail(){
		if($this->mCheckPurchase == 0) return "�͵�Ǩ�ͧ";
		if($this->mCheckPurchase == 1) return "��ҹ���͹��ѵ�";
		if($this->mCheckPurchase == 2) return "����ҹ���͹��ѵ�";
	}
	
	
	var $mCheckPurchaseRemark;
	function getCheckPurchaseRemark() { return $this->mCheckPurchaseRemark; }
	function setCheckPurchaseRemark($data) { $this->mCheckPurchaseRemark = $data; }			
	
	var $mCheckPurchaseBy;
	function getCheckPurchaseBy() { return $this->mCheckPurchaseBy; }
	function setCheckPurchaseBy($data) { $this->mCheckPurchaseBy = $data; }			
	
	var $mCheckAccountBooking;
	function getCheckAccountBooking() { return $this->mCheckAccountBooking; }
	function setCheckAccountBooking($data) { $this->mCheckAccountBooking = $data; }			
	
	var $mCheckAccountBookingDate;
	function getCheckAccountBookingDate() { return $this->mCheckAccountBookingDate; }
	function setCheckAccountBookingDate($data) { $this->mCheckAccountBookingDate = $data; }
	
	var $mCheckAccountBookingRemark;
	function getCheckAccountBookingRemark() { return $this->mCheckAccountBookingRemark; }
	function setCheckAccountBookingRemark($data) { $this->mCheckAccountBookingRemark = $data; }			
	
	var $mCheckAccountBookingBy;
	function getCheckAccountBookingBy() { return $this->mCheckAccountBookingBy; }
	function setCheckAccountBookingBy($data) { $this->mCheckAccountBookingBy = $data; }
	
	var $mCheckAccountSending;
	function getCheckAccountSending() { return $this->mCheckAccountSending; }
	function setCheckAccountSending($data) { $this->mCheckAccountSending = $data; }			
	
	var $mCheckAccountSendingDate;
	function getCheckAccountSendingDate() { return $this->mCheckAccountSendingDate; }
	function setCheckAccountSendingDate($data) { $this->mCheckAccountSendingDate = $data; }
	
	var $mCheckAccountSendingRemark;
	function getCheckAccountSendingRemark() { return $this->mCheckAccountSendingRemark; }
	function setCheckAccountSendingRemark($data) { $this->mCheckAccountSendingRemark = $data; }			
	
	var $mCheckAccountSendingBy;
	function getCheckAccountSendingBy() { return $this->mCheckAccountSendingBy; }
	function setCheckAccountSendingBy($data) { $this->mCheckAccountSendingBy = $data; }

	var $mCheckAccountSendingCom;
	function getCheckAccountSendingCom() { return $this->mCheckAccountSendingCom; }
	function setCheckAccountSendingCom($data) { $this->mCheckAccountSendingCom = $data; }
	
	var $mSumDiscountOver;
	function getSumDiscountOver() { return $this->mSumDiscountOver; }
	function setSumDiscountOver($data) { $this->mSumDiscountOver = $data; }
	
	var $mSumOrderId;
	function getSumOrderId() { return $this->mSumOrderId; }
	function setSumOrderId($data) { $this->mSumOrderId = $data; }
	
	var $mCredit;
	function getCredit() { return $this->mCredit; }
	function setCredit($data) { $this->mCredit = $data; }
	
	var $mCreditDate;
	function getCreditDate() { return $this->mCreditDate; }
	function setCreditDate($data) { $this->mCreditDate = $data; }
	
	var $mCreditSendingDate;
	function getCreditSendingDate() { return $this->mCreditSendingDate; }
	function setCreditSendingDate($data) { $this->mCreditSendingDate = $data; }
	
	var $mTaxDate;
	function getTaxDate() { return $this->mTaxDate; }
	function setTaxDate($data) { $this->mTaxDate = $data; }
	
	var $mTaxDueDate;
	function getTaxDueDate() { return $this->mTaxDueDate; }
	function setTaxDueDate($data) { $this->mTaxDueDate = $data; }
	
	var $mTaxPrice;
	function getTaxPrice() { return $this->mTaxPrice; }
	function setTaxPrice($data) { $this->mTaxPrice = $data; }
	
	var $mEstimateFinanceDate;
	function getEstimateFinanceDate() { return $this->mEstimateFinanceDate; }
	function setEstimateFinanceDate($data) { $this->mEstimateFinanceDate = $data; }
	
	var $mStockStatus;
	function getStockStatus() { return $this->mStockStatus; }
	function setStockStatus($data) { $this->mStockStatus = $data; }	
	
	var $mStockCarStatus;
	function getStockCarStatus() { return $this->mStockCarStatus; }
	function setStockCarStatus($data) { $this->mStockCarStatus = $data; }	

	var $mStockCrlStatus;
	function getStockCrlStatus() { return $this->mStockCrlStatus; }
	function setStockCrlStatus($data) { $this->mStockCrlStatus = $data; }
	
	//�Ң����ͺ
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }
	
	//�ҢҨͧ
	var $mCompanyIds;
	function getCompanyIds() { return $this->mCompanyIds; }
	function setCompanyIds($data) { $this->mCompanyIds = $data; }	
	
	var $mFirstname;
	function getFirstname() { return $this->mFirstname; }
	function setFirstname($data) { $this->mFirstname = $data; }	
	
	var $mLastname;
	function getLastname() { return $this->mLastname; }
	function setLastname($data) { $this->mLastname = $data; }	
	
	var $mNickname;
	function getNickname() { return $this->mNickname; }
	function setNickname($data) { $this->mNickname = $data; }				
	
	var $mBookingPlace;
	function getBookingPlace() { return $this->mBookingPlace; }
	function setBookingPlace($data) { $this->mBookingPlace = $data; }				

	var $mSendingPlace;
	function getSendingPlace() { return $this->mSendingPlace; }
	function setSendingPlace($data) { $this->mSendingPlace = $data; }				
	
	var $mChangeProduct;
	function getChangeProduct() { return $this->mChangeProduct; }
	function setChangeProduct($data) { $this->mChangeProduct = $data; }				
	
	var $mChangeNumber;
	function getChangeNumber() { return $this->mChangeNumber; }
	function setChangeNumber($data) { $this->mChangeNumber = $data; }				
	
	var $mChangeRemark;
	function getChangeRemark() { return $this->mChangeRemark; }
	function setChangeRemark($data) { $this->mChangeRemark = $data; }				
	
	var $mBookingCancelReasonRemark ;
	function getBookingCancelReasonRemark() { return $this->mBookingCancelReasonRemark ; }
	function setBookingCancelReasonRemark($data) { $this->mBookingCancelReasonRemark  = $data; }		
	
	var $mFundName;
	function getFundName() { return $this->mFundName; }
	function setFundName($data) { $this->mFundName = $data; }

	var $mBuyReason;
	function getBuyReason() { return $this->mBuyReason; }
	function setBuyReason($data) { $this->mBuyReason = $data; }
	
	var $mBond01;
	function getBond01() { return $this->mBond01; }
	function setBond01($data) { $this->mBond01 = $data; }

	var $mBond01Relate;
	function getBond01Relate() { return $this->mBond01Relate; }
	function setBond01Relate($data) { $this->mBond01Relate = $data; }

	var $mBond01RelateRemark;
	function getBond01RelateRemark() { return $this->mBond01RelateRemark; }
	function setBond01RelateRemark($data) { $this->mBond01RelateRemark = $data; }
	
	var $mBond01Remark;
	function getBond01Remark() { return $this->mBond01Remark; }
	function setBond01Remark($data) { $this->mBond01Remark = $data; }
	
	var $mBond02;
	function getBond02() { return $this->mBond02; }
	function setBond02($data) { $this->mBond02 = $data; }
	
	var $mBond02Relate;
	function getBond02Relate() { return $this->mBond02Relate; }
	function setBond02Relate($data) { $this->mBond02Relate = $data; }

	var $mBond02RelateRemark;
	function getBond02RelateRemark() { return $this->mBond02RelateRemark; }
	function setBond02RelateRemark($data) { $this->mBond02RelateRemark = $data; }	

	var $mBond02Remark;
	function getBond02Remark() { return $this->mBond02Remark; }
	function setBond02Remark($data) { $this->mBond02Remark = $data; }	
	
	
	function Order($objData=NULL) {
        If ($objData->order_id !="" OR $objData->company_id != "" OR $objData->company_ids != "" OR  $objData->sending_sale_id != "" OR $objData->booking_sale_id != "" OR $objData->sending_team_id != "" OR $objData->booking_team_id != ""  OR $objData->sum_order_id != "" OR $objData->buy_company != "" OR $objData->sending_date != ""  ) {

            $this->setOrderId($objData->order_id);
            $this->setOrderDate($objData->order_date);
			$this->setOrderStatus($objData->order_status);
			$this->setOrderNumber($objData->order_number);
			$this->setRecieveNumber($objData->recieve_number);
			$this->setDealer($objData->dealer);
            $this->setOrderPrice($objData->order_price);
			$this->setOrderReservePrice($objData->order_reserve_price);
			$this->setOrderReservePrice01($objData->order_reserve_price01);
			$this->setOrderReservePrice02($objData->order_reserve_price02);
			$this->setOrderOldCar($objData->order_old_car);
			$this->setOrderOldCarNumber($objData->order_old_car_number);
            $this->setOrderLocation($objData->order_location);
			
			$this->setBookingNumber($objData->booking_number);
			$this->setBookingNumberNohead($objData->booking_number_nohead);
			$this->setBookingCustomerId($objData->booking_customer_id);
			$this->setBookingDate($objData->booking_date);
			$this->setBookingCashierDate($objData->booking_cashier_date);
			$this->setBookingCashierTime($objData->booking_cashier_time);
			$this->setBookingCashierId($objData->booking_cashier_id);
			$this->setBookingCarColor($objData->booking_car_color);
			$this->setBookingCarSeries($objData->booking_car_series);
			$this->setBookingCarType($objData->booking_car_type);
			$this->setBookingCarGear($objData->booking_car_gear);
			$this->setBookingCarPrice($objData->booking_car_price);
			$this->setBookingCarRemark($objData->booking_car_remark);
			$this->setBookingCompanyName($objData->booking_company_name);
			$this->setBookingCompanyShortName($objData->booking_company_short_name);
			$this->setBookingSaleId($objData->booking_sale_id);
			$this->setBookingSaleName($objData->booking_sale_name);
			$this->setBookingSaleNickname($objData->booking_sale_nickname);
			$this->setBookingTeamId($objData->booking_team_id);
			$this->setBookingTeamName($objData->booking_team_name);
			$this->setBookingRemark($objData->booking_remark);
			$this->setBookingCancelDate($objData->booking_cancel_date);
			$this->setBookingCancelNumber($objData->booking_cancel_number);
			$this->setBookingCancelReason($objData->booking_cancel_reason);
			$this->setBookingCancelPrice($objData->booking_cancel_price);
			$this->setBookingCancelApprove($objData->booking_cancel_approve);
			$this->setBookingMail($objData->booking_mail);
			$this->setBookingMailDate($objData->booking_mail_date);
			$this->setBookingThankyou($objData->booking_thankyou);

			
			$this->setSendingNumber($objData->sending_number);
			$this->setSendingNumberNohead($objData->sending_number_nohead);
			$this->setSendingCustomerId($objData->sending_customer_id);
			$this->setSendingDate($objData->sending_date);
			$this->setSendingCompanyName($objData->sending_company_name);
			$this->setSendingCompanyShortName($objData->sending_company_short_name);
			$this->setSendingSaleId($objData->sending_sale_id);
			$this->setSendingSaleName($objData->sending_sale_name);
			$this->setSendingSaleNickname($objData->sending_sale_nickname);			
			$this->setSendingTeamId($objData->sending_team_id);
			$this->setSendingTeamName($objData->sending_team_name);
			$this->setSendingRemark($objData->sending_remark);
			$this->setSendingCarRemark($objData->sending_car_remark);
			$this->setSendingCancelDate($objData->sending_cancel_date);
			$this->setSendingCancelNumber($objData->sending_cancel_number);
			$this->setSendingCancelReason($objData->sending_cancel_reason);
			$this->setSendingCancelPrice($objData->sending_cancel_price);
			$this->setSendingCancelApprove($objData->sending_cancel_approve);
			$this->setSendingMail($objData->sending_mail);
			$this->setSendingMailDate($objData->sending_mail_date);
			$this->setSendingCashierDate($objData->sending_cashier_date);
			$this->setSendingCashierTime($objData->sending_cashier_time);
			$this->setSendingCashierId($objData->sending_cashier_id);
			$this->setSendingThankyou($objData->sending_thankyou);
			
			$this->setSwitchSale($objData->switch_sale);
			$this->setSwitchCar($objData->switch_car);
			$this->setSwitchCustomer($objData->switch_customer);

			$this->setStockCarId($objData->stock_car_id);
			$this->setStockRedId($objData->stock_red_id);
			$this->setRedCodePrice($objData->red_code_price);
			$this->setNoRedCode($objData->no_red_code);
			
			$this->setBlackCodeStatus($objData->black_code_status);
			$this->setBlackCodeNumber($objData->black_code_number);
			$this->setBlackCodeDate($objData->black_code_date);
			$this->setRecieveDate($objData->recieve_date);
			
			$this->setRegistryStatus($objData->registry_status);
			$this->setRegistryConditionId($objData->registry_condition_id);
			$this->setRegistryPrice($objData->registry_price);
			$this->setRegistryTaxYear($objData->registry_tax_year);
			$this->setRegistryTaxYearNumber($objData->registry_tax_year_number);
			$this->setRegistrySendDate($objData->registry_send_date);
			$this->setRegistryMissDocument($objData->registry_miss_document);
			$this->setRegistryCondition($objData->registry_condition);
			$this->setRegistryTakeCode($objData->registry_take_code);
			$this->setRegistryTakeCodeDate($objData->registry_take_code_date);
			$this->setRegistryTakeBook($objData->registry_take_book);
			$this->setRegistryTakeBookDate($objData->registry_take_book_date);
			$this->setRegistryRemark($objData->registry_remark);
			$this->setRegistryEditBy($objData->registry_edit_by);
			$this->setRegistryEditDate($objData->registry_edit_date);
			$this->setRegistryPerson($objData->registry_person);
			$this->setRegistryDate01($objData->registry_date_01);
			$this->setRegistryDate02($objData->registry_date_02);
			$this->setRegistryDate05($objData->registry_date_05);
			$this->setRegistryDate07($objData->registry_date_07);
			$this->setRegistryText03($objData->registry_text_03);
			$this->setRegistryText04($objData->registry_text_04);
			$this->setRegistryText05($objData->registry_text_05);
			$this->setRegistryText06($objData->registry_text_06);
			$this->setRegistryText07($objData->registry_text_07);
			$this->setRegistryRemark01($objData->registry_remark_01);
			$this->setRegistryRemark02($objData->registry_remark_02);
			$this->setRegistryRemark03($objData->registry_remark_03);
			$this->setRegistryRemark04($objData->registry_remark_04);
			$this->setRegistryRemark05($objData->registry_remark_05);
			$this->setRegistryRemark06($objData->registry_remark_06);
			$this->setRegistryRemark07($objData->registry_remark_07);
			$this->setRegistryRemark08($objData->registry_remark_08);
			$this->setRegistryRecieveType01($objData->registry_recieve_type_01);
			$this->setRegistryRecieveType02($objData->registry_recieve_type_02);
			$this->setRegistryRecieveName01($objData->registry_recieve_name_01);
			$this->setRegistryRecieveName02($objData->registry_recieve_name_02);
			$this->setRegistrySign($objData->registry_sign);
			$this->setRegistrySignDate($objData->registry_sign_date);
			$this->setRegistryPrice1($objData->registry_price1);//�ҤҨ�����¹�ع

			$this->setBuyType($objData->buy_type);
			$this->setBuyCompany($objData->buy_company);
			$this->setBuyFee($objData->buy_fee);
			$this->setBuyTime($objData->buy_time);
			$this->setBuyPayment($objData->buy_payment);
			$this->setBuyPrice($objData->buy_price);
			$this->setBuyDown($objData->buy_down);
			$this->setBuyTotal($objData->buy_total);
			$this->setBuyRemark($objData->buy_remark);
			$this->setBuyBegin($objData->buy_begin);
			$this->setBuyProduct($objData->buy_product);
			$this->setBuyEngine($objData->buy_engine);
			$this->setBuyEngineRemark($objData->buy_engine_remark);
			
			$this->setPrbCompany($objData->prb_company);
			$this->setPrbExpire($objData->prb_expire);
			$this->setPrbPrice($objData->prb_price);
			$this->setInsureCompany($objData->insure_company);
			$this->setInsureExpire($objData->insure_expire);
			$this->setInsurePrice($objData->insure_price);
			$this->setInsureType($objData->insure_type);
			$this->setInsureYear($objData->insure_year);
			$this->setInsureBudget($objData->insure_budget);
			$this->setInsureFrom($objData->insure_from);
			$this->setInsureFromDetail($objData->insure_from_detail);
			$this->setInsureRemark($objData->insure_remark);
			$this->setInsureCustomerPay($objData->insure_customer_pay);
			$this->setInsureImport($objData->insure_import);
			
			$this->setDiscountPrice($objData->discount_price);
			$this->setDiscountSubdown($objData->discount_subdown);
			$this->setDiscountPremium($objData->discount_premium);
			$this->setDiscountProduct($objData->discount_product);
			$this->setDiscountInsurance($objData->discount_insurance);
			$this->setDiscountGoa($objData->discount_goa);
			$this->setDiscount($objData->discount);
			$this->setDiscountSubdownVat($objData->discount_subdown_vat);
			$this->setDiscountSubdownVatPercent($objData->discount_subdown_vat_percent);
			$this->setDiscountMargin($objData->discount_margin);
			$this->setDiscountMarginDate($objData->discount_margin_date);
			$this->setDiscountOther($objData->discount_other);
			$this->setDiscountOtherPrice($objData->discount_other_price);
			$this->setDiscountAll($objData->discount_all);
			$this->setDiscountOver($objData->discount_over);
			$this->setDiscountCustomerPay($objData->discount_customer_pay);
			$this->setDiscountCustomerPayReason($objData->discount_customer_pay_reason);
			$this->setDiscountTMT($objData->discount_tmt);
			
			$this->setTMBNumber($objData->tmb_number);
			
			$this->setTMTFree($objData->tmt_free);
			$this->setTMTFreeRemark($objData->tmt_free_remark);
			$this->setTMTCoupon($objData->tmt_coupon);
			$this->setTMTCouponRemark($objData->tmt_coupon_remark);
			$this->setTMTOther($objData->tmt_other);	
			$this->setTMTOtherRemark($objData->tmt_other_remark);
			
			$this->setFinanceDate($objData->finance_date);
			$this->setFinanceNumber($objData->finance_number);
			$this->setComCar($objData->com_car);
			$this->setComFinance($objData->com_finance);
			$this->setComInsure($objData->com_insure);
			$this->setComOldCar($objData->com_old_car);
			$this->setComEquipment($objData->com_equipment);
			$this->setComExtra($objData->com_extra);
			$this->setComExtraRemark($objData->com_extra_remark);
			$this->setFinanceRemain($objData->finance_remain);
			$this->setFinanceCom($objData->finance_com);
			$this->setFinanceComInsure($objData->finance_com_insure);
			$this->setFinanceFaceCar($objData->finance_face_car);
			$this->setFinanceFaceCom($objData->finance_face_com);
			
			$this->setScan01($objData->scan_01);
			$this->setScan02($objData->scan_02);
			$this->setScan03($objData->scan_03);
			$this->setScan04($objData->scan_04);
			$this->setScan05($objData->scan_05);
			$this->setScan06($objData->scan_06);
			$this->setScan07($objData->scan_07);
			$this->setScan08($objData->scan_08);
			$this->setScan09($objData->scan_09);
			$this->setScan10($objData->scan_10);
			$this->setScan11($objData->scan_11);
			
			$this->setCarMarginId($objData->car_margin_id);
			
			$this->setAddBy($objData->add_by);
			$this->setAddDate($objData->add_date);
			$this->setEditBy($objData->edit_by);
			$this->setEditDate($objData->edit_date);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
			
			$this->setCheckPurchase($objData->check_purchase);
			$this->setCheckPurchaseRemark($objData->check_purchase_remark);
			$this->setCheckPurchaseBy($objData->check_purchase_by);
			
			$this->setCheckAccountBooking($objData->check_account_booking);
			$this->setCheckAccountBookingDate($objData->check_account_booking_date);
			$this->setCheckAccountBookingRemark($objData->check_account_booking_remark);
			$this->setCheckAccountBookingBy($objData->check_account_booking_by);
			
			$this->setCheckAccountSending($objData->check_account_sending);
			$this->setCheckAccountSendingDate($objData->check_account_sending_date);
			$this->setCheckAccountSendingRemark($objData->check_account_sending_remark);
			$this->setCheckAccountSendingBy($objData->check_account_sending_by);
			$this->setCheckAccountSendingCom($objData->check_account_sending_com);

			$this->setSumDiscountOver($objData->sum_discount_over);
			$this->setSumOrderId($objData->sum_order_id);
			
			$this->setCredit($objData->credit);
			$this->setCreditDate($objData->credit_date);
			$this->setCreditSendingDate($objData->credit_sending_date);
			
			$this->setTaxDate($objData->tax_date);
			$this->setTaxDueDate($objData->tax_duedate);
			$this->setTaxPrice($objData->tax_price);
			
			$this->setEstimateFinanceDate($objData->estimate_finance_date);
			$this->setStockStatus($objData->stock_status);
			$this->setStockCarStatus($objData->stock_car_status);
			$this->setStockCrlStatus($objData->stock_crl_status);
			$this->setCompanyId($objData->company_id);
			$this->setCompanyIds($objData->company_ids);
			
			$this->setFirstname($objData->firstname);
			$this->setLastname($objData->lastname);
			$this->setNickname($objData->nickname);
			$this->setBookingPlace($objData->booking_place);
			$this->setSendingPlace($objData->sending_place);
			
			$this->setChangeProduct($objData->change_product);
			$this->setChangeNumber($objData->change_number);
			$this->setChangeRemark($objData->change_remark);
			$this->setBookingCancelReasonRemark($objData->booking_cancel_reason_remark);
			$this->setFundName($objData->fund_name);
			
			$this->setBuyReason($objData->buy_reason);
			$this->setBond01($objData->bond01);
			$this->setBond01Relate($objData->bond01_relate);
			$this->setBond01RelateRemark($objData->bond01_relate_remark);
			$this->setBond01Remark($objData->bond01_remark);
			$this->setBond02($objData->bond02);
			$this->setBond02Relate($objData->bond02_relate);
			$this->setBond02RelateRemark($objData->bond02_relate_remark);
			$this->setBond02Remark($objData->bond02_remark);
			
			
        }
    }

	function init(){	
            $this->setBookingRemark(stripslashes($this->mBookingRemark));
			$this->setBookingCarRemark(stripslashes($this->mBookingCarRemark));
			$this->setSendingCarRemark(stripslashes($this->mSendingCarRemark));
            $this->setSendingRemark(stripslashes($this->mSendingRemark));
            $this->setOrderNumber(stripslashes($this->mOrderNumber));
            $this->setOrderLocation(stripslashes($this->mOrderLocation));
			$this->setBuyRemark(stripslashes($this->mBuyRemark));
			$this->setInsureRemark(stripslashes($this->mInsureRemark));
	}
	
	function load() {

		if ($this->mOrderId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." O "
					." WHERE order_id = ".$this->mOrderId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Order($row);
                $result->freeResult();
				return true;
            }
        }
	   $this->unsetConnection();
		return false;
	}

	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Order($row);
                $result->freeResult();
				return true;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	function loadMaxSendingDate($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MAX(sending_cashier_date) as sending_date FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
		//echo $strSql;
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Order($row);
                $result->freeResult();
				return true;
            }
        }

	   $this->unsetConnection();
		return false;
	}	
	
	function loadMinSendingDate($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MIN(sending_cashier_date) as sending_date FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
		//echo $strSql;
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->Order($row);
                $result->freeResult();
				return true;
            }
        }

	   $this->unsetConnection();
		return false;
	}		
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(order_id) AS RSUM  FROM ".$this->TABLE
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	//function load for sale report : rp01
	function loadSumByConditionSale($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.order_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_customer C ON C.customer_id = O.booking_customer_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = O.booking_car_series "
					." LEFT JOIN t_car_model CM ON CS.car_model_id = CM.car_model_id "
					." LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	    $this->unsetConnection();
		return false;
	}
	
	//function load for sale report : rp01
	function loadSumByConditionSaleSending($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.order_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
					." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
					." LEFT JOIN t_car_model CM ON CS.car_model_id = CM.car_model_id "
					." LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	    $this->unsetConnection();		
		return false;
	}
	
	//function load for sale report : rp01
	function loadSumByConditionBooking($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.order_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_customer C ON C.customer_id = O.booking_customer_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = O.booking_car_series "
					." LEFT JOIN t_car_model CM ON CS.car_model_id = CM.car_model_id "
					." LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	//function load for sale report : rp01
	function loadSumByConditionSending($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.order_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
					." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
					." LEFT JOIN t_car_model CM ON CS.car_model_id = CM.car_model_id "
					." LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
					." WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	
	//function load for sale report : rp01
	function loadSumByConditionAccount($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(O.order_id) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
					." LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
					." LEFT JOIN t_insure_company I ON I.insure_company_id = O.insure_company "
					." WHERE ".$strCondition;
		//echo $strSql."<br><br>";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	//function load for sale report : rp01
	function loadOverMarginByConditionAccount($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT sum(O.discount_over) AS RSUM  FROM ".$this->TABLE." O "
					." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
					." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
					." LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
					." WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}
	
	
	function add() {
		global $sMemberId;	
		global $sCompanyId;	
		
		$this->mBookingTeamId = $this->getSaleTeam($this->mBookingSaleId);
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);
				
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_date, order_status, order_number, order_price, order_reserve_price, order_location, "
						." booking_number, booking_number_nohead, booking_customer_id, booking_date, booking_cashier_date, booking_cashier_time, booking_cashier_id, booking_car_color, booking_car_series, booking_car_type, booking_car_gear, booking_car_price, booking_car_remark,  booking_sale_id, booking_team_id, booking_remark, "
						." sending_number, sending_number_nohead, sending_customer_id, sending_date, sending_sale_id, sending_team_id, sending_remark, sending_car_remark , "
						." switch_sale, switch_car, switch_customer, stock_car_id, stock_red_id ,  red_code_price ,  no_red_code, black_code_status ,  black_code_number, black_code_date, recieve_date , "
						." buy_type ,  buy_company ,  buy_fee ,  buy_time ,  buy_payment ,  buy_price ,  buy_down ,  buy_total , buy_remark, "
						." prb_company ,  prb_expire , prb_price ,  insure_company , insure_expire ,  insure_price ,  insure_type ,  insure_year ,  insure_budget , insure_from, insure_from_detail,  insure_remark , "
						." discount_price, discount_subdown, discount_premium, discount_product, discount_insurance, discount_goa, discount, tmb_number, add_by, add_date ,company_ids , company_id , booking_place ) "
						." VALUES ( '".date("Y-m-d")."' , "
						."  '".$this->mOrderStatus."' , "
						."  '".$this->mOrderNumber."' , "
						."  '".$this->mOrderPrice."' , "
						."  '".$this->mOrderReservePrice."' , "
						."  '".$this->mOrderLocation."' , "
						
						."  '".$this->mBookingNumber."' , "
						."  '".$this->mBookingNumberNohead."' , "
						."  '".$this->mBookingCustomerId."' , "
						."  '".$this->mBookingDate."' , "
						."  '".$this->mBookingCashierDate."' , "
						."  '".$this->mBookingCashierTime."' , "
						."  '".$this->mBookingCashierId."' , "
						."  '".$this->mBookingCarColor."' , "
						."  '".$this->mBookingCarSeries."' , "
						."  '".$this->mBookingCarType."' , "
						."  '".$this->mBookingCarGear."' , "
						."  '".$this->mBookingCarPrice."' , "
						."  '".$this->mBookingCarRemark."' , "
						."  '".$this->mBookingSaleId."' , "
						."  '".$this->mBookingTeamId."' , "
						."  '".$this->mBookingRemark."' , "
						
						."  '".$this->mSendingNumber."' , "
						."  '".$this->mSendingNumberNohead."' , "
						."  '".$this->mSendingCustomerId."' , "
						."  '".$this->mSendingDate."' , "
						."  '".$this->mSendingSaleId."' , "
						."  '".$this->mSendingTeamId."' , "
						."  '".$this->mSendingRemark."' , "
						."  '".$this->mSendingCarRemark."' , "
						
						."  '".$this->mSwitchSale."' , "
						."  '".$this->mSwitchCar."' , "
						."  '".$this->mSwitchCustomer."' , "
						."  '".$this->mStockCarId."' , "
						." '".$this->mStockRedId."' , "
						." '".$this->mRedCodePrice."' , "
						." '".$this->mNoRedCode."' , "
						." '".$this->mBlackCodeStatus."' , "
						." '".$this->mBlackCodeNumber."' , "
						." '".$this->mBlackCodeDate."' , "
						." '".$this->mRecieveDate."' , "
						
						." '".$this->mBuyType."' , "
						." '".$this->mBuyCompany."' , "
						." '".$this->mBuyFee."' , "
						." '".$this->mBuyTime."' , "
						." '".$this->mBuyPayment."' , "
						." '".$this->mBuyPrice."' , "
						." '".$this->mBuyDown."' , "
						." '".$this->mBuyTotal."' , "
						." '".$this->mBuyRemark."' , "
						
						." '".$this->mPrbCompany."' , "
						." '".$this->mPrbExpire."' , "
						." '".$this->mPrbPrice."' , "
						." '".$this->mInsureCompany."' , "
						." '".$this->mInsureExpire."' , "
						." '".$this->mInsurePrice."' , "
						." '".$this->mInsureType."' , "
						." '".$this->mInsureYear."' , "
						." '".$this->mInsureBudget."' , "
						." '".$this->mInsureFrom."' , "
						." '".$this->mInsureFromDetail."' , "
						." '".$this->mInsureRemark."' , "
						
						." '".$this->mDiscountPrice."' , "
						." '".$this->mDiscountSubdown."' , "
						." '".$this->mDiscountPremium."' , "
						." '".$this->mDiscountProduct."' , "
						." '".$this->mDiscountInsurance."' , "
						." '".$this->mDiscountGoa."' , "
						." '".$this->mDiscount."' , "
						." '".$this->mTMBNumber."' , "
						
						." '".$this->mAddBy."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$sCompanyId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mBookingPlace."' ) ";

        $this->getConnection();

		//echo $strSql;
		
        $result = $this->query($strSql);
        $this->mOrderId = mysql_insert_id();
		$resultId = $this->mOrderId;
		//echo "hId=".$resultId;
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"add");			
        return $resultId;
	}
	
	function addAccountDealer() {
		global $sMemberId;	
		global $sCompanyId;	
		
		$this->mBookingTeamId = $this->getSaleTeam($this->mBookingSaleId);
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);
				
		$strSql = "INSERT INTO ".$this->TABLE
						." ( order_date, order_status, order_number, order_price, order_reserve_price, order_location, "
						." booking_number, booking_number_nohead, booking_customer_id, booking_date, booking_cashier_date, booking_cashier_time,booking_car_color, booking_car_series, booking_car_type, booking_car_gear, booking_car_price, booking_car_remark,  booking_sale_id, booking_team_id, booking_remark, "
						." sending_cashier_date, sending_cashier_time, sending_number, sending_number_nohead, sending_customer_id, sending_date, sending_sale_id, sending_team_id, sending_remark, sending_car_remark , "
						." switch_sale, switch_car, switch_customer, stock_car_id, stock_red_id ,  red_code_price ,  no_red_code, black_code_status ,  black_code_number, black_code_date, recieve_date , "
						." buy_type ,  buy_company ,  buy_fee ,  buy_time ,  buy_payment ,  buy_price ,  buy_down ,  buy_total , buy_remark, "
						." prb_company ,  prb_expire , prb_price ,  insure_company , insure_expire ,  insure_price ,  insure_type ,  insure_year ,  insure_budget , insure_from, insure_from_detail,  insure_remark , "
						." discount_price, discount_subdown, discount_premium, discount_product, discount_insurance, discount_goa, discount, tmb_number, add_by, add_date ,company_ids , company_id , dealer, booking_place ) "
						." VALUES ( '".date("Y-m-d")."' , "
						."  '".$this->mOrderStatus."' , "
						."  '".$this->mOrderNumber."' , "
						."  '".$this->mOrderPrice."' , "
						."  '".$this->mOrderReservePrice."' , "
						."  '".$this->mOrderLocation."' , "
						
						."  '".$this->mBookingNumber."' , "
						."  '".$this->mBookingNumberNohead."' , "
						."  '".$this->mBookingCustomerId."' , "
						."  '".$this->mBookingDate."' , "
						."  '".$this->mBookingCashierDate."' , "
						."  '".$this->mBookingCashierTime."' , "
						."  '".$this->mBookingCarColor."' , "
						."  '".$this->mBookingCarSeries."' , "
						."  '".$this->mBookingCarType."' , "
						."  '".$this->mBookingCarGear."' , "
						."  '".$this->mBookingCarPrice."' , "
						."  '".$this->mBookingCarRemark."' , "
						."  '".$this->mBookingSaleId."' , "
						."  '".$this->mBookingTeamId."' , "
						."  '".$this->mBookingRemark."' , "
						
						."  '".$this->mSendingCashierDate."' , "
						."  '".$this->mSendingCashierTime."' , "
						."  '".$this->mSendingNumber."' , "
						."  '".$this->mSendingNumberNohead."' , "
						."  '".$this->mSendingCustomerId."' , "
						."  '".$this->mSendingDate."' , "
						."  '".$this->mSendingSaleId."' , "
						."  '".$this->mSendingTeamId."' , "
						."  '".$this->mSendingRemark."' , "
						."  '".$this->mSendingCarRemark."' , "
						
						."  '".$this->mSwitchSale."' , "
						."  '".$this->mSwitchCar."' , "
						."  '".$this->mSwitchCustomer."' , "
						."  '".$this->mStockCarId."' , "
						." '".$this->mStockRedId."' , "
						." '".$this->mRedCodePrice."' , "
						." '".$this->mNoRedCode."' , "
						." '".$this->mBlackCodeStatus."' , "
						." '".$this->mBlackCodeNumber."' , "
						." '".$this->mBlackCodeDate."' , "
						." '".$this->mRecieveDate."' , "
						
						." '".$this->mBuyType."' , "
						." '".$this->mBuyCompany."' , "
						." '".$this->mBuyFee."' , "
						." '".$this->mBuyTime."' , "
						." '".$this->mBuyPayment."' , "
						." '".$this->mBuyPrice."' , "
						." '".$this->mBuyDown."' , "
						." '".$this->mBuyTotal."' , "
						." '".$this->mBuyRemark."' , "
						
						." '".$this->mPrbCompany."' , "
						." '".$this->mPrbExpire."' , "
						." '".$this->mPrbPrice."' , "
						." '".$this->mInsureCompany."' , "
						." '".$this->mInsureExpire."' , "
						." '".$this->mInsurePrice."' , "
						." '".$this->mInsureType."' , "
						." '".$this->mInsureYear."' , "
						." '".$this->mInsureBudget."' , "
						." '".$this->mInsureFrom."' , "
						." '".$this->mInsureFromDetail."' , "
						." '".$this->mInsureRemark."' , "
						
						." '".$this->mDiscountPrice."' , "
						." '".$this->mDiscountSubdown."' , "
						." '".$this->mDiscountPremium."' , "
						." '".$this->mDiscountProduct."' , "
						." '".$this->mDiscountInsurance."' , "
						." '".$this->mDiscountGoa."' , "
						." '".$this->mDiscount."' , "
						." '".$this->mTMBNumber."' , "
						
						." '".$this->mAddBy."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$sCompanyId."' , "
						."  '".$sCompanyId."' , "
						."  '1' , "
						."  '".$this->mBookingPlace."' ) ";

        $this->getConnection();		
        If ($result = $this->query($strSql)) { 
            $this->mOrderId = mysql_insert_id();
			//echo $strSql;
            return $this->mOrderId;
        } else {
			return false;
	    }
		//exit;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"addAccountDealer");			
        return $resultId;
	}
	
	
	
	function update(){
		global $sMemberId;	
		$this->mBookingTeamId = $this->getSaleTeam($this->mBookingSaleId);
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "	
						." ,  order_price = '".$this->mOrderPrice."'  "	
						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "	
						." ,  order_location= '".$this->mOrderLocation."'  "

						." ,  booking_number = '".$this->mBookingNumber."'  "	
						." ,  booking_number_nohead = '".$this->mBookingNumberNohead."'  "	
						." ,  booking_customer_id = '".$this->mBookingCustomerId."'  "	
						." ,  booking_date = '".$this->mBookingDate."'  "
						." ,  booking_car_color = '".$this->mBookingCarColor."'  "
						." ,  booking_car_series = '".$this->mBookingCarSeries."'  "
						." ,  booking_car_type = '".$this->mBookingCarType."'  "
						." ,  booking_car_gear = '".$this->mBookingCarGear."'  "
						." ,  booking_car_price = '".$this->mBookingCarPrice."'  "
						." ,  booking_car_remark = '".$this->mBookingCarRemark."'  "
						." ,  booking_sale_id = '".$this->mBookingSaleId."'  "
						." ,  booking_team_id = '".$this->mBookingTeamId."'  "
						." ,  booking_remark = '".$this->mBookingRemark."'  "
						
						." ,  sending_number = '".$this->mSendingNumber."'  "	
						." ,  sending_number_nohead = '".$this->mSendingNumberNohead."'  "	
						." ,  sending_customer_id = '".$this->mSendingCustomerId."'  "	
						." ,  sending_date = '".$this->mSendingDate."'  "	
						." ,  sending_sale_id = '".$this->mSendingSaleId."'  "	
						." ,  sending_team_id = '".$this->mSendingTeamId."'  "	
						." ,  sending_remark = '".$this->mSendingRemark."'  "	
						." ,  sending_car_remark = '".$this->mSendingCarRemark."'  "	
						." ,  switch_sale = '".$this->mSwitchSale."'  "
						." ,  switch_car = '".$this->mSwitchCar."'  "
						." ,  switch_customer = '".$this->mSwitchCustomer."'  "

						." ,  stock_car_id = '".$this->mStockCarId."'  "	
						." , stock_red_id = '".$this->mStockRedId."'  "
						." , red_code_price = '".$this->mRedCodePrice."'  "
						." , black_code_status = '".$this->mBlackCodeStatus."'  "
						." , black_code_number = '".$this->mBlackCodeNumber."'  "
						." , black_code_date = '".$this->mBlackCodeDate."'  "
						." , recieve_date = '".$this->mRecieveDate."'  "
						
						." , buy_type = '".$this->mBuyType."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_fee = '".$this->mBuyFee."'  "
						." , buy_time = '".$this->mBuyTime."'  "
						." , buy_payment = '".$this->mBuyPayment."'  "
						." , buy_price = '".$this->mBuyPrice."'  "
						." , buy_down = '".$this->mBuyDown."'  "
						." , buy_total = '".$this->mBuyTotal."'  "
						." , buy_remark = '".$this->mBuyRemark."'  "
						." , prb_company = '".$this->mPrbCompany."'  "
						." , prb_expire = '".$this->mPrbExpire."'  "
						." , prb_price = '".$this->mPrbPrice."'  "
						." , insure_company = '".$this->mInsureCompany."'  "
						." , insure_expire = '".$this->mInsureExpire."'  "
						." , insure_price = '".$this->mInsurePrice."'  "
						." , insure_type = '".$this->mInsureType."'  "
						." , insure_year = '".$this->mInsureYear."'  "
						." , insure_budget = '".$this->mInsureBudget."'  "
						." , insure_from = '".$this->mInsureFrom."'  "
						." , insure_from_detail = '".$this->mInsureFromDetail."'  "
						." , insure_remark = '".$this->mInsureRemark."'  "
						." , insure_customer_pay = '".$this->mInsureCustomerPay."'  "
						
						." , discount_price = '".$this->mDiscountPrice."'  "
						." , discount_subdown = '".$this->mDiscountSubdown."'  "
						." , discount_premium = '".$this->mDiscountPremium."'  "
						." , discount_product = '".$this->mDiscountProduct."'  "
						." , discount_insurance = '".$this->mDiscountInsurance."'  "
						." , discount_goa = '".$this->mDiscountGoa."'  "
						." , discount = '".$this->mDiscount."'  "
						." , discount_subdown_vat = '".$this->mDiscountSubdownVat."'  "
						." , discount_margin = '".$this->mDiscountMargin."'  "
						." , discount_other = '".$this->mDiscountOther."'  "
						." , discount_other_price = '".$this->mDiscountOtherPrice."'  "
						." , discount_all = '".$this->mDiscountAll."'  "
						." , discount_over = '".$this->mDiscountOver."'  "
						
						." , tmb_number = '".$this->mTMBNumber."'  "
						
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update");
	   

        $result = $this->query($strSql);
        $this->unsetConnection();
		return $result;
	}
	
	function updateCrlBooking(){
		global $sMemberId;
		$this->mBookingTeamId = $this->getSaleTeam($this->mBookingSaleId);
		
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "
						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "	
						." ,  order_location= '".$this->mOrderLocation."'  "

						." ,  booking_customer_id = '".$this->mBookingCustomerId."'  "	
						." ,  booking_date = '".$this->mBookingDate."'  "
						." ,  booking_car_color = '".$this->mBookingCarColor."'  "
						." ,  booking_car_series = '".$this->mBookingCarSeries."'  "
						." ,  booking_car_type = '".$this->mBookingCarType."'  "
						." ,  booking_car_gear = '".$this->mBookingCarGear."'  "
						." ,  booking_car_price = '".$this->mBookingCarPrice."'  "
						." ,  booking_car_remark = '".$this->mBookingCarRemark."'  "
						." ,  booking_sale_id = '".$this->mBookingSaleId."'  "
						." ,  booking_team_id = '".$this->mBookingTeamId."'  "
						." ,  booking_remark = '".$this->mBookingRemark."'  "
						
						." ,  buy_reason = '".$this->mBuyReason."'  "
						." ,  bond01 = '".$this->mBond01."'  "
						." ,  bond01_relate = '".$this->mBond01Relate."'  "
						." ,  bond01_relate_remark = '".$this->mBond01RelateRemark."'  "
						." ,  bond01_remark = '".$this->mBond01Remark."'  "
						." ,  bond02 = '".$this->mBond02."'  "
						." ,  bond02_relate = '".$this->mBond02Relate."'  "
						." ,  bond02_relate_remark = '".$this->mBond02RelateRemark."'  "
						." ,  bond02_remark = '".$this->mBond02Remark."'  "
						

						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_crl_booking");		

        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateCashierBooking(){
		global $sMemberId;
		$this->mBookingTeamId = $this->getSaleTeam($this->mBookingSaleId);
		
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "
						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "	
						." ,  order_location= '".$this->mOrderLocation."'  "

						." ,  booking_number = '".$this->mBookingNumber."'  "	
						." ,  booking_number_nohead = '".$this->mBookingNumberNohead."'  "	
						." ,  booking_customer_id = '".$this->mBookingCustomerId."'  "	
						." ,  booking_date = '".$this->mBookingDate."'  "
						." ,  booking_cashier_date = '".$this->mBookingCashierDate."'  "
						." ,  booking_cashier_time = '".$this->mBookingCashierTime."'  "
						." ,  booking_cashier_id = '".$this->mBookingCashierId."'  "
						." ,  booking_car_color = '".$this->mBookingCarColor."'  "
						." ,  booking_car_series = '".$this->mBookingCarSeries."'  "
						." ,  booking_car_type = '".$this->mBookingCarType."'  "
						." ,  booking_car_gear = '".$this->mBookingCarGear."'  "
						." ,  booking_car_price = '".$this->mBookingCarPrice."'  "
						." ,  booking_car_remark = '".$this->mBookingCarRemark."'  "
						." ,  booking_sale_id = '".$this->mBookingSaleId."'  "
						." ,  booking_team_id = '".$this->mBookingTeamId."'  "
						." ,  booking_remark = '".$this->mBookingRemark."'  "
						
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_cashier_booking");

        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateCashierSending(){
		global $sMemberId;

		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." , sending_number = '".$this->mSendingNumber."'  "	
						." , sending_number_nohead = '".$this->mSendingNumberNohead."'  "
						." , recieve_date = '".$this->mRecieveDate."'  "	
						." , sending_cashier_date = '".$this->mSendingCashierDate."'  "	
						." , sending_cashier_time = '".$this->mSendingCashierTime."'  "	
						." , sending_cashier_id = '".$this->mSendingCashierId."'  "	
						." , stock_red_id = '".$this->mStockRedId."'  "
						
						." , no_red_code = '".$this->mNoRedCode."'  "
						." , order_price = '".$this->mOrderPrice."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "
						." ,  order_location= '".$this->mOrderLocation."'  "
						." ,  sending_customer_id = '".$this->mSendingCustomerId."'  "
						." ,  sending_sale_id = '".$this->mSendingSaleId."'  "	
						." ,  sending_team_id = '".$this->mSendingTeamId."'  "	
						." ,  sending_remark = '".$this->mSendingRemark."'  "	
						." ,  switch_sale = '".$this->mSwitchSale."'  "
						." ,  switch_customer = '".$this->mSwitchCustomer."'  "
						." , credit = '".$this->mCredit."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_type = '".$this->mBuyType."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;	
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_cashier_sending");	
        
		$result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function updateAccountDealer(){
		global $sMemberId;	
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);
	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." , sending_number = '".$this->mSendingNumber."'  "	
						." , sending_number_nohead = '".$this->mSendingNumberNohead."'  "
						." , recieve_date = '".$this->mRecieveDate."'  "	
						." , sending_cashier_date = '".$this->mSendingCashierDate."'  "	
						." , sending_cashier_time = '".$this->mSendingCashierTime."'  "	
						." , stock_red_id = '".$this->mStockRedId."'  "
						." , red_code_price = '".$this->mRedCodePrice."'  "
						." , no_red_code = '".$this->mNoRedCode."'  "
						." , order_price = '".$this->mOrderPrice."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "
						." ,  order_location= '".$this->mOrderLocation."'  "
						." ,  sending_customer_id = '".$this->mSendingCustomerId."'  "
						." ,  sending_sale_id = '".$this->mSendingSaleId."'  "	
						." ,  sending_team_id = '".$this->mSendingTeamId."'  "	
						." ,  sending_remark = '".$this->mSendingRemark."'  "	
						." ,  switch_sale = '".$this->mSwitchSale."'  "
						." ,  switch_customer = '".$this->mSwitchCustomer."'  "
						." , credit = '".$this->mCredit."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_type = '".$this->mBuyType."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;	
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_account_dealer");	
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		


function updateCrlSending(){
		global $sMemberId;
		global $sCompanyId;
		
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);

		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  order_number = '".$this->mOrderNumber."'  "	
						." ,  recieve_number = '".$this->mRecieveNumber."'  "
						." ,  order_price = '".$this->mOrderPrice."'  "	
						." ,  order_location= '".$this->mOrderLocation."'  "

						." ,  sending_number = '".$this->mSendingNumber."'  "	
						." ,  sending_number_nohead = '".$this->mSendingNumberNohead."'  "	
						." ,  sending_customer_id = '".$this->mSendingCustomerId."'  "	
						." ,  sending_date = '".$this->mSendingDate."'  "	
						." ,  sending_sale_id = '".$this->mSendingSaleId."'  "	
						." ,  sending_team_id = '".$this->mSendingTeamId."'  "
						." ,  sending_remark = '".$this->mSendingRemark."'  "	
						." ,  sending_car_remark = '".$this->mSendingCarRemark."'  "
						." ,  switch_sale = '".$this->mSwitchSale."'  "
						." ,  switch_car = '".$this->mSwitchCar."'  "
						." ,  switch_customer = '".$this->mSwitchCustomer."'  "

						." ,  stock_car_id = '".$this->mStockCarId."'  "	
						." , stock_red_id = '".$this->mStockRedId."'  "
						." , red_code_price = '".$this->mRedCodePrice."'  "
						." , no_red_code = '".$this->mNoRedCode."'  "


						." , buy_type = '".$this->mBuyType."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_fee = '".$this->mBuyFee."'  "
						." , buy_time = '".$this->mBuyTime."'  "
						." , buy_payment = '".$this->mBuyPayment."'  "
						." , buy_price = '".$this->mBuyPrice."'  "
						." , buy_down = '".$this->mBuyDown."'  "
						." , buy_total = '".$this->mBuyTotal."'  "
						." , buy_remark = '".$this->mBuyRemark."'  "
						." , buy_begin = '".$this->mBuyBegin."'  "
						." , buy_product = '".$this->mBuyProduct."'  "
						
						." ,  registry_person = '".$this->mRegistryPerson."'  "	
						." ,  registry_price = '".$this->mRegistryPrice."'  "	
						
						." , prb_company = '".$this->mPrbCompany."'  "
						." , prb_price = '".$this->mPrbPrice."'  "
						
						." , insure_company = '".$this->mInsureCompany."'  "
						." , insure_expire = '".$this->mInsureExpire."'  "
						." , insure_price = '".$this->mInsurePrice."'  "
						." , insure_type = '".$this->mInsureType."'  "
						." , insure_year = '".$this->mInsureYear."'  "
						." , insure_budget = '".$this->mInsureBudget."'  "
						." , insure_from = '".$this->mInsureFrom."'  "
						." , insure_from_detail = '".$this->mInsureFromDetail."'  "
						." , insure_remark = '".$this->mInsureRemark."'  "
						." , insure_customer_pay = '".$this->mInsureCustomerPay."'  "
						
						." , tmb_number = '".$this->mTMBNumber."'  "

						." , discount_price = '".$this->mDiscountPrice."'  "
						." , discount_subdown = '".$this->mDiscountSubdown."'  "
						." , discount_premium = '".$this->mDiscountPremium."'  "
						." , discount_product = '".$this->mDiscountProduct."'  "
						." , discount_insurance = '".$this->mDiscountInsurance."'  "
						." , discount_goa = '".$this->mDiscountGoa."'  "
						." , discount = '".$this->mDiscount."'  "
						." , discount_subdown_vat = '".$this->mDiscountSubdownVat."'  "
						." , discount_margin = '".$this->mDiscountMargin."'  "
						." , discount_margin_date = '".$this->mDiscountMarginDate."'  "
						." , discount_all = '".$this->mDiscountAll."'  "
						." , discount_over = '".$this->mDiscountOver."'  "
						." , discount_other = '".$this->mDiscountOther."'  "
						." , discount_other_price = '".$this->mDiscountOtherPrice."'  "
						." , discount_customer_pay = '".$this->mDiscountCustomerPay."'  "
						." , discount_customer_pay_reason = '".$this->mDiscountCustomerPayReason."'  "
						." , discount_tmt = '".$this->mDiscountTMT."'  "

						." ,  tmt_free = '".$this->mTMTFree."'  "
						." ,  tmt_free_remark = '".$this->mTMTFreeRemark."'  "
						." ,  tmt_coupon = '".$this->mTMTCoupon."'  "
						." ,  tmt_coupon_remark = '".$this->mTMTCouponRemark."'  "
						." ,  tmt_other = '".$this->mTMTOther."'  "
						." ,  tmt_other_remark = '".$this->mTMTOtherRemark."'  "

						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "
						." ,  order_reserve_price01 = '".$this->mOrderReservePrice01."'  "
						." ,  order_reserve_price02 = '".$this->mOrderReservePrice02."'  "
						." ,  recieve_date = '".$this->mRecieveDate."'  "
						." ,  company_id = '".$sCompanyId."'  "

						." ,  change_product = '".$this->mChangeProduct."'  "
						." ,  change_number = '".$this->mChangeNumber."'  "
						." ,  change_remark = '".$this->mChangeRemark."'  "

						." ,  order_old_car = '".$this->mOrderOldCar."'  "
						." ,  order_old_car_number = '".$this->mOrderOldCarNumber."'  "

						." ,  buy_reason = '".$this->mBuyReason."'  "
						." ,  bond01 = '".$this->mBond01."'  "
						." ,  bond01_relate = '".$this->mBond01Relate."'  "
						." ,  bond01_relate_remark = '".$this->mBond01RelateRemark."'  "
						." ,  bond01_remark = '".$this->mBond01Remark."'  "
						." ,  bond02 = '".$this->mBond02."'  "
						." ,  bond02_relate = '".$this->mBond02Relate."'  "
						." ,  bond02_relate_remark = '".$this->mBond02RelateRemark."'  "
						." ,  bond02_remark = '".$this->mBond02Remark."'  "	
						
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_crl_sending");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	
	
	
	
function updateCrlSending01(){
		global $sMemberId;
		global $sCompanyId;
		
		$this->mSendingTeamId = $this->getSaleTeam($this->mSendingSaleId);

		$strSql = "UPDATE ".$this->TABLE
						." SET   bond01 = '".$this->mBond01."'  "
						." ,  bond01_relate = '".$this->mBond01Relate."'  "
						." ,  bond01_relate_remark = '".$this->mBond01RelateRemark."'  "
						." ,  bond01_remark = '".$this->mBond01Remark."'  "
						." ,  bond02 = '".$this->mBond02."'  "
						." ,  bond02_relate = '".$this->mBond02Relate."'  "
						." ,  bond02_relate_remark = '".$this->mBond02RelateRemark."'  "
						." ,  bond02_remark = '".$this->mBond02Remark."'  "	
						
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_crl_bond");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	
	
	
	
	
	
	
	
	
	
	
	
function updateScan($hType){
		global $sMemberId;

		$strSql = "UPDATE ".$this->TABLE
						." SET   scan_01 = '".$this->mScan01."'  "
						." ,  scan_02 = '".$this->mScan02."'  "
						." ,  scan_03 = '".$this->mScan03."'  "
						." ,  scan_04 = '".$this->mScan04."'  "
						." ,  scan_05 = '".$this->mScan05."'  "
						." ,  scan_06 = '".$this->mScan06."'  "

						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,$hType);
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	
function updateScanRegistry($hType){
		global $sMemberId;

		$strSql = "UPDATE ".$this->TABLE
						." SET   scan_07 = '".$this->mScan07."'  "
						." ,   scan_08= '".$this->mScan08."'  "
						." ,   scan_09= '".$this->mScan09."'  "
						." ,   scan_10= '".$this->mScan10."'  "
						." ,   scan_11= '".$this->mScan11."'  "	
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,$hType);
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				
	
	
	function updateCrlBookingMail(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET  booking_mail = '".$this->mBookingMail."'  "
						." ,  booking_mail_date = '".date("Y-m-d")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_crl_booking_mail");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function updateCrlSendingMail(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET  sending_mail = '".$this->mSendingMail."'  "
						." ,  sending_mail_date = '".date("Y-m-d")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_crl_sending_mail");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function updateCashierBookingCancel(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  booking_cancel_date = '".date("Y-m-d")."'  "
						." ,  booking_cancel_number = '".$this->mBookingCancelNumber."'  "
						." ,  booking_cancel_reason = '".$this->mBookingCancelReason."'  "
						." ,  booking_cancel_approve = '".$this->mBookingCancelApprove."'  "
						." ,  booking_cancel_price = '".$this->mBookingCancelPrice."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_cashier_booking_cancel");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function updateCashierSendingCancel(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET  order_status = '".$this->mOrderStatus."'  "
						." ,  sending_cancel_date = '".date("Y-m-d")."'  "
						." ,  sending_cancel_number = '".$this->mSendingCancelNumber."'  "
						." ,  sending_cancel_reason = '".$this->mSendingCancelReason."'  "
						." ,  sending_cancel_approve = '".$this->mSendingCancelApprove."'  "
						." ,  sending_cancel_price = '".$this->mSendingCancelPrice."'  "
						." , edit_by  = '".$this->mEditBy."'  "
						." , edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_cashier_sending_cancel");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateRegistry(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET  black_code_status = '".$this->mBlackCodeStatus."'  "
						." ,  black_code_number = '".$this->mBlackCodeNumber."'  "	
						." ,  black_code_date = '".$this->mBlackCodeDate."'  "	
						." ,  registry_status = '".$this->mRegistryStatus."'  "	
						." ,  registry_condition_id = '".$this->mRegistryConditionId."'  "	
						." ,  registry_price = '".$this->mRegistryPrice."'  "	
						." ,  registry_tax_year= '".$this->mRegistryTaxYear."'  "
						." ,  registry_tax_year_number = '".$this->mRegistryTaxYearNumber."'  "	
						." ,  registry_send_date = '".$this->mRegistrySendDate."'  "	
						." ,  registry_miss_document = '".$this->mRegistryMissDocument."'  "	
						." ,  registry_person = '".$this->mRegistryPerson."'  "	
						." ,  registry_take_code = '".$this->mRegistryTakeCode."'  "
						." ,  registry_take_code_date = '".$this->mRegistryTakeCodeDate."'  "
						." ,  registry_take_book = '".$this->mRegistryTakeBook."'  "
						." ,  registry_take_book_date = '".$this->mRegistryTakeBookDate."'  "
						." ,  registry_remark = '".$this->mRegistryRemark."'  "
						." ,  registry_date_01 = '".$this->mRegistryDate01."'  "
						." ,  registry_date_02 = '".$this->mRegistryDate02."'  "
						." ,  registry_date_05 = '".$this->mRegistryDate05."'  "
						." ,  registry_date_07 = '".$this->mRegistryDate07."'  "
						." ,  registry_text_03 = '".$this->mRegistryText03."'  "
						." ,  registry_text_04 = '".$this->mRegistryText04."'  "
						." ,  registry_text_05 = '".$this->mRegistryText05."'  "
						." ,  registry_text_06 = '".$this->mRegistryText06."'  "
						." ,  registry_text_07 = '".$this->mRegistryText07."'  "
						." ,  registry_remark_01 = '".$this->mRegistryRemark01."'  "
						." ,  registry_remark_02 = '".$this->mRegistryRemark02."'  "
						." ,  registry_remark_03 = '".$this->mRegistryRemark03."'  "
						." ,  registry_remark_04 = '".$this->mRegistryRemark04."'  "
						." ,  registry_remark_05 = '".$this->mRegistryRemark05."'  "
						." ,  registry_remark_06 = '".$this->mRegistryRemark06."'  "
						." ,  registry_remark_07 = '".$this->mRegistryRemark07."'  "
						." ,  registry_remark_08 = '".$this->mRegistryRemark08."'  "
						." ,  registry_recieve_type_01 = '".$this->mRegistryRecieveType01."'  "
						." ,  registry_recieve_type_02 = '".$this->mRegistryRecieveType02."'  "
						." ,  registry_recieve_name_01 = '".$this->mRegistryRecieveName01."'  "
						." ,  registry_recieve_name_02 = '".$this->mRegistryRecieveName02."'  "
						." ,  registry_sign = '".$this->mRegistrySign."'  "
						." ,  registry_sign_date = '".$this->mRegistrySignDate."'  "
						." ,  registry_edit_by  = '".$this->mEditBy."'  "
						." ,  order_status  = '".$this->mOrderStatus."'  "
						." ,  registry_edit_date  = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		//exit;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_registry");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function updateRegistry01(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET registry = '".$this->mRegistry."'  "
						." ,  registry_date = '".$this->mRegistryDate."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_registry_status");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				
		
	function updateRegistryStatus(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET registry_status = '".$this->mRegistryStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}					
	
	function updateRegistryBlackCode(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET registry_date_05 = '".$this->mRegistryDate05."'  "
						." ,  registry_text_05 = '".$this->mRegistryText05."'  "
						." ,  registry_recieve_name_01 = '".$this->mRegistryRecieveName01."'  "
						." ,  registry_recieve_type_01 = '".$this->mRegistryRecieveType01."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				

	function updateAccountBooking(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET check_account_booking = '".$this->mCheckAccountBooking."'  "
						." ,  check_account_booking_date = '".date("Y-m-d")."'  "
						." ,  check_account_booking_remark = '".$this->mCheckAccountBookingRemark."'  "
						." ,  check_account_booking_by = '".$this->mCheckAccountBookingBy."'  "
						
						." ,  order_price = '".$this->mOrderPrice."'  "
						
						." , buy_type = '".$this->mBuyType."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_fee = '".$this->mBuyFee."'  "
						." , buy_time = '".$this->mBuyTime."'  "
						." , buy_payment = '".$this->mBuyPayment."'  "
						." , buy_price = '".$this->mBuyPrice."'  "
						." , buy_down = '".$this->mBuyDown."'  "
						." , buy_total = '".$this->mBuyTotal."'  "
						." , buy_remark = '".$this->mBuyRemark."'  "
						." , buy_begin = '".$this->mBuyBegin."'  "
						." , buy_product = '".$this->mBuyProduct."'  "
						." , buy_engine = '".$this->mBuyEngine."'  "
						." , buy_engine_remark = '".$this->mBuyEngineRemark."'  "
						
						." ,  registry_person = '".$this->mRegistryPerson."'  "	
						." ,  registry_price = '".$this->mRegistryPrice."'  "	
						." ,  registry_price1 = '".$this->mRegistryPrice1."'  "	
						
						." , prb_company = '".$this->mPrbCompany."'  "
						." , prb_price = '".$this->mPrbPrice."'  "
						
						." , insure_company = '".$this->mInsureCompany."'  "
						." , insure_expire = '".$this->mInsureExpire."'  "
						." , insure_price = '".$this->mInsurePrice."'  "
						." , insure_type = '".$this->mInsureType."'  "
						." , insure_year = '".$this->mInsureYear."'  "
						." , insure_budget = '".$this->mInsureBudget."'  "
						." , insure_from = '".$this->mInsureFrom."'  "
						." , insure_from_detail = '".$this->mInsureFromDetail."'  "
						." , insure_remark = '".$this->mInsureRemark."'  "
						." , insure_customer_pay = '".$this->mInsureCustomerPay."'  "

						." , discount_price = '".$this->mDiscountPrice."'  "
						." , discount_subdown = '".$this->mDiscountSubdown."'  "
						." , discount_premium = '".$this->mDiscountPremium."'  "
						." , discount_product = '".$this->mDiscountProduct."'  "
						." , discount_insurance = '".$this->mDiscountInsurance."'  "
						." , discount_goa = '".$this->mDiscountGoa."'  "
						." , discount = '".$this->mDiscount."'  "
						." , discount_subdown_vat = '".$this->mDiscountSubdownVat."'  "
						." , discount_margin = '".$this->mDiscountMargin."'  "
						." , discount_margin_date = '".$this->mDiscountMarginDate."'  "
						." , discount_all = '".$this->mDiscountAll."'  "
						." , discount_over = '".$this->mDiscountOver."'  "
						." , discount_other = '".$this->mDiscountOther."'  "
						." , discount_other_price = '".$this->mDiscountOtherPrice."'  "
						." , discount_customer_pay = '".$this->mDiscountCustomerPay."'  "
						." , discount_customer_pay_reason = '".$this->mDiscountCustomerPayReason."'  "
						." , discount_tmt = '".$this->mDiscountTMT."'  "
						
						." ,  tmt_free = '".$this->mTMTFree."'  "
						." ,  tmt_free_remark = '".$this->mTMTFreeRemark."'  "
						." ,  tmt_coupon = '".$this->mTMTCoupon."'  "
						." ,  tmt_coupon_remark = '".$this->mTMTCouponRemark."'  "
						." ,  tmt_other = '".$this->mTMTOther."'  "
						." ,  tmt_other_remark = '".$this->mTMTOtherRemark."'  "
						
						." ,  booking_car_price = '".$this->mBookingCarPrice."'  "
						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "
						." ,  order_reserve_price01 = '".$this->mOrderReservePrice01."'  "
						." ,  order_reserve_price02 = '".$this->mOrderReservePrice02."'  "
						
						." ,  order_old_car = '".$this->mOrderOldCar."'  "
						." ,  order_old_car_number = '".$this->mOrderOldCarNumber."'  "
						
						." , stock_red_id = '".$this->mStockRedId."'  "
						." , red_code_price = '".$this->mRedCodePrice."'  "
						." , no_red_code = '".$this->mNoRedCode."'  "
						
						." ,  order_status = '".$this->mOrderStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_account_booking");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				

	function updateAccountSending(){
		global $sMemberId;	
	
		$strSql = "UPDATE ".$this->TABLE
						." SET check_account_sending = '".$this->mCheckAccountSending."'  "
						." ,  check_account_sending_date = '".date("Y-m-d")."'  "
						." ,  check_account_sending_remark = '".$this->mCheckAccountSendingRemark."'  "
						." ,  check_account_sending_by = '".$this->mCheckAccountSendingBy."'  "
						." ,  check_account_sending_com = '".$this->mCheckAccountSendingCom."'  "
						
						." , buy_type = '".$this->mBuyType."'  "
						." , buy_company = '".$this->mBuyCompany."'  "
						." , buy_fee = '".$this->mBuyFee."'  "
						." , buy_time = '".$this->mBuyTime."'  "
						." , buy_payment = '".$this->mBuyPayment."'  "
						." , buy_price = '".$this->mBuyPrice."'  "
						." , buy_down = '".$this->mBuyDown."'  "
						." , buy_total = '".$this->mBuyTotal."'  "
						." , buy_remark = '".$this->mBuyRemark."'  "
						." , buy_begin = '".$this->mBuyBegin."'  "
						." , buy_product = '".$this->mBuyProduct."'  "
						." , buy_engine = '".$this->mBuyEngine."'  "
						." , buy_engine_remark = '".$this->mBuyEngineRemark."'  "
						
						." ,  registry_person = '".$this->mRegistryPerson."'  "	
						." ,  registry_price = '".$this->mRegistryPrice."'  "	
						." ,  registry_price1 = '".$this->mRegistryPrice1."'  "	
						
						." , prb_company = '".$this->mPrbCompany."'  "
						." , prb_price = '".$this->mPrbPrice."'  "
						
						." , insure_company = '".$this->mInsureCompany."'  "
						." , insure_expire = '".$this->mInsureExpire."'  "
						." , insure_price = '".$this->mInsurePrice."'  "
						." , insure_type = '".$this->mInsureType."'  "
						." , insure_year = '".$this->mInsureYear."'  "
						." , insure_budget = '".$this->mInsureBudget."'  "
						." , insure_from = '".$this->mInsureFrom."'  "
						." , insure_from_detail = '".$this->mInsureFromDetail."'  "
						." , insure_remark = '".$this->mInsureRemark."'  "
						." , insure_customer_pay = '".$this->mInsureCustomerPay."'  "

						." , discount_price = '".$this->mDiscountPrice."'  "
						." , discount_subdown = '".$this->mDiscountSubdown."'  "
						." , discount_premium = '".$this->mDiscountPremium."'  "
						." , discount_product = '".$this->mDiscountProduct."'  "
						." , discount_insurance = '".$this->mDiscountInsurance."'  "
						." , discount_goa = '".$this->mDiscountGoa."'  "
						." , discount = '".$this->mDiscount."'  "
						." , discount_subdown_vat = '".$this->mDiscountSubdownVat."'  "
						." , discount_subdown_vat_percent = '".$this->mDiscountSubdownVatPercent."'  "
						." , discount_margin = '".$this->mDiscountMargin."'  "
						." , discount_margin_date = '".$this->mDiscountMarginDate."'  "
						." , discount_all = '".$this->mDiscountAll."'  "
						." , discount_over = '".$this->mDiscountOver."'  "
						." , discount_other = '".$this->mDiscountOther."'  "
						." , discount_other_price = '".$this->mDiscountOtherPrice."'  "
						." , discount_customer_pay = '".$this->mDiscountCustomerPay."'  "
						." , discount_customer_pay_reason = '".$this->mDiscountCustomerPayReason."'  "
						." , discount_tmt = '".$this->mDiscountTMT."'  "
						
						." ,  tmt_free = '".$this->mTMTFree."'  "
						." ,  tmt_free_remark = '".$this->mTMTFreeRemark."'  "
						." ,  tmt_coupon = '".$this->mTMTCoupon."'  "
						." ,  tmt_coupon_remark = '".$this->mTMTCouponRemark."'  "
						." ,  tmt_other = '".$this->mTMTOther."'  "
						." ,  tmt_other_remark = '".$this->mTMTOtherRemark."'  "
						
						." ,  order_price = '".$this->mOrderPrice."'  "
						." ,  order_reserve_price = '".$this->mOrderReservePrice."'  "
						." ,  order_reserve_price01 = '".$this->mOrderReservePrice01."'  "
						." ,  order_reserve_price02 = '".$this->mOrderReservePrice02."'  "
						
						." ,  com_car = '".$this->mComCar."'  "
						." ,  com_insure = '".$this->mComInsure."'  "
						." ,  com_old_car = '".$this->mComOldCar."'  "
						." ,  com_equipment = '".$this->mComEquipment."'  "
						." ,  com_finance = '".$this->mComFinance."'  "
						
						." ,  order_old_car = '".$this->mOrderOldCar."'  "
						." ,  order_old_car_number = '".$this->mOrderOldCarNumber."'  "
						
						." ,  switch_car = '".$this->mSwitchCar."'  "
						." ,  stock_car_id = '".$this->mStockCarId."'  "	
						." , stock_red_id = '".$this->mStockRedId."'  "
						." , red_code_price = '".$this->mRedCodePrice."'  "
						." , no_red_code = '".$this->mNoRedCode."'  "
						
						." , car_margin_id = '".$this->mCarMarginId."'  "
						." , estimate_finance_date = '".$this->mEstimateFinanceDate."'  "
						
						." ,  order_status = '".$this->mOrderStatus."'  "
						." ,  sending_place = '".$this->mSendingPlace."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_account_sending");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}					
	
	function updateAccountFinance(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET finance_date = '".$this->mFinanceDate."'  "
						." ,  finance_number = '".$this->mFinanceNumber."'  "
						." ,  com_car = '".$this->mComCar."'  "
						." ,  com_finance = '".$this->mComFinance."'  "
						." ,  com_insure = '".$this->mComInsure."'  "
						." ,  com_old_car = '".$this->mComOldCar."'  "
						." ,  com_equipment = '".$this->mComEquipment."'  "
						." ,  com_extra = '".$this->mComExtra."'  "
						." ,  com_extra_remark = '".$this->mComExtraRemark."'  "
						." ,  finance_remain = '".$this->mFinanceRemain."'  "
						." ,  finance_com = '".$this->mFinanceCom."'  "
						." ,  finance_com_insure = '".$this->mFinanceComInsure."'  "
						." ,  finance_face_car = '".$this->mFinanceFaceCar."'  "
						." ,  finance_face_com = '".$this->mFinanceFaceCom."'  "
						." ,  check_account_sending_com = '".$this->mCheckAccountSendingCom."'  "
						." ,  tax_date = '".$this->mTaxDate."'  "
						." ,  tax_duedate = '".$this->mTaxDueDate."'  "
						." ,  tax_price = '".$this->mTaxPrice."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_account_finance");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				
	
	function updateAccountInsure(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET insure_expire = '".$this->mInsureExpire."'  "
						." ,  insure_number = '".$this->mInsureNumber."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_account_insure");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}		
	
	function updateStockStatus(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_status = '".$this->mStockStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_order_stock_status");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStockCarStatus(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_car_status = '".$this->mStockCarStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_order_stock_car_status");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}			
	
	function updateStockCrlStatus(){
		global $sMemberId;
		
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_crl_status = '".$this->mStockCrlStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_order_stock_crl_status");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateAccountFix(){
		global $sMemberId;
			
		$strSql = "UPDATE ".$this->TABLE
						." SET booking_team_id = '".$this->mBookingTeamId."'  "
						." ,  company_ids = '".$this->mCompanyIds."'  "
						." ,  booking_sale_id = '".$this->mBookingSaleId."'  "
						." ,  booking_place= '".$this->mBookingPlace."'  "
						." ,  sending_team_id = '".$this->mSendingTeamId."'  "
						." ,  company_id = '".$this->mCompanyId."'  "
						." ,  sending_sale_id = '".$this->mSendingSaleId."'  "
						." ,  sending_place = '".$this->mSendingPlace."'  "
						
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_fix");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}				
	
	
	function updateDelete(){
		global $sMemberId;	
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_delete");	
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateCheckPurchase(){
		global $sMemberId;
	
		// 0 : not checked
		// 1 : check ok
		// 1 : check not ok
		$strSql = "UPDATE ".$this->TABLE
						." SET check_purchase = '".$this->mCheckPurchase."'  "
						." , check_purchase_remark = '".$this->mCheckPurchaseRemark."'  "
						." , check_purchase_by = '".$this->mCheckPurchaseBy."'  "
						." ,  order_status = '".$this->mOrderStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_check_purchase");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStatus(){
		global $sMemberId;
	
		// 0 : not checked
		// 1 : check ok
		// 1 : check not ok
		$strSql = "UPDATE ".$this->TABLE
						." SET order_status = '".$this->mOrderStatus."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_status_cashier_to_account");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateStockRed(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_red_id = '".$this->mStockRedId."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();

		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_stock_red");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateInsureImport(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET insure_import = '".$this->mInsureImport."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateStockCar(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET stock_car_id = '".$this->mStockCarId."' , order_status = '".$this->mOrderStatus."' "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_stock_car_id");		
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateBookingThankyou(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET booking_thankyou = '".$this->mBookingThankyou."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_booking_thankyou");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
		
	function updateSendingThankyou(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET sending_thankyou = '".$this->mSendingThankyou."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"update_sending_thankyou");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateOrderReservePrice01(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET order_reserve_price01 = '".$this->mOrderReservePrice01."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"�����ͧ���駷�� 1");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateOrderReservePrice02(){
		global $sMemberId;
	
		$strSql = "UPDATE ".$this->TABLE
						." SET order_reserve_price02 = '".$this->mOrderReservePrice02."'  "
						." WHERE  order_id = ".$this->mOrderId."  ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"�����ͧ���駷�� 2");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
		
	function updateByCondition($sql){
		$strSql = $sql;
        $this->getConnection();	
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function delete() {
		global $sMemberId;
			
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_id=".$this->mOrderId." ";
        $this->getConnection();
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderId,"delete");
		
        $result = $this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if($Mode == "update"){
			//check order_number duplicate
			if($this->checkDuplicateOrder($Mode)){
				$asrErrReturn["order_number"] = "�Ţ���㺨ͧ��ӫ�͹��س��к�����";
			}
			
			//check not exiest stock_car
			if($this->checkStockCarExist() == false){
				$asrErrReturn["car_number"] = "����������Ţ�ѧ����к��ʵ�ͤö ��س����͡�ҡ��¡����ҹ��";
			}

			//check stock_car has take over by other user
			if($this->checkStockCarDuplicate($Mode)){
				$asrErrReturn["car_number"] = "�����Ţ�ѧ���١��ҹ���º��������";
			}			
		
		}else{
			//check order_number duplicate
			if($this->checkDuplicateOrder($Mode)){
				$asrErrReturn["order_number"] = "�Ţ���㺨ͧ��ӫ�͹��س��к�����";
			}
			
			//check not exiest stock_car
			if($this->checkStockCarExist() == false){
				$asrErrReturn["car_number"] = "����������Ţ�ѧ����к��ʵ�ͤö ��س����͡�ҡ��¡����ҹ��";
			}			
			
			//check stock_car has take over by other user
			if($this->checkStockCarDuplicate($Mode)){
				$asrErrReturn["car_number"] = "�����Ţ�ѧ���١��ҹ���º��������";
			}						
			
		
		}
        Return $asrErrReturn;
    }
	
	 Function checkCashierBooking($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

		if($this->mBookingCarSeries == "" OR $this->mBookingCarSeries == 0 ) $asrErrReturn["booking_car_series"] = "��辺����Ẻö ��س����͡�����Ũҡ�к�";
		if ($this->mOrderNumber == "") { 
			$asrErrReturn["order_number"] = "��س��к�����㺨ͧ";
		}else{
			if(!$this->checkUniqueOrderNumber(strtolower($Mode) )){
				$asrErrReturn["order_number"] = '����㺨ͧ��ӫ�͹';
			}
		}

        Return $asrErrReturn;
    }
	
	 Function checkCrlBooking($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if($this->mBookingSaleId == "" OR $this->mBookingSaleId == 0 ) $asrErrReturn["booking_sale_id"] = "��辺���ʾ�ѡ�ҹ��� ��س����͡�����Ũҡ�к�";
		if($this->mBookingCarSeries == "" OR $this->mBookingCarSeries == 0 ) $asrErrReturn["booking_car_series"] = "��辺����Ẻö ��س����͡�����Ũҡ�к�";
        Return $asrErrReturn;
    }	
	
	 Function checkCashierBookingCancel($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }	
		
 	Function checkCashierSending($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if($Mode == "update"){
			
		}else{

		}
		if($this->mSendingSaleId == "" OR $this->mSendingSaleId == 0 ) $asrErrReturn["sale_id"] = "��辺���ʾ�ѡ�ҹ��� ��س����͡�����Ũҡ�к�";
        Return $asrErrReturn;
    }		
		
 	Function checkCrlSending($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

		if($this->mSendingSaleId == "" OR $this->mSendingSaleId == 0 ) $asrErrReturn["sale_id"] = "��辺���ʾ�ѡ�ҹ��� ��س����͡�����Ũҡ�к�";
		
		if($this->mCancelStockCar != 1){
					if($this->mStockCarId == "" OR $this->mStockCarId == 0 ){
						 $asrErrReturn["stock_car_id"] = "�����Ţ�ѧ���١��ͧ ��س����͡�����Ũҡ�к�";
					}else{
						if($this->mCarNumber == ""){
					 		$asrErrReturn["stock_car_id"] = "��س����͡�����Ţ�ѧ";
						}else{
							$objStockCar = new StockCar();
							if( !$objStockCar->loadByCondition(" stock_car_id= ".$this->mStockCarId." AND car_number ='".$this->mCarNumber."'" )){
								$asrErrReturn["stock_car_id"] = "�����Ţ�ѧ���١��ͧ ��س����͡�����Ũҡ�к�";
							}else{
								if($objStockCar->getOrderId() > 0 AND $objStockCar->getOrderId() != $this->mOrderId ){
									$objOrder = new Order;
									$objOrder->setOrderId($objStockCar->getOrderId());
									$objOrder->load();
								
									$asrErrReturn["stock_car_id"] = "ö�١�кبҡ㺨ͧ ".$objOrder->getOrderNumber();
								}
							}
						}
					}
		}
		
		
        Return $asrErrReturn;
    }		
			
	 Function checkCashierSendingCancel($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if($Mode == "update"){
		
		}else{

		}
        Return $asrErrReturn;
    }		
	
	Function checkDuplicateOrder($Mode){
		if($Mode == "update"){
			$strSql = "SELECT * FROM ".$this->TABLE." WHERE order_id <> ".$this->mOrderId." AND order_number = '".$this->mOrderNumber."'  ";
		}else{
			$strSql = "SELECT * FROM ".$this->TABLE." WHERE order_number = '".$this->mOrderNumber."' ";
		}
		
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return true;
            }
        }
		return false;
	}
	
	Function checkStockCarExist(){
		$strSql = "SELECT * FROM t_stock_car  WHERE car_number  = '".$this->mCarNumber."'  ";
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return true;
            }
        }
		return false;
	}	
	
	
	Function checkStockCarDuplicate($Mode){
		if($Mode == "update"){
			$strSql = "SELECT * FROM ".$this->TABLE." WHERE order_id <> ".$this->mOrderId." AND stock_car_id = '".$this->mStockCarId."'  ";
		}else{
			$strSql = "SELECT * FROM ".$this->TABLE." WHERE stock_car_id = '".$this->mStockCarId."' ";
		}
		
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return true;
            }
        }
		return false;
	}	
	
    function checkUniqueOrderNumber($strMode)
    {
        $strSql  = "SELECT order_number  FROM t_order ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE order_number = ".trim(strtoupper($this->convStr4SQL($this->mOrderNumber)));
		}else{
			$strSql .= " WHERE order_number = ".trim(strtoupper($this->convStr4SQL($this->mOrderNumber)))." and order_id != ".$this->mOrderId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			if(copy($file, PATH_UPLOAD.$mFileName) ){
				//echo PATH_UPLOAD.$mFileName;
			}else{
				echo "false upload";		
			}
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_UPLOAD.$mFileName) And (is_file(PATH_UPLOAD.$mFileName) ) )
	    {
		    unLink(PATH_UPLOAD.$mFileName);
	    }
	}	
	
	function getSaleTeam($hSaleId){
		if($hSaleId > 0){
			$objSale = new Member();
			$objSale->setMemberId($hSaleId);
			$objSale->load();
			return $objSale->getTeam();
		}else{
			return 0;
		}
	}
	
}

/*********************************************************
		Class :				OrderList
		Last update :		25  Nov 06
		Description:		Class manage order list
*********************************************************/

class OrderList extends DataList {
	var $TABLE = "t_order";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  * FROM t_order  O "

			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING


		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadCCard() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE." O "
			."  LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
			."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			."  LEFT JOIN t_car_series  CS ON SC.car_series_id = CS.car_series_id "
			."  LEFT JOIN t_member  M ON M.member_id = O.sending_sale_id "
			."  LEFT JOIN t_stock_red  R ON R.stock_red_id = O.stock_red_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  DISTINCT O.*  FROM t_order  O "
		."  LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
		."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
		."  LEFT JOIN t_car_series  CS ON SC.car_series_id = CS.car_series_id "
		."  LEFT JOIN t_member  M ON M.member_id = O.sending_sale_id "
		."  LEFT JOIN t_stock_red  R ON R.stock_red_id = O.stock_red_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	
	
	function loadCCard01() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE." O "


			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  O.* FROM t_order  O "

			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	
	
	function loadBooking() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE." O "
			."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
			."  LEFT JOIN t_customer C ON C.customer_id = O.booking_customer_id "
			."  LEFT JOIN t_car_series CS ON CS.car_series_id = O.booking_car_series "
			.$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  DISTINCT O.* FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
		."  LEFT JOIN t_customer C ON C.customer_id = O.booking_customer_id "
		."  LEFT JOIN t_car_series CS ON CS.car_series_id = O.booking_car_series "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	    //echo $strSql;
		//exit;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadAll() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE." O "
			."  LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
			."  LEFT JOIN t_customer B ON B.customer_id = O.booking_customer_id "
			."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			."  LEFT JOIN t_stock_red  SR ON SR.stock_red_id = O.stock_red_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  distinct O.* FROM t_order  O "
		."  LEFT JOIN t_customer C ON C.customer_id = O.sending_customer_id "
		."  LEFT JOIN t_customer B ON B.customer_id = O.booking_customer_id "
		."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
		."  LEFT JOIN t_stock_red  SR ON SR.stock_red_id = O.stock_red_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadTBR() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE." O "
			."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  distinct O.* FROM t_order  O "
		."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE
			." O "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  * FROM t_order  O "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	function loadCustomerUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE
			." O  LEFT JOIN t_customer C ON O.customer_id = C.customer_id  "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  O.* ,firstname, lastname  FROM t_order  O "
			.' LEFT JOIN t_customer C ON O.customer_id = C.customer_id  '
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
			
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		
	
	
	function loadReportTBR() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE
			." O "
			."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
			."  LEFT JOIN t_car_series  C ON C.car_series_id = O.booking_car_series "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  O.* FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
		."  LEFT JOIN t_car_series  C ON C.car_series_id = O.booking_car_series "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadReportRegistry() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT O.order_id) as rowCount FROM ".$this->TABLE
			." O "
			."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
			."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			."  LEFT JOIN t_car_series  CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT  O.* FROM t_order  O "
			."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
			."  LEFT JOIN t_stock_car  SC ON SC.stock_car_id = O.stock_car_id "
			."  LEFT JOIN t_car_series  CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING

		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	//load report account rp05 rp04
	function loadReportAccount() {

		$strSql = "SELECT  Count(DISTINCT O.order_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  SUM(discount_over) AS sum_discount_over , COUNT(O.order_id) AS sum_order_id, sending_sale_id  FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' GROUP BY sending_sale_id '
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	//load report account rp05 rp04
	function loadBookingCompany() {

		$strSql = "SELECT  Count(DISTINCT O.company_ids) as rowCount FROM t_order  O "
		."  LEFT JOIN t_company C ON C.company_id = O.company_ids "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(O.company_ids) AS company_ids , C.title as booking_company_name, C.short_title as booking_company_short_name  FROM t_order  O "
		."  LEFT JOIN t_company C ON C.company_id = O.company_ids "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		

	//load report account rp05 rp04
	function loadSendingCompany() {

		$strSql = "SELECT  Count(DISTINCT O.company_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_company C ON C.company_id = O.company_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(O.company_id) AS company_id ,  C.title as sending_company_name, C.short_title as sending_company_short_name  FROM t_order  O "
		." LEFT JOIN t_company C ON C.company_id = O.company_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }				
		
	
	
	//load report account rp05 rp04
	function loadBookingTeam() {

		$strSql = "SELECT  Count(DISTINCT O.booking_team_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_team T ON T.team_id = O.booking_team_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(booking_team_id) AS booking_team_id ,  T.title as booking_team_name FROM t_order  O "
		."  LEFT JOIN t_team T ON T.team_id = O.booking_team_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		

	//load report account rp05 rp04
	function loadSendingTeam() {

		$strSql = "SELECT  Count(DISTINCT O.sending_team_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_team T ON T.team_id = O.sending_team_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(sending_team_id) AS sending_team_id ,  T.title as sending_team_name FROM t_order  O "
		."  LEFT JOIN t_team T ON T.team_id = O.sending_team_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }				
	
	
	
	//load report account rp05 rp04
	function loadBookingMember() {

		$strSql = "SELECT  Count(DISTINCT O.booking_sale_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(booking_sale_id) AS booking_sale_id, M.firstname AS booking_sale_name, M.nickname as booking_sale_nickname  FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.booking_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		

	//load report account rp05 rp04
	function loadSendingMember() {

		$strSql = "SELECT  Count(DISTINCT O.sending_sale_id) as rowCount FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(sending_sale_id) AS sending_sale_id, M.firstname AS sending_sale_name, M.nickname as sending_sale_nickname  FROM t_order  O "
		."  LEFT JOIN t_member M ON M.member_id = O.sending_sale_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }			
	
	
	//load report account rp05 rp04
	function loadSendingFN() {

		$strSql = "SELECT  Count(DISTINCT O.buy_company) as rowCount FROM t_order  O "
		."  LEFT JOIN t_fund_company F ON F.fund_company_id = O.buy_company "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL();	// WHERE clause
			
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
	
	
		$strSql = "SELECT  DISTINCT(buy_company) AS buy_company ,  F.title as fund_name FROM t_order  O "
		."  LEFT JOIN t_fund_company F ON F.fund_company_id = O.buy_company "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_car_series CS ON CS.car_series_id = SC.car_series_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		// echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				
				$this->mItemList[] = new Order($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }					
	
	
}
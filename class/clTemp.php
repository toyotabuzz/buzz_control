<?
/*********************************************************
		Class :					Temp

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class Temp extends DB{

	var $TABLE="temp";

	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_sale_id;
	function get_sale_id() { return $this->m_sale_id; }
	function set_sale_id($data) { $this->m_sale_id = $data; }
	
	//acard, booking, ccard
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }

	var $m_source;
	function get_source() { return $this->m_source; }
	function set_source($data) { $this->m_source = $data; }
	
	var $m_customer_id;
	function get_customer_id() { return $this->m_customer_id; }
	function set_customer_id($data) { $this->m_customer_id = $data; }
	
	var $m_code;
	function get_code() { return $this->m_code; }
	function set_code($data) { $this->m_code = $data; }

	var $m_car_type;
	function get_car_type() { return $this->m_car_type; }
	function set_car_type($data) { $this->m_car_type = $data; }
	
	var $m_car_model_id;
	function get_car_model_id() { return $this->m_car_model_id; }
	function set_car_model_id($data) { $this->m_car_model_id = $data; }
	
	var $m_car_series_id;
	function get_car_series_id() { return $this->m_car_series_id; }
	function set_car_series_id($data) { $this->m_car_series_id = $data; }
	
	var $m_color;
	function get_color() { return $this->m_color; }
	function set_color($data) { $this->m_color = $data; }
	
	var $m_car_number;
	function get_car_number() { return $this->m_car_number; }
	function set_car_number($data) { $this->m_car_number = $data; }
	
	var $m_engine_number;
	function get_engine_number() { return $this->m_engine_number; }
	function set_engine_number($data) { $this->m_engine_number = $data; }
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }	
	
	var $m_register_year;
	function get_register_year() { return $this->m_register_year; }
	function set_register_year($data) { $this->m_register_year = $data; }
	
	var $m_date_verify;
	function get_date_verify() { return $this->m_date_verify; }
	function set_date_verify($data) { $this->m_date_verify = $data; }	
	
	var $m_suggest_from;
	function get_suggest_from() { return $this->m_suggest_from; }
	function set_suggest_from($data) { $this->m_suggest_from = $data; }
	
	var $m_call_date;
	function get_call_date() { return $this->m_call_date; }
	function set_call_date($data) { $this->m_call_date = $data; }
	
	var $m_call_remark;
	function get_call_remark() { return $this->m_call_remark; }
	function set_call_remark($data) { $this->m_call_remark = $data; }
	
	var $m_date_add;
	function get_date_add() { return $this->m_date_add; }
	function set_date_add($data) { $this->m_date_add = $data; }

	var $m_order_id;
	function get_order_id() { return $this->m_order_id; }
	function set_order_id($data) { $this->m_order_id = $data; }
	
	var $m_checked;
	function get_checked() { return $this->m_checked; }
	function set_checked($data) { $this->m_checked = $data; }	
	

	function Temp($objData=NULL) {
        If ($objData->car_id !="") {
			$this->set_car_id($objData->car_id);
			$this->set_sale_id($objData->sale_id);
			$this->set_source($objData->source);
			$this->set_car_type($objData->car_type);
			$this->set_customer_id($objData->customer_id);
			$this->set_code($objData->code);
			$this->set_car_model_id($objData->car_model_id);
			$this->set_car_series_id($objData->car_series_id);
			$this->set_color($objData->color);
			$this->set_price($objData->price);
			$this->set_car_number($objData->car_number);
			$this->set_engine_number($objData->engine_number);
			$this->set_register_year($objData->register_year);
			$this->set_date_verify($objData->date_verify);
			$this->set_suggest_from($objData->suggest_from);
			$this->set_call_date($objData->call_date);
			$this->set_call_remark($objData->call_remark);
			$this->set_date_add($objData->date_add);
			$this->set_order_id($objData->order_id);
			$this->set_checked($objData->checked);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_car_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_id =".$this->m_car_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Temp($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Temp($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function updateByCondition($strSql){
        $this->getConnection();
        return $this->query($strSql);
	}	
	
	function updateChecked(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  checked = '".$this->m_checked."' "
		." WHERE car_id = ".$this->m_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}	
	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_id=".$this->m_car_id." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Temp Company List

		Last update :		22 Mar 02

		Description:		Temp Company List

*********************************************************/

class TempList extends DataList {
	var $TABLE = "temp";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Temp($row);
			}
			return true;
		} else {
			return false;
		}
    }
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_id) as rowCount FROM ".$this->TABLE." IC  "
		." LEFT JOIN t_customer C ON IC.customer_id = C.customer_id  "		
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT IC.* FROM ".$this->TABLE." IC "
		." LEFT JOIN t_customer C ON IC.customer_id = C.customer_id  "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		//echo $strSql;
		
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Temp($row);
			}
			return true;
		} else {
			return false;
		}
    }

	

	
}
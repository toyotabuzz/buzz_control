<?
/*********************************************************
		Class :					Com

		Last update :	  10 Jan 02

		Description:	  Class manage t_com table

*********************************************************/
 
class Comm extends DB{

	var $TABLE="t_com";

	var $mComId;
	function getComId() { return $this->mComId; }
	function setComId($data) { $this->mComId = $data; }
	
	var $mComType;
	function getComType() { return $this->mComType; }
	function setComType($data) { $this->mComType = $data; }

	var $mStartDate;
	function getStartDate() { return $this->mStartDate; }
	function setStartDate($data) { $this->mStartDate = $data; }
	
	var $mEndDate;
	function getEndDate() { return $this->mEndDate; }
	function setEndDate($data) { $this->mEndDate = $data; }	

	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mCarSeriesId;
	function getCarSeriesId() { return htmlspecialchars($this->mCarSeriesId); }
	function setCarSeriesId($data) { $this->mCarSeriesId = $data; }

	var $mAmount;
	function getAmount() { return htmlspecialchars($this->mAmount); }
	function setAmount($data) { $this->mAmount = $data; }	
	
	var $mStockCarId;
	function getStockCarId() { return htmlspecialchars($this->mStockCarId); }
	function setStockCarId($data) { $this->mStockCarId = $data; }		
	
	var $mReleaseDay;
	function getReleaseDay() { return htmlspecialchars($this->mReleaseDay); }
	function setReleaseDay($data) { $this->mReleaseDay = $data; }			
	
	var $mAllTime;
	function getAllTime() { return htmlspecialchars($this->mAllTime); }
	function setAllTime($data) { $this->mAllTime = $data; }
	
	var $mAllCar;
	function getAllCar() { return htmlspecialchars($this->mAllCar); }
	function setAllCar($data) { $this->mAllCar = $data; }
	
	var $mDateAdd;
	function getDateAdd() { return htmlspecialchars($this->mDateAdd); }
	function setDateAdd($data) { $this->mDateAdd = $data; }				
	
	function Comm($objData=NULL) {
        If ($objData->com_id !="") {
            $this->setComId($objData->com_id);
			$this->setComType($objData->com_type);
			$this->setStartDate($objData->start_date);
			$this->setEndDate($objData->end_date);
			$this->setPrice($objData->price);
			$this->setCarSeriesId($objData->car_series_id);
			$this->setAmount($objData->amount);
			$this->setStockCarId($objData->stock_car_id);
			$this->setReleaseDay($objData->release_day);
			$this->setAllTime($objData->all_time);
			$this->setAllCar($objData->all_car);
			$this->setDateAdd($objData->date_add);
        }
    }

	function init(){
		$this->setPrice(stripslashes($this->mPrice));
	}
		
	function load() {

		if ($this->mComId == '') {
			return false;
		}
		$strSql = "SELECT OP.*  FROM ".$this->TABLE." OP "		
		."  WHERE com_id =".$this->mComId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Comm($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Comm($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( com_type, date_add, start_date, end_date, price, car_series_id, amount, stock_car_id , all_time, all_car, release_day ) "
						." VALUES ( '".$this->mComType."','".date("Y-m-d")."','".$this->mStartDate."','".$this->mEndDate."','".$this->mPrice."','".$this->mCarSeriesId."','".$this->mAmount."','".$this->mStockCarId."','".$this->mAllTime."','".$this->mAllCar."','".$this->mReleaseDay."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mComId = mysql_insert_id();
            return $this->mComId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET com_type = '".$this->mComType."'  "
						." , start_date = '".$this->mStartDate."'  "
						." , end_date = '".$this->mEndDate."'  "
						." , price = '".$this->mPrice."'  "
						." , car_series_id = '".$this->mCarSeriesId."'  "
						." , amount = '".$this->mAmount."'  "
						." , stock_car_id = '".$this->mStockCarId."'  "
						." , all_time = '".$this->mAllTime."'  "
						." , all_car = '".$this->mAllCar."'  "
						." , release_day = '".$this->mReleaseDay."'  "
						." WHERE  com_id = ".$this->mComId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE com_id=".$this->mComId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Price List

		Last update :		22 Mar 02

		Description:		Order Price List

*********************************************************/

class CommList extends DataList {
	var $TABLE = "t_com";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT com_id) as rowCount FROM ".$this->TABLE." OP "		
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.* FROM ".$this->TABLE." OP "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Comm($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getComId()."\"");
			if (($defaultId != null) && ($objItem->getComId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCustomerId()."</option>");
		}
		echo("</select>");
	}

}
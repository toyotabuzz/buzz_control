<?
/*********************************************************
		Class :					HelpdeskDetail

		Last update :	  10 Jan 02

		Description:	  Class manage t_helpdesk_detail table

*********************************************************/
 
class HelpdeskDetail extends DB{

	var $TABLE="t_helpdesk_detail";

	var $mHelpdeskDetailId;
	function getHelpdeskDetailId() { return $this->mHelpdeskDetailId; }
	function setHelpdeskDetailId($data) { $this->mHelpdeskDetailId = $data; }
	
	var $mHelpdeskId;
	function getHelpdeskId() { return $this->mHelpdeskId; }
	function setHelpdeskId($data) { $this->mHelpdeskId = $data; }	
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }
	
	var $mShow;
	function getShow() { return htmlspecialchars($this->mShow); }
	function setShow($data) { $this->mShow = $data; }
	
	function HelpdeskDetail($objData=NULL) {
        If ($objData->helpdesk_detail_id !="") {
            $this->setHelpdeskDetailId($objData->helpdesk_detail_id);
			$this->setHelpdeskId($objData->helpdesk_id);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
			$this->setShow($objData->show);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mHelpdeskDetailId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE helpdesk_detail_id =".$this->mHelpdeskDetailId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->HelpdeskDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->HelpdeskDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( helpdesk_id, title, rank, show ) "
						." VALUES (  '".$this->mHelpdeskId."' ,  '".$this->mTitle."' , '".$this->mRank."' , '".$this->mShow."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mHelpdeskDetailId = mysql_insert_id();
            return $this->mHelpdeskDetailId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , helpdesk_id = '".$this->mHelpdeskId."'  "
						." , rank= '".$this->mRank."'  "
						." , show = '".$this->mShow."'  "
						." WHERE  helpdesk_detail_id = ".$this->mHelpdeskDetailId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE helpdesk_detail_id=".$this->mHelpdeskDetailId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���Ǣ��";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer HelpdeskDetailTopic List

		Last update :		22 Mar 02

		Description:		Customer HelpdeskDetailTopic List

*********************************************************/

class HelpdeskDetailList extends DataList {
	var $TABLE = "t_helpdesk_detail";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT helpdesk_detail_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new HelpdeskDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getHelpdeskDetailId()."\"");
			if (($defaultId != null) && ($objItem->getHelpdeskDetailId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
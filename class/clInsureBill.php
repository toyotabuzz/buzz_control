<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureBill extends DB{

	var $TABLE="t_insure_bill";
	
	var $m_insure_bill_id;
	function get_insure_bill_id() { return $this->m_insure_bill_id; }
	function set_insure_bill_id($data) { $this->m_insure_bill_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_insure_quotation_id;
	function get_insure_quotation_id() { return $this->m_insure_quotation_id; }
	function set_insure_quotation_id($data) { $this->m_insure_quotation_id = $data; }
	
	var $m_add_date;
	function get_add_date() { return $this->m_add_date; }
	function set_add_date($data) { $this->m_add_date = $data; }
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	var $m_ref1;
	function get_ref1() { return $this->m_ref1; }
	function set_ref1($data) { $this->m_ref1 = $data; }
	
	var $m_ref2;
	function get_ref2() { return $this->m_ref2; }
	function set_ref2($data) { $this->m_ref2 = $data; }
	
	var $m_add_by;
	function get_add_by() { return $this->m_add_by; }
	function set_add_by($data) { $this->m_add_by = $data; }

	
	function InsureBill($objData=NULL) {
        If ($objData->insure_bill_id !="" or $objData->add_by !="" ) {
			$this->set_insure_bill_id($objData->insure_bill_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_insure_quotation_id($objData->insure_quotation_id);
			$this->set_add_date($objData->add_date);
			$this->set_status($objData->status);
			$this->set_ref1($objData->ref1);
			$this->set_ref2($objData->ref2);
			$this->set_add_by($objData->add_by);			
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_bill_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			."   LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "
			."  WHERE insure_bill_id =".$this->m_insure_bill_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBill($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT P.* FROM ".$this->TABLE." P "
			."   LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "		
			."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureBill($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_quotation_id , add_date , status , ref1 , ref2 , add_by  ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_insure_quotation_id."' , "
		." '".$this->m_add_date."' , "
		." '".$this->m_status."' , "
		." '".$this->m_ref1."' , "
		." '".$this->m_ref2."' , "
		." '".$this->m_add_by."'  "		
		." ) ";
		

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_bill_id = mysql_insert_id();
            return $this->m_insure_bill_id;
        } else {
			return false;
	    }

	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , insure_quotation_id = '".$this->m_insure_quotation_id."' "
		." , add_date = '".$this->m_add_date."' "
		." , status = '".$this->m_status."' "
		." , ref1 = '".$this->m_ref1."' "
		." , ref2 = '".$this->m_ref2."' "
		." , add_by = '".$this->m_add_by."' "
		
		." WHERE insure_bill_id = ".$this->m_insure_bill_id." "; 	

        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	

	function update_status(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." status = '".$this->m_status."' "		
		." WHERE insure_bill_id = ".$this->m_insure_bill_id." ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_bill_id=".$this->m_insure_bill_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureBillList extends DataList {
	var $TABLE = "t_insure_bill";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_bill_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id ".$this->getFilterSQL();	// WHERE clause
	    //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE
			." P  LEFT JOIN t_insure_quotation Q ON P.insure_quotation_id = Q.quotation_id "
			."   LEFT JOIN t_insure  T ON T.insure_id = P.insure_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureBill($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	

}
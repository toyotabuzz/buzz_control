<?
/*********************************************************
		Class :					InsureFree

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure_free table

*********************************************************/
 
class InsureFree extends DB{

	var $TABLE="t_insure_free";

	var $m_insure_free_id;
	function get_insure_free_id() { return $this->m_insure_free_id; }
	function set_insure_free_id($data) { $this->m_insure_free_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_stock_product_id;
	function get_stock_product_id() { return $this->m_stock_product_id; }
	function set_stock_product_id($data) { $this->m_stock_product_id = $data; }
	
	var $m_title;
	function get_title() { return $this->m_title; }
	function set_title($data) { $this->m_title = $data; }
	
	var $m_qty;
	function get_qty() { return $this->m_qty; }
	function set_qty($data) { $this->m_qty = $data; }
	
	var $m_price;
	function get_price() { return $this->m_price; }
	function set_price($data) { $this->m_price = $data; }
	
	function InsureFree($objData=NULL) {
        If ($objData->insure_free_id !="") {
			$this->set_insure_free_id($objData->insure_free_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_stock_product_id($objData->stock_product_id);
			$this->set_title($objData->title);
			$this->set_qty($objData->qty);
			$this->set_price($objData->price);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mInsureFreeId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_free_id =".$this->mInsureFreeId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFree($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureFree($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , stock_product_id , title , qty , price ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_stock_product_id."' , "
		." '".$this->m_title."' , "
		." '".$this->m_qty."' , "
		." '".$this->m_price."' "
		." ) "; 
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mInsureFreeId = mysql_insert_id();
            return $this->mInsureFreeId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , stock_product_id = '".$this->m_stock_product_id."' "
		." , title = '".$this->m_title."' "
		." , qty = '".$this->m_qty."' "
		." , price = '".$this->m_price."' "
		." WHERE insure_free_id = ".$this->m_insure_free_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_free_id=".$this->mInsureFreeId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class InsureFreeList extends DataList {
	var $TABLE = "t_insure_free";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_free_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureFree($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}
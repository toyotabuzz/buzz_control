<?
/*********************************************************
		Class :					InsurePaymentLog

		Last update :	  2021/03/31

		Description:	  Class manage juinsure_payment_log table

*********************************************************/
 
class InsurePaymentLog extends DB {
    var $TABLE="t_insure_payment_log";

    var $mId;
	function getId() { return $this->mId; }
	function setId($data) { $this->mId = $data; }

    var $mInsureId;
	function getInsureId() { return $this->mInsureId; }
	function setInsureId($data) { $this->mInsureId = $data; }

    var $mInsureRemark;
	function getInsureRemark() { return $this->mInsureRemark; }
	function setInsureRemark($data) { $this->mInsureRemark = $data; }

    var $mInsureItemId;
	function getInsureItemId() { return $this->mInsureItemId; }
	function setInsureItemId($data) { $this->mInsureItemId = $data; }

    var $mInsureItemPaymentTypeId;
	function getInsureItemPaymentTypeId() { return $this->mInsureItemPaymentTypeId; }
	function setInsureItemPaymentTypeId($data) { $this->mInsureItemPaymentTypeId = $data; }

    var $mInsureItemTransferBankId;
	function getInsureItemTransferBankId() { return $this->mInsureItemTransferBankId; }
	function setInsureItemTransferBankId($data) { $this->mInsureItemTransferBankId = $data; }

    var $mInsureItemTransferBankBranch;
	function getInsureItemTransferBankBranch() { return $this->mInsureItemTransferBankBranch; }
	function setInsureItemTransferBankBranch($data) { $this->mInsureItemTransferBankBranch = $data; }

    var $mInsureItemPaymentBankId;
	function getInsureItemPaymentBankId() { return $this->mInsureItemPaymentBankId; }
	function setInsureItemPaymentBankId($data) { $this->mInsureItemPaymentBankId = $data; }

    var $mInsureItemPaymentBankAcc;
	function getInsureItemPaymentBankAcc() { return $this->mInsureItemPaymentBankAcc; }
	function setInsureItemPaymentBankAcc($data) { $this->mInsureItemPaymentBankAcc = $data; }

    var $mInsureItemPaymentDate;
	function getInsureItemPaymentDate() { return $this->mInsureItemPaymentDate; }
	function setInsureItemPaymentDate($data) { $this->mInsureItemPaymentDate = $data; }

    var $mInsureItemPaymentAmt;
	function getInsureItemPaymentAmt() { return $this->mInsureItemPaymentAmt; }
	function setInsureItemPaymentAmt($data) { $this->mInsureItemPaymentAmt = $data; }

    var $mInsureItemPaymentFile;
	function getInsureItemPaymentFile() { return $this->mInsureItemPaymentFile; }
	function setInsureItemPaymentFile($data) { $this->mInsureItemPaymentFile = $data; }

    var $mCreatedAt;
	function getCreatedAt() { return $this->mCreatedAt; }
	function setCreatedAt($data) { $this->mCreatedAt = $data; }

    var $mCreatedBy;
	function getCreatedBy() { return $this->mCreatedBy; }
	function setCreatedBy($data) { $this->mCreatedBy = $data; }

    var $mUpdatedAt;
	function getUpdatedAt() { return $this->mUpdatedAt; }
	function setUpdatedAt($data) { $this->mUpdatedAt = $data; }

    var $mUpdatedBy;
	function getUpdatedBy() { return $this->mUpdatedBy; }
	function setUpdatedBy($data) { $this->mUpdatedBy = $data; }

	function InsurePaymentLog($objData=NULL) {
        if ($objData->insure_id !="") {
			$this->setId($objData->id);
			$this->setInsureId($objData->insure_id);
			$this->setInsureRemark($objData->insure_remark);
			$this->setInsureItemId($objData->insure_item_id);
			$this->setInsureItemPaymentTypeId($objData->insure_item_payment_type_id);
			$this->setInsureItemTransferBankId($objData->insure_item_transfer_bank_id);
			$this->setInsureItemTransferBankBranch($objData->insure_item_transfer_bank_branch);
			$this->setInsureItemPaymentBankId($objData->insure_item_payment_bank_id);
			$this->setInsureItemPaymentBankAcc($objData->insure_item_payment_bank_acc);
			$this->setInsureItemPaymentDate($objData->insure_item_payment_date);
			$this->setInsureItemPaymentAmt($objData->insure_item_payment_amt);
			$this->setInsureItemPaymentFile($objData->insure_item_payment_file);
			$this->setCreatedAt($objData->created_at);
			$this->setCreatedBy($objData->created_by);
			$this->setUpdatedAt($objData->updated_at);
			$this->setUpdatedBy($objData->updated_by);
        }
    }

	function init() {

	}

	function load() {
		if ($this->mId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->mId;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsurePaymentLog($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadByCondition($strCondition='') {
		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql."<br>";
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsurePaymentLog($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

    function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id, insure_remark, insure_item_id, insure_item_payment_type_id, insure_item_transfer_bank_id, insure_item_transfer_bank_branch, insure_item_payment_bank_id, insure_item_payment_bank_acc, insure_item_payment_date, insure_item_payment_amt, insure_item_payment_file, created_at, created_by, updated_at, updated_by ) " ." VALUES ( "
		." '".$this->mInsureId."' , "
		." '".$this->mInsureRemark."' , "
		." '".$this->mInsureItemId."' , "
		." '".$this->mInsureItemPaymentTypeId."' , "
		." '".$this->mInsureItemTransferBankId."' , "
		." '".$this->mInsureItemTransferBankBranch."' , "
		." '".$this->mInsureItemPaymentBankId."' , "
		." '".$this->mInsureItemPaymentBankAcc."' , "
		." '".$this->mInsureItemPaymentDate."' , "
		." '".$this->mInsureItemPaymentAmt."' , "
		." '".$this->mInsureItemPaymentFile."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."', "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) "; 

        $this->getConnection();		
        if ($Result = $this->query($strSql)) {
            $this->mId = mysql_insert_id();
            return $this->mId;
        } else {
			return false;
	    }
	}

}
<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_color table

*********************************************************/
 
class CustomerCareer extends DB{

	var $TABLE="t_customer_career";

	var $m_customer_career_id;
	function get_customer_career_id() { return $this->m_customer_career_id; }
	function set_customer_career_id($data) { $this->m_customer_career_id = $data; }
	
	var $m_career_id;
	function get_career_id() { return $this->m_career_id; }
	function set_career_id($data) { $this->m_career_id = $data; }
	
	var $m_customer_id;
	function get_customer_id() { return $this->m_customer_id; }
	function set_customer_id($data) { $this->m_customer_id = $data; }
	
	var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }


	function CustomerCareer($objData=NULL) {
        If ($objData->customer_career_id !="") {
			$this->set_customer_career_id($objData->customer_career_id);
			$this->set_career_id($objData->career_id);
			$this->set_customer_id($objData->customer_id);
			$this->set_remark($objData->remark);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCustomerCareerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE customer_career_id =".$this->mCustomerCareerId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerCareer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mCustomerCareerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE customer_career_id =".$this->mCustomerCareerId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerCareer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerCareer($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( career_id , customer_id , remark ) " ." VALUES ( "
		." '".$this->m_career_id."' , "
		." '".$this->m_customer_id."' , "
		." '".$this->m_remark."' "
		." ) "; 
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCustomerCareerId = mysql_insert_id();
            return $this->m_customer_career_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  remark = '".$this->m_remark."' "
		." WHERE customer_career_id = ".$this->m_customer_career_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE customer_career_id=".$this->m_customer_career_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class CustomerCareerList extends DataList {
	var $TABLE = "t_customer_career";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT customer_career_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerCareer($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCustomerCareerId()."\"");
			if (($defaultId != null) && ($objItem->getCustomerCareerId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
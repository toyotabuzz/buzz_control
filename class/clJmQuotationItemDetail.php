<?
/*********************************************************
		Class :			  Insure Quotation

		Last update :	  06 april 2021

		Description:	  Class manage jmquotation_item_detail table

*********************************************************/
 
class JmQuotationItemDetail extends DB{

	var $TABLE="jmquotation_item_detail";

    var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }

	var $m_quotation_id;
	function get_quotation_id() { return $this->m_quotation_id; }
	function set_quotation_id($data) { $this->m_quotation_id = $data; }

	var $m_quotation_item_id;
	function get_quotation_item_id() { return $this->m_quotation_item_id; }
	function set_quotation_item_id($data) { $this->m_quotation_item_id = $data; }
	
	var $m_tmcampaign_id;
	function get_tmcampaign_id() { return $this->m_tmcampaign_id; }
	function set_tmcampaign_id($data) { $this->m_tmcampaign_id = $data; }	

	var $m_product_id;
	function get_product_id() { return $this->m_product_id; }
	function set_product_id($data) { $this->m_product_id = $data; }	
	
	var $m_premium_detail_id;
	function get_premium_detail_id() { return $this->m_premium_detail_id; }
	function set_premium_detail_id($data) { $this->m_premium_detail_id = $data; }

	var $m_is_premium_old;
	function get_is_premium_old() { return $this->m_is_premium_old; }
	function set_is_premium_old($data) { $this->m_is_premium_old = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }

	function JmQuotationItemDetail($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_quotation_id($objData->quotation_id);
			$this->set_quotation_item_id($objData->quotation_item_id);
			$this->set_tmcampaign_id($objData->tmcampaign_id);
			$this->set_product_id($objData->product_id);
			$this->set_premium_detail_id($objData->premium_detail_id);
			$this->set_is_premium_old($objData->is_premium_old);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->JmQuotationItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->JmQuotationItemDetail($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_id , quotation_item_id , tmcampaign_id , product_id , premium_detail_id , is_premium_old , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_quotation_id."' , "
		." '".$this->m_quotation_item_id."' , "
		." '".$this->m_tmcampaign_id."' , "
		." '".$this->m_product_id."' , "
		." '".$this->m_premium_detail_id."' , "
		." '".$this->m_is_premium_old."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." quotation_id = '".$this->m_quotation_id."' "
		." , quotation_item_id = '".$this->m_quotation_item_id."' "
		." , tmcampaign_id = '".$this->m_tmcampaign_id."' "
		." , product_id = '".$this->m_product_id."' "
		." , premium_detail_id = '".$this->m_premium_detail_id."' "
		." , is_premium_old = '".$this->m_is_premium_old."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
        $strSql = " DELETE FROM ".$this->TABLE
                 . " WHERE id=".$this->m_id." ";
         $this->getConnection();
         $result=$this->query($strSql);
         $this->unsetConnection();
         return $result;
    }

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }

	function getCampaignCode($quotation_item_id) {
		$strSql = "SELECT tmproduct.product_code AS campaign_code FROM t_insure_quotation_item "
		."LEFT JOIN jmquotation_item_detail ON jmquotation_item_detail.quotation_item_id = t_insure_quotation_item.quotation_item_id "
		."LEFT JOIN tmproduct ON tmproduct.id = jmquotation_item_detail.product_id "
		."WHERE t_insure_quotation_item.quotation_item_id = '".$quotation_item_id."'";

		$this->getConnection();
		if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->campaign_code = $row->campaign_code;
                $result->freeResult();
				$this->unsetConnection();
				return true;
            }
        }
		return false;
	}
}

/*********************************************************
		Class :				Insure Company List

		Last update :		06 april 2021

		Description:		Insure Company List

*********************************************************/
<?
/*********************************************************
		Class :					Label

		Last update :	  10 Jan 02

		Description:	  Class manage t_label table

*********************************************************/
 
class Label extends DB{

	var $TABLE="t_label";

	var $mLabelId;
	function getLabelId() { return $this->mLabelId; }
	function setLabelId($data) { $this->mLabelId = $data; }
	
	var $mText01;
	function getText01() { return htmlspecialchars(stripslashes($this->mText01)); }
	function setText01($data) { $this->mText01 = $data; }
	
	var $mText02;
	function getText02() { return htmlspecialchars(stripslashes($this->mText02)); }
	function setText02($data) { $this->mText02 = $data; }
	
	var $mText03;
	function getText03() { return stripslashes($this->mText03); }
	function setText03($data) { $this->mText03 = $data; }
	
	var $mText04;
	function getText04() { return stripslashes($this->mText04); }
	function setText04($data) { $this->mText04 = $data; }
	
	var $mText05;
	function getText05() { return htmlspecialchars(stripslashes($this->mText05)); }
	function setText05($data) { $this->mText05 = $data; }
	
	var $mText06;
	function getText06() { return htmlspecialchars(stripslashes($this->mText06)); }
	function setText06($data) { $this->mText06 = $data; }
	
	var $mText07;
	function getText07() { return htmlspecialchars(stripslashes($this->mText07)); }
	function setText07($data) { $this->mText07 = $data; }
	
	var $mText08;
	function getText08() { return htmlspecialchars(stripslashes($this->mText08)); }
	function setText08($data) { $this->mText08 = $data; }
	
	var $mText09;
	function getText09() { return htmlspecialchars(stripslashes($this->mText09)); }
	function setText09($data) { $this->mText09 = $data; }
	
	var $mText10;
	function getText10() { return htmlspecialchars(stripslashes($this->mText10)); }
	function setText10($data) { $this->mText10 = $data; }
	
	
	var $mH1;
	function getH1() { return htmlspecialchars($this->mH1); }
	function setH1($data) { $this->mH1 = $data; }
	
	var $mH2;
	function getH2() { return htmlspecialchars($this->mH2); }
	function setH2($data) { $this->mH2 = $data; }
	
	var $mW1;
	function getW1() { return htmlspecialchars($this->mW1); }
	function setW1($data) { $this->mW1 = $data; }
	
	var $mW2;
	function getW2() { return htmlspecialchars($this->mW2); }
	function setW2($data) { $this->mW2 = $data; }
	
	var $mCompanyId;
	function getCompanyId() { return htmlspecialchars($this->mCompanyId); }
	function setCompanyId($data) { $this->mCompanyId = $data; }	
	
	var $mMtype;
	function getMtype() { return htmlspecialchars($this->mMtype); }
	function setMtype($data) { $this->mMtype = $data; }		
	
	function Label($objData=NULL) {
        If ($objData->label_id !="") {
            $this->setLabelId($objData->label_id);
			$this->setText01($objData->text01);
			$this->setText02($objData->text02);
			$this->setText03($objData->text03);
			$this->setText04($objData->text04);
			$this->setText05($objData->text05);
			$this->setText06($objData->text06);
			$this->setText07($objData->text07);
			$this->setText08($objData->text08);
			$this->setText09($objData->text09);
			$this->setText10($objData->text10);
			$this->setH1($objData->h1);
			$this->setH2($objData->h2);
			$this->setW1($objData->w1);
			$this->setW2($objData->w2);
			$this->setCompanyId($objData->company_id);
			$this->setMtype($objData->mtype);
        }
    }

	function init(){
		$this->setText01(stripslashes($this->mText01));
	}
		
	function load() {

		if ($this->mLabelId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE label_id =".$this->mLabelId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Label($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Label($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( text01, text02, text03,  text04, text05, text06, text07, text08, text09, text10, h1,h2,w1,w2, company_id , mtype ) "
						." VALUES ( '".$this->mText01."',  '".$this->mText02."',  '".$this->mText03."',  '".$this->mText04."',  '".$this->mText05."',  '".$this->mText06."',  '".$this->mText07."',  '".$this->mText08."',  '".$this->mText09."',  '".$this->mText10."' ,  '".$this->mH1."'  ,  '".$this->mH2."' ,  '".$this->mW1."' ,  '".$this->mW2."' ,  '".$this->mCompanyId."'  ,  '".$this->mMtype."'   ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mLabelId = mysql_insert_id();
            return $this->mLabelId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET text01 = '".addslashes($this->mText01)."'  "
						." , text02 = '".addslashes($this->mText02)."'  "
						." , text03 = '".addslashes($this->mText03)."'  "
						." , text04 = '".addslashes($this->mText04)."'  "
						." , text05 = '".addslashes($this->mText05)."'  "
						." , text06 = '".addslashes($this->mText06)."'  "
						." , text07 = '".addslashes($this->mText07)."'  "
						." , text08 = '".addslashes($this->mText08)."'  "
						." , text09 = '".addslashes($this->mText09)."'  "
						." , text10 = '".addslashes($this->mText10)."'  "
						." , h1 = '".$this->mH1."'  "
						." , h2 = '".$this->mH2."'  "
						." , w1 = '".$this->mW1."'  "
						." , w2 = '".$this->mW2."'  "
						." , company_id= '".$this->mCompanyId."'  "
						." , mtype = '".$this->mMtype."'  "
						." WHERE  label_id = ".$this->mLabelId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE label_id=".$this->mLabelId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}		
}

/*********************************************************
		Class :				Customer LabelTopic List

		Last update :		22 Mar 02

		Description:		Customer LabelTopic List

*********************************************************/

class LabelList extends DataList {
	var $TABLE = "t_label";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT label_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Label($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getLabelId()."\"");
			if (($defaultId != null) && ($objItem->getLabelId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getText01()."</option>");
		}
		echo("</select>");
	}			
	
}
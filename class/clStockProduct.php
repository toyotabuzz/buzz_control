<?
/*********************************************************
		Class :				StockProduct

		Last update :		20 Aug 05

		Description:		Class manage stock_product table

*********************************************************/
 
class StockProduct extends User{

	var $TABLE="t_stock_product";

	var $mStockProductId;
	function getStockProductId() { return $this->mStockProductId; }
	function setStockProductId($data) { $this->mStockProductId = $data; }	

	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mStockProductGroupId;
	function getStockProductGroupId() { return htmlspecialchars($this->mStockProductGroupId); }
	function setStockProductGroupId($data) { $this->mStockProductGroupId = $data; }
	
	var $mStockProductGroupDetail;
	function getStockProductGroupDetail() { return htmlspecialchars($this->mStockProductGroupDetail); }
	function setStockProductGroupDetail($data) { $this->mStockProductGroupDetail = $data; }

	var $mStockProductTypeId;
	function getStockProductTypeId() { return htmlspecialchars($this->mStockProductTypeId); }
	function setStockProductTypeId($data) { $this->mStockProductTypeId = $data; }

	var $mStockProductTypeDetail;
	function getStockProductTypeDetail() { return htmlspecialchars($this->mStockProductTypeDetail); }
	function setStockProductTypeDetail($data) { $this->mStockProductTypeDetail = $data; }
	
	var $mStockProductPackId;
	function getStockProductPackId() { return htmlspecialchars($this->mStockProductPackId); }
	function setStockProductPackId($data) { $this->mStockProductPackId = $data; }

	var $mStockProductPackDetail;
	function getStockProductPackDetail() { return htmlspecialchars($this->mStockProductPackDetail); }
	function setStockProductPackDetail($data) { $this->mStockProductPackDetail = $data; }
	
	var $mRetail;
	function getRetail() { return htmlspecialchars($this->mRetail); }
	function setRetail($data) { $this->mRetail = $data; }
	
	var $mPrice;
	function getPrice() { return htmlspecialchars($this->mPrice); }
	function setPrice($data) { $this->mPrice = $data; }	

	var $mPriceArv;
	function getPriceArv() { return htmlspecialchars($this->mPriceArv); }
	function setPriceArv($data) { $this->mPriceArv = $data; }	

	var $mPriceType;
	function getPriceType() { return htmlspecialchars($this->mPriceType); }
	function setPriceType($data) { $this->mPriceType = $data; }	
	
	function getPriceTypeDetail() {
		if($this->mPriceType == 1) return "�ҡ�����"; 
		if($this->mPriceType == 2) return "��˹��ͧ"; 
	}		
	
	var $mMinValue;
	function getMinValue() { return htmlspecialchars($this->mMinValue); }
	function setMinValue($data) { $this->mMinValue = $data; }

	var $mMaxValue;
	function getMaxValue() { return htmlspecialchars($this->mMaxValue); }
	function setMaxValue($data) { $this->mMaxValue = $data; }
	
	var $mLogo;
	function getLogo() { return htmlspecialchars($this->mLogo); }
	function setLogo($data) { $this->mLogo = $data; }	
	
	var $mCarType;
	function getCarType() { return htmlspecialchars($this->mCarType); }
	function setCarType($data) { $this->mCarType = $data; }	
	
	var $mTMT;
	function getTMT() { return htmlspecialchars($this->mTMT); }
	function setTMT($data) { $this->mTMT = $data; }	

	function getTMTDetail() {
		if($this->mTMT == 0) return "�Թ��� BUZZ"; 
		if($this->mTMT == 1) return "�Թ��� TMT"; 
	}		
	
	//ʶҹ���Դ���
	var $mAddPlace;
	function getAddPlace() { return htmlspecialchars($this->mAddPlace); }
	function setAddPlace($data) { $this->mAddPlace = $data; }	
	
	//��è�ҧ����
	var $mWorkType;
	function getWorkType() { return htmlspecialchars($this->mWorkType); }
	function setWorkType($data) { $this->mWorkType = $data; }			
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }		
	
	
	function StockProduct($objData=NULL) {
        If ($objData->stock_product_id !="") {
            $this->setStockProductId($objData->stock_product_id);
			$this->setTitle($objData->title);
			$this->setStockProductGroupId($objData->stock_product_group_id);
			$this->setStockProductTypeId($objData->stock_product_type_id);
			$this->setStockProductPackId($objData->stock_product_pack_id);
			$this->setStockProductGroupDetail($objData->stock_product_group_detail);
			$this->setStockProductTypeDetail($objData->stock_product_type_detail);
			$this->setStockProductPackDetail($objData->stock_product_pack_detail);
			$this->setRetail($objData->retail);
			$this->setPrice($objData->price);
			$this->setPriceArv($objData->price_arv);
			$this->setPriceType($objData->price_type);
			$this->setMinValue($objData->min_value);
			$this->setMaxValue($objData->max_value);
			$this->setCode($objData->code);
			$this->setLogo($objData->logo);
			$this->setCarType($objData->car_type);
			$this->setTMT($objData->tmt);
			$this->setAddPlace($objData->add_place);
			$this->setWorkType($objData->work_type);
			$this->setRemark($objData->remark);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
		$this->setCode(stripslashes($this->mCode));
	}
		
	function load() {

		if ($this->mStockProductId == '') {
			return false;
		}
		$strSql = "SELECT SP.*, SPG.title AS stock_product_group_detail, SPT.title AS stock_product_type_detail, SPP.title AS stock_product_pack_detail  FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		."  WHERE stock_product_id =".$this->mStockProductId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockProduct($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadUTF8() {

		if ($this->mStockProductId == '') {
			return false;
		}
		$strSql = "SELECT SP.*, SPG.title AS stock_product_group_detail, SPT.title AS stock_product_type_detail, SPP.title AS stock_product_pack_detail  FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		."  WHERE stock_product_id =".$this->mStockProductId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockProduct($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, stock_product_group_id, stock_product_type_id, stock_product_pack_id, retail, price, price_arv, price_type, min_value, max_value, logo, car_type, tmt, add_place, work_type, remark, code  )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".$this->mStockProductGroupId."' , "
						."  '".$this->mStockProductTypeId."' , "
						."  '".$this->mStockProductPackId."' , "
						."  '".$this->mRetail."' , "
						."  '".$this->mPrice."' , "
						."  '".$this->mPriceArv."' , "
						."  '".$this->mPriceType."' , "
						."  '".$this->mMinValue."' , "
						."  '".$this->mMaxValue."' , "
						."  '".$this->mLogo."' , "
						."  '".$this->mCarType."' , "
						."  '".$this->mTMT."' , "
						."  '".$this->mAddPlace."' , "
						."  '".$this->mWorkType."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mCode."' )";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mStockProductId = mysql_insert_id();
            return $this->mStockProductId;
        } else {
			return false;
	    }
	}
	
	
	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." stock_product_group_id = '".$this->mStockProductGroupId."' , "
						." stock_product_type_id = '".$this->mStockProductTypeId."' , "
						." stock_product_pack_id = '".$this->mStockProductPackId."' , "
						." retail = '".$this->mRetail."' , "
						." price = '".$this->mPrice."' , "
						." price_arv = '".$this->mPriceArv."' , "
						." price_type = '".$this->mPriceType."' , "
						." code = '".$this->mCode."' , "
						." min_value = '".$this->mMinValue."' , "
						." max_value = '".$this->mMaxValue."' , "
						." car_type = '".$this->mCarType."' , "
						." tmt = '".$this->mTMT."' , "
						." add_place = '".$this->mAddPlace."' , "
						." work_type = '".$this->mWorkType."' , "
						." logo = '".$this->mLogo."' , "
						." remark = '".$this->mRemark."'  "
						." WHERE  stock_product_id = ".$this->mStockProductId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_product_id=".$this->mStockProductId." ";
        $this->getConnection();
        $this->query($strSql);
		$this->deletePicture($this->mLogo);
		
       $strSql = " DELETE FROM t_stock_dealer  "
                . " WHERE stock_product_id=".$this->mStockProductId." ";
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($strMode)
    {
		if ($this->mTitle == "") $asrErrReturn["title"] = '��س��к�';
		if ($this->mCode == "") {
			$asrErrReturn["code"] = '��س��к������Թ���';
		}else{
			if(!$this->checkUniqueCode(strtolower($strMode) )){
				$asrErrReturn["code"] = '�����Թ��ҫ�ӫ�͹';
			}		
		}				
        Return $asrErrReturn;
    }
	
    function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT code  FROM t_stock_product ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)));
		}else{
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)))." and stock_product_id != ".$this->mStockProductId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_IMG.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_IMG.$mFileName) And (is_file(PATH_IMG.$mFileName) ) )
	    {
		    unLink(PATH_IMG.$mFileName);
	    }
	}	
	
}


/*********************************************************
		Class :				StockProductList

		Last update :		20 Nov 02

		Description:		StockProduct list

*********************************************************/


class StockProductList extends DataList {
	var $TABLE = "t_stock_product";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT SP.stock_product_id) as rowCount FROM ".$this->TABLE." SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SP.*, SPG.title AS stock_product_group_detail, SPT.title AS stock_product_type_detail, SPP.title AS stock_product_pack_detail  FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProduct($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadCarProduct() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT SP.stock_product_id) as rowCount FROM ".$this->TABLE." SP  "
		."  LEFT JOIN t_stock_product_car_model SPCM ON SPCM.stock_product_id = SP.stock_product_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT DISTINCT(SP.stock_product_id) AS stock_product_id FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_car_model SPCM ON SPCM.stock_product_id = SP.stock_product_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProduct($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadCarProductUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT SP.stock_product_id) as rowCount FROM ".$this->TABLE." SP  "
		."  LEFT JOIN t_stock_product_car_model SPCM ON SPCM.stock_product_id = SP.stock_product_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT DISTINCT(SP.stock_product_id) AS stock_product_id FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_car_model SPCM ON SPCM.stock_product_id = SP.stock_product_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProduct($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadCRL() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_product_id) as rowCount FROM ".$this->TABLE." SP  "		
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "		
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SP.*, SPT.title AS stock_product_type_detail  FROM ".$this->TABLE."  SP  "	
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProduct($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_product_id) as rowCount FROM ".$this->TABLE." SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SP.*, SPG.title AS stock_product_group_detail, SPT.title AS stock_product_type_detail, SPP.title AS stock_product_pack_detail  FROM ".$this->TABLE."  SP  "
		."  LEFT JOIN t_stock_product_group SPG ON SPG.stock_product_group_id = SP.stock_product_group_id "
		."  LEFT JOIN t_stock_product_type SPT ON SPT.stock_product_type_id = SP.stock_product_type_id "
		."  LEFT JOIN t_stock_product_pack SPP ON SPP.stock_product_pack_id = SP.stock_product_pack_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockProduct($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"0\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockProductId()."\"");
			if (($defaultId != null) && ($objItem->getStockProductId() == $defaultId)) {
				echo(" selected");
			}
			if($objItem->getTMT==0){
			echo(">BUZZ - ".$objItem->getCode().":".$objItem->getTitle()."</option>");
			}else{
			echo(">TMT - ".$objItem->getCode().":".$objItem->getTitle()."</option>");
			}
		}
		echo("</select>");
	}	
}
<?
/*********************************************************
		Class :				InsureHistory

		Last update :		20 Aug 05

		Description:		Class manage insure_history table

*********************************************************/
 
class InsureHistory extends User{

	var $TABLE="t_insure_history";

	var $m_insure_history_id;
	function get_insure_history_id() { return $this->m_insure_history_id; }
	function set_insure_history_id($data) { $this->m_insure_history_id = $data; }
	
	var $m_edit_date;
	function get_edit_date() { return $this->m_edit_date; }
	function set_edit_date($data) { $this->m_edit_date = $data; }
	
	var $m_edit_by;
	function get_edit_by() { return $this->m_edit_by; }
	function set_edit_by($data) { $this->m_edit_by = $data; }
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_insure_customer_id;
	function get_insure_customer_id() { return $this->m_insure_customer_id; }
	function set_insure_customer_id($data) { $this->m_insure_customer_id = $data; }
	
	function InsureHistory($objData=NULL) {
        If ($objData->insure_history_id !="") {
			$this->set_insure_history_id($objData->insure_history_id);
			$this->set_edit_date($objData->edit_date);
			$this->set_edit_by($objData->edit_by);
			$this->set_car_id($objData->car_id);
			$this->set_insure_customer_id($objData->insure_customer_id);
        }
    }

	function init(){	

	}
		
	function load() {

		if ($this->mInsureHistoryId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_history_id =".$this->mInsureHistoryId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureHistory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	

	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureHistory($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( edit_date , edit_by , car_id , insure_customer_id ) " ." VALUES ( "
		." '".$this->m_edit_date."' , "
		." '".$this->m_edit_by."' , "
		." '".$this->m_car_id."' , "
		." '".$this->m_insure_customer_id."' "
		." ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mInsureHistoryId = mysql_insert_id();
            return $this->mInsureHistoryId;
        } else {
			return false;
	    }
	}
	

	
}


/*********************************************************
		Class :				InsureHistoryList

		Last update :		20 Nov 02

		Description:		InsureHistory list

*********************************************************/


class InsureHistoryList extends DataList {
	var $TABLE = "t_insure_history";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_history_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureHistory($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	

}
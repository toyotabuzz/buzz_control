<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_color table

*********************************************************/
 
class CustomerAcard extends DB{

	var $TABLE="t_customer_acard";

	var $mCustomerId;
	function getCustomerId() { return $this->mCustomerId; }
	function setCustomerId($data) { $this->mCustomerId = $data; }
	
	var $mInformationDate;
	function getInformationDate() { return htmlspecialchars($this->mInformationDate); }
	function setInformationDate($data) { $this->mInformationDate = $data; }
	
	var $mInfo;
	function getInfo() { return htmlspecialchars($this->mInfo); }
	function setInfo($data) { $this->mInfo = $data; }
	
	var $mPrintDate;
	function getPrintDate() { return htmlspecialchars($this->mPrintDate); }
	function setPrintDate($data) { $this->mPrintDate = $data; }
	
	var $mPrintDate1;
	function getPrintDate1() { return htmlspecialchars($this->mPrintDate1); }
	function setPrintDate1($data) { $this->mPrintDate1 = $data; }
	
	var $mEventId;
	function getEventId() { return htmlspecialchars($this->mEventId); }
	function setEventId($data) { $this->mEventId = $data; }
	
	var $mEventName;
	function getEventName() { return htmlspecialchars($this->mEventName); }
	function setEventName($data) { $this->mEventName = $data; }
	
	var $mMailing;
	function getMailing() { return htmlspecialchars($this->mMailing); }
	function setMailing($data) { $this->mMailing = $data; }
	
	var $mSaleName;
	function getSaleName() { return htmlspecialchars($this->mSaleName); }
	function setSaleName($data) { $this->mSaleName = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mFirstname;
	function getFirstname() { return htmlspecialchars($this->mFirstname); }
	function setFirstname($data) { $this->mFirstname = $data; }
	
	var $mLastname;
	function getLastname() { return htmlspecialchars($this->mLastname); }
	function setLastname($data) { $this->mLastname = $data; }
	
	var $mBirthday;
	function getBirthday() { return htmlspecialchars($this->mBirthday); }
	function setBirthday($data) { $this->mBirthday = $data; }
	
	var $mBirthday1;
	function getBirthday1() { return htmlspecialchars($this->mBirthday1); }
	function setBirthday1($data) { $this->mBirthday1 = $data; }
	
	var $mAddress;
	function getAddress() { return htmlspecialchars($this->mAddress); }
	function setAddress($data) { $this->mAddress = $data; }
	
	var $mTumbon;
	function getTumbon() { return htmlspecialchars($this->mTumbon); }
	function setTumbon($data) { $this->mTumbon = $data; }
	
	var $mAmphur;
	function getAmphur() { return htmlspecialchars($this->mAmphur); }
	function setAmphur($data) { $this->mAmphur = $data; }
	
	var $mProvince;
	function getProvince() { return htmlspecialchars($this->mProvince); }
	function setProvince($data) { $this->mProvince = $data; }
	
	var $mZip;
	function getZip() { return htmlspecialchars($this->mZip); }
	function setZip($data) { $this->mZip = $data; }
	
	var $mTumbonCode;
	function getTumbonCode() { return htmlspecialchars($this->mTumbonCode); }
	function setTumbonCode($data) { $this->mTumbonCode = $data; }

	var $mAmphurCode;
	function getAmphurCode() { return htmlspecialchars($this->mAmphurCode); }
	function setAmphurCode($data) { $this->mAmphurCode = $data; }

	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }	
	
	var $mZipCode;
	function getZipCode() { return htmlspecialchars($this->mZipCode); }
	function setZipCode($data) { $this->mZipCode = $data; }	
	
	var $mHomeTel;
	function getHomeTel() { return htmlspecialchars($this->mHomeTel); }
	function setHomeTel($data) { $this->mHomeTel = $data; }
	
	var $mMobile;
	function getMobile() { return htmlspecialchars($this->mMobile); }
	function setMobile($data) { $this->mMobile = $data; }
	
	var $mOfficeTel;
	function getOfficeTel() { return htmlspecialchars($this->mOfficeTel); }
	function setOfficeTel($data) { $this->mOfficeTel = $data; }
	
	var $mNeedCar;
	function getNeedCar() { return htmlspecialchars($this->mNeedCar); }
	function setNeedCar($data) { $this->mNeedCar = $data; }
	
	var $mSeries;
	function getSeries() { return htmlspecialchars($this->mSeries); }
	function setSeries($data) { $this->mSeries = $data; }
	
	var $mColor;
	function getColor() { return htmlspecialchars($this->mColor); }
	function setColor($data) { $this->mColor = $data; }
	
	var $mUsedCar;
	function getUsedCar() { return htmlspecialchars($this->mUsedCar); }
	function setUsedCar($data) { $this->mUsedCar = $data; }
	
	var $mUsedSeries;
	function getUsedSeries() { return htmlspecialchars($this->mUsedSeries); }
	function setUsedSeries($data) { $this->mUsedSeries = $data; }
	
	var $mUsedColor;
	function getUsedColor() { return htmlspecialchars($this->mUsedColor); }
	function setUsedColor($data) { $this->mUsedColor = $data; }
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }
	
	var $mEditBy;
	function getEditBy() { return htmlspecialchars($this->mEditBy); }
	function setEditBy($data) { $this->mEditBy = $data; }
	
	var $mCarCode;
	function getCarCode() { return htmlspecialchars($this->mCarCode); }
	function setCarCode($data) { $this->mCarCode = $data; }
	
	var $mDealer;
	function getDealer() { return htmlspecialchars($this->mDealer); }
	function setDealer($data) { $this->mDealer = $data; }
	
	var $mBoothLocation;
	function getBoothLocation() { return htmlspecialchars($this->mBoothLocation); }
	function setBoothLocation($data) { $this->mBoothLocation = $data; }
	
	var $mGradeId;
	function getGradeId() { return htmlspecialchars($this->mGradeId); }
	function setGradeId($data) { $this->mGradeId = $data; }
	
	var $mPrints;
	function getPrints() { return htmlspecialchars($this->mPrints); }
	function setPrints($data) { $this->mPrints = $data; }
	
	var $mDuplicateInfo;
	function getDuplicateInfo() { return htmlspecialchars($this->mDuplicateInfo); }
	function setDuplicateInfo($data) { $this->mDuplicateInfo = $data; }
	
	var $mRollbackMail;
	function getRollbackMail() { return htmlspecialchars($this->mRollbackMail); }
	function setRollbackMail($data) { $this->mRollbackMail = $data; }
	
	var $mFax;
	function getFax() { return htmlspecialchars($this->mFax); }
	function setFax($data) { $this->mFax = $data; }
	
	var $mGroupId;
	function getGroupId() { return htmlspecialchars($this->mGroupId); }
	function setGroupId($data) { $this->mGroupId = $data; }
	
	var $mGroupName;
	function getGroupName() { return htmlspecialchars($this->mGroupName); }
	function setGroupName($data) { $this->mGroupName = $data; }
	
	var $mGroupTypeId;
	function getGroupTypeId() { return htmlspecialchars($this->mGroupTypeId); }
	function setGroupTypeId($data) { $this->mGroupTypeId = $data; }
	
	var $mMailCode;
	function getMailCode() { return htmlspecialchars($this->mMailCode); }
	function setMailCode($data) { $this->mMailCode = $data; }
	
	var $mCheckName;
	function getCheckName() { return htmlspecialchars($this->mCheckName); }
	function setCheckName($data) { $this->mCheckName = $data; }	
	
	function CustomerAcard($objData=NULL) {
        If ($objData->customer_id !="") {
            $this->setCustomerId($objData->customer_id);
			$this->setInformationDate($objData->information_date);
			$this->setInfo($objData->info);
			$this->setPrintDate($objData->print_date);
			$this->setPrintDate1($objData->print_date1);
			$this->setEventId($objData->event_id);
			$this->setEventName($objData->event_name);
			$this->setMailing($objData->mailing);
			$this->setSaleName($objData->sale_name);
			$this->setTitle($objData->title);
			$this->setFirstname($objData->firstname);
			$this->setLastname($objData->lastname);
			$this->setBirthday($objData->birthday);
			$this->setBirthday1($objData->birthday1);
			$this->setAddress($objData->address);
			$this->setTumbon($objData->tumbon);
			$this->setAmphur($objData->amphur);
			$this->setProvince($objData->province);
			$this->setZip($objData->zip);
			$this->setTumbonCode($objData->tumbon_code);
			$this->setAmphurCode($objData->amphur_code);
			$this->setProvinceCode($objData->province_code);
			$this->setZipCode($objData->zip_code);
			$this->setHomeTel($objData->home_tel);
			$this->setMobile($objData->mobile);
			$this->setOfficeTel($objData->office_tel);
			$this->setNeedCar($objData->need_car);
			$this->setSeries($objData->series);
			$this->setColor($objData->color);
			$this->setUsedCar($objData->used_car);
			$this->setUsedSeries($objData->used_series);
			$this->setUsedColor($objData->used_color);
			$this->setRemark($objData->remark);
			$this->setEditBy($objData->edit_by);
			$this->setCarCode($objData->car_code);
			$this->setDealer($objData->dealer);
			$this->setBoothLocation($objData->booth_location);
			$this->setGradeId($objData->grade_id);
			$this->setPrints($objData->prints);
			$this->setDuplicateInfo($objData->duplicate_info);
			$this->setRollbackMail($objData->rollback_mail);
			$this->setFax($objData->fax);
			$this->setGroupId($objData->group_id);
			$this->setGroupName($objData->group_name);
			$this->setGroupTypeId($objData->group_type_id);
			$this->setMailCode($objData->mail_code);
			$this->setCheckName($objData->check_name);
			
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCustomerId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE customer_id =".$this->mCustomerId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerAcard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerAcard($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ) "
						." VALUES ( '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCustomerId = mysql_insert_id();
            return $this->mCustomerId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}
	
	function updateSql($strSql){
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}	
	
	function updateName(){
		$strSql = "UPDATE ".$this->TABLE
						." SET firstname = '".$this->mFirstname."'  "
						." , lastname = '".$this->mLastname."'  "
						." , title = '".$this->mTitle."'  "
						." , check_name = 1 "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}	

	function updateDate(){
		$strSql = "UPDATE ".$this->TABLE
						." SET info = '".$this->mInfo."'  "
						." , print_date1 =  '".$this->mPrintDate1."'  "
						." , birthday1 =  '".$this->mBirthday1."'  "
						." , check_name = 1 "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}	
	
	function updateCheckName(){
		$strSql = "UPDATE ".$this->TABLE
						." SET check_name = 1 "
						." WHERE  customer_id = ".$this->mCustomerId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}	
	
	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE customer_id=".$this->mCustomerId." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class CustomerAcardList extends DataList {
	var $TABLE="t_customer_acard";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT customer_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerAcard($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCustomerAcardId()."\"");
			if (($defaultId != null) && ($objItem->getCustomerAcardId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
<?
/*********************************************************
		Class :					Department

		Last update :	  10 Jan 02

		Description:	  Class manage t_department table

*********************************************************/
 
class Department extends DB{

	var $TABLE="t_department";

	var $mDepartmentId;
	function getDepartmentId() { return $this->mDepartmentId; }
	function setDepartmentId($data) { $this->mDepartmentId = $data; }
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	function Department($objData=NULL) {
        If ($objData->department_id !="") {
            $this->setDepartmentId($objData->department_id);
			$this->setCode($objData->code);
			$this->setTitle($objData->title);
        }
    }

	function init(){	
		$this->setCode(stripslashes($this->mCode));
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mDepartmentId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE department_id =".$this->mDepartmentId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Department($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Department($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( code, title ) "
						." VALUES ( '".$this->mCode."' , "
						."  '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mDepartmentId = mysql_insert_id();
            return $this->mDepartmentId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET code = '".$this->mCode."' , "
						." title = '".$this->mTitle."'  "
						." WHERE  department_id = ".$this->mDepartmentId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE department_id=".$this->mDepartmentId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mCode == "") {
			$asrErrReturn["code"] = '��س��к�����';
		}else{
			if(!$this->checkUniqueCode(strtolower($strMode) )){
				$asrErrReturn["code"] = '���ʫ�ӫ�͹';
			}		
		}				
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت���Ἱ�";
        Return $asrErrReturn;
    }
	
    function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT code  FROM t_department ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)));
		}else{
			$strSql .= " WHERE code = ".trim(strtoupper($this->convStr4SQL($this->mCode)))." and department_id != ".$this->mDepartmentId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				DepartmentList

		Last update :		22 Mar 02

		Description:		Department user list

*********************************************************/


class DepartmentList extends DataList {
	var $TABLE = "t_department";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT department_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Department($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getDepartmentId()."\"");
			if (($defaultId != null) && ($objItem->getDepartmentId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script) {
		echo ("<select class=\"field\" $script Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getDepartmentId()."\"");
			if (($defaultId != null) && ($objItem->getDepartmentId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
}
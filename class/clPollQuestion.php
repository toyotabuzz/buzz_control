<?
/*********************************************************
		Class :				PollQuestion

		Last update :		20 Nov 02

		Description:		Class manage PollQuestiont table

*********************************************************/
 
class PollQuestion extends DB{

	var $TABLE="t_poll_question";
	
	var $mPollQuestionId;
	function getPollQuestionId() { return $this->mPollQuestionId; }
	function setPollQuestionId($data) { $this->mPollQuestionId = $data; }
	
	var $mPollId;
	function getPollId() { return $this->mPollId; }
	function setPollId($data) { $this->mPollId = $data; }

	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mRank;
	function getRank() { return $this->mRank; }
	function setRank($data) { $this->mRank = $data; }

	function PollQuestion($objData=NULL) {
        If ($objData->pollQuestionId !="") {
            $this->setPollQuestionId($objData->pollQuestionId);
			$this->setPollId($objData->pollId);
			$this->setTitle($objData->title);
			$this->setRank($objData->rank);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
	}
	
	function load() {

		if ($this->mPollQuestionId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE pollQuestionId =".$this->mPollQuestionId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollQuestion($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, pollId, rank  )"
						." VALUES ( '".$this->mTitle."' , "
						."  '".$this->mPollId."' , "
						."  '".$this->mRank."' ) ";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollQuestionId = mysql_insert_id();
            return $this->mPollQuestionId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." pollId = '".$this->mPollId."' , "
						." rank = '".$this->mRank."'  "
						." WHERE  pollQuestionId = ".$this->mPollQuestionId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pollQuestionId=".$this->mPollQuestionId." ";
        $this->getConnection();
        $this->query($strSql);
       $strSql = " DELETE FROM t_poll_answer "
                . " WHERE pollQuestionId=".$this->mPollQuestionId." ";
        $this->query($strSql);

	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				if ($this->mTitle == "") $asrErrReturn["title"] = "Please enter Title";
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_FILE_NEWS.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_FILE_NEWS.$mFileName) And (is_file(PATH_FILE_NEWS.$mFileName) ) )
	    {
		    unLink(PATH_FILE_NEWS.$mFileName);
	    }
	}
}

/*********************************************************
		Class :				PollQuestionList

		Last update :		20 Nov 02

		Description:		PollQuestionuser list

*********************************************************/


class PollQuestionList extends DataList {
	var $TABLE = "t_poll_question";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pollQuestionId) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "				
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollQuestion($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
}
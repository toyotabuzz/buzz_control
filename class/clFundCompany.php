<?
/*********************************************************
		Class :					Fund Company

		Last update :	  10 Jan 02

		Description:	  Class manage t_fund_company table

*********************************************************/
 
class FundCompany extends DB{

	var $TABLE="t_fund_company";

	var $mFundCompanyId;
	function getFundCompanyId() { return $this->mFundCompanyId; }
	function setFundCompanyId($data) { $this->mFundCompanyId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mP01;
	function getP01() { return htmlspecialchars($this->mP01); }
	function setP01($data) { $this->mP01 = $data; }
	
	var $mP02;
	function getP02() { return htmlspecialchars($this->mP02); }
	function setP02($data) { $this->mP02 = $data; }
	
	var $mP03;
	function getP03() { return htmlspecialchars($this->mP03); }
	function setP03($data) { $this->mP03 = $data; }
	
	var $mP04;
	function getP04() { return htmlspecialchars($this->mP04); }
	function setP04($data) { $this->mP04 = $data; }
	
	function FundCompany($objData=NULL) {
        If ($objData->fund_company_id !="") {
            $this->setFundCompanyId($objData->fund_company_id);
			$this->setTitle($objData->title);
			$this->setCode($objData->code);
			$this->setP01($objData->p01);
			$this->setP02($objData->p02);
			$this->setP03($objData->p03);
			$this->setP04($objData->p04);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mFundCompanyId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE fund_company_id =".$this->mFundCompanyId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->FundCompany($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->FundCompany($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, code, p01, p02, p03, p04 ) "
						." VALUES ( '".$this->mTitle."' ,  '".$this->mCode."' ,  '".$this->mP01."' ,  '".$this->mP02."' ,  '".$this->mP03."' ,  '".$this->mP04."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mFundCompanyId = mysql_insert_id();
            return $this->mFundCompanyId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." , code = '".$this->mCode."'  "
						." , p01 = '".$this->mP01."'  "
						." , p02 = '".$this->mP02."'  "
						." , p03 = '".$this->mP03."'  "
						." , p04 = '".$this->mP04."'  "
						." WHERE  fund_company_id = ".$this->mFundCompanyId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE fund_company_id=".$this->mFundCompanyId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == "") $asrErrReturn["code"] = "��س��к�";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Fund Company List

		Last update :		22 Mar 02

		Description:		Fund Company List

*********************************************************/

class FundCompanyList extends DataList {
	var $TABLE = "t_fund_company";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT fund_company_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new FundCompany($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getFundCompanyId()."\"");
			if (($defaultId != null) && ($objItem->getFundCompanyId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
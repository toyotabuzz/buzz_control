<?
/*********************************************************
		Class :					Monitor

		Last update :	  10 Jan 02

		Description:	  Class manage t_monitor table

*********************************************************/
 
class Monitor extends DB{

	var $TABLE="t_monitor";

	var $mMonitorId;
	function getMonitorId() { return $this->mMonitorId; }
	function setMonitorId($data) { $this->mMonitorId = $data; }
	
	var $mMemberId;
	function getMemberId() { return $this->mMemberId; }
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mTableName;
	function getTableName() { return $this->mTableName; }
	function setTableName($data) { $this->mTableName = $data; }
	
	var $mRecordId;
	function getRecordId() { return $this->mRecordId; }
	function setRecordId($data) { $this->mRecordId = $data; }
	
	var $mStatus;
	function getStatus() { return $this->mStatus; }
	function setStatus($data) { $this->mStatus = $data; }
	
	var $mMonitorDate;
	function getMonitorDate() { return $this->mMonitorDate; }
	function setMonitorDate($data) { $this->mMonitorDate = $data; }
	
	function Monitor($objData=NULL) {
        If ($objData->monitor_id OR $objData->record_id !="") {
			$this->setMonitorId($objData->monitor_id);
			$this->setMemberId($objData->member_id);
			$this->setTableName($objData->table_name);
			$this->setRecordId($objData->record_id);
			$this->setStatus($objData->status);
			$this->setMonitorDate($objData->monitor_date);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mMonitorId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE monitor_id =".$this->mMonitorId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Monitor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Monitor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByConditionMax($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  MO , (SELECT max(monitor_id) as monitor_id   FROM t_monitor  WHERE ".$strCondition." GROUP BY table_name ) maxresults  WHERE MO.monitor_id = maxresults.monitor_id ";
		$this->getConnection();
		
		//echo $strSql;
		
		
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Monitor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}		
	
	//function load for sale report : rp01
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(monitor_id) AS RSUM  FROM ".$this->TABLE." M "
					." WHERE ".$strCondition;
		//echo $strSql;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
		return false;
	}
	

	function add($member_id, $table_name, $record_id, $status) {
		$strSql = "INSERT INTO ".$this->TABLE." ( member_id,table_name,record_id,status,monitor_date ) "
		." VALUES ( "
		." '".$member_id."' , "
		." '".$table_name."' , "
		." '".$record_id."' , "
		." '".$status."' , "
		." '".date("Y-m-d H:i:s")."'  "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mMonitorId = mysql_insert_id();
            return $this->mMonitorId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  member_id = '".$this->mMemberId."' "
		." , table_name = '".$this->mTableName."' "
		." , record_id = '".$this->mRecordId."' "
		." , status = '".$this->mStatus."' "
		." , monitor_date = '".$this->mMonitorDate."' "
		." WHERE monitor_id = ".$this->mMonitorId." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE monitor_id=".$this->mMonitorId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���Ǣ�͡�õԴ���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer MonitorTopic List

		Last update :		22 Mar 02

		Description:		Customer MonitorTopic List

*********************************************************/

class MonitorList extends DataList {
	var $TABLE = "t_monitor";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT monitor_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Monitor($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadCompany() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT monitor_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_member M ON M.member_id = P.member_id  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT P.* FROM ".$this->TABLE." P "
			." LEFT JOIN t_member M ON M.member_id = P.member_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Monitor($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadDistinct() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT record_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT DISTINCT(record_id) AS record_id FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Monitor($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getMonitorId()."\"");
			if (($defaultId != null) && ($objItem->getMonitorId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
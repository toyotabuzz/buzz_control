<?
/*********************************************************
		Class :					Order Extra

		Last update :	  10 Jan 02

		Description:	  Class manage t_order_extra table

*********************************************************/
 
class OrderExtra extends DB{

	var $TABLE="t_order_extra";

	var $mOrderExtraId;
	function getOrderExtraId() { return $this->mOrderExtraId; }
	function setOrderExtraId($data) { $this->mOrderExtraId = $data; }
	
	var $mOrderId;
	function getOrderId() { return $this->mOrderId; }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mPrId;
	function getPrId() { return $this->mPrId; }
	function setPrId($data) { $this->mPrId = $data; }
	
	// ʶҹТͧ ��÷���¡��
	// 1 ��ͨͧ
	// 2 ������ͺ
	// 3 ����
	// 4 ��Сѹ���-�����
	// 5 ��Сѹ���-��������
	var $mExtraType;
	function getExtraType() { return ($this->mExtraType); }
	function setExtraType($data) { $this->mExtraType = $data; }

	var $mExtraDate;
	function getExtraDate() { return ($this->mExtraDate); }
	function setExtraDate($data) { $this->mExtraDate = $data; }

	var $mName;
	function getName() { return ($this->mName); }
	function setName($data) { $this->mName = $data; }
	
	var $mAddress;
	function getAddress() { return ($this->mAddress); }
	function setAddress($data) { $this->mAddress = $data; }
	
	var $mBookingNumber;
	function getBookingNumber() { return htmlspecialchars($this->mBookingNumber); }
	function setBookingNumber($data) { $this->mBookingNumber = $data; }	
	
	var $mSendingNumber;
	function getSendingNumber() { return htmlspecialchars($this->mSendingNumber); }
	function setSendingNumber($data) { $this->mSendingNumber = $data; }		
	
	var $mSendingNumberNohead;
	function getSendingNumberNohead() { return htmlspecialchars($this->mSendingNumberNohead); }
	function setSendingNumberNohead($data) { $this->mSendingNumberNohead = $data; }		
	
	var $mTotalPrice;
	function getTotalPrice() { return htmlspecialchars($this->mTotalPrice); }
	function setTotalPrice($data) { $this->mTotalPrice = $data; }
	
	var $mAddBy;
	function getAddBy() { return htmlspecialchars($this->mAddBy); }
	function setAddBy($data) { $this->mAddBy = $data; }

	var $mAddDate;
	function getAddDate() { return htmlspecialchars($this->mAddDate); }
	function setAddDate($data) { $this->mAddDate = $data; }
	
	var $mEditBy;
	function getEditBy() { return htmlspecialchars($this->mEditBy); }
	function setEditBy($data) { $this->mEditBy = $data; }
	
	var $mEditDate;
	function getEditDate() { return htmlspecialchars($this->mEditDate); }
	function setEditDate($data) { $this->mEditDate = $data; }	
	
	var $mDeleteStatus;
	function getDeleteStatus() { return $this->mDeleteStatus; }
	function setDeleteStatus($data) { $this->mDeleteStatus = $data; }
	
	var $mDeleteBy;
	function getDeleteBy() { return $this->mDeleteBy; }
	function setDeleteBy($data) { $this->mDeleteBy = $data; }
	
	var $mDeleteReason;
	function getDeleteReason() { return $this->mDeleteReason; }
	function setDeleteReason($data) { $this->mDeleteReason = $data; }
	
	var $mDeleteDate;
	function getDeleteDate() { return $this->mDeleteDate; }
	function setDeleteDate($data) { $this->mDeleteDate = $data; }	
	
	var $mCompanyId;
	function getCompanyId() { return $this->mCompanyId; }
	function setCompanyId($data) { $this->mCompanyId = $data; }		
	
	var $mNumTime;
	function getNumTime() { return $this->mNumTime; }
	function setNumTime($data) { $this->mNumTime = $data; }		
	
	function OrderExtra($objData=NULL) {
        If ($objData->order_extra_id !="") {
            $this->setOrderExtraId($objData->order_extra_id);
			$this->setOrderId($objData->order_id);
			$this->setPrId($objData->pr_id);
			$this->setExtraDate($objData->extra_date);
			$this->setExtraType($objData->extra_type);
			$this->setName($objData->name);
			$this->setAddress($objData->address);
			$this->setBookingNumber($objData->booking_number);
			$this->setSendingNumber($objData->sending_number);
			$this->setSendingNumberNohead($objData->sending_number_nohead);
			$this->setTotalPrice($objData->total_price);
			$this->setAddBy($objData->add_by);
			$this->setAddDate($objData->add_date);
			$this->setEditBy($objData->edit_by);
			$this->setEditDate($objData->edit_date);
			$this->setDeleteStatus($objData->delete_status);
			$this->setDeleteBy($objData->delete_by);
			$this->setDeleteReason($objData->delete_reason);
			$this->setDeleteDate($objData->delete_date);
			$this->setCompanyId($objData->company_id);
			$this->setNumTime($objData->num_time);
        }
    }

	function init(){

	}
		
	function load() {

		if ($this->mOrderExtraId == '') {
			return false;
		}
		$strSql = "SELECT OP.*  FROM ".$this->TABLE." OP "
		."  WHERE order_extra_id =".$this->mOrderExtraId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderExtra($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->OrderExtra($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		global $sMemberId;	
		$strSql = "INSERT INTO ".$this->TABLE
						." ( company_id, num_time, order_id, pr_id, extra_date, extra_type, name, address, booking_number, sending_number, sending_number_nohead, total_price, add_by, add_date  ) "
						." VALUES ( '".$this->mCompanyId."', '".$this->mNumTime."', '".$this->mOrderId."','".$this->mPrId."','".$this->mExtraDate."','".$this->mExtraType."','".$this->mName."','".$this->mAddress."','".$this->mBookingNumber."','".$this->mSendingNumber."','".$this->mSendingNumberNohead."','".$this->mTotalPrice."','".$this->mAddBy."','".date("Y-m-d H:i:s")."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mOrderExtraId = mysql_insert_id();
			
			$objMonitor = new Monitor();
			if($this->mExtraType == 1){
			$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"�����ͧ");
			}
			if($this->mExtraType == 2){
			$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"�������ͺ");
			}
			if($this->mExtraType == 3){
			$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"��������");
			}
						
            return $this->mOrderExtraId;
        } else {
			return false;
	    }
	}

	function update(){
		global $sMemberId;		
		$strSql = "UPDATE ".$this->TABLE
						." SET order_id = '".$this->mOrderId."'  "
						." , pr_id = '".$this->mPrId."'  "
						." , num_time = '".$this->mNumTime."'  "
						." , extra_date = '".$this->mExtraDate."'  "
						." , extra_type = '".$this->mExtraType."'  "
						." , name = '".$this->mName."'  "
						." , address = '".$this->mAddress."'  "
						." , booking_number = '".$this->mBookingNumber."'  "
						." , sending_number = '".$this->mSendingNumber."'  "
						." , sending_number_nohead = '".$this->mSendingNumberNohead."'  "
						." , total_price = '".$this->mTotalPrice."'  "
						." , edit_by = '".$this->mEditBy."'  "
						." , edit_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_extra_id = ".$this->mOrderExtraId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		if($this->mExtraType == 1){
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"��������ͧ");
		}
		if($this->mExtraType == 2){
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"����������ͺ");
		}
		if($this->mExtraType == 3){
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"�����������");
		}
		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function updateDelete(){
		global $sMemberId;		
		// 0 : not delete
		// 1 : deleted
		$strSql = "UPDATE ".$this->TABLE
						." SET delete_status = '".$this->mDeleteStatus."'  "
						." , delete_by = '".$this->mDeleteBy."'  "
						." , delete_reason = '".$this->mDeleteReason."'  "
						." , delete_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  order_extra_id = ".$this->mOrderExtraId."  ";
        $this->getConnection();
		//echo $strSql;
		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"update delete");
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	function delete() {
		global $sMemberId;		
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE order_extra_id=".$this->mOrderExtraId." ";
        $this->getConnection();
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_order_payment  "
                . " WHERE order_extra_id=".$this->mOrderExtraId." ";

		
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"delete");		
		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		global $sMemberId;		
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
       $result=$this->query($strSql);
		$objMonitor = new Monitor();
		$objMonitor->add($sMemberId,$this->TABLE,$this->mOrderExtraId,"delete by condition");
		
       
		$this->unsetConnection();
		return $result;		
		
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Order Extra List

		Last update :		22 Mar 02

		Description:		Order Extra List

*********************************************************/

class OrderExtraList extends DataList {
	var $TABLE = "t_order_extra";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_extra_id) as rowCount FROM ".$this->TABLE." OP "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "
		."  ".$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.*  FROM ".$this->TABLE." OP "
		." LEFT JOIN t_order O ON O.order_id = OP.order_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderExtra($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadExtra() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT order_extra_id) as rowCount FROM ".$this->TABLE." OP "
		."  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT OP.*  FROM ".$this->TABLE." OP "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new OrderExtra($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getOrderExtraId()."\"");
			if (($defaultId != null) && ($objItem->getOrderExtraId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCustomerId()."</option>");
		}
		echo("</select>");
	}

}
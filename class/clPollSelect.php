<?
/*********************************************************
		Class :					PollSelect

		Last update :	  10 Jan 02

		Description:	  Class manage t_poll_select table

*********************************************************/
 
class PollSelect extends DB{

	var $TABLE="t_poll_select";

	var $m_poll_select_id;
	function get_poll_select_id() { return $this->m_poll_select_id; }
	function set_poll_select_id($data) { $this->m_poll_select_id = $data; }
	
	var $m_qa_id;
	function get_qa_id() { return $this->m_qa_id; }
	function set_qa_id($data) { $this->m_qa_id = $data; }
	
	var $m_event_id;
	function get_event_id() { return $this->m_event_id; }
	function set_event_id($data) { $this->m_event_id = $data; }
	
	var $m_type;
	function get_type() { return $this->m_type; }
	function set_type($data) { $this->m_type = $data; }
	
	var $m_rank;
	function get_rank() { return $this->m_rank; }
	function set_rank($data) { $this->m_rank = $data; }	
	
	function PollSelect($objData=NULL) {
        If ($objData->poll_select_id !="") {
			$this->set_poll_select_id($objData->poll_select_id);
			$this->set_qa_id($objData->qa_id);
			$this->set_event_id($objData->event_id);
			$this->set_type($objData->type);
			$this->set_rank($objData->rank);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_poll_select_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE poll_select_id =".$this->m_poll_select_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PollSelect($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		//echo $strSql;
		
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->PollSelect($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( qa_id , event_id , type, rank ) " ." VALUES ( "
		." '".$this->m_qa_id."' , "
		." '".$this->m_event_id."' , "
		." '".$this->m_type."' , "
		." '".$this->m_rank."' "
		." ) "; 
		//echo $strSql;
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_poll_select_id = mysql_insert_id();
            return $this->m_poll_select_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." qa_id = '".$this->m_qa_id."' "
		." , event_id = '".$this->m_event_id."' "
		." , type = '".$this->m_type."' "
		." , rank = '".$this->m_rank."' "
		." WHERE poll_select_id = ".$this->m_poll_select_id." "; 
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE poll_select_id=".$this->m_poll_select_id." ";
        $this->getConnection();
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class PollSelectList extends DataList {
	var $TABLE = "t_poll_select";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT poll_select_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollSelect($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPollSelectId()."\"");
			if (($defaultId != null) && ($objItem->getPollSelectId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPollSelectId()."\"");
			if (($defaultId != null) && ($objItem->getPollSelectId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
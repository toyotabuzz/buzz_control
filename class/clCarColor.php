<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_car_color table

*********************************************************/
 
class CarColor extends DB{

	var $TABLE="t_car_color";

	var $mCarColorId;
	function getCarColorId() { return $this->mCarColorId; }
	function setCarColorId($data) { $this->mCarColorId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }	
	
	function CarColor($objData=NULL) {
        If ($objData->car_color_id !="") {
            $this->setCarColorId($objData->car_color_id);
			$this->setTitle($objData->title);
			$this->setCode($objData->code);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mCarColorId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_color_id =".$this->mCarColorId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarColor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mCarColorId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE car_color_id =".$this->mCarColorId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarColor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CarColor($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title , code ) "
						." VALUES ( '".$this->mTitle."' ,  '".$this->mCode."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mCarColorId = mysql_insert_id();
            return $this->mCarColorId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' ,  "
						."  code = '".$this->mCode."'  "
						." WHERE  car_color_id = ".$this->mCarColorId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE car_color_id=".$this->mCarColorId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class CarColorList extends DataList {
	var $TABLE = "t_car_color";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT car_color_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CarColor($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getCarColorId()."\"");
			if (($defaultId != null) && ($objItem->getCarColorId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
<?
/*********************************************************
		Class :					Place

		Last update :	  10 Jan 02

		Description:	  Class manage t_place table

*********************************************************/
 
class StockPlace extends DB{

	var $TABLE="t_stock_place";

	var $mStockPlaceId;
	function getStockPlaceId() { return $this->mStockPlaceId; }
	function setStockPlaceId($data) { $this->mStockPlaceId = $data; }
	
	var $mStockCargoId;
	function getStockCargoId() { return $this->mStockCargoId; }
	function setStockCargoId($data) { $this->mStockCargoId = $data; }	
	
	var $mCargoTitle;
	function getCargoTitle() { return $this->mCargoTitle; }
	function setCargoTitle($data) { $this->mCargoTitle = $data; }		
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	function StockPlace($objData=NULL) {
        If ($objData->stock_place_id !="") {
            $this->setStockPlaceId($objData->stock_place_id);
			$this->setStockCargoId($objData->stock_cargo_id);
			$this->setCargoTitle($objData->cargo_title);
			$this->setTitle($objData->title);
			$this->setCode($objData->code);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mStockPlaceId == '') {
			return false;
		}
		$strSql = "SELECT SP. * , SC.title AS cargo_title  FROM ".$this->TABLE." SP   "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SP.stock_cargo_id "
		." WHERE stock_place_id =".$this->mStockPlaceId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPlace($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mStockPlaceId == '') {
			return false;
		}
		$strSql = "SELECT SP. * , SC.title AS cargo_title  FROM ".$this->TABLE." SP   "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SP.stock_cargo_id "
		." WHERE stock_place_id =".$this->mStockPlaceId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPlace($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT SP. * , SC.title AS cargo_title  FROM ".$this->TABLE." SP   "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SP.stock_cargo_id "
		." WHERE stock_place_id =".$this->mStockPlaceId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockPlace($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, stock_cargo_id, code ) "
						." VALUES ( '".$this->mTitle."', '".$this->mStockCargoId."', '".$this->mCode."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockPlaceId = mysql_insert_id();
            return $this->mStockPlaceId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						."  code = '".$this->mCode."' ,  "
						." stock_cargo_id = '".$this->mStockCargoId."'  "
						." WHERE  stock_place_id = ".$this->mStockPlaceId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_place_id=".$this->mStockPlaceId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == ""){
			$asrErrReturn["code"] = "��س��к�";
		}else{
			if(!$this->checkUnique($Mode,"code","stock_place_id","t_stock_place",$this->mStockPlaceId,$this->mCode)){
				$asrErrReturn["code"] = '���ʤ�ѧ��ӫ�͹';
			}
		}
        Return $asrErrReturn;
    }
	
   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class StockPlaceList extends DataList {
	var $TABLE = "t_stock_place";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_place_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT SP. * , SC.title AS cargo_title  FROM ".$this->TABLE." SP   "
		." LEFT JOIN t_stock_cargo SC ON SC.stock_cargo_id = SP.stock_cargo_id "
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockPlace($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockPlaceId()."\"");
			if (($defaultId != null) && ($objItem->getStockPlaceId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCargoTitle()." > ".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockPlaceId()."\"");
			if (($defaultId != null) && ($objItem->getStockPlaceId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getCargoDetail()." > ".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
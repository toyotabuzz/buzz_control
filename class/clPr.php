<?
/*********************************************************
		Class :				PR

		Last update :		21 May 05

		Description:		Class manage PO_Order table

*********************************************************/
 
class PR extends DB{

	var $TABLE="t_pr";
	
	var $mPrId;
	function getPrId() { return $this->mPrId; }
	function setPrId($data) { $this->mPrId = $data; }

	var $mMemberId;
	function getMemberId() { return $this->mMemberId;}
	function setMemberId($data) { $this->mMemberId = $data; }
	
	var $mDepartmentId;
	function getDepartmentId() { return $this->mDepartmentId;}
	function setDepartmentId($data) { $this->mDepartmentId = $data; }	

	var $mPrNumber;
	function getPrNumber() { return $this->mPrNumber;}
	function setPrNumber($data) { $this->mPrNumber = $data; }
	
	var $mPrDate;
	function getPrDate() { return htmlspecialchars($this->mPrDate); }
	function setPrDate($data) { $this->mPrDate = $data; }

	var $mDeliveryDate;
	function getDeliveryDate() { return htmlspecialchars($this->mDeliveryDate); }
	function setDeliveryDate($data) { $this->mDeliveryDate = $data; }

	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }
	
	function displayStatus(){
		if ($this->mStatus == 0){return "�͡��͹��ѵԨҡ���˹��Ἱ�";}
		if ($this->mStatus == 1){return "�͡�õ�Ǩ�ͺ�ҡ�Ѵ����";}
		if ($this->mStatus == 4){return "�͡�õ�Ǩ�ͺ�ҡ�ѭ��";}
		if ($this->mStatus == 2){return "�ͷ����觫���";}
		if ($this->mStatus == 3){return "�����觫��ͤú����";}
	}	
	
	var $mRemark;
	function getRemark() { return $this->mRemark;}
	function setRemark($data) { $this->mRemark = $data; }	
	
	var $mDepartmentDetail;
	function getDepartmentDetail() { return htmlspecialchars($this->mDepartmentDetail); }
	function setDepartmentDetail($data) { $this->mDepartmentDetail = $data; }
	
	var $mPosition;
	function getPosition() { return htmlspecialchars($this->mPosition); }
	function setPosition($data) { $this->mPosition = $data; }

	function getPositionDetail() {
		if($this->mPosition == 1) return "������ü��Ѵ���"; 
		if($this->mPosition == 2) return "�ͧ������ü��Ѵ���"; 
		if($this->mPosition == 3) return "���Ѵ���"; 
		if($this->mPosition == 4) return "�����¼��Ѵ���"; 
		if($this->mPosition == 5) return "���˹�ҽ���"; 
		if($this->mPosition == 6) return "���˹��Ἱ�"; 
		if($this->mPosition == 7) return "��ѡ�ҹ"; 

	}	
	
	var $mApprove;
	// ���͹��ѵԨҡ�ҧ���ǡ�
	function getApprove() { return $this->mApprove;}
	function setApprove($data) { $this->mApprove = $data; }	
	
	var $mApproveBy;
	function getApproveBy() { return $this->mApproveBy;}
	function setApproveBy($data) { $this->mApproveBy = $data; }	
	
	var $mApproveDate;
	function getApproveDate() { return $this->mApproveDate;}
	function setApproveDate($data) { $this->mApproveDate = $data; }	
	
	var $mOrderId;
	function getOrderId() { return htmlspecialchars($this->mOrderId); }
	function setOrderId($data) { $this->mOrderId = $data; }
	
	var $mStockCarId;
	function getStockCarId() { return htmlspecialchars($this->mStockCarId); }
	function setStockCarId($data) { $this->mStockCarId = $data; }
	
	var $mBpNumber;
	function getBpNumber() { return htmlspecialchars($this->mBpNumber); }
	function setBpNumber($data) { $this->mBpNumber = $data; }		
	
	var $mGsNumber;
	function getGsNumber() { return htmlspecialchars($this->mGsNumber); }
	function setGsNumber($data) { $this->mGsNumber = $data; }		
	
	var $mFixNumber;
	function getFixNumber() { return htmlspecialchars($this->mFixNumber); }
	function setFixNumber($data) { $this->mFixNumber = $data; }	
	
	var $mCompanyId;
	function getCompanyId() { return htmlspecialchars($this->mCompanyId); }
	function setCompanyId($data) { $this->mCompanyId = $data; }		
	
	var $mAccountRemark;
	function getAccountRemark() { return htmlspecialchars($this->mAccountRemark); }
	function setAccountRemark($data) { $this->mAccountRemark = $data; }		
	
	function PR($objData=NULL) {
        If ($objData->pr_id!="") {
            $this->setPrId($objData->pr_id);
            $this->setMemberId($objData->member_id);
			$this->setDepartmentId($objData->department_id);
			
			$this->setPrNumber($objData->pr_number);
            $this->setPrDate($objData->pr_date);
			$this->setDeliveryDate($objData->delivery_date);
			
			$this->setDepartmentDetail($objData->department_detail);
			$this->setPosition($objData->position);

			$this->setStatus($objData->status);
			$this->setRemark($objData->remark);
			
			$this->setApprove($objData->approve);
			$this->setApproveBy($objData->approve_by);
			$this->setApproveDate($objData->approve_date);
			$this->setStockCarId($objData->stock_car_id);
			$this->setOrderId($objData->order_id);
			$this->setBpNumber($objData->bp_number);
			$this->setGsNumber($objData->gs_number);
			$this->setFixNumber($objData->fix_number);
			$this->setCompanyId($objData->company_id);
			$this->setAccountRemark($objData->account_remark);
        }
    }

	function init(){	
		$this->setRemark(stripslashes($this->mRemark));
	}
	
	function load() {

		if ($this->mPrId == '') {
			return false;
		}
		$strSql = "SELECT O.*, M.firstname, M.lastname, department, position, title AS department_detail  FROM ".$this->TABLE." O "
					." LEFT JOIN t_member M ON M.member_id = O.member_id "
					." LEFT JOIN t_department D ON M.department = D.department_id "
					." WHERE pr_id = ".$this->mPrId;
		$this->getConnection();

        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PR($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		global $sCompanyId;
		$strSql = "INSERT INTO ".$this->TABLE
						." ( member_id, company_id, department_id, pr_number, pr_date, delivery_date, status, remark, order_id, stock_car_id, approve, approve_by, approve_date, bp_number, gs_number, fix_number ) "
						." VALUES ( '".$this->mMemberId."' , "
						."  '".$sCompanyId."' , "
						."  '".$this->mDepartmentId."' , "
						."  '".$this->mPrNumber."' , "
						."  '".$this->mPrDate."' , "
						."  '".$this->mDeliveryDate."' , "
						."  '".$this->mStatus."' , "
						."  '".$this->mRemark."' , "
						."  '".$this->mOrderId."' , "
						."  '".$this->mStockCarId."' , "
						."  '".$this->mApprove."' , "
						."  '".$this->mApproveBy."' , "
						."  '".date("Y-m-d H:i:s")."' , "
						."  '".$this->mBpNumber."' , "
						."  '".$this->mGsNumber."' , "
						."  '".$this->mFixNumber."' ) ";
        $this->getConnection();

		//echo $strSql;
		
        If ($result = $this->query($strSql)) { 
            $this->mPrId = mysql_insert_id();
            return $this->mPrId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET member_id = '".$this->mMemberId."'  "
						." ,  pr_number = '".$this->mPrNumber."'  "
						." ,  pr_date = '".$this->mPrDate."'  "
						." ,  delivery_date = '".$this->mDeliveryDate."'  "
						." ,  status = '".$this->mStatus."'  "
						." ,  remark = '".$this->mRemark."'  "
						." ,  order_id = '".$this->mOrderId."'  "
						." ,  stock_car_id = '".$this->mStockCarId."'  "
						." ,  bp_number = '".$this->mBpNumber."'  "
						." ,  gs_number = '".$this->mGsNumber."'  "
						." ,  fix_number = '".$this->mFixNumber."'  "
						." WHERE  pr_id = ".$this->mPrId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}

	function updateStatus($value){
		$strSql = "UPDATE ".$this->TABLE
						." SET  status = '".$value."'  "
						." WHERE  pr_id = ".$this->mPrId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}
	
	function updateApprove(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  approve = '".$this->mApprove."'   "
						." ,  approve_by = '".$this->mApproveBy."'  "
						." ,  approve_date = '".date("Y-m-d H:i:s")."'  "
						." WHERE  pr_id = ".$this->mPrId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}
	
	function updateApproveAccount(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  status = '".$this->mStatus."'   "
						." ,  account_remark = '".$this->mAccountRemark."'  "
						." WHERE  pr_id = ".$this->mPrId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);
	}	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE pr_id=".$this->mPrId." ";
				
        $this->getConnection();
        $this->query($strSql);
		$this->deleteOrderItem();
		//echo $strSql;
	}

	 Function check($strMode)
    {
		global $langUserError;
		$asrErrReturn=array();
        Switch ( strtolower($strMode) )
        {
            Case "update":
            Case "add":
				
				if ($this->mMemberId == "") $asrErrReturn["member_id"] = "��س��кؼ����觫���";
				
				if ($this->mPrNumber == "") {
					$asrErrReturn["pr_number"] = '��س��к��Ţ���㺢ͫ���';
				}else{
					if(!$this->checkUniquePrNumber(strtolower($strMode) )){
						$asrErrReturn["pr_number"] = '�Ţ���㺢ͫ��ͫ�ӫ�͹';
					}		
				}				
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }
	
	 Function checkApprove($Mode)
    {
		global $langUserError;
		$asrErrReturn=array();
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }	
	
	
	function deleteOrderItem(){
       $strSql = " DELETE FROM t_pr_item "
                . " WHERE pr_id =".$this->mPrId." ";
        $this->getConnection();
        $this->query($strSql);
	}
	
    function checkUniquePrNumber($strMode)
    {
        $strSql  = "SELECT pr_number FROM t_pr ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE pr_number = ".trim(strtoupper($this->convStr4SQL($this->mPrNumber)));
		}else{
			$strSql .= " WHERE pr_number = ".trim(strtoupper($this->convStr4SQL($this->mPrNumber)))." and pr_id != ".$this->mPrId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
	
}

/*********************************************************
		Class :				PRList

		Last update :		21 May 05

		Description:	Class manage pr  list

*********************************************************/


class PRList extends DataList {
	var $TABLE = "t_pr";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pr_id) as rowCount FROM ".$this->TABLE." O  "
		." LEFT JOIN t_member M ON M.member_id = O.member_id "
		." LEFT JOIN t_department D ON M.department = D.department_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT O.*, M.firstname, M.lastname, department, position, title AS department_detail  FROM ".$this->TABLE." O "
					." LEFT JOIN t_member M ON M.member_id = O.member_id "
					." LEFT JOIN t_department D ON M.department = D.department_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PR($row);
			}
			return true;
		} else {
			return false;
		}
    }
	
	function loadList() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pr_id) as rowCount FROM ".$this->TABLE." O  "
		." LEFT JOIN t_member M ON M.member_id = O.member_id "
		." LEFT JOIN t_department D ON M.department = D.department_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_order R ON R.order_id = SC.order_id "
		." LEFT JOIN t_stock_red SR ON SR.stock_red_id = R.stock_red_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT O.*, M.firstname, M.lastname, department, position, title AS department_detail  FROM ".$this->TABLE." O "
		." LEFT JOIN t_member M ON M.member_id = O.member_id "
		." LEFT JOIN t_department D ON M.department = D.department_id "
		." LEFT JOIN t_stock_car SC ON SC.stock_car_id = O.stock_car_id "
		." LEFT JOIN t_order R ON R.order_id = SC.order_id "
		." LEFT JOIN t_stock_red SR ON SR.stock_red_id = R.stock_red_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PR($row);
			}
			return true;
		} else {
			return false;
		}
    }

}

/*********************************************************
		Class :				PRItem

		Last update :		21 May 05

		Description:		Class manage pr_item 

*********************************************************/
 
class PRItem extends DB{

	var $TABLE="t_pr_item";
	
	var $mPrItemId;
	function getPrItemId() { return $this->mPrItemId; }
	function setPrItemId($data) { $this->mPrItemId = $data; }

	var $mPrId;
	function getPrId() { return $this->mPrId; }
	function setPrId($data) { $this->mPrId = $data; }
	
	var $mPoId;
	function getPoId() { return $this->mPoId; }
	function setPoId($data) { $this->mPoId = $data; }	
	
	var $mOrderStockId;
	function getOrderStockId() { return $this->mOrderStockId; }
	function setOrderStockId($data) { $this->mOrderStockId = $data; }	

	var $mStockProductId;
	function getStockProductId() { return $this->mStockProductId;}
	function setStockProductId($data) { $this->mStockProductId = $data; }
	
	var $mStockProductDetail;
	function getStockProductDetail() { return $this->mStockProductDetail;}
	function setStockProductDetail($data) { $this->mStockProductDetail = $data; }

	var $mStockCarIds;
	function getStockCarIds() { return $this->mStockCarIds;}
	function setStockCarIds($data) { $this->mStockCarIds = $data; }	
	
	var $mDealerId;
	function getDealerId() { return $this->mDealerId;}
	function setDealerId($data) { $this->mDealerId = $data; }
	
	var $mDealerDetail;
	function getDealerDetail() { return $this->mDealerDetail;}
	function setDealerDetail($data) { $this->mDealerDetail = $data; }
	
	var $mQty;
	function getQty() { return $this->mQty;}
	function setQty($data) { $this->mQty = $data; }

	var $mPrice;
	function getPrice() { return $this->mPrice;}
	function setPrice($data) { $this->mPrice = $data; }
	
	var $mNewPrice;
	function getNewPrice() { return $this->mNewPrice;}
	function setNewPrice($data) { $this->mNewPrice = $data; }	
	
	var $mRemark;
	function getRemark() { return htmlspecialchars($this->mRemark); }
	function setRemark($data) { $this->mRemark = $data; }

	var $mStatus;
	function getStatus() { return htmlspecialchars($this->mStatus); }
	function setStatus($data) { $this->mStatus = $data; }
	
	function displayStatus(){
		if ($this->mStatus == 0){return "�ѧ�����ӡ����觫���";}
		if ($this->mStatus == 1){return "�ӡ����觫�������";}
		if ($this->mStatus == 2){return "��¡���¡�ѧ�����ӡ����觫���";}
	}
	
	
	function PRItem($objData=NULL) {
        If ($objData->pr_item_id!="" or $objData->dealer_id != "") {
            $this->setPrItemId($objData->pr_item_id);
            $this->setPrId($objData->pr_id);
			$this->setPoId($objData->po_id);
			$this->setOrderStockId($objData->order_stock_id);
            $this->setStockProductId($objData->stock_product_id);
			$this->setStockCarIds($objData->stock_car_ids);
			$this->setStockProductDetail($objData->stock_product_detail);
			$this->setDealerId($objData->dealer_id);
			$this->setDealerDetail($objData->dealer_detail);
            $this->setQty($objData->qty);
			$this->setPrice($objData->price);
			$this->setNewPrice($objData->new_price);
			$this->setRemark($objData->remark);
			$this->setStatus($objData->status);
        }
    }
	
	function load() {

		if ($this->mPrItemId == '') {
			return false;
		}
		$strSql = "SELECT P.*, D.title AS dealer_detail, SP.title AS stock_product_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." WHERE pr_item_id = ".$this->mPrItemId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PRItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		$strSql = "SELECT P.*, D.title AS dealer_detail, SP.title AS stock_product_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PRItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( pr_id, stock_product_id, order_stock_id, stock_car_ids, dealer_id, qty, price, new_price, remark ) "
						." VALUES ( '".$this->mPrId."' , "
						."  '".$this->mStockProductId."' , "
						."  '".$this->mOrderStockId."' , "
						."  '".$this->mStockCarIds."' , "
						."  '".$this->mDealerId."' , "
						."  '".$this->mQty."' , "
						."  '".$this->mPrice."' , "
						."  '".$this->mNewPrice."' , "
						."  '".$this->mRemark."' )";

        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPrItemId = mysql_insert_id();
            return $this->mPrItemId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  status = '".$this->mStatus."'  "
						." ,  pr_id = '".$this->mPrId."'  "	
						." ,  stock_product_id = '".$this->mStockProductId."'  "	
						." ,  stock_car_ids = '".$this->mStockCardIds."'  "	
						." ,  order_stock_id = '".$this->mOrderStockId."'  "	
						." ,  dealer_id = '".$this->mDealerId."'  "	
						." ,  qty = '".$this->mQty."'  "	
						." ,  price = '".$this->mPrice."'  "	
						." ,  new_price = '".$this->mNewPrice."'  "	
						." ,  remark = '".$this->mRemark."'  "	
						." WHERE  pr_item_id = ".$this->mPrItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
	function updateQty(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  qty = '".$this->mQty."'  "
						." WHERE  pr_item_id = ".$this->mPrItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updatePo(){
		$strSql = "UPDATE ".$this->TABLE
						." SET  status = '".$this->mStatus."'  "
						." ,  po_id = '".$this->mPoId."'  "	
						." ,  stock_product_id = '".$this->mStockProductId."'  "	
						." ,  dealer_id = '".$this->mDealerId."'  "	
						." ,  qty = '".$this->mQty."'  "	
						." ,  price = '".$this->mPrice."'  "	
						." ,  remark = '".$this->mRemark."'  "	
						." WHERE  pr_item_id = ".$this->mPrItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus($value){
		$strSql = "UPDATE ".$this->TABLE
						." SET  status = '".$value."'  "
						." WHERE  pr_item_id = ".$this->mPrItemId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function delete(){
       $strSql = " DELETE FROM t_pr_item "
                . " WHERE pr_item_id=".$this->mPrItemId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	
}

/*********************************************************
		Class :				PRItemList

		Last update :		21 May 05

		Description:		Class manage PRItemlist

*********************************************************/

class PRItemList extends DataList {
	var $TABLE = "t_pr_item";	

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pr_item_id) as rowCount FROM ".$this->TABLE
			." P  LEFT JOIN t_pr PR ON PR.pr_id = P.pr_id  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT P.*, D.title AS dealer_detail, SP.title AS stock_product_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." LEFT JOIN t_pr PR ON PR.pr_id = P.pr_id  "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PRItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadList() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pr_item_id) as rowCount FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." LEFT JOIN t_pr PR ON PR.pr_id = P.pr_id  "
			." LEFT JOIN t_stock_car SC ON SC.stock_car_id = PR.stock_car_id "
			." LEFT JOIN t_order R ON R.order_id = SC.order_id "
			
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT P.*, D.title AS dealer_detail, SP.title AS stock_product_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." LEFT JOIN t_pr PR ON PR.pr_id = P.pr_id  "
			." LEFT JOIN t_stock_car SC ON SC.stock_car_id = PR.stock_car_id "
			." LEFT JOIN t_order R ON R.order_id = SC.order_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PRItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadPurchase() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT pr_item_id) as rowCount FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." LEFT JOIN t_po PO ON PO.po_id = P.po_id  "
			." LEFT JOIN t_stock_car SC ON P.stock_car_ids = SC.stock_car_id "
			
			.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT P.*, D.title AS dealer_detail, SP.title AS stock_product_detail  FROM ".$this->TABLE." P "
			." LEFT JOIN t_dealer D ON D.dealer_id = P.dealer_id  "
			." LEFT JOIN t_stock_product SP ON SP.stock_product_id = P.stock_product_id  "
			." LEFT JOIN t_po PO ON PO.po_id = P.po_id  "
			." LEFT JOIN t_stock_car SC ON P.stock_car_ids = SC.stock_car_id "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PRItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadDealer() {
		global $sCompanyId;
		$strSql = "SELECT P.dealer_id FROM ".$this->TABLE." P "
			." LEFT JOIN t_pr PR ON PR.pr_id = P.pr_id  "
			." WHERE PR.status=2 AND PR.approve=1 AND PR.company_id=$sCompanyId AND P.po_id = 0 "
			.' GROUP BY dealer_id LIMIT 0 , 100 ';	// ORDER BY clause
	
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PRItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
}
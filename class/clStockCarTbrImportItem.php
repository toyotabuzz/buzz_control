<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class StockCarTbrImportItem extends DB{

	var $TABLE="t_stock_car_tbr_import_item";

	var $m_stock_car_tbr_import_item_id;
	function get_stock_car_tbr_import_item_id() { return $this->m_stock_car_tbr_import_item_id; }
	function set_stock_car_tbr_import_item_id($data) { $this->m_stock_car_tbr_import_item_id = $data; }
	
	var $m_stock_car_tbr_import_id;
	function get_stock_car_tbr_import_id() { return $this->m_stock_car_tbr_import_id; }
	function set_stock_car_tbr_import_id($data) { $this->m_stock_car_tbr_import_id = $data; }
	
	var $m_no;
	function get_no() { return $this->m_no; }
	function set_no($data) { $this->m_no = $data; }
	
	var $m_invoice_date;
	function get_invoice_date() { return $this->m_invoice_date; }
	function set_invoice_date($data) { $this->m_invoice_date = $data; }
	
	var $m_invoice_number;
	function get_invoice_number() { return $this->m_invoice_number; }
	function set_invoice_number($data) { $this->m_invoice_number = $data; }
	
	var $m_invoice_due_date;
	function get_invoice_due_date() { return $this->m_invoice_due_date; }
	function set_invoice_due_date($data) { $this->m_invoice_due_date = $data; }
	
	var $m_model;
	function get_model() { return $this->m_model; }
	function set_model($data) { $this->m_model = $data; }
	
	var $m_color;
	function get_color() { return $this->m_color; }
	function set_color($data) { $this->m_color = $data; }
	
	var $m_color_name;
	function get_color_name() { return $this->m_color_name; }
	function set_color_name($data) { $this->m_color_name = $data; }
	
	var $m_vin_no;
	function get_vin_no() { return $this->m_vin_no; }
	function set_vin_no($data) { $this->m_vin_no = $data; }
	
	var $m_engine_no;
	function get_engine_no() { return $this->m_engine_no; }
	function set_engine_no($data) { $this->m_engine_no = $data; }
	
	var $m_production_year;
	function get_production_year() { return $this->m_production_year; }
	function set_production_year($data) { $this->m_production_year = $data; }
	
	var $m_payment_term_code;
	function get_payment_term_code() { return $this->m_payment_term_code; }
	function set_payment_term_code($data) { $this->m_payment_term_code = $data; }
	
	var $m_tmt_departure_date;
	function get_tmt_departure_date() { return $this->m_tmt_departure_date; }
	function set_tmt_departure_date($data) { $this->m_tmt_departure_date = $data; }

	function StockCarTbrImportItem($objData=NULL) {
        If ($objData->stock_car_tbr_import_item_id !="") {
		$this->set_stock_car_tbr_import_item_id($objData->stock_car_tbr_import_item_id);
		$this->set_stock_car_tbr_import_id($objData->stock_car_tbr_import_id);
		$this->set_no($objData->no);
		$this->set_invoice_date($objData->invoice_date);
		$this->set_invoice_number($objData->invoice_number);
		$this->set_invoice_due_date($objData->invoice_due_date);
		$this->set_model($objData->model);
		$this->set_color($objData->color);
		$this->set_color_name($objData->color_name);
		$this->set_vin_no($objData->vin_no);
		$this->set_engine_no($objData->engine_no);
		$this->set_production_year($objData->production_year);
		$this->set_payment_term_code($objData->payment_term_code);
		$this->set_tmt_departure_date($objData->tmt_departure_date);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_stock_car_tbr_import_item_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_car_tbr_import_item_id =".$this->m_stock_car_tbr_import_item_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarTbrImportItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCarTbrImportItem($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( stock_car_tbr_import_id , no , invoice_date , invoice_number , invoice_due_date , model , color , color_name , vin_no , engine_no , production_year , payment_term_code , tmt_departure_date ) " ." VALUES ( "
		." '".$this->m_stock_car_tbr_import_id."' , "
		." '".$this->m_no."' , "
		." '".$this->m_invoice_date."' , "
		." '".$this->m_invoice_number."' , "
		." '".$this->m_invoice_due_date."' , "
		." '".$this->m_model."' , "
		." '".$this->m_color."' , "
		." '".$this->m_color_name."' , "
		." '".$this->m_vin_no."' , "
		." '".$this->m_engine_no."' , "
		." '".$this->m_production_year."' , "
		." '".$this->m_payment_term_code."' , "
		." '".$this->m_tmt_departure_date."' "		
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_stock_car_tbr_import_item_id = mysql_insert_id();
            return $this->m_stock_car_tbr_import_item_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." stock_car_tbr_import_id = '".$this->m_stock_car_tbr_import_id."' "
		." , no = '".$this->m_no."' "
		." , invoice_date = '".$this->m_invoice_date."' "
		." , invoice_number = '".$this->m_invoice_number."' "
		." , invoice_due_date = '".$this->m_invoice_due_date."' "
		." , model = '".$this->m_model."' "
		." , color = '".$this->m_color."' "
		." , color_name = '".$this->m_color_name."' "
		." , vin_no = '".$this->m_vin_no."' "
		." , engine_no = '".$this->m_engine_no."' "
		." , production_year = '".$this->m_production_year."' "
		." , payment_term_code = '".$this->m_payment_term_code."' "
		." , tmt_departure_date = '".$this->m_tmt_departure_date."' "
		." WHERE stock_car_tbr_import_item_id = ".$this->m_stock_car_tbr_import_item_id." "; 
		//echo $strSql;
        $this->getConnection();		
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}
	
	function update_no(){
		$strSql = "UPDATE ".$this->TABLE." SET  no = '".$this->m_no."' "
		." WHERE stock_car_tbr_import_item_id = ".$this->m_stock_car_tbr_import_item_id." "; 

        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}	

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_car_tbr_import_item_id=".$this->m_stock_car_tbr_import_item_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class StockCarTbrImportItemList extends DataList {
	var $TABLE = "t_stock_car_tbr_import_item";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_car_tbr_import_item_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_stock_car_tbr_import ST ON ST.stock_car_tbr_import_id = P.stock_car_tbr_import_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_stock_car_tbr_import ST ON ST.stock_car_tbr_import_id = P.stock_car_tbr_import_id "		
		.$this->getFilterSQL()	// WHERE clause
		.' '.$this->getSortSQL()	// ORDER BY clause
		.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCarTbrImportItem($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	
}
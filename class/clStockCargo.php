<?
/*********************************************************
		Class :					Cargo

		Last update :	  10 Jan 02

		Description:	  Class manage t_cargo table

*********************************************************/
 
class StockCargo extends DB{

	var $TABLE="t_stock_cargo";

	var $mStockCargoId;
	function getStockCargoId() { return $this->mStockCargoId; }
	function setStockCargoId($data) { $this->mStockCargoId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }

	var $mCode;
	function getCode() { return htmlspecialchars($this->mCode); }
	function setCode($data) { $this->mCode = $data; }
	
	var $mCompanyId;
	function getCompanyId() { return htmlspecialchars($this->mCompanyId); }
	function setCompanyId($data) { $this->mCompanyId = $data; }	
	
	function StockCargo($objData=NULL) {
        If ($objData->stock_cargo_id !="") {
            $this->setStockCargoId($objData->stock_cargo_id);
			$this->setTitle($objData->title);
			$this->setCode($objData->code);
			$this->setCompanyId($objData->company_id);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mStockCargoId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE stock_cargo_id =".$this->mStockCargoId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCargo($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->StockCargo($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title, code, company_id ) "
						." VALUES ( '".$this->mTitle."', '".$this->mCode."', '".$this->mCompanyId."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mStockCargoId = mysql_insert_id();
            return $this->mStockCargoId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."' , "
						." code = '".$this->mCode."' , "
						." company_id= '".$this->mCompanyId."'  "
						." WHERE  stock_cargo_id = ".$this->mStockCargoId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE stock_cargo_id=".$this->mStockCargoId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к�";
		if ($this->mCode == ""){$asrErrReturn["code"] = "��س��к�";}else{
			if(!$this->checkUniqueCode($Mode)){
				$asrErrReturn["code"] = '���ʤ�ѧ��ӫ�͹';
			}		
		}
        Return $asrErrReturn;
    }
	
    function checkUniqueCode($strMode)
    {
        $strSql  = "SELECT code FROM t_stock_cargo ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE code = " .$this->convStr4SQL($this->mCode);
		}else{
			$strSql .= " WHERE code = " .$this->convStr4SQL($this->mCode)." and stock_cargo_id != ".$this->mStockCargoId;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				Payment Subject List

		Last update :		22 Mar 02

		Description:		Payment Subject List

*********************************************************/

class StockCargoList extends DataList {
	var $TABLE = "t_stock_cargo";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT stock_cargo_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockCargo($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");	
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockCargoId()."\"");
			if (($defaultId != null) && ($objItem->getStockCargoId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
	function printSelectScript($name, $defaultId = null ,$header = null,$script = null) {
		echo ("<select class=\"field\" name=\"".$name."\" onchange=\"".$script."\" >\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getStockCargoId()."\"");
			if (($defaultId != null) && ($objItem->getStockCargoId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}	
	
	
	
}
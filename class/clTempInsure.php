<?
/*********************************************************
		Class :					TempInsure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class TempInsure extends DB{

	var $TABLE="t_temp_insure";

var $m_link_id;
function get_link_id() { return $this->m_link_id; }
function set_link_id($data) { $this->m_link_id = $data; }	
	
var $m_insure_id;
function get_insure_id() { return $this->m_insure_id; }
function set_insure_id($data) { $this->m_insure_id = $data; }

var $m_sale_id;
function get_sale_id() { return $this->m_sale_id; }
function set_sale_id($data) { $this->m_sale_id = $data; }

var $m_team_id;
function get_team_id() { return $this->m_team_id; }
function set_team_id($data) { $this->m_team_id = $data; }

var $m_car_id;
function get_car_id() { return $this->m_car_id; }
function set_car_id($data) { $this->m_car_id = $data; }

var $m_customer_id;
function get_customer_id() { return $this->m_customer_id; }
function set_customer_id($data) { $this->m_customer_id = $data; }

var $m_company_id;
function get_company_id() { return $this->m_company_id; }
function set_company_id($data) { $this->m_company_id = $data; }

var $m_quotation_id;
function get_quotation_id() { return $this->m_quotation_id; }
function set_quotation_id($data) { $this->m_quotation_id = $data; }

var $m_quotation_item_id;
function get_quotation_item_id() { return $this->m_quotation_item_id; }
function set_quotation_item_id($data) { $this->m_quotation_item_id = $data; }

var $m_year_extend;
function get_year_extend() { return $this->m_year_extend; }
function set_year_extend($data) { $this->m_year_extend = $data; }

var $m_date_protect;
function get_date_protect() { return $this->m_date_protect; }
function set_date_protect($data) { $this->m_date_protect = $data; }

var $m_k_check;
function get_k_check() { return $this->m_k_check; }
function set_k_check($data) { $this->m_k_check = $data; }

var $m_k_broker_id;
function get_k_broker_id() { return $this->m_k_broker_id; }
function set_k_broker_id($data) { $this->m_k_broker_id = $data; }

var $m_k_prb_id;
function get_k_prb_id() { return $this->m_k_prb_id; }
function set_k_prb_id($data) { $this->m_k_prb_id = $data; }

var $m_k_stock_id;
function get_k_stock_id() { return $this->m_k_stock_id; }
function set_k_stock_id($data) { $this->m_k_stock_id = $data; }

var $m_k_number;
function get_k_number() { return $this->m_k_number; }
function set_k_number($data) { $this->m_k_number = $data; }

var $m_k_start_date;
function get_k_start_date() { return $this->m_k_start_date; }
function set_k_start_date($data) { $this->m_k_start_date = $data; }

var $m_k_end_date;
function get_k_end_date() { return $this->m_k_end_date; }
function set_k_end_date($data) { $this->m_k_end_date = $data; }

var $m_k_frees;
function get_k_frees() { return $this->m_k_frees; }
function get_k_frees_text() { 
	if($this->m_k_frees == 0) return  "�١��ҫ���";
	if($this->m_k_frees == 1) return  "Dealer ��";
	if($this->m_k_frees == 2) return  "�ṹ����";
	if($this->m_k_frees == 3) return  "��໭��µ��";
}

function set_k_frees($data) { $this->m_k_frees = $data; }

var $m_p_check;
function get_p_check() { return $this->m_p_check; }
function set_p_check($data) { $this->m_p_check = $data; }

var $m_p_broker_id;
function get_p_broker_id() { return $this->m_p_broker_id; }
function set_p_broker_id($data) { $this->m_p_broker_id = $data; }

var $m_p_prb_id;
function get_p_prb_id() { return $this->m_p_prb_id; }
function set_p_prb_id($data) { $this->m_p_prb_id = $data; }

var $m_p_stock_id;
function get_p_stock_id() { return $this->m_p_stock_id; }
function set_p_stock_id($data) { $this->m_p_stock_id = $data; }

var $m_p_number;
function get_p_number() { return $this->m_p_number; }
function set_p_number($data) { $this->m_p_number = $data; }

var $m_p_start_date;
function get_p_start_date() { return $this->m_p_start_date; }
function set_p_start_date($data) { $this->m_p_start_date = $data; }

var $m_p_end_date;
function get_p_end_date() { return $this->m_p_end_date; }
function set_p_end_date($data) { $this->m_p_end_date = $data; }

var $m_p_frees;
function get_p_frees() { return $this->m_p_frees; }
function get_p_frees_text() { 
	if($this->m_p_frees == 4) return  "�١��ҫ���";
	if($this->m_p_frees == 1) return  "Dealer ��";
	if($this->m_p_frees == 2) return  "TMT ��";
	if($this->m_p_frees == 3) return  "F/N ��";
	if($this->m_p_frees == 5) return  "��Сѹ�����";
}
function set_p_frees($data) { $this->m_p_frees = $data; }

var $m_k_num01;
function get_k_num01() { return $this->m_k_num01; }
function set_k_num01($data) { $this->m_k_num01 = $data; }

var $m_k_num02;
function get_k_num02() { return $this->m_k_num02; }
function set_k_num02($data) { $this->m_k_num02 = $data; }

var $m_k_num03;
function get_k_num03() { return $this->m_k_num03; }
function set_k_num03($data) { $this->m_k_num03 = $data; }

var $m_k_num04;
function get_k_num04() { return $this->m_k_num04; }
function set_k_num04($data) { $this->m_k_num04 = $data; }

var $m_k_num05;
function get_k_num05() { return $this->m_k_num05; }
function set_k_num05($data) { $this->m_k_num05 = $data; }

var $m_k_num06;
function get_k_num06() { return $this->m_k_num06; }
function set_k_num06($data) { $this->m_k_num06 = $data; }

var $m_k_num07;
function get_k_num07() { return $this->m_k_num07; }
function set_k_num07($data) { $this->m_k_num07 = $data; }

var $m_k_num08;
function get_k_num08() { return $this->m_k_num08; }
function set_k_num08($data) { $this->m_k_num08 = $data; }

var $m_k_num09;
function get_k_num09() { return $this->m_k_num09; }
function set_k_num09($data) { $this->m_k_num09 = $data; }

var $m_k_num10;
function get_k_num10() { return $this->m_k_num10; }
function set_k_num10($data) { $this->m_k_num10 = $data; }

var $m_k_num11;
function get_k_num11() { return $this->m_k_num11; }
function set_k_num11($data) { $this->m_k_num11 = $data; }

var $m_k_num12;
function get_k_num12() { return $this->m_k_num12; }
function set_k_num12($data) { $this->m_k_num12 = $data; }

var $m_k_num13;
function get_k_num13() { return $this->m_k_num13; }
function set_k_num13($data) { $this->m_k_num13 = $data; }

var $m_k_niti;
function get_k_niti() { return $this->m_k_niti; }
function set_k_niti($data) { $this->m_k_niti = $data; }

var $m_k_dis_type_01;
function get_k_dis_type_01() { return $this->m_k_dis_type_01; }
function set_k_dis_type_01($data) { $this->m_k_dis_type_01 = $data; }

var $m_k_dis_type_02;
function get_k_dis_type_02() { return $this->m_k_dis_type_02; }
function set_k_dis_type_02($data) { $this->m_k_dis_type_02 = $data; }

var $m_k_dis_type_03;
function get_k_dis_type_03() { return $this->m_k_dis_type_03; }
function set_k_dis_type_03($data) { $this->m_k_dis_type_03 = $data; }

var $m_k_dis_con_01;
function get_k_dis_con_01() { return $this->m_k_dis_con_01; }
function set_k_dis_con_01($data) { $this->m_k_dis_con_01 = $data; }

var $m_k_dis_con_02;
function get_k_dis_con_02() { return $this->m_k_dis_con_02; }
function set_k_dis_con_02($data) { $this->m_k_dis_con_02 = $data; }

var $m_k_free;
function get_k_free() { return $this->m_k_free; }
function set_k_free($data) { $this->m_k_free = $data; }

var $m_k_type;
function get_k_type() { return $this->m_k_type; }
function set_k_type($data) { $this->m_k_type = $data; }

var $m_k_type_remark;
function get_k_type_remark() { return $this->m_k_type_remark; }
function set_k_type_remark($data) { $this->m_k_type_remark = $data; }

var $m_k_fix;
function get_k_fix() { return $this->m_k_fix; }
function set_k_fix($data) { $this->m_k_fix = $data; }

var $m_p_car_type;
function get_p_car_type() { return $this->m_p_car_type; }
function set_p_car_type($data) { $this->m_p_car_type = $data; }

var $m_p_prb_price;
function get_p_prb_price() { return $this->m_p_prb_price; }
function set_p_prb_price($data) { $this->m_p_prb_price = $data; }

var $m_p_year;
function get_p_year() { return $this->m_p_year; }
function set_p_year($data) { $this->m_p_year = $data; }

var $m_p_call_date;
function get_p_call_date() { return $this->m_p_call_date; }
function set_p_call_date($data) { $this->m_p_call_date = $data; }

var $m_p_get_date;
function get_p_get_date() { return $this->m_p_get_date; }
function set_p_get_date($data) { $this->m_p_get_date = $data; }

var $m_p_call_number;
function get_p_call_number() { return $this->m_p_call_number; }
function set_p_call_number($data) { $this->m_p_call_number = $data; }

var $m_p_remark;
function get_p_remark() { return $this->m_p_remark; }
function set_p_remark($data) { $this->m_p_remark = $data; }

var $m_p_insure_vat;
function get_p_insure_vat() { return $this->m_p_insure_vat; }
function set_p_insure_vat($data) { $this->m_p_insure_vat = $data; }

var $m_p_argon;
function get_p_argon() { return $this->m_p_argon; }
function set_p_argon($data) { $this->m_p_argon = $data; }

var $m_p_total;
function get_p_total() { return $this->m_p_total; }
function set_p_total($data) { $this->m_p_total = $data; }

var $m_pay_number;
function get_pay_number() { return $this->m_pay_number; }
function set_pay_number($data) { $this->m_pay_number = $data; }

var $m_pay_type;
function get_pay_type() { return $this->m_pay_type; }
function set_pay_type($data) { $this->m_pay_type = $data; }
	
//booking, ccard, cancel
var $m_status;
function get_status() { return $this->m_status; }
function set_status($data) { $this->m_status = $data; }	

var $m_k_send;
function get_k_send() { return $this->m_k_send; }
function set_k_send($data) { $this->m_k_send = $data; }	

var $m_k_send_date;
function get_k_send_date() { return $this->m_k_send_date; }
function set_k_send_date($data) { $this->m_k_send_date = $data; }	
	
var $m_p_send;
function get_p_send() { return $this->m_p_send; }
function set_p_send($data) { $this->m_p_send = $data; }	

var $m_p_send_date;
function get_p_send_date() { return $this->m_p_send_date; }
function set_p_send_date($data) { $this->m_p_send_date = $data; }		

var $m_prepare;
function get_prepare() { return $this->m_prepare; }
function set_prepare($data) { $this->m_prepare = $data; }	

var $m_prepare_date;
function get_prepare_date() { return $this->m_prepare_date; }
function set_prepare_date($data) { $this->m_prepare_date = $data; }	

var $m_prepare_no;
function get_prepare_no() { return $this->m_prepare_no; }
function set_prepare_no($data) { $this->m_prepare_no = $data; }	

var $m_prepare_by;
function get_prepare_by() { return $this->m_prepare_by; }
function set_prepare_by($data) { $this->m_prepare_by = $data; }	

var $m_direct;
function get_direct() { return $this->m_direct; }
function set_direct($data) { $this->m_direct = $data; }	

var $m_p_acc_check;
function get_p_acc_check() { return $this->m_p_acc_check; }
function set_p_acc_check($data) { $this->m_p_acc_check = $data; }	

var $m_p_acc_com;
function get_p_acc_com() { return $this->m_p_acc_com; }
function set_p_acc_com($data) { $this->m_p_acc_com = $data; }	

var $m_p_acc_remark;
function get_p_acc_remark() { return $this->m_p_acc_remark; }
function set_p_acc_remark($data) { $this->m_p_acc_remark = $data; }	

var $m_p_acc_id;
function get_p_acc_id() { return $this->m_p_acc_id; }
function set_p_acc_id($data) { $this->m_p_acc_id = $data; }	

var $m_k_acc_check;
function get_k_acc_check() { return $this->m_k_acc_check; }
function set_k_acc_check($data) { $this->m_k_acc_check = $data; }	

var $m_k_acc_com;
function get_k_acc_com() { return $this->m_k_acc_com; }
function set_k_acc_com($data) { $this->m_k_acc_com = $data; }	

var $m_k_acc_remark;
function get_k_acc_remark() { return $this->m_k_acc_remark; }
function set_k_acc_remark($data) { $this->m_k_acc_remark = $data; }	

var $m_k_acc_id;
function get_k_acc_id() { return $this->m_k_acc_id; }
function set_k_acc_id($data) { $this->m_k_acc_id = $data; }	
	
var $m_p_rnum;
function get_p_rnum() { return $this->m_p_rnum; }
function set_p_rnum($data) { $this->m_p_rnum = $data; }	

var $m_p_rnum_date;
function get_p_rnum_date() { return $this->m_p_rnum_date; }
function set_p_rnum_date($data) { $this->m_p_rnum_date = $data; }

/*-------------------------------- other ------------------------------------------------------*/

var $m_firstname;
function get_firstname() { return $this->m_firstname; }
function set_firstname($data) { $this->m_firstname = $data; }	

var $m_lastname;
function get_lastname() { return $this->m_lastname; }
function set_lastname($data) { $this->m_lastname = $data; }	

var $m_code;
function get_code() { return $this->m_code; }
function set_code($data) { $this->m_code = $data; }	

var $m_p_short_title;
function get_p_short_title() { return $this->m_p_short_title; }
function set_p_short_title($data) { $this->m_p_short_title = $data; }	

var $m_k_short_title;
function get_k_short_title() { return $this->m_k_short_title; }
function set_k_short_title($data) { $this->m_k_short_title = $data; }	
	
var $m_p_broker_title;
function get_p_broker_title() { return $this->m_p_broker_title; }
function set_p_broker_title($data) { $this->m_p_broker_title = $data; }	

var $m_k_broker_title;
function get_k_broker_title() { return $this->m_k_broker_title; }
function set_k_broker_title($data) { $this->m_k_broker_title = $data; }		

var $m_sale_name;
function get_sale_name() { return $this->m_sale_name; }
function set_sale_name($data) { $this->m_sale_name = $data; }		

var $m_insure_date;
function get_insure_date() { return $this->m_insure_date; }
function set_insure_date($data) { $this->m_insure_date = $data; }		

var $m_k_new;
function get_k_new() { return $this->m_k_new; }
function set_k_new($data) { $this->m_k_new = $data; }		

var $m_officer_id;
function get_officer_id() { return $this->m_officer_id; }
function set_officer_id($data) { $this->m_officer_id = $data; }		

var $m_officer_date;
function get_officer_date() { return $this->m_officer_date; }
function set_officer_date($data) { $this->m_officer_date = $data; }		

var $m_k_status;
function get_k_status() { return $this->m_k_status; }
function set_k_status($data) { $this->m_k_status = $data; }		

var $m_k_car_type;
function get_k_car_type() { return $this->m_k_car_type; }
function set_k_car_type($data) { $this->m_k_car_type = $data; }		

var $m_p_vat;
function get_p_vat() { return $this->m_p_vat; }
function set_p_vat($data) { $this->m_p_vat = $data; }		

var $m_doc01;
function get_doc01() { return $this->m_doc01; }
function set_doc01($data) { $this->m_doc01 = $data; }		

var $m_doc02;
function get_doc02() { return $this->m_doc02; }
function set_doc02($data) { $this->m_doc02 = $data; }		

var $m_doc03;
function get_doc03() { return $this->m_doc03; }
function set_doc03($data) { $this->m_doc03 = $data; }		

var $m_doc04;
function get_doc04() { return $this->m_doc04; }
function set_doc04($data) { $this->m_doc04 = $data; }		

var $m_link_date;
function get_link_date() { return $this->m_link_date; }
function set_link_date($data) { $this->m_link_date = $data; }		


/*-------------------------------- other ------------------------------------------------------*/
	
	function TempInsure($objData=NULL) {
        If ($objData->insure_id !="") {
			$this->set_link_id($objData->link_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_sale_id($objData->sale_id);
			$this->set_team_id($objData->team_id);
			$this->set_car_id($objData->car_id);
			$this->set_customer_id($objData->customer_id);
			$this->set_company_id($objData->company_id);
			$this->set_quotation_id($objData->quotation_id);
			$this->set_quotation_item_id($objData->quotation_item_id);
			$this->set_year_extend($objData->year_extend);
			$this->set_date_protect($objData->date_protect);
			$this->set_k_check($objData->k_check);
			$this->set_k_broker_id($objData->k_broker_id);
			$this->set_k_prb_id($objData->k_prb_id);
			$this->set_k_stock_id($objData->k_stock_id);
			$this->set_k_number($objData->k_number);
			$this->set_k_start_date($objData->k_start_date);
			$this->set_k_end_date($objData->k_end_date);
			$this->set_k_frees($objData->k_frees);
			$this->set_p_check($objData->p_check);
			$this->set_p_broker_id($objData->p_broker_id);
			$this->set_p_prb_id($objData->p_prb_id);
			$this->set_p_stock_id($objData->p_stock_id);
			$this->set_p_number($objData->p_number);
			$this->set_p_start_date($objData->p_start_date);
			$this->set_p_end_date($objData->p_end_date);
			$this->set_p_frees($objData->p_frees);
			$this->set_k_num01($objData->k_num01);
			$this->set_k_num02($objData->k_num02);
			$this->set_k_num03($objData->k_num03);
			$this->set_k_num04($objData->k_num04);
			$this->set_k_num05($objData->k_num05);
			$this->set_k_num06($objData->k_num06);
			$this->set_k_num07($objData->k_num07);
			$this->set_k_num08($objData->k_num08);
			$this->set_k_num09($objData->k_num09);
			$this->set_k_num10($objData->k_num10);
			$this->set_k_num11($objData->k_num11);
			$this->set_k_num12($objData->k_num12);
			$this->set_k_num13($objData->k_num13);
			$this->set_k_niti($objData->k_niti);
			$this->set_k_dis_type_01($objData->k_dis_type_01);
			$this->set_k_dis_type_02($objData->k_dis_type_02);
			$this->set_k_dis_type_03($objData->k_dis_type_03);
			$this->set_k_dis_con_01($objData->k_dis_con_01);
			$this->set_k_dis_con_02($objData->k_dis_con_02);
			$this->set_k_free($objData->k_free);
			$this->set_k_type($objData->k_type);
			$this->set_k_type_remark($objData->k_type_remark);
			$this->set_k_fix($objData->k_fix);
			$this->set_p_car_type($objData->p_car_type);
			$this->set_p_prb_price($objData->p_prb_price);
			$this->set_p_year($objData->p_year);
			$this->set_p_call_date($objData->p_call_date);
			$this->set_p_get_date($objData->p_get_date);
			$this->set_p_call_number($objData->p_call_number);
			$this->set_p_remark($objData->p_remark);
			$this->set_p_insure_vat($objData->p_insure_vat);
			$this->set_p_argon($objData->p_argon);
			$this->set_p_total($objData->p_total);
			
			$this->set_pay_number($objData->pay_number);
			$this->set_pay_type($objData->pay_type);
			$this->set_status($objData->status);
			$this->set_k_send($objData->k_send);
			$this->set_k_send_date($objData->k_send_date);
			$this->set_p_send($objData->p_send);
			$this->set_p_send_date($objData->p_send_date);
			$this->set_prepare($objData->prepare);
			$this->set_prepare_date($objData->prepare_date);
			$this->set_prepare_no($objData->prepare_no);
			$this->set_prepare_by($objData->prepare_by);
			$this->set_direct($objData->direct);
			
			$this->set_p_acc_check($objData->p_acc_check);
			$this->set_p_acc_com($objData->p_acc_com);
			$this->set_p_acc_remark($objData->p_acc_remark);
			$this->set_p_acc_id($objData->p_acc_id);
			
			$this->set_k_acc_check($objData->k_acc_check);
			$this->set_k_acc_com($objData->k_acc_com);
			$this->set_k_acc_remark($objData->k_acc_remark);
			$this->set_k_acc_id($objData->k_acc_id);
			
			$this->set_p_rnum($objData->p_rnum);
			$this->set_p_rnum_date($objData->p_rnum_date);
			
			
			$this->set_firstname($objData->firstname);
			$this->set_lastname($objData->lastname);
			$this->set_code($objData->code);
			$this->set_p_short_title($objData->p_short_title);
			$this->set_k_short_title($objData->k_short_title);
			$this->set_p_broker_title($objData->p_broker_title);
			$this->set_k_broker_title($objData->k_broker_title);
			$this->set_sale_name($objData->sale_name);
			
			$this->set_insure_date($objData->insure_date);
			$this->set_k_new($objData->k_new);
			$this->set_officer_id($objData->officer_id);
			$this->set_officer_date($objData->officer_date);
			
			$this->set_k_status($objData->k_status);
			$this->set_k_car_type($objData->k_car_type);
			$this->set_p_vat($objData->p_vat);
			
			$this->set_doc01($objData->doc01);
			$this->set_doc02($objData->doc02);
			$this->set_doc03($objData->doc03);
			$this->set_doc04($objData->doc04);
			
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_id =".$this->m_insure_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsure($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsure($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadSumByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT COUNT(insure_id) AS RSUM  FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $rsum = $row->RSUM;
                $result->freeResult();
				return $rsum;
            }
        }
	   $this->unsetConnection();
		return false;
	}

	function loadMax($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MAX(register_year), insure_id FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->TempInsure($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
	
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id, link_date,  sale_id , team_id, car_id , customer_id , company_id,  quotation_id, year_extend , date_protect , k_check, k_broker_id , k_prb_id , k_stock_id , k_number , k_start_date , k_end_date ,  k_frees , p_check, p_broker_id , p_prb_id , p_stock_id , p_number , p_start_date , p_end_date , p_frees , k_num01 , k_num02 , k_num03 , k_num04 , k_num05 , k_num06 , k_num07 , k_num08 , k_num09 , k_num10 , k_num11 , k_num12 , k_num13 , k_niti , k_dis_type_01,  k_dis_type_02,  k_dis_type_03,  k_dis_con_01 , k_dis_con_02 , k_free , k_type , k_type_remark , k_fix , p_car_type , p_prb_price , p_year , p_call_date , p_get_date , p_call_number , p_remark , p_insure_vat, p_argon, p_total, pay_number , pay_type , status, direct, p_rnum, p_rnum_date,  insure_date, k_new , officer_id, officer_date, k_status ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".date("Y-m-d")."' , "
		." '".$this->m_sale_id."' , "
		." '".$this->m_team_id."' , "
		." '".$this->m_car_id."' , "
		." '".$this->m_customer_id."' , "
		." '".$this->m_company_id."' , "
		." '".$this->m_quotation_id."' , "
		." '".$this->m_year_extend."' , "
		." '".$this->m_date_protect."' , "
		." '".$this->m_k_check."' , "
		." '".$this->m_k_broker_id."' , "
		." '".$this->m_k_prb_id."' , "
		." '".$this->m_k_stock_id."' , "
		." '".$this->m_k_number."' , "
		." '".$this->m_k_start_date."' , "
		." '".$this->m_k_end_date."' , "
		." '".$this->m_k_frees."' , "
		." '".$this->m_p_check."' , "
		." '".$this->m_p_broker_id."' , "
		." '".$this->m_p_prb_id."' , "
		." '".$this->m_p_stock_id."' , "
		." '".$this->m_p_number."' , "
		." '".$this->m_p_start_date."' , "
		." '".$this->m_p_end_date."' , "
		." '".$this->m_p_frees."' , "
		." '".$this->m_k_num01."' , "
		." '".$this->m_k_num02."' , "
		." '".$this->m_k_num03."' , "
		." '".$this->m_k_num04."' , "
		." '".$this->m_k_num05."' , "
		." '".$this->m_k_num06."' , "
		." '".$this->m_k_num07."' , "
		." '".$this->m_k_num08."' , "
		." '".$this->m_k_num09."' , "
		." '".$this->m_k_num10."' , "
		." '".$this->m_k_num11."' , "
		." '".$this->m_k_num12."' , "
		." '".$this->m_k_num13."' , "
		." '".$this->m_k_niti."' , "
		." '".$this->m_k_dis_type_01."' , "
		." '".$this->m_k_dis_type_02."' , "
		." '".$this->m_k_dis_type_03."' , "
		." '".$this->m_k_dis_con_01."' , "
		." '".$this->m_k_dis_con_02."' , "
		." '".$this->m_k_free."' , "
		." '".$this->m_k_type."' , "
		." '".$this->m_k_type_remark."' , "
		." '".$this->m_k_fix."' , "
		." '".$this->m_p_car_type."' , "
		." '".$this->m_p_prb_price."' , "
		." '".$this->m_p_year."' , "
		." '".$this->m_p_call_date."' , "
		." '".$this->m_p_get_date."' , "
		." '".$this->m_p_call_number."' , "
		." '".$this->m_p_remark."' , "
		." '".$this->m_p_insure_vat."' , "
		." '".$this->m_p_argon."' , "
		." '".$this->m_p_total."' , "
		." '".$this->m_pay_number."' , "
		." '".$this->m_pay_type."' , "
		." '".$this->m_status."' , "
		." '".$this->m_direct."' , "
		." '".$this->m_p_rnum."' , "
		." '".$this->m_p_rnum_date."' , "
		." '".$this->m_insure_date."' , "
		." '".$this->m_k_new."' , "
		." '".$this->m_officer_id."' , "
		." '".$this->m_officer_date."' , "
		." '".$this->m_k_status."' "
		." ) "; 

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_link_id = mysql_insert_id();
            return $this->m_link_id;
        } else {
			return false;
	    }
		
	}


	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_id=".$this->m_insure_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
	
	function uploadPicture($file=null,$mFileName){
		// --- first part: upload Banner ---
		if ($file != null && $file != "none") {			
			copy($file, PATH_UPLOAD_INS.$mFileName);
		}
	}

	function deletePicture($mFileName){
		if (file_exists(PATH_UPLOAD_INS.$mFileName) And (is_file(PATH_UPLOAD_INS.$mFileName) ) )
	    {
		    unLink(PATH_UPLOAD_INS.$mFileName);
	    }
	}	
	
}

/*********************************************************
		Class :				TempInsure Company List

		Last update :		22 Mar 02

		Description:		TempInsure Company List

*********************************************************/

class TempInsureList extends DataList {
	var $TABLE = "t_temp_insure";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new TempInsure($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }


	
	function loadList() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_id) as rowCount FROM ".$this->TABLE." P  "
			." LEFT JOIN t_insure_car IC ON IC.car_id = P.car_id "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = IC.insure_customer_id "
			." LEFT JOIN t_insure_company CP ON CP.insure_company_id = P.p_prb_id "
			." LEFT JOIN t_insure_company CK ON CK.insure_company_id = P.k_prb_id "
			." LEFT JOIN t_insure_broker BP ON BP.insure_broker_id = P.p_broker_id "
			." LEFT JOIN t_insure_broker BK ON BK.insure_broker_id = P.k_broker_id "
			." LEFT JOIN t_member M ON M.member_id = P.sale_id "

			.$this->getFilterSQL();	// WHERE clause
		//echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.*, C.firstname , C.lastname , IC.code, CP.short_title as p_short_title, CK.short_title as k_short_title, BP.title as p_broker_title , BK.title as k_broker_title, M.firstname as sale_name FROM ".$this->TABLE." P "
			." LEFT JOIN t_insure_car IC ON IC.car_id = P.car_id "
			." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = IC.insure_customer_id "
			." LEFT JOIN t_insure_company CP ON CP.insure_company_id = P.p_prb_id "
			." LEFT JOIN t_insure_company CK ON CK.insure_company_id = P.k_prb_id "
			." LEFT JOIN t_insure_broker BP ON BP.insure_broker_id = P.p_broker_id "
			." LEFT JOIN t_insure_broker BK ON BK.insure_broker_id = P.k_broker_id "
			." LEFT JOIN t_member M ON M.member_id = P.sale_id "
			
			.' '.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING


		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new TempInsure($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
}
<?
/*********************************************************
		Class :				PollPhone

		Last update :		20 Nov 02

		Description:		Class manage PollPhone table

*********************************************************/
 
class PollPhone extends DB{

	var $TABLE="t_poll_phone";
	
	var $mPollPhoneId;
	function getPollPhoneId() { return $this->mPollPhoneId; }
	function setPollPhoneId($data) { $this->mPollPhoneId = $data; }

	var $mTitle;
	function getTitle() { return $this->mTitle; }
	function setTitle($data) { $this->mTitle = $data; }

	function PollPhone($objData=NULL) {
        If ($objData->poll_phone_id!="") {
            $this->setPollPhoneId($objData->poll_phone_id);
			$this->setTitle($objData->title);
        }
    }

	function init(){	
		$this->setTitle(stripslashes($this->mTitle));
	}
	
	function load() {

		if ($this->mPollPhoneId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE
					." WHERE poll_phone_id =".$this->mPollPhoneId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
	            $this->PollPhone($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function loadCount($strCondition) {

		$strSql = "SELECT Count(pollId) as rowCount   FROM ".$this->TABLE
					." WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				return $row->rowCount;
            }
        }
		return false;
	}


	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title )"
						." VALUES (  '".$this->mTitle."' )";
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mPollPhoneId = mysql_insert_id();
            return $this->mPollPhoneId;
        } else {
			return false;
	    }
	}

	function update(){

		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." WHERE  poll_phone_id = ".$this->mPollPhoneId."  ";
        $this->getConnection();
		//echo $strSql;
        return $this->query($strSql);

	}

	function delete() {
        $this->getConnection();
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE poll_phone_id=".$this->mPollPhoneId." ";
        $this->query($strSql);
       $strSql = " DELETE FROM t_poll_back "
                . " WHERE type='phone' and pollId=".$this->mPollPhoneId." ";
        $this->query($strSql);
		
       $strSql = " DELETE FROM t_poll_select "
                . " WHERE type='phone' and event_id=".$this->mPollPhoneId." ";
        $this->query($strSql);
	}

	 Function check($Mode)
    {
		global $langPhoneError;
        $Mode = StrToLower($Mode);

        Switch ( $Mode )
        {
            Case "update":
            Case "add":
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }

}

/*********************************************************
		Class :				PollPhoneList

		Last update :		20 Nov 02

		Description:		PollPhonephone list

*********************************************************/


class PollPhoneList extends DataList {
	var $TABLE = "t_poll_phone";	

	function load() {
		// also gets latest delivery date
        //Get Number of Phones list
        $strSql = "SELECT Count(DISTINCT poll_phone_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new PollPhone($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getPollPhoneId()."\"");
			if (($defaultId != null) && ($objItem->getPollPhoneId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}				
	
}
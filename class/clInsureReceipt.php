<?
/*********************************************************
		Class :			  InsureReceipt

		Last update :	  11 Mar 21

		Description:	  Class manage jminsure_receipt table

*********************************************************/
 
class InsureReceipt extends DB{

	var $TABLE="jminsure_receipt";

	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }

	var $m_insure_item_id;
	function get_insure_item_id() { return $this->m_insure_item_id; }
	function set_insure_item_id($data) { $this->m_insure_item_id = $data; }

    var $m_order_price_id;
	function get_order_price_id() { return $this->m_order_price_id; }
	function set_order_price_id($data) { $this->m_order_price_id = $data; }

    var $m_receipt_doc_type;
	function get_receipt_doc_type() { return $this->m_receipt_doc_type; }
	function set_receipt_doc_type($data) { $this->m_receipt_doc_type = $data; }

    var $m_receipt_no;
	function get_receipt_no() { return $this->m_receipt_no; }
	function set_receipt_no($data) { $this->m_receipt_no = $data; }

    var $m_receipt_date;
	function get_receipt_date() { return $this->m_receipt_date; }
	function set_receipt_date($data) { $this->m_receipt_date = $data; }

    var $m_receipt_amt;
	function get_receipt_amt() { return $this->m_receipt_amt; }
	function set_receipt_amt($data) { $this->m_receipt_amt = $data; }

    var $m_remark;
	function get_remark() { return $this->m_remark; }
	function set_remark($data) { $this->m_remark = $data; }
	
	var $m_isactive;
	function get_isactive() { return $this->m_isactive; }
	function set_isactive($data) { $this->m_isactive = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }
    
    function InsureReceipt($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_insure_id($objData->insure_id);
			$this->set_insure_item_id($objData->insure_item_id);
			$this->set_order_price_id($objData->order_price_id);
			$this->set_receipt_doc_type($objData->receipt_doc_type);
			$this->set_receipt_no($objData->receipt_no);
			$this->set_receipt_date($objData->receipt_date);
            $this->set_receipt_amt($objData->receipt_amt);
            $this->set_remark($objData->remark);
			$this->set_isactive($objData->isactive);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id." ";
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureReceipt($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->InsureReceipt($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( insure_id , insure_item_id , order_price_id , receipt_doc_type, receipt_no , receipt_date, receipt_amt , remark , isactive  , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_insure_id."' , "
		." '".$this->m_insure_item_id."' , "
		." '".$this->m_order_price_id."' , "
		." '".$this->m_receipt_doc_type."' , "
		." '".$this->m_receipt_no."' , "
		." '".$this->m_receipt_date."' , "
		." '".$this->m_receipt_amt."' , "
		." '".$this->m_remark."' , "
		." '".$this->m_isactive."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." insure_id = '".$this->m_insure_id."' "
		." , insure_item_id = '".$this->m_insure_item_id."' "
		." , order_price_id = '".$this->m_order_price_id."' "
		." , receipt_doc_type = '".$this->m_receipt_doc_type."' "
		." , receipt_no = '".$this->m_receipt_no."' "
		." , receipt_date = '".$this->m_receipt_date."' "
		." , receipt_amt = '".$this->m_receipt_amt."' "
		." , remark = '".$this->m_remark."' "
		." , isactive = '".$this->m_isactive."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function updateOrderPayment(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."   order_price_id = '".$this->m_order_price_id."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :			  Insure Company List

		Last update :	  11 Mar 21

		Description:	  Insure Company List

*********************************************************/


class InsureReceiptList extends DataList {
	var $TABLE = "jminsure_receipt";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT Q.id) as rowCount FROM ".$this->TABLE
			." Q  ".$this->getFilterSQL();	// WHERE clause
	   //echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		//echo $strSql;
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureReceipt($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadSum() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT SUM(receipt_amt) as rowCount FROM ".$this->TABLE
			." Q ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT * FROM ".$this->TABLE." Q "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new StockInsureDetail($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

}

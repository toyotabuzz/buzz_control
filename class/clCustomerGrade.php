<?
/*********************************************************
		Class :					Customer Grade

		Last update :	  10 Jan 02

		Description:	  Class manage t_customer_grade table

*********************************************************/
 
class CustomerGrade extends DB{

	var $TABLE="t_customer_grade";

	var $mGradeId;
	function getGradeId() { return $this->mGradeId; }
	function setGradeId($data) { $this->mGradeId = $data; }
	
	var $mTitle;
	function getTitle() { return htmlspecialchars($this->mTitle); }
	function setTitle($data) { $this->mTitle = $data; }
	
	function CustomerGrade($objData=NULL) {
        If ($objData->grade_id !="") {
            $this->setGradeId($objData->grade_id);
			$this->setTitle($objData->title);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mGradeId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE grade_id =".$this->mGradeId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerGrade($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->CustomerGrade($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( title ) "
						." VALUES ( '".$this->mTitle."' ) ";
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mGradeId = mysql_insert_id();
            return $this->mGradeId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET title = '".$this->mTitle."'  "
						." WHERE  grade_id = ".$this->mGradeId."  ";
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE grade_id=".$this->mGradeId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��кت����ô�١���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Customer Grade List

		Last update :		22 Mar 02

		Description:		Customer Grade List

*********************************************************/

class CustomerGradeList extends DataList {
	var $TABLE = "t_customer_grade";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT grade_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new CustomerGrade($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($countryName, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$countryName."\">\n");
		if ($header != "") {echo ("<option value=\"\">- $header -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getGradeId()."\"");
			if (($defaultId != null) && ($objItem->getGradeId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}		
	
	
}
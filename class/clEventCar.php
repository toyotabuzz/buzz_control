<?
/*********************************************************
		Class :					Car Color

		Last update :	  10 Jan 02

		Description:	  Class manage t_event_car table

*********************************************************/
 
class EventCar extends DB{

	var $TABLE="t_event_car";

	var $m_event_car_id;
	function get_event_car_id() { return $this->m_event_car_id; }
	function set_event_car_id($data) { $this->m_event_car_id = $data; }
	
	var $m_event_id;
	function get_event_id() { return $this->m_event_id; }
	function set_event_id($data) { $this->m_event_id = $data; }
	
	var $m_car_model_id;
	function get_car_model_id() { return $this->m_car_model_id; }
	function set_car_model_id($data) { $this->m_car_model_id = $data; }
	
	function EventCar($objData=NULL) {
        If ($objData->event_car_id !="") {
			$this->set_event_car_id($objData->event_car_id);
			$this->set_event_id($objData->event_id);
			$this->set_car_model_id($objData->car_model_id);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->mEventCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE event_car_id =".$this->mEventCarId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadUTF8() {

		if ($this->mEventCarId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE event_car_id =".$this->mEventCarId;
		$this->getConnectionUTF8();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->EventCar($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( event_id , car_model_id ) " ." VALUES ( "
		." '".$this->m_event_id."' , "
		." '".$this->m_car_model_id."' "
		." ) "; 
        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->mEventCarId = mysql_insert_id();
            return $this->mEventCarId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." event_id = '".$this->m_event_id."' "
		." , car_model_id = '".$this->m_car_model_id."' "
		." WHERE event_car_id = ".$this->m_event_car_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;

	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE event_car_id=".$this->mEventCarId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strSql) {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strSql;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
		if ($this->mTitle == "") $asrErrReturn["title"] = "��س��к���";
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Car Color List

		Last update :		22 Mar 02

		Description:		Car Color List

*********************************************************/

class EventCarList extends DataList {
	var $TABLE = "t_event_car";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT event_car_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause

		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new EventCar($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" Name=\"".$name."\">\n");
		if ($header != "") echo ("<option value='0'>- $header -</option>");
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getEventCarId()."\"");
			if (($defaultId != null) && ($objItem->getEventCarId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTitle()."</option>");
		}
		echo("</select>");
	}			
	
}
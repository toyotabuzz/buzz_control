<?
/*********************************************************
		Class :					Insure

		Last update :	  10 Jan 02

		Description:	  Class manage t_insure table

*********************************************************/
 
class InsureEdit extends DB{

	var $TABLE="t_insure_edit";

	var $m_insure_edit_id;
	function get_insure_edit_id() { return $this->m_insure_edit_id; }
	function set_insure_edit_id($data) { $this->m_insure_edit_id = $data; }
	
	var $m_car_id;
	function get_car_id() { return $this->m_car_id; }
	function set_car_id($data) { $this->m_car_id = $data; }
	
	var $m_sale_id;
	function get_sale_id() { return $this->m_sale_id; }
	function set_sale_id($data) { $this->m_sale_id = $data; }
	
	var $m_edit_type;
	function get_edit_type() { return $this->m_edit_type; }
	function set_edit_type($data) { $this->m_edit_type = $data; }
	
	var $m_old_message;
	function get_old_message() { return $this->m_old_message; }
	function set_old_message($data) { $this->m_old_message = $data; }
	
	var $m_new_message;
	function get_new_message() { return $this->m_new_message; }
	function set_new_message($data) { $this->m_new_message = $data; }
	
	var $m_edit_date;
	function get_edit_date() { return $this->m_edit_date; }
	function set_edit_date($data) { $this->m_edit_date = $data; }
	
	var $m_admin_approve;
	function get_admin_approve() { return $this->m_admin_approve; }
	function set_admin_approve($data) { $this->m_admin_approve = $data; }

	var $m_admin_id;
	function get_admin_id() { return $this->m_admin_id; }
	function set_admin_id($data) { $this->m_admin_id = $data; }
	
	var $m_admin_date;
	function get_admin_date() { return $this->m_admin_date; }
	function set_admin_date($data) { $this->m_admin_date = $data; }
	
	var $m_admin_remark;
	function get_admin_remark() { return $this->m_admin_remark; }
	function set_admin_remark($data) { $this->m_admin_remark = $data; }
	
	var $m_status;
	function get_status() { return $this->m_status; }
	function set_status($data) { $this->m_status = $data; }
	
	function InsureEdit($objData=NULL) {
        If ($objData->insure_edit_id !="") {
			$this->set_insure_edit_id($objData->insure_edit_id);
			$this->set_car_id($objData->car_id);
			$this->set_sale_id($objData->sale_id);
			$this->set_edit_type($objData->edit_type);
			$this->set_old_message($objData->old_message);
			$this->set_new_message($objData->new_message);
			$this->set_edit_date($objData->edit_date);
			$this->set_admin_approve($objData->admin_approve);
			$this->set_admin_date($objData->admin_date);
			$this->set_admin_id($objData->admin_id);
			$this->set_admin_remark($objData->admin_remark);
			$this->set_status($objData->status);
        }
    }

	function init(){
		
	}
		
	function load() {

		if ($this->m_insure_edit_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE insure_edit_id =".$this->m_insure_edit_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureEdit($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureEdit($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	
	function loadMaxCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT MAX(insure_edit_id) as insure_edit_id FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->InsureEdit($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( car_id , sale_id , edit_type , old_message , new_message , edit_date ) " ." VALUES ( "
		." '".$this->m_car_id."' , "
		." '".$this->m_sale_id."' , "
		." '".$this->m_edit_type."' , "
		." '".$this->m_old_message."' , "
		." '".$this->m_new_message."' , "
		." '".$this->m_edit_date."'  "
		." ) ";

        $this->getConnection();		
        If ($Result = $this->query($strSql)) { 
            $this->m_insure_edit_id = mysql_insert_id();
            return $this->m_insure_edit_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		."  admin_approve = '".$this->m_admin_approve."' "
		." , admin_id = '".$this->m_admin_id."' "
		." , admin_date = '".date("Y-m-d H:i:s")."' "
		." , admin_remark = '".$this->m_admin_remark."' "
		." , status = '".$this->m_status."' "
		." WHERE insure_edit_id = ".$this->m_insure_edit_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function updateStatus(){
		$strSql = "UPDATE ".$this->TABLE 
		." SET "
		."  status = '".$this->m_status."' "
		." WHERE insure_edit_id = ".$this->m_insure_edit_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE insure_edit_id=".$this->m_insure_edit_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}
	
	function deleteByCondition($strCondition) {
		if ($strCondition == '') {
			return false;
		}
	
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE ".$strCondition;
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}	
	

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);		
        Return $asrErrReturn;
    }
}

/*********************************************************
		Class :				Insure Company List

		Last update :		22 Mar 02

		Description:		Insure Company List

*********************************************************/

class InsureEditList extends DataList {
	var $TABLE = "t_insure_edit";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT insure_edit_id) as rowCount FROM ".$this->TABLE
			." P  ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT * FROM ".$this->TABLE." "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureEdit($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }

	function loadSale() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_edit_id) as rowCount FROM ".$this->TABLE." P  "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT P.* FROM ".$this->TABLE." P "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureEdit($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadSearch() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT P.insure_edit_id) as rowCount FROM ".$this->TABLE." P  "
		." LEFT JOIN t_insure_car IC ON IC.car_id = P.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = C.insure_customer_id "
		.$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = " SELECT distinct P.* FROM ".$this->TABLE." P "
		." LEFT JOIN t_insure_car IC ON IC.car_id = P.car_id "
		." LEFT JOIN t_insure_customer01 C ON C.insure_customer_id = C.insure_customer_id "		
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
		
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new InsureEdit($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }		
	
}
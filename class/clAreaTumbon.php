<?
/*********************************************************
		Class :				Tumbon

		Last update :		16 NOV 02

		TumbonCode:		Class manage tumbon table

*********************************************************/

class Tumbon extends DB{

	var $TABLE="t_area_tumbon";
	
	var $mTumbonId;
	function getTumbonId() { return $this->mTumbonId; }
	function setTumbonId($data) { $this->mTumbonId = $data; }
	
	var $mTumbonCode;
	function getTumbonCode() { return htmlspecialchars($this->mTumbonCode); }
	function setTumbonCode($data) { $this->mTumbonCode = $data; }

	var $mTumbonName;
	function getTumbonName() { return htmlspecialchars($this->mTumbonName); }
	function setTumbonName($data) { $this->mTumbonName = $data; }

	var $mPostCode;
	function getPostCode() { return htmlspecialchars($this->mPostCode); }
	function setPostCode($data) { $this->mPostCode = $data; }
	
	var $mAmphurCode;
	function getAmphurCode() { return htmlspecialchars($this->mAmphurCode); }
	function setAmphurCode($data) { $this->mAmphurCode = $data; }

	var $mAmphurName;
	function getAmphurName() { return htmlspecialchars($this->mAmphurName); }
	function setAmphurName($data) { $this->mAmphurName = $data; }	
	
	var $mProvinceCode;
	function getProvinceCode() { return htmlspecialchars($this->mProvinceCode); }
	function setProvinceCode($data) { $this->mProvinceCode = $data; }

	var $mProvinceName;
	function getProvinceName() { return htmlspecialchars($this->mProvinceName); }
	function setProvinceName($data) { $this->mProvinceName = $data; }

	var $mRank;
	function getRank() { return htmlspecialchars($this->mRank); }
	function setRank($data) { $this->mRank = $data; }	
	
	function Tumbon($objData=NULL) {
        If ($objData->tumbon_id!="") {
            $this->setTumbonId($objData->tumbon_id);
			$this->setTumbonCode($objData->tumbon_code);
            $this->setTumbonName($objData->tumbon_name);
			$this->setPostCode($objData->post_code);
			$this->setAmphurCode($objData->amphur_code);
            $this->setAmphurName($objData->amphur_name);
			$this->setProvinceCode($objData->province_code);
			$this->setProvinceName($objData->province_name);
			
        }
    }
	
	function init(){
		$this->setTumbonName(stripslashes($this->mTumbonName));
	}
	
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT C.*  FROM ".$this->TABLE." C "
		." WHERE".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Tumbon($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	
	
	
	function load() {

		if ($this->mTumbonId == '') {
			return false;
		}
		$strSql = "SELECT C.*, P.province_name  , A.amphur_name   FROM ".$this->TABLE." C "
		." LEFT JOIN t_area_province P ON P.province_code = C.province_code "
		." LEFT JOIN t_area_amphur A ON A.amphur_code = C.amphur_code "
		." WHERE C.tumbon_id =".$this->mTumbonId;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->Tumbon($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE
						." ( tumbon_name, amphur_code, post_code, province_code, tumbon_code ) "
						." VALUES ( '".$this->mTumbonName."' ,"
						." '".$this->mAmphurCode."' , "
						." '".$this->mPostCode."' , "
						." '".$this->mProvinceCode."' , "
						." '".$this->mTumbonCode."' ) ";
						
        $this->getConnection();
        If ($result = $this->query($strSql)) { 
            $this->mTumbonId = mysql_insert_id();
            return $this->mTumbonId;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE
						." SET tumbon_name = '".$this->mTumbonName."' , "
						." amphur_code = '".$this->mAmphurCode."' ,  "
						." province_code = '".$this->mProvinceCode."' ,  "
						." tumbon_code = '".$this->mTumbonCode."' ,  "
						." post_code = '".$this->mPostCode."'   "						
						." WHERE  tumbon_id = ".$this->mTumbonId."  ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE tumbon_id=".$this->mTumbonId." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	 Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);
        
        Switch ( $Mode )
        {
            Case "update":
            Case "add":
				if ($this->mTumbonName == "") $asrErrReturn["tumbon_name"] = "��س��кت��͵Ӻ�";
				
                Break;
            Case "Delete":
                Break;
        }
        Return $asrErrReturn;
    }

   function checkUnique($strMode,$field,$field1,$table,$compare,$value)
    {
        $strSql  = "SELECT $field FROM $table ";
		if (strtolower($strMode) == "add"){
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value);
		}else{
			$strSql .= " WHERE $field = " .$this->convStr4SQL($value)." and $field1 != ".$compare;
		}
		//echo $strSql;
        $this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())      //Username found.
            {
				//return $row->dealer_id;
				return false;
			}
        }
       return true;
    }
	
	
}

/*********************************************************
		Class :				TumbonList

		Last update :		22 Mar 02

		TumbonCode:		Tumbon user list

*********************************************************/


class TumbonList extends DataList {
	var $TABLE = "t_area_tumbon";

	function load() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT tumbon_id) as rowCount FROM ".$this->TABLE." C  "
		." LEFT JOIN t_area_province P ON P.province_code = C.province_code "
		." LEFT JOIN t_area_amphur A ON A.amphur_code = C.amphur_code "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*, P.province_name  , A.amphur_name   FROM ".$this->TABLE." C "
		." LEFT JOIN t_area_province P ON P.province_code = C.province_code "
		." LEFT JOIN t_area_amphur A ON A.amphur_code = C.amphur_code "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Tumbon($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }
	
	function loadLite() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT tumbon_id) as rowCount FROM ".$this->TABLE." C  "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnection();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.* FROM ".$this->TABLE." C "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnection();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Tumbon($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	
	
	
	function loadUTF8() {
		// also gets latest delivery date
        //Get Number of Users list
        $strSql = "SELECT Count(DISTINCT tumbon_id) as rowCount FROM ".$this->TABLE." C  "
		." LEFT JOIN t_area_province P ON P.province_code = C.province_code "
		." LEFT JOIN t_area_amphur A ON A.amphur_code = C.amphur_code "
			." ".$this->getFilterSQL();	// WHERE clause
	   // echo $strSql;
		$this->getConnectionUTF8();
		if ( $result = $this->query($strSql) ) {
			$row = $result->nextRow();
			$this->mCount = $row->rowCount;
			if ( $this->mCount == 0 ) {
				return false;
			}
		}
		$strSql = "SELECT C.*, P.province_name  , A.amphur_name   FROM ".$this->TABLE." C "
		." LEFT JOIN t_area_province P ON P.province_code = C.province_code "
		." LEFT JOIN t_area_amphur A ON A.amphur_code = C.amphur_code "
			.$this->getFilterSQL()	// WHERE clause
			.' '.$this->getSortSQL()	// ORDER BY clause
			.' '.$this->getLimitSQL();	// PAGING
	
		$this->getConnectionUTF8();
		if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new Tumbon($row);
			}
			$result->freeResult();
			$this->unsetConnection();
			return true;
		} else {
			$this->unsetConnection();
			return false;
		}
    }	

	function printSelect($name, $defaultId = null ,$header = null) {
		echo ("<select class=\"field\" name=\"".$name."\">\n");
		if ($header == "search") {echo ("<option value=\"0\">- any tumbon -</option>");};
		if ($header == "Add" || $header == "Update" || $header == "") {echo ("<option value=\"0\">- select tumbon -</option>");};
		foreach ($this->mItemList as $objItem) {					
			echo("<option value=\"".$objItem->getTumbonId()."\"");
			if (($defaultId != null) && ($objItem->getTumbonId() == $defaultId)) {
				echo(" selected");
			}
			echo(">".$objItem->getTumbonName()."</option>");
		}
		echo("</select>");
	}

}
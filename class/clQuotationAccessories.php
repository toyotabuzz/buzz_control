<?
/*********************************************************
		Class :			  QuotationAccessories

		Last update :	  11 Mar 21

		Description:	  Class manage jmquotation_accessories table

*********************************************************/
 
class QuotationAccessories extends DB{

	var $TABLE="jmquotation_accessories";
	var $mItemList;
	var $m_id;
	function get_id() { return $this->m_id; }
	function set_id($data) { $this->m_id = $data; }
	
	var $m_quotation_id;
	function get_quotation_id() { return $this->m_quotation_id; }
	function set_quotation_id($data) { $this->m_quotation_id = $data; }

	var $m_insure_id;
	function get_insure_id() { return $this->m_insure_id; }
	function set_insure_id($data) { $this->m_insure_id = $data; }
	
	var $m_item_name;
	function get_item_name() { return $this->m_item_name; }
	function set_item_name($data) { $this->m_item_name = $data; }
	
	var $m_item_price;
	function get_item_price() { return $this->m_item_price; }
	function set_item_price($data) { $this->m_item_price = $data; }
	
	var $m_created_at;
	function get_created_at() { return $this->m_created_at; }
	function set_created_at($data) { $this->m_created_at = $data; }
	
	var $m_created_by;
	function get_created_by() { return $this->m_created_by; }
	function set_created_by($data) { $this->m_created_by = $data; }

	var $m_updated_at;
	function get_updated_at() { return $this->m_updated_at; }
	function set_updated_at($data) { $this->m_updated_at = $data; }

	var $m_updated_by;
	function get_updated_by() { return $this->m_updated_by; }
	function set_updated_by($data) { $this->m_updated_by = $data; }

    function QuotationAccessories($objData=NULL) {
        If ($objData->id !="") {
			$this->set_id($objData->id);
			$this->set_quotation_id($objData->quotation_id);
			$this->set_insure_id($objData->insure_id);
			$this->set_item_name($objData->item_name);
			$this->set_item_price($objData->item_price);
			$this->set_created_at($objData->created_at);
			$this->set_created_by($objData->created_by);
			$this->set_updated_at($objData->updated_at);
			$this->set_updated_by($objData->updated_by);
        }
    }

	function init(){
		$this->setTitle(stripslashes($this->mTitle));
	}
		
	function load() {

		if ($this->m_id == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE id =".$this->m_id;
		$this->getConnection();
        if ($result = $this->query($strSql))
        {
            if ($row = $result->nextRow())
            {
				$this->QuotationAccessories($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}
	
	function loadByCondition($strCondition) {

		if ($strCondition == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE."  WHERE ".$strCondition;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
            if ($row = $result->nextRow()) {
				$this->QuotationAccessories($row);
                $result->freeResult();
				return true;
            }
        }
		return false;
	}	
	

	function add() {
		$strSql = "INSERT INTO ".$this->TABLE ." ( quotation_id , insure_id , item_name , item_price , created_at , created_by , updated_at , updated_by ) " ." VALUES ( "
		." '".$this->m_quotation_id."' , "
		." '".$this->m_insure_id."' , "
		." '".$this->m_item_name."' , "
		." '".$this->m_item_price."' , "
		." NOW(), "
		." '".$_SESSION['sMemberId']."' , "
        ." NOW(), "
		." '".$_SESSION['sMemberId']."' "
		." ) ";
        $this->getConnection();		
        if ($Result = $this->query($strSql)) { 
            $this->m_id = mysql_insert_id();
            return $this->m_id;
        } else {
			return false;
	    }
	}

	function update(){
		$strSql = "UPDATE ".$this->TABLE." SET "
		." quotation_id = '".$this->m_quotation_id."' "
		." , insure_id = '".$this->m_insure_id."' "
		." , item_name = '".$this->m_item_name."' "
		." , item_price = '".$this->m_item_price."' "
		." , created_at = NOW() "
		." , created_by = '".$_SESSION['sMemberId']."' "
		." , updated_at = NOW() "
		." , updated_by = '".$_SESSION['sMemberId']."' "
		." WHERE id = ".$this->m_id." "; 
        $this->getConnection();
		//echo $strSql;
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	function delete() {
       $strSql = " DELETE FROM ".$this->TABLE
                . " WHERE id=".$this->m_id." ";
        $this->getConnection();
        $result=$this->query($strSql);
		$this->unsetConnection();
		return $result;
	}

	Function check($Mode)
    {
		global $langUserError;
        $Mode = StrToLower($Mode);

        Return $asrErrReturn;
    }

	function loadDataAccess($quotationId) {
		if ($quotationId == '') {
			return false;
		}
		$strSql = "SELECT * FROM ".$this->TABLE." WHERE quotation_id = ".$quotationId;
		$this->getConnection();
        if ($result = $this->query($strSql)) {
			while ($row = $result->nextRow()) {
				$this->mItemList[] = new QuotationAccessories($row);
			}
			// $this->QuotationAccessories($row);
			$result->freeResult();
			$this->unsetConnection();
			return true;
        } else {
			$this->unsetConnection();
			return false;
		}
	}
}

/*********************************************************
		Class :			  Insure Company List

		Last update :	  11 Mar 21

		Description:	  Insure Company List

*********************************************************/
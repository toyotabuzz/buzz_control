<?
header("Cache-Control: no-cache");
session_start();
// -- Database ------------------------------------------------------------
    define("DB_HOST", "localhost");			    // Database Host
    define("DB_USER", "root");			        // Database User
    define("DB_PASS", "");				        // password for user of database
    define("DB_NAME", "buzzs");               // Database Name

/** Directory *************/
	Define ("gDIR_IMG","../images/misc/");

// -- Paths ---------------------------------------------------------------
	define("PATH_BASE","../");
    define("PATH_CLASS",PATH_BASE."class/");
	define("PATH_INCLUDE",PATH_BASE."include/");
	define("PATH_IMG",PATH_BASE."images/misc/");
	define("URL_BASE","purchase");
	
// -- Users and Misc -------------------------------------------------

	define("USERNAME_MIN", 3);
    define("USERNAME_MAX", 15);
	define("PASSWORD_MIN", 3);
    define("PASSWORD_MAX", 15);

    define("TITLE", "KSJ");               // Title of pages
	

// -- Class include -------------------------------------------------------
    include(PATH_CLASS."clDb.php");
	include(PATH_CLASS."clDataList.php");

	include (PATH_CLASS."english.php");
	include (PATH_CLASS."functions.php");

// -- Sort and Paging -------------------------------------------------
	define ("DEF_PAGING",15);
	define ("DEF_SORT_ASC","images/arrowDown.gif");
	define ("DEF_SORT_DESC","images/arrowUp.gif");
	define ("DEF_SORT_ASC_ACTIVE","images/arrowDownRed.gif");
	define ("DEF_SORT_DESC_ACTIVE","images/arrowUpRed.gif");
?>

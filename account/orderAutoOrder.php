<?
include "common.php";

$objOrderList = new OrderList();
$objOrderList->setFilter(" order_number LIKE '%".$q."%'  AND order_status = 0 AND delete_status = 0 AND company_id=$sCompanyId ");
$objOrderList->setPageSize(0);
$objOrderList->setSortDefault(" order_number ASC");
$objOrderList->loadUTF8();

forEach($objOrderList->getItemList() as $objItem) {
$objCustomer = new Customer();
$objCustomer->setCustomerId($objItem->getBookingCustomerId());
$objCustomer->loadUTF8();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objItem->getBookingCarSeries());
$objCarSeries->load();

$objColor = new CarColor();
$objColor->setCarColorId($objItem->getBookingCarColor());
$objColor->loadUTF8();

$objStockRed = new StockRed();
$objStockRed->setStockRedId($objItem->getStockRedId());
$objStockRed->loadUTF8();

$objSale = new Member();
$objSale->setMemberId($objItem->getBookingSaleId());
$objSale->loadUTF8();

$objStockCar = new StockCar();
$objStockCar->setStockCarId($objItem->getStockCarId());
$objStockCar->loadUTF8();


?>
<div onSelect="this.text.value='<?=$objItem->getOrderNumber()?>';document.frm01.hOrderId.value='<?=$objItem->getOrderId()?>';document.frm01.hCarNumber.value='<?=$objStockCar->getCarNumber()?>';document.getElementById('series').innerHTML='<?=$objCarSeries->getCarModelTitle()." ".$objCarSeries->getTitle()." /  ".$objColor->getTitle()?>';document.getElementById('delivery').innerHTML='<?=$objItem->getSendingDate()?>';document.getElementById('customer').innerHTML='<?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?>';document.getElementById('sale').innerHTML='<?=$objSale->getFirstname()." ".$objSale->getLastname()?>';"><span class='informal'></span><span><?=$objItem->getOrderNumber()?></span></div>
<?}

unset($objOrderList);
?>
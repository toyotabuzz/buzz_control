<?
include("common.php");

$objCustomer = new Customer();
$objEvent = new CustomerEvent();
$objOrderStockOtherList = new OrderStockOtherList();
$objOrderStockOtherPremiumList = new OrderStockOtherList();
$objStockProductList = new StockProductList();
$objCarSeries = new CarSeries();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		

		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();

		if($hCustomerId != ""){	
			$objOrder->setBookingCustomerId($hCustomerId);			
		}
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		$strMode="Update";

		
		$objSale = new Member();
		$objSale->setMemberId($objCustomer->getSaleId());
		$objSale->load();
		
		$hCustomerSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		
		$arrDate = explode("-",$objOrder->getPrbExpire());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];		
		
		$objContactList = new ContactList();
		$objContactList->setPageSize(0);
		$objContactList->setSortDefault(" title ASC");
		$objContactList->load();				
		
		$objCustomerContactList = new CustomerContactList();
		$objCustomerContactList->setFilter("   customer_id = $hId ");
		$objCustomerContactList->setPageSize(0);
		$objCustomerContactList->setSortDefault(" contact_date DESC ");
		$objCustomerContactList->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();
		
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hCustomerId);
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard(1);
			$objCustomer->setCCard($hCCard);
            $objCustomer->setTypeId("B");
            $objCustomer->setGradeId($hGradeId);
            $objCustomer->setGroupId($hGroupId);
			$objCustomer->setEventId($hEventId);
			$hPostedDate = $Year."-".$Month."-".$Day;
            $objCustomer->setInformationDate($hPostedDate);
            $objCustomer->setSaleId($hCustomerSaleId);
			$hBirthday = $Year01."-".$Month01."-".$Day01;
            $objCustomer->setBirthday($hBirthday);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setEmail($hEmail);
			$objCustomer->setIDCard($hIDCard);
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setHomeTel($hHomeTel);
			$objCustomer->setMobile($hMobile);
			$objCustomer->setOfficeTel($hOfficeTel);
			$objCustomer->setFax($hFax);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setAddress01($hAddress01);
			$objCustomer->setTumbon01($hTumbon01);
			$objCustomer->setAmphur01($hAmphur01);
			$objCustomer->setProvince01($hProvince01);
			$objCustomer->setTumbonCode01($hTumbonCode01);
			$objCustomer->setAmphurCode01($hAmphurCode01);
			$objCustomer->setProvinceCode01($hProvinceCode01);
			$objCustomer->setZipCode01($hZipCode01);
			$objCustomer->setZip01($hZip01);
			$objCustomer->setTel01($hTel01);
			$objCustomer->setFax01($hFax01);
			$objCustomer->setAddress02($hAddress02);
			$objCustomer->setTumbon02($hTumbon02);
			$objCustomer->setAmphur02($hAmphur02);
			$objCustomer->setProvince02($hProvince02);
			$objCustomer->setTumbonCode02($hTumbonCode02);
			$objCustomer->setAmphurCode02($hAmphurCode02);
			$objCustomer->setProvinceCode02($hProvinceCode02);
			$objCustomer->setZipCode02($hZipCode02);
			$objCustomer->setZip02($hZip02);
			$objCustomer->setTel02($hTel02);
			$objCustomer->setFax02($hFax02);
			$objCustomer->setAddress03($hAddress03);
			$objCustomer->setTumbon03($hTumbon03);
			$objCustomer->setAmphur03($hAmphur03);
			$objCustomer->setProvince03($hProvince03);
			$objCustomer->setTumbonCode03($hTumbonCode03);
			$objCustomer->setAmphurCode03($hAmphurCode03);
			$objCustomer->setProvinceCode03($hProvinceCode03);
			$objCustomer->setZipCode03($hZipCode03);
			$objCustomer->setZip03($hZip03);
			$objCustomer->setTel03($hTel03);
			$objCustomer->setFax03($hFax03);
			
			
            $objOrder->setOrderId($hOrderId);			
			
			$hBookingDate = $Year02."-".$Month02."-".$Day02;
			$objOrder->setBookingCustomerId($hBookingCustomerId);
			$objOrder->setBookingNumber($hBookingNumber);
			$objOrder->setBookingDate($hBookingDate);
            $objOrder->setBookingSaleId($hBookingSaleId);
			
			$objOrder->setOrderStatus(1);
            $objOrder->setOrderNumber($hOrderNumber);
            $objOrder->setOrderPrice($hOrderPrice);
			$objOrder->setOrderReservePrice($hOrderReservePrice);
            $objOrder->setOrderLocation($hOrderLocation);
			
			
            $objOrder->setBookingRemark($hBookingRemark);
			$objOrder->setBookingCarType($hBookingCarModel);
			$objOrder->setBookingCarSeries($hBookingCarSeries);
			$objOrder->setBookingCarGear($hBookingCarGear);
			$objOrder->setBookingCarColor($hBookingCarColor);
			$objOrder->setBookingCarRemark($hBookingCarRemark);
			
			
			$objOrder->setDiscountPrice($hDiscountPrice);
			$objOrder->setDiscountSubdown($hDiscountSubdown);
			$objOrder->setDiscountPremium($hOrderPremiumPriceTotal);
			$objOrder->setDiscountProduct($hOrderProductPriceTotal);
			$objOrder->setDiscountInsurance($hDiscountInsurance);
			$objOrder->setDiscountGoa($hDiscountGoa);
			$objOrder->setDiscount($hDiscount);
			$objOrder->setDiscountMargin($hDiscountMargin);
			$objOrder->setDiscountSubdownVat($hDiscountSubdownVat);
			$objOrder->setDiscountAll($hDiscountAll);
			$objOrder->setDiscountOver($hDiscountOver);

			$objOrder->setBuyType($hBuyType);
			$objOrder->setBuyCompany($hBuyCompany);
			$objOrder->setBuyFee($hBuyFee);
			$objOrder->setBuyTime($hBuyTime);
			$objOrder->setBuyPayment($hBuyPayment);
			$objOrder->setBuyPrice($hBuyPrice);
			$objOrder->setBuyDown($hBuyDown);
			$objOrder->setBuyTotal($hBuyTotal);
			$objOrder->setBuyRemark($hBuyRemark);
			$objOrder->setPrbCompany($hPrbCompany);
			$hPrbExpire = $Year05."-".$Month05."-".$Day05;
			
			$objOrder->setPrbExpire($hPrbExpire);
			$objOrder->setPrbPrice($hPrbPrice);
			$objOrder->setInsureCompany($hInsureCompany);
			$hInsureExpire = $Year06."-".$Month06."-".$Day06;
			$objOrder->setInsureExpire($hInsureExpire);
			$objOrder->setInsurePrice($hInsurePrice);
			$objOrder->setInsureType($hInsureType);
			$objOrder->setInsureYear($hInsureYear);
			$objOrder->setInsureBudget($hInsureBudget);
			$objOrder->setInsureFrom($hInsureFrom);
			$objOrder->setInsureFromDetail($hInsureFromDetail);
			$objOrder->setInsureRemark($hInsureRemark);			
			$objOrder->setInsureCustomerPay($hInsureCustomerPay);
			
			$objOrder->setTMBNumber($hTMBNumber);	
			$objOrder->setEditBy($sMemberId);
			
			$objCarSeries = new CarSeries();
			$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
			$objCarSeries->load();
			
			$objCarColor = new CarColor();
			$objCarColor->setCarColorId($objOrder->getBookingCarColor());
			$objCarColor->load();			
			
			
			if(isset($hOrderProductAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderProductId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderProductQty);
				$objOrderStock->setPrice($hOrderProductPrice);
				$objOrderStock->setTMT($hOrderProductTMT);
				if($hOrderProductOther){
					$objOrderStock->setRemark($hOrderProduct);
				}
				$objOrderStock->add();
				
			}
			
			if(isset($hOrderProductUpdate)){
				for($i=0;$i<$hCountOrderProduct;$i++){
					if($_POST["hOrderProductDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderProductId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(0);
						$objOrderStock->setQty($_POST["hOrderProductQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderProductPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderProductTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderProductRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}
			
			if(isset($hOrderPremiumAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderPremiumId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderPremiumQty);
				$objOrderStock->setPrice($hOrderPremiumPrice);
				$objOrderStock->setTMT($hOrderPremiumTMT);
				$objOrderStock->setStockType(1);
				if($hOrderPremiumOther){
					$objOrderStock->setRemark($hOrderPremium);
				}
				$objOrderStock->add();
			}
			
			if(isset($hOrderPremiumUpdate)){
				for($i=0;$i<$hCountOrderPremium;$i++){
					if($_POST["hOrderPremiumDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderPremiumId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(1);
						$objOrderStock->setQty($_POST["hOrderPremiumQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderPremiumPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderPremiumTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderPremiumRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}
			
	
			if(!$hOrderProductAdd AND !$hOrderPremiumAdd AND !$hOrderProductUpdate AND !$hOrderPremiumUpdate){
	
	
    		$pasrErrCustomer = $objCustomer->check($hSubmit);
			$pasrErrOrder = $objOrder->checkCrlBooking($hSubmit);

			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					$objCustomer->update();
					$objOrder->updateCrlBooking();
					
					header("location:bcardNewList.php");
					exit;
				}
				unset ($objCustomer);


			}else{
				$objCustomer->init();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objOrder->getBookingCarColor());
				$objCarColor->load();
				
			}//end check Count($pasrErr)
		}
		
	}//end check $hOrderProductAdd 
}

$objOrderStockList = new OrderStockList();
$objOrderStockList->setFilter(" order_id= $hId AND stock_type=0 ");
$objOrderStockList->setPageSize(0);
$objOrderStockList->setSortDefault(" stock_product_id DESC");
$objOrderStockList->load();

$objOrderStockPremiumList = new OrderStockList();
$objOrderStockPremiumList->setFilter(" order_id= $hId AND stock_type=1 ");
$objOrderStockPremiumList->setPageSize(0);
$objOrderStockPremiumList->setSortDefault(" stock_product_id DESC");
$objOrderStockPremiumList->load();

$objOrderStockList01 = new OrderStockList();
$objOrderStockList01->setFilter(" order_id= $hId AND stock_type=2 ");
$objOrderStockList01->setPageSize(0);
$objOrderStockList01->setSortDefault(" stock_product_id DESC");
$objOrderStockList01->load();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Toyota Buzz  : Cashier System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_report.css">
	<SCRIPT language=javascript src="../script/functions.js" type=text/javascript></SCRIPT>
</head>
<body>
<div align="center"><strong>ACC18 ��§ҹ��������´�������١��Ҩͧ</strong></div>
<table cellpadding="3" cellspacing="0" align="center" class="search">
<tr>
	<td>�Ţ���㺨ͧ</td>
	
	<td><?=$objOrder->getOrderNumber()?>&nbsp;</td>
</tr>
<tr>
	<td>�ѹ���ͧ</td>
	
	<td><?=$Day02?>/<?=$Month02?>/<?=$Year02?>&nbsp;</td>
</tr>
<tr>
	<td>�ҤҢ��</td>
	
	<td><?=number_format($objOrder->getOrderPrice(),2)?>&nbsp;</td>
</tr>
<tr>
	<td>�ӹǹ�Թ�ͧ</td>
	
	<td><?=number_format($objOrder->getOrderReservePrice(),2)?>&nbsp;</td>
</tr>
<tr>
	<td>��ѡ�ҹ���</td>	
	<td><?=$hSaleName?>&nbsp;</td>
</tr>
<?
		$objGroup = new CustomerGroup();
		$objGroup->setGroupId($objCustomer->getGroupId());
		$objGroup->load();

		$objEvent = new CustomerEvent();
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();

?>
<tr>
	<td>���觷����</td>	
	<td><?=$objGroup->getTitle()?>&nbsp;<?=$objEvent->getTitle()?></td>
</tr>
<tr>
	<td>�����١��Ҩͧ</td>
	
	<td><?=$objCustomer->getTitleDetail();?> 					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<?=$name?>&nbsp;</td>
</tr>
<tr>
	<td>�������</td>
	
	<td><?=$objCustomer->getAddress();?> <?=$objCustomer->getTumbon();?> <?=$objCustomer->getAmphur();?> <?=$objCustomer->getProvince();?> <?=$objCustomer->getZip();?>&nbsp;</td>
</tr>
<tr>
	<td>���Ѿ���ҹ</td>
	
	<td><?=$objCustomer->getHomeTel();?>&nbsp;</td>
</tr>
<tr>
	<td>��Ͷ��</td>
	
	<td><?=$objCustomer->getMobile();?>&nbsp;</td>
</tr>
<tr>
	<td>���Ѿ����ӧҹ</td>
	
	<td><?=$objCustomer->getOfficeTel();?>&nbsp;</td>
</tr>
<tr>
	<td>Ẻö</td>
			<?if($hCarSeries == ""){
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				$hCarSeries = $objCarSeries->getTitle();
				$hCarModelId = $objCarSeries->getCarModelId();
				}
			?>
	<td><?=$hCarSeries;?>&nbsp;</td>
</tr>
<tr>
	<td>���ö</td>
	
	<td><?=$objCarSeries->getCarModelTitle()?>&nbsp;</td>
</tr>
<tr>
	<td>������</td>
	
	<td><?=$objCarSeries->getCarTypeDetail()?>&nbsp;</td>
</tr>
<tr>
	<td>Model</td>
	
	<td><?=$objCarSeries->getModel()?>&nbsp;</td>
</tr>
<tr>
	<td>�����</td>
	
	<td><?=$objOrder->getBookingCarGear()?>&nbsp;</td>
</tr>
<tr>
	<td>��ö</td>	
	<td><?=$objCarColor->getTitle()?>&nbsp;</td>
</tr>
<tr>
	<td>��¡�âͧ��</td>
	
	<td>
		<table class="search" cellpadding="0" cellspacing="0">
		<?if($objOrderStockList->mCount > 0){?>
			<?
			$i=0;
			$nPriceProductTMT = 0;
			$nPriceProductBuzz = 0;
			forEach($objOrderStockList->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPriceProductTMT = $nPriceProductTMT+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPriceProductBuzz = $nPriceProductBuzz+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td  width="250" valign="top" class="i_background04"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?></td>
			<td  width="20" align="center"  class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50" class="listDetail" align="right"><?=number_format($objItem->getPrice(),2);?></td>
			

		</tr>
				<?}else{?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td valign="top" class="i_background04"><?=$strBuzz?>&nbsp;���� <?=$objItem->getRemark();?></td>
			<td  width="20" align="center"  class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50" class="listDetail" align="right"><?=number_format($objItem->getPrice(),2);?></td>
			

		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>		
		</table>
	
	
	</td>
</tr>
<tr>
	<td>��¡�âͧ Premium</td>
	
	<td>
		<table class="search" cellpadding="0" cellspacing="0">
		<?if($objOrderStockPremiumList->mCount > 0){?>
			<?
			$i=0;
			$nPricePremiumTMT = 0;
			$nPricePremiumBuzz = 0;
			forEach($objOrderStockPremiumList->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPricePremiumTMT = $nPricePremiumTMT+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPricePremiumBuzz = $nPricePremiumBuzz+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td  width="250" valign="top" class="i_background04"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?></td>
			<td  width="20" align="center" valign="top"  class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50"  valign="top" class="listDetail" align="right"><?=$objItem->getPrice();?></td>
		
		</tr>
				<?}else{?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td  width="250" valign="top" class="i_background04"><?=$strBuzz?>&nbsp;���� <?=$objItem->getRemark();?></td>
			<td  width="20" align="center" valign="top" class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50"  valign="top" class="listDetail" align="right"><?=$objItem->getPrice();?></td>
		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>	&nbsp;	
		</table>
	</td>
</tr>
<tr>
	<td>��¡���١��ҫ����ػ�ó�����</td>
	
	<td>
		<table class="search" cellpadding="0" cellspacing="0">
		<?if($objOrderStockList01->mCount > 0){?>
			<?
			$i=0;
			$nPriceProductTMT = 0;
			$nPriceProductBuzz = 0;
			forEach($objOrderStockList01->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPriceProductTMT = $nPriceProductTMT+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPriceProductBuzz = $nPriceProductBuzz+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td  width="250" valign="top" class="i_background04"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?></td>
			<td  width="20" align="center"  class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50" class="listDetail" align="right"><?=number_format($objItem->getPrice(),2);?></td>
			

		</tr>
				<?}else{?>
		<tr>
			<td width="15" align="center"><?=$i+1?></td>
			<td valign="top" class="i_background04"><?=$strBuzz?>&nbsp;���� <?=$objItem->getRemark();?></td>
			<td  width="20" align="center"  class="listDetail"><?=$objItem->getQty()?></td>
			<td  width="50" class="listDetail" align="right"><?=number_format($objItem->getPrice(),2);?></td>
			

		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>		
		</table>
	
	
	</td>
</tr>
</table>
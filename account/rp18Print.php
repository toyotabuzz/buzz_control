<?
include("common.php");
include("incUtility.php");

if($hCompanyId >0 ){
	$objC = new Company();
	$objC->setCompanyId($hCompanyId);
	$objC->load();
	$sqlCompany = " AND O.company_id = $hCompanyId ";
	$strCompany = $objC->getTitle();
	
	$objTeamList = new TeamList();
	$objTeamList->setFilter(" P.company_id = $hCompanyId ");
	if($hTeamId != "" AND $hTeamId != 0) $objTeamList->setFilter(" team_id = $hTeamId ");
	$objTeamList->setPageSize(0);
	$objTeamList->setSortDefault(" title ASC");
	$objTeamList->load();	
	
}else{
	$objTeamList = new TeamList();
	if($hTeamId != "" AND $hTeamId != 0) $objTeamList->setFilter(" team_id = $hTeamId ");
	$objTeamList->setPageSize(0);
	$objTeamList->setSortDefault(" title ASC");
	$objTeamList->load();
}

if($hCarModelId != "" AND $hCarModelId != "0"){
	$sqlCarModel = " AND CS.car_model_id = ".$hCarModelId;
}


$arrDay[0]=1;
$arrDay[1]=2;
$arrDay[2]=3;
$arrDay[3]=4;
$arrDay[4]=5;
$arrDay[5]=6;
$arrDay[6]=7;
$arrDay[7]="8-14";
$arrDay[8]="15-21";
$arrDay[9]="22-30";
$arrDay[10]="31-60";
$arrDay[11]="61-90";
$arrDay[12]="90-120";
$arrDay[13]="�ҡ���� 120";

if($hReportType==1){
$arrQ[0]="( ( DATEDIFF(finance_date,booking_date) = 1  AND buy_type=2)  OR (  DATEDIFF(sending_cashier_date,booking_date) = 1  AND buy_type=1)   )";
$arrQ[1]="(  ( DATEDIFF(finance_date,booking_date) = 2    AND buy_type=2)  OR (    DATEDIFF(sending_cashier_date,booking_date) = 2    AND buy_type=1)    ) ";
$arrQ[2]="(  (  DATEDIFF(finance_date,booking_date) = 3   AND buy_type=2)   OR (    DATEDIFF(sending_cashier_date,booking_date) = 3  AND buy_type=1)    )  ";
$arrQ[3]="(  (  DATEDIFF(finance_date,booking_date) = 4  AND buy_type=2)    OR (    DATEDIFF(sending_cashier_date,booking_date) = 4  AND buy_type=1)   )  ";
$arrQ[4]="(  (  DATEDIFF(finance_date,booking_date) = 5  AND buy_type=2)    OR (    DATEDIFF(sending_cashier_date,booking_date) = 5  AND buy_type=1)    )  ";
$arrQ[5]="(  (  DATEDIFF(finance_date,booking_date) = 6  AND buy_type=2)   OR (    DATEDIFF(sending_cashier_date,booking_date) = 6   AND buy_type=1)   ) ";
$arrQ[6]="(  (  DATEDIFF(finance_date,booking_date) = 7  AND buy_type=2)   OR (    DATEDIFF(sending_cashier_date,booking_date) = 7  AND buy_type=1)    ) ";
$arrQ[7]="(  (  ( DATEDIFF(finance_date,booking_date) >= 8 AND  DATEDIFF(finance_date,booking_date) <= 14 )   AND buy_type=2)  OR (   ( DATEDIFF(sending_cashier_date,booking_date) >= 8 AND  DATEDIFF(sending_cashier_date,booking_date) <= 14 )  AND buy_type=1)    ) ";
$arrQ[8]="(  (  ( DATEDIFF(finance_date,booking_date) >= 15 AND  DATEDIFF(finance_date,booking_date) <= 21 )   AND buy_type=2)   OR (   (  DATEDIFF(sending_cashier_date,booking_date) >= 15 AND  DATEDIFF(sending_cashier_date,booking_date) <= 21 )  AND buy_type=1)   ) ";
$arrQ[9]="(  (  ( DATEDIFF(finance_date,booking_date) >= 22 AND  DATEDIFF(finance_date,booking_date) <= 30 )   AND buy_type=2)   OR (   ( DATEDIFF(sending_cashier_date,booking_date) >= 22 AND  DATEDIFF(sending_cashier_date,booking_date) <= 30 )   AND buy_type=1)   )";
$arrQ[10]="(  (  ( DATEDIFF(finance_date,booking_date) >= 31 AND  DATEDIFF(finance_date,booking_date) <= 60 )   AND buy_type=2)  OR (   ( DATEDIFF(sending_cashier_date,booking_date) >= 31 AND  DATEDIFF(sending_cashier_date,booking_date) <= 60 )   AND buy_type=1)   )";
$arrQ[11]="(  (  ( DATEDIFF(finance_date,booking_date) >= 61 AND  DATEDIFF(finance_date,booking_date) <= 90 )   AND buy_type=2)  OR (   ( DATEDIFF(sending_cashier_date,booking_date) >= 61 AND  DATEDIFF(sending_cashier_date,booking_date) <= 90 )  AND buy_type=1)    )";
$arrQ[12]="(  (  ( DATEDIFF(finance_date,booking_date) >= 90 AND  DATEDIFF(finance_date,booking_date) <= 120 )   AND buy_type=2)  OR (   ( DATEDIFF(sending_cashier_date,booking_date) >= 90 AND  DATEDIFF(sending_cashier_date,booking_date) <= 120 )  AND buy_type=1)   ) ";
$arrQ[13]="(  (  DATEDIFF(finance_date,booking_date) > 120   AND buy_type=2)  OR (    DATEDIFF(sending_cashier_date,booking_date) > 120   AND buy_type=1)    ) ";
}

if($hReportType==2){
$arrQ[0]="( (  DATEDIFF(sending_cashier_date,booking_date) = 1  AND buy_type=1)   )";
$arrQ[1]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 2    AND buy_type=1)    ) ";
$arrQ[2]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 3  AND buy_type=1)    )  ";
$arrQ[3]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 4  AND buy_type=1)   )  ";
$arrQ[4]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 5  AND buy_type=1)    )  ";
$arrQ[5]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 6   AND buy_type=1)   ) ";
$arrQ[6]="(   (    DATEDIFF(sending_cashier_date,booking_date) = 7  AND buy_type=1)    ) ";
$arrQ[7]="(   (   ( DATEDIFF(sending_cashier_date,booking_date) >= 8 AND  DATEDIFF(sending_cashier_date,booking_date) <= 14 )  AND buy_type=1)    ) ";
$arrQ[8]="(   (   (  DATEDIFF(sending_cashier_date,booking_date) >= 15 AND  DATEDIFF(sending_cashier_date,booking_date) <= 21 )  AND buy_type=1)   ) ";
$arrQ[9]="(   (   ( DATEDIFF(sending_cashier_date,booking_date) >= 22 AND  DATEDIFF(sending_cashier_date,booking_date) <= 30 )   AND buy_type=1)   )";
$arrQ[10]="(   (   ( DATEDIFF(sending_cashier_date,booking_date) >= 31 AND  DATEDIFF(sending_cashier_date,booking_date) <= 60 )   AND buy_type=1)   )";
$arrQ[11]="(   (   ( DATEDIFF(sending_cashier_date,booking_date) >= 61 AND  DATEDIFF(sending_cashier_date,booking_date) <= 90 )  AND buy_type=1)    )";
$arrQ[12]="(  (   ( DATEDIFF(sending_cashier_date,booking_date) >= 90 AND  DATEDIFF(sending_cashier_date,booking_date) <= 120 )  AND buy_type=1)   ) ";
$arrQ[13]="(  (    DATEDIFF(sending_cashier_date,booking_date) > 120   AND buy_type=1)    ) ";
}

if($hReportType==3){
$arrQ[0]="( ( DATEDIFF(finance_date,booking_date) = 1  AND buy_type=2)  )";
$arrQ[1]="(  ( DATEDIFF(finance_date,booking_date) = 2    AND buy_type=2) ) ";
$arrQ[2]="(  (  DATEDIFF(finance_date,booking_date) = 3   AND buy_type=2) )  ";
$arrQ[3]="(  (  DATEDIFF(finance_date,booking_date) = 4  AND buy_type=2) )  ";
$arrQ[4]="(  (  DATEDIFF(finance_date,booking_date) = 5  AND buy_type=2) )  ";
$arrQ[5]="(  (  DATEDIFF(finance_date,booking_date) = 6  AND buy_type=2) ) ";
$arrQ[6]="(  (  DATEDIFF(finance_date,booking_date) = 7  AND buy_type=2)  ) ";
$arrQ[7]="(  (  ( DATEDIFF(finance_date,booking_date) >= 8 AND  DATEDIFF(finance_date,booking_date) <= 14 )   AND buy_type=2)      ) ";
$arrQ[8]="(  (  ( DATEDIFF(finance_date,booking_date) >= 15 AND  DATEDIFF(finance_date,booking_date) <= 21 )   AND buy_type=2)   ) ";
$arrQ[9]="(  (  ( DATEDIFF(finance_date,booking_date) >= 22 AND  DATEDIFF(finance_date,booking_date) <= 30 )   AND buy_type=2)   )";
$arrQ[10]="(  (  ( DATEDIFF(finance_date,booking_date) >= 31 AND  DATEDIFF(finance_date,booking_date) <= 60 )   AND buy_type=2)   )";
$arrQ[11]="(  (  ( DATEDIFF(finance_date,booking_date) >= 61 AND  DATEDIFF(finance_date,booking_date) <= 90 )   AND buy_type=2) )";
$arrQ[12]="(  (  ( DATEDIFF(finance_date,booking_date) >= 90 AND  DATEDIFF(finance_date,booking_date) <= 120 )   AND buy_type=2)  ) ";
$arrQ[13]="(  (  DATEDIFF(finance_date,booking_date) > 120   AND buy_type=2)   ) ";
}

$objOrderList = new OrderList();
$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;
//$pstrCurrentUrl = "stockProduct.php";
if ($hSearch != "")
{  
	$hStartDate = $Year."-".$Month."-".$Day;
	$hEndDate = $Year01."-".$Month01."-".$Day01;
	
	$pstrCondition  = " ( booking_date >= '".$hStartDate."' AND  booking_date <= '".$hEndDate."' AND order_status=6  ) ";	
	$objOrder = new Order();

	$strDate = $Day."-".$Month."-".$Year." �֧ ".$Day01."-".$Month01."-".$Year01;
	
}

if ($hSearch01 != "" )
{  
	$hStartDate = $Year02."-".$Month02."-".$Day02;	
	$pstrCondition  = " ( booking_date = '".$hStartDate."'  AND order_status=6  ) ";	
	$objOrder = new Order();
	
	$strDate = $Day02."-".$Month02."-".$Year02;	
}

	if($hTeamId != "" AND $hTeamId != 0) {
		$objTeam = new Team();
		$objTeam->setTeamId($hTeamId);
		$objTeam->load();
		
		$strTeam = $objTeam->getTitle();
		
	}

if($hCarModelId != "" AND $hCarModelId != 0 ) {
	$objCarModel = new CarModel();
	$objCarModel->setCarModelId($hCarModelId);
	$objCarModel->load();
	
	$strCarModel = " ���ö ".$objCarModel->getTitle();
}

if($hReportType == 2 ) { $strReportType="����������ʴ";  }
if($hReportType == 3 ) { $strReportType="���������� F/N";  }


$pstrCondition01= $pstrCondition;
$page_title = "ACC15 ��§ҹ�������ҡ�èͧ�֧�Ѻ�Թ�ҡ F/N �¡�����ª��� Sale  $strDate $strTeam $strCarModel $strReportType $strCompany "; 
?>
<?include "../header/incHeaderReportPrint.php";?>
<br>
<div class="Head" align="center"><strong>ACC15 ��§ҹ�������ҡ�èͧ�֧�Ѻ�Թ�ҡ F/N �¡�����ª��� Sale <?=$strDate?> <?=$strTeam?> <?=$strCarModel?> <?=$strReportType?> <?=$strCompany?></strong></div>
<br>

	<table  align="center" cellpadding="2" cellspacing="0" class="search">
	<tr>
		<td valign="top" width="50" class="listTitle" align="center"  >�ѹ</td>
	<?forEach($objTeamList->getItemList() as $objItem) {
			$objMemberList = new MemberList();
			$objMemberList->setFilter(" team=".$objItem->getTeamId());
			$objMemberList->setPageSize(0);
			$objMemberList->setSortDefault(" nickname ASC ");
			$objMemberList->load();	
			forEach($objMemberList->getItemList() as $objItemMember) {
		?>
		<td valign="top" width="15" class="listTitle" align="center"  ><?=$objItemMember->getNickname();?></td>	
		<?}?>
		<td valign="top" width="25" class="listTitle" align="center"  ><font color=red><?=$objItem->getTitle();?></font></td>			
	<?}?>
		<td valign="top" width="25" class="listTitle" align="center"  >���</td>
	</tr>
	<?$k=0;
	for($i=0;$i<14;$i++){
	$teamAll=0;
	?>
	<tr >
		<td align="center" class="listDetail"><?=$arrDay[$i]?></td>
	<?forEach($objTeamList->getItemList() as $objItem) {
			
			unset($team);
			$objMemberList = new MemberList();
			$objMemberList->setFilter(" team=".$objItem->getTeamId());
			$objMemberList->setPageSize(0);
			$objMemberList->setSortDefault(" nickname ASC ");
			$objMemberList->load();	
			
			forEach($objMemberList->getItemList() as $objItemMember) {
				$objOrder = new Order();
				$num = $objOrder->loadSumByConditionAccount($pstrCondition." AND ".$arrQ[$i].$sqlCarModel." AND sending_sale_id= ".$objItemMember->getMemberId() );
				$team[$objItem->getTeamId()] = $team[$objItem->getTeamId()]+$num;
				$teamAll = $teamAll+$num;
				
				$sumMember[$objItemMember->getMemberId()] = $sumMember[$objItemMember->getMemberId()] +$num;
				$sumteam[$objItem->getTeamId()] = $sumteam[$objItem->getTeamId()]+$num;
				$sumAll = $sumAll+$num;
		?>
		<td  align="center"  class="listDetail" >&nbsp;<?if($num > 0) echo $num;?></td>	
		<?}?>
		<td  align="center"  class="listDetail" >&nbsp;<font color=red><?if( $team[$objItem->getTeamId()] > 0) echo $team[$objItem->getTeamId()]  ;?></font></td>			
	<?$k++;}?>
		<td  align="center"  class="listDetail"  >&nbsp;<?if( $teamAll > 0) echo $teamAll?></td>
	</tr>
	
	<?}?>
	<tr>
		<td valign="top" class="listTitle" align="center"  >���������</td>
	<?forEach($objTeamList->getItemList() as $objItem) {
			$objMemberList = new MemberList();
			$objMemberList->setFilter(" team=".$objItem->getTeamId());
			$objMemberList->setPageSize(0);
			$objMemberList->setSortDefault(" nickname ASC ");
			$objMemberList->load();	
			forEach($objMemberList->getItemList() as $objItemMember) {
		?>
		<td valign="top" class="listTitle" align="center"  >&nbsp;<?if( $sumMember[$objItemMember->getMemberId()] > 0) echo $sumMember[$objItemMember->getMemberId()] ;?></td>	
		<?}?>
		<td valign="top" class="listTitle" align="center"  >&nbsp;<font color=red><?if( $sumteam[$objItem->getTeamId()] > 0) echo $sumteam[$objItem->getTeamId()];?></font></td>			
	<?}?>
		<td valign="top"  class="listTitle" align="center"  >&nbsp;<?if( $sumAll > 0) echo $sumAll;?></td>
	</tr>
</table>

	
</body>
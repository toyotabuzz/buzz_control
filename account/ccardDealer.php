<?
include("common.php");

$objOrderBooking = new Order();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();
$objCarSeries = new CarSeries();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault(" title ASC");
$objFundCompanyList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		if($objOrder->getRecieveDate() == "0000-00-00 00:00:00"){
			$objOrder->setRecieveDate(date("Y-m-d H:i:s"));
		}
		
		$arrDate = explode(" ",$objOrder->getRecieveDate());
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrDate[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Hour2 = $arrTime[0];
		$Minute2 = $arrTime[1];
		
		if($objOrder->getSendingCashierDate() != "0000-00-00"){
			$arrDate1 = explode("-",$objOrder->getSendingCashierDate());
		}else{
			$arrDate1 = explode("-",date("Y-m-d"));
		}
		
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
		
		if($objOrder->getSendingCashierTime() != "00:00:00"){
			$arrTime = explode(":",$objOrder->getSendingCashierTime());
		}else{
			$arrTime = explode(":",date("H:i:s"));
		}
		$Hour1 = $arrTime[0];
		$Minute1 = $arrTime[1];
		
		$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
		$objCustomer->load();

		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
		$objCarSeries->load();
		
		
		$arrDate1 = explode("-",$objStockCar->getParkingDate());
		$Day04 = $arrDate1[2];
		$Month04 = $arrDate1[1];
		$Year04 = $arrDate1[0];
		
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
	} else {
		$strMode="Add";
		$arrDate = explode("-",date("Y-m-d"));
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];		
		
		$arrTime = explode(":",date("H:i:s"));
		$Hour1 = $arrTime[0];
		$Minute1 = $arrTime[1];
		
		$Hour2 = $arrTime[0];
		$Minute2 = $arrTime[1];	
		
		$Hour03 = $arrTime[0];
		$Minute03 = $arrTime[1];
		
		$arrDate1 = explode("-",date("Y-m-d"));
		$Day1 = $arrDate1[2];
		$Month1 = $arrDate1[1];
		$Year1 = $arrDate1[0];
		
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hSendingCustomerId);
            $objCustomer->setTypeId("C");
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard($hBCard);
			$objCustomer->setCCard(1);
            $objCustomer->setTitle($hTitle);
			$objCustomer->setCustomerTitleId($hCustomerTitleId);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hCustomerName));
			}else{
				$arrName = explode(" ",$hCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 1){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setAddress($hAddress);
			$objCustomer->setTumbon($hTumbon);
			$objCustomer->setAmphur($hAmphur);
			$objCustomer->setProvince($hProvince);
			$objCustomer->setTumbonCode($hTumbonCode);
			$objCustomer->setAmphurCode($hAmphurCode);
			$objCustomer->setProvinceCode($hProvinceCode);
			$objCustomer->setZipCode($hZipCode);
			$objCustomer->setZip($hZip);
			$objCustomer->setAddBy($sMemberId);
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setCompanyId($sCompanyId);
			
            $objOrder->setOrderId($hId);
			
			$objOrder->setBookingCustomerId($hSendingCustomerId);
			$objOrder->setBookingSaleId($hBookingSaleId);
			$hBookingDate = $Year."-".$Month."-".$Day;
			$objOrder->setBookingDate($hBookingDate);
			$hBookingDate1 = $Year1."-".$Month1."-".$Day1;
			$objOrder->setBookingCashierDate($hBookingDate1);
			$hBookingCashierTime = $Hour1.":".$Minute1.":00";
			$objOrder->setBookingCashierTime($hBookingCashierTime);
			$objOrder->setBookingRemark($hBookingRemark);

			
			$objStockCar = new StockCar();
			$objStockCar->setStockCarId($hStockCarId);
			$objStockCar->load();
			
			$objCarSeries = new CarSeries();
			$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
			$objCarSeries->load();
			
			$objOrder->setBookingCarPrice($objCarSeries->getPrice());
			$objOrder->setBookingCarColor($hColorId);
			$objOrder->setBookingCarGear($hGear);
			$objOrder->setBookingCarType($objCarSeries->getCarType());
			$objOrder->setBookingCarSeries($objStockCar->getCarSeriesId());
			
			$objOrder->setAddBy($sMemberId);
			$objOrder->setEditBy($sMemberId);

			$objOrder->setSendingCustomerId($hSendingCustomerId);
			//$objOrder->setSendingNumber($hSendingNumber);
			//$objOrder->setSendingNumberNohead($hSendingNumberNohead);
			$hSendingDate = $Year."-".$Month."-".$Day." ".$Hour2.":".$Minute2.":00";
            $objOrder->setRecieveDate($hSendingDate);
			$hSendingCashierDate = $Year1."-".$Month1."-".$Day1;
            $objOrder->setSendingCashierDate($hSendingCashierDate);
			$hSendingCashierTime = $Hour1.":".$Minute1.":00";
			$objOrder->setSendingCashierTime($hSendingCashierTime);
			$objOrder->setSendingSaleId($hSendingSaleId);
			$objOrder->setCarNumber($hCarNumber);
			$objOrder->setSendingRemark($hSendingRemark);
            $objOrder->setOrderNumber($hOrderNumber); 
			$objOrder->setRecieveNumber($hRecieveNumber); 
			$objOrder->setOrderPrice($hOrderPrice); 
			$objOrder->setStockCarId($hStockCarId);
			$objOrder->setNoRedCode($hNoRedCode);
			$objOrder->setStockRedId($hStockRedId);
			$objOrder->setBuyCompany($hBuyCompany);
			$objOrder->setBuyType($hBuyType);
			$objOrder->setCredit($hCredit);
			$objOrder->setDealer(1);
			$objOrder->setCompanyId($sCompanyId);
			
    		$pasrErrCustomer = $objCustomer->check($hSubmit);
			$pasrErrOrder = $objOrder->check($hSubmit);

			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					if($hSendingCustomerId != ""){
						$objCustomer->updateAccountDealer();
					}else{
						$hSendingCustomerId = $objCustomer->addAccountDealer();
					}
					$objOrder->setOrderStatus($hOrderStatus);
					$objOrder->setBookingCustomerId($hSendingCustomerId);
					$objOrder->setSendingCustomerId($hSendingCustomerId);
					$objOrder->setDealer(1);
					$objOrder->updateAccountDealer();
				}else{
					
					if($hSendingCustomerId != ""){
						$objCustomer->updateAccountDealer();
					}else{
						$hSendingCustomerId = $objCustomer->addAccountDealer();
					}
					$objOrder->setOrderStatus(6);
					$objOrder->setBookingCustomerId($hSendingCustomerId);
					$objOrder->setSendingCustomerId($hSendingCustomerId);
					$objOrder->setDealer(1);
					$hId = $objOrder->addAccountDealer();
				}			
				
				//check stock car				
				$objStockCar = new StockCar();
				if($hRecieveStatus > 0 AND $hRecieveStatus < 5){
					
					$objStockCar->setStockCarId($hStockCarId);
					$objStockCar->load();
					
					$objStockCar->setStatus(3);
					$objStockCar->setRecieveStatus($hRecieveStatus);
					$objStockCar->setRecieveNumber($hRecieveNumber);
					$hRecieveDate = $Year."-".$Month."-".$Day;
					$objStockCar->setRecieveDate($hRecieveDate);
					$objStockCar->setOrderId($hId);
					$objStockCar->setCustomerId($hSendingCustomerId);
					$objStockCar->updateRecieve();
				}else{
					$objStockCar->setStockCarId($hStockCarId);
					$objStockCar->load();
					
					$objStockCar->setStatus(2);
					$objStockCar->setRecieveStatus($hRecieveStatus);
					$objStockCar->setRecieveNumber($hRecieveNumber);
					$hRecieveDate = $Year."-".$Month."-".$Day;
					$objStockCar->setRecieveDate($hRecieveDate);
					$objStockCar->setOrderId($hId);
					$objStockCar->setCustomerId($hSendingCustomerId);
					$objStockCar->updateRecieve();
				}
				
				unset ($objCustomer);
				unset ($objOrder);
				header("location:ccardDealerList.php");
				exit;

			}else{
				$objCustomer->init();
			}//end check Count($pasrErr)
			
			}else{
				$arrDate = explode("-",$objOrder->getRecieveDate());
				$Day = $arrDate[2];
				$Month = $arrDate[1];
				$Year = $arrDate[0];
				
				$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
				$objBookingCustomer->load();
				
				$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objCustomer->load();
				
				// load stock car and red code
				
				$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
				$objBookingCarColor->load();
				
				$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
				$objBookingCarSeries->load();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();

		}
}

$pageTitle = "1. �к��������١���";
$strHead03="�ѹ�֡��¡�����ͺ";
if($strMode == "Add"){ $pageContent = "1.9 �ѹ�֡��¡�����ͺ��ҧ Dealer ";}else{$pageContent = "1.9 �ѹ�֡��¡�����ͺ��ҧ Dealer ";}
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
		
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}				
		
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Day.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 						
	
		if (document.forms.frm01.hCustomerName.value=="")
		{
			alert("��س��кت����١���");
			document.forms.frm01.hCustomerName.focus();
			return false;
		} 	
	
		if (document.forms.frm01.hAddress.value=="")
		{
			alert("��س��кط������");
			document.forms.frm01.hAddress.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hTumbonCode.value=="")
		{
			alert("��س����͡�ӺŨҡ�Ӻ�");
			document.forms.frm01.hTumbon.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hAmphurCode.value=="")
		{
			alert("��س����͡����ͨҡ�к�");
			document.forms.frm01.hAmphur.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hProvinceCode.value=="")
		{
			alert("��س����͡�ѧ��Ѵ�ҡ�к�");
			document.forms.frm01.hProvince.focus();
			return false;
		} 
		
		if (document.forms.frm01.hZipCode.value=="")
		{
			alert("��س����͡������ɳ���ҡ�к�");
			document.forms.frm01.hZip.focus();
			return false;
		} 						

		if (document.forms.frm01.hStockCarId.value=="" || document.forms.frm01.hStockCarId.value== 0)
		{
			alert("��س����͡�����Ţ�ѧ�ҡ�к�");
			document.forms.frm01.hCarNumber.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hRecieveStatus.value=="" )
		{
			alert("��س����͡�к�ʶҹ�ö���ͺ");
			document.forms.frm01.hRecieveStatus.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hRecieveNumber.value =="")
		{
			alert("��س����͡�к�����ͺö");
			document.forms.frm01.hRecieveNumber.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hOrderPrice.value =="" || document.forms.frm01.hOrderPrice.value =="0" )
		{
			alert("��س��к��Ҥ�ö");
			document.forms.frm01.hOrderPrice.focus();
			return false;
		} 	
		
		if (document.forms.frm01.hColorId.options[frm01.hColorId.selectedIndex].value=="0")
		{
			alert("��س����͡��ö");
			document.forms.frm01.hColorId.focus();
			return false;
		}	
		
		
		document.getElementById("hSubmit1").style.display = "none";
		document.frm01.submit();
		return true;

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				

<form name="frm01" action="ccardDealer.php" method="POST" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  	<input type="hidden" name="strMode" value="<?=$strMode?>">
	 	<input type="hidden" name="hId" value="<?=$hId?>">
	  	<input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  	<input type="hidden" name="hTrick" value="true">
	  	<input type="hidden"  name="hOrderStatus" size="20"  value="<?=$objOrder->getOrderStatus()?>">
		<input type="hidden"   name="hNoRedCode" value=1 >
		<input type="hidden"   name="hDealer" value=1 >
		<input type="hidden"  name="hBuyType" value=1 >	
	  	<input type="hidden" name="hACard" value="<?=$objCustomer->getACard();?>">
	  	<input type="hidden" name="hBCard" value="<?=$objCustomer->getBCard();?>">
  	  	<input type="hidden" name="hCCard" value="<?=$objCustomer->getCCard();?>">
		<input type="hidden"   name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>" >
		
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" ></td>
		</tr>
		</table>	
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong class="error">ʶҹС�����ͺ</strong> :</td>
			<td  valign="top" rowspan="2">

				<select name="hRecieveStatus" multiple size="3">					
					<option value="1" <?if($objStockCar->getRecieveStatus() ==1) echo "selected";?>> ���ͺ�����Ѻö����
					<option value="2" <?if($objStockCar->getRecieveStatus() ==2) echo "selected";?>> ���ͺ���ǽҡ�ʹ
					<option value="3" <?if($objStockCar->getRecieveStatus() ==3) echo "selected";?>> ���ͺ���Ǥ�ҧ��ǹ� �Ѻö����
					<option value="4" <?if($objStockCar->getRecieveStatus() ==4) echo "selected";?>> ���ͺ���Ǥ�ҧ��ǹ� �ҡ�ʹ
					<option value="5" <?if($objStockCar->getRecieveStatus() ==5) echo "selected";?>> �ѧ������ͺ ��ҧ��ǹ�
					<option value="6" <?if($objStockCar->getRecieveStatus() ==6) echo "selected";?>> �ѧ������ͺ����
				</select>
			</td>		
			<td   class="i_background03" valign="top"><strong class="error">�ѹ����Ѻ�ͺö</strong></td>
			<td  valign="top">				
				<table>
				<tr>
					<td>
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Day value="<?=$Day?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hour2 value="<?=$Hour2?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute2 value="<?=$Minute2?>"></td>			
						</tr>
						</table>
					</td>
				</tr>
				</table>		
				
			</td>
		</tr>
		<tr>
			<Td></TD>
			<td  class="i_background03" valign="top"><strong class="error">�Ţ�������ͺ</strong> :</td>
			<td  valign="top">
				<input type="textbox" name="hRecieveNumber" size="15" value="<?=$objStockCar->getRecieveNumber();?>">
			</td>		
		</tr>
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong></td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">				
				<table>
				<tr>
					<td>
					  	<table cellspacing="0" cellpadding="0">
						<tr>
							<td><INPUT align="middle" size="2" maxlength="2"   name=Day1 value="<?=$Day1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Month1 value="<?=$Month1?>"></td>
							<td>-</td>
							<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year1 value="<?=$Year1?>"></td>
							<td>&nbsp;</td>
							<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year1,Day1, Month1, Year1,popCal);return false"></td>		
							<td>&nbsp;&nbsp;&nbsp;&nbsp;����&nbsp;&nbsp;</td>						
							<td><INPUT align="middle" size="2" maxlength="2"  name=Hour1 value="<?=$Hour1?>"></td>
							<td width="2" align="center">:</td>
							<td><INPUT align="middle" size="2" maxlength="2"  name=Minute1 value="<?=$Minute1?>"></td>		
						</tr>
						</table>
					
					</td>
				</tr>
				</table>					
				
			</td>
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"></td>
			<td align="right" >

			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td colspan="3">
				<table>
				<tr>
					<td valign="top">
						<?=DropdownTitle("hTitle",$objCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>										
					<INPUT  name="hCustomerName" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerId.value != '' ) frm01.hCustomerTitleId.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerId.value='';"  size=50 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hCustomerTitleId",$objCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
			</td>
			
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"  ><input type="text" name="hAddress" size="50"  value="<?=$objCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td width="10%" class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>						
			<td  width="40%" valign="top"><input type="text" readonly size="6" name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><input type="text" name="hTumbon" onKeyDown="if(event.keyCode==13 && frm01.hTumbonCode.value != '' ) frm01.hAmphur.focus();if(event.keyCode !=13 ) frm01.hTumbonCode.value='';"  size="30"  value="<?=$objCustomer->getTumbon();?>"></td>
			<td  width="10%" valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>
			<td  width="40%" valign="top"><input type="text" readonly size="4" name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><input type="text" name="hAmphur" onKeyDown="if(event.keyCode==13 && frm01.hAmphurCode.value != '' ) frm01.hProvince.focus();if(event.keyCode !=13 ) frm01.hAmphurCode.value='';"  size="30"  value="<?=$objCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="text" size="2" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><input type="text" name="hProvince" onKeyDown="if(event.keyCode==13 && frm01.hProvinceCode.value != '' ) frm01.hZip.focus();if(event.keyCode !=13 ) frm01.hProvinceCode.value='';"  size="30"  value="<?=$objCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="text" size="5" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><input type="text" name="hZip" onKeyDown="if(event.keyCode==13 && frm01.hZipCode.value != '' ) frm01.hCarNumber.focus();if(event.keyCode !=13 ) frm01.hZipCode.value='';"  size="30"  value="<?=$objCustomer->getZip();?>"></td>
		</tr>	
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"></td>
			<td align="right" >
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>


<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="20%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>			
			<td width="30%" valign="top" >
				<input type="hidden" readonly size=3 name="hStockCarIdTemp" value="<?=$objOrder->getStockCarId()?>">
				<input type="hidden" readonly size=3 name="hStockCarId" value="<?=$objOrder->getStockCarId()?>">
				<input type="text" name="hCarNumber" size="30"  value="<?=$objStockCar->getCarNumber()?>">
			</td>
			<td width="20%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><label id="hEngineNumber"><?=$objStockCar->getEngineNumber()?></label></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<label id="hCarModel01"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<label id="hCarSeries01"><?=$objCarSeries->getTitle()?></label>								
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>Model</strong> </td>
			<td>
				<label id="hModel"><?=$objCarSeries->getModel()?></label>	
			</td>
			<td class="i_background03" valign="top"><strong>�ѹ��� TBR</strong></td>
			<td ><input type="text" name="hTMBNumber" size="10"  value="<?=$objOrder->getTMBNumber()?>"></td>
		</tr>						
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hGear">
					<option value="Auto" <?if($objStockCar->getGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objStockCar->getGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<select  name="hCarType">
					<option value="1" <?if($objCarSeries->getCarType() == "1") echo "selected"?>>ö��
					<option value="2" <?if($objCarSeries->getCarType() == "2") echo "selected"?>>ö�к�
					<option value="3" <?if($objCarSeries->getCarType() == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($objCarSeries->getCarType() == "4") echo "selected"?>>ö���
				</select>			
			</td>		
			<td class="i_background03" width="20%"><strong>�Ҥ�ö</strong></td>
			<td width="30%">
				<input type="text" name="hOrderPrice" size="20"  value="<?=$objOrder->getOrderPrice()?>"> �ҷ				
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
<?if ($strMode == "Update"){?>
	<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();" >
<?}else{?>
	<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button"  onclick="return check_submit();" >
<?}?>
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" value="¡��ԡ��¡��" onclick="window.location='ccardList.php'" class="button">			
	<br><br>
	</td>
</tr>
</table>
</form>

<script>
	new CAPXOUS.AutoComplete("hCustomerName", function() {return "ccardAutoDealer.php?q=" + this.text.value;});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {return "ccardAutoOrderNumber.php?q=" + this.text.value;});	
	
	new CAPXOUS.AutoComplete("hTumbon", function() {var str = this.text.value;	if( str.length > 2){return "ccardAutoTumbon.php?q=" + this.text.value;}});	
	
	new CAPXOUS.AutoComplete("hAmphur", function() {var str = this.text.value;if( str.length > 2){return "acardAutoAmphur.php?q=" + this.text.value;}});		
	
	new CAPXOUS.AutoComplete("hZip", function() {var str = this.text.value;	if( str.length > 2){return "ccardAutoTumbon.php?q=" + this.text.value;}});			
	
	new CAPXOUS.AutoComplete("hProvince", function() {
		var str = this.text.value;	
		if( str.length > 2){	
			return "acardAutoProvince.php?q=" + this.text.value;
		}
	});				
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "ccardAutoSale.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hCarNumber", function() {
		return "ccardAutoStockCarDealer.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hStockRedName", function() {
		return "ccardAutoRedCode.php?q=" + this.text.value;
	});	
	
</script>
<?
	include("h_footer.php")
?>
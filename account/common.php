<?
include_once("../conf_inside.php");
define("URL_BASE","account");

include_once(PATH_CLASS."clUser.php");
include_once(PATH_CLASS."clAdmin.php");
include_once(PATH_CLASS."clMember.php");
include_once(PATH_CLASS."clDepartment.php");
include_once(PATH_CLASS."clTeam.php");
include_once(PATH_CLASS."clModule.php");
include_once(PATH_CLASS."clMemberModule.php");
include_once(PATH_CLASS."clMemberLog.php");
include_once(PATH_CLASS."clCustomerGroup.php");
include_once(PATH_CLASS."clCustomerGrade.php");
include_once(PATH_CLASS."clCustomerEvent.php");
include_once(PATH_CLASS."clContact.php");
include_once(PATH_CLASS."clCustomer.php");
include_once(PATH_CLASS."clCustomerContact.php");
include_once(PATH_CLASS."clCustomerDuplicate.php");
include_once(PATH_CLASS."clCustomerTitle.php");

include_once(PATH_CLASS."clSale.php");
include_once(PATH_CLASS."clCarType.php");
include_once(PATH_CLASS."clCarModel.php");
include_once(PATH_CLASS."clCarSeries.php");
include_once(PATH_CLASS."clCarColor.php");
include_once(PATH_CLASS."clCarMargin.php");
include_once(PATH_CLASS."clCarMarginItem.php");
include_once(PATH_CLASS."clCustomerExpectCar.php");
include_once(PATH_CLASS."clCustomerUseCar.php");
include_once(PATH_CLASS."clCarPremium.php");

include_once(PATH_CLASS."clAreaProvince.php");
include_once(PATH_CLASS."clAreaAmphur.php");
include_once(PATH_CLASS."clAreaTumbon.php");
include_once(PATH_CLASS."clAreaPostcode.php");

include_once(PATH_CLASS."clStockRed.php");
include_once(PATH_CLASS."clStockRedItem.php");

include_once(PATH_CLASS."clOrder.php");
include_once(PATH_CLASS."clOrderStock.php");
include_once(PATH_CLASS."clOrderStockOther.php");
include_once(PATH_CLASS."clCashierReserve.php");
include_once(PATH_CLASS."clCashierReserveItem.php");

include_once(PATH_CLASS."clCompany.php");
include_once(PATH_CLASS."clStockCargo.php");
include_once(PATH_CLASS."clStockPlace.php");
include_once(PATH_CLASS."clStockProductType.php");
include_once(PATH_CLASS."clStockProductGroup.php");
include_once(PATH_CLASS."clStockProductPack.php");
include_once(PATH_CLASS."clStockPaper.php");
include_once(PATH_CLASS."clStockProduct.php");
include_once(PATH_CLASS."clStockDealer.php");
include_once(PATH_CLASS."clStock.php");
include_once(PATH_CLASS."clStockItem.php");
include_once(PATH_CLASS."clSPCM.php");
include_once(PATH_CLASS."clStockPremium.php");
include_once(PATH_CLASS."clStockPremiumItem.php");
include_once(PATH_CLASS."clStockCar.php");
include_once(PATH_CLASS."clStockCarItem.php");
include_once(PATH_CLASS."clStockReason.php");
include_once(PATH_CLASS."clDealer.php");
include_once(PATH_CLASS."clPr.php");
include_once(PATH_CLASS."clStockInsure.php");
include_once(PATH_CLASS."clStockInsureDetail.php");

include_once(PATH_CLASS."clFundCompany.php");
include_once(PATH_CLASS."clInsureCompany.php");
include_once(PATH_CLASS."clPrbCompany.php");
include_once(PATH_CLASS."clCom.php");
include_once(PATH_CLASS."clInfo.php");
include_once(PATH_CLASS."clInsureBroker.php");
include_once(PATH_CLASS."clInsure.php");
include_once(PATH_CLASS."clInsureCar.php");
include_once(PATH_CLASS."clInsureItem.php");
include_once(PATH_CLASS."clInsureItemDetail.php");
include_once(PATH_CLASS."clInsureFree.php");
include_once(PATH_CLASS."clInsureFee.php");
include_once(PATH_CLASS."clInsureType.php");
include_once(PATH_CLASS."clInsureCom.php");
include_once(PATH_CLASS."clInsureAcc.php");
include_once(PATH_CLASS."clInsureTeam.php");
include_once(PATH_CLASS."clInsureCustomer.php");
include_once(PATH_CLASS."clInsurePayment.php");
include_once(PATH_CLASS."clPaymentSubject.php");
include_once(PATH_CLASS."clBank.php");

include_once(PATH_CLASS."clMonitor.php");
include_once(PATH_CLASS."clErr.php");
include_once(PATH_CLASS."clMsg.php");
include_once(PATH_CLASS."clMsgReply.php");

include_once(PATH_CLASS."clOrderTransfer.php");
include_once(PATH_CLASS."clBookBank.php");

include_once(PATH_CLASS."utilityFunction.php");
include_once(PATH_CLASS."incUtility.php");
include_once(PATH_INCLUDE."utilityString.php");


include_once(PATH_CLASS."clPaymentType.php");
include_once(PATH_CLASS."clInsureFee.php");
include_once(PATH_CLASS."clInsureFee1.php");
include_once(PATH_CLASS."clInsureItemTrack.php");
include_once(PATH_CLASS."clInsureMailItem.php");
include_once(PATH_CLASS."clInsureMailItemDetail.php");
include_once(PATH_CLASS."clInsureForm.php");
include_once(PATH_CLASS."clRegistryPrice.php");

include("../include/date.php");

define("PATH_WEB",PATH_BASE."account/");

$objUser = new Member();
if (!$objUser->isLogged()){
	header("Location: ../index.php?hMsg=session+expire");
	exit;
}else{
	//check right access
	$objMemberModule = new MemberModule();
	$objMemberModule->loadByConditionMenu(" member_id = $sMemberId AND module_type=1  AND module_code = '".$hModuleCode."'  ");
	//echo $objMemberModule->getStatus();
	
	if( $objMemberModule->getStatus() == "0" ){
		header("Location:message.php?hType=no_access");
		exit;
	}else{
		$arrModuleAccess = explode(":",$objMemberModule->getAccess());
	}
}

$currentFile = $_SERVER["PHP_SELF"];
$parts = Explode('/', $currentFile);
if(strpos($parts[count($parts) - 1],"Auto")){
	header('Content-Type: text/html; charset=utf-8');
}

if(strpos($parts[count($parts) - 1],"auto_")){
	header('Content-Type: text/html; charset=utf-8');
}

?>
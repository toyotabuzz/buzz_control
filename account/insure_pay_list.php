<?
include("common.php");
include("incUtility.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$objBankList = new BankList();
$objBankList->setPageSize(0);
$objBankList->setSortDefault(" title ASC ");
$objBankList->load();

$objBookBankList = new BookBankList();
$objBookBankList->setFilter(" company_id=14 ");
$objBookBankList->setPageSize(0);
$objBookBankList->setSortDefault(" bank ASC ");
$objBookBankList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;
//$pstrCurrentUrl = "stockProduct.php";

$objStockProductList = New StockProductList;

if(isset($hSearch)){

	if ( $hKeyword!="" ){
		$pstrCondition  = " ( ( title LIKE '%".$hKeyword ."%') OR ( stock_number LIKE '%".$hKeyword ."%') ) ";	
	}
	
	$hStartDate = $Year."-".$Month."-".$Day;
	$hEndDate = $Year01."-".$Month01."-".$Day01;
	
	if($hStartDate != "--" AND $hStartDate != "0000-00-00"){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( OP.check_date >= '$hStartDate' and OP.check_date <='$hEndDate' )   ";
		}else{
			$pstrCondition =" ( OP.check_date >= '$hStartDate' and OP.check_date <='$hEndDate' )   ";
		}
	}
	
	$hStartDate1 = $hInsureYear."-".$hInsureMonth."-".$hInsureDay;
	$hEndDate1 = $hInsureYear01."-".$hInsureMonth01."-".$hInsureDay01;
	
	if($hStartDate1 != "--" AND $hStartDate1 != "0000-00-00"){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( I.payment_date >= '$hStartDate1' and I.payment_date <='$hEndDate1' )   ";
		}else{
			$pstrCondition =" (  I.payment_date >= '$hStartDate1' and I.payment_date <='$hEndDate1' )   ";
		}
	}
	
	if($hCode != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( C.code LIKE '%$hCode%' )   ";
		}else{
			$pstrCondition =" ( C.code LIKE '%$hCode%' )   ";
		}
	}
	
	if($hPrice != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( OP.price = '$hPrice' )   ";
		}else{
			$pstrCondition =" ( OP.price = '$hPrice' )   ";
		}
	}
	
	if($hName != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND (  ( IC.firstname LIKE '%$hName%' )  or   ( IC.lastname LIKE '%$hName%' )  )  ";
		}else{
			$pstrCondition =" (  ( IC.firstname LIKE '%$hName%' )  or   ( IC.lastname LIKE '%$hName%' )  )     ";
		}
	}
	
	if($hName != ""){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( IC.firstname  LIKE '%$hName%'  OR  IC.lastname  LIKE '%$hName%'  )   ";
		}else{
			$pstrCondition =" (  IC.firstname  LIKE '%$hName%' OR  IC.lastname  LIKE '%$hName%'  )   ";
		}
	
	}
	
	if($hPaymentBank != "0"){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( OP.bank_id = '$hPaymentBank' )   ";
		}else{
			$pstrCondition =" ( OP.bank_id = '$hPaymentBank' )   ";
		}
	
	}
	
	
	if($hPaymentBookBank != "0"){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( OP.bookbank_id = '$hPaymentBookBank' )   ";
		}else{
			$pstrCondition =" ( OP.bookbank_id = '$hPaymentBookBank' )   ";
		}
	}
	
	
	if($hCompanyId > 0 ){
		if($pstrCondition != ""){
			$pstrCondition .=" AND ( INS.company_id = '$hCompanyId' )   ";
		}else{
			$pstrCondition =" ( INS.company_id = '$hCompanyId' )   ";
		}
	}
	
	if($pstrCondition != ""){
		$pstrCondition.=" AND OP.payment_type_id = 4  AND acc_check='' AND I.payment_status =''  ";
	}else{
		$pstrCondition.="  OP.payment_type_id = 4 AND acc_check='' AND I.payment_status =''  ";
	}
	
	$sConditionPay = $pstrCondition;	
	session_register("sConditionPay");
	
	
}else{

	if($sConditionPay != ""){
		$pstrCondition = $sConditionPay;
	}else{
	
		$pstrCondition ="  ( INS.company_id = '$sCompanyId' AND OP.payment_type_id = 4  AND acc_check='' AND I.payment_status =''  )   ";
		$sConditionPay = $pstrCondition;	
		session_register("sConditionPay");
	}
}

if (isset($hSubmitStock) )   {  //Request to delete some StockProduct release
    $objInsureItem = New InsurePayment;
    $intCountDelete = 0;
    for ($i = 0; $i <= $hCountTotal; $i++) {
        if ( $hStock[$i] )  //Delete is checked.
        {
				$objInsureItem->setInsurePaymentId($hStock[$i]);
				$objInsureItem->setAccCheck("yes");
				$objInsureItem->setAccCheckDate(date("Y-m-d"));
				$objInsureItem->setAccCheckRemark($hStockRemark[$i]);
				$objInsureItem->setAccCheckBy($sMemberId);
				$objInsureItem->updateAcc();
				++$intCountDelete;
	    }
    }
    if ($pstrMsg == "" ) $pstrMsg .= "*** ź " .$intCountDelete . " ��¡�� ***";
    unset($objInsureItem);
}

echo $pstrCondition;

if ($hSort == "") $hSort = " check_date  ASC ";
$objStockProductList = new InsurePaymentList();
$objStockProductList->setFilter($pstrCondition);
$objStockProductList->setPageSize(PAGE_SIZE);
$objStockProductList->setPage($hPage);
$objStockProductList->setSortDefault(" check_date  ASC");
$objStockProductList->setSort($hSort);
$objStockProductList->load();

$pCurrentUrl = "stock_pay_list.php?hStockProductGroupId=$hStockProductGroupId&hStockProductTypeId=$hStockProductTypeId&hCarType=$hCarType&hKeyword=$hKeyword&hPage=$hPage&hSearch=$hSearch";
	// for paging only (sort resets page viewing)

$pageTitle = "6. �к��ѭ�ջ�Сѹ���";
$pageContent = "6.3 ��Ǩ�ͺ��¡���͹�Թ";
	
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT><br>
<br>

<table align="center" cellspacing="2" cellpadding="2" border="0" class="search">
<form  name="frm01" action="insure_pay_list.php" method="post"  >
<tr>
	<td>�ѹ���Ѵ���� :</td>
	<td align="center">	
	  	<table cellspacing="0" cellpadding="0">
		<tr>			
			<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay value="<?=$hInsureDay?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth value="<?=$hInsureMonth?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hInsureYear value="<?=$hInsureYear?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear,hInsureDay, hInsureMonth, hInsureYear,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=hInsureDay01 value="<?=$hInsureDay01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hInsureMonth01 value="<?=$hInsureMonth01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hInsureYear01 value="<?=$hInsureYear01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hInsureYear01,hInsureDay01, hInsureMonth01, hInsureYear01,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
	<td>�ѹ����͹ :</td>
	<td align="center">	
	  	<table cellspacing="0" cellpadding="0">
		<tr>			
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
	</tr>
	<tr>
		<td>����¹ö :</td>
		<td>
			<input type="text" name="hCode" size="40" value="">			
		</td>
		<td>����-���ʡ��</td>
		<td>
			<input type="text" name="hName" size="40" value="">			
		</td>
	</tr>
	<tr>
		<td>�͹�ҡ��Ҥ�� :</td>
		<td><?$objBankList->printSelectInsure("hPaymentBank",$hPaymentBank,"���͡��¡�ø�Ҥ��");?></td>
		<td>�͹��Һѭ�ո�Ҥ�� :</td>
		<td><?$objBookBankList->printSelectInsure("hPaymentBookBank",$hPaymentBookBank,"���͡�ѭ�� Buzz");?></td>
	</tr>
	<tr>
		<td>�ӹǹ�Թ :</td>
		<td>
			<input type="text" name="hPrice" size="40" value="">
		</td>
		<td>�Ң� :</td>
		<td>
			<?
			$objCList = new CompanyList();
			$objCList->setPageSize(0);
			$objCList->load();
			$objCList->printSelect("hCompanyId",$hCompanyId,"��س����͡�Ң�");
			?>			
		</td>

	</tr>

	<tr>
	
	<td align="center" colspan="4"><input type="submit" name="hSearch" value="Search"></td>
</tr>		
</table>

</form>	



<table border="0" width="98%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="50%">
	<?
		$objStockProductList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objStockProductList->printPaging($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objStockProductList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="right"></td>
</tr>
</table>
<form name="frm" action="insure_pay_list.php" method="POST">
<table border="0" width="1100" align="center" cellpadding="1" cellspacing="2">
<tr>
	<td  align="center" class="ListTitle">�ӴѺ</td>	
	<td align="center" class="ListTitle">�Ң�</td>	
	<td align="center" class="ListTitle">Sale</td>
	<td align="center" class="ListTitle">�ѹ��� Sale<br>����¡��</td>	
	<td align="center" class="ListTitle">�ѹ����͹�Թ</td>	
	<td align="center" class="ListTitle">�Ǵ���</td>	
	<td align="center" class="ListTitle">�١���</td>	
	<td align="center" class="ListTitle">����¹ö</td>	
	<td  align="center" class="ListTitle">�ѭ�պ�������</td>
	<td  align="center" class="ListTitle">�Ң�</td>
	<td  align="center" class="ListTitle" width="100">�ӹǹ�Թ</td>
	<td align="center" class="ListTitle">͹��ѵ�</td>
	<td  align="center" class="ListTitle">�����˵�<br>���͹��ѵ�</td>
	<td  align="center" class="ListTitle">��������´</td>
</tr>
	<?
	$i=0;
	forEach($objStockProductList->getItemList() as $objItem) {
	
	$objInsureItem = new insureItem();
	$objInsureItem->set_insure_item_id($objItem->getOrderId());
	$objInsureItem->load();	
	
	$objInsure = new insure();
	$objInsure->set_insure_id($objInsureItem->get_insure_id());
	$objInsure->load();
	
	$objInsureCar = new insureCar();
	$objInsureCar->set_car_id($objInsure->get_car_id());
	$objInsureCar->load();	

	$objC = new InsureCustomer();
	$objC->setInsureCustomerId($objInsureCar->get_insure_customer_id());
	$objC->load();
	
	$objS = new Member();
	$objS->setMemberId($objInsure->get_sale_id());
	$objS->load();	
	
	$objB = new Bank();
	$objB->setBankId($objItem->getBankId());
	$objB->load();
	
	$objIC = new InsureCar();	
	$objIC->set_car_id($objInsure->get_car_id());
	$objIC->load();	
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td align="center"><?=$i+1?></td>
	<td align="center" >
		<?
		$objCompany  = new Company();
		$objCompany->setCompanyId($objInsure->get_company_id());
		$objCompany->load();	
		echo $objCompany->getShortTitleEn();?>
	</td>	
	<td >&nbsp;<?=$objS->getFirstname()?></td>
	<?$arrSaleDate = explode(" ",$objItem->getSaleDate());?>
	<td align="center" >&nbsp;<?=formatShortDate($arrSaleDate[0])?><br><?=$arrSaleDate[1]?></td>	
	<td align="center" >&nbsp;<?=formatShortDate($objItem->getCheckDate())?></td>	
	<td align="center" >&nbsp;<?=$objInsureItem->get_payment_order()."/".$objInsure->get_pay_number()?></td>	
	<td >&nbsp;<?=$objC->getFirstname()." ".$objC->getLastname()?></td>	
	<td align="center" >&nbsp;<?=$objIC->get_code()?></td>	

	<td>
		<?
		$objBB  = new BookBank();
		$objBB->set_bookbank_id($objItem->getBookBankId());
		$objBB->load();	
		echo $objBB->get_bank();?>
	</td>
	<td><?=$objBB->get_branch();?></td>
	<td align="right" >&nbsp;<?=number_format($objItem->getPrice(),2)?></td>	
	<td align="center" ><input type="checkbox" name="hStock[<?=$i?>]" value="<?=$objItem->getInsurePaymentId()?>"></td>
	<td align="center" ><input size="10" type="text" name="hStockRemark[<?=$i?>]" value="<?=$objItem->getAccCheckRemark()?>"></td>
	<td align="center" ><input type="button" name="hSubmitStock" value="��������´" onclick="window.open('insure_detail.php?hInsureId=<?=$objInsure->get_insure_id()?>');"></td>
</tr>
	<?
	++$i;
	}
	?>
<tr>
	<td  align="right" class="ListTitle" colspan="13"></td>	
	<td align="center" class="ListTitle"><input type="Submit" name="hSubmitStock" value="͹��ѵ�" onclick="return confirmStock();"></td>
	<td  align="center" class="ListTitle"></td>
	<td align="center" class="ListTitle"></td>
</tr>	
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>
<?
	Unset($objStockProductList);
	include("h_footer.php")
?>
<script language="JavaScript">
	function confirmStock()
	{
		if(document.frm.hSaleId.value==0){
			alert("��س����͡�ŷ���ԡ");
			document.frm.hSaleId.focus();
			return false;
		}	
	
		if(confirm("�س��ͧ��÷���͹��ѵ���¡���������")){
			return true;
		}
		return false;
	}
</script>

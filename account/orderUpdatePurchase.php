<?
include("common.php");

$objOrder = new PR();
$objOrderItemList = new PRItemList();

$objItemList = New StockProductList;
$objItemList->setPageSize(0);
$objItemList->setSort(" title asc ");
$objItemList->load();

$objUser = new Member();
$objUser->setMemberId($hSaleId);
$objUser->load();

$mCount = $objItemList->getCount();

if (empty($hSubmit)) {
	if ($hId !="") {
		$objOrder->setPrId($hId);
		$objOrder->load();
		$strMode="Update";

		if ($hSort == "") $hSort = " pr_item_id ASC ";
		$objOrderItemList = new PRItemList();
		$objOrderItemList->setFilter(" P.pr_id = ".$hId);
		$objOrderItemList->setPageSize(0);
		$objOrderItemList->setSortDefault(" pr_item_id ASC");
		$objOrderItemList->load();
		
		$objUser = new Member();
		$objUser->setMemberId($objOrder->getMemberId());
		$objUser->load();

		$arrDate = explode("-",$objOrder->getPrDate());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getDeliveryDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$hOrderId = $objOrder->getOrderId();
		$hStockCarId = $objOrder->getStockCarId();
	} else {
		$arrDate = explode("-",date("Y-m-d"));
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		//gen code
		$objPRList = new PRList();
		$objPRList->setFilter(" pr_number like 'ACC%' ");
		$objPRList->setPageSize(1);
		$objPRList->setSort(" pr_id DESC ");
		$objPRList->load();
		if($objPRList->mCount > 0){
				forEach($objPRList->getItemList() as $objItem) {
					$arrCount = explode("-",$objItem->getPrNumber());
					$mCount=$arrCount[2];
				}
				$mCount++;
				$code = "ACC-".date("dmy")."-".$mCount;
		}else{
				$code = "ACC-".date("dmy")."-1";
		}
		
		$strMode="Add";
	}

} else {

	if (!empty($hSubmit)) {

            $objOrder->setPrId($hId);
			$objOrder->setMemberId($hSaleId);
			$objOrder->setPrNumber($hPrNumber);
			$objOrder->setOrderId($hOrderId);
			$objOrder->setRemark($hRemarkPR);
			$objOrder->setStockCarId($hStockCarId);
			$hDeliveryDate = $Year02."-".$Month02."-".$Day02;
			$objOrder->setDeliveryDate($hDeliveryDate);
			$hPrDate = $Year01."-".$Month01."-".$Day01;
			$objOrder->setPrDate($hPrDate);
			if(isset($hSubmit2)){
				$objOrder->setStatus(1);
			}else{
				$objOrder->setStatus(2);
			}
			$objOrder->setAccountRemark($hAccountRemark);
			$objOrder->setApproveBy($sMemberId);
			$objOrder->setCompanyId($sCompanyId);
			
    		$pasrErr = $objOrder->check($hSubmit);

			if (!isset($hAddAccountId) and !isset($hUpdateAccountId) and !isset($hSubmitLoad)){

			If ( Count($pasrErr) == 0 ){

				if ($strMode=="Update") {
					$objOrder->updateApproveAccount();
				} else {
					$hId=$objOrder->add();
				}
				unset ($objOrder);

				
				header("location:prApprovePurchaseList.php");
				exit;
			}else{
				$objOrder->init();
				
				if ($hId !="") {
					$objOrder->setPrId($hId);
					$objOrder->load();
					$strMode="Update";
			
					if ($hSort == "") $hSort = " pr_item_id ASC ";
					$objOrderItemList = new PRItemList();
					$objOrderItemList->setFilter(" P.pr_id = ".$hId);
					$objOrderItemList->setPageSize(0);
					$objOrderItemList->setSortDefault(" pr_item_id ASC");
					$objOrderItemList->load();
					
					$objUser = new Member();
					$objUser->setMemberId($objOrder->getMemberId());
					$objUser->load();
			
					$arrDate = explode("-",$objOrder->getPrDate());
					$Day01 = $arrDate[2];
					$Month01 = $arrDate[1];
					$Year01 = $arrDate[0];
					
					$arrDate = explode("-",$objOrder->getDeliveryDate());
					$Day02 = $arrDate[2];
					$Month02 = $arrDate[1];
					$Year02 = $arrDate[0];
					
					$hOrderId = $objOrder->getOrderId();					
					$hStockCarId = $objOrder->getStockCarId();
				} else {
					$objUser = new Member();
					$objUser->setMemberId($sMemberId);
					$objUser->load();
				
					$strMode="Add";
				}
				
			}//end check Count($pasrErr)
		}
		}//end check AddAccountId
}
$pageTitle = "2. �к����觫���";
$strHead03 = "�к����觫���";
$pageContent = "2.1  ��㺢ͫ���";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
	
		if (document.forms.frm01.hPrNumber.value=="")
		{
			alert("��س��к��Ţ���㺢ͫ���");
			document.forms.frm01.hPrNumber.focus();
			return false;
		} 	
	
		if (document.forms.frm01.Day01.value=="" || document.forms.frm01.Day01.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day01.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day01.value,1,31) == false) {
				document.forms.frm01.Day01.focus();
				return false;
			}
		} 			

		if (document.forms.frm01.Month01.value==""  || document.forms.frm01.Month01.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month01.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month01.value,1,12) == false){
				document.forms.frm01.Month01.focus();
				return false;
			}
		} 		
		
		if (document.forms.frm01.Year01.value==""  || document.forms.frm01.Year01.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear01.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year01.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year01.focus();
				return false;
			}
		} 							

		if (document.forms.frm01.Day02.value=="" || document.forms.frm01.Day02.value=="00")
		{
			alert("��س��к��ѹ������ͺ");
			document.forms.frm01.Day02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day02.value,1,31) == false) {
				document.forms.frm01.Day02.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month02.value==""  || document.forms.frm01.Month02.value=="00")
		{
			alert("��س��к���͹������ͺ");
			document.forms.frm01.Month02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month02.value,1,12) == false){
				document.forms.frm01.Month02.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year02.value==""  || document.forms.frm01.Year02.value=="0000")
		{
			alert("��س��кػշ�����ͺ");
			document.forms.frm01.hYear02.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year02.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year02.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hOrderNumber.value !="" && document.forms.frm01.hOrderId.value == "" )
		{
			alert("��س����͡�Ţ���㺨ͧ�ҡ�к�");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hCarNumber.value !="" && document.forms.frm01.hStockCarId.value == "" )
		{
			alert("��س����͡�Ţ����ͧ�ҡ�к�");
			document.forms.frm01.hCarNumber.focus();
			return false;
		} 		
	
	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>

<script language="JavaScript">
<!--
	function MM_goToURL() { //v3.0
	  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	function WindowOpen(Url,wName){
		window.open(Url,wName,"width=470,height=300,scrollbars=1");
	}
	
	function checkformList(){
		var title = document.frm.hStockProductId.value;
		var qty = document.frm.hQty.value;
		var pack = document.frm.hPack.value;
		var str_error = "";
		
		if(title == ""){str_error= "��س��к���¡��\n";}
		if(qty == ""){str_error= str_error+"��س��к���¨ӹǹ\n";}
		if(pack == ""){str_error= str_error+"��س��к�˹���\n";}
		
		if(str_error != ""){
			alert(str_error);
			return false;
		}else{
			return true;
		}
		
		
	}

//-->
</script>	
<br>
					<?if(isset($pasrErr) AND sizeof($pasrErr) > 0){?><br>
						<table cellpadding="0" cellspacing="0" width="450" align="center">
						<tr>
							<td width="4"><img src="images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
							<td background="images/box/white_top.gif"><img src="images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
							<td width="4"><img src="images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
						</tr>
						<tr>
							<td width="4" background="images/box/white_middle_left.gif"><img src="images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
							<td width="100%">
								<table width="100%">
								<tr>
									<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
									<td class="error" valign="top">
										<strong>Some error occur please check:</strong><br> 
										<table>
										<?foreach ($pasrErr as $key => $value) {
										?>
										<tr>
											<td class="error">- <?echo "$value";?></td>
										</tr>
										<?}?>
										</table>										
									</td>
								</tr>
								</table>							
							</td>
							<td width="4" background="images/box/white_middle_right.gif"><img src="images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
						</tr>
						<tr>
							<td width="4"><img src="images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
							<td background="images/box/white_bottom.gif"><img src="images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
							<td width="4"><img src="images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
						</tr>
						</table>	<br><br>			
					<?}?>		
<form name=frm01  action="orderUpdatePurchase.php?hId=<?=$hId?>" method="POST"  onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">	 
	  <input type="hidden" name="hMemberId" value="<?=$objOrder->getMemberId()?>">
  <table width="90%" border="0" cellspacing="1" cellpadding="2" align="center"
	<tr>
    <td  class="listTitle" align="right" valign="top"><strong>�����觢ͧ����</strong>:</td>
	<td  class="ListDetail" valign="top">
				<?if($objOrder->getMemberId() == ""){
				$objOrder->setMemberId($sMemberId);
				$objUser = new Member();
				$objUser->setMemberId($sMemberId);
				$objUser->load();
				
				?>
				<input type="hidden" name="hSaleId"  value="<?=$objOrder->getMemberId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hSaleId.value='';"   name="hSale"  size=40 value="<?=$objUser->getFirstname()."  ".$objUser->getLastname()?>">
				<?}else{?>
				<input type="hidden" name="hSaleId"  value="<?=$objOrder->getMemberId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSaleId.value != '' ) frm01.hIDCard.focus();if(event.keyCode !=13 ) frm01.hSaleId.value='';"   name="hSale"  size=40 value="<?=$objUser->getFirstname()."  ".$objUser->getLastname()?>">
				<?}?>
    </td>
    <td  class="listTitle" align="right" valign="top">�Ţ���㺢ͫ���</td>
	<td  class="ListDetail" valign="top">
		<?if($objOrder->getPrNumber() == ""){?>
		<INPUT type="hidden" name="hPrNumber" size=25  value="<?=$code?>">
		<?=$code?>
		<?}else{?>
		<INPUT type="hidden" name="hPrNumber" size=25  value="<?=$objOrder->getPrNumber()?>">		
		<?=$objOrder->getPrNumber()?>
		<?}?>
    </td>
  	</tr>	
	<tr>
    <td  class="listTitle" align="right"><strong>�ѹ������</strong>:</td>
	<td  class="ListDetail">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day01 value="<?=$Day01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false"></td>		
		</tr>
		</table>
    </td>
    <td  class="listTitle" align="right"><strong>�ѹ����ͧ���</strong>:</td>
	<td  class="ListDetail">
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT align="middle" size="2" maxlength="2"   name=Day02 value="<?=$Day02?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false"></td>		
		</tr>
		</table>
    </td>
	</tr>

	</table>
	<br><br>
  <table width="90%" border="0" cellspacing="1" cellpadding="2" align="center">
	<tr>
    <td colspan="2">�ó�����Թ�������Ѻö���ͺ��س��к� (���͹��ʡ���١��ҹ͡, ���ö, ��ö, ����¹, �Ţ�ѧ, sale ���͢����ŷ�����)</td>
	</tr>
	<tr>
    <td  class="listTitle" align="right" width="15%" valign="top"><strong>�����˵�</strong>:</td>
	<td  class="ListDetail" width="70%" >
		<textarea rows="5" cols="100" name="hRemarkPR"><?=$objOrder->getRemark()?></textarea>
    </td>
	</table>	
	<br><br>
  <table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
  	<tr><td><strong>�����ô��觢ͧ�����¡�â�ҧ��ҧ���</strong></td></tr>
   	<tr>    
    <td class="input">		
		<table border="0" width="100%" align="center" cellpadding="2" cellspacing="1">


			<?if(isset($hSubmitLoad)){
				if($sStockProductId != ""){	
					$arrStockProductId  = explode(",::",$sStockProductId);
					$i=sizeof($arrStockProductId);
				}else{
					$i=0;
				}
				$objOrderItemList = new OrderStockList();
				$objOrderItemList->setFilter("  P.tmt=0 AND  O.stock_car_id = ".$hStockCarId);
				$objOrderItemList->setPageSize(0);
				$objOrderItemList->setSortDefault(" SP.title ASC");
				$objOrderItemList->loadStock();
					forEach($objOrderItemList->getItemList() as $objItem) {
						
						if($i==0){
							$sStockProductId  = $objItem->getStockProductId();
							$sOrderStockId =  $objItem->getOrderStockId();
							$sQty =  $objItem->getQty();
							$sPrice =  $objItem->getPrice();							
						}else{
							$sStockProductId.= ":><:".$objItem->getStockProductId();
							$sOrderStockId.= ":><:".$objItem->getOrderStockId();							
							$sQty.=  ":><:".$objItem->getQty();
							$sPrice.=  ":><:".$objItem->getPrice();
							
						}
						$i++;
					}?>
			<?}?>	

		
		<div align="center" class="error"><?=$errPack.$errStockProductId.$errQty?></div>
		<input type="hidden" name="sStockProductId" value="<?=$sStockProductId?>">
		<input type="hidden" name="sDealerId" value="<?=$sDealerId?>">
		<input type="hidden" name="sDealer" value="<?=$sDealer?>">
		<input type="hidden" name="sOrderStockId" value="<?=$sOrderStockId?>">
		<input type="hidden" name="sQty" value="<?=$sQty?>">
		<input type="hidden" name="sRemark" value="<?=$sRemark?>">
		<input type="hidden" name="sPrice" value="<?=$sPrice?>">
		<?if (!$hId){?>

			<?if ($sStockProductId != ""){
				$arrStockProductId = explode(":><:",$sStockProductId);
				$arrDealerId = explode(":><:",$sDealerId);
				$arrDealer = explode(":><:",$sDealer);
				$arrOrderStockId = explode(":><:",$sOrderStockId);
				$arrQty = explode(":><:",$sQty);
				$arrRemark = explode(":><:",$sRemark);
				$arrPrice = explode(":><:",$sPrice);
			?>

				<tr>				
					<td width="20%" align="center" class="listTitle" >��¡��</td>								
					<td width="10%" align="center" class="listTitle">˹���</td>					
					<td width="20%" align="center" class="listTitle" >��ҹ���</td>
					<td width="10%" align="center" class="listTitle">�ӹǹ</td>
					<td width="10%" align="center" class="listTitle">�Ҥ�</td>					
					
					<td width="20%" align="center" class="listTitle">�����˵�</td>
				</tr>
				<?
				for($i=0;$i<sizeof($arrStockProductId);$i++){
					$objItem = new StockProduct();
					$objItem->setStockProductId($arrStockProductId[$i]);
					$objItem->load();
					if($arrDealerId[$i] != ""){
						$objDealer = new Dealer();
						$objDealer->setDealerId($arrDealerId[$i]);
						$objDealer->load();
						$hDealerTemp = $objDealer->getTitle();
					}else{
						$hDealerTemp = $arrDealer[$i];
					}
				?>
				<input type="hidden" name="hPrItemId_<?=$i?>" value="" >
				<input type="hidden" name="hStockProductId_<?=$i?>" value="<?=$arrStockProductId[$i];?>" >
				<input type="hidden" name="hOrderStockId_<?=$i?>" value="<?=$arrOrderStockId[$i];?>" >
				<tr>					
					<td class="ListDetail" valign="top"><?=$objItem->getTitle();?></td>
					<td align="center" class="ListDetail" valign="top"><?=$objItem->getStockProductPackDetail();?></td>		
					<td class="ListDetail"><input type="hidden" name="hDealerId_<?=$i?>" value="<?=$arrDealerId[$i];?>" ><input type="text" size="30" name="hDealer_<?=$i?>" value="<?=$hDealerTemp;?>"></td>
					<td align="right" class="ListDetail"><input type="text" size="3" style="text-align:right" name="hQty_<?=$i?>" value="<?=$arrQty[$i];?>"></td>
					<td align="right" class="ListDetail"><input type="text" size="10"  style="text-align:right" name="hPrice_<?=$i?>" value="<?=$arrPrice[$i];?>"></td>
					
					<td class="ListDetail"><input type="text" name="hRemark_<?=$i?>" value="<?=$arrRemark[$i]?>" size="20"></td>
				</tr>				
				<?}?>

				<tr>
					<td colspan="4" align="center" class="listTitle"></td>
					<td align="center" class="listTitle"></td>
					<td class="listTitle"></td>
				</tr>
				</table>
			<?}?>
	
		<?}else{
			if ($objOrderItemList->getCount() > 0 ){
		?>	
				<tr>				
					<td width="20%" align="center" class="listTitle" >��¡��</td>								
					<td width="10%" align="center" class="listTitle">˹���</td>					
					<td width="20%" align="center" class="listTitle" >��ҹ���</td>
					<td width="10%" align="center" class="listTitle">�ӹǹ</td>
					<td width="10%" align="center" class="listTitle">�Ҥ�</td>					
					
					<td width="20%" align="center" class="listTitle">�����˵�</td>
				</tr>
				<?
				$i=0;
				forEach($objOrderItemList->getItemList() as $objItem) {
				$objStockProduct  = new StockProduct();
				$objStockProduct->setStockProductId($objItem->getStockProductId());
				$objStockProduct->load();
				?>
				<input type="hidden" name="hPrItemId_<?=$i?>" value="<?=$objItem->getPrItemId();?>" >
				<input type="hidden" name="hStockProductId_<?=$i?>" value="<?=$objItem->getStockProductId();?>" >
				<input type="hidden" name="hOrderStockId_<?=$i?>" value="<?=$objItem->getOrderStockId();?>" >
				<tr>
					<td class="ListDetail" valign="top"><?=$objItem->getStockProductDetail();?></td>										
					<td align="center" class="ListDetail" valign="top"><?=$objStockProduct->getStockProductPackDetail();?></td>					
					<td class="ListDetail" valign="top"><input type="hidden" name="hDealerId_<?=$i?>" value="<?=$objItem->getDealerId();?>" ><input type="text" size="30" name="hDealer_<?=$i?>" value="<?=$objItem->getDealerDetail();?>"></td>
					<td align="center" class="ListDetail" valign="top"><input type="text" size="3" style="text-align:right" name="hQty_<?=$i?>" value="<?=$objItem->getQty();?>"></td>	
					<td align="center" class="ListDetail" valign="top"><input type="text" size="10"  style="text-align:right" name="hPrice_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>	
					
					<td align="center" class="ListDetail" valign="top"><input type="text" name="hRemark_<?=$i?>" value="<?=$objItem->getRemark();?>" size="30"></td>
				</tr>
				<?
					++$i;
				}
				?>

				<input type="hidden" name="hCountTotal" value="<?=$i?>">

				<tr>
					<td colspan="4" align="center" class="listTitle"></td>
					<td align="center" class="listTitle"></td>
					<td class="listTitle"></td>
				</tr>
				</table>

		<?}
		}?>

			
	</td>
	</tr>	
	</table>
		<br><br>
  <table width="90%" border="0" cellspacing="1" cellpadding="2" align="center">
	<tr>
    <td colspan="2">�����˵ء��͹��ѵԨҡ�ѭ��</td>
	</tr>
	<tr>
    <td  class="listTitle" align="right" width="15%" valign="top"><strong>�����˵�</strong>:</td>
	<td  class="ListDetail" width="70%" >
		<textarea rows="5" cols="100" name="hAccountRemark"><?=$objOrder->getAccountRemark()?></textarea>
    </td>
	</table>	
	<br><br>
	<table width="90%" align="center">
    <tr> 
		<td align="center">
        <input type="hidden" name="hSubmit" value="<?=$strMode?>">

		<input type="submit" name="hSubmit1" value="͹��ѵ���¡���ͷ����觫���" class="button" onclick="return checkValue();">
		&nbsp;&nbsp;&nbsp;&nbsp;		
		<input type="submit" name="hSubmit2" value="���͹��ѵ���¡��" class="button" onclick="return checkValue();">		
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='prApprovePurchaseList.php'">
      </td>
    </tr>
  </table>
</form>
	
<script>
	new CAPXOUS.AutoComplete("hStockProduct", function() {
		return "orderAutoStockProduct.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hDealer", function() {
		return "orderAutoDealer.php?q=" + this.text.value+"&r="+document.frm01.hStockProductId.value;
	});
	
	new CAPXOUS.AutoComplete("hOrderNumber", function() {
		return "orderAutoOrder.php?q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hCarNumber", function() {
		return "orderAutoCarNumber.php?q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hSale", function() {
		var str = this.text.value;	
		if( str.length > 1){	
			return "acardAutoSale.php?q=" + this.text.value;
		}
	});		
	
	<?if($strMode=="Update"){?>
		<?
		$i=0;
		forEach($objOrderItemList->getItemList() as $objItem) {?>
		new CAPXOUS.AutoComplete("hDealer_<?=$i?>", function() {
			return "orderAutoDealerItem.php?q=" + this.text.value+"&i="+<?=$i?>+"&r="+document.frm01.hStockProductId_<?=$i?>.value;
		});	
		<?
		$i++;
		}?>
	<?}else{?>
		<?for($i=0;$i<sizeof($arrStockProductId);$i++){?>
		new CAPXOUS.AutoComplete("hDealer_<?=$i?>", function() {
			return "orderAutoDealerItem.php?q=" + this.text.value+"&i="+<?=$i?>+"&r="+document.frm01.hStockProductId_<?=$i?>.value;
		});	
		<?}?>
	<?}?>
	
</script>	
<script>
function checkAdd(){
	if(document.frm01.hStockProductId.value == "" || document.frm01.hStockProductId.value == 0){
		alert("��س����͡��¡���Թ��Ҩҡ�к�");
		return false;
	}
	
	if(document.frm01.hQty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		return false;
	}

	return true;
}


function checkValue(){

	<?if($strMode=="Update"){?>

		<?
		$i=0;
		forEach($objOrderItemList->getItemList() as $objItem) {?>
			
			if(trim(document.frm01.hDealer_<?=$i?>.value) == ""){
				alert("��س��к���ҹ��ҷ���ͧ���");
				document.frm01.hDealer_<?=$i?>.focus();
				return false;		
			}
	
			if(document.frm01.hQty_<?=$i?>.value == ""){
				alert("��س��кبӹǹ����ͧ���");
				document.frm01.hQty_<?=$i?>.focus();
				return false;		
			}
			
			if(document.frm01.hPrice_<?=$i?>.value == "" || document.frm01.hPrice_<?=$i?>.value == "0"){
				alert("��س��к��Ҥҷ���ͧ���");
				document.frm01.hPrice_<?=$i?>.focus();
				return false;		
			}				
		<?
		$i++;
		}?>		
	
	
	<?}else{?>
		<?for($i=0;$i<sizeof($arrStockProductId);$i++){?>
			if(trim(document.frm01.hDealer_<?=$i?>.value) == ""){
				alert("��س��к���ҹ��ҷ���ͧ���");
				document.frm01.hDealer_<?=$i?>.focus();
				return false;		
			}
	
			if(document.frm01.hQty_<?=$i?>.value == "" ){
				alert("��س��кبӹǹ����ͧ���");
				document.frm01.hQty_<?=$i?>.focus();
				return false;		
			}
			
			if(document.frm01.hPrice_<?=$i?>.value == "" || document.frm01.hPrice_<?=$i?>.value == "0"){
				alert("��س��к��Ҥҷ���ͧ���");
				document.frm01.hPrice_<?=$i?>.focus();
				return false;		
			}	
		<?}?>
	<?}?>


	
	return true;
}


function ltrim(str) { 
	for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
	return str.substring(k, str.length);
}
function rtrim(str) {
	for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}
function trim(str) {
	return ltrim(rtrim(str));
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}

</script>

<?
	include("h_footer.php")
?>
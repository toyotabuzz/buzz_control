<?
include "common.php";

$objOrderList = new StockCarList();
$objOrderList->setFilter(" car_number LIKE '%".$q."%'   ");
$objOrderList->setPageSize(0);
$objOrderList->setSortDefault(" car_number ASC");
$objOrderList->loadUTF8();

forEach($objOrderList->getItemList() as $objItem) {

$objOrder = new Order();
$objOrder->setOrderId($objItem->getOrderId());
$objOrder->load();

$objCustomer = new Customer();
$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
$objCustomer->loadUTF8();

$objCarSeries = new CarSeries();
$objCarSeries->setCarSeriesId($objItem->getCarSeriesId());
$objCarSeries->loadUTF8();

$objColor = new CarColor();
$objColor->setCarColorId($objItem->getCarColorId());
$objColor->loadUTF8();

$objStockRed = new StockRed();
$objStockRed->setStockRedId($objOrder->getStockRedId());
$objStockRed->loadUTF8();

$objSale = new Member();
$objSale->setMemberId($objOrder->getSendingSaleId());
$objSale->loadUTF8();

$objStockCar = new StockCar();
$objStockCar->setStockCarId($objOrder->getStockCarId());
$objStockCar->loadUTF8();


?>
<div onSelect="this.text.value='<?=$objItem->getCarNumber()?>';document.frm01.hStockCarId.value='<?=$objItem->getStockCarId()?>';document.frm01.hOrderId.value='<?=$objOrder->getOrderId()?>';document.frm01.hOrderNumber.value='<?=$objOrder->getOrderNumber()?>';document.getElementById('series').innerHTML='<?=$objCarSeries->getCarModelTitle()." ".$objCarSeries->getTitle()." /  ".$objColor->getTitle()?>';document.getElementById('delivery').innerHTML='<?=$objOrder->getSendingDate()?>';document.getElementById('customer').innerHTML='<?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?>';document.getElementById('sale').innerHTML='<?=$objSale->getFirstname()." ".$objSale->getLastname()?>';"><span class='informal'></span><span><?=$objItem->getCarNumber()?>, [<?=$objOrder->getOrderNumber()?>] </span></div>
<?}

unset($objOrderList);
?>
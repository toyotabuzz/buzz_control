<?
include("common.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",100);

$objC = new CompanyList();
$objC->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objCarType01 = new CarTypeList();
$objCarType01->setPageSize(0);
$objCarType01->setSortDefault("title ASC");
$objCarType01->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objMemberList = new MemberList();
$objMemberList->setFilter(" D.code = 'INS' ");
$objMemberList->setPageSize(0);
$objMemberList->setSortDefault(" position, team, firstname, lastname ASC ");
$objMemberList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSort(" title ASC");
$objInsureCompanyList->load();

$objInsureBrokerList = new InsureBrokerList();
$objInsureBrokerList->setPageSize(0);
$objInsureBrokerList->setSort(" title ASC");
$objInsureBrokerList->load();

$pintCountTotal = 0;
$pstrCurrentUrl = $SCRIPT_NAME;

if($hSearch != ""){

if ( $hKeyword!="" )
{  
	$pstrCondition  .= " ( ( C.firstname LIKE '%".$hKeyword."%')  OR  ( C.lastname LIKE '%".$hKeyword."%')  OR  ( C.id_card LIKE '%".$hKeyword."%')    OR  ( C.home_tel LIKE '%".$hKeyword."%')  OR  ( C.mobile LIKE '%".$hKeyword."%') OR  ( C.office_tel LIKE '%".$hKeyword."%')   )";	
}

if($hCarType > 0 ){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_type = '".$hCarType."' ";
	}else{
		$pstrCondition .=" IC.car_type = '".$hCarType."' ";	
	}
}

if($hCarModel > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND IC.car_model_id = '".$hCarModel."' ";
	}else{
		$pstrCondition .=" IC.car_model_id = '".$hCarModel."' ";	
	}
}

if($hSaleId > 0){
	if($pstrCondition != ""){
		$pstrCondition .="AND P.sale_id = '".$hSaleId."' ";
	}else{
		$pstrCondition .=" P.sale_id = '".$hSaleId."' ";	
	}
}




if($Month02 > 0){
	if($Year02 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '$Year02-$Month02-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '$Year02-$Month02-%'   ";	
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND P.k_start_date LIKE '%-".$Month02."-%' ";
		}else{
			$pstrCondition .=" P.k_start_date LIKE '%-".$Month02."-%'   ";	
		}
	}
}


if($hDay != "" AND $hMonth != "" AND $hYear != ""){
	$hStartDate = $hYear."-".$hMonth."-".$hDay;
}

if($hDay01 != "" AND $hMonth01 != "" AND $hYear01 != ""){
	$hEndDate = $hYear01."-".$hMonth01."-".$hDay01;
}

if($hStartDate != ""){
	if($hEndDate != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_ins_date  >= '".$hStartDate."' AND P.k_ins_date <= '".$hEndDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_ins_date  >= '".$hStartDate."' AND P.k_ins_date <= '".$hEndDate."' ) ";
		}
	}else{
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_ins_date  = '".$hStartDate."' ) ";
		}else{
			$pstrCondition .=" ( P.k_ins_date = '".$hStartDate."' ) ";
		}
	}
}

if($hKeywordCar != "" ){
	if($pstrCondition != ""){
		$pstrCondition .="AND ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}else{
		$pstrCondition .=" ( IC.code LIKE '%".$hKeywordCar."%'  OR   IC.car_number LIKE '%".$hKeywordCar."%'  OR  IC.engine_number LIKE '%".$hKeywordCar."%'    )";
	}
}

if($hInsureBrokerId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_broker_id  = '".$hInsureBrokerId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_broker_id = '".$hInsureBrokerId."' ) ";
		}
}

if($hInsureCompanyId > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_prb_id  = '".$hInsureCompanyId."' ) ";
		}else{
			$pstrCondition .=" ( P.p_prb_id = '".$hInsureCompanyId."' ) ";
		}
}

if($hFrmType != "" ){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( I.frm_type  = '".$hFrmType."' ) ";
		}else{
			$pstrCondition .=" ( I.frm_type = '".$hFrmType."' ) ";
		}
}

if($hKeywordPrb != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}else{
			$pstrCondition .=" (  P.p_number LIKE '%".$hKeywordPrb."%' ) ";
		}
}


if($hInsureBrokerId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_broker_id = '".$hInsureBrokerId01."' ) ";
		}
}

if($hInsureCompanyId01 > 0){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_prb_id  = '".$hInsureCompanyId01."' ) ";
		}else{
			$pstrCondition .=" ( P.k_prb_id = '".$hInsureCompanyId01."' ) ";
		}
}

if($hKeywordKom != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}else{
			$pstrCondition .=" (  P.k_number LIKE '%".$hKeywordKom."%' ) ";
		}
}

	
if($hCompanyId > 0 ){
	if($pstrCondition != ""){
		$pstrCondition.=" AND P.company_id = $hCompanyId ";
	}else{
		$pstrCondition=" P.company_id = $hCompanyId ";
	}
}


if($hStatus != ""){
		if($pstrCondition != ""){
			$pstrCondition .="AND ( P.status =  '".$hStatus."' ) ";
		}else{
			$pstrCondition .=" (  P.status = '".$hStatus."' ) ";
		}
}

$sesConditionK6 = $pstrCondition;
session_register("sesConditionK6",$sesConditionK6);

}else{//end hSearch



}

$objM = new Member();
$objM->setMemberId($sMemberId);
$objM->load();


if($sesConditionK6 == ""){
	$pstrCondition= "  P.insure_date <> '0000-00-00' ";
}else{
	$pstrCondition= "  P.insure_date <> '0000-00-00'  AND ".$sesConditionK6;
}

echo $pstrCondition;

$objInsureList = new InsureList();
$objInsureList->setFilter($pstrCondition);
$objInsureList->setPageSize(PAGE_SIZE);
$objInsureList->setPage($hPage);
$objInsureList->setSortDefault(" k_start_date ASC ");
$objInsureList->setSort($hSort);
$objInsureList->loadList();

$pCurrentUrl = "insure_stock_kom_add.php?";
	// for paging only (sort resets page viewing)


$pageTitle = "6.2 ��������";
//$strHead03 = "���Ң������١���";
$pageContent = "6.2 �������� > �Ѻ��ҡ�������";
include("h_header.php");
?>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<SCRIPT language=javascript>

function populate01(frm01, selected, objSelect2) {
<?
		
		$i=1;
		forEach($objCarType01->getItemList() as $objItemCarType) {
			if($i==1) $hCarType = $objItemCarType->getCarTypeId();
		?>
			var individualCategoryArray<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarType01->getItemList() as $objItemCarType) {?>
			var individualCategoryArrayValue<?=$objItemCarType->getCarTypeId();?> = new Array(
		<?
			$objCarModelList = new CarModelList();
			$objCarModelList->setFilter(" car_type_id = ".$objItemCarType->getCarTypeId());
			$objCarModelList->setPageSize(0);
			$objCarModelList->setSortDefault(" title ASC");
			$objCarModelList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarModelList->getItemList() as $objItemCarModel) {
				$text.= " \"('".$objItemCarModel->getCarModelId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarType01->getItemList() as $objItemCarType) {?>
	if (selected == '<?=$objItemCarType->getCarTypeId()?>') 	{

		while (individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarType->getCarTypeId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarType->getCarTypeId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarType->getCarTypeId()?>[i]);			
		}
	}			
<?}?>

}


function populate02(frm01, selected, objSelect2) {
	<?
		$i=1;
		forEach($objCarModelList->getItemList() as $objItemCarModel) {
			if($i==1) $hCarModel = $objItemCarModel->getCarModelId();
		?>
			var individualCategoryArray<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
				$text.= " \"('- ����к� -')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getTitle()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
			$i++;
		}
	?>

<?
		forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
			var individualCategoryArrayValue<?=$objItemCarModel->getCarModelId();?> = new Array(
		<?
			$objCarSeriesList = new CarSeriesList();
			$objCarSeriesList->setFilter(" car_model_id = ".$objItemCarModel->getCarModelId());
			$objCarSeriesList->setPageSize(0);
			$objCarSeriesList->setSortDefault(" title ASC");
			$objCarSeriesList->load();
			$text = "";
			$text.= " \"('0')\",";
			forEach($objCarSeriesList->getItemList() as $objItemCarSeries) {
				$text.= " \"('".$objItemCarSeries->getCarSeriesId()."')\",";
			}
			$text = substr($text,0,(strlen($text)-1));
			echo $text;
			echo ");\n";
		}
	?>	
	
	
<?forEach($objCarModelList->getItemList() as $objItemCarModel) {?>
	if (selected == '<?=$objItemCarModel->getCarModelId()?>') 	{

		while (individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length < objSelect2.options.length) {
			objSelect2.options[(objSelect2.options.length - 1)] = null;
		}
		for (var i=0; i < individualCategoryArray<?=$objItemCarModel->getCarModelId()?>.length; i++) 	{
			eval("objSelect2.options[i]=" + "new Option" + individualCategoryArray<?=$objItemCarModel->getCarModelId()?>[i]);
			eval("objSelect2.options[i].value=" +individualCategoryArrayValue<?=$objItemCarModel->getCarModelId()?>[i]);			
		}
	}			
<?}?>

}

</script>
<br>
<br>
<div align="center" class="error"><?=$pstrMsg?></div>
<form name="frm01" action="<?=$PHP_SELF?>" method="post">
<table align="center" class="search" cellpadding="3">
<tr>
		<td align="right">�Ң� :</td>
		<td>
			<?
			$objCList = new CompanyList();
			$objCList->setPageSize(0);
			$objCList->load();
			$objCList->printSelect("hCompanyId",$hCompanyId,"��س����͡�Ң�");
			?>
			
		</td>
</tr>
<tr>
	<td align="right"> �ѹ���ѹ�֡�����š������� :</td>
	<td class="small">
	  	<table cellspacing="0" cellpadding="0">
		<tr>			
			<td><INPUT align="middle" size="2" maxlength="2"   name=hDay value="<?=$hDay?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hMonth value="<?=$hMonth?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hYear value="<?=$hYear?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear,hDay, hMonth, hYear,popCal);return false"></td>		
			<td>&nbsp;&nbsp;�֧�ѹ��� :</td>
			<td><INPUT align="middle" size="2" maxlength="2"   name=hDay01 value="<?=$hDay01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="2" maxlength="2"  name=hMonth01 value="<?=$hMonth01?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=hYear01 value="<?=$hYear01?>"></td>
			<td>&nbsp;</td>
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(hYear01,hDay01, hMonth01, hYear01,popCal);return false"></td>		
		</tr>
		</table>	
	</td>
</tr>	
<tr>
	<td align="right"> ������ö :</td>
	<td class="small">
        <input type="text" name="hKeywordCar" value="">&nbsp;&nbsp;���ҵ�� �Ţ����¹, �Ţ����ͧ , �Ţ�ѧ
	</td>
</tr>	
<tr>
	<td align="right"> �������١��� :</td>
	<td class="small">
        <input type="text" name="hKeyword" value="">&nbsp;&nbsp;���ҵ�� ����, ���ʡ��, ���ʺѵû�ЪҪ�
	</td>
</tr>	


<tr>
	<td align="center" colspan="2"><input type="submit" value="���Ң�����" name="hSearch"></td>
</tr>	
</table>
</form>	
<div  align="center">�� <?=$objInsureList->mCount;?> ��¡��</div>
<form name="frm" action="acardList.php" method="POST">
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="0">
<tr>
	<td width="90%">
	<?
		$objInsureList->printPrevious($pCurrentUrl,"hPage","<b>&lt;</b>","paging","disabled");
		echo(" ");
		$objInsureList->printPagingCount($pCurrentUrl,"hPage","paging","paging");
		echo(" ");
		$objInsureList->printNext($pCurrentUrl,"hPage","<b>&gt;</b>","paging","disabled");
	?>
	</td>
	<td align="center"></td>
</tr>
</table>
<table border="0" width="100%" align="center" cellpadding="2" cellspacing="2">
<tr>
	<td valign="top" width="2%" align="center" class="ListTitle">�ӴѺ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ����駧ҹ</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ţ����Ѻ��</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�ѹ���ѹ�֡<br>�Ѻ��Сѹ���</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">
	<font color="#cc3366"><strong>�����Ţ���������</strong></font>	
	</td>

	<td valign="top"  width="5%" align="center" class="ListTitle">�յ������</td>
	<td valign="top"  width="7%" align="center" class="ListTitle">
	�ѹ������ͧ
	</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��������</td>
	<td valign="top"  width="10%" align="center" class="ListTitle">����</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">�Ţ����¹</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��ѡ�ҹ���</td>
	<td valign="top"  width="5%" align="center" class="ListTitle">��Ǩ�ͺ</td>
</tr>
	<?
		$i=0;
		forEach($objInsureList->getItemList() as $objItem) {
		
		$objInsure = new Insure();
		$objInsure->set_insure_id($objItem->get_insure_id());
		$objInsure->load();
		
		$objInsureCar = new InsureCar();
		$objInsureCar->set_car_id($objInsure->get_car_id());
		$objInsureCar->load();
		
		$arrDate = explode("-",$objInsureCar->get_date_verify());
		$hYearExtend = $arrDate[0]+1;
		
		$objMember = new Member();
		$objMember->setMemberId($objInsure->get_sale_id());
		$objMember->load();
		
		$objCustomer = new InsureCustomer();
		$objCustomer->setInsureCustomerId($objInsureCar->get_insure_customer_id());
		$objCustomer->load();

		$objPrbCompany = new InsureCompany();
		$objPrbCompany->setInsureCompanyId($objInsure->get_p_prb_id());
		$objPrbCompany->load();
		
		$objKomCompany = new InsureCompany();
		$objKomCompany->setInsureCompanyId($objInsure->get_k_prb_id());
		$objKomCompany->load();
		
	?>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<?if($hPage==0)$hPage=1;?>
	<td valign="top" align="center" ><?=(($hPage-1)*PAGE_SIZE)+$i+1?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objInsure->get_insure_date())?></td>	
	<td valign="top" align="center" ><?=$objInsure->get_p_rnum()?></td>	
	<td valign="top" align="center" ><?=formatShortDate($objInsure->get_k_ins_date())?></td>	
	<td align="center" valign="top">
	<font color="#cc3366"><strong><?=$objInsure->get_k_number()?></strong></font>
	</td>
	<td valign="top" align="center" ><?=$objInsure->get_p_year()?></td>	
	<td valign="top" align="center" >
	<?=formatShortDate($objInsure->get_k_start_date())?>
	</td>	
	<td valign="top" align="center" ><?=$objKomCompany->getShortTitle()?></td>		
	<td valign="top"  ><?=$objCustomer->getTitleDetail()." ".$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></td>
	<td valign="top"  align="center" ><?=$objInsureCar->get_code()?></td>
	<td valign="top"  align="center" ><?=$objMember->getNickname()?></td>
	<td  valign="top" align="center">
	</td>	
</tr>
	<?
		++$i;
			}
	Unset($objCustomerList);
	?>
</table>
<input type="hidden" name="hCountTotal" value="<?=$i?>">
</form>

<?
	include("h_footer.php")
?>

<?
include("common.php");
Include("incUtility.php");

$objStockDealerList = new StockDealerList();

$objStockProductGroupList = new StockProductGroupList();
$objStockProductGroupList->setPageSize(0);
$objStockProductGroupList->setSortDefault(" code ASC ");
$objStockProductGroupList->load();

$objStockProductTypeList = new StockProductTypeList();
$objStockProductTypeList->setPageSize(0);
$objStockProductTypeList->setSortDefault(" code ASC ");
$objStockProductTypeList->load();

$objStockProductPackList = new StockProductPackList();
$objStockProductPackList->setPageSize(0);
$objStockProductPackList->setSortDefault(" code ASC ");
$objStockProductPackList->load();

$objCarType = new CarType();
$objCarType->loadByCondition(" title LIKE '%TOYOTA%'");

$objCarModelList = new CarModelList();
$objCarModelList->setFilter(" car_type_id = ".$objCarType->getCarTypeId());
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objSPCMList = new SPCMList();

$objStockProduct = new StockProduct();

if (empty($hSubmit)) {
	if ($hId!="") {
		$objStockProduct->setStockProductId($hId);
		$objStockProduct->load();
		
		$objStockDealerList->setFilter(" SD.stock_product_id = $hId ");
		$objStockDealerList->setPageSize(0);
		$objStockDealerList->setSortDefault(" dealer_detail ASC ");
		$objStockDealerList->load();
		
		$objSPCMList->setFilter(" stock_product_id = $hId  ");
		$objSPCMList->setPageSize(0);
		$objSPCMList->setSortDefault(" spcm_id ASC");
		$objSPCMList->load();
		
		$strMode="Update";
	} else {
		$strMode="Add";
	}
} else {
	if (!empty($hSubmit)) {

			$objStockProduct->setStockProductId($hId);
			$objStockProduct->setTitle($hTitle);
			$objStockProduct->setTMT(1);
            $objStockProduct->setCode($hCode);
			$objStockProduct->setStockProductGroupId($hStockProductGroupId);
            $objStockProduct->setStockProductTypeId($hStockProductTypeId);
			$objStockProduct->setStockProductPackId($hStockProductPackId);
			$objStockProduct->setRetail($hRetail);
			$objStockProduct->setPrice($hPrice);
			$objStockProduct->setCarType($hCarType);
			$objStockProduct->setPriceArv($hPriceArv);
			$objStockProduct->setPriceType($hPriceType);
			$objStockProduct->setMinValue($hMinValue);
            $objStockProduct->setMaxValue($hMaxValue);
			$objStockProduct->setAddPlace($hAddPlace);
			$objStockProduct->setWorkType($hWorkType);
			$objStockProduct->setRemark($hRemark);
			
			if (!isset($hRemoveImage0)){
				$objStockProduct->setLogo($hImage0);
			}else{
				$objStockProduct->deletePicture($hImage0);
			}

    		$pasrErr = $objStockProduct->check($hSubmit);

			//echo Count($pasrErr);
			if (!$hAddPrice and !$hUpdatePrice){
			
			If ( Count($pasrErr) == 0 ){
			
				for ($j=0;$j<1;$j++){
				if ($hPhoto_name[$j] != "") {
					//$pic = $hPhoto_name[$j].date("YmdHis").$hPhoto_type[$i];
					$pic = date("YmdHis").$j.substr($hPhoto_name[$j],-4);
					switch ($j) {
						case 0:
							$objStockProduct->deletePicture($hImage0);
							$objStockProduct->setLogo($pic);
							break;
					}//end switch
					$objStockProduct->uploadPicture($hPhoto[$j],$pic);
				}//end if
				}// end for	
			
			
				if ($strMode=="Update") {
					$objStockProduct->update();
				} else {
					$hId=$objStockProduct->add();
				}
				
				//update stock product car model
				$i=1;
				$objSPCM = new SPCM();
				forEach($objCarModelList->getItemList() as $objItem) {
					if(isset($_POST["hCarModelId_".$i])){
						if(!$objSPCM->loadByCondition(" stock_product_id = $hId  AND car_model_id = ".$objItem->getCarModelId())){
							$objSPCM->setStockProductId($hId);
							$objSPCM->setCarModelId($objItem->getCarModelId());
							$objSPCM->add();
						}
					}else{
						
						$objSPCM->deleteByCondition(" stock_product_id = $hId  AND car_model_id = ".$objItem->getCarModelId());
					
					}
					$i++;
				}
				
				if($strMode=="Add" AND $sDealer != ""){
					
					$arrDealerId  = explode(",::",$sDealerId);
					$arrDealer  = explode(",::",$sDealer);
					$arrRemark  = explode(",::",$sRemark);
					$arrPrice = explode(",::",$sPrice);
					
					$objStockDealer = new StockDealer();
					for ($i = 0; $i < sizeof($arrDealer);$i++) {
						
						if($arrDealerId[$i] == ""){
							$objDealer = new Dealer();
							$objDealer->setTitle($arrDealer[$i]);
							$arrDealerId[$i] = $objDealer->add();
						}
						
						$objStockDealer->setStockProductId($hId);
						$objStockDealer->setDealerId($arrDealerId[$i]);
						$objStockDealer->setRemark($arrRemark[$i]);
						$objStockDealer->setPrice($arrPrice[$i]);
						$objStockDealer->add();
					}
					unset($objStockDealer);
				}
				
				unset ($objStockProduct);
				
				header("location:stockProductList.php?hKeyword=$hKeyword&hPage=$hPage&hSearch=$hSearch&hStockProductGroupId=$hStockProductGroupId01&hStockProductTypeId=$hStockProductTypeId01&hCarType=$hCarType01");
				exit;
			}else{
				$objStockProduct->init();
			}//end check Count($pasrErr)
			
			}//end check addprice
		}
}

$pageTitle = "2. �����������ѡ";
$strHead03 = "�ѹ�֡�������Թ��� TMT";
$pageContent = "2.1 �ѹ�֡�������Թ��� TMT";
include("h_header.php");

?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hCode.value=="")
		{
			alert("��س��к������Թ���");
			document.forms.frm01.hCode.focus();
			return false;
		} 			
	
		if (document.forms.frm01.hTitle.value=="")
		{
			alert("��س��кت����Թ���");
			document.forms.frm01.hTitle.focus();
			return false;
		}				
		
		if (document.forms.frm01.hStockProductGroupId.options[frm01.hStockProductGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡������Թ���");
			document.forms.frm01.hStockProductGroupId.focus();
			return false;
		}	
		
		if (document.forms.frm01.hStockProductTypeId.options[frm01.hStockProductTypeId.selectedIndex].value=="0")
		{
			alert("��س����͡�������Թ���");
			document.forms.frm01.hStockProductTypeId.focus();
			return false;
		}	
		
		if (document.forms.frm01.hStockProductPackId.options[frm01.hStockProductPackId.selectedIndex].value=="0")
		{
			alert("��س����͡˹��¹Ѻ");
			document.forms.frm01.hStockProductPackId.focus();
			return false;
		}	
		
		if (document.forms.frm01.hMinValue.value=="")
		{
			alert("��س��кػ���ҳ����ش");
			document.forms.frm01.hMinValue.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hMaxValue.value=="")
		{
			alert("��س��кػ���ҳ�٧�ش");
			document.forms.frm01.hMaxValue.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hRetail.value=="")
		{
			alert("��س��кص鹷ع�Ѻ��ҵ�駵�");
			document.forms.frm01.hRetail.focus();
			return false;
		} 	
		
	}
</script><br>
<form name="frm01" action="stockProductUpdate.php" method="POST" ENCTYPE="multipart/form-data" onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	<input type="hidden" name="strMode" value="<?=$strMode?>">
	<input type="hidden" name="hId" value="<?=$hId?>">
	<input type="hidden" name="hKeyword" value="<?=$hKeyword?>">
	<input type="hidden" name="hPage" value="<?=$hPage?>">
	<input type="hidden" name="hSearch" value="<?=$hSearch?>">
	<input type="hidden" name="hStockProductGroupId01" value="<?=$hStockProductGroupId01?>">
	<input type="hidden" name="hStockProductTypeId01" value="<?=$hStockProductTypeId01?>">
	<input type="hidden" name="hCarType01" value="<?=$hCarType01?>">

	
  <table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
	<tr>
    <td class="bodyhead" align="right">�����Թ���:</td>
	<td >
        <input type="text"   name="hCode" maxlength="100" size="30" value="<?=$objStockProduct->getCode();?>">
        <?printError($pasrErr[code]);?>
    </td>
	</tr>
	<tr>
    <td class="bodyhead" align="right">�����Թ���:</td>
	<td >
        <input type="text"   name="hTitle" maxlength="100" size="50" value="<?=$objStockProduct->getTitle();?>">
        <?printError($pasrErr[title]);?>
    </td>
	</tr>	
	<tr>
    <td class="bodyhead" align="right">������Թ���:</td>
	<td >
		<?=$objStockProductGroupList->printSelect("hStockProductGroupId",$objStockProduct->getStockProductGroupId(),"��س����͡");?>
    </td>
	</tr>						
    <td class="bodyhead" align="right">�������Թ���:</td>
	<td >
		<?=$objStockProductTypeList->printSelect("hStockProductTypeId",$objStockProduct->getStockProductTypeId(),"��س����͡");?>
    </td>	
	</tr>		
	<tr>
    <td class="bodyhead" align="right">ʶҹ���Դ���:</td>
	<td >
		<select name="hAddPlace">
			<option value="" <?if($objStockProduct->getAddPlace() == "") echo "selected";?>>- ����к� -
			<option value="�آ����" <?if($objStockProduct->getAddPlace() == "�آ����") echo "selected";?>>�آ����
			<option value="buzz" <?if($objStockProduct->getAddPlace() == "buzz") echo "selected";?>>buzz
			<option value="����" <?if($objStockProduct->getAddPlace() == "����") echo "selected";?>>����
		</select>           
    </td>
	</tr>				
	<tr>
    <td class="bodyhead" align="right">��è�ҧ����:</td>
	<td >
        <input type="checkbox"   name="hWorkType"  value="1" <?if($objStockProduct->getWorkType() == 1) echo "checked";?>> (�Թ��ҹ���繻�������ҧ������ҹ Supplier �Դ������Դ�ӹǹ Stock)
    </td>
	</tr>	
    <tr> 
      <td align="right" width="30%" valign="top" class="need">������ö: </td>
      <td> 
		<select class="input" name="hCarType">
			<option value="0" selected >��س����͡
			<option value="1" <?if($objStockProduct->getCarType() == "1") echo "selected"?>>ö��
			<option value="2" <?if($objStockProduct->getCarType() == "2") echo "selected"?>>ö�к�
			<option value="3" <?if($objStockProduct->getCarType() == "3") echo "selected"?>>ö 7 �����
			<option value="4" <?if($objStockProduct->getCarType() == "4") echo "selected"?>>ö���
		</select>
      </td>
    </tr> 	
    <tr> 
      <td align="right" width="30%" valign="top" class="need">Model ö: </td>
      <td> 
	  			<input type="checkbox" onclick="checkAll();" name="hAll<?=$i?>" > ö������ &nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="uncheckAll();" name="hNo<?=$i?>" > ����к�ö
	  			<table>
				<tr>
				<?
				$i=1;
				$objSPCM = new SPCM();
				forEach($objCarModelList->getItemList() as $objItem) {
				?>
				<td>
				<input type="checkbox" name="hCarModelId_<?=$i?>" value="<?=$objItem->getCarModelId()?>"  <? if( $hId !="" ) {  if($objSPCM->loadByCondition("  stock_product_id = $hId AND car_model_id = ".$objItem->getCarModelId()) ) { echo "checked";}  }?>  > <?=$objItem->getTitle()?>&nbsp;&nbsp;				
				<?
				
				
				if(fmod($i,6) == 0){
					echo "</td></tr>";
				}else{
					echo "</tr>";
				}
					++$i;
						}
				
				?>
				</table>
      </td>
    </tr> 	
    <td class="bodyhead" align="right">˹��¹Ѻ:</td>
	<td >
		<?=$objStockProductPackList->printSelect("hStockProductPackId",$objStockProduct->getStockProductPackId(),"��س����͡");?>
    </td>
	</tr>						
    <tr> 
      <td align="right" width="20%">����ҳ����ش:</td>
      <td > 
       	<input type="text" name="hMinValue" maxlength="10" size="10" value="<?=$objStockProduct->getMinValue();?>">
      </td>
    </tr>	
    <tr> 
      <td align="right" >����ҳ�ҡ�ش:</td>
      <td > 
       	<input type="text" name="hMaxValue" maxlength="10" size="10" value="<?=$objStockProduct->getMaxValue();?>">
      </td>
    </tr>
	<tr>
    <td class="bodyhead" align="right">�鹷ع�Ѻ���:</td>
	<td >
        <input type="text"   name="hRetail" maxlength="20" size="10" value="<?=$objStockProduct->getRetail();?>">
    </td>
	</tr>		
	<tr>
    <td class="bodyhead" align="right">��˹�����Ҥҵ��:</td>
	<td >
		<input type="radio" name="hPriceType"   onclick="R01();hPriceArv.focus();"  value=1 <?if($objStockProduct->getPriceType() == 1 or $objStockProduct->getPriceType() == 0) echo "checked";?>>�ҡ�����&nbsp;&nbsp;&nbsp;<input type="radio" name="hPriceType"  onclick="R02();hPrice.focus();"  value=2 <?if($objStockProduct->getPriceType() == 2) echo "checked";?>>��˹��ͧ
    </td>
	</tr>	
	<tr id="R01"  <?if($objStockProduct->getPriceType() == 2 ) echo 'style="display:none"';?>>
    <td class="bodyhead" align="right">�ҤҢ������¨ҡ�����:</td>
	<td class="small" >
		<input type="text"   name="hPriceArv" maxlength="20" size="10" value="<?=$objStockProduct->getPriceArv();?>"> ( �鹷ع�Ѻ����٧�ش + 10% )
    </td>
	</tr>	
	<tr id="R02" <?if($objStockProduct->getPriceType() == 1 or $objStockProduct->getPriceType() == 0 ) echo 'style="display:none"';?>>
    <td class="bodyhead" align="right">�ҤҢ�¡�˹��ͧ:</td>
	<td >
		<input type="text"   name="hPrice" maxlength="20" size="10" value="<?=$objStockProduct->getPrice();?>">
    </td>
	</tr>		
    <tr>
    <td align="right">�ٻ�Թ���:</td>
    <td  >	
		<?if (trim($objStockProduct->getLogo()) != "") echo "<img src='".gDIR_IMG.$objStockProduct->getLogo()."'><br>"?>	  	
		<input type="file" name="hPhoto[0]" size="30">
		<input type="hidden" name="hImage0" size="50" value="<?=$objStockProduct->getLogo();?>">
    </td>
	</tr>	
    <tr>
    <td class="title" align="right"></td>
    <td >		
			ź�ٻ������� <input type="checkbox" name="hRemoveImage0"> 
    </td>
	</tr>		
    <tr>
    <td align="right" valign="top">�����˵��������:</td>
    <td  >	
		<textarea name="hRemark" rows="5" cols="80"><?=$objStockProduct->getRemark();?></textarea>
    </td>
	</tr>		
    <tr> 
      <td>&nbsp;</td>
      <td > <br><br><br>
        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
		<?if ($strMode == "Update"){?>
			<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
		<?}else{?>
			<input type="submit" name="hSubmit1" value="�ѹ�֡��¡��" class="button">
		<?}?>
        <img src="images/spacer.gif" alt="" width="20" height="1" border="0"><input type="Button" name="hSubmit"  class="button" value="¡��ԡ��¡��" onclick="window.location='stockProductList.php?hKeyword=<?=$hKeyword?>&hPage=<?=$hPage?>&hSearch=<?=$hSearch?>&hStockProductGroupId=<?=$hStockProductGroupId01?>&hStockProductTypeId=<?=$hStockProductTypeId01?>&hCarType=<?=$hCarType01?>'">
      </td>
    </tr>
  </table>
</form>

<script>
	new CAPXOUS.AutoComplete("hDealer", function() {
		return "stockProductAutoDealer.php?q=" + this.text.value;
	});
</script>	
<script>
	function R01(){
		document.getElementById("R02").style.display="none";
		document.getElementById("R01").style.display="";
	}
	
	function R02(){	
		document.getElementById("R01").style.display="none";
		document.getElementById("R02").style.display="";	
	}
	
	function checkAdd(){
		if(document.frm01.hDealer.value== ""){
			alert("��س��кت�����ҹ���");
			document.frm01.hDealer.focus();
			return false;
		}
		return true;
	}
</script>

<script>
	function checkAll(){
				<?
				$i=1;
				forEach($objCarModelList->getItemList() as $objItem) {
				?>
				eval("document.frm01.hCarModelId_<?=$i?>.checked = true");
				<?
				++$i;
				}
				
				?>
				eval("document.frm01.hNo.checked = false");
	}
	
	function uncheckAll(){		
				<?
				$i=1;
				forEach($objCarModelList->getItemList() as $objItem) {
				?>
				eval("document.frm01.hCarModelId_<?=$i?>.checked = false");
				<?
				++$i;
				}
				
				?>
				eval("document.frm01.hAll.checked = false");
	}


</script>
<?
	include("h_footer.php")
?>
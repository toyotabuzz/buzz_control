<?
define('FPDF_FONTPATH','../font/');
include("common.php");
require('../class/fpdf.php');
include("incUtility.php");

define ("NUMCOL_MAX",7);
define ("PAGE_SIZE",0);

if ($hSearch !="" )
{  

	$hStartDate = $Year."-".$Month."-".$Day;
	$hEndDate = $Year01."-".$Month01."-".$Day01;
	
	$pstrCondition  = " ( DATE(invoice_date) >= '".$hStartDate."' AND  DATE(invoice_date) <= '".$hEndDate."'  )";	
	$strDate = $Day."-".$Month."-".$Year." �֧�ѹ��� ".$Day01."-".$Month01."-".$Year01;	

}

$objStockCarList = new StockCarList();
$objStockCarList->setFilter($pstrCondition);
$objStockCarList->setPageSize(0);
$objStockCarList->setPage($hPage);
$objStockCarList->setSortDefault("  TBR, P.invoice_date ASC  ");
$objStockCarList->setSort($hSort);
$objStockCarList->load();

$page_title = "ACC21 ��§ҹ��â�͹��ѵԨ�����¹ $strDate $strCarModel $strTeam "; 

class PDF extends FPDF
{

//Page header
function Header()
{
	global $hStart;
	global $eday;
	global $page_title;
	$this->AddFont('angsa','B','angsab.php'); 
	$this->SetFont('angsa','B',20);
	$this->Cell(1010,60,$page_title,0,0,"C");
	$this->Ln();
	$this->SetFont('angsa','B',11);	
	$this->Cell(20,40,"�ӴѺ",1,0,"C");
	$this->Cell(30,40,"�ӴѺ",1,0,"C");
	$this->Cell(100,20,"��駨�˹���",1,0,"C");
	$this->Cell(45,40,"�ѹ���",1,0,"C");
	$this->Cell(402,20,"��������´ö",1,0,"C");
	$this->Cell(85,40,"����-���ʡ��",1,0,"C");
	$this->Cell(35,40,"������",1,0,"C");
	$this->Cell(95,40,"�Ţ���㺡ӡѺ����/�ѹ���",1,0,"C");	
	$this->Cell(50,40,"�ӹǹ�Թ",1,0,"C");
	$this->Cell(35,40,"�ѹ����ʹ�",1,0,"C");
	$this->Cell(35,40,"�ѹ���͹��ѵ�",1,0,"C");
	$this->Cell(55,40,"�����",1,0,"C");
	$this->Cell(40,40,"�ѹ����Ѻ",1,0,"C");
	$this->Cell(30,40,"�����˵�",1,0,"C");
	$this->Cell(10,20,"",0,0,"C");
	$this->Ln();

	$this->Cell(20,20,"",0,0,"C");
	$this->Cell(30,20,"�ѹ��� TBR",0,0,"C");
	$this->Cell(55,20,"�Ţ���",1,0,"C");
	$this->Cell(45,20,"�ѹ���",1,0,"C");
	$this->Cell(45,20,"�Ѻö��ԧ",0,0,"C");
		
	$this->Cell(65,20,"Model",1,0,"C");
	$this->Cell(65,20,"���ö",1,0,"C");
	$this->Cell(92,20,"Ẻö",1,0,"C");
	$this->Cell(55,20,"��",1,0,"C");
	$this->Cell(65,20,"�Ţ�ѧ",1,0,"C");
	$this->Cell(60,20,"�Ţ����ͧ",1,0,"C");
	
	$this->Cell(85,20,"",0,0,"C");
	$this->Cell(35,20,"��â��",0,0,"C");
	$this->Cell(50,20,"",0,0,"C");
	$this->Cell(45,20,"",0,0,"C");
	$this->Cell(50,20,"",0,0,"C");
	$this->Cell(35,20,"",0,0,"C");
	$this->Cell(35,20,"",0,0,"C");
	$this->Cell(55,20,"",0,0,"C");
	$this->Cell(40,20,"��� TMT",0,0,"C");
	$this->Cell(30,20,"",0,0,"C");

	$this->Ln();	
	
}

//Page footer
function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}


$pdf=new PDF("L","pt","A3");
$pdf->AliasNbPages();
//Data loading
$pdf->AddFont('angsa','','angsa.php'); 
$pdf->AddPage();
//Header


	$i=1;
	if($objStockCarList->mCount > 0){

		$j=1;

			forEach($objStockCarList->getItemList() as $objItem) {

				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objItem->getStockCarId());
				$objStockCar->load();
				
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
				$objCarSeries->load();
				
				$objCarColor = new CarColor();
				$objCarColor->setCarColorId($objStockCar->getCarColorId());
				$objCarColor->load();	
				
				$objOrder = new Order();
				$objOrder->setOrderId($objItem->getOrderId());
				$objOrder->load();

				$objFundCompany = new FundCompany();
				$objFundCompany->setFundCompanyId($objOrder->getBuyCompany());
				$objFundCompany->load();
				
				$objCustomer = new Customer();
				$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objCustomer->load();
				
				$arrDay = explode("-",$objStockCar->getInvoiceDate());
				
				$Day = $arrDay[2];
				$Month = $arrDay[1];
				$Year = $arrDay[0];

				$pdf->SetFont('angsa','',16);	
				$pdf->Cell(20,50,$i,1,0,"C");
				$pdf->Cell(30,50,$objStockCar->getTBR(),1,0,"C");
				$pdf->Cell(55,50,$Year.$objStockCar->getInvoiceNumber(),1,0,"C");
				$pdf->Cell(45,50,formatShortDate($objStockCar->getInvoiceDate()),1,0,"C");
				$pdf->Cell(45,50,formatShortDate($objStockCar->getStockDate()),1,0,"C");
				$pdf->Cell(65,50,substr($objCarSeries->getModel(),0,7),1,0,"C");
				$pdf->Cell(65,50,substr($objCarSeries->getCarModelTitle(),0,8),1,0,"C");
				$pdf->Cell(92,50,substr($objCarSeries->getTitle(),0,18),1,0,"L");
				$pdf->Cell(55,50,$objCarColor->getTitle(),1,0,"C");
				$pdf->Cell(65,50,substr($objStockCar->getCarNumber(),0,8),1,0,"C");
				$pdf->Cell(60,50,$objStockCar->getEngineNumber(),1,0,"C");
				$name=$objCustomer->getFirstname();
				if(strlen($name) > 15){
					$name2 = substr($name,15,15)." ".$objCustomer->getLastname();
				}else{
					$name2=$objCustomer->getLastname();
				}
				
				$pdf->Cell(85,50,substr($name,0,15),1,0,"L");

				if( $objOrder->getSendingCashierDate() >= $hStartDate){
					if($objOrder->getBuyType() == 2 ){
						$pdf->Cell(35,50,$objFundCompany->getCode(),1,0,"C");
					}else{
						$pdf->Cell(35,50,"ʴ",1,0,"C");
					}
				}else{
					$pdf->Cell(35,50,"",1,0,"C");
				}
				$pdf->Cell(50,50,$objOrder->getFinanceNumber(),1,0,"C");
				$pdf->Cell(45,50,formatShortDate($objOrder->getTaxDate()),1,0,"C");
				if($objOrder->getTaxPrice() > 0){
					$pdf->Cell(50,50,number_format($objOrder->getTaxPrice()),1,0,"R");
				}else{
					$pdf->Cell(50,50,"",1,0,"C");
				}
				
				$pdf->Cell(35,50,"",1,0,"C");
				$pdf->Cell(35,50,"",1,0,"C");
				$pdf->Cell(55,50,"",1,0,"C");
				
				$pdf->Cell(40,50,formatShortDate($objOrder->getRegistrySignDate()),1,0,"C");
				$pdf->Cell(30,50,nl2br($objItem->getInvoiceRemark()),1,0,"C");
				$pdf->Cell(55,30,"",0,0,"C");
				$pdf->Ln();
				
				
				$pdf->Cell(20,20,"",0,0,"C");
				$pdf->Cell(30,20,"",0,0,"C");
				$pdf->Cell(55,20,"",0,0,"C");
				$pdf->Cell(45,20,"",0,0,"C");
				$pdf->Cell(45,20,"",0,0,"C");
				$pdf->Cell(65,20,substr($objCarSeries->getModel(),7,30),0,0,"C");
				$pdf->Cell(65,20,substr($objCarSeries->getCarModelTitle(),8,200),0,0,"C");
				$pdf->Cell(92,20,substr($objCarSeries->getTitle(),18,200),0,0,"L");
				$pdf->Cell(55,20,"",0,0,"C");
				$pdf->Cell(65,20,substr($objStockCar->getCarNumber(),8,20),0,0,"C");
				$pdf->Cell(60,20,"",0,0,"C");				
				$pdf->Cell(85,20,substr($name2,0,15),0,0,"L");
				$pdf->Cell(35,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(45,20,"",0,0,"C");
				$pdf->Cell(50,20,"",0,0,"C");
				$pdf->Cell(35,20,"",0,0,"C");
				$pdf->Cell(35,20,"",0,0,"C");
				$pdf->Cell(55,20,"",0,0,"C");				
				$pdf->Cell(40,20,"",0,0,"C");
				$pdf->Cell(30,20,"",0,0,"C");
				$pdf->Ln();
			$i++;
			$j++;
			$k++;
			}//end foreach order

		
	}//end check count team	

$pdf->Output();
?>

<?
$hModuleCode="ACC0101";
include("common.php");

$objCustomer = new Customer();
$objEvent = new CustomerEvent();
$objOrderStockList = new OrderStockList();
$objOrderStockPremiumList = new OrderStockList();
$objOrderStockOtherList = new OrderStockOtherList();
$objOrderStockOtherPremiumList = new OrderStockOtherList();
$objStockProductList = new StockProductList();
$objOrder = new Order();
$objStockPremiumList = new StockProductList();
$objStockRed = new StockRed();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objCustomerEventList = new CustomerEventList();
$objCustomerEventList->setPageSize(0);
$objCustomerEventList->setSortDefault("title ASC");
$objCustomerEventList->load();

$objSaleList = new MemberList();
$objSaleList->setPageSize(0);
$objSaleList->setSortDefault("department_id, firstname, lastname ASC");
$objSaleList->load();

$objCarTypeList = new CarTypeList();
$objCarTypeList->setPageSize(0);
$objCarTypeList->setSortDefault("title ASC");
$objCarTypeList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModel1List = new CarSeriesList();
$objCarModel1List->setPageSize(0);
$objCarModel1List->setSortDefault(" model ASC");
$objCarModel1List->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getDiscountMarginDate());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];		

		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();

		if($hCustomerId != ""){	
			$objOrder->setBookingCustomerId($hCustomerId);			
		}
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		$strMode="Update";

		
		$objSale = new Member();
		$objSale->setMemberId($objCustomer->getSaleId());
		$objSale->load();
		
		$hCustomerSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$objContactList = new ContactList();
		$objContactList->setPageSize(0);
		$objContactList->setSortDefault(" title ASC");
		$objContactList->load();				
		
		$objCustomerContactList = new CustomerContactList();
		$objCustomerContactList->setFilter("   customer_id = $hId ");
		$objCustomerContactList->setPageSize(0);
		$objCustomerContactList->setSortDefault(" contact_date DESC ");
		$objCustomerContactList->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();
		
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {
	
			if(isset($hOrderProductAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderProductId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderProductQty);
				$objOrderStock->setPrice($hOrderProductPrice);
				$objOrderStock->setTMT($hOrderProductTMT);
				if($hOrderProductOther){
					$objOrderStock->setRemark($hOrderProduct);
				}
				$objOrderStock->add();
				//header("location:bcardUpdate.php?hId=$hId&#free");
				//exit;
				
			}
			
			if(isset($hOrderProductUpdate)){
				for($i=0;$i<$hCountOrderProduct;$i++){
					if($_POST["hOrderProductDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderProductId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(0);
						$objOrderStock->setQty($_POST["hOrderProductQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderProductPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderProductTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderProductRemark_".$i]);
						$objOrderStock->update();
					}
				}
				//header("location:bcardUpdate.php?hId=$hId&#free");
				//exit;
			}
			
			if(isset($hOrderProduct01Add)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderProduct01Id);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderProduct01Qty);
				$objOrderStock->setPrice($hOrderProduct01Price);
				$objOrderStock->setTMT($hOrderProduct01TMT);
				$objOrderStock->setStockType(2);
				if($hOrderProduct01Other){
					$objOrderStock->setRemark($hOrderProduct01);
				}
				$objOrderStock->add();
				//header("location:bcardUpdate.php?hId=$hId&#equip");
				//exit;
			}
			
			if(isset($hOrderProduct01Update)){
				for($i=0;$i<$hCountOrderProduct01;$i++){
					if($_POST["hOrderProduct01Delete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProduct01StockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProduct01StockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderProduct01Id_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(2);
						$objOrderStock->setQty($_POST["hOrderProduct01Qty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderProduct01Price_".$i]);
						$objOrderStock->setTMT($_POST["hOrderProduct01TMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderProduct01Remark_".$i]);
						$objOrderStock->update();
					}
				}
				//header("location:bcardUpdate.php?hId=$hId&#equip");
				//exit;
			}
			
			
			
			if(isset($hOrderPremiumAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderPremiumId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderPremiumQty);
				$objOrderStock->setPrice($hOrderPremiumPrice);
				$objOrderStock->setTMT($hOrderPremiumTMT);
				$objOrderStock->setStockType(1);
				if($hOrderPremiumOther){
					$objOrderStock->setRemark($hOrderPremium);
				}
				$objOrderStock->add();
				//header("location:bcardUpdate.php?hId=$hId&#free");
				//exit;
			}
			
			if(isset($hOrderPremiumUpdate)){
				for($i=0;$i<$hCountOrderPremium;$i++){
					if($_POST["hOrderPremiumDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderPremiumId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(1);
						$objOrderStock->setQty($_POST["hOrderPremiumQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderPremiumPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderPremiumTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderPremiumRemark_".$i]);
						$objOrderStock->update();
					}
				}
				//header("location:bcardUpdate.php?hId=$hId&#free");
				//exit;
			}	
	
	if(!$hOrderProductAdd AND !$hOrderPremiumAdd AND !$hOrderProductUpdate AND !$hOrderPremiumUpdate AND !$hOrderProduct01Update AND !$hOrderProduct01Add){
	
    	$objOrder->setOrderId($hOrderId);
		$objOrder->setOrderStatus($hStatus);
		$objOrder->setCheckAccountBooking($hStatus);
		
        $objOrder->setBookingRemark($hBookingRemark);
		$objOrder->setBookingCarType($hBookingCarModel);
		$objOrder->setBookingCarSeries($hBookingCarSeries);
		$objOrder->setBookingCarGear($hBookingCarGear);
		$objOrder->setBookingCarColor($hBookingCarColor);
		$objOrder->setBookingCarPrice($hBookingCarPrice);
		$objOrder->setBookingCarRemark($hBookingCarRemark);
		
		$objOrder->setDiscountPrice($hDiscountPrice);
		$objOrder->setDiscountSubdown($hDiscountSubdown);
		$objOrder->setDiscountPremium($hOrderPremiumPriceTotal);
		$objOrder->setDiscountProduct($hOrderProductPriceTotal);
		$objOrder->setDiscountInsurance($hDiscountInsurance);
		$objOrder->setDiscountGoa($hDiscountGoa);
		$objOrder->setDiscount($hDiscount);
		$objOrder->setDiscountMargin($hDiscountMargin);
		$hDiscountMarginDate = $Year05."-".$Month05."-".$Day05;
		$objOrder->setDiscountMarginDate($hDiscountMarginDate);
		$objOrder->setDiscountSubdownVat($hDiscountSubdownVat);
		$objOrder->setDiscountAll($hDiscountAll);
		$objOrder->setDiscountOver($hDiscountOver);
		$objOrder->setDiscountCustomerPay($hDiscountCustomerPay);	
		$objOrder->setDiscountCustomerPayReason($hDiscountCustomerPayReason);	
		$objOrder->setDiscountTMT($hDiscountTMT);

		$objOrder->setBuyType($hBuyType);
		$objOrder->setBuyCompany($hBuyCompany);
		$objOrder->setBuyFee($hBuyFee);
		$objOrder->setBuyTime($hBuyTime);
		$objOrder->setBuyPayment($hBuyPayment);
		$objOrder->setBuyPrice($hBuyPrice);
		$objOrder->setBuyDown($hBuyDown);
		$objOrder->setBuyTotal($hBuyTotal);
		$objOrder->setBuyRemark($hBuyRemark);
		$objOrder->setBuyProduct($hBuyProduct);
		$objOrder->setBuyBegin($hBuyBegin);
		$objOrder->setBuyEngine($hBuyEngine);
		$objOrder->setBuyEngineRemark($hBuyEngineRemark);
		
		$objOrder->setRegistryPerson($hRegistryPerson);
		$objOrder->setRegistryPrice($hRegistryPrice);
		$objOrder->setRegistryPrice1($hRegistryPrice1);
		
		$objOrder->setPrbCompany($hPrbCompany);
		
		$objOrder->setPrbExpire($hPrbExpire);
		$objOrder->setPrbPrice($hPrbPrice);
		$objOrder->setInsureCompany($hInsureCompany);
		$hInsureExpire = $Year06."-".$Month06."-".$Day06;
		$objOrder->setInsureExpire($hInsureExpire);
		$objOrder->setInsurePrice($hInsurePrice);
		$objOrder->setInsureType($hInsureType);
		$objOrder->setInsureYear($hInsureYear);
		$objOrder->setInsureBudget($hInsureBudget);
		$objOrder->setInsureFrom($hInsureFrom);
		$objOrder->setInsureFromDetail($hInsureFromDetail);
		$objOrder->setInsureRemark($hInsureRemark);			
		$objOrder->setInsureCustomerPay($hInsureCustomerPay);
		
		$objOrder->setTMBNumber($hTMBNumber);			
		
		$objOrder->setTMTFree($hTMTFree);
		$objOrder->setTMTFreeRemark($hTMTFreeRemark);
		$objOrder->setTMTCoupon($hTMTCoupon);
		$objOrder->setTMTCouponRemark($hTMTCouponRemark);
		$objOrder->setTMTOther($hTMTOther);
		$objOrder->setTMTOtherRemark($hTMTOtherRemark);		
		$objOrder->setDiscountOther($hDiscountOther);
		$objOrder->setDiscountOtherPrice($hDiscountOtherPrice);	
		
		$objOrder->setOrderPrice($hOrderPrice);
		$objOrder->setOrderReservePrice($hOrderReserve);
		$objOrder->setOrderReservePrice01($hOrderReserve01);
		$objOrder->setOrderReservePrice02($hOrderReserve02);

		$objOrder->setOrderOldCar($hDiscountOldCar);
		
		$objOrder->setStockRedId($hStockRedId);
		$objOrder->setNoRedCode($hNoRedCode);
		$objOrder->setRedCodePrice($hRedCodePrice);
		
		
		if($hStatus == 1){
			$objOrder->setOrderStatus(2);
		}else{
			if($hStatus == 3){
				$objOrder->setOrderStatus(0);
			}else{
				$objOrder->setOrderStatus(1);
			}
		}		
		$objOrder->setCheckAccountBookingBy($sMemberId);
		$objOrder->setCheckAccountBookingRemark($hRemark);
		$objOrder->updateAccountBooking();
		
		header("location:$hFrom");
		exit;

		
	}else{
	
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getDiscountMarginDate());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];		

		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hSaleName = $objSale->getFirstname()." ".$objSale->getLastname();

		if($hCustomerId != ""){	
			$objOrder->setBookingCustomerId($hCustomerId);			
		}
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		$strMode="Update";

		
		$objSale = new Member();
		$objSale->setMemberId($objCustomer->getSaleId());
		$objSale->load();
		
		$hCustomerSaleName = $objSale->getFirstname()." ".$objSale->getLastname();
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$objContactList = new ContactList();
		$objContactList->setPageSize(0);
		$objContactList->setSortDefault(" title ASC");
		$objContactList->load();				
		
		$objCustomerContactList = new CustomerContactList();
		$objCustomerContactList->setFilter("   customer_id = $hId ");
		$objCustomerContactList->setPageSize(0);
		$objCustomerContactList->setSortDefault(" contact_date DESC ");
		$objCustomerContactList->load();
		
		$objCarSeries = new CarSeries();
		$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
		$objCarSeries->load();
		
		$objCarColor = new CarColor();
		$objCarColor->setCarColorId($objOrder->getBookingCarColor());
		$objCarColor->load();
		
		$objEvent->setEventId($objCustomer->getEventId());
		$objEvent->load();
	
	
	}
	
	
	}//end empty submit
	
}

$objOrderStockList = new OrderStockList();
$objOrderStockList->setFilter(" order_id= $hId AND stock_type=0 ");
$objOrderStockList->setPageSize(0);
$objOrderStockList->setSortDefault(" stock_product_id DESC");
$objOrderStockList->load();

$objOrderStockPremiumList = new OrderStockList();
$objOrderStockPremiumList->setFilter(" order_id= $hId AND stock_type=1 ");
$objOrderStockPremiumList->setPageSize(0);
$objOrderStockPremiumList->setSortDefault(" stock_product_id DESC");
$objOrderStockPremiumList->load();

$objOrderStockList01 = new OrderStockList();
$objOrderStockList01->setFilter(" order_id= $hId AND stock_type=2 ");
$objOrderStockList01->setPageSize(0);
$objOrderStockList01->setSortDefault(" stock_product_id DESC");
$objOrderStockList01->load();


$pageTitle = "1. �к��������١���";
$pageContent = "1.3  �ѹ�֡�����š�èͧ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{
	
		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";			
			return false;
		}			
		
		if (document.forms.frm01.hBookingCarPrice.value=="" || document.forms.frm01.hBookingCarPrice.value==0)
		{
			alert("��س��к��ҤҢ��");
			document.forms.frm01.hBookingCarPrice.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.hBookingCarPrice.value) == false){
				document.forms.frm01.hBookingCarPrice.focus();
				return false;
			}
		}			
		
		checkStatus();
		
		document.forms.frm01.submit();
			
	}
	
	function checkStatus(){
	
		if(document.frm01.hStatus[0].checked == false && document.frm01.hStatus[1].checked == false && document.frm01.hStatus[2].checked == false){
			alert("��س����͡ʶҹе�Ǩ�ͧ");
			document.forms.frm01.hStatus[0].focus();
			return false;
		}
		
		return true;
	
	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<script language=JavaScript >

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			if(document.frm01.hCustomerSamePlace01.checked == true){
				document.frm01.hAddress01.value = document.frm01.hAddress.value;
				document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
				document.frm01.hProvince01.value = document.frm01.hProvince.value;
				document.frm01.hZip01.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode01.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode01.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode01.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode01.value = document.frm01.hZipCode.value;		
				document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax01.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress01.value = "";
				document.frm01.hTumbon01.value = "";
				document.frm01.hAmphur01.value = "";
				document.frm01.hProvince01.value = "";
				document.frm01.hZip01.value = "";		
				document.frm01.hTumbonCode01.value = "";
				document.frm01.hAmphurCode01.value = "";
				document.frm01.hProvinceCode01.value = "";
				document.frm01.hZipCode01.value = "";		
				document.frm01.hTel01.value = "";		
				document.frm01.hFax01.value = "";		
			}
		}
   		if(val ==2){
			if(document.frm01.hCustomerSamePlace02.checked == true){
				document.frm01.hAddress02.value = document.frm01.hAddress.value;
				document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
				document.frm01.hProvince02.value = document.frm01.hProvince.value;
				document.frm01.hZip02.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode02.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode02.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode02.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode02.value = document.frm01.hZipCode.value;		
				document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax02.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress02.value = "";
				document.frm01.hTumbon02.value = "";
				document.frm01.hAmphur02.value = "";
				document.frm01.hProvince02.value = "";
				document.frm01.hZip02.value = "";		
				document.frm01.hTumbonCode02.value = "";
				document.frm01.hAmphurCode02.value = "";
				document.frm01.hProvinceCode02.value = "";
				document.frm01.hZipCode02.value = "";		
				document.frm01.hTel02.value = "";		
				document.frm01.hFax02.value = "";		
			}
			
		}
   		if(val ==3){
			if(document.frm01.hCustomerSamePlace03.checked == true){
				document.frm01.hAddress03.value = document.frm01.hAddress.value;
				document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
				document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
				document.frm01.hProvince03.value = document.frm01.hProvince.value;
				document.frm01.hZip03.value = document.frm01.hZip.value;		
				document.frm01.hTumbonCode03.value = document.frm01.hTumbonCode.value;
				document.frm01.hAmphurCode03.value = document.frm01.hAmphurCode.value;
				document.frm01.hProvinceCode03.value = document.frm01.hProvinceCode.value;
				document.frm01.hZipCode03.value = document.frm01.hZipCode.value;		
				document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
				document.frm01.hFax03.value = document.frm01.hFax.value;		
			}else{
				document.frm01.hAddress03.value = "";
				document.frm01.hTumbon03.value = "";
				document.frm01.hAmphur03.value = "";
				document.frm01.hProvince03.value = "";
				document.frm01.hZip03.value = "";		
				document.frm01.hTumbonCode03.value = "";
				document.frm01.hAmphurCode03.value = "";
				document.frm01.hProvinceCode03.value = "";
				document.frm01.hZipCode03.value = "";		
				document.frm01.hTel03.value = "";		
				document.frm01.hFax03.value = "";		
			}
			
		}
   
   }

</script>
<br>

<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="bcardUpdate.php?#free" method="POST"  onKeyDown="if(event.keyCode==13) event.keyCode=9;">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderStatus" value="<?=$objOrder->getOrderStatus()?>">
	  <input type="hidden" name="hCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingNumber" value="<?=$objOrder->getBookingNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">	 	  
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	   <input type="hidden" name="hTrick" value="true">
		<input type="hidden" name="hFrom" value="<?=$hFrom?>">	
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrder->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$Day02?></td>
					<td>-</td>
					<td><?=$Month02?></td>
					<td>-</td>
					<td><?=$Year02?></td>
					<td>&nbsp;</td>
					<td></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" >
				<input type="hidden" readonly size="3" name="hBookingSaleId"  value="<?=$objOrder->getBookingSaleId();?>">
				<?=$hSaleName?>
			</td>
			<td class="i_background03" width="20%"><strong>�ҤҨͧ</strong> </td>
			<td width="30%">
				<?=$objOrder->getOrderReservePrice()?> �ҷ
			</td>			
		</tr>
		<tr>	
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" ><?=nl2br($objOrder->getBookingRemark())?></td>		
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>������١���</strong> </td>
			<td width="30%">
				<?$objCustomerGroup  = new CustomerGroup();
					$objCustomerGroup->setGroupId($objCustomer->getGroupId());
					$objCustomerGroup->load();
					echo $objCustomerGroup->getTitle();
				?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$Day?></td>
					<td>-</td>
					<td><?=$Month?></td>
					<td>-</td>
					<td><?=$Year?></td>
					<td>&nbsp;</td>
					<td></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong> </td>
			<td valign="top">
				<input type="hidden" name="hEventId"  value="<?=$objCustomer->getEventId()?>">
				<?=$objEvent->getTitle()?>

			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td >
				<input type="hidden" readonly size="3" name="hCustomerSaleId"  value="<?=$objCustomer->getSaleId();?>">
				<?=$hCustomerSaleName?>
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><?=$objCustomer->getIDCard();?></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGrade  = new CustomerGrade();
					$objCustomerGrade->setGradeId($objCustomer->getGradeId());
					$objCustomerGrade->load();
					echo $objCustomerGrade->getTitle();
				?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=$objCustomer->getTitleDetail();?>
					</td>
					<td valign="top">
					<?if($objCustomer->getFirstname() != ""){
						$name = $objCustomer->getFirstname()."  ".$objCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<?=$name?>
					</td>
					<td valign="top">					
					<?=$objCustomer->getCustomerTitleDetail();?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$Day01?></td>
					<td>-</td>
					<td><?=$Month01?></td>
					<td>-</td>
					<td><?=$Year01?></td>
					<td>&nbsp;</td>
					<td></td>		
				</tr>
				</table>
			</td>
		</tr>
		</table>
		
		<table width="98%" cellpadding="0" cellspacing="0" align="center">
		<tr><td align="right">		
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>				
		</td></tr>
		<tr>
		<td  class="i_background">
		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><?=$objCustomer->getAddress();?></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode"  value="<?=$objCustomer->getTumbonCode();?>"><?=$objCustomer->getTumbon();?></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3" readonly name="hAmphurCode"  value="<?=$objCustomer->getAmphurCode();?>"><?=$objCustomer->getAmphur();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode"  value="<?=$objCustomer->getProvinceCode();?>"><?=$objCustomer->getProvince();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode"  value="<?=$objCustomer->getZip();?>"><?=$objCustomer->getZip();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><?=$objCustomer->getHomeTel();?></td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><?=$objCustomer->getMobile();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><?=$objCustomer->getOfficeTel();?></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objCustomer->getFax();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><?=$objCustomer->getEmail();?></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><?=$objCustomer->getAddress01();?></td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode01"  value="<?=$objCustomer->getTumbonCode01();?>"><?=$objCustomer->getTumbon01();?> </td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%" valign="top"><input type="hidden" size="3" readonly name="hAmphurCode01"  value="<?=$objCustomer->getAmphurCode01();?>"><?=$objCustomer->getAmphur01();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode01"  value="<?=$objCustomer->getProvinceCode01();?>"><?=$objCustomer->getProvince01();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode01"  value="<?=$objCustomer->getZip01();?>"><?=$objCustomer->getZip01();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objCustomer->getTel01();?></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objCustomer->getFax01();?></td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><?=$objCustomer->getAddress02();?></td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hTumbonCode02"  value="<?=$objCustomer->getTumbonCode02();?>"><?=$objCustomer->getTumbon02();?> </td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hAmphurCode02"  value="<?=$objCustomer->getAmphurCode02();?>"><?=$objCustomer->getAmphur02();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode02"  value="<?=$objCustomer->getProvinceCode02();?>"><?=$objCustomer->getProvince02();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode02"  value="<?=$objCustomer->getZip02();?>"><?=$objCustomer->getZip02();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objCustomer->getTel02();?></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objCustomer->getFax02();?></td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><?=$objCustomer->getAddress03();?></td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden"  readonly size="3" readonly name="hTumbonCode03"  value="<?=$objCustomer->getTumbonCode03();?>"><?=$objCustomer->getTumbon03();?></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden"  readonly size="3" readonly name="hAmphurCode03"  value="<?=$objCustomer->getAmphurCode03();?>"><?=$objCustomer->getAmphur03();?></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hProvinceCode03"  value="<?=$objCustomer->getProvinceCode03();?>"><?=$objCustomer->getProvince03();?></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hZipCode03"  value="<?=$objCustomer->getZip03();?>"><?=$objCustomer->getZip03();?></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objCustomer->getTel03();?></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objCustomer->getFax03();?></td>
		</tr>		
		
		</table>
		
		</td></tr></table>
		<br>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="20%" class="i_background03"><strong>Ẻö</strong></td>			
			<?if($hCarSeries == ""){
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				$hCarSeries = $objCarSeries->getTitle();
				$arrCarSeries = explode(" ",$hCarSeries);
				$hCC = $arrCarSeries[0];
				$hCarModelId = $objCarSeries->getCarModelId();
				}
			?>
			<td width="30%"><input type="hidden" readonly size="3" name="hBookingCarSeries"  value="<?=$objOrder->getBookingCarSeries();?>"><?=$hCarSeries;?></td>		
			<td class="i_background03" valign="top" width="20%"><strong>���ö</strong> </td>
			<td valign="top" width="30%">
				<input type="hidden" name="hBookingCarModel" >
				<label id="car_model"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>				
		</tr>			
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<input type="hidden" name="hBookingCarType">
				<label id="car_type"><?=$objCarSeries->getCarTypeDetail()?></label>	
			</td>		
			<td class="i_background03" width="20%"><strong>Model</strong> </td>
			<td width="30%">
				<label id="car_code"><?=$objCarSeries->getModel()?></label>				
			</td>
		</tr>			
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td><?=$objOrder->getBookingCarGear();?></td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColor = new CarColor();
					$objCarColor->setCarColorId($objOrder->getBookingCarColor());
					$objCarColor->load();
					echo $objCarColor->getTitle();
				?>
			</td>
		</tr>		
		<tr>		
			<td class="i_background03" ><strong>�ҤҢ��</strong> </td>
			<td >
				<input type="text" name="hBookingCarPrice" size="20"  onblur="document.frm01.hOrderPrice.value=this.value;sum_all()" onfocus="document.frm01.hOrderPrice.value=this.value;sum_all()"   value="<?=$objOrder->getBookingCarPrice()?>"> �ҷ <font color=red>(�Ҥ��ԧ���������͡���ö)</font>
			</td>	
			<td  class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td ><?=$objOrder->getBookingCarRemark()?></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink"><a name="free">�ͧ��</a></td>
</tr>
</table>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
	
<table width="100%" class="i_background05" cellpadding="1" cellspacing="0">
<tr>
	<td class="ListDetail02" width="50%" align="center">��¡�âͧ��</td>
	<td class="ListDetail02" width="50%" align="center">�ͧ Premium</td>
</tr>
<tr>
	<td bgcolor="#778899" width="50%" align="center" valign="top">
		<table width=100%>
		<tr>
			<td width="60%" class="listTitle"  align="center">��¡�âͧ��</td>
			<td width="5%"  class="listTitle" align="center">�ӹǹ</td>
			<td width="15%"  class="listTitle" align="center">�Ҥ�</td>
			<td width="10%"  class="listTitle" align="center">TMT</td>
			<td width="10%"  class="listTitle" align="center"></td>
		</tr>		
		<tr>
			<td valign="top" class="listDetail"><input type="hidden" name="hOrderProductId"><input type="text" size=35 name="hOrderProduct" ><input type="checkbox" name="hOrderProductOther" value=1>����</td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProductQty" value="1"></td>
			<td valign="top"  class="listDetail" align="right"><input type="text" size="10" style="text-align:right"  name="hOrderProductPrice" value=""></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderProductTMT" value="1"></td>
			<td valign="top" class="listDetail" align="center">

			<input type="submit" name="hOrderProductAdd" value="&nbsp;����&nbsp;" class="button" onclick="return check_product();">

			</td>
		</tr>
		<tr>
			<td width="60%"  class="listTitle" align="center">��¡�âͧ��</td>
			<td width="5%" class="listTitle"  align="center">�ӹǹ</td>
			<td width="15%" class="listTitle"  align="center">�Ҥ�</td>
			<td width="10%" class="listTitle"  align="center"></td>
			<td width="10%"  class="listTitle" align="center">ź</td>
		</tr>				
		<?if($objOrderStockList->mCount > 0){?>
			<?
			$i=0;
			$nPriceProductTMT = 0;
			$nPriceProductBuzz = 0;
			forEach($objOrderStockList->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPriceProductTMT = $nPriceProductTMT+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPriceProductBuzz = $nPriceProductBuzz+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<input type="hidden" name="hOrderProductStockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderProductTMT_<?=$i?>" value="<?=$objItem->getTMT()?>">			
			<td valign="top" class="i_background04"><input type="checkbox" value=1 name="hCheckProduct[<?=$objItem->getOrderStockId()?>]" <?if($hCheckProduct[$objItem->getOrderStockId()]) echo "checked"?>><input type="hidden" name="hOrderProductId_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?>&nbsp;<font color=red>[<?=$objStockProduct->getCode();?>]</font></td>
			<td valign="top"  class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProductQty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderProductPrice_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderProductDelete_<?=$i?>" value="1"></td>
			
		</tr>
				<?}else{?>
		<tr>
			<input type="hidden" name="hOrderProductStockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderProductTMT_<?=$i?>" value="<?=$objItem->getTMT()?>">
			<input type="hidden" name="hOrderProductOther_<?=$i?>" value="1">
			<td valign="top" class="i_background04"><input type="checkbox" value=1 name="hCheckProduct[<?=$objItem->getOrderStockId()?>]" <?if($hCheckProduct[$objItem->getOrderStockId()]) echo "checked"?>><input type="hidden" name="hOrderProductId_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;���� <input type="text" size="25" name="hOrderProductRemark_<?=$i?>" value="<?=$objItem->getRemark();?>"></td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProductQty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderProductPrice_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top"  class="listDetail" align="center"><input type="checkbox" name="hOrderProductDelete_<?=$i?>" value="1"></td>
		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>		
		<tr>
			<td valign="top" colspan="4"><input type="hidden" name="hCountOrderProduct" value="<?=$i?>"></td>
			<td valign="top" align="center" colspan="1"><input type="submit" name="hOrderProductUpdate" value="���" class="button"></td>
 
		</tr>
		</table>
	
	</td>
	<td bgcolor="#e9967a" width="50%" align="center" valign="top">
		<table width=100%>
		<tr>
			<td width="60%" class="listTitle"  align="center">��¡�âͧ Premium</td>
			<td width="5%"  class="listTitle" align="center">�ӹǹ</td>
			<td width="15%"  class="listTitle" align="center">�Ҥ�</td>
			<td width="10%"  class="listTitle" align="center">TMT</td>
			<td width="10%"  class="listTitle" align="center"></td>
		</tr>		
		<tr>
			<td valign="top" class="listDetail"><input type="hidden" name="hOrderPremiumId"><input type="text" size=35 name="hOrderPremium" ><input type="checkbox" name="hOrderPremiumOther" value=1>����</td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderPremiumQty" value="1"></td>
			<td valign="top"  class="listDetail" align="right"><input type="text" size="10" style="text-align:right"  name="hOrderPremiumPrice" value=""></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderPremiumTMT" value="1"></td>
			<td valign="top" class="listDetail" align="center"><input type="submit" name="hOrderPremiumAdd" value="&nbsp;����&nbsp;" class="button" onclick="return check_premium();"></td>
		</tr>
		<tr>
			<td width="60%"  class="listTitle" align="center">��¡�âͧ Premium</td>
			<td width="5%" class="listTitle"  align="center">�ӹǹ</td>
			<td width="15%" class="listTitle"  align="center">�Ҥ�</td>
			<td width="10%" class="listTitle"  align="center"></td>
			<td width="10%"  class="listTitle" align="center">ź</td>
		</tr>				
		<?if($objOrderStockPremiumList->mCount > 0){?>
			<?
			$i=0;
			$nPricePremiumTMT = 0;
			$nPricePremiumBuzz = 0;
			forEach($objOrderStockPremiumList->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPricePremiumTMT = $nPricePremiumTMT+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPricePremiumBuzz = $nPricePremiumBuzz+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<input type="hidden" name="hOrderPremiumStockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderPremiumTMT_<?=$i?>" value="<?=$objItem->getTMT()?>">			
			<td valign="top" class="i_background04"><input type="hidden" name="hOrderPremiumId_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?></td>
			<td valign="top"  class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderPremiumQty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderPremiumPrice_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderPremiumDelete_<?=$i?>" value="1"></td>
			
		</tr>
				<?}else{?>
		<tr>
			<input type="hidden" name="hOrderPremiumStockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderPremiumTMT_<?=$i?>" value="<?=$objItem->getTMT()?>">
			<input type="hidden" name="hOrderPremiumOther_<?=$i?>" value="1">
			<td valign="top" class="i_background04"><input type="hidden" name="hOrderPremiumId_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;���� <input type="text" size="25" name="hOrderPremiumRemark_<?=$i?>" value="<?=$objItem->getRemark();?>"></td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderPremiumQty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderPremiumPrice_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top"  class="listDetail" align="center"><input type="checkbox" name="hOrderPremiumDelete_<?=$i?>" value="1"></td>
		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>		
		<tr>
			<td valign="top" colspan="4"><input type="hidden" name="hCountOrderPremium" value="<?=$i?>"></td>
			<td valign="top" align="center" colspan="1"><input type="submit" name="hOrderPremiumUpdate" value="���" class="button"></td>
 
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td valign="top" class="i_background02">
	</td>
	<td valign="top" class="i_background02">	
	</td>
</tr>
<tr>
	<td  class="i_background02">
		<table width=100%>
		<tr>
			<td  class="listTitle"   align="center"><strong>�ͧ�� Buzz</strong></td>
			<td  class="listTitle"    align="center"><strong>�ͧ�� TMT</strong></td>
			<td   class="listTitle"     align="center"><strong>����ͧ��</strong></td>
		</tr>
		<tr>
			<td bgcolor="#b0c4de" align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderProductPriceTotalBuzz" value="<?=$nPriceProductBuzz?>"></td>
			<td bgcolor="#b0c4de" align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderProductPriceTotalTMT" value="<?=$nPriceProductTMT?>"></td>
			<td bgcolor="#b0c4de" align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderProductPriceTotal" value="<?=$nPriceProductBuzz+$nPriceProductTMT?>"></td>
		</tr>
		</table>			
	</td>
	<td  class="i_background02">
		<table width=100%>
		<tr>
			<td  class="listTitle"     align="center"><strong>�ͧ Premium Buzz</strong></td>
			<td  class="listTitle"     align="center"><strong>�ͧ Premium TMT</strong></td>
			<td  class="listTitle"     align="center"><strong>����ͧ Premium</strong></td>
		</tr>
		<tr>
			<td bgcolor="#b0c4de" align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderPremiumPriceTotalBuzz" value="<?=$nPricePremiumBuzz?>"></td>
			<td bgcolor="#b0c4de"align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderPremiumPriceTotalTMT" value="<?=$nPricePremiumTMT?>"></td>
			<td bgcolor="#b0c4de" align="center"><input type="text" size="15"  readonly style="text-align:right" name="hOrderPremiumPriceTotal" value="<?=$nPricePremiumBuzz+$nPricePremiumTMT?>"></td>

		</tr>
		</table>

	</td>
</tr>
<tr>
	<td colspan="2">
		<table width="100%" cellpadding="2" cellspacing="1">
		<tr>
			<td class="listTitle" width="17%" >��ǹŴ�Թʴ</td>
			<td class="listDetail" ><input  size="15"  onblur="sum_all()" onfocus="sum_all()"  type="text" name="hDiscountPrice" value="<?=$objOrder->getDiscountPrice()?>"> �ҷ&nbsp;&nbsp;��ǹŴ�͡���¾���� <input  size="15"  onblur="sum_all()" onfocus="sum_all()"  type="text" name="hDiscountTMT" value="<?=$objOrder->getDiscountTMT()?>"> �ҷ</td>
			<td class="listTitle" width="17%" >��ǹŴ Subdown</td>
			<td class="listDetail" align="left"><input  size="15"  onblur="frm01.hDiscountSubdown01.value=this.value;sum_all()" onfocus="frm01.hDiscountSubdown01.value=this.value;sum_all()"  type="text" name="hDiscountSubdown" value="<?=$objOrder->getDiscountSubdown()?>"> �ҷ</td>
		</tr>
		<tr>
			<td  class="listTitle"  align="left">��ǹŴ Subdown Vat (7%)</td>
			<td bgcolor="#b0c4de" align="left" ><input  size="15"  type="text" name="hDiscountSubdownVat"  readonly  value="<?=$objOrder->getDiscountSubdownVat()?>">�ҷ</td>
			<td  class="listTitle"  align="left">��ǹŴ��Сѹ</td>
			<td bgcolor="#b0c4de" align="left"><input  size="15"  type="text" name="hDiscountInsurance" readonly  value="<?=$objOrder->getDiscountInsurance()?>"> �ҷ</td>
		</tr>
		<tr>		
			<td  class="listTitle"  align="left">����ͧ��</td>
			<td bgcolor="#b0c4de" align="left"><input  size="15"  type="text" name="hSumPremium" readonly  value="<?=$nPricePremiumBuzz+$nPricePremiumTMT+$nPriceProductBuzz+$nPriceProductTMT?>"> �ҷ</td>
			<td  class="listTitle"   align="left">�����ǹŴ</td>
			<td bgcolor="#b0c4de" align="left"><input  size="15"   type="text" name="hDiscountSumFree" readonly  value=""> �ҷ</td>
		</tr>
		<tr>
			<td class="listTitle" align="left">�Թ��������ǹŴ�Թ</td>
			<td class="listDetail" align="left"><input  size="15"  onblur="sum_all()" onfocus="sum_all()"  type="text" name="hDiscountCustomerPay" value="<?=$objOrder->getDiscountCustomerPay()?>"> �ҷ&nbsp;&nbsp;<input  size="50" type="text" name="hDiscountCustomerPayReason" value="<?=$objOrder->getDiscountCustomerPayReason()?>"> </td>
			<td  class="listTitle"  align="left">����ط��</td>
			<td bgcolor="#b0c4de" align="left"><input  size="15"  type="text" name="hDiscountAll" readonly  value="<?=$objOrder->getDiscountAll()?>"> �ҷ</td>				
		</tr>
		<tr>
			<td class="listTitle" align="left">Margin ����˹�</td>
			<td class="listDetail" align="left">
			
			
			<table>
			<tr>
				<td><input  size="15"  onblur="sum_all()" onfocus="sum_all()"  type="text" name="hDiscountMargin" value="<?=$objOrder->getDiscountMargin()?>"> �ҷ</td>
				<td>�ѹ����˹� Margin</td>
				<td>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day05 value="<?=$Day05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month05 value="<?=$Month05?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year05 value="<?=$Year05?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year05,Day05, Month05, Year05,popCal);return false"></td>		
				</tr>
				</table>
				</td>
			</tr>
			</table>

			
			</td>
			<td  class="listTitle"  align="left">�Ҵ�Թ</td>
			<td bgcolor="#b0c4de" align="left" ><input  size="15"  readonly  type="text" name="hDiscountOver" value="<?=$objOrder->getDiscountOver()?>"> �ҷ</td>			

		</tr>
		</table>

	
	</td>
</tr>
</table>

	</td>
</tr>
</table>
<br>


<table width="100%" align="center" cellpadding="3" cellspacing="1" bgcolor="#000000">
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   width="20%">�Ҥҷ���¨�ԧ</td>
	<td  >&nbsp;</td>
	<td   width="20%" align="center"><input type="text"  onblur="document.frm01.hOrderPriceTotal01.value=this.value;sum_all()" onfocus="document.frm01.hOrderPriceTotal01.value=this.value;sum_all()"  name="hOrderPrice" size="20"  value="<?=$objOrder->getOrderPrice()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  >�ػ�ó�Դ���</td>
	<td  >&nbsp;</td>
	<td   align="right">&nbsp;</td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td >��ǹŴ�Թʴ</td>
	<td >&nbsp;</td>
	<td  align="center" bgcolor="#b0c4de"><input type="text" readonly  name="hDiscountPrice01" size="20"  value="<?=$objOrder->getDiscountPrice()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  align="right"><strong>����Թ</strong></td>
	<td >&nbsp;</td>
	<td align="center" bgcolor="#b0c4de"><input type="text" readonly  name="hOrderPriceTotal01" size="20"  value="<?=$objOrder->getOrderPrice()?>"></td>
</tr>
</table>

<br>

<table width="100%" align="center" cellpadding="3" cellspacing="1" bgcolor="#000000">
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  colspan="3">
	<input type="radio" name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 ) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" name="hBuyType" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��&nbsp;&nbsp;
	<input type="text" name="hBuyTime" size="5"  value="<?if($objOrder->getBuyTime() > 0) echo $objOrder->getBuyTime();?>"> �Ǵ&nbsp;&nbsp;
	�Ǵ�� <input type="text" onblur="sum_all()" onfocus="sum_all()" name="hBuyPrice" size="15"  value="<?if($objOrder->getBuyPrice() > 0) echo $objOrder->getBuyPrice();?>">&nbsp;&nbsp;
	�͡���� <input type="text" name="hBuyFee" size="5"  value="<?if($objOrder->getBuyFee() > 0) echo $objOrder->getBuyFee();?>"> %  &nbsp;&nbsp;
	�.�ṹ�� (���.) <?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?>
	</td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  width="20%">�����Թ��ǹ�/���ö</td>
	<td  >�ʹ�Ѵ &nbsp;<input type="text" onfocus="sum_all()"  onblur="document.frm01.hBuyDown.value=(document.frm01.hOrderPriceTotal01.value-this.value);sum_all()"  name="hBuyTotal" size="20"  value="<?if($objOrder->getBuyTotal() > 0) echo $objOrder->getBuyTotal();?>"></td>
	<td    width="20%"  align="center" bgcolor="#b0c4de"><input type="text" name="hBuyDown" size="20"  value="<?if($objOrder->getBuyDown() > 0) echo $objOrder->getBuyDown();?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td  >�����Թ�Ǵ�á</td>
	<td  ><input type="radio" name="hBuyBegin" onclick="document.frm01.hBuyBeginPrice.value=document.frm01.hBuyPrice.value" value=1 <?if($objOrder->getBuyBegin() == 1 ) echo "checked"?>>�鹧Ǵ&nbsp;&nbsp;&nbsp;<input type="radio"  onclick="document.frm01.hBuyBeginPrice.value=''"  name="hBuyBegin" value=2 <?if($objOrder->getBuyBegin() == 2 ) echo "checked"?>>���§Ǵ</td>
	<td   align="center" bgcolor="#b0c4de"><input type="text"  name="hBuyBeginPrice" size="20"  value="<?if($objOrder->getBuyBegin() == 1 ) echo $objOrder->getBuyPrice();?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" ><a name="equip">�١��ҫ����ػ�ó�����</a></td>
	<td >

		<table width=100%>
		<tr>
			<td width="60%" class="listTitle"  align="center">��¡���ػ�ó�</td>
			<td width="5%"  class="listTitle" align="center">�ӹǹ</td>
			<td width="15%"  class="listTitle" align="center">�Ҥ�</td>
			<td width="10%"  class="listTitle" align="center">TMT</td>
			<td width="10%"  class="listTitle" align="center"></td>
		</tr>		
		<tr>
			<td valign="top" class="listDetail"><input type="hidden" name="hOrderProduct01Id"><input type="text" size=35 name="hOrderProduct01" ><input type="checkbox" name="hOrderProduct01Other" value=1>����</td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProduct01Qty" value="1"></td>
			<td valign="top"  class="listDetail" align="right"><input type="text" size="10" style="text-align:right"  name="hOrderProduct01Price" value=""></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderProduct01TMT" value="1"></td>
			<td valign="top" class="listDetail" align="center"><input type="submit" name="hOrderProduct01Add" value="&nbsp;����&nbsp;" class="button" onclick="return check_product01();"></td>
		</tr>
		<tr>
			<td width="60%"  class="listTitle" align="center">��¡���ػ�ó�</td>
			<td width="5%" class="listTitle"  align="center">�ӹǹ</td>
			<td width="15%" class="listTitle"  align="center">�Ҥ�</td>
			<td width="10%" class="listTitle"  align="center"></td>
			<td width="10%"  class="listTitle" align="center">ź</td>
		</tr>				
		<?if($objOrderStockList01->mCount > 0){?>
			<?
			$i=0;
			$nPriceProductTMT01 = 0;
			$nPriceProductBuzz01 = 0;
			forEach($objOrderStockList01->getItemList() as $objItem) {
					if($objItem->getTMT() == 1){
						$strBuzz = "[TMT]";
						$nPriceProductTMT01 = $nPriceProductTMT01+($objItem->getPrice()*$objItem->getQty());
					}else{
						$strBuzz = "[Buzz]";
						$nPriceProductBuzz01 = $nPriceProductBuzz01+($objItem->getPrice()*$objItem->getQty());
					}
			
				if( $objItem->getStockProductId() > 0){
					$objStockProduct = new StockProduct();
					$objStockProduct->setStockProductId($objItem->getStockProductId());
					$objStockProduct->load();
					
			?>
		<tr>
			<input type="hidden" name="hOrderProduct01StockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderProduct01TMT_<?=$i?>" value="<?=$objItem->getTMT()?>">			
			<td valign="top" class="i_background04"><input type="checkbox" value=1 name="hCheckProduct03[<?=$objItem->getOrderStockId()?>]" <?if($hCheckProduct03[$objItem->getOrderStockId()]) echo "checked"?>><input type="hidden" name="hOrderProduct01Id_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;<?=$objStockProduct->getTitle();?>&nbsp;<font color=red>[<?=$objStockProduct->getCode();?>]</font></td>
			<td valign="top"  class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProduct01Qty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderProduct01Price_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top" class="listDetail" align="center"><input type="checkbox" name="hOrderProduct01Delete_<?=$i?>" value="1"></td>
			
		</tr>
				<?}else{?>
		<tr>
			<input type="hidden" name="hOrderProduct01StockId_<?=$i?>" value="<?=$objItem->getOrderStockId()?>">
			<input type="hidden" name="hOrderProduct01TMT_<?=$i?>" value="<?=$objItem->getTMT()?>">
			<input type="hidden" name="hOrderProduct01Other_<?=$i?>" value="1">
			<td valign="top" class="i_background04"><input type="checkbox" value=1 name="hCheckProduct03[<?=$objItem->getOrderStockId()?>]" <?if($hCheckProduct03[$objItem->getOrderStockId()]) echo "checked"?>><input type="hidden" name="hOrderProduct01Id_<?=$i?>" value="<?=$objItem->getStockProductId()?>"><?=$strBuzz?>&nbsp;���� <input type="text" size="25" name="hOrderProduct01Remark_<?=$i?>" value="<?=$objItem->getRemark();?>"></td>
			<td valign="top" class="listDetail"><input type="text" size="2" style="text-align:right"  name="hOrderProduct01Qty_<?=$i?>" value="<?=$objItem->getQty()?>"></td>
			<td valign="top" class="listDetail" align="right"><input type="text" size="10" readonly style="text-align:right"  name="hOrderProduct01Price_<?=$i?>" value="<?=$objItem->getPrice();?>"></td>
			<td valign="top" class="listDetail"></td>
			<td valign="top"  class="listDetail" align="center"><input type="checkbox" name="hOrderProduct01Delete_<?=$i?>" value="1"></td>
		</tr>
				<?}?>
			<?
			$i++;
			}?>
		<?}?>		
		<tr>
			<td valign="top" colspan="4"><input type="hidden" name="hCountOrderProduct01" value="<?=$i?>"></td>
			<td valign="top" align="center" colspan="1"><input type="submit" name="hOrderProduct01Update" value="���" class="button"></td>
 
		</tr>
		</table>
	
	
	
	
	</td>
	<td valign="top"  align="center" bgcolor="#b0c4de"><input type="text" readonly name="hEquipPrice" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?=$nPriceProductTMT01+$nPriceProductBuzz01?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td valign="top" >����</td>
	<td >
		<table width="100%" cellpadding="3" cellspacing="3">
		<tr><td valign="top">�ӹǹ : </td><td><input type="text" name="hBuyEngine" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?if($objOrder->getBuyEngine() > 0) echo $objOrder->getBuyEngine();?>"> �ҷ</td></tr>
		<tr>
			<td >��������´ : </td><td>
				<textarea name="hBuyEngineRemark" cols="60" rows="3"><?=$objOrder->getBuyEngineRemark()?></textarea>
			</td>
			
		</tr>
		</table>	
	</td>
	<td  align="center" bgcolor="#b0c4de"><input type="text" readonly name="hBuyProduct" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?if($objOrder->getBuyProduct() > 0) echo $objOrder->getBuyProduct();?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   valign="top" >���»�Сѹ (1)</td>
	<td >
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03"><strong>��Сѹ����Ҩҡ</strong> </td>
			<td colspan="3">
				<input type="Radio" onclick="checkInsure();sum_all();" name="hInsureFrom" value="1" <?if($objOrder->getInsureFrom() ==1) echo "checked"?>>���
				<label id="insure" <?if($objOrder->getInsureFrom() !=1) echo "style='display:none'"?>>�� <input type="text" name="hInsureFromDetail" size="30"  value="<?=$objOrder->getInsureFromDetail()?>"></label>
				<input type="Radio"  onclick="checkInsure();sum_all();"  name="hInsureFrom" value="2" <?if($objOrder->getInsureFrom() ==2) echo "checked"?>>�� TMT
				<input type="Radio"  onclick="checkInsure();sum_all();"  name="hInsureFrom" value="3" <?if($objOrder->getInsureFrom() ==3) echo "checked"?>>�� BUZZ
				<input type="Radio"  onclick="checkInsure();sum_all();"  name="hInsureFrom" value="4" <?if($objOrder->getInsureFrom() ==4) echo "checked"?>>�١���������
			</td>			
		</tr>			
		<tr>
			<td class="i_background03"><strong>����ѷ��Сѹ���</strong> </td>
			<td>
				<?$objInsureCompanyList->printSelect("hInsureCompany",$objOrder->getInsureCompany(),"��س����͡");?>
			</td>	
			<td class="i_background03"><strong>�ѹ������ػ�Сѹ���</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day06 value="<?=$Day06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month06 value="<?=$Month06?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year06 value="<?=$Year06?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year06,Day06, Month06, Year06,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>������»�Сѹ���</strong></td>
			<td ><input type="text" onblur="sum_all()" onfocus="sum_all()" name="hInsurePrice" size="10"  value="<?if($objOrder->getInsurePrice() > 0) echo $objOrder->getInsurePrice();?>"> �ҷ</td>
			<td class="i_background03"><strong>������</strong> </td>
			<td >
				<select  name="hInsureType">
					<option value="1" <?if($objOrder->getInsureType() ==1) echo "selected"?>>��Сѹ��ª�� 1
					<option value="2" <?if($objOrder->getInsureType() ==2) echo "selected"?>>��Сѹ��ª�� 2
					<option value="3" <?if($objOrder->getInsureType() ==3) echo "selected"?>>��Сѹ��ª�� 3
					<option value="4" <?if($objOrder->getInsureType() ==4) echo "selected"?>>GOA
				</select>
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>��Сѹ�շ��</strong>   </td>
			<td ><input type="text" name="hInsureYear" size="10"  value="<?if($objOrder->getInsureYear() > 0) echo $objOrder->getInsureYear();?>"> </td>
			<td class="i_background03"><strong>�ع��Сѹ</strong>   </td>
			<td ><input type="text" name="hInsureBudget" size="10"  value="<?if($objOrder->getInsureBudget() > 0) echo $objOrder->getInsureBudget();?>"> �ҷ </td>
		</tr>
		<tr>			
			<td valign="top" class="i_background03"><strong>�١��Ҩ�������</strong>   </td>
			<td valign="top" ><input type="text"  onblur="sum_all()" onfocus="sum_all()"  name="hInsureCustomerPay" size="10"  value="<?if($objOrder->getInsureCustomerPay() > 0) echo $objOrder->getInsureCustomerPay();?>"> �ҷ </td>
			<td valign="top" class="i_background03"><strong>�����˵�</strong></td>
			<td ><textarea name="hInsureRemark" cols="30" rows="3"><?=$objOrder->getInsureRemark()?></textarea></td>
		</tr>	
		</table>	
	
	</td>
	<td  align="center" valign="top"  bgcolor="#b0c4de"><input type="text" readonly name="hDiscountInsurance01" size="20"  value=""></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >���� �.�.�.</td>
	<td >����ѷ&nbsp;<?$objPrbCompanyList->printSelect("hPrbCompany",$objOrder->getPrbCompany(),"��س����͡");?>		</td>
	<td  align="center"><input type="text" name="hPrbPrice" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?if($objOrder->getPrbPrice() > 0) echo $objOrder->getPrbPrice();?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >����¹</td>
	<td >
	<table width="100%">
	<tr>
		<td width="70%">
		<input type="radio" name="hRegistryPerson" onclick="check_registry_price()" value=1 <?if($objOrder->getRegistryPerson()==1)  echo "checked"?>> �ؤ�� &nbsp;&nbsp;<input type="radio" name="hRegistryPerson" onclick="check_registry_price()"  value=2 <?if($objOrder->getRegistryPerson()==2)  echo "checked"?>> �ԵԺؤ��
		</td>
		<td align="right"></td>
	</tr>
	</table>	
	</td>
	<td  align="center">
	<input type="text"  name="hRegistryPrice" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?=$objOrder->getRegistryPrice()?>">		
	</td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >�Ѵ�ӻ���ᴧ</td>
	<td >				
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
							�����Ţ&nbsp;<input type="hidden"  readonly name="hStockRedId" size="2"  value="<?=$objOrder->getStockRedId();?>">	
							<input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>
	</td>
	<td  align="center" ><input type="text"  name="hRedCodePrice" onblur="sum_all()" onfocus="sum_all()" size="20"  value="<?=$objOrder->getRedCodePrice()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   align="right" ><strong>����Թ</strong></td>
	<td >&nbsp;</td>
	<td  align="center" bgcolor="#b0c4de"><input type="text"  readonly name="hSumAll01" size="20"  value=""></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >�ѡ Sub ��ǹ�</td>
	<td >&nbsp;</td>
	<td  align="center" bgcolor="#b0c4de"><input  size="20"   readonly type="text" name="hDiscountSubdown01" value="<?=$objOrder->getDiscountSubdown()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >�ѡ�Ѵ��ö</td>
	<td >���駷�� 1. <input type="text" name="hOrderReserve" size="10"   onblur="sum_order_reserve();sum_all();" onfocus="sum_order_reserve();sum_all();"  value="<?=$objOrder->getOrderReservePrice()?>">&nbsp;���駷�� 2. <input type="text" name="hOrderReserve01"  onblur="sum_order_reserve();sum_all();" onfocus="sum_order_reserve();sum_all();" size="10"  value="<?=$objOrder->getOrderReservePrice01()?>">&nbsp;���駷�� 3. <input type="text" name="hOrderReserve02"  onblur="sum_order_reserve();sum_all();" onfocus="sum_order_reserve();sum_all();" size="10"  value="<?=$objOrder->getOrderReservePrice02()?>"></td>
	<td  align="center" bgcolor="#b0c4de"><input type="text" readonly name="hOrderReserveAll" size="20"  value=""></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >��ǹŴ Subdown Vat (7%)</td>
	<td >&nbsp;</td>
	<td  align="center" bgcolor="#b0c4de"><input  size="20"  type="text" name="hDiscountSubdownVat01"  readonly  value="<?=$objOrder->getDiscountSubdownVat()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   >ö���</td>
	<td >&nbsp;</td>
	<td  align="center"><input  size="20"  type="text" onblur="sum_all()" onfocus="sum_all()" name="hDiscountOldCar"  value="<?=$objOrder->getOrderOldCar()?>"></td>
</tr>
<tr id="choice1" style="background-color:#f0f0f0; cursor:hand" onmouseover="movein(this)" onmouseout="moveout01(this)">
	<td   align="right"  ><strong>�����Թ���</strong></td>
	<td >&nbsp;</td>
	<td  align="center" bgcolor="#b0c4de"><input type="text"  readonly name="hSumAll02" size="20"  value=""></td>
</tr>
</table>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ TMT</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%" ><strong>�ͧ�������</strong></td>
			<td width="30%" valign="top" ><input type="text" name="hDiscountOther" size="80"  value="<?echo $objOrder->getDiscountOther();?>"></td>
			<td class="i_background03" align="right"  width="20%" ><strong>�ӹǹ�Թ</strong> </td>
			<td width="30%" valign="top"><input type="text" name="hDiscountOtherPrice" size="10"  value="<?if($objOrder->getDiscountOtherPrice() > 0) echo $objOrder->getDiscountOtherPrice();?>"> �ҷ</td>			
		</tr>			
		<tr>
			<td class="i_background03" width="20%" ><strong>�ͧ��</strong></td>
			<td width="30%" valign="top" ><input type="text" name="hTMTFreeRemark" size="80"  value="<?echo $objOrder->getTMTFreeRemark();?>"></td>
			<td class="i_background03" align="right"  width="20%" ><strong>�ӹǹ�Թ</strong> </td>
			<td width="30%" valign="top"><input type="text" name="hTMTFree" size="10"  value="<?if($objOrder->getTMTFree() > 0) echo $objOrder->getTMTFree();?>"> �ҷ</td>			
		</tr>			
		<tr>
			<td class="i_background03" width="20%" ><strong>�Թ�Ѵ�մ</strong></td>
			<td width="30%" valign="top" ><input type="text" name="hTMTCouponRemark" size="80"  value="<?echo $objOrder->getTMTCouponRemark();?>"></td>
			<td class="i_background03" align="right"  width="20%" ><strong>�ӹǹ�Թ</strong> </td>
			<td width="30%" valign="top"><input type="text" name="hTMTCoupon" size="10"  value="<?if($objOrder->getTMTCoupon() > 0) echo $objOrder->getTMTCoupon();?>"> �ҷ</td>			
		</tr>	
		<tr>
			<td class="i_background03" width="20%" ><strong>����</strong></td>
			<td width="30%" valign="top" ><input type="text" name="hTMTOtherRemark" size="80"  value="<?echo $objOrder->getTMTOtherRemark();?>"></td>
			<td class="i_background03" align="right" width="20%" ><strong>�ӹǹ�Թ</strong> </td>
			<td width="30%" valign="top"><input type="text" name="hTMTOther" size="10"  value="<?if($objOrder->getTMTOther() > 0) echo $objOrder->getTMTOther();?>"> �ҷ</td>			
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background_pink">ʶҹШҡ�ѭ��</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>

<table >
<tr>
	<td>��Ǩ�ͧ</td>
	<td><input type="radio" name="hStatus" value=1 <?if($objOrder->getCheckAccountBooking() == 1) echo "checked"?>> ��ҹ���͹��ѵ� &nbsp;&nbsp;<input type="radio" name="hStatus" value=2 <?if($objOrder->getCheckAccountBooking() == 2) echo "checked"?>> ����ҹ���͹��ѵ�&nbsp;&nbsp;  <input type="radio" name="hStatus" value=3 <?if($objOrder->getCheckAccountBooking() == 3) echo "checked"?>> ����ҹ���͹��ѵ����͡ʶҹ����� ��Ǩ�ͧ CRL&nbsp;  </td>
</tr>
<tr>
	<td valign="top">�����˵�</td>
	<td><textarea rows="5" cols="100" name="hRemark"><?=$objOrder->getCheckAccountBookingRemark()?></textarea></td>
</tr>
</table>

	</td>
</tr>
</table>

<table align="center" width="100%">

<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
	  

	  
	<?if ($strMode == "Update"){?>
		<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();">
	<?}else{?>
		<input type="button" name="hSubmit1" value="�ѹ�֡��¡��" class="button" onclick="return check_submit();">
	<?}?>
	&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Button" name="hSubmit" class="button" value="¡��ԡ��¡��" onclick="window.location='<?=$hFrom?>'">		
	<br><br>
	</td>
</tr>
</table>

</form>


<table width="100%">
<tr>
	<td width="50%"><iframe frameborder="0" src="order_file.php?hId=<?=$hId?>" width=300 height=200> </iframe></td>
	<td width="50%"><iframe frameborder="0" width="500" height="200" src="order_update.php?hId=<?=$hId?>"> </iframe></td>
</tr>
</table>
<?
	include("h_footer.php")
?>
<script>
	
	new CAPXOUS.AutoComplete("hOrderProduct", function() {
		return "bcardAutoStockProduct.php?hBookingCarSeries="+document.frm01.hBookingCarSeries.value+"&q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hOrderProduct01", function() {
		return "bcardAutoStockProduct01.php?hBookingCarSeries="+document.frm01.hBookingCarSeries.value+"&q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hOrderPremium", function() {
		return "bcardAutoStockProductPremium.php?q=" + this.text.value;
	});		

	new CAPXOUS.AutoComplete("hStockRedName", function() {
		return "ccardAutoRedCode.php?q=" + this.text.value;
	});		
	
	function check_product(){
	
	if(document.frm01.hOrderProductOther.checked == false){	
		if(document.frm01.hOrderProductId.value == "" || document.frm01.hOrderProductId.value == 0){
			alert("��س����͡��¡���Թ��Ҩҡ�к�");
			document.frm01.hTrick.value="false";	
			document.frm01.hOrderProduct.focus();	
			return false;
		}
	}else{
		if(document.frm01.hOrderProduct.value== ""){
			alert("��س��кت����Թ���");
			document.frm01.hTrick.value="false";		
			document.frm01.hOrderProduct.focus();
			return false;
		}
	
	}
	
	if(document.frm01.hOrderProductQty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		document.frm01.hTrick.value="false";		
		document.frm01.hOrderProductQty.focus();
		return false;
	}


	return true;		
	}
	
	function check_product01(){
	
	if(document.frm01.hOrderProduct01Other.checked == false){	
		if(document.frm01.hOrderProduct01Id.value == "" || document.frm01.hOrderProduct01Id.value == 0){
			alert("��س����͡��¡���Թ��Ҩҡ�к�");
			document.frm01.hTrick.value="false";	
			document.frm01.hOrderProduct01.focus();	
			return false;
		}
	}else{
		if(document.frm01.hOrderProduct01.value== ""){
			alert("��س��кت����Թ���");
			document.frm01.hTrick.value="false";		
			document.frm01.hOrderProduct01.focus();
			return false;
		}
	
	}
	
	if(document.frm01.hOrderProduct01Qty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		document.frm01.hTrick.value="false";		
		document.frm01.hOrderProduct01Qty.focus();
		return false;
	}


	return true;		
	}
	
	
	

	function check_premium(){
	
	if(document.frm01.hOrderPremiumOther.checked == false){	
		if(document.frm01.hOrderPremiumId.value == "" || document.frm01.hOrderPremiumId.value == 0){
			alert("��س����͡��¡���Թ��Ҩҡ�к�");
			document.frm01.hTrick.value="false";	
			document.frm01.hOrderPremium.focus();	
			return false;
		}
	}else{
		if(document.frm01.hOrderPremium.value== ""){
			alert("��س��кت����Թ���");
			document.frm01.hTrick.value="false";		
			document.frm01.hOrderPremium.focus();
			return false;
		}
	
	}
	
	if(document.frm01.hOrderPremiumQty.value== ""){
		alert("��س��кبӹǹ����ͧ���");
		document.frm01.hTrick.value="false";		
		document.frm01.hOrderPremiumQty.focus();
		return false;
	}

	return true;		
	}	
	
	function checkInsure(){
		if(document.frm01.hInsureFrom[0].checked == true){
			document.getElementById("insure").style.display = "";	
		}else{
			document.getElementById("insure").style.display = "none";	
		}	
	}	
	
	function sum_subdown_vat(){
		totalVat=0;
		if(document.frm01.hDiscountSubdown.value != "" && document.frm01.hDiscountSubdown.value != "0"){
			totalVat = Math.round((document.frm01.hDiscountSubdown.value*1.07)-document.frm01.hDiscountSubdown.value);		

		}
	
		document.frm01.hDiscountSubdownVat.value= totalVat;	
		document.frm01.hDiscountSubdownVat01.value= totalVat;	
	
	}
	
	
	function sum_order_price(){
		hDiscountPrice=0;
		hOrderPrice=0;
		total=0;
		if(document.frm01.hDiscountPrice.value != "" && document.frm01.hDiscountPrice.value != "0"){
			hDiscountPrice = document.frm01.hDiscountPrice.value*1;	
			document.frm01.hDiscountPrice01.value = hDiscountPrice;
		}
		
		if(document.frm01.hOrderPrice.value != "" && document.frm01.hOrderPrice.value != "0"){
			hOrderPrice = document.frm01.hOrderPrice.value*1;	
		}
		total = hOrderPrice-hDiscountPrice;
	
		document.frm01.hOrderPriceTotal01.value= total;	
	
	}
		
	function sum_buydown(){
		hOrderPriceTotal01=0;
		hBuyTotal=0;
		total=0;
		if(document.frm01.hOrderPriceTotal01.value != "" && document.frm01.hOrderPriceTotal01.value != "0"){
			hOrderPriceTotal01 = document.frm01.hOrderPriceTotal01.value*1;	
		}
		
		if(document.frm01.hBuyTotal.value != "" && document.frm01.hBuyTotal.value != "0"){
			hBuyTotal = document.frm01.hBuyTotal.value*1;	
		}
		total = hOrderPriceTotal01-hBuyTotal;
	
		document.frm01.hBuyDown.value= total;	
	}
	
	function sum_buy_engine(){
		hBuyEngine=0;
		hDiscountCustomerPay=0;
		total=0;
		if(document.frm01.hBuyEngine.value != "" && document.frm01.hBuyEngine.value != "0"){
			hBuyEngine = document.frm01.hBuyEngine.value*1;	
		}
		
		if(document.frm01.hDiscountCustomerPay.value != "" && document.frm01.hDiscountCustomerPay.value != "0"){
			hDiscountCustomerPay= (document.frm01.hDiscountCustomerPay.value*1);
		}
		total = hBuyEngine+hDiscountCustomerPay;
	
		document.frm01.hBuyProduct.value= total;	
	}
	
	
	
	function sum_free(){
		hDiscountPrice=0;
		hDiscountSubdown=0;
		hDiscountInsurance=0;
		hDiscountTMT=0;
		if(document.frm01.hDiscountPrice.value != "" && document.frm01.hDiscountPrice.value != "0"){
			hDiscountPrice= (document.frm01.hDiscountPrice.value*1);
		}
		
		if(document.frm01.hDiscountSubdown.value != "" && document.frm01.hDiscountSubdown.value != "0"){
			hDiscountSubdown= (document.frm01.hDiscountSubdown.value*1);
		}
		
		if(document.frm01.hDiscountInsurance.value != "" && document.frm01.hDiscountInsurance.value != "0"){
			hDiscountInsurance= (document.frm01.hDiscountInsurance.value*1);
		}
		
		if(document.frm01.hDiscountTMT.value != "" && document.frm01.hDiscountTMT.value != "0"){
			hDiscountTMT= (document.frm01.hDiscountTMT.value*1);
		}
		
		total = hDiscountPrice+hDiscountSubdown+hDiscountInsurance+hDiscountTMT;
		document.frm01.hDiscountSumFree.value = Math.round(total);
		
	}
	
	function sum_insure(){
		hInsurePrice=0;
		hInsureCustomerPay=0;
		total=0;
		if(document.frm01.hInsurePrice.value != "" && document.frm01.hInsurePrice.value != "0"){
			hInsurePrice= (document.frm01.hInsurePrice.value*1);
		}
		
		if(document.frm01.hInsureCustomerPay.value != "" && document.frm01.hInsureCustomerPay.value != "0"){
			hInsureCustomerPay= (document.frm01.hInsureCustomerPay.value*1);
		}
		
		if( document.frm01.hInsureFrom[0].checked == true ){			

				total = hInsurePrice;
				document.frm01.hDiscountInsurance.value = 0;
				document.frm01.hDiscountInsurance01.value = Math.round(total);
				
		}else{
		
				total = hInsurePrice-hInsureCustomerPay;		
				total01 = hInsureCustomerPay;		
				document.frm01.hDiscountInsurance.value = Math.round(total);
				document.frm01.hDiscountInsurance01.value = Math.round(total01);
			
		}		
	}
	
	
	function sum_over(){
		hDiscountAll=0;
		hDiscountMargin=0;
		hTotal=0;
		if(document.frm01.hDiscountAll.value != "" && document.frm01.hDiscountAll.value != "0"){
			hDiscountAll= (document.frm01.hDiscountAll.value*1);
		}
		
		if(document.frm01.hDiscountMargin.value != "" && document.frm01.hDiscountMargin.value != "0"){
			hDiscountMargin= (document.frm01.hDiscountMargin.value*1);
		}
		
		hTotal = hDiscountMargin - hDiscountAll;
		document.frm01.hDiscountOver.value = Math.round(hTotal);
		
		
	}
	
	function sum_discount_customer_pay(){
		hSumPremium=0;
		hDiscountCustomerPay=0;
		hTotal=0;
		if(document.frm01.hSumPremium.value != "" && document.frm01.hSumPremium.value != "0"){
			hSumPremium= (document.frm01.hSumPremium.value*1);
		}
		
		if(document.frm01.hDiscountCustomerPay.value != "" && document.frm01.hDiscountCustomerPay.value != "0"){
			hDiscountCustomerPay= (document.frm01.hDiscountCustomerPay.value*1);
		}
		hTotal = hSumPremium -hDiscountCustomerPay;
		document.frm01.hSumPremium.value= Math.round(hTotal);
		document.frm01.hBuyProduct.value= Math.round(hDiscountCustomerPay);
	}
	
	function sum_premium(){
		hOrderProductPriceTotal=0;
		hOrderPremiumPriceTotal=0;
		
		hTotal=0;
		if(document.frm01.hOrderProductPriceTotal.value != "" && document.frm01.hOrderProductPriceTotal.value != "0"){
			hOrderProductPriceTotal= (document.frm01.hOrderProductPriceTotal.value*1);
		}
		
		if(document.frm01.hOrderPremiumPriceTotal.value != "" && document.frm01.hOrderPremiumPriceTotal.value != "0"){
			hOrderPremiumPriceTotal= (document.frm01.hOrderPremiumPriceTotal.value*1);
		}
		
		hTotal = hOrderProductPriceTotal + hOrderPremiumPriceTotal;
		document.frm01.hSumPremium.value = Math.round(hTotal);		
		
		
	}	
	
	function sum_discount_all(){
		hSumPremium=0;
		hDiscountSumFree=0;
		
		hTotal=0;
		if(document.frm01.hSumPremium.value != "" && document.frm01.hSumPremium.value != "0"){
			hSumPremium= (document.frm01.hSumPremium.value*1);
		}
		
		if(document.frm01.hDiscountSumFree.value != "" && document.frm01.hDiscountSumFree.value != "0"){
			hDiscountSumFree= (document.frm01.hDiscountSumFree.value*1);
		}
		
		
		hTotal = hSumPremium + hDiscountSumFree;
		document.frm01.hDiscountAll.value = Math.round(hTotal);		
		
		
	}
	
	
	function sum_all(){
		sum_free();
		sum_subdown_vat();
		sum_insure();
		sum_premium();
		sum_order_price();		
		sum_discount_customer_pay();
		sum_buy_engine();
		sum_discount_all();		
		sum_over();		
		sum_buydown();
		sum_all02();
	}
	
	sum_all();

	
	function sum_order_reserve(){
		hOrderReserve=0;
		hOrderReserve01=0;
		hOrderReserve02=0;
		total=0;
		if(document.frm01.hOrderReserve.value != "" && document.frm01.hOrderReserve.value != "0"){
			hOrderReserve= (document.frm01.hOrderReserve.value*1);
		}
		if(document.frm01.hOrderReserve01.value != "" && document.frm01.hOrderReserve01.value != "0"){
			hOrderReserve01= (document.frm01.hOrderReserve01.value*1);
		}
		if(document.frm01.hOrderReserve02.value != "" && document.frm01.hOrderReserve02.value != "0"){
			hOrderReserve02= (document.frm01.hOrderReserve02.value*1);
		}
		
		total = hOrderReserve+hOrderReserve01+hOrderReserve02;
		document.frm01.hOrderReserveAll.value = Math.round(total);
		
		
	}
	
	function sum_all01(){
		sum_order_reserve();	
	
		hBuyDown=0;
		if(document.frm01.hBuyDown.value != "" && document.frm01.hBuyDown.value != "0"){hBuyDown= (document.frm01.hBuyDown.value*1);}
		
		hBuyBeginPrice=0;
		if(document.frm01.hBuyBeginPrice.value != "" && document.frm01.hBuyBeginPrice.value != "0"){hBuyBeginPrice= (document.frm01.hBuyBeginPrice.value*1);}		
		
		hBuyProduct=0;
		if(document.frm01.hBuyProduct.value != "" && document.frm01.hBuyProduct.value != "0"){hBuyProduct= (document.frm01.hBuyProduct.value*1);}		
		
		hDiscountInsurance01=0;
		if(document.frm01.hDiscountInsurance01.value != "" && document.frm01.hDiscountInsurance01.value != "0"){hDiscountInsurance01= (document.frm01.hDiscountInsurance01.value*1);}		
		
		hPrbPrice=0;
		if(document.frm01.hPrbPrice.value != "" && document.frm01.hPrbPrice.value != "0"){hPrbPrice= (document.frm01.hPrbPrice.value*1);}		
		
		hRegistryPrice=0;
		if(document.frm01.hRegistryPrice.value != "" && document.frm01.hRegistryPrice.value != "0"){hRegistryPrice= (document.frm01.hRegistryPrice.value*1);}		
		
		hRedCodePrice=0;
		if(document.frm01.hRedCodePrice.value != "" && document.frm01.hRedCodePrice.value != "0"){hRedCodePrice= (document.frm01.hRedCodePrice.value*1);}		
		
		total = 0;
		total = hBuyDown+hBuyBeginPrice+hBuyProduct+hDiscountInsurance01+hPrbPrice+hRegistryPrice+hRedCodePrice;
		document.frm01.hSumAll01.value = Math.round(total);
		
	}

	function sum_all02(){
		sum_all01();
	
		hSumAll01=0;
		if(document.frm01.hSumAll01.value != "" && document.frm01.hSumAll01.value != "0"){hSumAll01= (document.frm01.hSumAll01.value*1);}		
	
		hDiscountSubdown01=0;
		if(document.frm01.hDiscountSubdown01.value != "" && document.frm01.hDiscountSubdown01.value != "0"){hDiscountSubdown01= (document.frm01.hDiscountSubdown01.value*1);}		
				
		hOrderReserveAll=0;
		if(document.frm01.hOrderReserveAll.value != "" && document.frm01.hOrderReserveAll.value != "0"){hOrderReserveAll= (document.frm01.hOrderReserveAll.value*1);}				
		
		hDiscountSubdownVat01=0;
		if(document.frm01.hDiscountSubdownVat01.value != "" && document.frm01.hDiscountSubdownVat01.value != "0"){hDiscountSubdownVat01= (document.frm01.hDiscountSubdownVat01.value*1);}		
				
		hDiscountOldCar=0;
		if(document.frm01.hDiscountOldCar.value != "" && document.frm01.hDiscountOldCar.value != "0"){hDiscountOldCar= (document.frm01.hDiscountOldCar.value*1);}				
		
		hEquipPrice=0;
		if(document.frm01.hEquipPrice.value != "" && document.frm01.hEquipPrice.value != "0"){hEquipPrice= (document.frm01.hEquipPrice.value*1);}				
		
		
		total=0;
		total=hSumAll01-hDiscountSubdown01-hOrderReserveAll+hDiscountSubdownVat01-hDiscountOldCar+hEquipPrice;
		document.frm01.hSumAll02.value = Math.round(total);		
	}
	
	
	function checkRedCode(){
		if(document.frm01.hNoRedCode.checked == true){
			document.frm01.hStockRedId.value = "";
			document.frm01.hStockRedName.value="";
		}
	}
	
	
	function check_registry_price(){
	<?
	$objRP = new RegistryPrice();
	if(!$objRP->loadByCondition(" car_model_id = '".$hCarModelId."' and cc = '".$hCC."'    ")){
		$objRP->loadByCondition(" car_model_id = '".$hCarModelId."'   ");
	}
	
	?>	
	if(document.frm01.hRegistryPerson[0].checked == true){
			document.frm01.hRegistryPrice.value = "<?=$objRP->getPrice1();?>";
	}else{
			document.frm01.hRegistryPrice.value = "<?=$objRP->getPrice3();?>";
	}
	sum_all();
	}
	
</script>
<?PHP
Function DropDownLanguage($SelectName,$Selected,$SelectDefault) 
{
    Echo "\n<select name=\"".$SelectName."\">\n";
    If ($SelectDefault) Echo "\t<option value=\"\">".$SelectDefault."</option>\n";
    $strSql = "SELECT * FROM Language ORDER BY languageName";

    $objDb = New Db;
    $objDb->getConnection();
    If ($result = $objDb->query($strSql))
    {
        While ( $row = $result->nextRow() )
        {
        If ( ($Selected == $row->languageCode) Or ($Selected == $row->languageName) ) { Echo "\t<option value=\"".$row->languageCode."\" selected>".$row->languageName."</option>\n"; }
        Else {Echo "\t<option value=\"".$row->languageCode."\">".$row->languageName."</option>\n"; }
        }
    }
    $result->freeResult;
    UnSet($objDb);

    Echo "</select>";
 //   Return $strHtml;
}

Function getLanguageName($languageCode){
	$strSql = "select languageName from Language where languageCode = '".$languageCode."'";
    $objDb = New Db;
    $objDb->getConnection();
	$sth = mysql_query($strSql) or die("Can't select language code");
	if (mysql_num_rows($sth) != 0){
		while ($result = mysql_fetch_array($sth)){
		  	return $result[languageName];	
		}
	}
    $result->freeResult;
}

Function setLanguage($websiteCode) {
    
    $objDb = New Db();
    $strSql = " SELECT L.* "
            . " FROM Website "
            . " LEFT JOIN WebsiteLanguage AS WL "
            . " ON Website.websiteCode = WL.websiteCode "
            . " LEFT JOIN Language AS L "
            . " ON L.languageCode = WL.languageCode "
            . " WHERE Website.WebsiteCode = '".$websiteCode."' ";
    $objDb->getConnection();
    If ($Result = $objDb->query($strSql)) {
        If ($Row = $Result->nextRow()) {
            
            Global $sLanguageCode, $sCharset;
            
            $sLanguageCode = $Row->languageCode;
            $sCharset = $Row->charset;
            
            session_register("sLanguageCode");
            session_register("sCharset");
        }
    }
}

Function RedMsg($strError) {
    Echo "<span class='Error'>".$strError."</span>";
}

Function DisplayError($TextErr)
{
    If ( Trim($TextErr) != "" )  {?>
    &nbsp;&nbsp;<span class="error"><?=$TextErr?></span>
<?php }
}

Function LimitString($str,$intSize) {
    If (StrLen($str) <= $intSize) Return $str;
    Else Return SubStr($str,0,$intSize)."...";
}

Function DropDownMenu($SelectList,$SelectName,$Selected,$SelectDefault,$SelectJs = NULL) {
    
    Echo "\n<select name=\"".$SelectName."\" ".$SelectJs.">\n";
    
    If ($SelectDefault)
        If ($Selected == "")     Echo "\t<option value=\"\" selected>".$SelectDefault."</option>\n";
        Else Echo "\t<option value=\"\">".$SelectDefault."</option>\n";
    
    While (List($key,$val) = Each($SelectList))
        If (($Selected == $key) AND ($Selected!=""))
            Echo "\t<option value=\"".$key."\" selected>".$val."</option>\n";
        Else
            Echo "\t<option value=\"".$key."\">".$val."</option>\n";
    
    Echo "</select>";
}

Function RadioButton($SelectList,$SelectName,$Selected) {
    
    While (List($key,$val) = Each($SelectList)) {
        If (($Selected == $key) AND ($Selected!=""))
            $strCheck = " checked ";
        Else $strCheck = "";
        Echo "\t<input type=\"radio\" name=\"".$SelectName."\" value=\"".$key."\" ".$strCheck.">".$val."\n";
    }
}

Function Dropdown($arrValue,$SelectName, $Selected,$SelectDefault=Null, $Optional=Null)
{
    Echo "\n<select name=\"".$SelectName."\" ".$Optional.">\n";
    If ($SelectDefault) Echo "\t<option value=\"\">".$SelectDefault."</option>\n";
    
    If (Is_Array($arrValue)) {
    ForEach ($arrValue As $Key => $strValue)
    {
        Echo "\t<option value=\"".$Key."\"";
        If ( (($Selected == $Key) Or ($Selected == $strValue)) AND ($Selected!=""))
            { Echo " selected"; }
        Echo ">".$strValue."</option>\n";
    }
    }
    Echo "</select>\n";
}

Function checkDropdown($value,$selectName)
{
	$arrValue = explode(",",$value);
	$size  = sizeof($arrValue);
	if($size > 1){
		echo "<select name='".$selectName."'>";
		for($i=0;$i<$size;$i++){
			echo "<option value='".$arrValue[$i]."'>".$arrValue[$i];
		}
		echo "</select>";
	}else{
		echo "<input type='hidden' name='".$selectName."' value='".$value."'  >";
		echo $value;
	}
}


Function UrlFormat($strUrl) {
    If ($strUrl) {
        If (!StrStr(StrToLower($strUrl),"http://")) {
            Return "http://".Trim($strUrl);
        }
    }
    Return $strUrl;
}

Function HttpToString($HttpString)
{ 
    $strReturn = HtmlSpecialChars(StripSlashes(Trim($HttpString)));
    Return $strReturn;
}

Function UrlConcatParameter($Url,$NewParameter)
{
    If (StrStr($Url,"?") == True)
    { Return $Url ."&" .$NewParameter; }
    Else
    { Return $Url ."?" .$NewParameter; }
}

Function SqlConcatQuery(&$OldSql,$NewSql,$Operator=Null)
//$Operator = OR/AND
{
    If ( ($OldSql == "") And ($NewSql == "") )
    { Return ""; }
    
    If (!IsSet($Operator))
    { $Operator = "AND"; }

    If ( $OldSql == "" )
    {
        If ( (StrStr(StrToUpper($NewSql),"WHERE")) )
        { $OldSql = $NewSql; }
        Else
        { $OldSql = " WHERE (" . $NewSql . ")"; }
    }
    Else
    {
        If (!StrStr(StrToLower($OldSql),"where"))
        { $OldSql = " WHERE " .$OldSql;}
        
        If ($NewSql!="")
        { $OldSql = $OldSql . " ". $Operator ." (". $NewSql . ")"; }
        Else
        { $OldSql = $OldSql; }
    }
    
    If (SubStr_Count($OldSql, "WHERE")>1 )
    {
        $strTemp = Str_Replace ("WHERE", "", $OldSql); 
        $OldSql = " WHERE " .$strTemp;
    }
    Return $OldSql;
}

Function currencyFormat($Money)
{
	return number_format($Money,2);
}

Function calculateAge($age){


}

Function checkSwearword($mystring){
	$objBBSSwearwordList = new sBBSSwearwordList();
	$objBBSSwearwordList->setPageSize(0);
	$objBBSSwearwordList->setSortDefault(" title ASC");
	$objBBSSwearwordList->load();

	forEach($objBBSSwearwordList->getItemList() as $objItem) {
		$pos= strpos($mystring, $objItem->getTitle());
		$pos= $pos." ";
		
		if(trim($pos) != ""){
			//echo $mystring." : found<br>";
			return true;
		}else{
			//echo $mystring." : not found<br>";
			//return false;
		}
	}

	return false;
}

Function checkIcon($mystring){
	$objBBSIconList = new sBBSIconList();
	$objBBSIconList->setPageSize(0);
	$objBBSIconList->setSortDefault(" title ASC");
	$objBBSIconList->load();

	forEach($objBBSIconList->getItemList() as $objItem) {
		$strReplace = '<img src="'.PATH_IMAGE.$objItem->getIcon().'">';
		$mystring = str_replace($objItem->getCode(),$strReplace,$mystring);
	}
	return $mystring;
}

Function printOrderNo($orderNo)
{
	if($orderNo < 10){ return "0000".$orderNo; }
	if($orderNo >= 10 AND $orderNo < 100 ){ return "000".$orderNo; }
	if($orderNo >= 100 AND $orderNo < 1000 ){ return "00".$orderNo; }
	if($orderNo >= 1000 AND $orderNo < 10000 ){ return "0".$orderNo; }
	if($orderNo >= 10000){ return $orderNo; }
}

Function currencyThai($money)
{


	//��Ѻ format ���������ٻ xxx.xx
	$currency  = number_format($money,2);
	//echo $currency." = ";	

	$arrCurrency = explode(".",$currency);
	$arrCurrencyBath = explode(",",$arrCurrency[0]);
	$currencyBath = implode($arrCurrencyBath,"");

	if(strlen($currencyBath) > 6){
		$currencyMillion = substr($currencyBath,0,strlen($currencyBath)-6);
		$currencyBath = substr($currencyBath,-6);
	}else{
		$currencyMillion = 0;
		$currencyBath = $currencyBath;
	}
	$currencySatang = $arrCurrency[1];
	
	if($currencyBath > 0 or $currencyMillion > 0){
	
			if($currencyMillion >0){
		
				for($i=0;$i<strlen($currencyMillion);$i++){
					$strPosition = strlen($currencyMillion)-$i;
					$strSatang = substr($currencyMillion,$i,1);
					if($strSatang >0){
						echo thaiNumeric($strSatang,$strPosition);
					}
				}
				echo "��ҹ";
			}
			
			
			if($currencyBath >0){
				for($i=0;$i<strlen($currencyBath);$i++){
					$strPosition = strlen($currencyBath)-$i;
					$strSatang = substr($currencyBath,$i,1);
					//echo $strSatang."-".$strPosition."<br>";
					if($strSatang >0){
						echo thaiNumeric($strSatang,$strPosition);
					}
					//$currencySatang = substr($currencySatang,$i,strlen($currencySatang)-1);
				}
			}
		echo "�ҷ";
		
	}
	//ʵҧ��
	if($arrCurrency[1] >0){
		for($i=0;$i<strlen($arrCurrency[1]);$i++){
			$strPosition = strlen($arrCurrency[1])-$i;
			$strSatang = substr($currencySatang,$i,1);
			//echo $strSatang."-".$strPosition."<br>";
			if($strSatang >0){
				echo thaiNumeric($strSatang,$strPosition);
			}
			//$currencySatang = substr($currencySatang,$i,strlen($currencySatang)-1);
		}
		echo "ʵҧ��";
	}
	
	//echo $arrCurrency[0];
}

Function currencyThai01($money)
{


	//��Ѻ format ���������ٻ xxx.xx
	$currency  = number_format($money,2);
	//echo $currency." = ";	

	$arrCurrency = explode(".",$currency);
	$arrCurrencyBath = explode(",",$arrCurrency[0]);
	$currencyBath = implode($arrCurrencyBath,"");

	if(strlen($currencyBath) > 6){
		$currencyMillion = substr($currencyBath,0,strlen($currencyBath)-6);
		$currencyBath = substr($currencyBath,-6);
	}else{
		$currencyMillion = 0;
		$currencyBath = $currencyBath;
	}
	$currencySatang = $arrCurrency[1];
	
	if($currencyBath > 0 or $currencyMillion > 0){
	
			if($currencyMillion >0){
		
				for($i=0;$i<strlen($currencyMillion);$i++){
					$strPosition = strlen($currencyMillion)-$i;
					$strSatang = substr($currencyMillion,$i,1);
					if($strSatang >0){
						$text.= thaiNumeric($strSatang,$strPosition);
					}
				}
				$text.= "��ҹ";
			}
			
			
			if($currencyBath >0){
				for($i=0;$i<strlen($currencyBath);$i++){
					$strPosition = strlen($currencyBath)-$i;
					$strSatang = substr($currencyBath,$i,1);
					//echo $strSatang."-".$strPosition."<br>";
					if($strSatang >0){
						$text.= thaiNumeric($strSatang,$strPosition);
					}
					//$currencySatang = substr($currencySatang,$i,strlen($currencySatang)-1);
				}
			}
		$text.= "�ҷ";
		
	}
	//ʵҧ��
	if($arrCurrency[1] >0){
		for($i=0;$i<strlen($arrCurrency[1]);$i++){
			$strPosition = strlen($arrCurrency[1])-$i;
			$strSatang = substr($currencySatang,$i,1);
			//echo $strSatang."-".$strPosition."<br>";
			if($strSatang >0){
				$text.= thaiNumeric($strSatang,$strPosition);
			}
			//$currencySatang = substr($currencySatang,$i,strlen($currencySatang)-1);
		}
		$text.= "ʵҧ��";
	}
	
	return $text;
}

Function thaiNumeric($numeric,$position){
	switch($numeric){
		case 0 : 
		$strNumeric =  "�ٹ��";
		break;
		case 1 : 
			if($position == 2){
				$strNumeric =  "";
			}else{
				$strNumeric =  "˹��";
			}
		break;
		case 2 : 
			if($position == 2){
				$strNumeric =  "���";
			}else{
				$strNumeric =  "�ͧ";
			}
		break;
		case 3 : 
		$strNumeric =  "���";
		break;
		case 4 : 
		$strNumeric =  "���";
		break;
		case 5 : 
		$strNumeric =  "���";
		break;
		case 6 : 
		$strNumeric =  "ˡ";
		break;
		case 7 : 
		$strNumeric =  "��";
		break;
		case 8 : 
		$strNumeric =  "Ỵ";
		break;
		case 9 : 
		$strNumeric =  "���";
		break;
	}

	switch ($position){
		case 1:
			$strNumeric.= "";
			break;
		case 2:
			$strNumeric.= "�Ժ";
			break;
		case 3:
			$strNumeric.= "����";
			break;
		case 4:
			$strNumeric.= "�ѹ";
			break;
		case 5:
			$strNumeric.= "����";
			break;
		case 6:
			$strNumeric.= "�ʹ";
			break;
		case 7:
			$strNumeric.= "��ҹ";
			break;
	}
	
	return $strNumeric;
}


function checkMinute($num){
	if($num > 0){
		$num = $num*1;
		echo number_format($num,2);	
	}else{
		if($num == 0){
			echo "0.00";
		}else{
			$num = $num*1;
			echo number_format($num,2);	
		}
	}
}

function checkMobile($num){
	$arrNum = explode(",",$num);
	if(sizeof($arrNum) == 1){
		if(strlen(trim($num)) == 10){
			$str01 = substr($num,0,3);
			$str02 = substr($num,3,3);
			$str03 = substr($num,6,4);
			$allStr = $str01."-".$str02."-".$str03;	
		}else{
			$allStr=$num;
		}
	
	}else{
		for($i=0;$i<sizeof($arrNum);$i++){
				if(strlen(trim($arrNum[$i])) == 10){
					$str01 = substr($arrNum[$i],0,3);
					$str02 = substr($arrNum[$i],3,3);
					$str03 = substr($arrNum[$i],6,4);
					$allStr = $str01."-".$str02."-".$str03;	
					$arrNum[$i]=$allStr;
				}
		}
		$allStr =  implode(",",$arrNum);
	}
	return $allStr;
}

function checkTel($num){
	$arrNum = explode(",",$num);
	if(sizeof($arrNum) == 1){
		if(strlen(trim($num)) == 10){
			$str01 = substr($num,0,3);
			$str02 = substr($num,3,3);
			$str03 = substr($num,6,4);
			$allStr = $str01."-".$str02."-".$str03;	
		}else{
			if(strlen(trim($num)) == 9){
				$str01 = substr($num,0,2);
				$str02 = substr($num,2,3);
				$str03 = substr($num,5,4);
				$allStr = $str01."-".$str02."-".$str03;	
			}else{
				if(strlen(trim($num)) > 15){
					$anum = substr($num,0,10);
					$bnum = substr($num,10,100);

					$allStr = checkTel($anum);
					
					$arrStr = $allStr.' '.$bnum;
				}else{
					$allStr=$num;
				}
			}
		}
	
	}else{
		for($i=0;$i<sizeof($arrNum);$i++){
				if(strlen(trim($arrNum[$i])) == 10){
					$str01 = substr($arrNum[$i],0,3);
					$str02 = substr($arrNum[$i],3,3);
					$str03 = substr($arrNum[$i],6,4);
					$allStr = $str01."-".$str02."-".$str03;	
					$arrNum[$i]=$allStr;
				}else{
					if(strlen(trim($arrNum[$i])) == 9){
						$str01 = substr($arrNum[$i],0,2);
						$str02 = substr($arrNum[$i],2,3);
						$str03 = substr($arrNum[$i],5,4);
						$allStr = $str01."-".$str02."-".$str03;	
						$arrNum[$i]=$allStr;
					}else{
						if(strlen(trim($arrNum[$i])) > 15){
							$anum = substr($arrNum[$i],0,10);
							$bnum = substr($arrNum[$i],10,100);

							$allStr = checkTel($anum);
							
							$arrNum[$i] = $allStr.' '.$bnum;
						}
					}
				}
		}
		$allStr =  implode(",",$arrNum);
	}
	return $allStr;
}

function utf8_to_tis620($string) {
  $str = $string;
  $res = "";
  for ($i = 0; $i < strlen($str); $i++) {
    if (ord($str[$i]) == 224) {
      $unicode = ord($str[$i+2]) & 0x3F;
      $unicode |= (ord($str[$i+1]) & 0x3F) << 6;
      $unicode |= (ord($str[$i]) & 0x0F) << 12;
      $res .= chr($unicode-0x0E00+0xA0);
      $i += 2;
    } else {
      $res .= $str[$i];
    }
  }
  return $res;
}

	
function tis2utf8($tis) {
   for( $i=0 ; $i< strlen($tis) ; $i++ ){
      $s = substr($tis, $i, 1);
      $val = ord($s);
      if( $val < 0x80 ){
         $utf8 .= $s;
      } elseif ( ( 0xA1 <= $val and $val <= 0xDA ) or ( 0xDF <= $val and $val <= 0xFB ) ){
         $unicode = 0x0E00 + $val - 0xA0;
         $utf8 .= chr( 0xE0 | ($unicode >> 12) );
         $utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
         $utf8 .= chr( 0x80 | ($unicode & 0x3F) );
      }
   }
   return $utf8;
}


?>
<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objSendingCustomer = new Customer();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();
$objCarSeries = new CarSeries();
$objSendingEvent = new CustomerEvent();

$objEvent = new CustomerEvent();
$objOrderStockList = new OrderStockList();
$objOrderStockPremiumList = new OrderStockList();
$objOrderStockOtherList = new OrderStockOtherList();
$objOrderStockOtherPremiumList = new OrderStockOtherList();
$objStockProductList = new StockProductList();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomer = new Customer();
/*
$objCustomerBList = new CustomerList();
$objCustomerBList->setFilter(" type_id = 'C' ");
$objCustomerBList->setPageSize(0);
$objCustomerBList->load();

$yearStart = date("Y")."-01-01";
$yearEnd = date("Y")."-12-31";

$objCustomerBYearList = new CustomerList();
$objCustomerBYearList->setFilter(" type_id = 'C'  AND ( add_date >= '$yearStart' AND add_date <= '$yearEnd' ) ");
$objCustomerBYearList->setPageSize(0);
$objCustomerBYearList->load();

$monthStart  = date("Y-m")."-01";
$monthEnd  = date("Y-m")."-31";

$objCustomerBMonthList = new CustomerList();
$objCustomerBMonthList->setFilter(" type_id = 'C' AND ( add_date >= '$monthStart' AND add_date <= '$monthEnd' ) ");
$objCustomerBMonthList->setPageSize(0);
$objCustomerBMonthList->load();

$objCustomerBDayList = new CustomerList();
$objCustomerBDayList->setFilter(" type_id = 'C' AND add_date >= '".date("Y-m-d")." 00:00:00' ");
$objCustomerBDayList->setPageSize(0);
$objCustomerBDayList->load();
*/

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objCustomerEventList = new CustomerEventList();
$objCustomerEventList->setPageSize(0);
$objCustomerEventList->setSortDefault("title ASC");
$objCustomerEventList->load();

$objCarTypeList = new CarTypeList();
$objCarTypeList->setPageSize(0);
$objCarTypeList->setSortDefault("title ASC");
$objCarTypeList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModel1List = new CarSeriesList();
$objCarModel1List->setPageSize(0);
$objCarModel1List->setSortDefault(" model ASC");
$objCarModel1List->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objStockPremiumList = new StockPremiumList();
$objStockPremiumList->setPageSize(0);
$objStockPremiumList->setSortDefault(" title ASC");
$objStockPremiumList->setSort($hSort);
$objStockPremiumList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objStockPremiumList = new StockProductList();
$objStockPremiumList->setFilter(" SPT.code= 'F001' ");
$objStockPremiumList->setPageSize(0);
$objStockPremiumList->setSortDefault(" SP.title ASC");
$objStockPremiumList->setSort($hSort);
$objStockPremiumList->loadCRL();

$objStockProductList = new StockProductList();

$objRegistryConditionList = new RegistryConditionList();
$objRegistryConditionList->setPageSize(0);
$objRegistryConditionList->setSortDefault(" title ASC");
$objRegistryConditionList->load();


$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();		
		
		$arrDate = explode("-",$objOrder->getSendingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getPrbExpire());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryDate01());
		$DayA1= $arrDate[2];
		$MonthA1 = $arrDate[1];
		$YearA1 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryDate02());
		$DayA2= $arrDate[2];
		$MonthA2 = $arrDate[1];
		$YearA2 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryDate05());
		$DayA5= $arrDate[2];
		$MonthA5 = $arrDate[1];
		$YearA5 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryDate07());
		$DayA7= $arrDate[2];
		$MonthA7 = $arrDate[1];
		$YearA7 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryTakeCodeDate());
		$DayA8= $arrDate[2];
		$MonthA8 = $arrDate[1];
		$YearA8 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistryTakeBookDate());
		$DayA9= $arrDate[2];
		$MonthA9 = $arrDate[1];
		$YearA9 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRegistrySendDate());
		$DayA10= $arrDate[2];
		$MonthA10 = $arrDate[1];
		$YearA10 = $arrDate[0];
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		if(isset($hSearchSendingCustomerId)){
			$hCustomerId = $hSearchSendingCustomerId;		
			$objSendingCustomer->setCustomerId($hSearchSendingCustomerId);
			$objSendingCustomer->load();
			
			$objOrder->setSendingCustomerId($hSearchSendingCustomerId);
			
			$strMode="Update";		
		}else{
			$hCustomerId = $objOrder->getSendingCustomerId();		
			$objSendingCustomer->setCustomerId($objOrder->getSendingCustomerId());
			$objSendingCustomer->load();
			$strMode="Update";		
		}
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();		
				
		
		$arrDate = explode("-",$objBookingCustomer->getInformationDate());
		$BookingDay = $arrDate[2];
		$BookingMonth = $arrDate[1];
		$BookingYear = $arrDate[0];
		
		$arrDate = explode("-",$objBookingCustomer->getBirthDay());
		$BookingDay01 = $arrDate[2];
		$BookingMonth01 = $arrDate[1];
		$BookingYear01 = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getInformationDate());
		$DaySending = $arrDate[2];
		$MonthSending = $arrDate[1];
		$YearSending = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getBirthDay());
		$Day01Sending = $arrDate[2];
		$Month01Sending = $arrDate[1];
		$Year01Sending = $arrDate[0];		
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();				
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSendingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();		
		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
		$objCarSeries->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		$objSendingEvent->setEventId($objSendingCustomer->getEventId());
		$objSendingEvent->load();

		$objOrderStockList->setFilter(" SPT.code= 'A001' AND order_id = ".$hId);
		$objOrderStockList->setPageSize(0);
		$objOrderStockList->setSortDefault(" SP.title DESC");
		$objOrderStockList->setSort($hSort);
		$objOrderStockList->load();
		
		$objOrderStockPremiumList->setFilter(" SPT.code= 'F001'  AND order_id = ".$hId);
		$objOrderStockPremiumList->setPageSize(0);
		$objOrderStockPremiumList->setSortDefault(" SP.title DESC");
		$objOrderStockPremiumList->setSort($hSort);
		$objOrderStockPremiumList->load();		
		
		$objOrderStockOtherList->setFilter(" type=1 AND order_id = ".$hId);
		$objOrderStockOtherList->setPageSize(0);
		$objOrderStockOtherList->setSortDefault(" title DESC");
		$objOrderStockOtherList->setSort($hSort);
		$objOrderStockOtherList->load();
		
		$objOrderStockOtherPremiumList->setFilter(" type=2 AND order_id = ".$hId);
		$objOrderStockOtherPremiumList->setPageSize(0);
		$objOrderStockOtherPremiumList->setSortDefault(" title DESC");
		$objOrderStockOtherPremiumList->setSort($hSort);
		$objOrderStockOtherPremiumList->load();		
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {
		$objOrder->setOrderId($hId);
		$objOrder->setBlackCodeStatus($hBlackCodeStatus);
		$objOrder->setBlackCodeNumber($hBlackCodeNumber);
		$hBlackCodeDate = $Year."-".$Month."-".$Day;
		$objOrder->setBlackCodeDate($hBlackCodeDate);	
		$objOrder->setRegistryStatus($hRegistryStatus);
		$objOrder->setRegistryConditionId($hRegistryConditionId);
		$objOrder->setRegistryPrice($hRegistryPrice);
		$objOrder->setRegistryTaxYear($hRegistryTaxYear);
		$objOrder->setRegistryTaxYearNumber($hRegistryTaxYearNumber);
		$hRegistrySendDate = $YearA10."-".$MonthA10."-".$DayA10;
		$objOrder->setRegistrySendDate($hRegistrySendDate);
		$objOrder->setRegistryMissDocument($hRegistryMissDocument);
		$objOrder->setRegistryCondition($hRegistryCondition);
		$objOrder->setRegistryTakeCode($hRegistryTakeCode);
		$hRegistryTakeCodeDate = $YearA8."-".$MonthA8."-".$DayA8;
		$objOrder->setRegistryTakeCodeDate($hRegistryTakeCodeDate);
		$objOrder->setRegistryTakeBook($hRegistryTakeBook);
		$objOrder->setTMBNumber($hTMBNumber);
		$objOrder->setRedCodePrice($hRedCodePrice);
		
		$hRegistryDate01 = $YearA1."-".$MonthA1."-".$DayA1;
		$objOrder->setRegistryDate01($hRegistryDate01);
		$hRegistryDate02 = $YearA2."-".$MonthA2."-".$DayA2;
		$objOrder->setRegistryDate02($hRegistryDate02);
		$hRegistryDate05 = $YearA5."-".$MonthA5."-".$DayA5;
		$objOrder->setRegistryDate05($hRegistryDate05);
		$hRegistryDate07 = $YearA7."-".$MonthA7."-".$DayA7;
		$objOrder->setRegistryDate07($hRegistryDate07);
		$objOrder->setRegistryText03($hRegistryText03);
		$objOrder->setRegistryText04($hRegistryText04);
		$objOrder->setRegistryText05($hRegistryText05);
		$objOrder->setRegistryText06($hRegistryText06);
		$objOrder->setRegistryText07($hRegistryText07);
		$objOrder->setRegistryRemark01($hRegistryRemark01);
		$objOrder->setRegistryRemark02($hRegistryRemark02);
		$objOrder->setRegistryRemark03($hRegistryRemark03);
		$objOrder->setRegistryRemark04($hRegistryRemark04);
		$objOrder->setRegistryRemark05($hRegistryRemark05);
		$objOrder->setRegistryRemark06($hRegistryRemark06);
		$objOrder->setRegistryRemark07($hRegistryRemark07);
		$objOrder->setRegistryRecieveType01($hRegistryRecieveType01);
		$objOrder->setRegistryRecieveType02($hRegistryRecieveType02);	
		
		$hRegistryTakeBookDate = $YearA9."-".$MonthA9."-".$DayA9;
		$objOrder->setRegistryTakeBookDate($hRegistryTakeBookDate);
		$objOrder->setRegistryRemark($hRegistryRemark);
		$objOrder->setRegistryEditBy($sMemberId);
		$objOrder->setRegistryEditDate($hRegistryEditDate);	
		$objOrder->setOrderStatus(7);
		$objOrder->updateRegistry();
		//update registry document
		$i=0;
		forEach($objRegistryConditionList->getItemList() as $objItem) {
			$objRegistryDocument = new RegistryDocument();
			$objRegistryDocument01 = new RegistryDocument();
			if(isset($hRegistryCondition[$i])){
				if(!$objRegistryDocument->loadByCondition(" order_id= $hOrderId AND registry_condition_id = ".$objItem->getRegistryConditionId())){
					$objRegistryDocument01->setOrderId($hOrderId);
					$objRegistryDocument01->setRegistryConditionId($objItem->getRegistryConditionId());
					$objRegistryDocument01->add();
				}
			}else{
				if($objRegistryDocument->loadByCondition(" order_id= $hOrderId AND registry_condition_id = ".$objItem->getRegistryConditionId())){
					$objRegistryDocument01->setRegistryDocumentId($objRegistryDocument->getRegistryDocumentId());
					$objRegistryDocument01->delete();
				}
			}
			$i++;
		}
		
		header("location:ccardNewList.php");
		exit;

	}
}

$pageTitle = "2. �к�ʵ�ͤ����ᴧ-���´�";
$pageContent = "2.2  ������ʵ�ͤ���´�";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<script language=JavaScript >

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			document.frm01.hAddress01.value = document.frm01.hAddress.value;
			document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
			document.frm01.hProvince01.value = document.frm01.hProvince.value;
			document.frm01.hZip01.value = document.frm01.hZip.value;		
			document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax01.value = document.frm01.hFax.value;		
		}
   		if(val ==2){
			document.frm01.hAddress02.value = document.frm01.hAddress.value;
			document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
			document.frm01.hProvince02.value = document.frm01.hProvince.value;
			document.frm01.hZip02.value = document.frm01.hZip.value;		
			document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax02.value = document.frm01.hFax.value;		
		}
   		if(val ==3){
			document.frm01.hAddress03.value = document.frm01.hAddress.value;
			document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
			document.frm01.hProvince03.value = document.frm01.hProvince.value;
			document.frm01.hZip03.value = document.frm01.hZip.value;		
			document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax03.value = document.frm01.hFax.value;		
		}
   
   }

function Set_Display_TypeSendingCustomer(typeVal){
	document.getElementById("form_add01SendingCustomer").style.display = "none";
	document.getElementById("form_add02SendingCustomer").style.display = "none";
	document.getElementById("form_add03SendingCustomer").style.display = "none";
	document.getElementById("form_add04SendingCustomer").style.display = "none";
	document.getElementById("form_add01_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add02_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add03_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add04_detailSendingCustomer").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01SendingCustomer").style.display = "";
			document.getElementById("form_add01_detailSendingCustomer").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02SendingCustomer").style.display = "";
			document.getElementById("form_add02_detailSendingCustomer").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03SendingCustomer").style.display = "";
			document.getElementById("form_add03_detailSendingCustomer").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04SendingCustomer").style.display = "";
			document.getElementById("form_add04_detailSendingCustomer").style.display = "";
			break;
	}//switch
}


   function sameplaceSendingCustomer(val){
   		if(val ==1){
			document.frm01.hSendingCustomerAddress01.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon01.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur01.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince01.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip01.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel01.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax01.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==2){
			document.frm01.hSendingCustomerAddress02.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon02.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur02.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince02.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip02.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel02.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax02.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==3){
			document.frm01.hSendingCustomerAddress03.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon03.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur03.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince03.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip03.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel03.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax03.value = document.frm01.hSendingCustomerFax.value;		
		}
   
   }   
   
</script>

<br>
<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="ccardUpdate.php" method="POST"  onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objSendingCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objSendingCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objSendingCustomer->getCCard();?>">
	  <input type="hidden" name="hCustomerId"  value="<?=$objCustomer->getCustomerId();?>">
	  <input type="hidden" name="hSendingNumber" value="<?=$objOrder->getSendingNumber()?>">
	  <input type="hidden" name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>">	  	
	  <input type="hidden" name="hOrderStatus" value="1">
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingSaleId" value="<?=$objOrder->getBookingSaleId()?>">
	  <input type="hidden" name="hBookingCarColor" value="<?=$objOrder->getBookingCarColor()?>">
	  <input type="hidden" name="hBookingCarSeries" value="<?=$objOrder->getBookingCarSeries()?>">
	  <input type="hidden" name="hKeyword" value="<?=$hKeyword?>">
	  <input type="hidden" name="hSearch" value="<?=$hSearch?>">
	  <input type="hidden" name="hPage" value="<?=$hPage?>">

<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><input type="text" readonly  name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ������ͺ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly  value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly onblur="checkYear(this,this.value);"  value="<?=$Year?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >
				<input type="hidden" readonly size="2" name="hSendingSaleId"  value="<?=$objOrder->getSendingSaleId();?>">
				<input type="text" readonly name="hSendingSaleName" size="30"   onKeyDown="if(event.keyCode==13 && frm01.hSendingSaleId.value != '' ) frm01.hOrderPrice.focus();if(event.keyCode !=13 ) frm01.hSendingSaleId.value='';"  value="<?=$hSendingSaleName?>">
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">
				<input type="checkbox" value="1" disabled=true name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>> ���ꡡó�����¹��ѡ�ҹ���
			</td>			
		</tr>
	
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>�ҤҢ��</strong></td>
			<td width="30%" valign="top" ><input type="text" readonly name="hOrderPrice" size="30"  value="<?=$objOrder->getOrderPrice()?>"></td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top"></td>	
		</tr>			
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>������١���</strong> </td>
			<td width="30%">
				<?
				$objGroup  = new CustomerGroup();
				$objGroup->setGroupId($objBookingCustomer->getGroupId());
				$objGroup->load();
				
				echo $objGroup->getTitle();
				unset($objGroup);
				?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
				<?=$objBookingCustomer->getInformationDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���ʧҹ</strong> </td>
			<td valign="top">
				<?
				$objEvent  = new CustomerEvent();
				$objEvent->setEventId($objBookingCustomer->getEventId());
				$objEvent->load();
				
				echo $objEvent->getTitle();
				unset($objEvent);
				?>
			</td>
			<td class="i_background03"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td ><?=$hBookingSaleName?></td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><?=$objBookingCustomer->getIDCard();?></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?
				$objGrade  = new CustomerGrade();
				$objGrade->setGradeId($objBookingCustomer->getGradeId());
				$objGrade->load();
				
				echo $objGrade->getTitle();
				unset($objGrade);
				?>			
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table>
				<tr>
					<td valign="top">
						<?
						if($objBookingCustomer->getCustomerTitleId() > 0 ){
							echo $objBookingCustomer->getCustomerTitleDetail();
						}else{
							if($objBookingCustomer->getTitle() == ""){
									echo "�س";
							}else{ 
									echo $objBookingCustomer->getTitle(); 
							}
						}
						?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<?=$name?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getBirthDay()?></td>
		</tr>
		<tr>
			<td colspan="4" align="right">				
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>	

		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0" class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><?=$objBookingCustomer->getAmphur();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><?=$objBookingCustomer->getHomeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><?=$objBookingCustomer->getMobile();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><?=$objBookingCustomer->getOfficeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><?=$objBookingCustomer->getEmail();?>&nbsp;</td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress01();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince01();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip01();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel01();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax01();?>&nbsp;</td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress02();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon02();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur02();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince02();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip02();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel02();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax02();?>&nbsp;</td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince03();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip03();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel03();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax03();?>&nbsp;</td>
		</tr>		
		</table>
		
				
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCustomer" onclick="setCustomerValue()" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹����Ѻ�ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>���觷���Ңͧ�١���</strong> </td>
			<td width="30%">
				<?$objCustomerGroupList->printSelect("hSendingCustomerGroupId",$objSendingCustomer->getGroupId(),"��س����͡");?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly   value="<?=$DaySending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2" readonly   value="<?=$MonthSending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly onblur="checkYear(this,this.value);"  value="<?=$YearSending?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong></td>
			<td valign="top">
				<input type="hidden" size=3 name="hSendingEventId"  value="<?=$objSendingCustomer->getEventId()?>">
				<INPUT readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingEventId.value != '' ) frm01.hSendingCustomerSaleName.focus();if(event.keyCode !=13 ) frm01.hSendingEventId.value='';"   name="hEvent"  size=40 value="<?=$objSendingEvent->getTitle()?>">
				
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td >
				<input type="text" readonly size="3" name="hSendingCustomerSaleId"  value="<?=$objSendingCustomer->getSaleId();?>">
				<input readonly type="text" name="hSendingCustomerSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerSaleName.value != '' ) frm01.hSendingCustomerIDCard.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerSaleId.value='';"    value="<?=$hSendingSaleName?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><input readonly type="text" name="hSendingCustomerIDCard" size="30" maxlength="13"  value="<?=$objSendingCustomer->getIDCard();?>"></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGradeList->printSelect("hSendingCustomerGradeId",$objSendingCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hSendingCustomerTitle",$objSendingCustomer->getTitle(),"�س");?>
					</td>
					<td valign="top">
					<?if($objSendingCustomer->getFirstname() != ""){
						$name = $objSendingCustomer->getFirstname()."  ".$objSendingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<INPUT readonly  name="hSendingCustomerName"  size="20"  size=30 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hSendingCustomerTitleId",$objSendingCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  readonly   value="<?=$Day01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2" readonly   value="<?=$Month01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" readonly  onblur="checkYear(this,this.value);"  value="<?=$Year01Sending?>"></td>
					<td>&nbsp;</td>				
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="right">
				
				<table id="form_add01SendingCustomer" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>					
					
					
		<table id=form_add01_detailSendingCustomer width="100%" cellpadding="2" cellspacing="0"  class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3"  name="hSendingCustomerTumbonCode"  value="<?=$objSendingCustomer->getTumbonCode();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode.value != '' ) frm01.hSendingCustomerAmphur.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode.value='';" name="hSendingCustomerTumbon" size="50"  value="<?=$objSendingCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3"  name="hSendingCustomerAmphurCode"  value="<?=$objSendingCustomer->getAmphurCode();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode.value != '' ) frm01.hSendingCustomerProvince.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode.value='';" name="hSendingCustomerAmphur" size="30"  value="<?=$objSendingCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode"  value="<?=$objSendingCustomer->getProvinceCode();?>"><input type="text" readonly  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode.value != '' ) frm01.hSendingCustomerZip.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode.value='';" name="hSendingCustomerProvince" size="30"  value="<?=$objSendingCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode"  value="<?=$objSendingCustomer->getZip();?>"><input type="text" readonly name="hSendingCustomerZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode.value != '' ) frm01.hSendingCustomerHomeTel.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode.value='';" size="30"  value="<?=$objSendingCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerHomeTel" size="30"  value="<?=$objSendingCustomer->getHomeTel();?>"></td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerMobile" size="30"  value="<?=$objSendingCustomer->getMobile();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" readonly name="hSendingCustomerOfficeTel" size="30"  value="<?=$objSendingCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax" size="30"  value="<?=$objSendingCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" readonly name="hSendingCustomerEmail" size="30"  value="<?=$objSendingCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hSendingCustomerIncomplete" value="1" <?if($objSendingCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hSendingCustomerMailback" value="1" <?if($objSendingCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>		
		</table>
		<table  id=form_add02_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress01" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode01"  value="<?=$objSendingCustomer->getTumbonCode01();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode01.value != '' ) frm01.hSendingCustomerAmphur01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode01.value='';" name="hSendingCustomerTumbon01" size="50"  value="<?=$objSendingCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode01"  value="<?=$objSendingCustomer->getAmphurCode01();?>"><input type="text" readonly  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode01.value != '' ) frm01.hSendingCustomerProvince01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode01.value='';" name="hSendingCustomerAmphur01" size="30"  value="<?=$objSendingCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode01"  value="<?=$objSendingCustomer->getProvinceCode01();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode01.value != '' ) frm01.hSendingCustomerZip01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode01.value='';" name="hSendingCustomerProvince01" size="30"  value="<?=$objSendingCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode01"  value="<?=$objSendingCustomer->getZip01();?>"><input type="text"  readonly name="hSendingCustomerZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode01.value != '' ) frm01.hSendingCustomerTel01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode01.value='';" size="30"  value="<?=$objSendingCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel01" size="30"  value="<?=$objSendingCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax01" size="30"  value="<?=$objSendingCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><input type="text" readonly maxlength="50"  name="hSendingCustomerAddress02" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress02();?>"></td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode02"  value="<?=$objSendingCustomer->getTumbonCode02();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode02.value != '' ) frm01.hSendingCustomerAmphur02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode02.value='';" name="hSendingCustomerTumbon02" size="50"  value="<?=$objSendingCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode02"  value="<?=$objSendingCustomer->getAmphurCode02();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode02.value != '' ) frm01.hSendingCustomerProvince02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode02.value='';" name="hSendingCustomerAmphur02" size="30"  value="<?=$objSendingCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode02"  value="<?=$objSendingCustomer->getProvinceCode02();?>"><input type="text"  readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode02.value != '' ) frm01.hSendingCustomerZip02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode02.value='';" name="hSendingCustomerProvince02" size="30"  value="<?=$objSendingCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode02"  value="<?=$objSendingCustomer->getZip02();?>"><input type="text" name="hSendingCustomerZip02" readonly  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode02.value != '' ) frm01.hSendingCustomerTel02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode02.value='';" size="30"  value="<?=$objSendingCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel02" size="30"  value="<?=$objSendingCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax02" size="30"  value="<?=$objSendingCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><input type="text" readonly  maxlength="50"  name="hSendingCustomerAddress03" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress03();?>"></td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode03"  value="<?=$objSendingCustomer->getTumbonCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode03.value != '' ) frm01.hSendingCustomerAmphur03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode03.value='';" name="hSendingCustomerTumbon03" size="50"  value="<?=$objSendingCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode03"  value="<?=$objSendingCustomer->getAmphurCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode03.value != '' ) frm01.hSendingCustomerProvince03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode03.value='';" name="hSendingCustomerAmphur03" size="30"  value="<?=$objSendingCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode03"  value="<?=$objSendingCustomer->getProvinceCode03();?>"><input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode03.value != '' ) frm01.hSendingCustomerZip03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode03.value='';" name="hSendingCustomerProvince03" size="30"  value="<?=$objSendingCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode03"  value="<?=$objSendingCustomer->getZip03();?>"><input type="text"  readonly name="hSendingCustomerZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode03.value != '' ) frm01.hSendingCustomerTel03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode03.value='';" size="30"  value="<?=$objSendingCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" readonly name="hSendingCustomerTel03" size="30"  value="<?=$objSendingCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" readonly name="hSendingCustomerFax03" size="30"  value="<?=$objSendingCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>					
					
					
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="20%"><strong>���ö</strong> </td>
			<td valign="top" width="30%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="20%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="30%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="20%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>			
			<td width="30%" ><input type="text" readonly name="hCarNumber" size="30"  value="<?=$objStockCar->getCarNumber()?>"></td>
			<td width="20%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><label id="hEngineNumber"><?=$objStockCar->getEngineNumber()?></label></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<label id="hCarModel01"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<label id="hCarSeries01"><?=$objCarSeries->getTitle()?></label>								
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hGear">
					<option value="Auto" <?if($objStockCar->getGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objStockCar->getGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<select  name="hCarType">
					<option value="1" <?if($objCarSeries->getCarType() == "1") echo "selected"?>>ö��
					<option value="2" <?if($objCarSeries->getCarType() == "2") echo "selected"?>>ö�к�
					<option value="3" <?if($objCarSeries->getCarType() == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($objCarSeries->getCarType() == "4") echo "selected"?>>ö���
				</select>			
			</td>		
			<td class="i_background03" width="20%"></td>
			<td width="30%">
				
			</td>
		</tr>			
		<tr>			
			<td class="i_background03"><strong>�ѹ����˹����ͺ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT readonly align="middle" size="2" maxlength="2"   value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="2" maxlength="2" 3 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT readonly align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"   value="<?=$Year03?>"></td>
					<td>&nbsp;</td>					
				</tr>
				</table>
			</td>		
		
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td ><input type="text" readonly name="hSendingCarRemark" size="50"  value="<?=$objOrder->getSendingCarRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>


<br>

<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�ë���</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��������ë���</strong></td>
			<td width="30%" valign="top" ><input type="radio" disabled  name="hBuyType" value=1 <?if($objOrder->getBuyType() == 1 ) echo "checked"?>>����ʴ&nbsp;&nbsp;&nbsp;<input type="radio" disabled name="hBuyType" value=2 <?if($objOrder->getBuyType() == 2 ) echo "checked"?>>�ṹ��</td>
			<td class="i_background03" width="20%" valign="top"><strong>����ѷ�ṹ��</strong> </td>
			<td width="30%" valign="top">
				<?$objFundCompanyList->printSelect("hBuyCompany",$objOrder->getBuyCompany(),"��س����͡");?>
			</td>			
		</tr>			
		<tr>
			<td class="i_background03"><strong>�͡����</strong> </td>
			<td ><input type="text" readonly name="hBuyFee" size="10"  value="<?if($objOrder->getBuyFee() > 0) echo $objOrder->getBuyFee();?>"> %</td>
			<td class="i_background03"><strong>��������</strong>   </td>
			<td ><input type="text" readonly  name="hBuyTime" size="10"  value="<?if($objOrder->getBuyTime() > 0) echo $objOrder->getBuyTime();?>"> ��͹</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>��ҧǴ</strong> </td>
			<td ><input type="text" readonly  name="hBuyPrice" size="20"  value="<?if($objOrder->getBuyPrice() > 0) echo $objOrder->getBuyPrice();?>"> �ҷ</td>
			<td class="i_background03"><strong>�Ҥ�ö</strong>   </td>
			<td ><input type="text" readonly  name="hBuyTotal" size="20"  value="<?if($objOrder->getBuyTotal() > 0) echo $objOrder->getBuyTotal();?>"> �ҷ</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>��ǹ�</strong></td>
			<td ><input type="text" readonly  name="hBuyDown" size="20"  value="<?if($objOrder->getBuyDown() > 0) echo $objOrder->getBuyDown();?>"> �ҷ</td>
			<td   class="i_background03"><strong>�ʹ�Ѵ</strong></td>
			<td ><input type="text" readonly  name="hBuyTotal" size="20"  value="<?if($objOrder->getBuyTotal() > 0) echo $objOrder->getBuyTotal();?>"> �ҷ</td>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3"><input type="text"  readonly  name="hBuyRemark" size="80"  value="<?=$objOrder->getBuyRemark()?>"></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>
<?
$date = $objOrder->getBlackCodeDate();
$arrDate = explode("-",$date);
$Day = $arrDate[2];
$Month = $arrDate[1];
$Year = $arrDate[0];

$date01 = $objOrder->getRegistrySendDate();
$arrDate01 = explode("-",$date01);
$Day01 = $arrDate01[2];
$Month01 = $arrDate01[1];
$Year01 = $arrDate01[0];

$date02 = $objOrder->getRegistryTakeCodeDate();
$arrDate02 = explode("-",$date02);
$Day02 = $arrDate02[2];
$Month02 = $arrDate02[1];
$Year02 = $arrDate02[0];

$date03 = $objOrder->getRegistryTakeBookDate();
$arrDate03 = explode("-",$date03);
$Day03 = $arrDate03[2];
$Month03 = $arrDate03[1];
$Year03 = $arrDate03[0];
?>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">�����Ũ�����¹</td>
			<td width="65%"></td>
			<td align="right" >

			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">			
		<tr>			
			<td class="i_background03" width="20%" valign="top"><strong>����¹����ᴧ</strong> </td>
			<td width="30%" valign="top">
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>							
							<input type="text" readonly onKeyDown="if(event.keyCode==13 && frm01.hStockRedId.value != '' ) frm01.hRedCodePrice.focus();if(event.keyCode !=13 ) frm01.hStockRedId.value='';"  name="hStockRedName" size="10" maxlength="10"  value="<?=$objStockRed->getStockName()?>">&nbsp;&nbsp;&nbsp;
					</td>
					<td valign="top"><input type="checkbox"  disabled  onclick="checkRedCode();" name="hNoRedCode" value=1 <?if($objOrder->getNoRedCode() == 1) echo "checked";?>> �礡ó��١�������ͧ��÷���¹ö</td>
				</tr>
				</table>

				
			</td>		
			<td class="i_background03" width="20%" ><strong>�Ѵ�ӻ���ᴧ</strong></td>
			<td width="30%" ><input type="text"  name="hRedCodePrice" size="15"  value="<?=$objOrder->getRedCodePrice()?>"> �ҷ</td>			
		</tr>
		<tr>			
			<td class="i_background03"><strong>��Ҩ�����¹</strong></td>
			<td ><input type="text" name="hRegistryPrice" size="15"  value="<?=$objOrder->getRegistryPrice()?>"> �ҷ</td>		
			<td class="i_background03"><strong>�ѹ��� TBR</strong></td>
			<td >
				<?=$objStockCar->getTBR()?>
			</td>
		</tr>


		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" >
				<textarea rows="5" cols="100" name="hRegistryRemark"><?=$objOrder->getRegistryRemark();?></textarea>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<table width="100%">
<tr>
	<td width="10%" class="listTitle">�Ѻ���·���¹</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryStatus" value=1 <?if($objOrder->getRegistryStatus() ==1) echo "checked"?> >�͡������ú</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryStatus" value=2 <?if($objOrder->getRegistryStatus() ==2) echo "checked"?> >��ͤ�Ţ</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryStatus" value=3 <?if($objOrder->getRegistryStatus() ==3) echo "checked"?> >�觨�����¹</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryStatus" value=4 <?if($objOrder->getRegistryStatus() ==4) echo "checked"?> >�Ѻ���·���¹�ҡ����</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryStatus" value=5 <?if($objOrder->getRegistryStatus() ==5) echo "checked"?> >�١����Ѻ����</td>
</tr>
<tr>
	<td class="listDetail" >�ѹ���</td>
	<td class="listDetail" >
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA1 value="<?=$DayA1?>"></td>
			<td>-</td>
			<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA1 value="<?=$MonthA1?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA1 value="<?=$YearA1?>"></td>
			<td>&nbsp;</td>		
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA1,DayA1, MonthA1, YearA1,popCal);return false"></td>			
		</tr>
		</table>
	</td>
	<td class="listDetail" >
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA2 value="<?=$DayA2?>"></td>
			<td>-</td>
			<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA2 value="<?=$MonthA2?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA2 value="<?=$YearA2?>"></td>
			<td>&nbsp;</td>		
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA2,DayA2, MonthA2, YearA2,popCal);return false"></td>			
		</tr>
		</table>
	</td>
	<td class="listDetail" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA10 value="<?=$DayA10?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA10 value="<?=$MonthA10?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA10 value="<?=$YearA10?>"></td>
					<td>&nbsp;</td>		
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA10,DayA10, MonthA10, YearA10,popCal);return false"></td>			
				</tr>
				</table>
	</td>
	<td class="listDetail" >

				
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DayA8 value="<?=$DayA8?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA8 value="<?=$MonthA8?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA8 value="<?=$YearA8?>"></td>
					<td>&nbsp;</td>		
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA8,DayA8, MonthA8, YearA8,popCal);return false"></td>			
				</tr>
				</table>
	</td>
	<td class="listDetail" >
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA5 value="<?=$DayA5?>"></td>
			<td>-</td>
			<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA5 value="<?=$MonthA5?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA5 value="<?=$YearA5?>"></td>
			<td>&nbsp;</td>		
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA5,DayA5, MonthA5, YearA5,popCal);return false"></td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td valign="top" class="listDetail">��������´</td>
	<td valign="top" class="listDetail" rowspan="4">
		�͡��÷��Ҵ
		<table>
			<?
			$i=0;
			forEach($objRegistryConditionList->getItemList() as $objItem) {
				$objRegistryDocument = new RegistryDocument();
				if($objRegistryDocument->loadByCondition(" order_id = $hOrderId AND registry_condition_id = ".$objItem->getRegistryConditionId()) ){
					$found = 1;
				}else{
					$found = 2;
				}
			?>
			<input type="hidden" name="hRegistryDocumentId[<?=$i?>]" value="<?=$objRegistryDocument->getRegistryDocumentId()?>">
		<tr>
			<td align="center"><input type="checkbox" name="hRegistryCondition[<?=$i?>]"   <?if($found ==1) echo "checked"?> value="<?=$objItem->getRegistryConditionId()?>"></td>
			<td ><?=$objItem->getTitle()?></td>			
		</tr>
			<?
			++$i;
			}
			Unset($objRegistryConditionList);
			?>
		<tr>
			<td colspan="2">�����˵�</td>
		</tr>
		<tr>
			<td colspan="2"><textarea cols="20" rows="5" name="hRegistryRemark01" ><?=$objOrder->getRegistryRemark01()?></textarea></td>
		</tr>
		</table>	
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td>�����˵�</td>
		</tr>
		<tr>
			<td><textarea cols="20" rows="5" name="hRegistryRemark02" ><?=$objOrder->getRegistryRemark02()?></textarea></td>
		</tr>
		</table>		
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td>����觨�</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryText03" size="20" value="<?=$objOrder->getRegistryText03()?>"></td>
		</tr>
		<tr>
			<td>�����˵�</td>
		</tr>
		<tr>
			<td><textarea cols="20" rows="5" name="hRegistryRemark03" ><?=$objOrder->getRegistryRemark03()?></textarea></td>
		</tr>
		</table>		
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td colspan=2>����¹���´�</td>
		</tr>
		<tr>
			<td colspan=2><input type="text" name="hBlackCodeNumber" size="10"  value="<?=$objOrder->getBlackCodeNumber()?>"></td>
		</tr>
		<tr>
			<td colspan=2>�ѹ��訴����¹�ҡ����</td>
		</tr>
		<tr>
			<td colspan=2>
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>		
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>			
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >������ջշ��</td>
			<td >������ջ�Шӻ�</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryTaxYearNumber" size="5"  value="<?=$objOrder->getRegistryTaxYearNumber()?>"></td>
			<td><input type="text" name="hRegistryTaxYear" size="10"  value="<?=$objOrder->getRegistryTaxYear()?>"> �ҷ</td>
		</tr>
		<tr>
			<td colspan=2>��ѡ�ҹ����Ѻ����</td>
		</tr>
		<tr>
			<td colspan=2><input type="text" name="hRegistryText04" size="20" value="<?=$objOrder->getRegistryText04()?>"></td>
		</tr>
		<tr>
			<td colspan=2>�����˵�</td>
		</tr>
		<tr>
			<td colspan=2><textarea cols="20" rows="5" name="hRegistryRemark04" ><?=$objOrder->getRegistryRemark04()?></textarea></td>
		</tr>
		</table>		
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td>�����١����Ѻ����</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryRecieveName01" size="20" value="<?=$objOrder->getRegistryRecieveName01()?>"></td>
		</tr>
		<tr>
			<td>��ѡ�ҹ������ͺ����</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryText05" size="20" value="<?=$objOrder->getRegistryText05()?>"></td>
		</tr>
		<tr>
			<td>������������ͺ����</td>
		</tr>
		<tr>
			<td><input type="radio" name="hRegistryRecieveType01" value="direct" <?if($objOrder->getRegistryRecieveType01() =="direct") echo "checked"?>>�١����Ѻ�ͧ&nbsp;&nbsp;<input type="radio" name="hRegistryRecieveType01" value="mail" <?if($objOrder->getRegistryRecieveType01() =="mail") echo "checked"?>>�����</td>
		</tr>
		<tr>
			<td>�����˵�</td>
		</tr>
		<tr>
			<td><textarea cols="20" rows="5" name="hRegistryRemark05" ><?=$objOrder->getRegistryRemark05()?></textarea></td>
		</tr>
		</table>		
	</td>
</tr>
<tr>
	<td width="10%" class="listTitle">�Ѻ��������¹</td>
	
	<td width="18%" class="listTitle"></td>
	<td width="18%" class="listTitle"></td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryTakeBook" value=1 <?if($objOrder->getRegistryTakeBook() ==1) echo "checked"?>>�Ѻ��������¹�ҡ����</td>
	<td width="18%" class="listTitle"><input type="radio" name="hRegistryTakeBook" value=2 <?if($objOrder->getRegistryTakeBook() ==2) echo "checked"?>>�١����Ѻ��������¹</td>
</tr>
<tr>
	<td class="listDetail" >�ѹ���</td>

	<td class="listDetail" >

	</td>
	<td class="listDetail" >

	</td>
	<td class="listDetail" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA9 value="<?=$DayA9?>"></td>
					<td>-</td>
					<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA9 value="<?=$MonthA9?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA9 value="<?=$YearA9?>"></td>
					<td>&nbsp;</td>		
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA9,DayA9, MonthA9, YearA9,popCal);return false"></td>			
				</tr>
				</table>
	</td>
	<td class="listDetail" >
	  	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><INPUT  align="middle" size="2" maxlength="2"   name=DayA7 value="<?=$DayA7?>"></td>
			<td>-</td>
			<td><INPUT  align="middle" size="2" maxlength="2"  name=MonthA7 value="<?=$MonthA7?>"></td>
			<td>-</td>
			<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearA7 value="<?=$YearA7?>"></td>
			<td>&nbsp;</td>		
			<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearA7,DayA7, MonthA7, YearA7,popCal);return false"></td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td valign="top" class="listDetail">��������´</td>

	<td class="listDetail" valign="top">
	
	</td>
	<td class="listDetail" valign="top">
	
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td colspan=2>��ѡ�ҹ����Ѻ����</td>
		</tr>
		<tr>
			<td colspan=2><input type="text" name="hRegistryText06" size="20" value="<?=$objOrder->getRegistryText06()?>"></td>
		</tr>
		<tr>
			<td colspan=2>�����˵�</td>
		</tr>
		<tr>
			<td colspan=2><textarea cols="20" rows="5" name="hRegistryRemark06" ><?=$objOrder->getRegistryRemark06()?></textarea></td>
		</tr>
		</table>		
	</td>
	<td class="listDetail" valign="top">
		<table width="100%">
		<tr>
			<td>�����١����Ѻ����</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryRecieveName01" size="20" value="<?=$objOrder->getRegistryRecieveName01()?>"></td>
		</tr>
		<tr>
			<td>��ѡ�ҹ������ͺ����</td>
		</tr>
		<tr>
			<td><input type="text" name="hRegistryText07" size="20" value="<?=$objOrder->getRegistryText07()?>"></td>
		</tr>
		<tr>
			<td>������������ͺ����</td>
		</tr>
		<tr>
			<td><input type="radio" name="hRegistryRecieveType02" value="direct" <?if($objOrder->getRegistryRecieveType02() =="direct") echo "checked"?>>�١����Ѻ�ͧ&nbsp;&nbsp;<input type="radio" name="hRegistryRecieveType02" value="mail" <?if($objOrder->getRegistryRecieveType02() =="mail") echo "checked"?>>�����</td>
		</tr>
		<tr>
			<td>�����˵�</td>
		</tr>
		<tr>
			<td><textarea cols="20" rows="5" name="hRegistryRemark07" ><?=$objOrder->getRegistryRemark07()?></textarea></td>
		</tr>
		</table>		
	</td>
</tr>
</table>



<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" value="��Ѻ�" onclick="window.location='stockBlackList.php?hKeyword=<?=$hKeyword?>&hSearch=<?=$hSearch?>&hPage=<?=$hPage?>'">			
	<br><br>
	</td>
</tr>
</table>
<?if ($strMode == "Update"){
$objMemberTemp = new Member();
$objMemberTemp->setMemberId($objOrder->getEditBy());
$objMemberTemp->load();
?>		
<table width="100%" >
<tr>
	<td align="right">��䢢���������ش�� :   <?=$objMemberTemp->getNickname()?>     �ѹ���    <?=$objOrder->getEditDate()?> </td>
</tr>
</table>
<?
unset($objMemberTemp);
}?>		
</form>


<script>
	
	function showBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "";	
		document.getElementById("showBookingCustomer").style.display = "none";		
		document.getElementById("hideBookingCustomer").style.display = "";		
	}

	function hideBookingCustomer(){
		document.getElementById("bookingCustomer").style.display = "none";	
		document.getElementById("showBookingCustomer").style.display = "";			
		document.getElementById("hideBookingCustomer").style.display = "none";
	}

	function showBookingOrder(){
		document.getElementById("bookingOrder").style.display = "";	
		document.getElementById("showBookingOrder").style.display = "none";		
		document.getElementById("hideBookingOrder").style.display = "";		
	}

	function hideBookingOrder(){
		document.getElementById("bookingOrder").style.display = "none";	
		document.getElementById("showBookingOrder").style.display = "";			
		document.getElementById("hideBookingOrder").style.display = "none";
	}
	
	function showBookingCar(){
		document.getElementById("bookingCar").style.display = "";	
		document.getElementById("showBookingCar").style.display = "none";		
		document.getElementById("hideBookingCar").style.display = "";		
	}

	function hideBookingCar(){
		document.getElementById("bookingCar").style.display = "none";	
		document.getElementById("showBookingCar").style.display = "";			
		document.getElementById("hideBookingCar").style.display = "none";
	}

</script>


<?
	include("h_footer.php")
?>
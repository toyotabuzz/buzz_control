<?
include("common.php");

$objOrderBooking = new Order();
$objBookingCustomer = new Customer();
$objBookingCarColor = new CarColor();
$objBookingCarSeries = new CarSeries();
$objSendingCustomer = new Customer();
$objCustomer = new Customer();
$objStockRed = new StockRed();
$objStockCar = new StockCar();
$objCarSeries = new CarSeries();
$objSendingEvent = new CustomerEvent();

$objEvent = new CustomerEvent();

$objCustomerTitleList = new CustomerTitleList();
$objCustomerTitleList->setPageSize(0);
$objCustomerTitleList->setSortDefault(" title ASC ");
$objCustomerTitleList->load();

$objCustomer = new Customer();

$objCustomerGradeList = new CustomerGradeList();
$objCustomerGradeList->setPageSize(0);
$objCustomerGradeList->setSortDefault("title ASC");
$objCustomerGradeList->load();

$objCustomerGroupList = new CustomerGroupList();
$objCustomerGroupList->setPageSize(0);
$objCustomerGroupList->setSortDefault("title ASC");
$objCustomerGroupList->load();

$objCustomerEventList = new CustomerEventList();
$objCustomerEventList->setPageSize(0);
$objCustomerEventList->setSortDefault("title ASC");
$objCustomerEventList->load();

$objCarTypeList = new CarTypeList();
$objCarTypeList->setPageSize(0);
$objCarTypeList->setSortDefault("title ASC");
$objCarTypeList->load();

$objCarModelList = new CarModelList();
$objCarModelList->setPageSize(0);
$objCarModelList->setSortDefault("title ASC");
$objCarModelList->load();

$objCarColorList = new CarColorList();
$objCarColorList->setPageSize(0);
$objCarColorList->setSortDefault("title ASC");
$objCarColorList->load();

$objCarRoleList = new CarModelList();
$objCarRoleList->setPageSize(0);
$objCarRoleList->setSortDefault(" title ASC");
$objCarRoleList->load();

$objCarModel1List = new CarSeriesList();
$objCarModel1List->setPageSize(0);
$objCarModel1List->setSortDefault(" model ASC");
$objCarModel1List->load();

$objCarSeriesList = new CarSeriesList();
$objCarSeriesList->setPageSize(0);
$objCarSeriesList->setSortDefault(" title ASC");
$objCarSeriesList->load();

$objStockPremiumList = new StockPremiumList();
$objStockPremiumList->setPageSize(0);
$objStockPremiumList->setSortDefault(" title ASC");
$objStockPremiumList->setSort($hSort);
$objStockPremiumList->load();

$objFundCompanyList = new FundCompanyList();
$objFundCompanyList->setPageSize(0);
$objFundCompanyList->setSortDefault("title ASC");
$objFundCompanyList->load();

$objInsureCompanyList = new InsureCompanyList();
$objInsureCompanyList->setPageSize(0);
$objInsureCompanyList->setSortDefault("title ASC");
$objInsureCompanyList->load();

$objPrbCompanyList = new PrbCompanyList();
$objPrbCompanyList->setPageSize(0);
$objPrbCompanyList->setSortDefault("title ASC");
$objPrbCompanyList->load();

$objOrder = new Order();

if (empty($hSubmit)) {
	if ($hId!="") {
		$hOrderId = $hId;
	
		$objOrder->setOrderId($hId);
		$objOrder->load();
		$strMode="Update";
		
		$objOrderBooking->setOrderId($hId);
		$objOrderBooking->load();		
		
		$arrDate = explode("-",$objOrder->getSendingDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getBookingDate());
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getRecieveDate());
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getBlackCodeDate());
		$Day04 = $arrDate[2];
		$Month04 = $arrDate[1];
		$Year04 = $arrDate[0];		
		
		$arrDate = explode("-",$objOrder->getDiscountMarginDate());
		$Day05 = $arrDate[2];
		$Month05 = $arrDate[1];
		$Year05 = $arrDate[0];
		
		$arrDate = explode("-",$objOrder->getInsureExpire());
		$Day06 = $arrDate[2];
		$Month06 = $arrDate[1];
		$Year06 = $arrDate[0];
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		if(isset($hSearchSendingCustomerId)){
			$hCustomerId = $hSearchSendingCustomerId;		
			$objSendingCustomer->setCustomerId($hSearchSendingCustomerId);
			$objSendingCustomer->load();
			
			$objOrder->setSendingCustomerId($hSearchSendingCustomerId);
			
			$strMode="Update";		
		}else{
			$hCustomerId = $objOrder->getSendingCustomerId();		
			$objSendingCustomer->setCustomerId($objOrder->getSendingCustomerId());
			$objSendingCustomer->load();
			$strMode="Update";		
		}
		$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
		$objBookingCustomer->load();		
				
		
		$arrDate = explode("-",$objBookingCustomer->getInformationDate());
		$BookingDay = $arrDate[2];
		$BookingMonth = $arrDate[1];
		$BookingYear = $arrDate[0];
		
		$arrDate = explode("-",$objBookingCustomer->getBirthDay());
		$BookingDay01 = $arrDate[2];
		$BookingMonth01 = $arrDate[1];
		$BookingYear01 = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getInformationDate());
		$DaySending = $arrDate[2];
		$MonthSending = $arrDate[1];
		$YearSending = $arrDate[0];
		
		$arrDate = explode("-",$objSendingCustomer->getBirthDay());
		$Day01Sending = $arrDate[2];
		$Month01Sending = $arrDate[1];
		$Year01Sending = $arrDate[0];		
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getBookingSaleId());
		$objSale->load();
		
		$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();				
		
		$objSale = new Member();
		$objSale->setMemberId($objOrder->getSendingSaleId());
		$objSale->load();
		
		$hSendingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();		
		// load stock car and red code
		
		$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
		$objBookingCarColor->load();
		
		$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
		$objBookingCarSeries->load();		
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objOrder->getStockCarId());
		$objStockCar->load();
		
		$objCarSeries->setCarSeriesId($objStockCar->getCarSeriesId());
		$objCarSeries->load();
		
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($objOrder->getStockRedId());
		$objStockRed->load();
		
		$objSendingEvent->setEventId($objSendingCustomer->getEventId());
		$objSendingEvent->load();
		
	} else {
	
		if($hCustomerId > 0){
		$objCustomer->setCustomerId($hCustomerId);
		$objCustomer->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objCustomer->getInformationDate());
		$Day = $arrDate[2];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$arrDate = explode("-",$objCustomer->getBirthDay());
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		}
	
		$strMode="Add";
	}

} else {
	if (!empty($hSubmit)) {

            $objCustomer->setCustomerId($hSendingCustomerId);
			$objCustomer->setACard($hACard);
			$objCustomer->setBCard($hBCard);
			$objCustomer->setCCard(1);
            $objCustomer->setTypeId("C");
            $objCustomer->setGradeId($hSendingCustomerGradeId);
            $objCustomer->setGroupId($hSendingCustomerGroupId);
			$objCustomer->setEventId($hSendingEventId);
			$hPostedDate = $YearSending."-".$MonthSending."-".$DaySending;
            $objCustomer->setInformationDate($hPostedDate);
            $objCustomer->setSaleId($hSendingCustomerSaleId);
			$hBirthday = $Year01Sending."-".$Month01Sending."-".$Day01Sending;
            $objCustomer->setBirthday($hBirthday);
            $objCustomer->setTitle($hSendingCustomerTitle);
			if($hTitle == "���" OR $hTitle == "˨�"){
				$objCustomer->setFirstname(trim($hSendingCustomerName));
			}else{
				$arrName = explode(" ",$hSendingCustomerName);
				$objCustomer->setFirstname(trim($arrName[0]));
				
				if(sizeof($arrName) > 2){
					for($i=1;$i<sizeof($arrName);$i++){
						$strName= $strName." ".$arrName[$i];
					}
				}

				$objCustomer->setLastname( trim($strName) );
			}
			$objCustomer->setEmail($hSendingCustomerEmail);
			$objCustomer->setIDCard($hSendingCustomerIDCard);
			$objCustomer->setAddress($hSendingCustomerAddress);
			$objCustomer->setTumbon($hSendingCustomerTumbon);
			$objCustomer->setAmphur($hSendingCustomerAmphur);
			$objCustomer->setProvince($hSendingCustomerProvince);			
			$objCustomer->setTumbonCode($hSendingCustomerTumbonCode);
			$objCustomer->setAmphurCode($hSendingCustomerAmphurCode);
			$objCustomer->setProvinceCode($hSendingCustomerProvinceCode);
			$objCustomer->setZipCode($hSendingCustomerZipCode);
			$objCustomer->setZip($hSendingCustomerZip);
			$objCustomer->setHomeTel($hSendingCustomerHomeTel);
			$objCustomer->setMobile($hSendingCustomerMobile);
			$objCustomer->setOfficeTel($hSendingCustomerOfficeTel);
			$objCustomer->setFax($hSendingCustomerFax);			
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setAddress01($hSendingCustomerAddress01);
			$objCustomer->setTumbon01($hSendingCustomerTumbon01);
			$objCustomer->setAmphur01($hSendingCustomerAmphur01);
			$objCustomer->setProvince01($hSendingCustomerProvince01);
			$objCustomer->setZip01($hSendingCustomerZip01);
			$objCustomer->setTumbonCode01($hSendingCustomerTumbonCode01);
			$objCustomer->setAmphurCode01($hSendingCustomerAmphurCode01);
			$objCustomer->setProvinceCode01($hSendingCustomerProvinceCode01);
			$objCustomer->setZipCode01($hSendingCustomerZipCode01);			
			$objCustomer->setTel01($hSendingCustomerTel01);
			$objCustomer->setFax01($hSendingCustomerFax01);
			$objCustomer->setAddress02($hSendingCustomerAddress02);
			$objCustomer->setTumbon02($hSendingCustomerTumbon02);
			$objCustomer->setAmphur02($hSendingCustomerAmphur02);
			$objCustomer->setProvince02($hSendingCustomerProvince02);
			$objCustomer->setZip02($hSendingCustomerZip02);
			$objCustomer->setTumbonCode02($hSendingCustomerTumbonCode02);
			$objCustomer->setAmphurCode02($hSendingCustomerAmphurCode02);
			$objCustomer->setProvinceCode02($hSendingCustomerProvinceCode02);
			$objCustomer->setZipCode02($hSendingCustomerZipCode02);
			$objCustomer->setTel02($hSendingCustomerTel02);
			$objCustomer->setFax02($hSendingCustomerFax02);
			$objCustomer->setAddress03($hSendingCustomerAddress03);
			$objCustomer->setTumbon03($hSendingCustomerTumbon03);
			$objCustomer->setAmphur03($hSendingCustomerAmphur03);
			$objCustomer->setProvince03($hSendingCustomerProvince03);
			$objCustomer->setZip03($hSendingCustomerZip03);
			$objCustomer->setTumbonCode03($hSendingCustomerTumbonCode03);
			$objCustomer->setAmphurCode03($hSendingCustomerAmphurCode03);
			$objCustomer->setProvinceCode03($hSendingCustomerProvinceCode03);
			$objCustomer->setZipCode03($hSendingCustomerZipCode03);
			$objCustomer->setTel03($hSendingCustomerTel03);
			$objCustomer->setFax03($hSendingCustomerFax03);	
			$objCustomer->setEditBy($sMemberId);
			$objCustomer->setIncomplete($hSendingCustomerIncomplete);
			$objCustomer->setMailback($hSendingCustomerMailback);
			
            $objOrder->setOrderId($hOrderId);
			if($hCancelStockCar){
				$objOrder->setOrderStatus("2");
			}else{
				$objOrder->setOrderStatus("3");
			}
            $objOrder->setOrderNumber($hOrderNumber);
            $objOrder->setOrderPrice($hOrderPrice);
            $objOrder->setOrderLocation($hOrderLocation);
			$objOrder->setSendingCustomerId($hSendingCustomerId);
			$objOrder->setSendingNumber($hSendingNumber);
			$objOrder->setSendingNumberNohead($hSendingNumberNohead);
			$hSendingDate = $Year."-".$Month."-".$Day;
            $objOrder->setSendingDate($hSendingDate);
			$objOrder->setSendingSaleId($hSendingSaleId);
			$objOrder->setSendingRemark($hSendingRemark);
			$objOrder->setSendingCarRemark($hSendingCarRemark);
			$objOrder->setStockRedId($hStockRedId);
			$objOrder->setNoRedCode($hNoRedCode);
			$objOrder->setStockCarId($hStockCarId);
			$objOrder->setStockCarIdTemp($hStockCarIdTemp);
			$objOrder->setCancelStockCar($hCancelStockCar);
			
			//��Ǩ�׹��� stock car
			if($hStockCarIdTemp != $hStockCarId AND $hStockCarIdTemp != ""){
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($hStockCarIdTemp);
				$objStockCar->setStatus(0);
				$objStockCar->setOrderId(0);
				$objStockCar->setCustomerId(0);
				$objStockCar->updateStatusByStockCarId();
			}
			
			$objOrder->setCarNumber($hCarNumber);
			$objOrder->setRedCodePrice($hRedCodePrice);
			$objOrder->setBlackCodeStatus($hBlackCodeStatus);
			$objOrder->setBlackCodeNumber($hBlackCodeNumber);
			$hBlackDate = $Year04."-".$Month04."-".$Day04;
			$objOrder->setBlackCodeDate($hBlackDate);
			$hRecieveDate = $Year03."-".$Month03."-".$Day03;
			$objOrder->setRecieveDate($hRecieveDate);
			
			
			$objStockCarCheck  = new StockCar();
			$objStockCarCheck->setStockCarId($hStockCarId);
			$objStockCarCheck->load();
			
			
			if($hBookingCarSeries != $objStockCarCheck->getCarSeriesId()){
				$objOrder->setSwitchCar(1);
			}else{
				$objOrder->setSwitchCar(0);
			}
			
			if($hSendingCustomerId == "" OR ($hBookingCustomerId != $hSendingCustomerId)){
				$objOrder->setSwitchCustomer(1);
			}else{
				$objOrder->setSwitchCustomer(0);
			}
			
			if($hBookingSaleId != $hSendingSaleId){
				$objOrder->setSwitchSale(1);
			}else{
				$objOrder->setSwitchSale(0);
			}
			
			
		$objOrder->setDiscountPrice($hDiscountPrice);
		$objOrder->setDiscountSubdown($hDiscountSubdown);
		$objOrder->setDiscountPremium($hOrderPremiumPriceTotal);
		$objOrder->setDiscountProduct($hOrderProductPriceTotal);
		$objOrder->setDiscountInsurance($hDiscountInsurance);
		$objOrder->setDiscountGoa($hDiscountGoa);
		$objOrder->setDiscount($hDiscount);
		$objOrder->setDiscountMargin($hDiscountMargin);
		$hDiscountMarginDate = $Year05."-".$Month05."-".$Day05;
		$objOrder->setDiscountMarginDate($hDiscountMarginDate);
		$objOrder->setDiscountSubdownVat($hDiscountSubdownVat);
		$objOrder->setDiscountAll($hDiscountAll);
		$objOrder->setDiscountOver($hDiscountOver);
		$objOrder->setDiscountCustomerPay($hDiscountCustomerPay);	
		$objOrder->setDiscountCustomerPayReason($hDiscountCustomerPayReason);	
		$objOrder->setDiscountTMT($hDiscountTMT);

		$objOrder->setBuyType($hBuyType);
		$objOrder->setBuyCompany($hBuyCompany);
		$objOrder->setBuyFee($hBuyFee);
		$objOrder->setBuyTime($hBuyTime);
		$objOrder->setBuyPayment($hBuyPayment);
		$objOrder->setBuyPrice($hBuyPrice);
		$objOrder->setBuyDown($hBuyDown);
		$objOrder->setBuyTotal($hBuyTotal);
		$objOrder->setBuyRemark($hBuyRemark);
		$objOrder->setBuyProduct($hBuyProduct);
		$objOrder->setBuyBegin($hBuyBegin);
		$objOrder->setBuyEngine($hBuyEngine);
		$objOrder->setBuyEngineRemark($hBuyEngineRemark);
		
		$objOrder->setRegistryPerson($hRegistryPerson);
		$objOrder->setRegistryPrice($hRegistryPrice);
		
		$objOrder->setPrbCompany($hPrbCompany);
		
		$objOrder->setPrbExpire($hPrbExpire);
		$objOrder->setPrbPrice($hPrbPrice);
		$objOrder->setInsureCompany($hInsureCompany);
		$hInsureExpire = $Year06."-".$Month06."-".$Day06;
		$objOrder->setInsureExpire($hInsureExpire);
		$objOrder->setInsurePrice($hInsurePrice);
		$objOrder->setInsureType($hInsureType);
		$objOrder->setInsureYear($hInsureYear);
		$objOrder->setInsureBudget($hInsureBudget);
		$objOrder->setInsureFrom($hInsureFrom);
		$objOrder->setInsureFromDetail($hInsureFromDetail);
		$objOrder->setInsureRemark($hInsureRemark);			
		$objOrder->setInsureCustomerPay($hInsureCustomerPay);
		
		$objOrder->setTMBNumber($hTMBNumber);			
		
		$objOrder->setTMTFree($hTMTFree);
		$objOrder->setTMTFreeRemark($hTMTFreeRemark);
		$objOrder->setTMTCoupon($hTMTCoupon);
		$objOrder->setTMTCouponRemark($hTMTCouponRemark);
		$objOrder->setTMTOther($hTMTOther);
		$objOrder->setTMTOtherRemark($hTMTOtherRemark);		
		$objOrder->setDiscountOther($hDiscountOther);
		$objOrder->setDiscountOtherPrice($hDiscountOtherPrice);	
		
		$objOrder->setOrderPrice($hOrderPrice);
		$objOrder->setOrderReservePrice($hOrderReserve);
		$objOrder->setOrderReservePrice01($hOrderReserve01);
		$objOrder->setOrderReservePrice02($hOrderReserve02);

		$objOrder->setOrderOldCar($hDiscountOldCar);
		
		$objOrder->setStockRedId($hStockRedId);
		$objOrder->setNoRedCode($hNoRedCode);
		$objOrder->setRedCodePrice($hRedCodePrice);
						
		$objOrder->setEditBy($sMemberId);
			
			
			if(isset($hOrderProductAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderProductId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderProductQty);
				$objOrderStock->setPrice($hOrderProductPrice);
				$objOrderStock->setTMT($hOrderProductTMT);
				if($hOrderProductOther){
					$objOrderStock->setRemark($hOrderProduct);
				}
				$objOrderStock->add();
			}
			
			if(isset($hOrderProductUpdate)){
				for($i=0;$i<$hCountOrderProduct;$i++){
					if($_POST["hOrderProductDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderProductStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderProductId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(0);
						$objOrderStock->setQty($_POST["hOrderProductQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderProductPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderProductTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderProductRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}
			
			if(isset($hOrderPremiumAdd)){
				$objOrderStock = new OrderStock();
				$objOrderStock->setStockProductId($hOrderPremiumId);
				$objOrderStock->setOrderId($hId);
				$objOrderStock->setQty($hOrderPremiumQty);
				$objOrderStock->setPrice($hOrderPremiumPrice);
				$objOrderStock->setTMT($hOrderPremiumTMT);
				$objOrderStock->setStockType(1);
				if($hOrderPremiumOther){
					$objOrderStock->setRemark($hOrderPremium);
				}
				$objOrderStock->add();
			}
			
			if(isset($hOrderPremiumUpdate)){
				for($i=0;$i<$hCountOrderPremium;$i++){
					if($_POST["hOrderPremiumDelete_".$i]){
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->delete();
					}else{
						$objOrderStock = new OrderStock();
						$objOrderStock->setOrderStockId($_POST["hOrderPremiumStockId_".$i]);
						$objOrderStock->setStockProductId($_POST["hOrderPremiumId_".$i]);
						$objOrderStock->setOrderId($hId);
						$objOrderStock->setStockType(1);
						$objOrderStock->setQty($_POST["hOrderPremiumQty_".$i]);
						$objOrderStock->setPrice($_POST["hOrderPremiumPrice_".$i]);
						$objOrderStock->setTMT($_POST["hOrderPremiumTMT_".$i]);
						$objOrderStock->setRemark($_POST["hOrderPremiumRemark_".$i]);
						$objOrderStock->update();
					}
				}
			
			}			
			

			if(!$hOrderProductAdd AND !$hOrderPremiumAdd AND !$hOrderProductUpdate AND !$hOrderPremiumUpdate){			
			
			
    		$pasrErrCustomer = $objCustomer->checkCrlSending($hSubmit);
			$pasrErrOrder = $objOrder->checkCrlSending($hSubmit);
			If ( Count($pasrErrCustomer) == 0 AND Count($pasrErrOrder) == 0){

				if ($strMode=="Update") {
					if($hSendingCustomerId == "" OR $hSendingCustomerId == "0"){
						
						$hCustomerId = $objCustomer->add();
						$objOrder->setSendingCustomerId($hCustomerId);
						//echo "add";
						//exit;
						
					}else{
						$objCustomer->updateCrlSending();
						//echo "update";
						//exit;
					}
					
					$objOrder->updateCrlSending();
					
					//update stock car
					$objStockCar = new StockCar();
					$objStockCar->setCarNumber($hCarNumber);
					$objStockCar->setOrderId($hId);
					$objStockCar->setStatus(2);
					$objStockCar->updateStatus();
					
					//update stock red
					header("location:ccardNewList.php?hKeyword=$hKeyword&hSearch=$hSearch&hPage=$hPage");
					exit;
				}
				unset ($objCustomer);


			}else{
				$objCustomer->init();
				$objOrder->init();
				
				//$objOrderBooking->setOrderId($hId);
				//$objOrderBooking->load();		
				
				$arrDate = explode("-",$objOrder->getSendingDate());
				$Day = $arrDate[2];
				$Month = $arrDate[1];
				$Year = $arrDate[0];			
				
				$arrDate = explode("-",$objOrder->getBookingDate());
				$Day02 = $arrDate[2];
				$Month02 = $arrDate[1];
				$Year02 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getRecieveDate());
				$Day03 = $arrDate[2];
				$Month03 = $arrDate[1];
				$Year03 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getBlackCodeDate());
				$Day04 = $arrDate[2];
				$Month04 = $arrDate[1];
				$Year04 = $arrDate[0];		
				
				$arrDate = explode("-",$objOrder->getDiscountMarginDate());
				$Day05 = $arrDate[2];
				$Month05 = $arrDate[1];
				$Year05 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getInsureExpire());
				$Day06 = $arrDate[2];
				$Month06 = $arrDate[1];
				$Year06 = $arrDate[0];
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
				/*
				$hCustomerId = $objOrder->getSendingCustomerId();
			
				$objSendingCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objSendingCustomer->load();
				$strMode="Update";
				*/
				$objSendingCustomer = $objCustomer;
				
				
				$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
				$objBookingCustomer->load();		
						
				
				$arrDate = explode("-",$objBookingCustomer->getInformationDate());
				$BookingDay = $arrDate[2];
				$BookingMonth = $arrDate[1];
				$BookingYear = $arrDate[0];
				
				$arrDate = explode("-",$objBookingCustomer->getBirthDay());
				$BookingDay01 = $arrDate[2];
				$BookingMonth01 = $arrDate[1];
				$BookingYear01 = $arrDate[0];
				
				$arrDate = explode("-",$objSendingCustomer->getInformationDate());
				$DaySending = $arrDate[2];
				$MonthSending = $arrDate[1];
				$YearSending = $arrDate[0];
				
				$arrDate = explode("-",$objSendingCustomer->getBirthDay());
				$Day01Sending = $arrDate[2];
				$Month01Sending = $arrDate[1];
				$Year01Sending = $arrDate[0];		
				
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getBookingSaleId());
				$objSale->load();
				
				$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();				
				
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getSendingSaleId());
				$objSale->load();
				
				$hSendingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();		
				// load stock car and red code
				
				$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
				$objBookingCarColor->load();
				
				$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
				$objBookingCarSeries->load();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
				
				
				$objSendingEvent->setEventId($objSendingCustomer->getEventId());
				$objSendingEvent->load();
				
			}//end check Count($pasrErr)

		
		}else{//end check $hOrderProductAdd 
		
				//$objOrderBooking->setOrderId($hId);
				//$objOrderBooking->load();		
				
				$arrDate = explode("-",$objOrder->getSendingDate());
				$Day = $arrDate[2];
				$Month = $arrDate[1];
				$Year = $arrDate[0];			
				
				$arrDate = explode("-",$objOrder->getBookingDate());
				$Day02 = $arrDate[2];
				$Month02 = $arrDate[1];
				$Year02 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getRecieveDate());
				$Day03 = $arrDate[2];
				$Month03 = $arrDate[1];
				$Year03 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getBlackCodeDate());
				$Day04 = $arrDate[2];
				$Month04 = $arrDate[1];
				$Year04 = $arrDate[0];		
				
				$arrDate = explode("-",$objOrder->getPrbExpire());
				$Day05 = $arrDate[2];
				$Month05 = $arrDate[1];
				$Year05 = $arrDate[0];
				
				$arrDate = explode("-",$objOrder->getInsureExpire());
				$Day06 = $arrDate[2];
				$Month06 = $arrDate[1];
				$Year06 = $arrDate[0];
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
				/*
				$hCustomerId = $objOrder->getSendingCustomerId();
			
				$objSendingCustomer->setCustomerId($objOrder->getSendingCustomerId());
				$objSendingCustomer->load();
				$strMode="Update";
				*/
				$objSendingCustomer = $objCustomer;
				
				
				$objBookingCustomer->setCustomerId($objOrderBooking->getBookingCustomerId());
				$objBookingCustomer->load();		
						
				
				$arrDate = explode("-",$objBookingCustomer->getInformationDate());
				$BookingDay = $arrDate[2];
				$BookingMonth = $arrDate[1];
				$BookingYear = $arrDate[0];
				
				$arrDate = explode("-",$objBookingCustomer->getBirthDay());
				$BookingDay01 = $arrDate[2];
				$BookingMonth01 = $arrDate[1];
				$BookingYear01 = $arrDate[0];
				
				$arrDate = explode("-",$objSendingCustomer->getInformationDate());
				$DaySending = $arrDate[2];
				$MonthSending = $arrDate[1];
				$YearSending = $arrDate[0];
				
				$arrDate = explode("-",$objSendingCustomer->getBirthDay());
				$Day01Sending = $arrDate[2];
				$Month01Sending = $arrDate[1];
				$Year01Sending = $arrDate[0];		
				
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getBookingSaleId());
				$objSale->load();
				
				$hBookingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();				
				
				$objSale = new Member();
				$objSale->setMemberId($objOrder->getSendingSaleId());
				$objSale->load();
				
				$hSendingSaleName = $objSale->getFirstname()." ".$objSale->getLastname();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();		
				// load stock car and red code
				
				$objBookingCarColor->setCarColorId($objOrderBooking->getBookingCarColor());
				$objBookingCarColor->load();
				
				$objBookingCarSeries->setCarSeriesId($objOrderBooking->getBookingCarSeries());
				$objBookingCarSeries->load();		
				
				$objStockCar = new StockCar();
				$objStockCar->setStockCarId($objOrder->getStockCarId());
				$objStockCar->load();
				
				$objStockRed = new StockRed();
				$objStockRed->setStockRedId($objOrder->getStockRedId());
				$objStockRed->load();
				
				
				$objSendingEvent->setEventId($objSendingCustomer->getEventId());
				$objSendingEvent->load();
		
		
		
		}
	
	}	
}

$objOrderStockList = new OrderStockList();
$objOrderStockList->setFilter(" order_id= $hId AND stock_type=0 ");
$objOrderStockList->setPageSize(0);
$objOrderStockList->setSortDefault(" stock_product_id DESC");
$objOrderStockList->load();

$objOrderStockPremiumList = new OrderStockList();
$objOrderStockPremiumList->setFilter(" order_id= $hId AND stock_type=1 ");
$objOrderStockPremiumList->setPageSize(0);
$objOrderStockPremiumList->setSortDefault(" stock_product_id DESC");
$objOrderStockPremiumList->load();


$pageTitle = "1. �к��������١���";
$strHead03 = "�ѹ�֡�����š�����ͺ";
$pageContent = "1.5  �ѹ�֡�����š�����ͺ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit()
	{

		if (document.forms.frm01.hTrick.value=="false" ){
			document.frm01.hTrick.value="true";
			return false;
		}			
	
	
		if (document.forms.frm01.hOrderNumber.value=="")
		{
			alert("��س��к��Ţ���㺨ͧ");
			document.forms.frm01.hOrderNumber.focus();
			return false;
		} 			
	
		if (document.forms.frm01.Day.value=="" || document.forms.frm01.Day.value=="00")
		{
			alert("��س��к��ѹ��������");
			document.forms.frm01.Day.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day.value,1,31) == false) {
				document.forms.frm01.Day.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month.value==""  || document.forms.frm01.Month.value=="00")
		{
			alert("��س��к���͹��������");
			document.forms.frm01.Month.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month.value,1,12) == false){
				document.forms.frm01.Month.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year.value==""  || document.forms.frm01.Year.value=="0000")
		{
			alert("��س��кػշ����������");
			document.forms.frm01.hYear.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year.focus();
				return false;
			}
		} 					
	
		if (document.forms.frm01.hSendingSaleId.value=="")
		{
			alert("��س��кؾ�ѡ�ҹ���");
			document.forms.frm01.hSendingSaleId.focus();
			return false;
		} 			
		
		if (document.forms.frm01.hOrderPrice.value=="" || document.forms.frm01.hOrderPrice.value==0)
		{
			alert("��س��к��ҤҢ��");
			document.forms.frm01.hOrderPrice.focus();
			return false;
		}else{
			if(checkNumValue(document.forms.frm01.hOrderPrice.value) == false){
				document.forms.frm01.hOrderPrice.focus();
				return false;
			}
		}				

		if (document.forms.frm01.hSendingCustomerGroupId.options[frm01.hSendingCustomerGroupId.selectedIndex].value=="0")
		{
			alert("��س����͡���觷���Ңͧ�١���");
			document.forms.frm01.hSendingCustomerGroupId.focus();
			return false;
		} 	

		if (document.forms.frm01.hSendingEventId.value=="" || document.forms.frm01.hSendingEventId.value==0)
		{
			alert("��س��кت��ͧҹ�͡ʶҹ���");
			document.forms.frm01.hEvent.focus();
			return false;
		} 		
	
		if (document.forms.frm01.hSendingCustomerSaleId.value=="" || document.forms.frm01.hSendingCustomerSaleId.value==0)
		{
			alert("��س��кت��ͼ����������");
			document.forms.frm01.hSendingCustomerSaleName.focus();
			return false;
		} 		
		
		if (document.forms.frm01.hSendingCustomerName.value=="")
		{
			alert("��س��кت���-���ʡ��");
			document.forms.frm01.hSendingCustomerName.focus();
			return false;
		} 			

		if(document.forms.frm01.hSendingCustomerIncomplete.checked == false){
			
	
			if (document.forms.frm01.hSendingCustomerAddress.value=="")
			{
				alert("��س��кط������");
				Set_Display_TypeSendingCustomer('1');
				document.forms.frm01.hSendingCustomerAddress.focus();
				return false;
			} 	
		
			if (document.forms.frm01.hSendingCustomerTumbonCode.value=="")
			{
				alert("��س��кصӺ�");
				Set_Display_TypeSendingCustomer('1');
				document.forms.frm01.hSendingCustomerTumbon.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hSendingCustomerAmphurCode.value=="")
			{
				alert("��س��к������");
				Set_Display_TypeSendingCustomer('1');
				document.forms.frm01.hSendingCustomerAmphur.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hSendingCustomerProvinceCode.value=="")
			{
				alert("��س��кبѧ��Ѵ");
				Set_Display_TypeSendingCustomer('1');
				document.forms.frm01.hSendingCustomerProvince.focus();
				return false;
			} 
			
			if (document.forms.frm01.hSendingCustomerZipCode.value=="")
			{
				alert("��س��к�������ɳ���");
				Set_Display_TypeSendingCustomer('1');
				document.forms.frm01.hSendingCustomerZip.focus();
				return false;
			} 					
			
			if (document.forms.frm01.hSendingCustomerAddress03.value=="")
			{
				alert("��س��кط�����������͡���");
				Set_Display_TypeSendingCustomer('4');
				document.forms.frm01.hSendingCustomerAddress03.focus();
				return false;
			} 	
		
			if (document.forms.frm01.hSendingCustomerTumbonCode03.value=="")
			{
				alert("��س��кصӺŷ�����͡���");
				Set_Display_TypeSendingCustomer('4');
				document.forms.frm01.hSendingCustomerTumbon03.focus();
				return false;
			} 	
			
			if (document.forms.frm01.hSendingCustomerAmphurCode03.value=="")
			{
				alert("��س��к�����ͷ�����͡���");
				Set_Display_TypeSendingCustomer('4');
				document.forms.frm01.hSendingCustomerAmphur03.focus();
				return false;
			} 			
			
			if (document.forms.frm01.hSendingCustomerProvinceCode03.value=="")
			{
				alert("��س��кبѧ��Ѵ������͡���");
				Set_Display_TypeSendingCustomer('4');
				document.forms.frm01.hSendingCustomerProvince03.focus();
				return false;
			} 
			
			if (document.forms.frm01.hSendingCustomerZipCode03.value=="")
			{
				alert("��س��к�������ɳ��������͡���");
				Set_Display_TypeSendingCustomer('4');
				document.forms.frm01.hSendingCustomerZip03.focus();
				return false;
			} 				
					

		}
		if(document.frm01.hCancelStockcar.checked == false){
			if (document.forms.frm01.hStockCarId.value=="")
			{
				alert("��س����͡Ẻö�ҡ�к�");
				document.forms.frm01.hCarNumber.focus();
				return false;
			} 	
		}
	/*
		if (document.forms.frm01.Day03.value=="" || document.forms.frm01.Day03.value=="00")
		{
			alert("��س��к��ѹ����˹����ͺ");
			document.forms.frm01.Day03.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Day03.value,1,31) == false) {
				document.forms.frm01.Day03.focus();
				return false;
			}
		} 			
	
		if (document.forms.frm01.Month03.value==""  || document.forms.frm01.Month03.value=="00")
		{
			alert("��س��к���͹����˹����ͺ");
			document.forms.frm01.Month03.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Month03.value,1,12) == false){
				document.forms.frm01.Month03.focus();
				return false;
			}
		} 			
		
		if (document.forms.frm01.Year03.value==""  || document.forms.frm01.Year03.value=="0000")
		{
			alert("��س��кػշ���˹����ͺ");
			document.forms.frm01.hYear03.focus();
			return false;
		}else{
			if(checkNum(document.forms.frm01.Year03.value,<?=date("Y")-100?>,<?=date("Y")?>) == false) {
				document.forms.frm01.Year03.focus();
				return false;
			}
		} 					
		
		if( document.frm01.hNoRedCode.checked == false ){
		
			if (document.forms.frm01.hStockRedId.value=="" || document.forms.frm01.hStockRedId.value== "0")
			{
				alert("��س����͡����ᴧ�ҡ�к�");
				document.forms.frm01.hStockRedName.focus();
				return false;
			} 					
		}
		*/
		
	}
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<script language=JavaScript >

function Set_Load(){
	document.getElementById("form_add01").style.display = "";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
}

function Set_Display_Type(typeVal){
	document.getElementById("form_add01").style.display = "none";
	document.getElementById("form_add02").style.display = "none";
	document.getElementById("form_add03").style.display = "none";
	document.getElementById("form_add04").style.display = "none";
	document.getElementById("form_add01_detail").style.display = "none";
	document.getElementById("form_add02_detail").style.display = "none";
	document.getElementById("form_add03_detail").style.display = "none";
	document.getElementById("form_add04_detail").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01").style.display = "";
			document.getElementById("form_add01_detail").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02").style.display = "";
			document.getElementById("form_add02_detail").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03").style.display = "";
			document.getElementById("form_add03_detail").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04").style.display = "";
			document.getElementById("form_add04_detail").style.display = "";
			break;
	}//switch
}


   function sameplace(val){
   		if(val ==1){
			document.frm01.hAddress01.value = document.frm01.hAddress.value;
			document.frm01.hTumbon01.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur01.value = document.frm01.hAmphur.value;
			document.frm01.hProvince01.value = document.frm01.hProvince.value;
			document.frm01.hZip01.value = document.frm01.hZip.value;		
			document.frm01.hTel01.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax01.value = document.frm01.hFax.value;		
		}
   		if(val ==2){
			document.frm01.hAddress02.value = document.frm01.hAddress.value;
			document.frm01.hTumbon02.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur02.value = document.frm01.hAmphur.value;
			document.frm01.hProvince02.value = document.frm01.hProvince.value;
			document.frm01.hZip02.value = document.frm01.hZip.value;		
			document.frm01.hTel02.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax02.value = document.frm01.hFax.value;		
		}
   		if(val ==3){
			document.frm01.hAddress03.value = document.frm01.hAddress.value;
			document.frm01.hTumbon03.value = document.frm01.hTumbon.value;
			document.frm01.hAmphur03.value = document.frm01.hAmphur.value;
			document.frm01.hProvince03.value = document.frm01.hProvince.value;
			document.frm01.hZip03.value = document.frm01.hZip.value;		
			document.frm01.hTel03.value = document.frm01.hHomeTel.value;		
			document.frm01.hFax03.value = document.frm01.hFax.value;		
		}
   
   }

function Set_Display_TypeSendingCustomer(typeVal){
	document.getElementById("form_add01SendingCustomer").style.display = "none";
	document.getElementById("form_add02SendingCustomer").style.display = "none";
	document.getElementById("form_add03SendingCustomer").style.display = "none";
	document.getElementById("form_add04SendingCustomer").style.display = "none";
	document.getElementById("form_add01_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add02_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add03_detailSendingCustomer").style.display = "none";
	document.getElementById("form_add04_detailSendingCustomer").style.display = "none";

	switch(typeVal){
		case "1" :		
			document.getElementById("form_add01SendingCustomer").style.display = "";
			document.getElementById("form_add01_detailSendingCustomer").style.display = "";
			break;
		case "2" :		
			document.getElementById("form_add02SendingCustomer").style.display = "";
			document.getElementById("form_add02_detailSendingCustomer").style.display = "";
			break;
		case "3" :		
			document.getElementById("form_add03SendingCustomer").style.display = "";
			document.getElementById("form_add03_detailSendingCustomer").style.display = "";
			break;
		case "4" :		
			document.getElementById("form_add04SendingCustomer").style.display = "";
			document.getElementById("form_add04_detailSendingCustomer").style.display = "";
			break;
	}//switch
}


   function sameplaceSendingCustomer(val){
   		if(val ==1){
			document.frm01.hSendingCustomerAddress01.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon01.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur01.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince01.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip01.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel01.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax01.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==2){
			document.frm01.hSendingCustomerAddress02.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon02.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur02.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince02.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip02.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel02.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax02.value = document.frm01.hSendingCustomerFax.value;		
		}
   		if(val ==3){
			document.frm01.hSendingCustomerAddress03.value = document.frm01.hSendingCustomerAddress.value;
			document.frm01.hSendingCustomerTumbon03.value = document.frm01.hSendingCustomerTumbon.value;
			document.frm01.hSendingCustomerAmphur03.value = document.frm01.hSendingCustomerAmphur.value;
			document.frm01.hSendingCustomerProvince03.value = document.frm01.hSendingCustomerProvince.value;
			document.frm01.hSendingCustomerZip03.value = document.frm01.hSendingCustomerZip.value;		
			document.frm01.hSendingCustomerTel03.value = document.frm01.hSendingCustomerHomeTel.value;		
			document.frm01.hSendingCustomerFax03.value = document.frm01.hSendingCustomerFax.value;		
		}
   
   }   
   
</script>

<?if(sizeof($pasrErrCustomer) > 0  OR sizeof($pasrErrOrder) > 0){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" border="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" border="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErrCustomer as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>
					<?foreach ($pasrErrOrder as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" border="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" border="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" border="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" border="0"></td>
	</tr>
	</table>				
<?}?>				



<form name="frm01" action="ccardUpdate.php#free" method="POST"  onsubmit="return check_submit();" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hOrderId" value="<?=$hId?>">
	  <input type="hidden" name="hACard" value="<?=$objSendingCustomer->getACard();?>">
	  <input type="hidden" name="hBCard" value="<?=$objSendingCustomer->getBCard();?>">
  	  <input type="hidden" name="hCCard" value="<?=$objSendingCustomer->getCCard();?>">
	  <input type="hidden" name="hCustomerId"  value="<?=$objCustomer->getCustomerId();?>">
	  <input type="hidden" name="hSendingNumber" value="<?=$objOrder->getSendingNumber()?>">
	  <input type="hidden" name="hSendingCustomerId" value="<?=$objOrder->getSendingCustomerId()?>">	  	
	  <input type="hidden" name="hOrderStatus" value="1">
	  <input type="hidden" name="hOrderNumberTemp" value="<?=$objOrder->getOrderNumber()?>">
	  <input type="hidden" name="hBookingCustomerId" value="<?=$objOrder->getBookingCustomerId()?>">
	  <input type="hidden" name="hBookingSaleId" value="<?=$objOrder->getBookingSaleId()?>">
	  <input type="hidden" name="hBookingCarColor" value="<?=$objOrder->getBookingCarColor()?>">
	  <input type="hidden" name="hBookingCarSeries" value="<?=$objOrder->getBookingCarSeries()?>">
	  <input type="hidden" name="hKeyword" value="<?=$hKeyword?>">
	  <input type="hidden" name="hSearch" value="<?=$hSearch?>">
	  <input type="hidden" name="hPage" value="<?=$hPage?>">
	  <input type="hidden" name="hRefer" value="<?if($hRefer==""){echo $HTTP_REFERER;}else{ echo $hRefer;}?>">
	   <input type="hidden" name="hTrick" value="true">
	  

<a name="bookingOrderA"></a>
<span style="display:none" id="bookingOrder">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�����š�èͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><?=$objOrderBooking->getOrderNumber()?></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ���ͧ</strong></td>
			<td  valign="top">
				<?=$objOrderBooking->getBookingDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >				
				<?=$hBookingSaleName?>
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">

			</td>			
		</tr>
		</table>
	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="85%">�����š�����ͺ</td>
			<td align="right" >
				<span id="hideBookingOrder" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="hideBookingOrder()">��͹�����š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingOrder">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingOrder()"></td>
					<td><font color=red><a href="#bookingOrderA" onclick="showBookingOrder()">�ʴ������š�èͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>	
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td  class="i_background03" valign="top"><strong>�Ţ���㺨ͧ</strong> </td>
			<td  valign="top"><input type="text" name="hOrderNumber" size="30"  value="<?=$objOrder->getOrderNumber()?>"></td>		
			<td  valign="top" class="i_background03"><strong>�ѹ��������</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" width="20%" valign="top"><strong>��ѡ�ҹ���</strong>   </td>
			<td width="30%" valign="top" >
				<input type="hidden" readonly size="2" name="hSendingSaleId"  value="<?=$objOrder->getSendingSaleId();?>">
				<input type="text" name="hSendingSaleName" size="30"   onKeyDown="if(event.keyCode==13 && frm01.hSendingSaleId.value != '' ) frm01.hOrderPrice.focus();if(event.keyCode !=13 ) frm01.hSendingSaleId.value='';"  value="<?=$hSendingSaleName?>">
			</td>
			<td class="i_background03" width="20%" valign="top"></td>
			<td width="30%" valign="top">
				<input type="checkbox" value="1" disabled=true name="hSwitchSale" <?if($objOrder->getSwitchSale() > 0) echo "checked"?>> ���ꡡó�����¹��ѡ�ҹ���
			</td>			
		</tr>
		
		</table>
	</td>
</tr>
</table>
<br>
<a name="bookingCustomerA"></a>
<span style="display:none" id="bookingCustomer">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">�������١��Ҩͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td bgcolor="#fdf2b5">
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>������١���</strong> </td>
			<td width="30%">
				<?
				$objGroup  = new CustomerGroup();
				$objGroup->setGroupId($objBookingCustomer->getGroupId());
				$objGroup->load();
				
				echo $objGroup->getTitle();
				unset($objGroup);
				?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
				<?=$objBookingCustomer->getInformationDate()?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���ʧҹ</strong> </td>
			<td valign="top">
				<?
				$objEvent  = new CustomerEvent();
				$objEvent->setEventId($objBookingCustomer->getEventId());
				$objEvent->load();
				
				echo $objEvent->getTitle();
				unset($objEvent);
				?>
			</td>
			<td class="i_background03"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td ><?=$hBookingSaleName?></td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><?=$objBookingCustomer->getIDCard();?></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?
				$objGrade  = new CustomerGrade();
				$objGrade->setGradeId($objBookingCustomer->getGradeId());
				$objGrade->load();
				
				echo $objGrade->getTitle();
				unset($objGrade);
				?>			
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table>
				<tr>
					<td valign="top">
						<?
						if($objBookingCustomer->getCustomerTitleId() > 0 ){
							echo $objBookingCustomer->getCustomerTitleDetail();
						}else{
							if($objBookingCustomer->getTitle() == ""){
									echo "�س";
							}else{ 
									echo $objBookingCustomer->getTitle(); 
							}
						}
						?>
					</td>
					<td valign="top">
					<?if($objBookingCustomer->getFirstname() != ""){
						$name = $objBookingCustomer->getFirstname()."  ".$objBookingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<?=$name?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getBirthDay()?></td>
		</tr>
		<tr>
			<td colspan="4" align="right">				
				<table id="form_add01" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_Type('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_Type('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>	

		<table id=form_add01_detail width="100%" cellpadding="2" cellspacing="0" class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><?=$objBookingCustomer->getAmphur();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><?=$objBookingCustomer->getHomeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><?=$objBookingCustomer->getMobile();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><?=$objBookingCustomer->getOfficeTel();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><?=$objBookingCustomer->getEmail();?>&nbsp;</td>
			<td class="i_background03"></td>
			<td></td>
		</tr>		
		</table>
		<table  id=form_add02_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress01();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince01();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip01();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel01();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax01();?>&nbsp;</td>
		</tr>
		</table>
		<table  id=form_add03_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress02();?>&nbsp;</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getTumbon02();?>&nbsp;</td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><?=$objBookingCustomer->getAmphur02();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince02();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip02();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel02();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax02();?>&nbsp;</td>
		</tr>		
		</table>
		<table  id=form_add04_detail style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><?=$objBookingCustomer->getAddress03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getTumbon03();?>&nbsp;</td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><?=$objBookingCustomer->getAmphur03();?>&nbsp;</td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><?=$objBookingCustomer->getProvince03();?>&nbsp;</td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><?=$objBookingCustomer->getZip03();?>&nbsp;</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><?=$objBookingCustomer->getTel03();?>&nbsp;</td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><?=$objBookingCustomer->getFax03();?>&nbsp;</td>
		</tr>		
		</table>
		
				
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
</span>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="13%">�������١����Ѻ�ͺö</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCustomer" onclick="setCustomerValue()" <?if($objOrder->getSwitchCustomer() > 0) echo "checked"?>> ���ꡡó�����¹����Ѻ�ͺ</td>
			<td align="right" >
				<span id="hideBookingCustomer" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="hideBookingCustomer()">��͹�������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCustomer">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCustomer()"></td>
					<td><font color=red><a href="#bookingCustomerA" onclick="showBookingCustomer()">�ʴ��������١��Ҩͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" width="20%"><strong>���觷���Ңͧ�١���</strong> </td>
			<td width="30%">
				<?$objCustomerGroupList->printSelect("hSendingCustomerGroupId",$objSendingCustomer->getGroupId(),"��س����͡");?>
			</td>
			<td class="i_background03" width="20%"><strong>�ѹ�����������</strong></td>
			<td width="30%">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=DaySending value="<?=$DaySending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=MonthSending value="<?=$MonthSending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=YearSending value="<?=$YearSending?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(YearSending,DaySending, MonthSending, YearSending,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>���ͧҹ�͡ʶҹ���</strong></td>
			<td valign="top">
				<input type="hidden" size=3 name="hSendingEventId"  value="<?=$objSendingCustomer->getEventId()?>">
				<INPUT onKeyDown="if(event.keyCode==13 && frm01.hSendingEventId.value != '' ) frm01.hSendingCustomerSaleName.focus();if(event.keyCode !=13 ) frm01.hSendingEventId.value='';"   name="hEvent"  size=40 value="<?=$objSendingEvent->getTitle()?>">
				
			</td>
			<td class="i_background03" valign="top"><strong>��ѡ�ҹ�����������</strong>   </td>
			<td >
				<input type="text" readonly size="3" name="hSendingCustomerSaleId"  value="<?=$objSendingCustomer->getSaleId();?>">
				<input type="text" name="hSendingCustomerSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerSaleName.value != '' ) frm01.hSendingCustomerIDCard.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerSaleId.value='';"    value="<?=$hSendingSaleName?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѵû�ЪҪ�</strong></td>
			<td ><input type="text" name="hSendingCustomerIDCard" size="30" maxlength="13"  value="<?=$objSendingCustomer->getIDCard();?>"></td>
			<td class="i_background03"><strong>�ô</strong></td>
			<td >
				<?$objCustomerGradeList->printSelect("hSendingCustomerGradeId",$objSendingCustomer->getGradeId(),"��س����͡");?>
			</td>
		</tr>
		<tr>
			<td class="i_background03"  valign="top"><strong>����-���ʡ��</strong> </td>
			<td>
				<table cellpadding="1" cellspacing="0">
				<tr>
					<td valign="top">
						<?=DropdownTitle("hSendingCustomerTitle",$objSendingCustomer->getTitle(),"-");?>
					</td>
					<td valign="top">
					<?if($objSendingCustomer->getFirstname() != ""){
						$name = $objSendingCustomer->getFirstname()."  ".$objSendingCustomer->getLastname();
					}else{
						$name = "";
					}?>					
					<INPUT  name="hSendingCustomerName"  size="20"  size=30 value="<?=$name?>">
					</td>
					<td valign="top">					
					<?$objCustomerTitleList->printSelect("hSendingCustomerTitleId",$objSendingCustomer->getCustomerTitleId(),"���͡��");?>
					</td>
				</tr>
				</table>
				</td>
			<td class="i_background03"  valign="top"><strong>�ѹ�Դ</strong></td>
			<td  valign="top">
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day01Sending value="<?=$Day01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01Sending value="<?=$Month01Sending?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year01Sending value="<?=$Year01Sending?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year01Sending, Day01Sending, Month01Sending, Year01Sending,popCal);return false"></td>		
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="right">
				
				<table id="form_add01SendingCustomer" cellpadding="5" cellspacing="0" >
				<tr>
					<td align="center" class="i_background">�������Ѩ�غѹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>					
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add02SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background">�������������¹��ҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add03SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background" >���ӧҹ</td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('4');">������͡���</a></td>
				</tr>
				</table>
				<table  id="form_add04SendingCustomer" style="display:none"  cellpadding="5"  cellspacing="0" >
				<tr>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('1');">�������Ѩ�غѹ</a></td>
					<td align="center" class="i_background02"><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('2');">�������������¹��ҹ</a></td>
					<td align="center" class="i_background02" ><a href="#name" class="tab" onclick="Set_Display_TypeSendingCustomer('3');">���ӧҹ</a></td>
					<td align="center" class="i_background" >������͡���</td>
				</tr>
				</table>					
					
					
		<table id=form_add01_detailSendingCustomer width="100%" cellpadding="2" cellspacing="0"  class="i_background">		
		<tr>
			<td class="i_background03" valign="top"><strong>�������Ѩ�غѹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hSendingCustomerAddress" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress();?>"></td>
		</tr>
		<tr>
			<td   width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3"  name="hSendingCustomerTumbonCode"  value="<?=$objSendingCustomer->getTumbonCode();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode.value != '' ) frm01.hSendingCustomerAmphur.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode.value='';" name="hSendingCustomerTumbon" size="50"  value="<?=$objSendingCustomer->getTumbon();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td    width="35%" valign="top"><input type="hidden" size="3"  name="hSendingCustomerAmphurCode"  value="<?=$objSendingCustomer->getAmphurCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode.value != '' ) frm01.hSendingCustomerProvince.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode.value='';" name="hSendingCustomerAmphur" size="30"  value="<?=$objSendingCustomer->getAmphur();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode"  value="<?=$objSendingCustomer->getProvinceCode();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode.value != '' ) frm01.hSendingCustomerZip.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode.value='';" name="hSendingCustomerProvince" size="30"  value="<?=$objSendingCustomer->getProvince();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode"  value="<?=$objSendingCustomer->getZip();?>"><input type="text" name="hSendingCustomerZip"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode.value != '' ) frm01.hSendingCustomerHomeTel.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode.value='';" size="30"  value="<?=$objSendingCustomer->getZip();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ���ҹ</strong> </td>
			<td><input type="text" name="hSendingCustomerHomeTel" size="30"  value="<?=$objSendingCustomer->getHomeTel();?>"></td>
			<td class="i_background03"><strong>��Ͷ��</strong></td>
			<td><input type="text" name="hSendingCustomerMobile" size="30"  value="<?=$objSendingCustomer->getMobile();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ����ӧҹ</strong></td>
			<td><input type="text" name="hSendingCustomerOfficeTel" size="30"  value="<?=$objSendingCustomer->getOfficeTel();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hSendingCustomerFax" size="30"  value="<?=$objSendingCustomer->getFax();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������</strong></td>
			<td><input type="text" name="hSendingCustomerEmail" size="30"  value="<?=$objSendingCustomer->getEmail();?>"></td>
			<td class="i_background03"></td>
			<td></td>
		</tr>	
		<tr>
			<td class="i_background03"><strong>�������������ó�</strong></td>
			<td><input type="checkbox" name="hSendingCustomerIncomplete" value="1" <?if($objSendingCustomer->getIncomplete() == 1) echo "checked";?>></td>
			<td class="i_background03"><strong>�����µա�Ѻ</strong></td>
			<td><input type="checkbox" name="hSendingCustomerMailback" value="1" <?if($objSendingCustomer->getMailback() == 1) echo "checked";?>></td>
		</tr>		
		</table>
		<table  id=form_add02_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>�������������¹��ҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hSendingCustomerAddress01" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress01();?>">&nbsp;&nbsp;<input type="checkbox" name="hSendingCustomerSamePlace01" onclick="sameplaceSendingCustomer(1)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode01"  value="<?=$objSendingCustomer->getTumbonCode01();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode01.value != '' ) frm01.hSendingCustomerAmphur01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode01.value='';" name="hSendingCustomerTumbon01" size="50"  value="<?=$objSendingCustomer->getTumbon01();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode01"  value="<?=$objSendingCustomer->getAmphurCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode01.value != '' ) frm01.hSendingCustomerProvince01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode01.value='';" name="hSendingCustomerAmphur01" size="30"  value="<?=$objSendingCustomer->getAmphur01();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode01"  value="<?=$objSendingCustomer->getProvinceCode01();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode01.value != '' ) frm01.hSendingCustomerZip01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode01.value='';" name="hSendingCustomerProvince01" size="30"  value="<?=$objSendingCustomer->getProvince01();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode01"  value="<?=$objSendingCustomer->getZip01();?>"><input type="text" name="hSendingCustomerZip01"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode01.value != '' ) frm01.hSendingCustomerTel01.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode01.value='';" size="30"  value="<?=$objSendingCustomer->getZip01();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hSendingCustomerTel01" size="30"  value="<?=$objSendingCustomer->getTel01();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hSendingCustomerFax01" size="30"  value="<?=$objSendingCustomer->getFax01();?>"></td>
		</tr>
		</table>
		<table  id=form_add03_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>���������ӧҹ</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hSendingCustomerAddress02" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress02();?>">&nbsp;&nbsp;<input type="checkbox" name="hSendingCustomerSamePlace02" onclick="sameplaceSendingCustomer(2)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td   width="15%"   class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode02"  value="<?=$objSendingCustomer->getTumbonCode02();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode02.value != '' ) frm01.hSendingCustomerAmphur02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode02.value='';" name="hSendingCustomerTumbon02" size="50"  value="<?=$objSendingCustomer->getTumbon02();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td   width="15%"  valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td   width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode02"  value="<?=$objSendingCustomer->getAmphurCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode02.value != '' ) frm01.hSendingCustomerProvince02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode02.value='';" name="hSendingCustomerAmphur02" size="30"  value="<?=$objSendingCustomer->getAmphur02();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode02"  value="<?=$objSendingCustomer->getProvinceCode02();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode02.value != '' ) frm01.hSendingCustomerZip02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode02.value='';" name="hSendingCustomerProvince02" size="30"  value="<?=$objSendingCustomer->getProvince02();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode02"  value="<?=$objSendingCustomer->getZip02();?>"><input type="text" name="hSendingCustomerZip02"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode02.value != '' ) frm01.hSendingCustomerTel02.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode02.value='';" size="30"  value="<?=$objSendingCustomer->getZip02();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hSendingCustomerTel02" size="30"  value="<?=$objSendingCustomer->getTel02();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hSendingCustomerFax02" size="30"  value="<?=$objSendingCustomer->getFax02();?>"></td>
		</tr>		
		</table>
		<table  id=form_add04_detailSendingCustomer style="display:none" width="100%" cellpadding="2" cellspacing="0"  class="i_background">
		<tr>
			<td class="i_background03" valign="top"><strong>����������͡���</strong></td>
			<td colspan="3"><input type="text"  maxlength="50"  name="hSendingCustomerAddress03" maxlength="50" size="50"  value="<?=$objSendingCustomer->getAddress03();?>">&nbsp;&nbsp;<input type="checkbox" name="hSendingCustomerSamePlace03" onclick="sameplaceSendingCustomer(3)"> ������ǡѺ�������Ѩ�غѹ</td>
		</tr>
		<tr>
			<td  width="15%"  class="i_background03" valign="top"><strong>�Ӻ�/�ǧ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerTumbonCode03"  value="<?=$objSendingCustomer->getTumbonCode03();?>"><input type="text" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerTumbonCode03.value != '' ) frm01.hSendingCustomerAmphur03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerTumbonCode03.value='';" name="hSendingCustomerTumbon03" size="50"  value="<?=$objSendingCustomer->getTumbon03();?>"> <a href="areaTumbonList.php">�����������</a></td>
			<td  width="15%"   valign="top" class="i_background03"><strong>�����/ࢵ</strong></td>			
			<td  width="35%"  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerAmphurCode03"  value="<?=$objSendingCustomer->getAmphurCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerAmphurCode03.value != '' ) frm01.hSendingCustomerProvince03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerAmphurCode03.value='';" name="hSendingCustomerAmphur03" size="30"  value="<?=$objSendingCustomer->getAmphur03();?>"></td>
		</tr>
		<tr>
			<td  valign="top" class="i_background03"><strong>�ѧ��Ѵ</strong></td>			
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerProvinceCode03"  value="<?=$objSendingCustomer->getProvinceCode03();?>"><input type="text"  onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerProvinceCode03.value != '' ) frm01.hSendingCustomerZip03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerProvinceCode03.value='';" name="hSendingCustomerProvince03" size="30"  value="<?=$objSendingCustomer->getProvince03();?>"></td>
			<td  valign="top" class="i_background03"><strong>������ɳ���</strong></td>
			<td  valign="top"><input type="hidden" size="3" readonly name="hSendingCustomerZipCode03"  value="<?=$objSendingCustomer->getZip03();?>"><input type="text" name="hSendingCustomerZip03"  maxlength="5" onKeyDown="if(event.keyCode==13 && frm01.hSendingCustomerZipCode03.value != '' ) frm01.hSendingCustomerTel03.focus();if(event.keyCode !=13 ) frm01.hSendingCustomerZipCode03.value='';" size="30"  value="<?=$objSendingCustomer->getZip03();?>"></td>
		</tr>
		<tr>
			<td class="i_background03"><strong>���Ѿ��</strong> </td>
			<td><input type="text" name="hSendingCustomerTel03" size="30"  value="<?=$objSendingCustomer->getTel03();?>"></td>
			<td class="i_background03"><strong>ῡ��</strong></td>
			<td><input type="text" name="hSendingCustomerFax03" size="30"  value="<?=$objSendingCustomer->getFax03();?>"></td>
		</tr>		
		
		</table>					
					
					
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
<br>
<a name="bookingCarA"></a>
<span style="display:none" id="bookingCar">
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="i_background03" valign="top" width="20%"><strong>���ö</strong> </td>
			<td valign="top" width="30%">
				<?=$objBookingCarSeries->getCarModelTitle()?>
			</td>				
			<td width="20%" class="i_background03"><strong>Ẻö</strong>   </td>			
			<td width="30%"><?=$objBookingCarSeries->getTitle();?></td>				
		</tr>		
		<tr>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td colspan="3" >
				<?=$objBookingCarColor->getTitle()?>
			</td>
		</tr>		
		</table>
	</td>
</tr>
</table>
</span>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td class="i_background">
		<table width="100%" >
		<tr>
			<td width="10%">������ö���ͺ</td>
			<td width="65%"><input type="checkbox" disabled=true value="1" name="hSwitchCar" <?if($objOrder->getSwitchCar() > 0) echo "checked"?>> ���ꡡó�����¹ö</td>
			<td align="right" >
				<span id="hideBookingCar" style="display:none">
				<table>
				<tr>
					<td><img src="../images/zoomout.gif" alt="" width="18" height="19" border="0" onclick="hideBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="hideBookingCar()">��͹������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			
				<span id="showBookingCar">
				<table>
				<tr>
					<td><img src="../images/zoomin.gif" alt="" width="18" height="19" border="0" onclick="showBookingCar()"></td>
					<td><font color=red><a href="#bookingCarA" onclick="showBookingCar()">�ʴ�������ö�ͧ</a></font></td>
				</tr>
				</table>		
				</span>
			</td>	
		</tr>
		</table>
	</td>
</tr>
</table>

<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td width="20%"  class="i_background03" valign="top"><strong>�����Ţ�ѧ</strong></td>			
			<td width="30%" >
				<input type="hidden" readonly size=3 name="hStockCarIdTemp" value="<?=$objOrder->getStockCarId()?>">
				<input type="hidden" readonly size=3 name="hStockCarId" value="<?=$objOrder->getStockCarId()?>">
				<input type="text" name="hCarNumber" size="30"  value="<?=$objStockCar->getCarNumber()?>">&nbsp;&nbsp;
				<input type="checkbox" onclick="check_cancel_stockcar()" name="hCancelStockCar" value="1"> ¡��ԡ�Ţ����ͧ�Ţ�ѧ
			</td>
			<td width="20%" valign="top" ><strong>�����Ţ����ͧ</strong></td>
			<td valign="top"><label id="hEngineNumber"><?=$objStockCar->getEngineNumber()?></label></td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>���ö</strong> </td>
			<td>
				<label id="hCarModel01"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>
			<td class="i_background03"><strong>Ẻö</strong>   </td>
			<td >
				<label id="hCarSeries01"><?=$objCarSeries->getTitle()?></label>								
			</td>
		</tr>					
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td>
				<select  name="hGear">
					<option value="Auto" <?if($objStockCar->getGear() == "Auto") echo "selected"?>>Auto
					<option value="Manual" <?if($objStockCar->getGear() == "Manual") echo "selected"?>>Manual
				</select>
			</td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColorList->printSelect("hColorId",$objStockCar->getCarColorId(),"��س����͡");?>
			</td>
		</tr>		
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<select  name="hCarType">
					<option value="1" <?if($objCarSeries->getCarType() == "1") echo "selected"?>>ö��
					<option value="2" <?if($objCarSeries->getCarType() == "2") echo "selected"?>>ö�к�
					<option value="3" <?if($objCarSeries->getCarType() == "3") echo "selected"?>>ö 7 �����
					<option value="4" <?if($objCarSeries->getCarType() == "4") echo "selected"?>>ö���
				</select>			
			</td>		
			<td class="i_background03" width="20%"></td>
			<td width="30%">
				
			</td>
		</tr>			
		<tr>			
			<td class="i_background03"><strong>�ѹ����˹����ͺ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day03 value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month03 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year03 value="<?=$Year03?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year03,Day03, Month03, Year03,popCal);return false"></td>		
				</tr>
				</table>
			</td>		
			<td class="i_background03" valign="top"><strong>�ѹ��� TBR</strong></td>
			<td ><input type="text" name="hTMBNumber" size="10"  value="<?=$objOrder->getTMBNumber()?>"></td>
		</tr>
		</tr>
		<tr>			
			<td class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3" ><textarea name="hSendingCarRemark" rows="3" cols="50"><?=$objOrder->getSendingCarRemark()?></textarea></td>
		</tr>
		</table>
	</td>
</tr>
</table>

<br>
<br>
<table width="100%" cellpadding="3" cellspacing="0">
<tr>
	<td class="i_background">������ö�ͧ</td>
</tr>
</table>
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td width="20%" class="i_background03"><strong>Ẻö</strong></td>			
			<?if($hCarSeries == ""){
				$objCarSeries = new CarSeries();
				$objCarSeries->setCarSeriesId($objOrder->getBookingCarSeries());
				$objCarSeries->load();
				$hCarSeries = $objCarSeries->getTitle();
				}
			?>
			<td width="30%"><input type="hidden" readonly size="3" name="hBookingCarSeries"  value="<?=$objOrder->getBookingCarSeries();?>"><?=$hCarSeries;?></td>		
			<td class="i_background03" valign="top" width="20%"><strong>���ö</strong> </td>
			<td valign="top" width="30%">
				<input type="hidden" name="hBookingCarModel" >
				<label id="car_model"><?=$objCarSeries->getCarModelTitle()?></label>				
			</td>				
		</tr>			
		<tr>
			<td width="20%" class="i_background03"><strong>������</strong> </td>
			<td width="30%">
				<input type="hidden" name="hBookingCarType">
				<label id="car_type"><?=$objCarSeries->getCarTypeDetail()?></label>	
			</td>		
			<td class="i_background03" width="20%"><strong>Model</strong> </td>
			<td width="30%">
				<label id="car_code"><?=$objCarSeries->getModel()?></label>				
			</td>
		</tr>			
		<tr>
			<td class="i_background03"><strong>�����</strong> </td>
			<td><?=$objOrder->getBookingCarGear();?></td>
			<td class="i_background03"><strong>��ö</strong>   </td>
			<td >
				<?$objCarColor = new CarColor();
					$objCarColor->setCarColorId($objOrder->getBookingCarColor());
					$objCarColor->load();
					echo $objCarColor->getTitle();
				?>
			</td>
		</tr>		
		<tr>			
			<td  class="i_background03" valign="top"><strong>�����˵�</strong></td>
			<td colspan="3"><?=$objOrder->getBookingCarRemark()?></td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br>


<br>
<table align="center" width="100%">
<tr>
	<td colspan="4" class="i_background03" align="center">
	<br><br>
      <input type="hidden" name="hSubmit" value="<?=$strMode?>">
      &nbsp;&nbsp;&nbsp;&nbsp;<input type="Button" name="hSubmit" class="button" value="�Դ˹�Ҩ�" onclick="window.close();">			
	<br><br>
	</td>
</tr>
</table>
<?if ($strMode == "Update"){
$objMemberTemp = new Member();
$objMemberTemp->setMemberId($objOrder->getEditBy());
$objMemberTemp->load();
?>		
<table width="100%" >
<tr>
	<td align="right">��䢢���������ش�� :   <?=$objMemberTemp->getNickname()?>     �ѹ���    <?=$objOrder->getEditDate()?> </td>
</tr>
</table>
<?
unset($objMemberTemp);
}?>		
</form>


<?
	include("h_footer.php")
?>
<?
include("common.php");

$objSaleList = new MemberList();
$objSaleList->setFilter(" department = '4' ");
$objSaleList->setPageSize(0);
$objSaleList->setSortDefault(" nickname ASC");
$objSaleList->load();

$objRecieveMoneyBy = new MemberList();
$objRecieveMoneyBy->setPageSize(0);
$objRecieveMoneyBy->setSortDefault(" nickname ASC");
$objRecieveMoneyBy->load();

if ( $hDelete )  //Delete is checked.
{
	$objStockRedItem = new StockRedItem();
	$objStockRedItem->setStockRedItemId($hDelete);
	$objStockRedItem->delete();
}


$objStockRed = new StockRed();

if (empty($hSubmit)) {
	if ($hId !="" ) {
		$objStockRed->setStockRedId($hId);
		$objStockRed->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objStockRed->getStockDate());
		$arrTime = explode(" ",$arrDate[2]);
		$arrTime01 = explode(":",$arrTime[1]);
		
		$Day = $arrTime[0];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$Hours03 = $arrTime01[0];
		$Minute03 = $arrTime01[1];
		
		$arrDate = explode("-",date("Y-m-d"));
		$arrTime = explode(":",date("H:i:s"));
		
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$Hours01 = $arrTime[0];
		$Minute01 = $arrTime[1];
		
		$arrDate = explode("-",date("Y-m-d"));
		$arrTime = explode(":",date("H:i:s"));
		
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$Hours02 = $arrTime[0];
		$Minute02 = $arrTime[1];
		
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$Hours03 = $arrTime[0];
		$Minute03 = $arrTime[1];		
		
		
	} else {
		$strMode="Add";		
		$arrDate = explode(" ",date("Y-m-d H:i:s"));
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrTime[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Day03 = $arrDay[2];
		$Month03 = $arrDay[1];
		$Year03 = $arrDay[0];
		
		$Hours03 = $arrTime01[0];
		$Minute03 = $arrTime01[1];
		
	}

} else {
	if (!empty($hSubmit)) {
            $objStockRed->setStockRedId($hStockRedId);
			$hStockDate = $Year."-".$Month."-".$Day." ".$Hours03.":".$Minute03.":00";
			$objStockRed->setStockDate($hStockDate);
			$objStockRed->setStockBy($sMemberId);
			$objStockRed->setStockName($hStockName);
			$objStockRed->setStatus($hStatus);
			$objStockRed->setPrice($hPrice);
			$objStockRed->setReal($hReal);
			
    		$pasrErr = $objStockRed->check($hSubmit);

			If ( Count($pasrErr) == 0 ){
				if ($strMode=="Update") {
					$objStockRed->update();
					unset ($objStockRed);	
					header("location:stockRedList.php");
					exit;
				} else {
					$pId=$objStockRed->add();
					//header("location:stockRedUpdate.php");
					//exit;
					$objStockRed->setStockDate($hStockDate);
					$objStockRed->setStockBy($sMemberId);
					$objStockRed->setStockName($hStockName);
					$objStockRed->setStatus(0);
					header("location:stockRedList.php");
					exit;
				}
				

			}else{
				$objStockRed->init();
			}//end check Count($pasrErr)
		}
}


if($hTransitIn){
		$objStockRedIn = new StockRedItem();
		$objStockRedIn->setStockRedItemId($hStockRedItemId);
		$objStockRedIn->setStockRedId($hStockRedId);
		$hStockDate = $Year01."-".$Month01."-".$Day01;
		$objStockRedIn->setStockDate($hStockDate);
		$hStockTime = $Hours01.":".$Minute01.":0";
		$objStockRedIn->setStockTime($hStockTime);
		$objStockRedIn->setStockBy($sMemberId);
		$objStockRedIn->setStockNumber($hStockNumber);
		$objStockRedIn->setStockReasonId($hReasonId);
		$objStockRedIn->setSaleId($hSaleId);
		$objStockRedIn->setStockCarId($hStockCarId);
		$objStockRedIn->setRemark($hRemark);
		$objStockRedIn->setPrice($hPrice);
		$objStockRedIn->setStatus(1);
		$objStockRedIn->add();
		
		//update parking
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStockCarId($hStockCarId);
		$objStockRed->updateStockCarId();
		
		//update complete
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStatus(1);
		$objStockRed->updateStatus();
		
		header("location:stockRedUpdate.php?hId=$hStockRedId");
}

if($hTransitOut){
		$objStockRedOut = new StockRedItem();
		$objStockRedOut->setStockRedItemId($hStockRedItemId);
		$objStockRedOut->setStockNumberReturn($hStockNumberReturn);
		$objStockRedOut->setStockRedId($hStockRedId);
		$hStockDateReturn = $Year02."-".$Month02."-".$Day02;
		$objStockRedOut->setStockDateReturn($hStockDateReturn);
		$hStockTimeReturn = $Hours02.":".$Minute02.":00";
		$objStockRedOut->setStockTimeReturn($hStockTimeReturn);
		$objStockRedOut->setStockByReturn($sMemberId);
		$objStockRedOut->setSaleIdReturn($hSaleId);
		$objStockRedOut->setStockBlack($hStockBlack);
		$objStockRedOut->setRecieveBy($hSaleId);
		$objStockRedOut->setRecieveMoneyBy($hRecieveMoneyBy);
		$objStockRedOut->setRemarkReturn($hRemarkReturn);
		if($hReturnStatus ==1){
		$objStockRedOut->setStatus(0);
		}else{
		$objStockRedOut->setStatus(1);
		}
		$objStockRedOut->setReturnNumber($hReturnNumber);
		$objStockRedOut->setReturnStatus($hReturnStatus);
		$objStockRedOut->setRecieveType($hRecieveType02);
		$objStockRedOut->update();
		
		if($hReturnStatus ==1){
		//update parking
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStockCarId(0);
		$objStockRed->updateStockCarId();
		
		//update complete
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStatus(0);
		$objStockRed->updateStatus();	
		}
		//update registry
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($hStockCarId);
		$objStockCar->load();
				
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
				
		$objOrder = new Order();
		$objOrder->setOrderId($hOrderId);
		$hStockDateReturn01 = $Year03."-".$Month03."-".$Day03;
		$objOrder->setRegistryDate05($hStockDateReturn01);
		$objOrder->setRegistryText05($objMember->getFirstname()."  ".$objMember->getLastname());
		$objOrder->setRegistryRecieveName01($hRecieveStockBlack);
		$objOrder->setRegistryRecieveType01($hRegistryRecieveType01);
		$objOrder->updateRegistryBlackCode();
		
		header("location:stockRedList.php");
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Toyota Buzz  : Cashier System</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-874" />
	<meta http-equiv="imagetoolbar" content="no" />
	<link rel="stylesheet" href="../css/element_report_8_border.css">
	<SCRIPT language=javascript src="../script/functions.js" type=text/javascript></SCRIPT>
</head>
<body>
<div align="center"><strong><strong>PER01</strong> ��§ҹ����Ѻ����¹����ᴧ</strong></div>

<table width="700" cellpadding="3" cellspacing="0" class="search" align="center">
<tr>
	<td width="20%" class="listDetail">�����Ţ����¹����ᴧ</td>
	<td width="30%" class="listDetail"><?=$objStockRed->getStockName()?></td>
	<td width="20%" class="listDetail">�ѹ���ҷ���Ѻ�������ʵ�ͤ</td>
	<td width="30%" class="listDetail">&nbsp;<?=$Day."/".$Month."/".$Year;?></td>
</tr>
<tr>
	<td class="listDetail">�Ҥҷع����ᴧ</td>
	<td class="listDetail"><?=$objStockRed->getPrice()?></td>
	<td class="listDetail">����������ᴧ</td>
	<td class="listDetail"><br>
	<?if($objStockRed->getReal() == 1) echo "����ᴧ�ҡ����";?>
	<?if($objStockRed->getReal() == 2) echo "����ᴧ Buzz";?>
	<?if($objStockRed->getReal() == 3) echo "����ᴧ�ҡ�١���";?>
	</td>
</tr>
<tr>
	<td class="listDetail">ʶҹТͧ���»Ѩ�غѹ</td>
	<td colspan="3" class="listDetail">&nbsp;
			<?=$objStockRed->getStatusDetail()?> &nbsp;&nbsp;
			
			<?if($objStockRed->getStatus() == 1){
				$objOrder = new Order();
				if($objOrder->loadByCondition(" stock_car_id = ".$objStockRed->getStockCarId()) ){
				echo "���㺨ͧ�Ţ��� : ".$objOrder->getOrderNumber()."";
				$objCustomer = new Customer();
				$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
				$objCustomer->load();
				echo " �ͧ�١��Ҫ���:".$objCustomer->getFirstname()." ".$objCustomer->getLastname();			
				}	
			}?>			
	
	</td>

</tr>
<tr>
	<td colspan="4" class="listTitle">����¹����ѵԻ���ᴧ</td>
</tr>

<?
$objStockRedItemList = new StockRedItemList();
$objStockRedItemList->setFilter(" stock_red_id = $hId  ");
$objStockRedItemList->setPageSize(0);
$objStockRedItemList->setSortDefault(" stock_date DESC");
$objStockRedItemList->load();
?>

<tr>

		
		<td colspan="2" align="center"  class="listDetail02"><strong>��¡���ԡ</strong></td>
		<td colspan="2"  align="center" class="listDetail02"><strong>��¡�ä׹</strong></td>
		
</tr>
	<?
		$i=0;
		forEach($objStockRedItemList->getItemList() as $objItem) {
		$objSale = new Member();
		$objSale->setMemberId($objItem->getSaleId());
		$objSale->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->getStockBy());
		$objMember->load();
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objItem->getStockCarId());
		$objStockCar->load();
		
		$objOrder = new Order();
		$objOrder->setOrderId($objStockCar->getOrderId());
		$objOrder->load();
		
		
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
		$objCustomer->load();
		
		if(fmod($i,2) == 0){
			$bgColor= "e0e0e0";
		}else{
			$bgColor= "f2f2f2";
		}
		
	?>		
		<tr>			
			<td  class="listDetail" valign="top" colspan="2" >
			<table width="100%">
			<tr>
				<td>�ѹ���ҷ���ԡ</td>
				<td>:</td>
				<td><?=$objItem->getStockDate()?> <?=$objItem->getStockTime()?></td>
				<td>Sale</td>
				<td>:</td>
				<td><?=$objSale->getFirstname()?> (<?=$objSale->getNickname()?>)</td>
			</tr>
			<tr>
				<td>�١���</td>
				<td>:</td>
				<td><?=$objCustomer->getFirstname()."  ".$objCustomer->getLastname()?></td>
				<td>�����Ţ�ѧ</td>
				<td>:</td>
				<td><?=$objStockCar->getCarNumber()?></td>
			</tr>
			<tr>
				<td>�Ţ��������</td>
				<td>:</td>
				<td><?=$objOrder->getSendingNumber()?></td>
				<td>�Ҥ��Ѵ��</td>
				<td>:</td>
				<td><?=number_format($objItem->getPrice(),2)?></td>
			</tr>
			</table>		
			
			</td>			

			<?if($objItem->getStatus() == 1){?>
			<td  class="listDetail" valign="top" align="center" colspan="2"><font color=red><strong>�ѧ�������¡�ä׹����ᴧ</strong></font></td>
			
			<?}else{
		$objSale = new Member();
		$objSale->setMemberId($objItem->getSaleIdReturn());
		$objSale->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->getStockByReturn());
		$objMember->load();
		
		$objRecieveMoney = new Member();
		$objRecieveMoney->setMemberId($objItem->getStockByReturn());
		$objRecieveMoney->load();		
			?>
			<td  colspan="2" class="listDetail" valign="top" >
			<table width="100%">
			<tr>
				<td width="20%">�ѹ���ҷ���Ѻ�׹</td>
				<td>:</td>
				<td width="30%"><?=$objItem->getStockDateReturn()?> <?=$objItem->getStockTimeReturn()?></td>
				<td width="20%">������Ţ���</td>
				<td>:</td>
				<td width="30%"><?=$objItem->getReturnNumber()?></td>
			</tr>
			<tr>
				<td>����Ѻ�׹</td>
				<td>:</td>
				<td><?=$objMember->getFirstname()."  ".$objMember->getLastname()?></td>
				<td>����������Ѻ�׹</td>
				<td>:</td>
				<td><?=$objItem->getRecieveType()?></td>
			</tr>
			<tr>
				<td>����¹���´�</td>
				<td>:</td>
				<td><?=$objItem->getStockBlack()?></td>
				<td>Sale</td>
				<td>:</td>
				<td><?=$objSale->getFirstname()?> (<?=$objSale->getNickname()?>)</td>
			</tr>
			</table>		
			
			</td>		
			<?}?>
			
		</tr>
		<?$i++;}?>
		</table>
	</td>
</tr>
</table>

</body>
</html>
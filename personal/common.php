<?
include("../conf_inside.php");
define("URL_BASE","personal");
	
include(PATH_CLASS."clUser.php");
include(PATH_CLASS."clAdmin.php");
include(PATH_CLASS."clMember.php");
include(PATH_CLASS."clModule.php");
include(PATH_CLASS."clMemberModule.php");
include(PATH_CLASS."clMemberLog.php");
include(PATH_CLASS."clCustomerGroup.php");
include(PATH_CLASS."clCustomerGrade.php");
include(PATH_CLASS."clCustomerEvent.php");
include(PATH_CLASS."clContact.php");
include(PATH_CLASS."clCustomer.php");
include(PATH_CLASS."clCustomerContact.php");
include(PATH_CLASS."clCustomerDuplicate.php");
include(PATH_CLASS."clCustomerTitle.php");

include(PATH_CLASS."clSale.php");
include(PATH_CLASS."clCarType.php");
include(PATH_CLASS."clCarModel.php");
include(PATH_CLASS."clCarSeries.php");
include(PATH_CLASS."clCarColor.php");
include(PATH_CLASS."clCustomerExpectCar.php");
include(PATH_CLASS."clCustomerUseCar.php");
include(PATH_CLASS."clCarPremium.php");
include(PATH_CLASS."clStockPremium.php");
include(PATH_CLASS."clStockPremiumItem.php");
include(PATH_CLASS."clStockCar.php");
include(PATH_CLASS."clStockCarItem.php");
include(PATH_CLASS."clStockReason.php");

include(PATH_CLASS."clAreaProvince.php");
include(PATH_CLASS."clAreaAmphur.php");
include(PATH_CLASS."clAreaTumbon.php");
include(PATH_CLASS."clAreaPostcode.php");

include(PATH_CLASS."clStockRed.php");
include(PATH_CLASS."clInfo.php");
include(PATH_CLASS."clStockRedItem.php");

include(PATH_CLASS."clOrder.php");
include(PATH_CLASS."clOrderStock.php");
include(PATH_CLASS."clOrderStockOther.php");
include(PATH_CLASS."clOrderPayment.php");
include(PATH_CLASS."clOrderPrice.php");
include(PATH_CLASS."clOrderExtra.php");
include(PATH_CLASS."clPaymentType.php");
include(PATH_CLASS."clPaymentSubject.php");
include(PATH_CLASS."clBank.php");
include(PATH_CLASS."clCashierReserve.php");
include(PATH_CLASS."clCashierReserveItem.php");

include(PATH_CLASS."clCompany.php");
include(PATH_CLASS."clStockCargo.php");
include(PATH_CLASS."clStockPlace.php");
include(PATH_CLASS."clStockProductType.php");
include(PATH_CLASS."clStockProductGroup.php");
include(PATH_CLASS."clStockProductPack.php");
include(PATH_CLASS."clStockPaper.php");
include(PATH_CLASS."clStockProduct.php");
include(PATH_CLASS."clStock.php");
include(PATH_CLASS."clStockItem.php");
include(PATH_CLASS."clDealer.php");

include(PATH_CLASS."clFundCompany.php");
include(PATH_CLASS."clInsureCompany.php");
include(PATH_CLASS."clPrbCompany.php");


include(PATH_CLASS."clMonitor.php");
include(PATH_CLASS."clMsg.php");
include(PATH_CLASS."clMsgReply.php");
include(PATH_CLASS."clLabelSetting.php");

include(PATH_CLASS."clLabel.php");
include(PATH_CLASS."utilityFunction.php");
include(PATH_CLASS."incUtility.php");

include(PATH_CLASS."clDepartment.php");
include(PATH_CLASS."clTeam.php");
include(PATH_CLASS."clErr.php");

include(PATH_CLASS."clMemberHistory.php");

define("PATH_WEB",PATH_BASE."cashier/");

$objUser = new Member();
if (!$objUser->isLogged()){
	header("Location: ../index.php?hMsg=session+expire");
	exit;
}else{
	//check right access
	if( $sRightAccessCrl == "0:0:0:0:0:0" ){
		header("Location: ../index.php?hMsg=������Է����ҹ����� ��سҵԴ��ͼ������к�");
		exit;
	}
}

Include(PATH_INCLUDE."utilityString.php");


$currentFile = $_SERVER["PHP_SELF"];
$parts = Explode('/', $currentFile);
if(strpos($parts[count($parts) - 1],"Auto")){
	header('Content-Type: text/html; charset=utf-8');
}

if(strpos($parts[count($parts) - 1],"auto_")){
	header('Content-Type: text/html; charset=utf-8');
}



?>
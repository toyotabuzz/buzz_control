<?
include "common.php";

$objStockCarList = new StockCarList();
$objStockCarList->setFilter(" car_number LIKE '%".trim($q)."%'   ");
$objStockCarList->setPageSize(0);
$objStockCarList->setSortDefault(" car_number ASC");
$objStockCarList->loadUTF8();

forEach($objStockCarList->getItemList() as $objItem) {
	$objOrder = new Order();
	$objOrder->setOrderId($objItem->getOrderId());
	$objOrder->load();
	
	$objCustomer = new Customer();
	$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
	$objCustomer->loadUTF8();
?>
<div onSelect="this.text.value='<?=$objItem->getCarNumber()?>';document.frm02.hStockCarId.value='<?=$objItem->getStockCarId()?>';document.frm02.hPrice.value='<?=$objOrder->getRedCodePrice()?>';document.frm02.hCustomerName.value='<?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?>';"><span class='informal'></span><span><?=$objItem->getCarNumber()?>,<?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?></span></div>
<?}

unset($objStockCarList);
?>
<?
include("common.php");

$objSaleList = new MemberList();
$objSaleList->setFilter(" department = '4' ");
$objSaleList->setPageSize(0);
$objSaleList->setSortDefault(" nickname ASC");
$objSaleList->load();

$objRecieveMoneyBy = new MemberList();
$objRecieveMoneyBy->setPageSize(0);
$objRecieveMoneyBy->setSortDefault(" nickname ASC");
$objRecieveMoneyBy->load();

if ( $hDelete )  //Delete is checked.
{
	$objStockRedItem = new StockRedItem();
	$objStockRedItem->setStockRedItemId($hDelete);
	$objStockRedItem->delete();
}


$objStockRed = new StockRed();

if (empty($hSubmit)) {
	if ($hId !="" ) {
		$objStockRed->setStockRedId($hId);
		$objStockRed->load();
		$strMode="Update";
		
		$arrDate = explode("-",$objStockRed->getStockDate());
		$arrTime = explode(" ",$arrDate[2]);
		$arrTime01 = explode(":",$arrTime[1]);
		
		$Day = $arrTime[0];
		$Month = $arrDate[1];
		$Year = $arrDate[0];
		
		$Hours03 = $arrTime01[0];
		$Minute03 = $arrTime01[1];
		
		$arrDate = explode("-",date("Y-m-d"));
		$arrTime = explode(":",date("H:i:s"));
		
		$Day01 = $arrDate[2];
		$Month01 = $arrDate[1];
		$Year01 = $arrDate[0];
		
		$Hours01 = $arrTime[0];
		$Minute01 = $arrTime[1];
		
		$arrDate = explode("-",date("Y-m-d"));
		$arrTime = explode(":",date("H:i:s"));
		
		$Day02 = $arrDate[2];
		$Month02 = $arrDate[1];
		$Year02 = $arrDate[0];
		
		$Hours02 = $arrTime[0];
		$Minute02 = $arrTime[1];
		
		$Day03 = $arrDate[2];
		$Month03 = $arrDate[1];
		$Year03 = $arrDate[0];
		
		$Hours03 = $arrTime[0];
		$Minute03 = $arrTime[1];		
		
		
	} else {
		$strMode="Add";		
		$arrDate = explode(" ",date("Y-m-d H:i:s"));
		$arrDay = explode("-",$arrDate[0]);
		$arrTime = explode(":",$arrTime[1]);
		
		$Day = $arrDay[2];
		$Month = $arrDay[1];
		$Year = $arrDay[0];
		
		$Day03 = $arrDay[2];
		$Month03 = $arrDay[1];
		$Year03 = $arrDay[0];
		
		$Hours03 = $arrTime01[0];
		$Minute03 = $arrTime01[1];
		
	}

} else {
	if (!empty($hSubmit)) {
            $objStockRed->setStockRedId($hStockRedId);
			$hStockDate = $Year."-".$Month."-".$Day." ".$Hours03.":".$Minute03.":00";
			$objStockRed->setStockDate($hStockDate);
			$objStockRed->setStockBy($sMemberId);
			$objStockRed->setStockName($hStockName);
			$objStockRed->setStatus($hStatus);
			$objStockRed->setPrice($hPrice);
			$objStockRed->setReal($hReal);
			$objStockRed->setCompanyId($sCompanyId);
			
    		$pasrErr = $objStockRed->check($strMode);

			If ( Count($pasrErr) == 0 ){
				if ($strMode=="Update") {
					$objStockRed->update();
					unset ($objStockRed);	
					header("location:stockRedList.php?hStatus=$hAStatus&hKeyword=$hAKeyword&hPage=$hAPage&hBlackCode=$hABlackCode&hCustomer=$hACustomer&hSendingNumber=$hASendingNumber&hSearch=$hASearch");
					exit;
				} else {
					$pId=$objStockRed->add();
					//header("location:stockRedUpdate.php");
					//exit;
					$objStockRed->setStockDate($hStockDate);
					$objStockRed->setStockBy($sMemberId);
					$objStockRed->setStockName($hStockName);
					$objStockRed->setStatus(0);
					header("location:stockRedList.php?hStatus=$hAStatus&hKeyword=$hAKeyword&hPage=$hAPage&hBlackCode=$hABlackCode&hCustomer=$hACustomer&hSendingNumber=$hASendingNumber&hSearch=$hASearch");
					exit;
				}
				

			}else{
				$objStockRed->init();
			}//end check Count($pasrErr)
		}
}


if($hTransitIn){
		$objStockRedIn = new StockRedItem();
		$objStockRedIn->setStockRedItemId($hStockRedItemId);
		$objStockRedIn->setStockRedId($hStockRedId);
		$objStockRedIn->setOrderId($hOrderId);
		$hStockDate = $Year01."-".$Month01."-".$Day01;
		$objStockRedIn->setStockDate($hStockDate);
		$hStockTime = $Hours01.":".$Minute01.":0";
		$objStockRedIn->setStockTime($hStockTime);
		$objStockRedIn->setStockBy($sMemberId);
		$objStockRedIn->setStockNumber($hStockNumber);
		$objStockRedIn->setStockReasonId($hReasonId);
		$objStockRedIn->setSaleId($hSaleId);
		$objStockRedIn->setStockCarId($hStockCarId);
		$objStockRedIn->setRemark($hRemark);
		$objStockRedIn->setPrice($hPrice);
		$objStockRedIn->setStatus(1);
		$objStockRedIn->add();
		
		//update parking
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStockCarId($hStockCarId);
		$objStockRed->updateStockCarId();
		
		//update complete
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStatus(1);
		$objStockRed->updateStatus();
		
		//update order stock_red_id
		$objOrder = new Order();
		$objOrder->setOrderId($hOrderId);
		$objOrder->setStockRedId($hStockRedId);
		$objOrder->updateStockRed();
		
		header("location:stockRedUpdate.php?hId=$hStockRedId&hAStatus=$hAStatus&hAKeyword=$hAKeyword&hAPage=$hAPage&hABlackCode=$hABlackCode&hACustomer=$hACustomer&hASendingNumber=$hASendingNumber&hASearch=$hASearch");
}

if($hTransitOut){


		$objStockRedOut = new StockRedItem();
		$objStockRedOut->setStockRedItemId($hStockRedItemId);
		$objStockRedOut->setStockNumberReturn($hStockNumberReturn);
		$objStockRedOut->setStockRedId($hStockRedId);
		$hStockDateReturn = $Year02."-".$Month02."-".$Day02;
		$objStockRedOut->setStockDateReturn($hStockDateReturn);
		$hStockTimeReturn = $Hours02.":".$Minute02.":00";
		$objStockRedOut->setStockTimeReturn($hStockTimeReturn);
		$objStockRedOut->setStockByReturn($sMemberId);
		$objStockRedOut->setSaleIdReturn($hSaleId);
		$objStockRedOut->setStockBlack($hStockBlack);
		$objStockRedOut->setRecieveBy($hSaleId);
		$objStockRedOut->setRecieveMoneyBy($hRecieveMoneyBy);
		$objStockRedOut->setRemarkReturn($hRemarkReturn);
		if($hReturnStatus ==1 OR $hReturnStatus == 3){
		$objStockRedOut->setStatus(0);
		}else{
		$objStockRedOut->setStatus(1);
		}
		$objStockRedOut->setReturnNumber($hReturnNumber);
		$objStockRedOut->setReturnStatus($hReturnStatus);
		$objStockRedOut->setRecieveType($hRecieveType02);
		$objStockRedOut->update();
		
		if($hReturnStatus ==1){
		//update parking
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStockCarId(0);
		$objStockRed->updateStockCarId();
		
		//update complete
		$objStockRed = new StockRed();
		$objStockRed->setStockRedId($hStockRedId);
		$objStockRed->setStatus(0);
		$objStockRed->updateStatus();	
		
		//update registry status
		$objOrder = new Order();
		$objOrder->setOrderId($hOrderId);
		$objOrder->setRegistryStatus(5);
		$objOrder->updateRegistryStatus();
		
		}
		
		if($hReturnStatus==3){
			if($hOrderId > 0) {
				$objOrder = new Order();
				$objOrder->setOrderId($hOrderId);
				$objOrder->setStockRedId($hStockRedId);
				$objOrder->updateStockRed();
			}
		}
		
		//update registry
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($hStockCarId);
		$objStockCar->load();
				
		$objMember = new Member();
		$objMember->setMemberId($sMemberId);
		$objMember->load();
				
		$objOrder = new Order();
		$objOrder->setOrderId($hOrderId);
		$hStockDateReturn01 = $Year03."-".$Month03."-".$Day03;
		$objOrder->setRegistryDate05($hStockDateReturn01);
		$objOrder->setRegistryText05($objMember->getFirstname()."  ".$objMember->getLastname());
		$objOrder->setRegistryRecieveName01($hRecieveStockBlack);
		$objOrder->setRegistryRecieveType01($hRegistryRecieveType01);
		$objOrder->updateRegistryBlackCode();
		
		header("location:stockRedList.php?hStatus=$hAStatus&hKeyword=$hAKeyword&hPage=$hAPage&hBlackCode=$hABlackCode&hCustomer=$hACustomer&hSendingNumber=$hASendingNumber&hSearch=$hASearch");
}

$pageTitle = "2. �к�ʵ�ͤ����ᴧ";
$pageContent = "2.1  �ѹ�֡������ʵ�ͤ����ᴧ";
$strHead03 = "�ѹ�֡������ʵ�ͤ����ᴧ";
include("h_header.php");
?>
<script language="JavaScript">
	function check_submit_in()
	{

		if (document.forms.frm02.hSaleId.value=="0")
		{
			alert("��س����͡��ѡ�ҹ��¨ҡ�к�");
			document.forms.frm02.hSaleName.focus();
			return false;
		} 		

		if ( document.frm02.hReturnStatus[0].checked == true && document.forms.frm02.hReturnNumber.value== "" )
		{
			alert("��س��к��Ţ�����¤׹�Թ�Ѵ�ӻ���ᴧ");
			document.forms.frm02.hReturnNumber.focus();
			return false;
		} 		
	
		if ( document.frm02.hReturnStatus[2].checked == true && document.forms.frm02.hNewStockRedId.value== "0" )
		{
			alert("��س��к��Ţ����¹����ᴧ����");
			document.forms.frm02.hNewStockName.focus();
			return false;
		} 		

	}
	
	
	function check_submit_out()
	{

		if (document.forms.frm02.hSaleId.value=="")
		{
			alert("��س����͡��ѡ�ҹ��¨ҡ�к�");
			document.forms.frm02.hSaleName.focus();
			return false;
		} 		
	
		if (document.forms.frm02.hStockCarId.value=="")
		{
			alert("��س����͡�����Ţ�ѧ");
			document.forms.frm02.hStockCarNumber.focus();
			return false;
		} 			
	
	}	
</script>
<DIV id=popCal style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
<IFRAME name=popFrame src="<?=PATH_INCLUDE."popcjs.htm"?>" frameBorder=0 width=165 scrolling=no height=155></IFRAME></DIV>
<SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
<br>
<?if(sizeof($pasrErr) > 0  ){?>
	<table cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td width="4"><img src="../images/box/white_top_left.gif" alt="" width="4" height="4" bstock="0"></td>
		<td background="../images/box/white_top.gif"><img src="../images/box/white_top.gif" alt="" width="1" height="4" bstock="0"></td>
		<td width="4"><img src="../images/box/white_top_right.gif" alt="" width="4" height="4" bstock="0"></td>
	</tr>
	<tr>
		<td width="4" background="../images/box/white_middle_left.gif"><img src="../images/box/white_middle_left.gif" alt="" width="4" height="1" bstock="0"></td>
		<td width="100%">
			<table width="100%">
			<tr>
				<td valign="top"><img src="../images/box/errormessage.gif" alt="" width="43" height="39" bstock="0"></td>
				<td class="error" valign="top">
					<strong>Some error occur please check:</strong><br> 
					<table>
					<?foreach ($pasrErr as $key => $value) {
					?>
					<tr>
						<td class="error">- <?echo "$value";?></td>
					</tr>
					<?}?>					
					</table>										
				</td>
			</tr>
			</table>							
		</td>
		<td width="4" background="../images/box/white_middle_right.gif"><img src="../images/box/white_middle_right.gif" alt="" width="4" height="1" bstock="0"></td>
	</tr>
	<tr>
		<td width="4"><img src="../images/box/white_bottom_left.gif" alt="" width="4" height="4" bstock="0"></td>
		<td background="../images/box/white_bottom.gif"><img src="../images/box/white_bottom.gif" alt="" width="1" height="4" bstock="0"></td>
		<td width="4"><img src="../images/box/white_bottom_right.gif" alt="" width="4" height="4" bstock="0"></td>
	</tr>
	</table>				
<?}?>
<form name="frm01" action="stockRedUpdate.php" method="post" onsubmit="return check_submit();">
	  <input type="hidden" name="strMode" value="<?=$strMode?>">
	  <input type="hidden" name="hId" value="<?=$hId?>">
	  <input type="hidden" name="hStockRedId" value="<?=$hId?>">
	  <input type="hidden" name="hAStatus" value="<?=$hAStatus?>">
	  <input type="hidden" name="hAKeyword" value="<?=$hAKeyword?>">
	  <input type="hidden" name="hAPage" value="<?=$hAPage?>">
	  <input type="hidden" name="hABlackCode" value="<?=$hBlackCode?>">
	  <input type="hidden" name="hACustomer" value="<?=$hACustomer?>">
	  <input type="hidden" name="hASendingNumber" value="<?=$hASendingNumber?>">
	  <input type="hidden" name="hASearch" value="<?=$hASearch?>">
<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td  class="i_background03" valign="top"><strong>�����Ţ����¹����ᴧ</strong> (<font color=red>*</font>)</td>
			<td valign="top"><input type="text" name="hStockName" size="30" maxlength="17"  value="<?=$objStockRed->getStockName()?>"></td>
			<td width="20%" class="i_background03"><strong>�ѹ���ҷ���Ѻ�������ʵ�ͤ</strong></td>
			<td width="30%" >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"   name=Day value="<?=$Day?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month value="<?=$Month?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4" onblur="checkYear(this,this.value);"  name=Year value="<?=$Year?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onclick="popFrame.fPopCalendar(Year,Day, Month, Year,popCal);return false"></td>					
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>					
					<td><INPUT align="middle" size="2"  maxlength="2"  name=Hours03 value="<?=$Hours03?>"></td>
					<td width="2" align="center">:</td>
					<td><INPUT align="middle" size="2"  maxlength="2"  name=Minute03 value="<?=$Minute03?>"></td>
				</tr>
				</table>	
			</td>
		</tr>		
		<tr>			
			<td  class="i_background03" valign="top"><strong>�Ҥҷع����ᴧ</strong> (<font color=red>*</font>)</td>
			<td valign="top"><input type="text" name="hPrice" size="10" maxlength="17"  value="<?=$objStockRed->getPrice()?>"> �ҷ</td>
			<td width="20%" class="i_background03" valign="top" ><strong>����������ᴧ</strong> (<font color=red>*</font>)</td>
			<td width="30%" ><input type="radio" name="hReal" value=1 <?if($objStockRed->getReal() == 1) echo "checked";?>>����ᴧ�ҡ����&nbsp;&nbsp;<input type="radio" name="hReal" value=2  <?if($objStockRed->getReal() == 2) echo "checked";?>>����ᴧ buzz&nbsp;&nbsp;<br><input type="radio" name="hReal" value=3  <?if($objStockRed->getReal() == 3) echo "checked";?>>���¨ҡ�١���</td>
		</tr>		
		<tr>			
			<td  class="i_background03" valign="top"><strong>ʶҹл���ᴧ</strong> (<font color=red>*</font>)</td>
			<td valign="top">
				<select name="hStatus">
					<option value=0 <?if($objStockRed->getStatus() == 0) echo "selected"?>>��ҧ
					<option value=1 <?if($objStockRed->getStatus() == 1) echo "selected"?>>�����ҧ
				</select>
			
			</td>
		</tr>		
		<?if ($strMode == "Update"){?>
		<tr>			
			<td  class="i_background03"><strong>ʶҹТͧ���»Ѩ�غѹ</strong></td>
			<td colspan="3" class="error" >
			<?=$objStockRed->getStatusDetail()?> &nbsp;&nbsp;
			
			<?if($objStockRed->getStatus() == 1 AND $objStockRed->getStockCarId() > 0){
				$objOrder = new Order();
				if($objOrder->loadByCondition(" stock_car_id = ".$objStockRed->getStockCarId()) ){
				echo "���㺨ͧ�Ţ��� : <a href='ccardView.php?hId=".$objOrder->getOrderId()."' target='_blank'>".$objOrder->getOrderNumber()."</a>";
				$objCustomer = new Customer();
				$objCustomer->setCustomerId($objOrder->getBookingCustomerId());
				$objCustomer->load();
				echo " �ͧ�١��Ҫ���:".$objCustomer->getFirstname()." ".$objCustomer->getLastname();			
				}	
			}?>		
			
			</td>
		</tr>
		<?}?>				
		<tr>
			<td colspan="4" class="i_background03" align="center">
			<br><br>
        
		<?if ($strMode == "Update"){?>
			<input type="submit" name="hSubmit1" value="�����¡��" class="button" >
		<?}else{?>
			<input type="submit" name="hSubmit1" value="������¡��" class="button" >
		<?}?>
		<input type="hidden" name="hSubmit" value="<?=$strMode?>">&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" name="hCancel" value="¡��ԡ������" class="button" onclick="window.location='stockRedList.php?hStatus=<?=$hAStatus?>&hKeyword=<?=$hAKeyword?>&hPage=<?=$hAPage?>&hBlackCode=<?=$hABlackCode?>&hCustomer=<?=$hACustomer?>&hSendingNumber=<?=$hASendingNumber?>&hSearch=<?=$hASearch?>';" >
			<br><br>
			</td>
		</tr>
		</table>
	</td>
</tr>
</form>
</table>

<?if($strMode == "Update"){
$objStockRedItemList = new StockRedItemList();
$objStockRedItemList->setFilter(" stock_red_id = $hId and status= 1 ");
$objStockRedItemList->setPageSize(0);
$objStockRedItemList->setSortDefault(" stock_date DESC");
$objStockRedItemList->load();
?>
<table width="100%" class="i_background05">
<tr>
	<td>���ҧ��¡���ԡ/�Ѻ�׹����ᴧ</td>
</tr>
</table>
<table width="100%" class="i_background05">
<tr>
	<td align="center" width="50%"><strong>��¡���ԡ����ᴧ</strong></td>
	<td align="center"><strong>��¡���Ѻ�׹����ᴧ ���ͺ���´�</strong></td>
</tr>
<tr>
	<td valign="top">
<?if($objStockRedItemList->mCount > 0){
	$objStockRedItem = new StockRedItem();
	$objStockRedItem->loadByCondition(" stock_red_id=$hId AND status=1");
	
	$arrDate = explode("-",$objStockRedItem->getStockDate());
	$Day01 = $arrDate[2];
	$Month01 = $arrDate[1];
	$Year01 = $arrDate[0];
	
	$arrTime = explode(":",$objStockRedItem->getStockTime());
	$Hours01 = $arrTime[0];
	$Minute01 = $arrTime[1];

	
?>


<table width="100%" class="i_background02">
<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td class="i_background03"><strong>�Ţ�����ԡ</strong></td>
			<td >	
				<input type="text" readonly  name="hStockNumber" size="30"  value="<?=$objStockRedItem->getStockNumber()?>">
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>������Ѻ�Թ</strong></td>
			<td >
				<?
				$objStockCar = new StockCar();
				$objStockCar->loadByCondition(" stock_car_id = ".$objStockRedItem->getStockCarId());
				
				$objOrder = new Order();
				$objOrder->setOrderId($objStockCar->getOrderId());
				$objOrder->load();
				?>			
				<input type="text" readonly  name="hSale" size="30"  value="<?=$objOrder->getSendingNumberNohead()?>">
			</td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>�ѹ���ҷ���ԡ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" readonly size="2" maxlength="2"  name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" readonly size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" readonly size="4" maxlength="4"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><INPUT align="middle" size="2" readonly maxlength="2"  name=Hours01 value="<?=$Hours01?>"></td>
					<td width="2" align="center">:</td>
					<td><INPUT align="middle" size="2" readonly maxlength="2"  name=Minute01 value="<?=$Minute01?>"></td>					
				</tr>
				</table>	
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>�ԡ�� Sale</strong></td>
			<td >
				<?
				$objSale = new Member();
				$objSale->setMemberId($objStockRedItem->getSaleId());
				$objSale->load();
				?>			
				<input type="text" readonly  name="hSale" size="30"  value="<?=$objSale->getFirstname()." ".$objSale->getLastname()." (".$objSale->getNickname().") "?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѧ</strong></td>
			<?
			$objCustomer = new Customer();
			$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
			$objCustomer->load();
			?>
			<td ><input type="text" readonly  name="hStockCarNumber01" size="30"  value="<?=$objStockCar->getCarNumber()?>"></td>
		</tr>	
		<tr>			
			<td class="i_background03"><strong>�١���</strong></td>
			<td ><input type="text"readonly  name="hCustomerName" size="30"  value="<?=$objCustomer->getFirstname()." ".$objCustomer->getLastname()?>"></td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>�ӹǹ�Թ�Ѵ��</strong></td>
			<td ><input type="text" readonly name="hPrice" size="10"  value="<?=$objStockRedItem->getPrice()?>">�ҷ</td>
		</tr>				
		<tr>
			<td colspan="4" class="i_background03" align="center">

				&nbsp;
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>	


<?}else{?>	
	
<table width="100%" class="i_background02">
<form name="frm02" action="stockRedUpdate.php" method="post"  onsubmit="return check_submit_out();">
<input type="hidden" name="hTransitIn" value=1>
<input type="hidden" name="hStockRedId" value="<?=$hId?>">
<input type="hidden" name="hStockRedId" value="<?=$hId?>">
<input type="hidden" name="hAStatus" value="<?=$hAStatus?>">
<input type="hidden" name="hAKeyword" value="<?=$hAKeyword?>">
<input type="hidden" name="hAPage" value="<?=$hAPage?>">
<input type="hidden" name="hABlackCode" value="<?=$hBlackCode?>">
<input type="hidden" name="hACustomer" value="<?=$hACustomer?>">
<input type="hidden" name="hASendingNumber" value="<?=$hASendingNumber?>">
<input type="hidden" name="hASearch" value="<?=$hASearch?>">




<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td class="i_background03"><strong>�Ţ�����ԡ</strong></td>
			<td >				
				<input type="text" name="hStockNumber" size="30"    value="<?=$hStockNumber?>">
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>�ѹ���ҷ���ԡ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Day01 value="<?=$Day01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month01 value="<?=$Month01?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year01 value="<?=$Year01?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onkeydown="document.dataForm1.Hours.focus();" onclick="popFrame.fPopCalendar(Year01,Day01, Month01, Year01,popCal);return false;"></td>					
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>						
					<td><INPUT align="middle" size="2" maxlength="2"  name=Hours01 value="<?=$Hours01?>"></td>
					<td width="2" align="center">:</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Minute01 value="<?=$Minute01?>"></td>									
				</tr>
				</table>	
			</td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>�ԡ�� Sale</strong></td>
			<td >
				<input type="hidden" readonly size="3" name="hSaleId"  value="">
				<input type="text" name="hSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm02.hSaleName.value != '' ) frm02.hStockCarNumber.focus();if(event.keyCode !=13 ) frm02.hSaleId.value='';"    value="<?=$hSaleName?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����Ţ�ѧ</strong></td>
			<td ><input type="hidden" readonly size="3" name="hStockCarId" value=""><input type="text" name="hStockCarNumber" size="30"  value="<?=$hOrderNumber?>"></td>
		</tr>		
		<tr>			
			<td class="i_background03"><strong>�١���</strong></td>
			<td ><input type="text" name="hCustomerName" readonly="" size="30"  value="<?=$hCustomerName?>"></td>
		</tr>
		<tr>			
			<td class="i_background03"><strong>�ӹǹ�Թ�Ѵ��</strong></td>
			<td ><input type="text" name="hPrice" size="10"  value="<?=$hPrice?>">�ҷ</td>
		</tr>		
		<tr>
			<td colspan="4" class="i_background03" align="center">
		        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
				<?if ($strMode == "Update"){?>
					<input type="submit" name="hSubmit1" value="�ѹ�֡��¡���ԡ" class="button" >
				<?}else{?>
					<input type="submit" name="hSubmit1" value="�ѹ�֡��¡���ԡ" class="button" >
				<?}?>
		        &nbsp;&nbsp;&nbsp;&nbsp;<input type="Reset" name="hSubmit" value="��૵������" class="button" >
			
			</td>
		</tr>
		</table>
	</td>
</tr>
</form>
</table>	
	
<?}?>	
	

	
	</td>
	<td valign="top">
<?
if($objStockRedItemList->mCount > 0){
	$objStockRedItem = new StockRedItem();
	$objStockRedItem->loadByCondition(" stock_red_id=$hId AND status=1");

	$objStockCar = new StockCar();
	$objStockCar->setStockCarId($objStockRedItem->getStockCarId());
	$objStockCar->load();
	
	$objOrder = new Order();
	$objOrder->setOrderId($objStockCar->getOrderId());
	$objOrder->load();
	
	$arrDay = explode("-",$objStockRedItem->getStockDateReturn());
	$arrTime = explode(":",$objStockRedItem->getStockTimeReturn());
	
	if($arrDay[2] != "00"){
	$Day02 = $arrDay[2];
	$Month02 = $arrDay[1];
	$Year02 = $arrDay[0];
	$Hours02 = $arrTime01[0];
	$Minute02 = $arrTime01[1];
	
	}
	

	
	$arrDay = explode("-",$objOrder->getRegistryDate05());
	
	$Day03 = $arrDay[2];
	$Month03 = $arrDay[1];
	$Year03 = $arrDay[0];

	
	$objSale = new Member();
	$objSale->setMemberId($objStockRedItem->getSaleIdReturn());
	$objSale->load();
	
	$objNewStockRed = new StockRed();
	$objNewStockRed->setStockRedId($objStockRedItem->getNewStockRedId());
	$objNewStockRed->load();
	
?>	
	
	
<table width="100%" class="i_background02">
<form name="frm02" action="stockRedUpdate.php" method="post" onsubmit="return check_submit_in();">
<input type="hidden" name="hTransitOut" value=1>
<input type="hidden" name="hStockRedId" value="<?=$hId?>">
<input type="hidden" name="hStockRedItemId" value="<?=$objStockRedItem->getStockRedItemId()?>">
<input type="hidden" name="hOrderId" value="<?=$objOrder->getOrderId()?>">

<input type="hidden" name="hAStatus" value="<?=$hAStatus?>">
<input type="hidden" name="hAKeyword" value="<?=$hAKeyword?>">
<input type="hidden" name="hAPage" value="<?=$hAPage?>">
<input type="hidden" name="hABlackCode" value="<?=$hBlackCode?>">
<input type="hidden" name="hACustomer" value="<?=$hACustomer?>">
<input type="hidden" name="hASendingNumber" value="<?=$hASendingNumber?>">
<input type="hidden" name="hASearch" value="<?=$hASearch?>">


<tr>
	<td>
		<table width="100%" cellpadding="2" cellspacing="0">
		<tr>			
			<td class="i_background03"><strong>ʶҹС������¹���·���¹</strong></td>
			<td >
				<input type="radio" id=radio1  name="hReturnStatus" value="1" <?if($objStockRedItem->getReturnStatus() == 1 OR $objStockRedItem->getReturnStatus() == 0) echo "checked"?>>�Ѻ����ᴧ���»��´�<br>
				<input type="radio" id=radio2  name="hReturnStatus" value="2" <?if($objStockRedItem->getReturnStatus() == 2) echo "checked"?>>���»��´��ѧ������Ѻ�׹����ᴧ<br>
				<input type="radio" id=radio3  name="hReturnStatus" value="3" <?if($objStockRedItem->getReturnStatus() == 3) echo "checked"?>>����¹����ᴧ Buzz �繻���ᴧ����
				<input type="hidden" name="hNewStockRedId" value=<?=$objNewStockRed->getStockRedId();?>>
				<br>�Ţ����¹����ᴧ���� <input type="text" name="hNewStockName" size="10" maxlength="17"  value="<?=$objNewStockRed->getStockName()?>">
			</td>
		</tr>				
		<tr>			
			<td class="i_background03"><strong>�Ţ���㺨��¤׹�Թ�Ѵ�ӻ���ᴧ</strong></td>
			<td >
				<input type="text"  name="hReturnNumber" size="30"  value="<?=$objStockRedItem->getReturnNumber()?>">
			</td>
		</tr>		
		<tr>
			<td class="i_background03"><strong>�ѹ���ҷ��׹����ᴧ</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Day02 value="<?=$Day02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month02 value="<?=$Month02?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year02 value="<?=$Year02?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onkeydown="document.dataForm1.Hours.focus();" onclick="popFrame.fPopCalendar(Year02,Day02, Month02, Year02,popCal);return false;"></td>					
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>						
					<td><INPUT align="middle" size="2" maxlength="2"  name=Hours02 value="<?=$Hours02?>"></td>
					<td width="2" align="center">:</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Minute02 value="<?=$Minute02?>"></td>									
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>����������Ѻ�׹����ᴧ</strong></td>
			<td><input type="radio" name="hRecieveType02" value="direct"  <?if($objStockRedItem->getRecieveType() == "direct" OR $objStockRedItem->getRecieveType() == "" ) echo "checked"?>>�١��Ҥ׹�ͧ&nbsp;&nbsp;<input type="radio" name="hRecieveType02" value="mail" <?if($objStockRedItem->getRecieveType() == "mail" ) echo "checked"?>>�����</td>
		</tr>		
		<tr>			
			<td class="i_background03"><strong>�׹�� Sale</strong>   (<font color=red>*</font>)</td>
			<td >
				<input type="hidden" readonly size="3" name="hSaleId"  value="<?=$objStockRedItem->getSaleIdReturn()?>">
				<input type="text" name="hSaleName" size="30"  onKeyDown="if(event.keyCode==13 && frm02.hSaleName.value != '' ) frm02.hStockBlack.focus();if(event.keyCode !=13 ) frm02.hSaleId.value='';"    value="<?=$objSale->getFirstname()."  ".$objSale->getLastname();?>">
			</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>����¹���´�</strong></td>
			<td ><input type="text"  name="hStockBlack" size="10" maxlength="10"  value="<?=$objOrder->getBlackCodeNumber()?>"></td>			
		</tr>
		<tr>
			<td class="i_background03"><strong>�ѹ������ͺ���´�</strong></td>
			<td >
			  	<table cellspacing="0" cellpadding="0">
				<tr>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Day03 value="<?=$Day03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="2" maxlength="2"  name=Month03 value="<?=$Month03?>"></td>
					<td>-</td>
					<td><INPUT align="middle" size="4" maxlength="4"  onblur="checkYear(this,this.value);"  name=Year03 value="<?=$Year03?>"></td>
					<td>&nbsp;</td>
					<td><IMG src="../images/b_date.jpg" align="absmiddle" onkeydown="document.dataForm1.Hours.focus();" onclick="popFrame.fPopCalendar(Year03,Day03, Month03, Year03,popCal);return false;"></td>					
				</tr>
				</table>	
			</td>
		</tr>
		<tr>
			<td class="i_background03"><strong>������������ͺ���´�</strong></td>
			<td><input type="radio" name="hRegistryRecieveType01" value="direct" <?if($objOrder->getRegistryRecieveType01() == "direct" OR $objOrder->getRegistryRecieveType01() == "" ) echo "checked"?>>�١����Ѻ�ͧ&nbsp;&nbsp;<input type="radio" name="hRegistryRecieveType01" value="mail" <?if($objOrder->getRegistryRecieveType01() == "mail" ) echo "checked"?>>�����</td>
		</tr>
		<tr>			
			<td  class="i_background03"><strong>�����١����Ѻ����</strong></td>
			<td ><input type="text" name="hRecieveStockBlack" size="30" maxlength="100"  value="<?=$objOrder->getRegistryRecieveName01()?>"></td>			
		</tr>		
		<tr>			
			<td class="i_background03"><strong>�����˵�</strong></td>
			<td ><input type="text" name="hRemarkReturn" size="30"  value="<?=$objStockRedItem->getRemarkReturn()?>"></td>
		</tr>
		<tr>
			<td colspan="4" class="i_background03" align="center">
		        <input type="hidden" name="hSubmit" value="<?=$strMode?>">
				<?if ($strMode == "Update"){?>
					<input type="submit" name="hSubmit1" value="�ѹ�֡��¡�ä׹" class="button" >
				<?}else{?>
					<input type="submit" name="hSubmit1" value="�ѹ�֡��¡�ä׹" class="button" >
				<?}?>
		        &nbsp;&nbsp;&nbsp;&nbsp;<input type="Reset" name="hSubmit" value="��૵������" class="button" >
			
			</td>
		</tr>
		</table>
	</td>
</tr>
</form>
</table>	
<?}?>	
	</td>
</tr>
</table>
<table width="100%" class="i_background05">
<tr>
	<td>����¹����ѵԻ���ᴧ</td>
</tr>
</table>
<?
$objStockRedItemList = new StockRedItemList();
$objStockRedItemList->setFilter(" stock_red_id = $hId  ");
$objStockRedItemList->setPageSize(0);
$objStockRedItemList->setSortDefault(" stock_date DESC");
$objStockRedItemList->load();
?>
<table width="100%" class="i_background02" cellpadding="0" cellspacing="0">
<tr>
	<td>
		<table width="100%" cellpadding="3" cellspacing="1">
		<tr>
		<td class="listTitle" width="5%">&nbsp;</td>
		<td  align="center" width="45%" class="listDetail02"><strong>��¡���ԡ</strong></td>
		<td  align="center" width="45%" class="listDetail03"><strong>��¡�ä׹</strong></td>
		<td class="listTitle" width="5%"></td>
		</tr>
	<?
		$i=0;
		forEach($objStockRedItemList->getItemList() as $objItem) {
		$objSale = new Member();
		$objSale->setMemberId($objItem->getSaleId());
		$objSale->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->getStockBy());
		$objMember->load();
		
		$objStockCar = new StockCar();
		$objStockCar->setStockCarId($objItem->getStockCarId());
		$objStockCar->load();
		
		$objOrder = new Order();
		$objOrder->setOrderId($objStockCar->getOrderId());
		$objOrder->load();
		
		
		$objCustomer = new Customer();
		$objCustomer->setCustomerId($objOrder->getSendingCustomerId());
		$objCustomer->load();
		
		if(fmod($i,2) == 0){
			$bgColor= "e0e0e0";
		}else{
			$bgColor= "f2f2f2";
		}
		
	?>		
		<tr>			
			<td class="listTitle" valign="top"><?=$i+1?>.</td>
			<td  class="listDetail" valign="top" >
			<table width="100%">
			<tr>
				<td>�Ţ�����ԡ</td>
				<td>:</td>
				<td><?=$objItem->getStockNumber()?></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>�ѹ���ҷ���ԡ</td>
				<td>:</td>
				<td><?=$objItem->getStockDate()?> <?=$objItem->getStockTime()?></td>
				<td>Sale</td>
				<td>:</td>
				<td><?=$objSale->getFirstname()?> (<?=$objSale->getNickname()?>)</td>
			</tr>
			<tr>
				<td>�١���</td>
				<td>:</td>
				<td><?=$objCustomer->getFirstname()."  ".$objCustomer->getLastname()?></td>
				<td>�����Ţ�ѧ</td>
				<td>:</td>
				<td><?=$objStockCar->getCarNumber()?></td>
			</tr>
			<tr>
				<td>�Ţ��������</td>
				<td>:</td>
				<td><?=$objOrder->getSendingNumberNohead()?></td>
				<td>�Ҥ��Ѵ��</td>
				<td>:</td>
				<td><?=number_format($objItem->getPrice(),2)?></td>
			</tr>
			</table>		
			
			</td>			

			<?if($objItem->getStatus() == 1){?>
			<td  class="listDetail" valign="top" align="center"><font color=red><strong>�ѧ�������¡�ä׹����ᴧ</strong></font></td>
			
			<?}else{
		$objSale = new Member();
		$objSale->setMemberId($objItem->getSaleIdReturn());
		$objSale->load();
		
		$objMember = new Member();
		$objMember->setMemberId($objItem->getStockByReturn());
		$objMember->load();
		
		$objRecieveMoney = new Member();
		$objRecieveMoney->setMemberId($objItem->getStockByReturn());
		$objRecieveMoney->load();		
			?>
			<td  bgcolor="<?=$bgColor?>"   class="listDetail" valign="top" >
			<table width="100%">
			<tr>
				<td width="20%">�ѹ���ҷ���Ѻ�׹</td>
				<td>:</td>
				<td width="30%"><?=$objItem->getStockDateReturn()?> <?=$objItem->getStockTimeReturn()?></td>
				<td width="20%">������Ţ���</td>
				<td>:</td>
				<td width="30%"><?=$objItem->getReturnNumber()?></td>
			</tr>
			<tr>
				<td>����Ѻ�׹</td>
				<td>:</td>
				<td><?=$objMember->getFirstname()."  ".$objMember->getLastname()?></td>
				<td>����������Ѻ�׹</td>
				<td>:</td>
				<td><?=$objItem->getRecieveType()?></td>
			</tr>
			<tr>
				<td>����¹���´�</td>
				<td>:</td>
				<td><?=$objItem->getStockBlack()?></td>
				<td>Sale</td>
				<td>:</td>
				<td><?=$objSale->getFirstname()?> (<?=$objSale->getNickname()?>)</td>
			</tr>
			<tr>
				<td>�����˵�</td>
				<td>:</td>
				<td colspan="4"><?=$objItem->getRemarkReturn()?></td>
			</tr>
			</table>		
			
			</td>		
			<?}?>
			<td class="listTitle" align="center"><input type="button"  class="button"  value="ź��¡��" size=30  onclick="javascript:checkDelete('<?=$objItem->getStockRedItemId()?>');"></td>
		</tr>
		<?$i++;}?>
		</table>
	</td>
</tr>
</table>
<?}?>
<?if($hId!=""){?>
<script>
	function checkDelete(val){
		if(confirm('�س��ͧ���ź��¡�ù��������� ?')){
			window.location = "stockRedUpdate.php?hAStatus=<?=$hAStatus?>&hAKeyword=<?=$hAKeyword?>&hAPage=<?=$hAPage?>&hABlackCode=<?=$hABlackCode?>&hACustomer=<?=$hACustomer?>&hASendingNumber=<?=$hASendingNumber?>&hASearch=<?=$hASearch?>&hDelete="+val+"&hId="+<?=$hId?>;
		}else{
			return false;
		}	
	}
</script>
<?}?>
<script>
	new CAPXOUS.AutoComplete("hModel", function() {
		return "stockRedAutoSeries.php?q=" + this.text.value;
	});
	
	new CAPXOUS.AutoComplete("hStockName", function() {
		return "stockRedAutoStockName.php?q=" + this.text.value;
	});	
	
	new CAPXOUS.AutoComplete("hNewStockName", function() {
		return "stockRedAutoNewStockName.php?q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hStockCarNumber", function() {
		return "stockRedAutoCarNumber.php?q=" + this.text.value;
	});		
	
	new CAPXOUS.AutoComplete("hSaleName", function() {
		return "stockRedAutoSale.php?q=" + this.text.value;
	});		
	
</script>

<?include("h_footer.php");?>